# ACL ROS Package

## Introduction

This is a set of ROS packages that consist of the core of the real-time vehicle control in RAVEN.  Includes control packages for the quads and RC cars.

## Building

Just add the acl-ros-pkg directory to your ROS-PACKAGE-PATH and rosmake the packages you want.

## Dependencies

### acl-utils ###
Most packages depend on the [acl-utils] [acl-utils-link] library.

### raven-rviz ###
Visualization and vehicle interaction in RAVEN is typically done via the [raven-rviz] [raven-rviz-link] repository.

### vicon-filters ###
The Vicon data filters are located in the [vicon-filters] [vicon-filters-link] repository.

### Mosek ###
Path planner requires MOSEK optimization package to be installed.
You can request an academic license and follow the installation process at [MOSEK][mosek-site].
Add the following lines to your .bashrc (assuming you put your mosek folder in your home folder).

```
# mosek setup
export PATH=$HOME/mosek/7/tools/platform/linux64x86/bin:$PATH
export LD_LIBRARY_PATH=$HOME/mosek/7/tools/platform/linux64x86/bin:$LD_LIBRARY_PATH
```

### Eigen ###
The Eigen library should be installed under `/usr/include/Eigen`. If not you might get some compile errors complaining about not being able to find Eigen header files.

If yours is installed under `/usr/included/eigen3/Eigen`, you can create a symbolic link to it from the proper location by:
```
$ cd /usr/include
$ sudo ln -s eigen3/Eigen Eigen
```



[acl-utils-link]: https://bitbucket.org/acl-mit/acl-utils
[raven-rviz-link]: https://bitbucket.org/acl-mit/raven_rviz
[vicon-filters-link]: https://bitbucket.org/acl-mit/vicon-filters
[mosek-site]: https://mosek.com/