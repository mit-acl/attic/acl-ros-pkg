#!/usr/bin/env python
import roslib; roslib.load_manifest('cart_pole_sim')
import sys
import numpy as np
import rospy
import random
import math
import matplotlib.pyplot as plt
from cart_pole_sim.srv import RunStep
from cart_pole_sim.msg import CartPoleState

"""
State = [x, dx, theta, dtheta]
phi = [x, dx, theta, dtheta]
size of A = 4x4
size of b,z,phi = 10x1
"""

STATEDIM = 5
PHIDIM = 4
THETADIM = 5
MAXITERATION = 50000
DT = 1.0/60.0       # 60 Hz
class NAC():
    
    def __init__(self,name):
        self.name = name;
        self.theta0 = np.ones((THETADIM,1))#np.array([[1.],[0.001],[0.001],[0],[0],[0],[0],[0],[0],[0]])
        self.theta0 = np.array([[-5.71], [-11.3], [-82.1], [-21.6], [1]])
#         self.theta0 = np.array([[-200], [-200], [-400], [-200], [1]])
#         self.theta0 = np.array([[-10], [-200], [-400], [-200], [1]])
        self.ref = np.array([[3.],[0.],[0.]])
        
        # parameters
        self.Lambda = 0     # z rate
        self.alpha = 0.1    # learning rate
        self.beta = 0.9     # forgetting factor
        self.gamma = 0.95   # discount factor
        self.h = 1         # window over which we check w
        self.epsilon = math.pi/180  # check on angle between w vectors
        
        # reward function
        self.Q = np.diag(np.array([1.25, 1, 12, 0.25]))
        self.R = np.array([0.1])#0.01])
        
        # initialize variables
        self.currentState = CartPoleState()
        self.x = np.zeros((STATEDIM,MAXITERATION))
        self.x[:,0] = self.initialState()
        self.theta = np.zeros((THETADIM,MAXITERATION))
        self.theta[:,0:1] = self.theta0
        self.currentState.x = self.x[0,0]
        self.currentState.dx = self.x[1,0]
        self.currentState.theta = self.x[2,0]
        self.currentState.dtheta = self.x[3,0]
        self.A = np.zeros((PHIDIM+THETADIM,PHIDIM+THETADIM,MAXITERATION))
        self.b = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.z = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.u = np.zeros((MAXITERATION))
        self.r = np.zeros((MAXITERATION))
        self.phiTilde = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.phiHat = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.v = np.zeros((PHIDIM,MAXITERATION))
        self.w = np.zeros((THETADIM,MAXITERATION))
        print 'Starting state: ' + str(self.x[:,0:1])
        
    def initialState(self):
        x = np.zeros(STATEDIM)
#         print "init state"
#         print getRandomState(0,0,0,0,0.1,0,0,0)
#         print x[0:STATEDIM-1]
        x[0:STATEDIM-1] = getRandomState(0,0,0,0,0.3,0,0,0).transpose()
        x[-1] = 1; # eta
        return x
    
    def runAlg(self):
        for t in range(0,MAXITERATION-1):
#             print "iteration: " + str(t)
            self.Execute(t)
            self.CriticEvaluation(t)
            self.ActorUpdate(t)
            # check if state is valid
            if not self.checkState(self.x[:,t+1]):
                # reset state
                self.x[:,t] = self.initialState()
#                 print self.x[:,t]
                self.u[t] = getControl(self.x[0:STATEDIM-1,t], self.theta[:,t])
                self.x[:,t+1:t+2] = self.run_step(self.x[:,t:t+1], DT, self.u[t], True)
            print t
#            print 'Control: ' + str(self.u[t])
#            print 'State: ' + str(self.x[:,t+1:t+2])
#            print 'Reward: ' + str(self.r[t])
#            print 'Theta: ' + str(self.theta[:,t])

        self.plotResults()

    def plotResults(self):            
        x = np.arange(0, len(self.u))
        plt.plot(x, self.u.transpose(), label='Omega Desired')
        plt.plot(x, self.r.transpose(), label='Reward')
        plt.plot(x, self.x[0,:].transpose(), label='V_x')
        plt.plot(x, self.theta[0,:].transpose(), label='Theta_1')
        plt.legend()
        plt.show()

    def ActorUpdate(self,t):
        # in case nothing else, set new theta to old -- this may get overwritten below
        self.theta[:,t+1:t+2] = self.theta[:,t:t+1]
        
        # for first h runs, do nothing
        if t > self.h:
            update = True
            # check all the previous h w vectors
            for tau in range(self.h):
                if angleBetweenVectors(self.w[:,t+1], self.w[:,t-tau]) > self.epsilon:
                    update = False
            # if all of the w vectors are within tolerance, update
            if update == True:
                # update parameters
                self.theta[:,t+1:t+2] = self.theta[:,t:t+1] + self.alpha*self.w[:,t+1:t+2]
                eta = self.theta[-1,t+1]
                if eta > 50:
                    self.theta[-1:t+1] = 50
                elif eta < -50:
                    self.theta[-1:t+1] = -50
                print "theta: " + str(self.theta[:,t+1])
#                 print "w: " + str(self.w[:,t+1:t+2])
                # forget sufficient statistics
                self.z[:,t+1:t+2] *= self.beta
                self.A[:,:,t+1] *= self.beta
                self.b[:,t+1:t+2] *= self.beta
            

    def CriticEvaluation(self,t):
        # step 4.1 -- update basis functions
        self.phiTilde[0:PHIDIM,t:t+1] = self.getPhi(t+1)
        self.phiHat[0:PHIDIM,t:t+1] = self.getPhi(t)
        
#         print self.phiHat[PHIDIM:PHIDIM+THETADIM,t:t+1]
#         print self.getThetaGrad(t,self.theta[:,t:t+1])
        
        self.phiHat[PHIDIM:PHIDIM+THETADIM,t] = self.getThetaGrad(t,self.theta[:,t])
        
        # step 4.2 -- Update sufficient statistics
        self.z[:,t+1:t+2] = self.Lambda*self.z[:,t:t+1] + self.phiHat[:,t:t+1]
        tmp = self.phiHat[:,t:t+1] - self.gamma*self.phiTilde[:,t:t+1]
        self.A[:,:,t+1] = self.A[:,:,t] + np.dot(self.z[:,t+1:t+2], tmp.transpose())
        self.b[:,t+1:t+2] = self.b[:,t:t+1] + self.z[:,t+1:t+2]*self.r[t]
        
        # step 4.3 -- Update critic parameters
        if (np.linalg.det(self.A[:,:,t+1])) == 0:
            # pseudo inverse
            tmp = np.linalg.lstsq(self.A[:,:,t+1], self.b[:,t+1:t+2])[0]
        else:
            # true inverse
            tmp = np.linalg.solve(self.A[:,:,t+1], self.b[:,t+1:t+2])
        
        self.v[:,t+1:t+2] = tmp[0:PHIDIM]
        self.w[:,t+1:t+2] = tmp[PHIDIM:PHIDIM+THETADIM]
        

    def Execute(self,t):
        # step 3, run the simulation
        self.u[t] = getControl(self.x[0:STATEDIM-1,t], self.theta[:,t])
        self.x[:,t+1:t+2] = self.run_step(self.x[:,t:t+1], DT, self.u[t], True)
            
        self.r[t] = self.getReward(self.x[:,t:t+1],self.u[t])
#         print "reward: " + str(self.r[t])

    def checkState(self,s):
        x = s[0]
        dx = s[1]
        theta = s[2]
        dtheta = s[3]
        valid = True
        if theta > math.pi/6 or theta < -math.pi/6:
            valid = False
        if x > 1.5 or x < -1.5:
            valid = False
            
        return valid

    """ custom phi function """
    def getPhi(self,t):
        x = self.x[0,t]
        dx = self.x[1,t]
        theta = self.x[2,t]
        dtheta = self.x[3,t]
        phi = np.array([[x, dx, theta, dtheta]])
        return phi.transpose()
    
    """ custom theta gradient function """
    def getThetaGrad(self,t,theta):
        # theta = [K^t, eta]^t
        # pi = Normal(K^t*x, sigma^2)
        # d/dK log(pi) = 
        K = theta[0:STATEDIM-1]
        eta = theta[-1]
        sigma = eta2sigma(eta)
        
#         print "self.u"
#         print self.u
#         print self.u[t]
#         print K.transpose()
#         print self.x[:,t:t+1]
        x = self.x[0:STATEDIM-1,t]
        tmp = self.u[t] + np.dot(K.transpose(),x)
        dlogpi_dK = -x/sigma**2 * tmp
        dsigma_deta = -math.exp(eta)/(math.exp(eta) + 1)**2
        dlogpi_deta = (1/sigma**3 * tmp**2 - 1/sigma) * dsigma_deta
#         dlogpi_deta = dlogpi_deta[0]
        
#         dlogpi_dtheta = np.array([[dlogpi_dK],[dlogpi_deta]])
        dlogpi_dtheta = np.zeros(STATEDIM)
#         print dlogpi_dtheta
        dlogpi_dtheta[0:4] = dlogpi_dK.transpose()
        dlogpi_dtheta[4] = dlogpi_deta
#         print "dlogpi_dK"
#         print dlogpi_dK
#         print "dsigma_deta"
#         print dsigma_deta
#         print "dlogpi_deta"
#         print dlogpi_deta
#         print "dlogpi_dtheta"
#         print dlogpi_dtheta
        
        return dlogpi_dtheta
    
    
    def getReward(self,state,control):
        """ define custom reward function here """
        # r = x^T*Q*x + u^T*R*u
        x = state[0:STATEDIM-1]
#         print "s"
#         print x
#         print "Q"
#         print self.Q
#         print np.dot(x.transpose(),self.Q)
        r = -np.dot(np.dot(x.transpose(),self.Q),x) - np.dot(np.dot(control.transpose(),self.R),control)
        return r

    def run_step(self, x, dt, force, vis):
        
        s = self.currentState
        s.x = x[0]
        s.dx = x[1]
        s.theta = x[2]
        s.dtheta = x[3]
        eta = x[4] # just carry this over
        rospy.wait_for_service('/CP01/run_step')
        try:
            run_step = rospy.ServiceProxy('/CP01/run_step', RunStep)
            resp1 = run_step(s,dt,force, vis)
            self.currentState = resp1.finalState
            x = resp1.finalState.x
            dx = resp1.finalState.dx
            theta = resp1.finalState.theta
            dtheta = resp1.finalState.dtheta
            return np.array([[x], [dx], [theta], [dtheta], [eta]])
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

""" u = Normal(K^T*x, sigma^2) """
def getControl(x,theta):
    K = theta[0:STATEDIM-1]
    eta = theta[-1]
    sigma = eta2sigma(eta)
    
    u = random.normalvariate(-np.dot(K.transpose(),x), sigma)
    return u

#""" full state feedback, linear control """
#def getControl(state,ref,theta):
#    ff = ref[0]/(0.035/2.0) # feedforward wheel speed for reference body velocity
#    s = np.array(state)
#    r = np.array(ref)
#    t = np.array(theta)
#    u = -np.dot(t.transpose(),(s - r)) + ff
#    return u

def eta2sigma(eta):
    sigma = 0.1 + 1/(1 + math.exp(eta))
    return sigma

def angleBetweenVectors(a,b):
    tmp = np.dot(a,b)/(np.linalg.norm(a)*np.linalg.norm(b))
#    print 'tmp: ' + str(tmp)
    tmp = sat(tmp,1.0,-1.0)
    return math.acos(tmp)

def getRandomState(mu1,sigma1,mu2,sigma2,mu3,sigma3,mu4,sigma4):
    x = random.normalvariate(mu1,sigma1)
    dx = random.normalvariate(mu2,sigma2)
    theta = random.normalvariate(mu3,sigma3)
    dtheta = random.normalvariate(mu4,sigma4)
    x = np.array([[x],[dx],[theta],[dtheta]])
    return x

def sat(val,hi,low):
    if val > hi:
        val = hi
    elif val < low:
        val = low
    return val

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Error: need to specify vehicle name as input argument"
    else:
        vehName = sys.argv[1]
        print "Starting Natural Actor-Critic Algorithm node for: " + vehName
        
        try:
            c = NAC(vehName)
            rospy.init_node('NActorCritic')
            c.runAlg()
        except rospy.ROSInterruptException:
            pass
