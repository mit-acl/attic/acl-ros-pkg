#!/usr/bin/env python
import roslib; roslib.load_manifest('cart_pole_sim')
import sys
import numpy as np
import rospy
import random
import math
import matplotlib.pyplot as plt
from std_msgs.msg import Float64
from cart_pole_sim.msg import CartPoleState
from cart_pole_sim.srv import RunStep

class CartPole:
    
    cmd = Float64()
    
    def __init__(self):
        self.state = CartPoleState()
        self.pub = rospy.Publisher("CP01/cpCmd", Float64)
    
    def cpStateCB(self,data):
        self.state = data
        
        X = np.array([self.state.x, self.state.dx, self.state.theta, self.state.dtheta])
        K = np.array([-5.71, -11.3, -82.1, -21.6])

        self.cmd.data = -np.dot(K,X)
        self.pub.publish(self.cmd)


def mainFunc():
    rospy.init_node('cp_lqr_srv')
    c = CartPole()
    rospy.Subscriber("CP01/state", CartPoleState, c.cpStateCB)
    rospy.spin()
    

if __name__ == "__main__":
        
    try:
        mainFunc()
    except rospy.ROSInterruptException:
        pass
