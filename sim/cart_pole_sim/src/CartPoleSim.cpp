/*!
 * \file CartPoleSim.cpp
 *
 * CartPoleSim class file
 *
 *  Created on: Mar 25, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#include "CartPoleSim.hpp"


CartPoleSim::CartPoleSim() {
	dynamics.setInitialState(getInitialState());
	dynamicsStep.setInitialState(getInitialState());
	broadcastSrvState = false;

}

CartPoleSim::~CartPoleSim() {
	// TODO Auto-generated destructor stub
}

/**
 * Listen to CmdMessages (fom joystick or other program)
 * @param msg
 */
void CartPoleSim::cmdCallback(const std_msgs::Float64& msg) {
	dynamics.setForce(msg.data);
}

/**
 * @return An initial state struct with all zero elements
 */
acl::sCartPoleState CartPoleSim::getInitialState(void) {

	acl::sCartPoleState s;
	s.x = 0.0;
	s.dx = 0.0;
	s.theta = PI/8.0;
	s.dtheta = 0.0;

	return s;
}

/**
 * Run a single time step of the dynamics
 * @param req Initial state, time step, and action
 * @param res Final state
 * @return True
 */
bool CartPoleSim::runSimStep(cart_pole_sim::RunStep::Request &req,
		cart_pole_sim::RunStep::Response &res) {
	// set initial state
	dynamicsStep.setInitialState(rosState2state(req.startState));

	// set actions
	dynamicsStep.setForce(req.force);

	// integrate forward dynamics
	dynamicsStep.integrateStep(req.dt);

	// get new state
	res.finalState = state2RosState(dynamicsStep.getState());

	// choose whether or not the broadcast state will come from these dynamics or not
	broadcastSrvState = req.showVis;

	return true;
}

/**
 * Main function for running the simulation at a fixed rate of 1/DT Hz
 * @param e
 */
void CartPoleSim::runSim(const ros::TimerEvent& e){

	// integrate forward dynamics and get state
	dynamics.integrateStep(DT);
	cart_pole_sim::CartPoleState state;
	if (broadcastSrvState)
	{
    	state = state2RosState(dynamicsStep.getState());
    	state.F = dynamicsStep.getForce();
	}
	else
	{
    	state = state2RosState(dynamics.getState());
    	state.F = dynamics.getForce();
	}

	// publish results
	state.header.frame_id = ros::this_node::getNamespace().substr(1,4);
	state.header.stamp = ros::Time::now();
	state_pub.publish(state);
}

/**
 * Simple helper function for converting acl::sCartPoleState to ros message CarState
 * @param in
 * @return
 */
cart_pole_sim::CartPoleState CartPoleSim::state2RosState(acl::sCartPoleState in)
{
	cart_pole_sim::CartPoleState out;

	out.x = in.x;
	out.dx = in.dx;
	out.theta = in.theta;
	out.dtheta = in.dtheta;

	return out;
}

/**
 * Simple helper function for converting ros message CartPoleState to acl::sCartPoleState
 * @param in
 * @return
 */
acl::sCartPoleState CartPoleSim::rosState2state(cart_pole_sim::CartPoleState in){
	acl::sCartPoleState out;
	out.x = in.x;
	out.dx = in.dx;
	out.theta = in.theta;
	out.dtheta = in.dtheta;

	return out;
}
