/*!
 * \file CartPoleSim.hpp
 *
 * CartPoleSim header
 *
 *  Created on: Mar 25, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#ifndef CARSIM_HPP_
#define CARSIM_HPP_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"

// Local Messages/Services
#include "cart_pole_sim/CartPoleState.h"
#include "cart_pole_sim/RunStep.h"

// ACL Utils Library
#include "acl/dynamics/CartPoleDynamics.hpp"
#include "acl/utils.hpp"

const double DT = 1/60.0;	///< default simulation time step

/**
 *  Simple wrapper class for running CarDynamics in a ros node
 */
class CartPoleSim {
public:
	CartPoleSim();
	virtual ~CartPoleSim();

	ros::Publisher state_pub;
	acl::CartPoleDynamics dynamics;
	acl::CartPoleDynamics dynamicsStep;

	void cmdCallback(const std_msgs::Float64& msg);
	void runSim(const ros::TimerEvent& e);
	void integrateStep(double dt);
	bool runSimStep(cart_pole_sim::RunStep::Request &req, cart_pole_sim::RunStep::Response &res);

private:

	acl::sCartPoleState getInitialState(void);
	cart_pole_sim::CartPoleState state2RosState(acl::sCartPoleState);
	acl::sCartPoleState rosState2state(cart_pole_sim::CartPoleState);
	bool broadcastSrvState;
};

#endif /* CARSIM_HPP_ */
