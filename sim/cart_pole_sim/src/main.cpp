/*!
 * \file main.cpp
 *
 * Main function for the simple cart pole simulator
 *
 *  Created on: May 28, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "CartPoleSim.hpp"


int main(int argc, char **argv) {

	// Initialize vehicle
	CartPoleSim cp;

	ros::init(argc, argv, "sim");
	ros::NodeHandle n;

	// Check for proper renaming of node namespace
	if (!ros::this_node::getNamespace().compare("/")) {
		ROS_ERROR(
				"Error :: You should be using a launch file to specify the node namespace!\n");
		return (-1);
	}

	//## Welcome screen
	ROS_INFO(
			"\n\nStarting ROS Cart Pole Dynamics code for for %s...\n\n\n", ros::this_node::getNamespace().c_str());

	// Override car parameters if necessary

	// Set initial state if necessary

	// Set up state publishers
	cp.state_pub = n.advertise<cart_pole_sim::CartPoleState>("state", 0);

	// Set listener callbacks for command data
	ros::Subscriber sub_cmd = n.subscribe("cpCmd", 1,
			&CartPoleSim::cmdCallback, &cp);

	// initialize main controller loop
	ros::Timer controllerTimer = n.createTimer(ros::Duration(DT),
			&CartPoleSim::runSim, &cp);

	// Start service for requesting a single integrated step
	ros::ServiceServer service = n.advertiseService("run_step", &CartPoleSim::runSimStep, &cp);

//	double now = ros::Time::now().toSec();
//	for (int i=0; i<1000; i++){
//		car.dynamics.integrateStep(0.01);
//	}
//	ROS_INFO("Elapsed Time: %f",ros::Time::now().toSec() - now);


	// run the code
	ros::spin();

	return 0;
}



