/*!
 * \file InvertedPendulumSim.cpp
 *
 * InvertedPendulumSim class file
 *
 *  Created on: Mar 25, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#include "InvertedPendulumSim.hpp"


InvertedPendulumSim::InvertedPendulumSim() {
	dynamics.setInitialState(getInitialState());
	dynamicsStep.setInitialState(getInitialState());
	broadcastSrvState = false;
	prop1 = 0.1;
	prop2 = 0.5;

}

InvertedPendulumSim::~InvertedPendulumSim() {
	// TODO Auto-generated destructor stub
}

/**
 * Listen to CmdMessages (fom joystick or other program)
 * @param msg
 */
void InvertedPendulumSim::cmdCallback(const std_msgs::Float64& msg) {
	dynamics.setForce(msg.data);
}

/**
 * @return An initial state struct with all zero elements
 */
acl::sInvPendState InvertedPendulumSim::getInitialState(void) {

	acl::sInvPendState s;
	s.theta = PI;
	s.dtheta = 0.0;

	return s;
}

/**
 * Run a single time step of the dynamics
 * @param req Initial state, time step, and action
 * @param res Final state
 * @return True
 */
bool InvertedPendulumSim::runSimStep(inverted_pendulum_sim::RunStep::Request &req,
		inverted_pendulum_sim::RunStep::Response &res) {
	// set initial state
	dynamicsStep.setInitialState(rosState2state(req.startState));

	// set actions
	dynamicsStep.setForce(req.force);

	// integrate forward dynamics
	dynamicsStep.integrateStep(req.dt);

	// get new state
	res.finalState = state2RosState(dynamicsStep.getState());

	// choose whether or not the broadcast state will come from these dynamics or not
	broadcastSrvState = req.showVis;

	return true;
}

/**
 * Main function for running the simulation at a fixed rate of 1/SIM_DT Hz
 * @param e
 */
void InvertedPendulumSim::runSim(const ros::TimerEvent& e){

	// integrate forward dynamics
	dynamics.integrateStep(SIM_DT);
}

/**
 * Function for sending the simulation state at a fixed rate of 1/BRDCST_DT Hz
 * @param e
 */
void InvertedPendulumSim::sendState(const ros::TimerEvent& e){

	// get state
	inverted_pendulum_sim::State state;
	if (broadcastSrvState)
	{
    	state = state2RosState(dynamicsStep.getState());
    	state.force = dynamicsStep.getForce();
	}
	else
	{
    	state = state2RosState(dynamics.getState());
    	state.force = dynamics.getForce();
	}

	// publish results
	state.header.frame_id = ros::this_node::getNamespace().substr(1,4);
	state.header.stamp = ros::Time::now();
	state_pub.publish(state);

	//update joint_state
	sensor_msgs::JointState joint_state;
	joint_state.header.stamp = ros::Time::now();
	joint_state.name.resize(3);
	joint_state.position.resize(3);
	joint_state.name[0] ="arm_joint";
	joint_state.position[0] = state.theta;
	joint_state.name[1] ="prop1_joint";
	joint_state.position[1] = prop1;
	joint_state.name[2] ="prop2_joint";
	joint_state.position[2] = prop2;
	prop1 += 1;
	prop2 += 1;

	//send the joint state and transform
	joint_pub.publish(joint_state);
}

/**
 * Simple helper function for converting acl::sInvertedPendulumState to ros message CarState
 * @param in
 * @return
 */
inverted_pendulum_sim::State InvertedPendulumSim::state2RosState(acl::sInvPendState in)
{
	inverted_pendulum_sim::State out;

	out.theta = acl::wrap(in.theta);
	out.dtheta = in.dtheta;

	return out;
}

/**
 * Simple helper function for converting ros message InvertedPendulumState to acl::sInvertedPendulumState
 * @param in
 * @return
 */
acl::sInvPendState InvertedPendulumSim::rosState2state(inverted_pendulum_sim::State in){
	acl::sInvPendState out;

	out.theta = in.theta;
	out.dtheta = in.dtheta;

	return out;
}
