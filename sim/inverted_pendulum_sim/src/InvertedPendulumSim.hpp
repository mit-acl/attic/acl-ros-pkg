/*!
 * \file InvertedPendulumSim.hpp
 *
 * InvertedPendulumSim header
 *
 *  Created on: June 24, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#ifndef CARSIM_HPP_
#define CARSIM_HPP_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include <sensor_msgs/JointState.h>

// Local Messages/Services
#include "inverted_pendulum_sim/State.h"
#include "inverted_pendulum_sim/RunStep.h"

// ACL Utils Library
#include "acl/dynamics/InvertedPendulumDynamics.hpp"
#include "acl/utils.hpp"

const double SIM_DT = 1/100.0;	///< default simulation time step
const double BRDCST_DT = 1/30.0; ///< default state broadcast time step

/**
 *  Simple wrapper class for running CarDynamics in a ros node
 */
class InvertedPendulumSim {
public:
	InvertedPendulumSim();
	virtual ~InvertedPendulumSim();

	ros::Publisher state_pub, joint_pub;
	acl::InvPendDynamics dynamics;
	acl::InvPendDynamics dynamicsStep;

	void cmdCallback(const std_msgs::Float64& msg);
	void runSim(const ros::TimerEvent& e);
	void sendState(const ros::TimerEvent& e);
	void integrateStep(double dt);
	bool runSimStep(inverted_pendulum_sim::RunStep::Request &req, inverted_pendulum_sim::RunStep::Response &res);

private:

	acl::sInvPendState getInitialState(void);
	inverted_pendulum_sim::State state2RosState(acl::sInvPendState);
	acl::sInvPendState rosState2state(inverted_pendulum_sim::State);
	double prop1, prop2;
	bool broadcastSrvState;
};

#endif /* CARSIM_HPP_ */
