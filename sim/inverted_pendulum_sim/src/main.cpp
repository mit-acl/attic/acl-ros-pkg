/*!
 * \file main.cpp
 *
 * Main function for the simple inverted pendulum simulator
 *
 *  Created on: June 24, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "InvertedPendulumSim.hpp"


int main(int argc, char **argv) {

	// Initialize vehicle
	InvertedPendulumSim ip;

	ros::init(argc, argv, "sim");
	ros::NodeHandle n;

	// Check for proper renaming of node namespace
	if (!ros::this_node::getNamespace().compare("/")) {
		ROS_ERROR(
				"Error :: You should be using a launch file to specify the node namespace!\n");
		return (-1);
	}

	//## Welcome screen
	ROS_INFO(
			"\n\nStarting ROS Inverted Pendulum Dynamics code for for %s...\n\n\n", ros::this_node::getNamespace().c_str());

	// Override parameters if necessary
	acl::sInvPendParam params;
	params.m = 0.5;
	params.l = 0.75;
	params.l_cg = 0.5;
	params.k_fric = .011;
	params.J = 0.2;
	params.a = 0;
	params.noise_level = 0;
	ip.dynamics.setParamStruct(params);

	// Set initial state if necessary
	acl::sInvPendState state;
	state.dtheta = 0;
	state.theta = 0.01;
	ip.dynamics.setInitialState(state);

	// Set up state publishers
	ip.state_pub = n.advertise<inverted_pendulum_sim::State>("state", 1);
	ip.joint_pub = n.advertise<sensor_msgs::JointState>("/joint_states", 1);

	// Set listener callbacks for command data
	ros::Subscriber sub_cmd = n.subscribe("ipCmd", 1,
			&InvertedPendulumSim::cmdCallback, &ip);

	// initialize main controller loop
	ros::Timer controllerTimer = n.createTimer(ros::Duration(SIM_DT),
			&InvertedPendulumSim::runSim, &ip);

	// initialize state_data broadcast loop
	ros::Timer broadcastTimer = n.createTimer(ros::Duration(BRDCST_DT),
			&InvertedPendulumSim::sendState, &ip);

	// Start service for requesting a single integrated step
	ros::ServiceServer service = n.advertiseService("run_step", &InvertedPendulumSim::runSimStep, &ip);

	// run the code
	ros::spin();

	return 0;
}



