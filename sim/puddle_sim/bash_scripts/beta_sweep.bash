#!/bin/bash
PKG=/home/mark/acl-ros-pkg/sim/puddle_sim/log
NUM_CORES=8
export ROSLAUNCH_SSH_UNKNOWN=1

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=0.0 beta2:=0.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_0p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=0.5 beta2:=0.5 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_0p5_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=1. beta2:=1.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_1p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=1.5 beta2:=1.5 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_1p5_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=2. beta2:=2.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_2p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=3. beta2:=3.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_3p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=4. beta2:=4.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_4p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=5. beta2:=5.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_5p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=6. beta2:=6.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_6p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=7. beta2:=7.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_7p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=8. beta2:=8.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_8p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=9. beta2:=9.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_9p0_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 beta1:=10. beta2:=10.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_beta_10p0_1000.p
echo "1000 runs complete"
