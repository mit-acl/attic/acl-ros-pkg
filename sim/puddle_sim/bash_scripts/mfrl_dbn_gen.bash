#!/bin/bash
PKG=/home/mark/acl-ros-pkg/sim/puddle_sim/log
NUM_CORES=8
export ROSLAUNCH_SSH_UNKNOWN=1

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll_ps.launch numruns:=12 epsilon:=0.15 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_ps_eps15_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll_ps.launch numruns:=12 epsilon:=0.1 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_ps_eps1_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll_ps.launch numruns:=12 epsilon:=0.2 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_ps_eps2_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 unidirectional:=true &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_uni_1000.p
echo "1000 uni runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[3] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_just_top_1000.p
echo "1000 just top runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[2] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels2_1000.p
echo "1000 just top runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels1_1000.p
echo "1000 just top runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1,2] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels12_1000.p
echo "1000 just top runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[2,1] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels21_1000.p
echo "1000 just top runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1,3] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels13_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[2,3] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels23_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[3,2,1] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels321_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1,3,2] &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels132_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1,3,2] beta1:=0.0 beta2:= 1.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels132_beta1_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1,3,2] beta1:=0.0 beta2:= 2.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels132_beta2_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 levels:=[1,3,2] beta1:=0.0 beta2:= 5.0 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_levels132_beta5_1000.p
echo "1000 runs complete"


rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 useDBN:=true &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_dbn_1000.p
echo "1000 DBN runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 useDBN:=true unidirectional:=true &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_dbn_uni_1000.p
echo "1000 uni dbn runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 generative_access:=true &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_gen_1000.p
echo "1000 gen runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 real_world_samples:=4000 generative_access:=true useDBN:=true &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_dbn_gen_1000.p
echo "1000 dbn gen runs complete"
