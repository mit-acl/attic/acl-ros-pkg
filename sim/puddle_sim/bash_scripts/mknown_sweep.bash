#!/bin/bash
PKG=/home/mark/acl-ros-pkg/sim/puddle_sim/log
NUM_CORES=8
export ROSLAUNCH_SSH_UNKNOWN=1

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=5 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_5_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=15 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_15_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=30 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_30_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=45 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_45_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=60 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_60_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=75 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_75_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=90 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_90_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=105 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_105_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 mknown:=120 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_mknown_120_1000.p
echo "1000 runs complete"

