#!/bin/bash
PKG=/home/mark/acl-ros-pkg/sim/puddle_sim/log
NUM_CORES=8
export ROSLAUNCH_SSH_UNKNOWN=1

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=1 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_1_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=5 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_5_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=10 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_10_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=15 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_15_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=20 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_20_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=25 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_25_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=30 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_30_1000.p
echo "1000 runs complete"

rosrun puddle_sim save_reward.py &
for ((i=0;i<NUM_CORES;i++)); do
    roslaunch puddle_sim puddleAll.launch numruns:=12 munknown:=40 &
done
wait
cp $PKG/total_reward.p $PKG/total_reward_munknown_40_1000.p
echo "1000 runs complete"

