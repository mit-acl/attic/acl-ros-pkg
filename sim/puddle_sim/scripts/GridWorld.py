#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import copy

class GridWorld():


    def __init__(self, noise_level, rand_state=None):

        # seed random number generator
        self.rand_state = rand_state
        if self.rand_state == None:
            self.rand_state = np.random.RandomState()

        self.noise_level = noise_level
        self.num_actions = 4
        self.max_x = 3
        self.max_y = 4
        self.obstacles = {(2, 1), (0, 2), (0, 3), (1, 3)}
        self.goal = {(0, 4)}
        
        self.reward_map = np.zeros((self.max_x+1, self.max_y+1))
        s = np.zeros((2))
        for x in range(self.max_x+1):
            for y in range(self.max_y+1):
                s[0] = x
                s[1] = y
                self.reward_map[x, y] = self.reward(s)
        
        self.domain_fig = None;
        plt.ion()
        
    def gridStep(self, s, a, show_domain):
        sp, r, t = self.step(s, a)
        if show_domain:
            self.showDomain(sp);
        return sp, r, t

    def valid_state(self, s):
        # check if outside the boundaries
        if s[0] < 0 or s[0] > self.max_x:
            return False
        if s[1] < 0 or s[1] > self.max_y:
            return False
        
        # check if in obstacles
        if tuple(s) in self.obstacles:
            return False
        
        return True

    def isTerminal(self, s):
        if tuple(s) in self.goal:
            return True
        return False
    
    def reward(self, s):
        if tuple(s) in self.goal:
            return 100
        if tuple(s) in self.obstacles:
            return -100
        return 0

    def step(self, s, a):
        a = int(a)  # ensure a is an integer, not a 0d numpy array
        if a < 0:  # if you send a negative action, just get the reward for
                    # where you currently are -- helpful for getting terminal
                    # states
            ns = s
        else:
            if np.random.uniform() < self.noise_level:
                # with noise_level probability, choose a different action
                new_a = np.random.randint(self.num_actions)
                while a == new_a:
                    new_a = np.random.randint(self.num_actions)
                a = new_a
            
            ns = copy.copy(s)
            if a == 0:
                ns[0] += 1
            elif a == 1:
                ns[1] += 1
            elif a == 2:
                ns[0] -= 1
            elif a == 3:
                ns[1] -= 1
            
            if not self.valid_state(ns):
                ns = s
            
        return (ns, self.reward(ns), self.isTerminal(ns))
    
    def updatePlot(self):
        self.domain_fig.axes[0].set_xlim(0, 1)
        self.domain_fig.axes[0].set_ylim(0, 1)
        self.domain_fig.canvas.draw()

    def showDomain(self, s):
        pass
#         Draw the environment
        if self.domain_fig is None:

            # define the colormap
            cmap = plt.cm.get_cmap()
            cmaplist = [cmap(i) for i in range(cmap.N)]
            # force the first color entry to be green
            cmaplist[-1] = (1, 0, 1, 1.0)
            # create the new map
            cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)

            self.domain_fig, ax = plt.subplots()
            self.reward_im = ax.imshow(self.reward_map, extent=(0, self.max_y+1,
                                                                0, self.max_x+1),
                                       origin="lower",
                                       interpolation='none',
                                       vmin=
                                       -100,
                                       vmax=100,
                                       cmap=cmap)

            self.state_mark = ax.plot(s[1]+0.5, s[0]+0.5, 'kd', markersize=20)

            plt.axis('off')

            self.domain_fig.canvas.draw()
        else:

            self.state_mark[0].set_data([s[1]+0.5], [s[0]+0.5])
            self.domain_fig.canvas.draw()

