#!/usr/bin/env python
import roslib
roslib.load_manifest('puddle_sim')
from puddle_sim.srv import *
import rospy
from std_msgs.msg import MultiArrayDimension
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


class PuddleWorld():

    def __init__(self, noise_level, d, rand_state=None):

        # seed random number generator
        self.rand_state = rand_state
        if self.rand_state == None:
            self.rand_state = np.random.RandomState()

        # first two levels are deterministic
        self.noise_level = noise_level
        self.d = d
        # puddles
        self.puddles = np.array([[[0.2, .75], [.75, .75]], [[.75, .2],
                                                            [.75, .75]]])
        # "freeway" between puddles that only exists in mid-level sim
        self.plow1 = np.array([.2, 0.0])
        self.plow2 = np.array([1.0, 0.8])
        self.phi1 = np.array([0.0, 0.2])
        self.phi2 = np.array([0.8, 1.0])

        self.reward_map = np.zeros((100, 100))
        a = np.zeros((2))
        for i, x in enumerate(np.linspace(0, 1, 100)):
            for j, y in enumerate(np.linspace(0, 1, 100)):
                a[0] = x
                a[1] = y
                self.reward_map[j, i] = self.reward(a)

        self.actions_num = 4
        self.domain_fig = None;
        plt.ion()

        # diagonal actions
        d = np.sqrt(2.0) / 2.0
        self.actions = np.sqrt(2 * 0.07 ** 2) * np.array(
                         [[d, d], [d, -d], [-d, d], [-d, -d]], dtype="float")

        self.frame_num = 1

        # self.showDomain(np.array([0, 0]))
        # imlist = []
        # for i in np.arange(5):
        #     for j in np.arange(5):
        #         imlist.append(self.plotUnknownState(np.array([i / 10.0, i / 10.0 + .1, j / 10.0, j / 10.0 + .1])))

        # imlist[0].remove()

        # self.updatePlot()

        # set up publishers
       # self.state_pub = rospy.Publisher('/puddle_world/state', Float64MultiArray)
        # self.reward_pub = rospy.Publisher('/puddle_world/reward', Float64MultiArray)


    def puddleStep(self, s, a, show_domain):
        sp, r, t = self.step(s, a)
        if show_domain:
            self.showDomain(sp);
        return sp, r, t

    def handle_puddleStep(self, req):
        [s, r, t] = self.step(np.array([req.x, req.y]), req.a)
        if req.showDomain:
            self.showDomain(s);
        return PuddleStepResponse(s[0], s[1], r, t)

    def puddle_step_server_start(self):
        rospy.init_node('puddle_step_server')
        s = rospy.Service('puddle_step', PuddleStep, self.handle_puddleStep)
        rospy.spin()

    def isTerminal(self, s):
        return s.sum() > 0.85 * 2

    def step(self, s, a):
        a = int(a)  # ensure a is an integer, not a 0d numpy array
        if a < 0:  # if you send a negative action, just get the reward for
                    # where you currently are -- helpful for getting terminal
                    # states
            ns = s
        else:
            a = self.actions[a]
            noise = self.rand_state.normal()
            #print noise
            noise *= self.noise_level
            ns = s + a + noise
            # make sure we stay inside the [0,1]^2 region
            ns = np.minimum(ns, 1.)
            ns = np.maximum(ns, 0.)
        return (ns, self.reward(ns), self.isTerminal(ns))

    def reward(self, s):
        if self.isTerminal(s):
            return 0  # goal state reached
        reward = -1
        if self.d != 0:
            dists = self.puddleReward(s)
            if len(dists):
                reward -= 2 * 400 * (0.1 - dists[dists < 0.1]).max()
            if self.d == 1 and self.isLeft(s) and self.isRight(s):
                reward = -1
        return reward

    # compute puddle influence
    def puddleReward(self, s):
        d = self.puddles[:, 1, :] - self.puddles[:, 0, :]
        denom = (d ** 2).sum(axis=1)
        g = ((s - self.puddles[:, 0, :]) * d).sum(axis=1) / denom
        g = np.minimum(g, 1)
        g = np.maximum(g, 0)
        dists = np.sqrt(((self.puddles[:, 0, :] + g * d - s) ** 2).sum(axis=1))
        dists = dists[dists < 0.1]
        return dists

    # algorithm from
    # http://stackoverflow.com/questions/3461453/determine-which-side-of-a-line-a-point-lies
    def isLeft(self, s):
        x1 = self.plow1[0]
        x2 = self.plow2[0]
        y1 = self.plow1[1]
        y2 = self.plow2[1]
        return ((x2 - x1) * (s[1] - y1) - (y2 - y1) * (s[0] - x1)) > 0.0

    # algorithm from
    # http://stackoverflow.com/questions/3461453/determine-which-side-of-a-line-a-point-lies
    def isRight(self, s):
        x1 = self.phi1[0]
        x2 = self.phi2[0]
        y1 = self.phi1[1]
        y2 = self.phi2[1]
        return ((x2 - x1) * (s[1] - y1) - (y2 - y1) * (s[0] - x1)) < 0.0

    # s is a 1x4 numpy array containing [left x, right x, lower y, upper y]
    def plotUnknownState(self, s):
        pass
        # ax = self.domain_fig.gca()
        # # plot unknown states
        # return ax.imshow(np.array([[0, 0], [0, 0]]), extent=(s),
        #                  cmap='Greys', alpha=0.3)

    def updatePlot(self):
        self.domain_fig.axes[0].set_xlim(0, 1)
        self.domain_fig.axes[0].set_ylim(0, 1)
        self.domain_fig.canvas.draw()

    def showDomain(self, s):
        pass
#         Draw the environment
        if self.domain_fig is None:

            # define the colormap
            cmap = plt.cm.get_cmap()
            cmaplist = [cmap(i) for i in range(cmap.N)]
            # force the first color entry to be green
            cmaplist[-1] = (1, 0, 1, 1.0)
            # create the new map
            cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)

            self.domain_fig, ax = plt.subplots()
            self.reward_im = ax.imshow(self.reward_map, extent=(0, 1,
                                                                0, 1),
                                       origin="lower",
                                       vmin=
                                       -2*40,
                                       vmax=0,
                                       interpolation='bilinear',
                                       cmap=cmap)

            self.state_mark = ax.plot(s[0], s[1], 'kd', markersize=20)

            plt.axis('off')

            # create a second axes for the colorbar
            # if self.d == 2:
#                 ax2 = self.domain_fig.add_axes([0.85, 0.1, 0.03, 0.8])
#                 cb = mpl.colorbar.ColorbarBase(ax2, cmap=cmap,
#                                                spacing='proportional',
#                                                format='%1i',
#                                                boundaries=np.linspace(-2*40,
#                                                                       0, 256))

#                 ax2.set_ylabel('Reward', size=25)
# #                 ax2.xaxis.set_tick_params(labelsize=40)

            self.domain_fig.canvas.draw()
#             plt.savefig('/home/mark/acl-ros-pkg/sim/puddle_sim/figures/world'
#                         + str(self.d) + '.pdf', format='pdf',
#                         transparent=True, bbox_inches='tight')
        else:
#             if d != self.old_level:
#                 self.old_level = d
#                 print "\nhere\n"
#                 self.reward_im.set_data(self.reward_map)
            self.state_mark[0].set_data([s[0]], [s[1]])
#             plt.draw()
            self.domain_fig.canvas.draw()

            # for making movies
            #fname = '/home/mark/acl-ros-pkg/sim/puddle_sim/video/level_%d_%04d.png' % (self.d, self.frame_num)
            #self.domain_fig.savefig(fname, pad_inches=0,
            #                        transparent=True)
            #self.frame_num += 1

# to make a move with these saved images:
# 1) crop the images: mogrify -background none -extent 490x490+171+60 level_*.png
# 2) create movies: avconv -r 10 -i level_0_%04d.png -b:v 1000k test0.mp4 (one for each level)

#-----------------------------------------------------------------------------
# 2D numpy array to multiarray
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def numpyArray2DToMultiarray(na, ma):
    I = na.shape[0]
    J = na.shape[1]
    d1 = MultiArrayDimension()
    d2 = MultiArrayDimension()
    ma.layout.dim = [d1, d2]
    ma.layout.dim[0].size = I
    ma.layout.dim[0].stride = I * J
    ma.layout.dim[1].size = J
    ma.layout.dim[1].stride = J
    for i in range(I):
        ma.data = np.append(ma.data, na[i, :])

if __name__ == "__main__":
    pw = PuddleWorld(0.02, 2);
    pw.showDomain(np.array([0,0]))
    pw.showDomain(np.array([0.45,0.99]))
    pw.puddle_step_server_start()
