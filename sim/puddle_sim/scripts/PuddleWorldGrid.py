#!/usr/bin/env python

from puddle_sim.srv import *
import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayDimension
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import PuddleWorld as pw

class PuddleWorldGrid(pw.PuddleWorld):


    def __init__(self, noise_level, d):

        pw.PuddleWorld.__init__(self, noise_level, d)

        # grid world actions
        d = np.sqrt(2.0) / 2.0
        self.actions = np.sqrt(2 * 0.1 ** 2) * np.array(
                         [[d, d], [d, -d], [-d, d], [-d, -d]], dtype="float")

#-----------------------------------------------------------------------------
# Just override how the step function happens for the grid version of this world
#-----------------------------------------------------------------------------
    def step(self, s, a):
        a = int(a)  # ensure a is an integer, not a 0d numpy array
        if a < 0:  # if you send a negative action, just get the reward for
                    # where you currently are -- helpful for getting terminal
                    # states
            ns = s
        else:
            if np.random.uniform() < self.noise_level:
                # with noise_level probability, choose a different action
                new_a = np.random.randint(self.actions_num)
                while a == new_a:
                    new_a = np.random.randint(self.actions_num)
                a = new_a
            a = self.actions[a]
            ns = np.copy(s)
            for i in np.arange(2):
                if (s[i] + a[i]) < 1.0 and (s[i] + a[i]) > 0.:
                    ns[i] = s[i] + a[i]
        return (ns, self.reward(ns), self.isTerminal(ns))
