#!/usr/bin/env python
'''
Description: Reinforcement Learning using Multi-Fidelity Simulators

See ICRA 2014 paper by the above title for algorithm details

This class won't run on it's own.  It several functions need to be implemented
in a derived class.

Created on Nov 27, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import sets
import matplotlib.pyplot as plt
import cPickle as pickle
import copy
import os

from aclpy import utils
from aclpy import rmax
from aclpy import rmax_dbn
#import rmax
#import rmax_dbn


class MFRL:

    def __init__(self, name,
                unidirectional, generative_access, useDBN,
                levels, bandit, mr, mt, logFileIndex,
                logFile0, logFile1, logFile2):

        if self.rand_state == None:
            self.rand_state = np.random.RandomState()

        self.name = name
        self.unidirectional = unidirectional
        self.useDBN = useDBN
        self.generative_access = generative_access
        self.bandit = bandit
        self.mr = mr
        self.mt = mt
        self.gamma = 0.95

        # these values will probably be overridden in derived classes
        self.numConvStates = [100, 100, 50]
        self.mDown = [np.Inf, 20, 20]
        self.beta = [0.1, 0.1]
        self.saveQRate = 25  # save Q at top level every this many environment interactions
        self.showDomain = True

        # flags and counters for mfrl algorithm
        self.flags = MFRLFlags()

        # data structure for storing states that need to be viewed at lower level
        if self.generative_access:
            self.unknownStates = [set(), set(), set()]

        self.logFile = self.openLogFile(logFileIndex)

        savedLog = [logFile0, logFile1, logFile2]
        self.initMFRmax(levels)
        if self.bandit:
            self.initBandit(savedLog)
        else:
            self.initStateBased(savedLog, levels)

        self.flags.tmp_generative_state = self.initState()
        self.flags.tmp_generative_action = self.rmax[self.d].choose_action(self.cont2discState(self.flags.tmp_generative_state))


#-----------------------------------------------------------------------------
# Initialize the MF-Rmax algorithm
#-----------------------------------------------------------------------------
    def initMFRmax(self, levels):
        self.numD = len(levels)  # number of simulators
        self.d = 0
        self.con = [False] * self.numD  # each level starts not converged
        self.level_iterations = [[] for i in range(self.numD)]  # for storing
        # number of sim calls at each simulation level
        self.level_directions = [[] for i in range(self.numD)]  # for storing
        # direction of MRFL level changes
        self.bestQ = []  # save best Q for highest level every few iterations
        self.bestQ_samples = []  # real world sample number at which Q was saved

#-----------------------------------------------------------------------------
# Initialize the Rmax algorithm
#-----------------------------------------------------------------------------
    def initRmax(self, S, A, U, savedLog,
                 logFile, s_prime2sa=None, Rmax=0, levels=[1,2,3]):

        self.rmax = []
        cnt = 0
        for i in levels:
            print i
            print
            self.ts = self.getTerminalStates(cnt)
            cnt += 1
            if self.useDBN:
                self.rmax.append(rmax_dbn.RmaxDBN(S, A, U, self.ts,
                                logFile + 'log' + str(i-1) + '.npz',
                                savedLog=savedLog[i-1], mr=self.mr[i-1],
                                mt=self.mt[i-1], gamma=self.gamma,
                                s_prime2sa=s_prime2sa, Rmax=Rmax, rand_state=self.rand_state))
            else:
                self.rmax.append(rmax.Rmax(S, A, U, self.ts,
                                logFile + 'log' + str(i-1) + '.npz',
                                savedLog=savedLog[i-1], mr=self.mr[i-1],
                                mt=self.mt[i-1], gamma=self.gamma, rand_state=self.rand_state))

        # return another rmax handle for evaluating policies
        return rmax.Rmax(S, A, U, self.ts)

###########
# Functions to be implemented in derived classes
###########

#-----------------------------------------------------------------------------
# Open log files -- save in /package_path/log/
# File will have current time tacked onto it so we don't overwrite other files
#-----------------------------------------------------------------------------
    def openLogFile(self, logFileIndex):
        if logFileIndex < 0 :
            # use time as a random file extender
            logFileIndex = int(rospy.get_time() * 10000)
        # get the file path for puddle_sim
        logfile_dir = self.package_address + '/log/'
        if not os.path.exists(logfile_dir):
            os.makedirs(logfile_dir)
        self.logFileAddress = logfile_dir + 'mfrl_' + str(logFileIndex) + '.p'
        return open(self.logFileAddress, 'wb')

#-----------------------------------------------------------------------------
# Initialize state-based state-action space
#-----------------------------------------------------------------------------
    def initStateBased(self, savedLog, levels=[1,2,3]):
        rospy.logerr('Need to implement in derived class!')

#-----------------------------------------------------------------------------
# Initialize bandit state-action space
#-----------------------------------------------------------------------------
    def initBandit(self, savedLog):
        rospy.logerr('Need to implement in derived class!')

#-----------------------------------------------------------------------------
# Set initial state
#-----------------------------------------------------------------------------
    def initState(self):
        rospy.logerr('Need to implement in derived class!')

#-----------------------------------------------------------------------------
# Check if it is ok to run value iteration right now
# Should either run value iteration and return False, or return True
#-----------------------------------------------------------------------------
    def shouldRunValIteration(self):
        return True

#-----------------------------------------------------------------------------
# Reset the state
#-----------------------------------------------------------------------------
    def resetState(self):
        pass

#-----------------------------------------------------------------------------
# Call any custom plotting or other functions when moving up a level
#-----------------------------------------------------------------------------
    def moveUpCustom(self):
        pass

#-----------------------------------------------------------------------------
# Call any custom plotting or other functions when moving down a level
#-----------------------------------------------------------------------------
    def moveDownCustom(self):
        pass

#-----------------------------------------------------------------------------
# Call any custom functions when finished learning
#-----------------------------------------------------------------------------
    def closeCustom(self):
        pass

#-----------------------------------------------------------------------------
# Run a single step
#-----------------------------------------------------------------------------
    def getNextState(self, s, a, d):
        rospy.logerr('Need to implement in derived class!')
        return sp, r, term

#-----------------------------------------------------------------------------
# Set a set of terminal states
#-----------------------------------------------------------------------------
    def getTerminalStates(self, d):
        ts = sets.Set([])
        return ts

#-----------------------------------------------------------------------------
# Converts continuous states to discrete
#-----------------------------------------------------------------------------
    def cont2discState(self, cs):
        rospy.logerr('Need to implement in derived class!')

#-----------------------------------------------------------------------------
# Copy variables that are missing at the current level to the level above
#-----------------------------------------------------------------------------
    def copyMissingVars(self):
        pass

#-----------------------------------------------------------------------------
# UnknownStatesPlot -- plot all states when they become known
#-----------------------------------------------------------------------------
    def plotUnknownStates(self, s, a):
        pass

#-----------------------------------------------------------------------------
# Plot custom things like Q values
#-----------------------------------------------------------------------------
    def moveLevelPlot(self):
        pass

#-----------------------------------------------------------------------------
# Move down a level
#-----------------------------------------------------------------------------
    def moveDownLevel(self):
        # go down a level
        rospy.loginfo("\n\nMoving down to level %d\n\n", self.d - 1)

        self.moveDownCustom()

        # save pertinent data
        self.saveLog(-1)

        self.con[self.d - 1] = False
        self.d -= 1
        self.s = self.initState()  # self.init_state
        # run value iteration a level down
        self.flags.run_val_iteration = True
        self.flags.update_counter = 0
        self.flags.num_unknown_states = 0

#-----------------------------------------------------------------------------
# Move up a level
#-----------------------------------------------------------------------------
    def moveUpLevel(self):
        self.con[self.d] = True

        rospy.loginfo("\n\nMoving up to level %d\n\n", self.d + 1)

        # we only need to copy the missing variables the first time we move up
        if not self.flags.vars_copied:
            self.copyMissingVars()
            self.flags.vars_copied = True

        self.moveUpCustom()

        # set important values before moving up
        self.setQ()
        self.d += 1
        self.s = self.initState()  # self.init_state
        self.flags.reset_state = True
        self.flags.update_counter = 0
        self.flags.num_unknown_states = 0

#-----------------------------------------------------------------------------
# Get the next state and action from the list of unknown state-action pairs
# if you are doing generative access
#-----------------------------------------------------------------------------
    def processGenerativeAccess(self):
        # In the generative access case, run iterations started at unknown states above.
        # Each iteration should be run until that s,a pair becomes known
        # Once this is complete, start the learning from this level as normal

        sa = tuple(self.cont2discState(self.flags.tmp_generative_state)) + tuple(self.flags.tmp_generative_action)
        while self.rmax[self.d].stateActionKnown(sa):
            try:
                self.flags.tmp_generative_state, self.flags.tmp_generative_action = self.unknownStates[self.d + 1].pop()
            except:
                # done with generative access, start at this level from initial state
                self.flags.tmp_generative_state = self.initState()
                self.flags.tmp_generative_action = tuple(self.rmax[self.d].choose_action(self.cont2discState(self.flags.tmp_generative_state)))
                self.flags.doing_generative_eval = False
                break
            s = self.flags.tmp_generative_state
            a = np.array(self.flags.tmp_generative_action)
            sa = tuple(self.cont2discState(s)) + tuple(a)

        print self.unknownStates
        self.flags.update_counter = 0
        s = self.flags.tmp_generative_state
        a = np.array(self.flags.tmp_generative_action)

        return s, a
#-----------------------------------------------------------------------------
# Run the Multi-Fidelity Rmax algorithm
# real_world_samples: continue running rmax at top level until you
#                    have sampled the real world for at least
#                    real_world_samples times
#-----------------------------------------------------------------------------
    def runMFRL(self, real_world_samples=0):
        runR = 0
        mfrl_converged = False
        if self.unidirectional:
            self.mDown = [np.Inf, np.Inf, np.Inf]

        while not rospy.is_shutdown() and self.flags.i < 50000:  # max samples allowed

            ### Preliminary ###
            # check if this if the top level (the real world)
            top_level = False
            if self.d == self.numD - 1:
                top_level = True

            # define helper variables to ease code readability
            rm = self.rmax[self.d]
            if self.d > 0:
                rm_m1 = self.rmax[self.d - 1]
            if not top_level:
                rm_p1 = self.rmax[self.d + 1]

            if np.mod(self.flags.i, 100) == 0:
                rospy.loginfo("Iteration: " + str(self.flags.i) + "\n\n")

            # call value iteration every so often just in case
            if np.mod(self.flags.i, 50) == 0:
                self.flags.run_val_iteration = True
            ### End Preliminary ###

            # TODO: temp--fix
            if self.bandit:
                i = self.rmax[self.d].valIter()
                print "ran " + str(i + 1) + " value iterations"

            # get next action
            a = rm.choose_action(self.cont2discState(self.s))

            # potentially override state and action choices if generative access is allowed
            if self.flags.doing_generative_eval and self.flags.reset_state:
                self.s, a = self.processGenerativeAccess()

            # get state to initial state if working with real robot
            if self.flags.reset_state:
                self.flags.reset_state = False
                self.flags.state_was_reset = True
                self.resetState(a)

            # run value iteration
            if self.flags.run_val_iteration:
                self.flags.run_val_iteration = self.shouldRunValIteration()
                # TODO: fix overwriting reset here
                # if not self.flags.run_val_iteration and not self.flags.doing_generative_eval: # value iteration was successfully run
                    # we now have new info, reselect the best action

                    # get next action
                    # a = rm.choose_action(self.cont2discState(self.s))

                    # If we are resetting, overwrite the old reset command in case a is different
                    # if self.flags.state_was_reset:
                        # self.resetState(a)
            self.flags.state_was_reset = False

            # get state-action tuple
            sa = tuple(self.cont2discState(self.s)) + tuple(a)

            # check if this is a known state/action or not
            if (rm.stateActionKnown(sa)) or (sa[0:rm.numS] in rm.terminal_states):
                self.flags.update_counter += 1  # increment counter if this is a known state (or goal state)
            else:
                self.flags.update_counter = 0  # reset counter if state is unknown

            # check going down conditions
            # first, make sure you have seen unknown states at least mDown times
            if self.d > 0 and not rm_m1.stateActionKnown(sa) and not rm.stateActionKnown(sa) and not tuple(self.cont2discState(self.s)) in rm.terminal_states:
                self.flags.num_unknown_states += 1
                print "Number of unknown states visited: " + str(self.flags.num_unknown_states)
                if self.generative_access:
                    self.unknownStates[self.d].add((self.s, tuple(a)))

            # if going down conditions satisfied, go down
            if self.flags.num_unknown_states >= self.mDown[self.d] and self.setRhatThat():
                self.flags.tmp_generative_state = self.initState()
                if self.generative_access:                
                    try:
                        self.flags.tmp_generative_state, self.flags.tmp_generative_action = self.unknownStates[self.d].pop()
                    except:
                        # This should happen when not doing generative states/actions
                        self.flags.tmp_generative_state = self.initState()
                        self.flags.tmp_generative_action = tuple(self.rmax[self.d-1].choose_action(self.cont2discState(self.flags.tmp_generative_state)))
                    self.flags.doing_generative_eval = True
                self.moveDownLevel()

            # if going up conditions satisfied, go up
            elif self.flags.update_counter >= self.numConvStates[self.d]:  # and not self.flags.doing_generative_eval:
                # rospy.logwarn("\n\nLevel %d has converged\n\n", self.d)

                if top_level:
                    if not mfrl_converged:
                        rospy.loginfo("\n\nFinished learning!\n\n")
                        self.saveLog(0)
                    mfrl_converged = True

                    if self.flags.top_sim_cnt >= real_world_samples:
                        break
                    else:
                        self.flags.update_counter = 0
                        self.mDown = [np.Inf, np.Inf, np.Inf]  # ensure that you
                        # won't move down now that you have converged -- used
                        # just for evaluation purposes so you can ensure every
                        # trial on every core ran the same number of real
                        # world samples
                else:
                    # save pertinent data
                    self.saveLog(1)
                    self.moveUpLevel()

            # else, take a step
            else:

                ns, r, t = self.getNextState(self.s, a, self.d)
                self.flags.i += 1
                runR += r

                # save Q for top sim level
                if top_level:
                    if np.mod(self.flags.top_sim_cnt, self.saveQRate) == 0:
                        self.bestQ.append(copy.copy(rm.Q))
                        self.bestQ_samples.append(self.flags.top_sim_cnt)

                    self.flags.top_sim_cnt += 1


                sa = tuple(self.cont2discState(self.s)) + tuple(a)
                s_prime = self.cont2discState(ns)

                # update reward and transition functions
                now_known = False
                if top_level or not rm_p1.rewardKnown(sa):
                    rm.updateReward(sa, r)
                    if rm.rewardKnownExact(sa):
                        now_known = True
                if top_level or not rm_p1.transitionKnown(sa):
                    rm.updateTransition(sa, s_prime)
                    if rm.transitionKnownExact(sa):
                        now_known = True

                # if reward or transition went from unknown to known,
                # run value iteration
                if now_known:
                    self.plotUnknownStates(self.s, a)
                    self.flags.run_val_iteration = True
                    self.flags.update_counter = 0
                else:
                    pass

                if t:
                    self.s = self.initState()
                    self.flags.reset_state = True
                    self.flags.run_val_iteration = True
                    rospy.loginfo("Reward: " + str(runR))
                    runR = 0
                else:
                    self.s = ns



        self.moveLevelPlot()
        # save the last Q values if something has happened since you last saved them
        if len(self.bestQ_samples) > 0 and self.flags.top_sim_cnt > (self.bestQ_samples[-1] + 5):
            self.bestQ.append(copy.copy(rm.Q))
            self.bestQ_samples.append(self.flags.top_sim_cnt)
        self.saveLog(-2)
        self.logFile.close()

        self.closeCustom()

        if self.showDomain:
            raw_input("\n\nPress Enter to end program\n\n")

        return self.logFile.name

#-----------------------------------------------------------------------------
# Set Q (or U for the first time) at the level above you
#-----------------------------------------------------------------------------
    def setQ(self):

        # define helper variables to ease code readability
        rm = self.rmax[self.d]
        rm_p1 = self.rmax[self.d + 1]

        if rm_p1.nr.max() == 0:
            # initialize rmax[d+1] U to Q + beta
            rm_p1.Q = rm.Q + self.beta[self.d]
        else:
            # loop through states (at all levels)
            for sa_index, ntmp in np.ndenumerate(rm.nr):
                seenSA = False
                for i in np.arange(self.d + 1, self.numD):
                    if self.rmax[i].stateActionKnown(sa_index):
                        seenSA = True
                if not seenSA:
                    # set Q for this sa pair
                    rm_p1.Q[sa_index] = rm.Q[sa_index] + self.beta[self.d]
                # else
                    # don't set Q at the level above if false, we already have better information
#-----------------------------------------------------------------------------
# Transfer Rhat and That for use in the next level down
#-----------------------------------------------------------------------------
    def setRhatThat(self):

        significantChange = False
        # set Rhat and That at level d-1 when n >= m
        for sa_index, n_iter in np.ndenumerate(self.rmax[self.d].nr):

            # set R^
            if self.setTabularRhat(sa_index):
                significantChange = True

            # Pass T^
            if not self.useDBN:
                if self.setTabularThat(sa_index, n_iter):
                    significantChange = True

        if self.useDBN:
            if self.setDBNThat():
                significantChange = True

        return significantChange

#-----------------------------------------------------------------------------
# Transfer tabular R^ to levels below
#-----------------------------------------------------------------------------
    def setTabularRhat(self, sa_index):

        # define helper variables to ease code readability
        rm = self.rmax[self.d]
        rm_m1 = self.rmax[self.d - 1]

        if rm.rewardKnown(sa_index):
            # set R^ at d-1 to R^ at d
            diff = np.abs(rm_m1.Rhat[sa_index] - rm.Rhat[sa_index])
            rm_m1.Rhat[sa_index] = rm.Rhat[sa_index]

            # set n, and r at d-1
            rm_m1.nr[sa_index] = rm.nr[sa_index]
            rm_m1.r[sa_index] = rm.r[sa_index]

            if diff > 0.1:
                return True

        return False

#-----------------------------------------------------------------------------
# Transfer dynamic bayes net transition probability tables to levels below
#-----------------------------------------------------------------------------
    def setDBNThat(self):

        should_move = False
        for i, f in enumerate(self.rmax[self.d].factors):
            for parent_index, ntmp in np.ndenumerate(f.pr[0]):
                ind = (Ellipsis,) + parent_index
                column_sum = sum(f.pr[ind])
                if column_sum >= f.m:
                    if utils.probVariance(self.rmax[self.d - 1].factors[i].pr[ind],
                                          f.pr[ind]) > 0.1:
                        should_move = True
                    self.rmax[self.d - 1].factors[i].pr[ind] = f.pr[ind]
                    self.rmax[self.d - 1].factors[i].pr_col_sum[parent_index] = f.pr_col_sum[parent_index]

        return should_move


#-----------------------------------------------------------------------------
# Transfer tabular T^ to levels below
#-----------------------------------------------------------------------------
    def setTabularThat(self, sa_index, n_iter):

        # define helper variables to ease code readability
        rm = self.rmax[self.d]
        rm_m1 = self.rmax[self.d - 1]

        if rm.transitionKnown(sa_index):
            s_prime_list = rm.sa2s_prime[sa_index]
            y_d = np.zeros(len(s_prime_list))  # transition probabilities at level d
            y_dm1 = np.zeros(len(s_prime_list))  # transition probabilities at level d minus 1
            for i, s_prime_iter in enumerate(s_prime_list):
                sas_prime = sa_index + s_prime_iter
                y_d[i] = rm.That[sas_prime]
                if sas_prime in rm_m1.That:
                    y_dm1[i] = rm_m1.That[sas_prime]

                # copy transition probabilities
                rm_m1.That[sas_prime] = rm.That[sas_prime]


            # update s,a->s' and counts
            rm_m1.sa2s_prime[sa_index] = copy.copy(rm.sa2s_prime[sa_index])
            rm_m1.nt[sa_index] = copy.copy(rm.nt[sa_index])
            # make sure the sa_index will be known at this level (ok, since That will not be updated at rm_m1 again)
            if rm_m1.nt[sa_index] < rm_m1.mt:
                rm_m1.nt[sa_index] = rm_m1.mt

            # check if transition probabilities were significantly different
            if utils.probVariance(y_d, y_dm1) > 0.1:
                return True

        return False

#-----------------------------------------------------------------------------
# Save r, n, and sa2s_prime for use in the next simulator down
# move_dir: 1 if we are moving up, -1 if we are moving down, 0 if we finished,
#             -2 if we should just ignore this entry
#-----------------------------------------------------------------------------
    def saveLog(self, move_dir):
        # save pertinent data
        # number of iterations ran at this level of the sim
        if self.flags.i - self.flags.current_level_iter > 0:
            self.level_iterations[self.d].append(self.flags.i - self.flags.current_level_iter)
    #         if self.level_iterations[self.d][-1] == 1:
    #             pass
            self.level_directions[self.d].append(move_dir)

            self.flags.current_level_iter = copy.copy(self.flags.i)
            rospy.loginfo("\n\nLevelIterations: " + str(self.level_iterations))

            self.logFile.seek(0)
            log = {"level_iterations":self.level_iterations,
                   "level_directions":self.level_directions,
                   "bestQ":self.bestQ,
                   "bestQ_samples":self.bestQ_samples,
                   "reward":self.flags.top_level_reward}
            pickle.dump(log, self.logFile)

#-----------------------------------------------------------------------------
# Custom signal handler so we can get out of here while saving pertinent data
#-----------------------------------------------------------------------------
    def signal_handler(self, signal, frame):
        self.saveLog(0)
        rospy.signal_shutdown('Ctrl+C shutdown requested')
        sys.exit(0)


#==============================================================================
# MFRL flags
#==============================================================================
class MFRLFlags:
    def __init__(self):
        self.update_counter = 0
        self.num_unknown_states = 0
        self.current_level_iter = 0
        self.reset_state = True
        self.state_was_reset = False
        self.run_val_iteration = False
        self.use_start_state = False
        self.top_sim_cnt = 0
        self.i = 0
        self.top_level_reward = [[]]
        self.tmp_generative_state = 0
        self.tmp_generative_action = 0
        self.doing_generative_eval = False
        self.vars_copied = False
