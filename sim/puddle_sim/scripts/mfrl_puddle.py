#!/usr/bin/env python
'''
Description: Reinforcement Learning using Multi-Fidelity Simulators

See ICRA 2014 paper by the above title for algorithm details

This class won't run on it's own.  It several functions need to be implemented
in a derived class.

Created on Nov 27, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
from __future__ import division
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import sets
import matplotlib.pyplot as plt
import cPickle as pickle
import copy
import signal
import sys


#from aclpy import mfrl
import mfrl

import PuddleWorld as pw
import PuddleWorldGrid as pwg
from puddle_sim.msg import State


class MFRLPuddle(mfrl.MFRL):

    def __init__(self, name=None,
                unidirectional=False, generative_access=False, useDBN=True,
                levels=[1,2,3], bandit=False, grid_world=False, showDomain=False,
                beta=[0.0, 0.0], mKnown=[75, 75, 75], mUnknown=[20, 20],
                logFileIndex=0, logFile0=None, logFile1=None, logFile2=None):

        # seed random number generator
        # self.rand_state = np.random.RandomState(897)
        self.rand_state = None

        #levels = [3,2]

        # set up puddle world
        if grid_world:
            puddle_noise = np.array([0.0, 0.1 / 3, 0.2 / 3])
        else:
            puddle_noise = np.array([0.0, 0.01, 0.02])  # 0.0, 0.01])
        self.pw = []

        #### TODO: should loop over the levels variable here ####

        for i in levels:
            if grid_world:
                self.pw.append(pwg.PuddleWorldGrid(puddle_noise[i-1], i-1, self.rand_state))
            else:
                self.pw.append(pw.PuddleWorld(puddle_noise[i-1], i-1, self.rand_state))

        self.reward_fig = None
        self.q_fig = None
        plt.ion()

        mr = [1, 1, 1]
        if grid_world:
            mt = [1, 3, 4]
        else:
            mt = [2, 3, 4] #[3, 4, 5]

        # variables specific to puddle world
        self.package_address = roslib.packages.get_pkg_dir('puddle_sim')

        # call MFRL constructor
        mfrl.MFRL.__init__(self, name, unidirectional, generative_access, useDBN,
                           levels, bandit, mr, mt, logFileIndex,
                           logFile0, logFile1, logFile2)


        # these values will probably be overridden in derived classes
        self.numConvStates = mKnown
        self.mDown = [np.Inf] + mUnknown
        self.beta = beta
        self.saveQRate = 100  # save Q at top level every this many environment interactions
        self.showDomain = showDomain

        # plot unknown states
        gap = 0.05
        if self.showDomain:
            self.unknown_states = []
            for i in np.arange(self.numD):
                newDict = {}
                self.pw[i].showDomain(np.array([0, 0]))
                ts = self.getTerminalStates(i)
                for j in np.arange(self.numX):
                    for k in np.arange(self.numY):
                        s = (j, k)
                        if s not in ts:
                            for a in np.arange(self.numA):
                                sa = (j, k, a)
                                if a == 0:
                                    cs = np.array([(j + 0.5) / self.numX, (j + 1 - gap) / self.numX,
                                                   (k + 0.5) / self.numY, (k + 1 - gap) / self.numY])
                                elif a == 1:
                                    cs = np.array([(j + 0.5) / self.numX, (j + 1 - gap) / self.numX,
                                                   (k + gap) / self.numY, (k + 0.5) / self.numY])
                                elif a == 2:
                                    cs = np.array([(j + gap) / self.numX, (j + 0.5) / self.numX,
                                                   (k + 0.5) / self.numY, (k + 1 - gap) / self.numY])
                                elif a == 3:
                                    cs = np.array([(j + gap) / self.numX, (j + 0.5) / self.numX,
                                                   (k + gap) / self.numY, (k + 0.5) / self.numY])
                                newDict[sa] = self.pw[i].plotUnknownState(cs)
                self.unknown_states.append(newDict)
                self.pw[i].updatePlot()

###########
# Functions to be implemented in derived classes
###########
#-----------------------------------------------------------------------------
# Initialize state-based state-action space
#-----------------------------------------------------------------------------
    def initStateBased(self, savedLog, levels=[1,2,3]):
        # State and action space discretization
        self.numX = 10
        self.numY = 10
        self.numA = 4
        # self.init_state = np.array([0., 0.])
        logFile = self.package_address + '/log/'

        # create the discretized states and actions
        self.xbins = np.linspace(0, 1, self.numX, False)
        self.ybins = np.linspace(0, 1, self.numY, False)
        self.xbins = self.xbins[1:]  # has to do with how np.digitize assigns bins
        self.ybins = self.ybins[1:]

        self.s = self.initState()  # self.init_state

        for i in np.arange(self.numD):
            if savedLog[i] != None:
                savedLog[i] = logFile + savedLog[i] + '.npz'

        # initialize RMax algorithm for each simulation level
        S = [np.arange(self.numX), np.arange(self.numY)]
        A = [np.arange(self.numA)]
        max_reward = 0.0
        U = max_reward * np.ones((self.numX, self.numY, self.numA))

        if self.useDBN:
            s_prime2sa = np.array([[1, 0, 1],
                                   [0, 1, 1]])
            self.rm = self.initRmax(S, A, U, savedLog, logFile, s_prime2sa)
        else:
            self.rm = self.initRmax(S, A, U, savedLog, logFile, levels=levels)


#-----------------------------------------------------------------------------
# Initialize bandit state-action space -- NA to puddle-world
#-----------------------------------------------------------------------------
    def initBandit(self, savedLog):
        pass


#-----------------------------------------------------------------------------
# Set initial state
#-----------------------------------------------------------------------------
    def initState(self):

        self.flags.reset_state = True

        s = State(0.05, 0.05)
        # s.x += np.random.randn() * 0.03
        # s.y += np.random.randn() * 0.03
        if s.x < 0.0:
            s.x = 0.0
        if s.y < 0.0:
            s.y = 0.0
        return s

#-----------------------------------------------------------------------------
# Check if it is ok to reun value iteration right now
# Should either run value iteration and return False, or return True
#-----------------------------------------------------------------------------
    def shouldRunValIteration(self):
        i = self.rmax[self.d].valIter()
        if i > 20:
            rospy.loginfo("ran " + str(i + 1) + " value iterations\n")
        # print "Num known states visited in a row: " + str(self.flags.update_counter)
        return False

#-----------------------------------------------------------------------------
# Reset the state
#-----------------------------------------------------------------------------
    def resetState(self, a):
        pass


#-----------------------------------------------------------------------------
# Call any custom plotting or other functions when moving a level
#-----------------------------------------------------------------------------
    def moveLevelPlot(self):
        if self.showDomain:
            self.showEstReward()
            self.showQ()

#-----------------------------------------------------------------------------
# UnknownStatesPlot -- plot all states when they become known
#-----------------------------------------------------------------------------
    def plotUnknownStates(self, s, a):
        pass
        # if self.showDomain == True:
        #     # temporarily plot unknown and known states
        #     s = tuple(self.cont2discState(s))
        #     sa = s + tuple(a,)
        #     # if self.rmax[self.d].rewardKnown(sa) and s in self.unknown_states[self.d]:
        #     if sa in self.unknown_states[self.d]:
        #         if self.rmax[self.d].stateActionKnown(sa):
        #             self.unknown_states[self.d][sa].remove()  # remove imshow plot
        #             del self.unknown_states[self.d][sa]  # remove element from unknown_states
        #             # self.pw[self.d + self.dStart].updatePlot()

#-----------------------------------------------------------------------------
# Run a single step
#-----------------------------------------------------------------------------
    def getNextState(self, s, a, d, showDomain=None):
        if showDomain == None:
            showDomain = self.showDomain
        sp, r, term = self.pw[d].puddleStep(np.array([s.x, s.y]), a, showDomain)
        # for creating the PW video, we need snapshots from all the levels, not
        # just the current level
#        for i in range(self.numD):
#            if d == i:
#                sp, r, term = self.pw[i].puddleStep(np.array([s.x, s.y]), a, showDomain)
#            else:
#                s_init = self.initState()
#                self.pw[i].puddleStep(np.array([s_init.x, s_init.y]), -1, showDomain)
        return State(sp[0], sp[1]), r, term

#-----------------------------------------------------------------------------
# Set a set of terminal states
#-----------------------------------------------------------------------------
    def getTerminalStates(self, d):
        ts = sets.Set([])

        # construct set of terminal states
        for i in np.arange(self.numX):
            for j in np.arange(self.numY):
                s = self.disc2contState(np.array([i, j]))
                sp, r, t = self.getNextState(s, -1, d, False)
                if t:
                    ts.add((i, j))

        return ts

#-----------------------------------------------------------------------------
# converts 2d continuous states to discrete
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def cont2discState(self, cs):
#         print "xbins"
#         print self.xbins
#         print cs
        x = np.digitize(np.array([cs.x]), self.xbins)
        y = np.digitize(np.array([cs.y]), self.ybins)
        return np.array([x[0], y[0]])


##############
# New functions only for puddle-world
##############

#-----------------------------------------------------------------------------
# Evaluate policy Q m times to get an estimate of how many runs it takes to get to the goal
#-----------------------------------------------------------------------------

    def evalQ(self, logFileIndex, bestQ=None):

        package_address = roslib.packages.get_pkg_dir('puddle_sim')
#         self.logFileAddress = package_address + '/log/mfrl_' + str(logFileIndex) + '.p'
#         fout = open(policyEvalAddress, 'wb')

        if bestQ == None:
            f = open(self.logFileAddress, 'rb')
            p = pickle.load(f)
            bestQ = p["bestQ"]

        numTrials = 60
        numQ = len(bestQ)
        maxSteps = 600

        reward = np.zeros((numQ, numTrials, 2))
        rospy.loginfo("There are " + str(numQ) + " sets of Q values")
        for i in range(numQ):
            # get Q values
            rospy.loginfo("Starting Q value " + str(i))
            Q = bestQ[i]
            self.rm.Q = copy.copy(bestQ[i])
#            print "newQ"
#            print reward
            for j in range(numTrials):
                self.s = self.initState()  # self.init_state
                reward_sum = 0
                for k in range(maxSteps):
                    a = self.rm.choose_action(self.cont2discState(self.s))
                    ns, r, t = self.getNextState(self.s, a, self.numD - 1, False)
                    reward_sum += r

                    self.s = ns
                    if tuple(self.cont2discState(self.s)) in self.ts:
                        break
                reward[i, j, 0] = reward_sum
                reward[i, j, 1] = k
                rospy.loginfo("Trial " + str(j) + ": " + str(k) + " steps, " + str(reward_sum) + " reward")

#         pickle.dump(reward, fout)
#         fout.close()
        return reward

#-----------------------------------------------------------------------------
# converts 2d discrete states to continuous (using state centers)
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def disc2contState(self, ds):
        x = float(ds[0]) / self.numX + 1. / self.numX / 2.
        y = float(ds[1]) / self.numY + 1. / self.numY / 2.
        return State(x, y)

#-----------------------------------------------------------------------------
# r should be a numpy 2d array containing the estimated reward values
#-----------------------------------------------------------------------------
    def showEstReward(self):
        # Draw the estimated reward function
        ravg = self.rmax[self.d].r / self.rmax[self.d].nr
        ravg = np.nan_to_num(ravg)
        r = np.amin(ravg, axis=2)
        self.reward_fig = plt.figure("Estimated Reward")
        self.est_reward_im = plt.imshow(r.transpose(), extent=(0, 1, 0, 1),
                                    origin="lower")
        self.est_reward_im.set_data(r.transpose())
        plt.draw()

#-----------------------------------------------------------------------------
# r should be a numpy 2d array containing the estimated reward values
#-----------------------------------------------------------------------------
    def showQ(self):
        # Draw the estimated reward function
        q = np.amax(self.rmax[self.d].Q, axis=2)
        self.q_fig = plt.figure("Q")
        self.q_im = plt.imshow(q.transpose(), extent=(0, 1, 0, 1),
                                    origin="lower")
        self.q_im.set_data(q.transpose())
        plt.draw()


if __name__ == "__main__":
    try:
        rospy.init_node('mfrl_puddle', anonymous=True)
        argv = rospy.myargv(argv=sys.argv)
        if len(argv) == 2:
            if argv[1] in ['False', 'false', '0', 'no', 'not']:
                c = MFRLPuddle(showDomain=False)
            else:
                c = MFRLPuddle(showDomain=True)
        else:
            c = MFRLPuddle()
#        raw_input()
        c.runMFRL()
#         c.evalQ(12)

    except rospy.ROSInterruptException:
        pass
