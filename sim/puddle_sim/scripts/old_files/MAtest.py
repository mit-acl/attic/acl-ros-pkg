#!/usr/bin/env python
'''
Description: Run the rmax_puddle algorithm multiple times for an evaluation phase.

Created on Sept 12, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayDimension
from operator import mul

#-----------------------------------------------------------------------------
# multiarray to numpy array
#-----------------------------------------------------------------------------
def multiArray2NumpyArray(ma):
    numDim = len(ma.layout.dim)
    s = np.zeros(numDim)
    for i in np.arange(numDim):
        s[i] = ma.layout.dim[i].size
    na = np.reshape(ma.data, tuple(s))
    return na

#-----------------------------------------------------------------------------
# numpy array to multiarray
# TODO: not sure if the stride calculations are correct, although they aren't used
# in the multiArray2NumpyArray function so it might not matter
#-----------------------------------------------------------------------------
def numpyArray2MultiArray(na, ma):
    s = na.shape
    for i in np.arange(len(s)):
        d = MultiArrayDimension()
        d.size = s[i]
        d.stride = reduce(mul, s[i:len(s)])  # = s[0]*s[1]*s[2]*...
        ma.layout.dim.append(d)
    ma.data = np.reshape(na, -1)

x = np.zeros((2, 3, 4, 5, 6))
for i in range(len(x)):
    x[i] = i + 3

x[0, 1, 2] = 9
x[1, 0, 3] = 5

print 'x:'
print x

m = Float64MultiArray()

numpyArray2MultiArray(x, m)


x2 = multiArray2NumpyArray(m)

print 'x2'
print x2
print np.abs(x - x2).max()



