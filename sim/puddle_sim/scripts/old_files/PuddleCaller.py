#!/usr/bin/env python
import roslib; roslib.load_manifest('puddle_sim')

import sys

import rospy
from puddle_sim.srv import *

def PuddleCaller(s1, s2, a):
    rospy.wait_for_service('puddle_step')
    try:
        puddle_step = rospy.ServiceProxy('puddle_step', PuddleStep)
        resp1 = puddle_step(s1, s2, a)
        return resp1.r
    except rospy.ServiceException, e:
        print "Service call failed: %s" % e


if __name__ == "__main__":
    if len(sys.argv) == 4:
        s1 = float(sys.argv[1])
        s2 = float(sys.argv[2])
        a = float(sys.argv[3])
        print "%s " % (PuddleCaller(s1, s2, a))
    else:
        print("mistake")
        sys.exit(1)
