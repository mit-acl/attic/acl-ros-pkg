#!/usr/bin/env python

from puddle_sim.srv import *
import rospy
import numpy as np
import matplotlib.pyplot as plt

class PuddleWorldNone(object):


    def __init__(self):
        self.reward_map = np.zeros((100, 100))
        self.val_map = np.zeros((100, 100))
        self.pi_map = np.zeros((100, 100))
        a = np.zeros((2))
        for i, x in enumerate(np.linspace(0, 1, 100)):
            for j, y in enumerate(np.linspace(0, 1, 100)):
                a[0] = x
                a[1] = y
                self.reward_map[j, i] = self.reward(a)
        self.actions = 0.05 * np.array([[1, 0], [0, 1], [-1, 0], [0, -1]], dtype="float")
        self.actions_num = 4
        self.noise_level = 0.0;
        self.domain_fig = None;
        plt.ion()
#         self.showDomain(np.array([0, 0]))



    def handle_puddleStep(self, req):
        [s, r, t] = self.step(np.array([req.x, req.y]), req.a)
        if req.showDomain:
            self.showDomain(s);
        return PuddleStepResponse(s[0], s[1], r, t)

    def puddle_step_server_start(self):
        rospy.init_node('puddle_step_server')
        s = rospy.Service('puddle_step', PuddleStep, self.handle_puddleStep)
        rospy.spin()

    def isTerminal(self, s):
        return s.sum() > 0.85 * 2



    def step(self, s, a):
        if a < 0:  # if you send a negative action, just get the reward for
                    # where you currently are -- helpful for getting terminal
                    # states
            ns = s
        else:
            a = self.actions[a]
            ns = s + a + np.random.random() * self.noise_level
            # make sure we stay inside the [0,1]^2 region
            ns = np.minimum(ns, 1.)
            ns = np.maximum(ns, 0.)
        return (ns, self.reward(ns), self.isTerminal(ns))

    def reward(self, s):
        if self.isTerminal(s):
            return 0  # goal state reached
        return -1

    def showDomain(self, s, a=None):
        # Draw the environment
        if self.domain_fig is None:
            self.domain_fig = plt.figure("Domain")
            self.reward_im = plt.imshow(self.reward_map, extent=(0, 1, 0, 1),
                                        origin="lower")
            self.state_mark = plt.plot(s[0], s[1], 'kd', markersize=20)
            plt.draw()
        else:
            self.state_mark[0].set_data([s[0]], [s[1]])
            plt.draw()

if __name__ == "__main__":
    pw = PuddleWorldNone();
    pw.puddle_step_server_start()
