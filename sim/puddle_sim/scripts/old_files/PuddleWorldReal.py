#!/usr/bin/env python

from puddle_sim.srv import *
import rospy
import numpy as np
import sys
from PuddleWorldSmall import PuddleWorldSmall

class PuddleWorldReal(PuddleWorldSmall):


    def __init__(self):
        super(PuddleWorldReal, self).__init__()
        self.noise_level = .01;

    def reward(self, s):
        if self.isTerminal(s):
            return 0  # goal state reached
        reward = -1
        # compute puddle influence
        d = self.puddles[:, 1, :] - self.puddles[:, 0, :]
        denom = (d ** 2).sum(axis=1)
        g = ((s - self.puddles[:, 0, :]) * d).sum(axis=1) / denom
        g = np.minimum(g, 1)
        g = np.maximum(g, 0)
        dists = np.sqrt(((self.puddles[:, 0, :] + g * d - s) ** 2).sum(axis=1))
        dists = dists[dists < 0.1]
        if len(dists):
            reward -= 400 * (0.1 - dists[dists < 0.1]).max()
        return reward


if __name__ == "__main__":
    pw = PuddleWorldReal();
    pw.puddle_step_server_start()
