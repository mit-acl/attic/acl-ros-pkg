#!/usr/bin/env python

from puddle_sim.srv import *
import rospy
import numpy as np
import sys
from PuddleWorldNone import PuddleWorldNone

class PuddleWorldSmall(PuddleWorldNone):

    def __init__(self):
        self.puddles = np.array([[[0.2, .75], [.75, .75]], [[.75, .2], [.75, .75]]])
        self.plow1 = np.array([.2, 0.0])
        self.plow2 = np.array([1.0, 0.8])
        self.phi1 = np.array([0.0, 0.2])
        self.phi2 = np.array([0.8, 1.0])
        super(PuddleWorldSmall, self).__init__()
        self.noise_level = 0;  # still deterministic

    def reward(self, s):
        if self.isTerminal(s):
            return 0  # goal state reached
        reward = -1
        # compute puddle influence, but only in the overlapping part
        d = self.puddles[:, 1, :] - self.puddles[:, 0, :]
        denom = (d ** 2).sum(axis=1)
        g = ((s - self.puddles[:, 0, :]) * d).sum(axis=1) / denom
        g = np.minimum(g, 1)
        g = np.maximum(g, 0)
        dists = np.sqrt(((self.puddles[:, 0, :] + g * d - s) ** 2).sum(axis=1))
        dists = dists[dists < 0.1]
        if len(dists):
            reward -= 400 * (0.1 - dists[dists < 0.1]).max()
        if self.isLeft(s) and self.isRight(s):
            reward = -1
        return reward

    # algorithm from http://stackoverflow.com/questions/3461453/determine-which-side-of-a-line-a-point-lies
    def isLeft(self, s):
        x1 = self.plow1[0]
        x2 = self.plow2[0]
        y1 = self.plow1[1]
        y2 = self.plow2[1]
        return ((x2 - x1) * (s[1] - y1) - (y2 - y1) * (s[0] - x1)) > 0.0

    # algorithm from http://stackoverflow.com/questions/3461453/determine-which-side-of-a-line-a-point-lies
    def isRight(self, s):
        x1 = self.phi1[0]
        x2 = self.phi2[0]
        y1 = self.phi1[1]
        y2 = self.phi2[1]
        return ((x2 - x1) * (s[1] - y1) - (y2 - y1) * (s[0] - x1)) < 0.0

if __name__ == "__main__":
    pw = PuddleWorldSmall();
    pw.puddle_step_server_start()
