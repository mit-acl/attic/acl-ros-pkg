#!/usr/bin/env python
'''
Description: Implements the rmax algorithm from this paper:

Algorithm 1
Reinforcement Learning in Finite MDPs: PAC Analysis
http://research.microsoft.com/pubs/178881/strehl09a.pdf

Created on Aug 26, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import numpy as np
import copy

class Rmax:

    def __init__(self, S, A, U, terminal_states, logFile=None, savedLog=None,
                 maxDelta=0.001,
                 gamma=0.95, m=5,
                 s_prime2sa=None):

        # Initialize variables (lines 1 through 8)
        self.Q = copy.copy(U)
        self.r = np.zeros(U.shape)
        self.n = np.zeros(U.shape)
        self.Rhat = np.zeros(U.shape)
        self.gamma = copy.copy(gamma)
        self.m = copy.copy(m)
        self.terminal_states = copy.copy(terminal_states)  # set of terminal states, each
        # state is a tuple
        self.maxDelta = copy.copy(maxDelta)

        self.S = copy.copy(S)
        self.A = copy.copy(A)
        self.numS = len(S)
        self.numA = len(A)
        # Use dictionary for n_prime and That because they will be so sparse
        self.n_prime = {}
        self.That = {}
        # Use dictionary for sa -> s' mapping
        self.sa2s_prime = {}

        # set n in terminal states to m
        self.setTerminalStatesn()

        # init dbn factors
        self.s_prime2sa = copy.copy(s_prime2sa)
        if self.s_prime2sa != None:
            self.factors = []
            self.actions = []
            for i in np.arange(self.numS):
                f = Factor(self.m)
                f.numDisc = len(self.S[i])
                self.factors.append(f)
            for i in np.arange(self.numA):
                f = Factor(self.m)
                f.numDisc = len(self.A[i])
                self.actions.append(f)

            self.initDBN()



        # if applicable, load log file
        if savedLog != None:
            self.loadData(savedLog)

        # set up log file for saving pertinent data
        if logFile == None:
            self.logFile = None
        else:
            self.logFile = open(logFile, 'w')

#-----------------------------------------------------------------------------
# Set terminal states' n value to m when initializing algorithm
    def setTerminalStatesn(self):
        for sa_index, n in np.ndenumerate(self.n):
            if sa_index[0:self.numS] in self.terminal_states:
                self.n[sa_index] = self.m

#-----------------------------------------------------------------------------
# Given the Q values and a state s, return the action with the best reward.
#    If there are multiple actions with the same reward, randomly return one
#    of them.
#-----------------------------------------------------------------------------
    def choose_action(self, s):

        # get Q', the possible state action pairs from which to choose
        Qprime = self.Q[tuple(s)]

        # Randomly select among the best actions
        a_index = np.where(Qprime == Qprime.max())  # all possible best actions

        a = np.zeros(self.numA, dtype=np.int)  # somewhere to store best action
        rand_ind = np.random.randint(a_index[0].size)
        for i in np.arange(self.numA):
            a[i] = a_index[i][rand_ind]
        return a

#-----------------------------------------------------------------------------
# Update the rmax state, reward and count variables based on action taken
#-----------------------------------------------------------------------------
    def update(self, a, r, s, s_prime):

        # check for terminal states
        if tuple(s) in self.terminal_states:
            # should call reset function here
            return -1

        # create sa and sas' tuples for indexing purposes
        sa = tuple(s) + tuple(a)

        self.n[sa] += 1  # update count of that state action pair
        self.r[sa] += r  # record immediate reward

        # update rhat
        self.Rhat[sa] = float(self.r[sa]) / self.n[sa]

        # update generalized That
        if self.s_prime2sa != None:
            for i, f in enumerate(self.factors):
                f.update(s, a, s_prime[i])
        else:

            sas = sa + tuple(s_prime)

            # add to the sa -> s' dictionary.  If an element already exists,
            # append the new element to an array
            if sa in self.sa2s_prime:
                s_prime_list = self.sa2s_prime[sa]
                if tuple(s_prime) not in s_prime_list:
                    self.sa2s_prime[sa].append(tuple(s_prime))
            else:
                self.sa2s_prime[sa] = [tuple(s_prime)]

            # update immediate next-state (using dict)
            if sas in self.n_prime:
                self.n_prime[sas] += 1
            else:
                self.n_prime[sas] = 1

            # update That
            if sa in self.sa2s_prime:
                s_prime_list = self.sa2s_prime[sa]
                for s_prime_iter in s_prime_list:
                    sas_prime = sa + s_prime_iter
                    # update That
                    self.That[sas_prime] = float(self.n_prime[sas_prime]) / self.n[sa]



        # should run value iteration (val iter should be explicitly called
        # when this function returns 1
        if self.n[sa] == self.m:
            return 1

        # no value iteration ran
        return 0

#-----------------------------------------------------------------------------
# Run value iteration
# Main computation loop: should be a separate thread or action
    def valIter(self):

        if self.s_prime2sa != None:

            sa2sp = {}
            for sa_index, n in np.ndenumerate(self.n):
                nextStates = self.getReachableStates(sa_index)
                if len(nextStates) > 0:
                    sa2sp[sa_index] = nextStates

        maxDelta = 0
        for i in np.arange(5000):

            if self.s_prime2sa == None:
                for sa_index, n in np.ndenumerate(self.n):
                    if sa_index[0:self.numS] in self.terminal_states:
                        self.Q[sa_index] = 0  # episodic version -- no reward in goal

                    elif n >= self.m:
                        # calc expectation of Q
                        expectQ = 0
                        # loop over dictionary of sa -> s'
                        if sa_index in self.sa2s_prime:
                            s_prime_list = self.sa2s_prime[sa_index]
                            for s_prime_iter in s_prime_list:
                                sas_prime = sa_index + s_prime_iter
                                expectQ += self.That[sas_prime] * self.Q[tuple(s_prime_iter)].max()

                        # update Q (line 21 of alg. in paper)
                        tmp1 = self.Rhat[sa_index] + self.gamma * expectQ
                        delta = np.abs(self.Q[sa_index] - tmp1)
                        if delta > maxDelta:
                            maxDelta = copy.copy(delta)
                        self.Q[sa_index] = copy.copy(tmp1)

            else:  # generalized T^ learning

                for sa_index, ns_list in sa2sp.iteritems():
                    if sa_index[0:self.numS] in self.terminal_states:
                        self.Q[sa_index] = 0  # episodic version -- no reward in goal
                    else:
                        # calc expectation of Q
                        expectQ = 0
                        # loop over s'
                        for ns in ns_list:
                            expectQ += ns.pr * self.Q[ns.s].max()

                        # update Q (line 21 of alg. in paper)
                        tmp1 = self.Rhat[sa_index] + self.gamma * expectQ
                        delta = np.abs(self.Q[sa_index] - tmp1)
                        if delta > maxDelta:
                            maxDelta = copy.copy(delta)
                        self.Q[sa_index] = copy.copy(tmp1)


            if maxDelta < self.maxDelta:  # stopping condition -- nothing is changing
                break
            maxDelta = 0

        if i > 0:
            print "ran " + str(i + 1) + " value iterations"
            self.saveData()
        return i


#-----------------------------------------------------------------------------
# Set up generalized transition dynamics based on a mapping of s_prime to s,a
#-----------------------------------------------------------------------------
    def initDBN(self):

        # loop over the factors
        for i, f in enumerate(self.factors):
            sa = np.flatnonzero(self.s_prime2sa[i, :])
            s = np.flatnonzero(self.s_prime2sa[i, :self.numS])
            a = np.flatnonzero(self.s_prime2sa[i, self.numS:])
            f.parents = sa
            pr_ind = [f.numDisc]
            for j in np.arange(len(s)):
                pr_ind.append(self.factors[s[j]].numDisc)
            for j in np.arange(len(a)):
                pr_ind.append(self.actions[a[j]].numDisc)
            f.pr = np.zeros(pr_ind)


#-----------------------------------------------------------------------------
# Get a list of reachable next states for each a SA pair based on the current
# transition model.
#-----------------------------------------------------------------------------
    def getReachableStates(self, sa):

        # TODO: check if calling self.factorsKnown first will speed things up

        nextStates = []
        for f in self.factors:
            nextStates2 = []
            fprobs = f.getProb(sa)
            for v in np.arange(f.numDisc):
                if fprobs[v] > 0.0:
                    if len(nextStates) > 0:
                        for i, ns in enumerate(nextStates):
                            tmp = NextState()
                            tmp.s = ns.s + (v,)
                            tmp.pr = ns.pr * fprobs[v]
                            nextStates2.append(tmp)
                    else:
                        tmp = NextState()
                        tmp.s = (v,)
                        tmp.pr = fprobs[v]
                        nextStates2.append(tmp)
                elif fprobs[v] == -1:
                    return []

            if len(nextStates2) > 0:
                nextStates = copy.copy(nextStates2)

        return nextStates

#-----------------------------------------------------------------------------
# Quick method for determining if all the factors are known for a specific s,a
#-----------------------------------------------------------------------------
    def factorsKnown(self, sa):
        if self.s_prime2sa == None:  # tabular case
            if self.n[sa] >= self.m:
                return True
            else:
                return False
        else:  # dbn case
            for f in self.factors:
                if not f.isKnown(sa)[0]:  # check if that factor is known
                    return False
            return True  # if all factors are known, return True

#-----------------------------------------------------------------------------
# Save pertinent data to file.  Data structs are saved as npz files
# Get's called every time Q is updated
#-----------------------------------------------------------------------------
    def saveData(self, eps_d=0):
        if self.logFile != None:
            self.logFile.seek(0)
            np.savez(self.logFile, Q=self.Q, n=self.n, r=self.r, Rhat=self.Rhat,
                     That=self.That, gamma=self.gamma, m=self.m,
                     n_prime=self.n_prime, sa2s_prime=self.sa2s_prime, eps_d=eps_d)

#-----------------------------------------------------------------------------
# Load pertinent data from a file
#-----------------------------------------------------------------------------
    def loadData(self, logFile):
        l = open(logFile, 'r')
        try:
            npzfile = np.load(l)
            self.Q = npzfile['Q']
            self.n = npzfile['n']
            self.r = npzfile['r']
            self.Rhat = npzfile['Rhat']
            self.That = npzfile['That']
            self.gamma = npzfile['gamma']
            self.m = npzfile['m']
            self.n_prime = npzfile['n_prime']
            self.sa2s_prime = npzfile['sa2s_prime']
            self.eps_d = npzfile['eps_d']

            # temporary hack until I start using pickle
            self.That = self.That.reshape(-1)[0]
            self.n_prime = self.n_prime.reshape(-1)[0]
            self.sa2s_prime = self.sa2s_prime.reshape(-1)[0]
        except Exception:
            print 'File ' + logFile + ' could not be opened'



#==============================================================================
# Simple class for holding information about a factor (parents, counts, name,
# etc.).
#==============================================================================
class Factor:
    def __init__(self, m):  # , name):
#         self.name = name  # factor name (string)
        self.numDisc = 0  # number of discretizations of this factor
        self.pr = []  # conditional probability table of factor counts given parents
        self.parents = []  # list of parents of this factor
        self.m = m  # number of times a state-action pair needs to be seen

#-----------------------------------------------------------------------------
# Update the conditional probability table counts with observed state, action,
# and next state values
#-----------------------------------------------------------------------------
    def update(self, sa, s_prime):
        ind = (s_prime,) + tuple(np.array(sa)[self.parents])
        self.pr[ind] += 1

#-----------------------------------------------------------------------------
# Quick check to determine if all the factors of a state-action pair are known
# If known, returns true and number of times that s-a pair has been seen
#-----------------------------------------------------------------------------
    def isKnown(self, sa):
        # check if we've seen enough s,a pairs for this data to be meaningful
        ind = (Ellipsis,) + tuple(np.array(sa)[self.parents])
        n = np.sum(self.pr[ind])
        if n >= self.m:
            return True, n
        else:
            return False, n

#-----------------------------------------------------------------------------
# Get the probability of all the next factor values
#-----------------------------------------------------------------------------
    def getProb(self, sa):

        # array for holding probabilities
        prob = np.ones(self.numDisc)

        isKnown, n = self.isKnown(sa)
        if isKnown:
            for i in np.arange(self.numDisc):
                ind = (i,) + tuple(np.array(sa)[self.parents])
                prob[i] = self.pr[ind] / n
        else:
            prob *= -1

        return prob



#==============================================================================
# Simple class for holding the a reachable state and it's probability
#==============================================================================
class NextState:
    def __init__(self):
        self.s = 0  # tuple of the next state
        self.pr = 0  # probability of reaching that state
