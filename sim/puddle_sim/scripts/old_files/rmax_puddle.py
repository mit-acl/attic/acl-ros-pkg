#!/usr/bin/env python
'''
Description: Wrapper around the puddle ros server.  Implements the rmax
algorithm in the puddle domain.

Created on Aug 26, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import sets
import matplotlib.pyplot as plt
import cPickle as pickle
import copy

# from aclpy import rmax
import rmax
import rmax_dbn

from puddle_sim.srv import *
import PuddleWorld as pw


class RmaxPuddle:

    def __init__(self, mDown=20, gamma=0.95, showDomain=True, numConvStates=75,
                 logFileIndex=0, logFile0=None, logFile1=None, logFile2=None):

        self.showDomain = showDomain
        self.numConvStates = numConvStates
        self.mDown = mDown
        self.gamma = gamma
        self.uniDirectional = False
        if self.uniDirectional:
            self.mDown = np.Inf

        self.reward_fig = None
        self.q_fig = None
        plt.ion()

        self.useDBN = True

        # set up puddle world
        puddle_noise = np.array([0.0, 0.0, 0.01])
        self.pw = []
        for i in np.arange(3):
            self.pw.append(pw.PuddleWorld(puddle_noise[i], i))

        self.foundGoal = 0

        # get the file path for rospy_tutorials
        package_address = roslib.packages.get_pkg_dir('puddle_sim')

        self.logFileAddress = package_address + '/log/mfrmax' + str(logFileIndex) + '.p'
        self.policyEvalAddress = package_address + '/log/policyEval' + str(logFileIndex) + '.p'
        self.logFile = open(self.logFileAddress, 'wb')

        savedLog = [logFile0, logFile1, logFile2]
        self.initMFRmax()
        self.initRmax(savedLog)

#-----------------------------------------------------------------------------
# Initialize the MF-Rmax algorithm
#-----------------------------------------------------------------------------
    def initMFRmax(self):
        self.numD = 3  # number of simulators
        self.dStart = 0  # start at level zero
        self.d = 0
        self.con = [False] * self.numD  # each level starts not converged
        self.eps = 0.0 * np.ones(self.numD - 1)  # should be length maxd-1
        self.levelIterations = [[] for i in range(self.numD)]  # for storing
        # number of sim calls at each simulation level
        self.bestQ = []  # save best Q for highest level every few iterations

#-----------------------------------------------------------------------------
# Initialize the Rmax algorithm
#-----------------------------------------------------------------------------
    def initRmax(self, savedLog):
        # State and action space discretization
        self.numX = 10
        self.numY = 10
        self.numA = 4
        # self.init_state = np.array([0., 0.])
        logFile = '/home/mark/acl-ros-pkg/sim/puddle_sim/log/'

        # create the discretized states and actions
        self.xbins = np.linspace(0, 1, self.numX, False)
        self.ybins = np.linspace(0, 1, self.numY, False)
        self.xbins = self.xbins[1:]  # has to do with how np.digitize assigns bins
        self.ybins = self.ybins[1:]

        self.s = self.initState()  # self.init_state

        for i in np.arange(self.numD):
            if savedLog[i] != None:
                savedLog[i] = logFile + savedLog[i] + '.npz'

        # initialize RMax algorithm for each simulation level
        S = [np.arange(self.numX), np.arange(self.numY)]
        A = [np.arange(self.numA)]
        max_reward = 0.0
        U = max_reward * np.ones((self.numX, self.numY, self.numA))

        self.rmax = []
        for i in range(self.numD):
            self.ts = self.getTerminalStates(i)
            # Set up generalized mapping for transition dynamics
            if self.useDBN:
                s_prime2sa = np.array([[1, 0, 1],
                                       [0, 1, 1]])
                self.rmax.append(rmax_dbn.RmaxDBN(S, A, U, self.ts,
                            logFile + 'log' + str(i) + '.npz',
                            savedLog=savedLog[i],
                            s_prime2sa=s_prime2sa))
            else:
                self.rmax.append(rmax.Rmax(S, A, U, self.ts,
                            logFile + 'log' + str(i) + '.npz',
                            savedLog=savedLog[i]))

        # return another rmax handle for evaluating policies
        return rmax.Rmax(S, A, U, self.ts)

#-----------------------------------------------------------------------------
# Set initial state
#-----------------------------------------------------------------------------
    def initState(self):
        center = np.array([0.1, 0.1])
        center[0] += np.random.randn() * 0.03
        center[1] += np.random.randn() * 0.03
        if center[0] < 0.0:
            center[0] = 0.0
        if center[1] < 0.0:
            center[1] = 0.0
        return center

#-----------------------------------------------------------------------------
# Run the Multi-Fidelity Rmax algorithm
#-----------------------------------------------------------------------------
    def runRmax(self):
        updateCounter = 0
        currentLevelIter = 0
        numUnknownStates = 0
        topSimCnt = 0
        i = 0
        while not rospy.is_shutdown():
            if np.mod(i, 100) == 0:
                print "\n\nIteration: " + str(i) + "\n\n"

            if np.mod(i, 50) == 0:
                self.rmax[self.d].valIter()

            # get next action
            a = self.rmax[self.d].choose_action(self.cont2discState(self.s))
            sa = tuple(self.cont2discState(self.s)) + tuple(a)

            # check going down conditions
            # first, make sure you have seen unknown states at least mDown times
            if self.d > 0 and not self.rmax[self.d - 1].stateActionKnown(sa):  # self.rmax[self.d - 1].n[sa] < self.rmax[self.d - 1].m:
                numUnknownStates += 1
                print "Number of unknown states visited: " + str(numUnknownStates)
            if numUnknownStates >= self.mDown and self.setRhatThat():
                # go down a level
                numUnknownStates = 0
                rospy.logwarn("\n\nMoving down to level %d\n\n", self.d - 1)

                # save pertinent data
                currentLevelIter = self.saveLog(i, currentLevelIter)

                self.con[self.d - 1] = False
                self.d -= 1
                self.showEstReward()
                self.showQ()
                self.s = self.initState()  # self.init_state
                # run value iteration a level down
                self.rmax[self.d].valIter()
                updateCounter = 0

            else:
                ns, r, t = self.PuddleCaller(self.s, a, self.d + self.dStart,
                                             self.showDomain)
                if (self.rmax[self.d].n[sa] >= self.rmax[self.d].m) or (sa[0:self.rmax[self.d].numS] in self.rmax[self.d].terminal_states):
                    updateCounter += 1  # increment counter if this is a known state (or goal state)
                else:
                    updateCounter = 0  # reset counter if state is unknown
                # save Q for top sim level
                if self.d == (self.numD - 1):
                    topSimCnt += 1
                    if np.mod(topSimCnt, 100) == 1:
                        self.bestQ.append(copy.copy(self.rmax[self.d].Q))
                if self.d == (self.numD - 1) or self.rmax[self.d + 1].n[sa] < self.rmax[self.d + 1].m:
                    out = self.rmax[self.d].update(a, r, self.cont2discState(self.s),
                                           self.cont2discState(ns))
                    if out == 1:  # run value iteration
                        # self.rmax[self.d].valIter()
                        updateCounter = 0
                        # n in level d was m, check to make sure n at d-1 is also at least m
                        if self.d > 0 and self.rmax[self.d - 1].n[sa] < self.rmax[self.d - 1].m:
                            self.rmax[self.d - 1].n[sa] = self.rmax[self.d - 1].m

                    if out == -1:  # reached terminal state
                        self.foundGoal += 1
                        self.s = self.initState()  # self.init_state
                    else:
                        self.s = ns
                else:
                    if self.PuddleCaller(self.s, -1, self.d + self.dStart,
                                         False)[2]:  # returns true if terminal state
                        self.s = self.initState()  # self.init_state
                    else:
                        self.s = ns

            if updateCounter > self.numConvStates:
#                 print "Resulting 'n'"
#                 print self.rmax[self.d].n
                rospy.logwarn("\n\nLevel %d has converged\n\n", self.d)

                # save pertinent data
                currentLevelIter = self.saveLog(i, currentLevelIter)

                self.con[self.d] = True
                if self.d == self.numD - 1:
                    rospy.loginfo("\n\nFinished learning!\n\n")
                    break
                updateCounter = 0

            if self.d < (self.numD - 1) and self.con[self.d] == True:
                rospy.loginfo("\n\nMoving up to level %d\n\n", self.d + 1)

                # set important values before moving up
                self.setQ()
                self.d += 1
                self.s = self.initState()  # self.init_state
                updateCounter = 0
                numUnknownStates = 0

                # plotting
                self.showEstReward()
                self.showQ()
#                if self.d > 1:
#                    self.showDomain = True
#                else:
#                    self.showDomain = False

            i += 1

        self.showEstReward()
        self.showQ()
        self.bestQ.append(copy.copy(self.rmax[self.d].Q))
        self.saveLog(i, currentLevelIter)
        self.logFile.close()

        # raw_input("\n\nPress Enter to end program\n\n")

#-----------------------------------------------------------------------------
# Evaluate policy Q m times to get an estimate of how many runs it takes to get to the goal
#-----------------------------------------------------------------------------
    def evalQ(self, bestQ=None):

        fout = open(self.policyEvalAddress, 'wb')

        if bestQ == None:
            f = open(self.logFileAddress, 'rb')
            p = pickle.load(f)
            bestQ = p["bestQ"]

        numTrials = 20
        numQ = len(bestQ)
        reward = [[[] for j in range(numTrials)] for i in range(numQ)]
        rm = self.initRmax([None] * self.numD)
        print "There are " + str(numQ) + " sets of Q values"
        for i in range(numQ):
            # get Q values
            print "Starting Q value " + str(i)
            Q = bestQ[i]
            rm.Q = Q
#            print "newQ"
#            print reward
            for j in range(numTrials):
                self.s = self.initState()  # self.init_state
                for k in range(300):
                    a = rm.choose_action(self.cont2discState(self.s))
                    ns, r, t = self.PuddleCaller(self.s, a,
                                         self.numD - 1 + self.dStart, False)
                    reward[i][j].append(copy.copy(r))
                    self.s = ns
                    if tuple(self.cont2discState(self.s)) in self.ts:

                        break
                print "Trial " + str(j) + " took " + str(k) + " steps to complete"

        pickle.dump(reward, fout)
        fout.close()
        return reward



#-----------------------------------------------------------------------------
# Set Q (or U for the first time) at the level above you
#-----------------------------------------------------------------------------
    def setQ(self):
        if self.rmax[self.d + 1].n.max() < self.rmax[self.d + 1].m:
            # initialize rmax[d+1] U to Q + eps
            self.rmax[self.d + 1].Q = self.rmax[self.d].Q + self.eps[self.d]
        else:
            # loop through states (at all levels)
            for sa_index, ntmp in np.ndenumerate(self.rmax[self.d].n):
                seenSA = False
                for i in np.arange(self.d + 1, self.numD):
                    if self.useDBN:
                        if self.rmax[i].stateActionKnown(sa_index):
                            seenSA = True
                    else:
                        n = self.rmax[i].n[sa_index]
                        if n >= self.rmax[i].m:
                            seenSA = True
                if not seenSA:
                    # set Q for this sa pair
                    self.rmax[self.d + 1].Q[sa_index] = self.rmax[self.d].Q[sa_index] + self.eps[self.d]
                # else
                    # don't set Q at the level above if false, we already have better information
#-----------------------------------------------------------------------------
# Transfer Rhat and That for use in the next level down
#-----------------------------------------------------------------------------
    def setRhatThat(self):

        setValues = False
        # set Rhat and That at level d-1 when n >= m
        for sa_index, n_iter in np.ndenumerate(self.rmax[self.d].n):
            if n_iter >= self.rmax[self.d].m:
                # set Rhat
                diff = np.abs(self.rmax[self.d - 1].Rhat[sa_index] - self.rmax[self.d].Rhat[sa_index])
                if diff > 0.1:
                    setValues = True
                    self.rmax[self.d - 1].Rhat[sa_index] = self.rmax[self.d].Rhat[sa_index]
                # set n, and r
                self.rmax[self.d - 1].n[sa_index] = self.rmax[self.d].n[sa_index]
                self.rmax[self.d - 1].r[sa_index] = self.rmax[self.d].r[sa_index]

                if sa_index in self.rmax[self.d].sa2s_prime:
                    self.rmax[self.d - 1].sa2s_prime[sa_index] = self.rmax[self.d].sa2s_prime[sa_index]
                    s_prime_list = self.rmax[self.d].sa2s_prime[sa_index]
                    for s_prime_iter in s_prime_list:
                        sas_prime = sa_index + s_prime_iter
                        if sas_prime in self.rmax[self.d - 1].That:
                            diff = np.abs(self.rmax[self.d - 1].That[sas_prime] - self.rmax[self.d].That[sas_prime])
                            if diff > 0.1:
                                setValues = True
                                self.rmax[self.d - 1].That[sas_prime] = self.rmax[self.d].That[sas_prime]
                        else:
                            self.rmax[self.d - 1].That[sas_prime] = self.rmax[self.d].That[sas_prime]
                            setValues = True
        return setValues

#-----------------------------------------------------------------------------
# Set a set of terminal states
#-----------------------------------------------------------------------------
    def getTerminalStates(self, d):
        ts = sets.Set([])

        # construct set of terminal states
        for i in np.arange(self.numX):
            for j in np.arange(self.numY):
                s = self.disc2contState(np.array([i, j]))
                sp, r, t = self.PuddleCaller(s, -1, d + self.dStart, False)
                if t:
                    ts.add((i, j))

        return ts

#-----------------------------------------------------------------------------
# Save r, n, and sa2s_prime for use in the next simulator down
#-----------------------------------------------------------------------------
    def saveLog(self, i, currentLevelIter):
        # save pertinent data
        # number of iterations ran at this level of the sim
        self.levelIterations[self.d].append(i - currentLevelIter)
        print "\n\nLevelIterations: "
        print self.levelIterations

        self.logFile.seek(0)
        log = {"levelIterations":self.levelIterations, "bestQ":self.bestQ}
        pickle.dump(log, self.logFile)

        return i


#         np.savez(self.logFile, n=n, r=r, n_prime=n_prime, sa2s_prime=sa2s_prime)

#-----------------------------------------------------------------------------
# r should be a numpy 2d array containing the estimated reward values
#-----------------------------------------------------------------------------
    def showEstReward(self):
        # Draw the estimated reward function
#         print self.rmax[self.d].r
        ravg = self.rmax[self.d].r / self.rmax[self.d].n
#         print ravg
        ravg = np.nan_to_num(ravg)
#         print ravg
        r = np.amin(ravg, axis=2)
#         print r
        self.reward_fig = plt.figure("Estimated Reward")
        self.est_reward_im = plt.imshow(r.transpose(), extent=(0, 1, 0, 1),
                                    origin="lower")
        self.est_reward_im.set_data(r.transpose())
        plt.draw()

#-----------------------------------------------------------------------------
# r should be a numpy 2d array containing the estimated reward values
#-----------------------------------------------------------------------------
    def showQ(self):
        # Draw the estimated reward function
        q = np.amax(self.rmax[self.d].Q, axis=2)
        self.q_fig = plt.figure("Q")
        self.q_im = plt.imshow(q.transpose(), extent=(0, 1, 0, 1),
                                    origin="lower")
        self.q_im.set_data(q.transpose())
        plt.draw()

#-----------------------------------------------------------------------------
# Run a single step of puddleworld
#-----------------------------------------------------------------------------
    def PuddleCaller(self, s, a, d, showDomain):
        sp, r, term = self.pw[d].puddleStep(s, a, showDomain)
        return sp, r, term
#         rospy.wait_for_service('puddle_step')
#         try:
#             puddle_step = rospy.ServiceProxy('puddle_step', PuddleStep)
#             resp = puddle_step(s[0], s[1], a, d, showDomain)
#             sp = np.array([resp.xp, resp.yp])
#             return sp, resp.r, resp.term
#         except rospy.ServiceException, e:
#             print "Service call failed: %s" % e

#-----------------------------------------------------------------------------
# converts 2d discrete states to continuous (using state centers)
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def disc2contState(self, ds):
        x = float(ds[0]) / self.numX + 1. / self.numX / 2.
        y = float(ds[1]) / self.numY + 1. / self.numY / 2.
        return np.array([x, y])

#-----------------------------------------------------------------------------
# converts 2d continuous states to discrete
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def cont2discState(self, cs):
#         print "xbins"
#         print self.xbins
#         print cs
        x = np.digitize(np.array([cs[0]]), self.xbins)
        y = np.digitize(np.array([cs[1]]), self.ybins)
        return np.array([x[0], y[0]])



if __name__ == "__main__":
    try:
        rospy.init_node('rmax_puddle')
        argv = rospy.myargv(argv=sys.argv)
        if len(argv) == 2:
            c = RmaxPuddle(logFileIndex=argv[1])
        if len(argv) == 4:
            logFile0 = argv[1]
            logFile1 = argv[2]
            logFile2 = argv[3]
            c = RmaxPuddle(logFile0=logFile0, logFile1=logFile1,
                           logFile2=logFile2)
        else:
            c = RmaxPuddle()
        c.runRmax()
        raw_input("Press Enter to end program")
#         c.evalQ()

    except rospy.ROSInterruptException:
        pass
