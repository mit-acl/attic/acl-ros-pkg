#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter


f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/bandit1/mfrl_0.p', 'rb')


p = pickle.load(f)
numIter = p["level_iterations"]
print p
s = 0
fig, ax = plt.subplots()
# c = ['r','b','g']
c = np.array([1, 0, 0])
alpha = [0.2, 0.5, 1.0]
for i in np.arange(len(numIter[2])):
    numIter[2][i] /= 2.0
for i in np.arange(3):
    ax.bar(i, numIter[i][0], color=c, alpha=alpha[i])
    s += sum(numIter[i])
    for j in np.arange(1, len(numIter[i])):
        b = sum(numIter[i][:j])
        ax.bar(i, numIter[i][j], bottom=b, color=c, alpha=alpha[i])
        print b

ax.set_ylabel('Simulation Steps', size=20)
ax.set_xlabel('Simulator Fidelity', size=20)
# ax.set_aspect(0.0014)
ax.xaxis.set_major_locator(MultipleLocator(1.0))

# plt.axis('off')
fig.canvas.draw()
plt.savefig('/home/mark/acl-ros-pkg/veh_control/car_control/figures/mfrmax.pdf',
            format='pdf', transparent=True, bbox_inches='tight')
plt.show()


try:
    r = p["reward"]
    n = len(r)
    sumR = np.zeros(n)
    for i in np.arange(n):
        sumR[i] = np.sum(r[i])

    print sumR / 3

except:
    pass
