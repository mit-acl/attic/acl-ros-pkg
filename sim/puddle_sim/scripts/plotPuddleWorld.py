#!/usr/bin/env python
'''
Description: Simple plotting utility for puddle world domain

Created on Sep 9, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64MultiArray
import numpy as np
import matplotlib.pyplot as plt

class PlotPuddleWorld(object):


    def __init__(self):
        self.state = np.array([0.0, 0.0])
        self.reward_map = np.zeros((100.0, 100.0))
        for i in np.arange(20):
            for j in np.arange(20):
                self.reward_map[i, j] = -1.0

        self.plotReward = True
        self.domain_fig = None
#         plt.ion()

    def state_mark_cb(self, data):
        self.state[0] = data.data[0]
        self.state[1] = data.data[1]
        # print self.state

    def reward_cb(self, data):
        # convert Float64MultiArray to numpy array
        self.reward_map = multiarray2DToNumpyArray(data)
        self.plotReward = True
        print "\n\ngot reward map!!!\n\n"

    def plot(self, event):
        # Draw the environment
        if self.domain_fig is None:
            self.domain_fig = plt.figure("Domain")
            self.reward_im = plt.imshow(self.reward_map, extent=(0, 1, 0, 1),
                                        origin="lower", interpolation='nearest')
            self.state_mark = plt.plot(self.state[0], self.state[1], 'kd',
                                           markersize=20)
            plt.draw()
        else:
            if self.plotReward:
                self.plotReward = True
                for i in np.arange(20, 40):
                    for j in np.arange(20, 40):
                        self.reward_map[i, j] -= 0.1
                self.reward_im.set_data(self.reward_map)
#             self.reward_im = self.ax.imshow(self.reward_map, extent=(0, 1, 0, 1),
#                                     origin="lower")
            self.state += 0.01
            self.state_mark[0].set_data([self.state[0]], [self.state[1]])
            plt.draw()

#         for i in np.arange(20, 40):
#             for j in np.arange(20, 40):
#                 self.reward_map[i, j] -= 0.1
#         if self.domain_fig == None:
#             self.domain_fig, self.ax = plt.subplots()
#         self.reward_im = self.ax.imshow(self.reward_map, extent=(0, 1, 0, 1),
#                                     origin="lower", interpolation='nearest')
#         self.state_mark = self.ax.plot(self.state[0], self.state[1], 'kd',
#                                        markersize=20)
#         self.domain_fig.show()


    def plot_puddle_start(self):
        rospy.init_node('plot_puddle')
        rospy.Subscriber("/puddle_world/state", Float64MultiArray,
                         self.state_mark_cb)
#        rospy.Subscriber("/puddle_world/reward", Float64MultiArray,
#                         self.reward_cb)
        rospy.Timer(rospy.Duration(1.0), self.plot)
        rospy.spin()


#-----------------------------------------------------------------------------
# 2D Multiarray to numpy array -- convert multiarray data type to a numpy array
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def multiarray2DToNumpyArray(ma):
    I = ma.layout.dim[0].size
    J = ma.layout.dim[1].size
    na = np.empty([I, J])
    for i in range(I):
        for j in range(J):
            index = ma.layout.data_offset + ma.layout.dim[1].stride * i + j
            na[i, j] = ma.data[index]
    return na

if __name__ == "__main__":
    pw = PlotPuddleWorld();
    pw.plot_puddle_start()
