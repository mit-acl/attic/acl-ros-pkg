#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

def plotArrow(ax, x, c):
    ax.annotate('', xy=(i + x[0], b + numIter[i][j]), xycoords='data',
                xytext=(i + x[1], b + numIter[i][j]), textcoords='data',
                arrowprops=dict(width=3, headwidth=6, frac=0.04, color=c))

f = open('/home/mark/acl-ros-pkg/sim/puddle_sim/log/Jan7/mfrl_example_tab_bar.p', 'rb')
# f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/state_r3_v2_1/mfrl_0.p', 'rb')

p = pickle.load(f)
# try:
numIter = p["level_iterations"]
level_dir = p["level_directions"]
fig, ax = plt.subplots()
# c = ['r','b','g']
c = np.array([0, 0, 1, 1])
alpha = [0.2, 0.5, 1.0]
level_iterations = np.zeros(3)
for i in np.arange(3):
    for j in np.arange(len(numIter[i])):
        b = sum(numIter[i][:j])
        barlist = ax.bar(i, numIter[i][j], bottom=b, color=c,
                         alpha=alpha[i], linewidth=4., align='center')
        if level_dir[i][j] == -1:
            plotArrow(ax, [-0.47, 0.4], np.array([153. / 255, 0, 0]))
        elif level_dir[i][j] == 1:
            plotArrow(ax, [0.47, -0.4], np.array([0, 153. / 255, 76. / 255]))

print 'Base level: ' + str(sum(numIter[0]))
print 'Mid level: ' + str(sum(numIter[1]))
print 'Top level: ' + str(sum(numIter[2]))
print

ax.set_ylabel('Simulation Steps', size=20)
# This sets the ticks on the x axis to be exactly where we put
# the center of the bars.
# ax.set_xticks(ind)

# Labels for the ticks on the x axis.  It needs to be the same length
# as y (one label for each bar)
group_labels = ['No Puddle', 'Some Puddle',
                 'Full Puddle']

# Set the x tick labels to the group_labels defined above.
ax.set_xticklabels(group_labels, size=20)

# fig.autofmt_xdate()

for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(15)

# plt.axis('off')
fig.canvas.draw()
# plt.savefig('/home/mark/acl-ros-pkg/sim/puddle_sim/figures/mfrl_bar.pdf',
#            format='pdf', transparent=True , bbox_inches='tight')


plt.show()
# except:
#     pass

# try:
#     r = p["reward"]
#     n = len(r)
#     sumR = np.zeros(n)
#     for i in np.arange(n):
#         sumR[i] = np.sum(r[i])
#
#     print sumR
#
# except:
#     pass
