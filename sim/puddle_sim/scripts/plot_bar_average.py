#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
#import roslib
#roslib.load_manifest('puddle_sim')
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def plot(mean, err, labels, path, aspect, legend, c,
         loc='best', alph=1, samples=None, markers=['D', 'o', 's', '^']):

    n = len(mean)

    if n == 2:
        bar_width = 0.3
        offset = np.linspace(-bar_width, 0, n)
    if n == 3:
        bar_width = 0.22
        offset = np.linspace(-3*bar_width/2, bar_width/2, n)
    elif n == 4:
        bar_width = 0.15
        offset = np.linspace(-2*bar_width, bar_width, n)
        offset[:2] -= 0.03
        offset[2:] += 0.03
    fig, ax = plt.subplots()
    barplot = [[0]*3 for i in range(n)]
    legplot = [[0]*3 for i in range(n)]

    for kk in range(n):
        for i in range(3):
            barplot[kk][i] = ax.bar(i + offset[kk], mean[kk][i], color=c[kk],
                                    alpha=alph, linewidth=2.5,
                                    width=bar_width, align='edge')
            if samples is not None:
                ns = len(samples[kk][i, :])
                for j in range(ns):
                    #ax.plot(i + offset[kk]+(j)*bar_width/(ns)+bar_width/(2*ns),
                    legplot[kk][i] = ax.plot(i + offset[kk]+bar_width/2.0,
                                             samples[kk][i, j], marker=markers[kk],
                                             markersize=7, fillstyle='full',
                                             markeredgecolor='black',
                                             markeredgewidth=1.1,
                                             color=c[kk], linestyle="None")
            else:
                ax.errorbar(i + offset[kk] + bar_width/2.0, mean[kk][i],
                            err[kk][i], capsize=7,
                            color='black', elinewidth=1.3, alpha=alph)

    leg = ()
    for i in range(n):
        if samples is not None:
            leg = leg + (legplot[i][-1][0],)
        else:
            leg = leg + (barplot[i][-1])

    ax.legend(leg, legend, fancybox=True, fontsize=15, loc=loc, numpoints=1,
              markerscale=1.5)

    ax.set_ylabel('Simulation Steps', size=20)

    # This sets the ticks on the x axis to be exactly where we put
    # the center of the bars.
    ax.set_xticks(range(3))

    # Set the x tick labels to the group_labels defined above.
    ax.set_xticklabels(labels, size=20)

    fig.autofmt_xdate()

    if aspect is not None:
        ax.set_aspect(aspect/ax.get_data_ratio())

    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)

    fig.canvas.draw()
    plt.savefig(path, format='pdf', transparent=True, bbox_inches='tight')

    # plt.show()
