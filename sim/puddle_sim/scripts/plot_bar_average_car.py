#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
#import roslib
#roslib.load_manifest('puddle_sim')
import numpy as np
import cPickle as pickle
from scipy import stats

import plot_bar_average

package_address = '/home/mark/acl-ros-pkg/veh_control/car_control' #roslib.packages.get_pkg_dir('car_control')
labels = [r'$\text{Input Only~}(\boldsymbol{\Sigma_1})$',
          r'$\text{Dynamics Sim~}(\boldsymbol{\Sigma_2})$',
          r'$\text{Real Car~}(\boldsymbol{\Sigma_3})$']


class Figure:
    def __init__(self):
        self.loc = 'best'
        self.aspect = 1.15


f1 = Figure()
#### User parameters #####
f1.fig_path = package_address + '/figures/mfrl_bar_average_state.pdf'
f1.data_path = [package_address + '/log/state_r3_v3_',
                package_address + '/log/state_r3_v3_uni_']
f1.num_samples = 3
f1.colors = [np.array([0. / 255, 0. / 255, 255. / 255]),
             np.array([0. / 255, 153. / 255, 76. / 255])]
f1.legend = ('MFRL', 'UNI')
f1.markers = ['D', 'o']
#### End user parameters ####

f2 = Figure()  #
### User parameters #####
f2.fig_path = package_address + '/figures/mfrl_bar_average_bandit.pdf'
f2.data_path = [package_address + '/log/bandit_',
                package_address + '/log/bandit_uni_']
f2.num_samples = 3
f2.colors = [np.array([0. / 255, 0. / 255, 255. / 255]),
             np.array([0. / 255, 153. / 255, 76. / 255])]
f2.legend = ('MFRL', 'UNI')
f2.markers = ['D', 'o']
#### End user parameters ####

f3 = Figure()  #
### User parameters #####
f3.fig_path = package_address + '/figures/mfrl_bar_average_dbn.pdf'
f3.data_path = [package_address + '/log/state_r3_v3_',
                package_address + '/log/state_r3_v3_dbn_',
                package_address + '/log/state_r3_v3_uni_',
                package_address + '/log/state_r3_v3_dbn_uni_']
f3.legend = ('MFRL-TAB', 'MFRL-DBN', 'UNI-TAB', 'UNI-DBN')
f3.colors = [np.array([0. / 255, 0. / 255, 255. / 255]),
             np.array([0. / 255, 200. / 255, 255. / 255]),
             np.array([0. / 255, 153. / 255, 76. / 255]),
             np.array([0. / 255, 255. / 255, 0. / 255])]
f3.num_samples = 3
f3.markers = ['D', 's', 'o', '^']
#### End user parameters ####

f4 = Figure()  #
### User parameters #####
f4.fig_path = package_address + '/figures/mfrl_bar_average_dbn_big.pdf'
f4.data_path = [package_address + '/log/state_r4_v6_dbn_',
                package_address + '/log/state_r4_v6_dbn_gen_',
                package_address + '/log/state_r4_v6_dbn_uni_']
f4.legend = ('MFRL-DBN', 'MFRL-DBN-GEN', 'UNI-DBN')
f4.colors = [np.array([0. / 255, 200. / 255, 255. / 255]),
             np.array([255. / 255, 200. / 255, 0. / 255]),
             np.array([0. / 255, 255. / 255, 0. / 255])]
f4.num_samples = 2
f4.loc = (-.165,0.84)
f4.markers = ['s', 'p', '^']
#### End user parameters ####


F = [f1, f2, f3, f4]
F = [f4]
for fig in F:
    mean = []
    err = []
    samples_all = []
    for i in range(len(fig.data_path)):
        samples = np.zeros((3, fig.num_samples))
        switches = np.zeros((2, fig.num_samples))
        for j in range(fig.num_samples):
            f = open(fig.data_path[i] + str(j) + '/mfrl_0.p', 'rb')
            p = pickle.load(f)
            numIter = p["level_iterations"]
            for k in range(3):
                samples[k, j] = sum(numIter[k])
            switches[0, j] = 2 * len(numIter[0]) - 1
            switches[1, j] = 2 * len(numIter[1]) - 1

        print "samples:"
        print samples
        print "switches:"
        print switches
        print
        mean_samples = np.mean(samples, 1)
        std_dev = np.std(samples, 1)
        std_err = stats.sem(samples, 1)

        mean_switches = np.mean(switches, 1)
        std_dev_switches = np.std(switches, 1)
        std_err_switches = stats.sem(switches, 1)

        mean.append(mean_samples)
        err.append(std_dev)
        samples_all.append(samples)

        # print percent decrease
        if i == 1 or i == 3:
            print "percent decrease: " + str(100.0*(sum(mean[i-1]) - sum(mean[i]))/sum(mean[i-1])) + "%"

    plot_bar_average.plot(mean, err, labels, fig.fig_path, fig.aspect,
                          fig.legend, fig.colors, alph=0.5, samples=samples_all,
                          markers=fig.markers, loc=fig.loc)
