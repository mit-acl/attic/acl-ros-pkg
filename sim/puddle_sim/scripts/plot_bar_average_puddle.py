#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib
roslib.load_manifest('puddle_sim')
import numpy as np
import cPickle as pickle
from scipy import stats

import plot_bar_average

package_address = roslib.packages.get_pkg_dir('puddle_sim')

labels = [r'$\text{No Puddle~}(\boldsymbol{\Sigma_1})$',
          r'$\text{Some Puddle~}(\boldsymbol{\Sigma_2})$',
          r'$\text{Full Puddle~}(\boldsymbol{\Sigma_3})$']

#file_path = '/log/2times_neg_reward/total_reward_'
file_path = '/log/total_reward_'

ar = 1.15
class Figure:
    def __init__(self):
        self.loc = 'best'
        self.aspect = None


# -- used this version for ICRA 2014 results
f1 = Figure()
f1.files = [file_path + '1000.p',
            file_path + 'uni_1000.p',
            file_path + 'just_top_1000.p']
f1.legend = ('MFRL', 'UNI', 'RMAX')
f1.colors = [np.array([0. / 255, 0. / 255, 255. / 255]),
             np.array([0. / 255, 153. / 255, 76. / 255]),
             'm']
f1.name = 'mfrl_bar_average.pdf'
f1.aspect = ar

f2 = Figure()
f2.files = [file_path + '1000.p',
            file_path + 'dbn_1000.p',
            file_path + 'uni_1000.p',
            file_path + 'dbn_uni_1000.p']
f2.legend = ('MFRL-TAB', 'MFRL-DBN', 'UNI-TAB', 'UNI-DBN')
f2.colors = [np.array([0. / 255, 0. / 255, 255. / 255]),
             np.array([0. / 255, 200. / 255, 255. / 255]),
             np.array([0. / 255, 153. / 255, 76. / 255]),
             np.array([0. / 255, 255. / 255, 0. / 255])]
f2.name = 'mfrl_dbn_bar_average.pdf'
f2.loc = (0.51,0.75)
f2.aspect = ar

f3 = Figure()
f3.files = [file_path + '1000.p',
            file_path + 'gen_1000.p',
            file_path + 'dbn_1000.p',
            file_path + 'dbn_gen_1000.p']
f3.colors = [np.array([0. / 255, 0. / 255, 255. / 255]),
             np.array([0. / 255, 200. / 255, 255. / 255]),
             np.array([255. / 255, 0. / 255, 0. / 255]),
             np.array([255. / 255, 200. / 255, 0. / 255])]
f3.legend = ['MFRL', 'MFRL-GEN', 'MFRL-DBN', 'MFRL-DBN-GEN']
f3.name = 'mfrl_gen_bar_average.pdf'
f3.aspect = ar

path = '/home/mark/acl-ros-pkg/sim/puddle_sim/figures/'

F = [f1, f2, f3]
#F = [f1]

for fig in F:

    mean = []
    err = []
    for kk in range(len(fig.files)):
        f = open(package_address + fig.files[kk], 'rb')

        p = pickle.load(f)

        numIter = p["level_sample_list"]
        learning_runs = len(numIter[0])
        if learning_runs > 1000:
            learning_runs = 1000

        samples = np.zeros((3, learning_runs))
        switches = np.zeros((2, learning_runs))
        for j in np.arange(learning_runs):
            for i in np.arange(3):
                if i == 2 or (fig.files[kk] == file_path + 'just_top_1000.p'):  # TODO: this needs to be more robust:
                    # if the data were generated using the real_world_samples flag,
                    # then the last entry in level 2 has extra samples at the end,
                    # -- should ignore these extra samples
                    numIter[i][j] = numIter[i][j][:-1]
                samples[i, j] = sum(numIter[i][j])
            switches[0, j] = 2 * len(numIter[0][j]) - 1  # switches from level 0 to 1 (switches
                            # up and back down (hence 2), except for start)
            switches[1, j] = 2 * len(numIter[2][j]) - 1  # 2 switches except for end
            if sum(switches[:, j]) != 2 * len(numIter[1][j]):
                print "Error in counting number of switches!"

        mean_samples = np.mean(samples, 1)
        std_dev = np.std(samples, 1)
        std_err = stats.sem(samples, 1)

        mean_switches = np.mean(switches, 1)
        std_dev_switches = np.std(switches, 1)
        std_err_switches = stats.sem(switches, 1)

        # hack to get the just top plot to work
        if fig.files[kk] == file_path + 'just_top_1000.p':
            mean_samples[2] = mean_samples[0]
            mean_samples[0] = 0
            std_dev[2] = std_dev[0]
            std_dev[0] = 0
            std_err[2] = std_err[0]
            std_err[0] = 0

        mean.append(mean_samples)
        err.append(std_dev)

        # print percent decrease
        if kk == 1 or kk == 3:
            print "percent decrease: " + str(100.0*(sum(mean[kk-1]) - sum(mean[kk]))/sum(mean[kk-1])) + "%"

    plot_bar_average.plot(mean, err, labels, path + fig.name,
                          fig.aspect, fig.legend, fig.colors, fig.loc)
    print
    print
