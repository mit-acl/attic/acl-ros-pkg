#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 13, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

from process_policy import Figure

plot_inset = False

package_address = roslib.packages.get_pkg_dir('puddle_sim')

data_file = open(package_address + '/scripts/policy_data.p', 'rb')
figs = pickle.load(data_file)

for f in figs:
    if f.name == 'tabular':
        f.shinyColors = ['b', np.array([0. / 255, 153. / 255, 76. / 255]), 'm', 'y',np.array([50. / 255, 20. / 255, 20. / 255]), 'r', 'c']
        if plot_inset:
            f.shinyLegend = ['MFRL ($\Sigma_1$, $\Sigma_2$, $\Sigma_3$)', 'UNIL ($\Sigma_1$, $\Sigma_2$, $\Sigma_3$)', 'RMAX ($\Sigma_3$)', 'PS ($\Sigma_3$)', 'MFRL ($\Sigma_1$, $\Sigma_3$)', 'MFRL ($\Sigma_2$, $\Sigma_3$)', 'MFRL ($\Sigma_1$, $\Sigma_3$, $\Sigma_2$)']
            f.special = [4,5,6]
        else:
            f.shinyLegend = ['MFRL ($\Sigma_1$, $\Sigma_2$, $\Sigma_3$)', 'UNIL ($\Sigma_1$, $\Sigma_2$, $\Sigma_3$)', 'RMAX ($\Sigma_3$)', 'PS ($\Sigma_3$)', 'MFRL ($\Sigma_1$, $\Sigma_3$)', 'MFRL ($\Sigma_2$, $\Sigma_3$)']
            f.special = [4,5]

        f.xlim = [0, 3300]
        f.ylim = [-40, 0]

    elif f.name == 'dbn':
        f.shinyColors = [np.array([0. / 255, 0. / 255, 255. / 255]),
                          np.array([0. / 255, 200. / 255, 255. / 255]),
                          np.array([0. / 255, 153. / 255, 76. / 255]),
                          np.array([0. / 255, 255. / 255, 0. / 255])]
        f.shinyLegend = ['MFRL-TAB', 'MFRL-DBN', 'UNI-TAB', 'UNI-DBN']
        f.xlim = [0, 2500]
        f.ylim = [-900, 0]
        f.aspect = 1.15
        f.special = None

    elif f.name == 'gen':
        f.shinyColors = [np.array([0. / 255, 0. / 255, 255. / 255]),
                          np.array([255. / 255, 0. / 255, 0. / 255]),
                          np.array([0. / 255, 200. / 255, 255. / 255]),
                          np.array([255. / 255, 200. / 255, 0. / 255])]
        f.shinyLegend = ['MFRL', 'MFRL-GEN', 'MFRL-DBN', 'MFRL-DBN-GEN']
        f.xlim = [0, 1200]
        f.ylim = [-400, 0]
        f.special = None

    elif f.name == 'levels':
        f.shinyColors = ['b', 'g', 'm', 'r', 'c', 'k', 'y']
        f.shinyLegend = ['132', '321', '21', '12', '1', '2', '3']
        f.xlim = [0, 3300]
        f.ylim = [-40, 0]
        f.special = None

    elif f.name == 'ps':
        f.shinyColors = ['b', 'g', 'm']
        f.shinyLegend = ['eps2', 'eps1', 'esp1.5']
        f.xlim = [0, 3300]
        f.ylim = [-1050, 0]
        f.special = None

for f in figs:

    fig, ax = plt.subplots()
    opt_values = np.zeros(len(f.file))
    # this is an inset axes over the main axes
    x1in, x2in, y1in, y2in = .64, .61, .25, .21
    if f.name == 'tabular' and plot_inset:
        a = plt.axes([x1in, x2in, y1in, y2in], axisbg='y')
    for ii in np.arange(len(f.mean_r)):

        if not plot_inset and ii > 5:
            break

        if f.special is None or ii not in f.special:
            handle = ax.errorbar(f.x_axis[ii], f.mean_r[ii],
                                 yerr=f.std_err[ii], fmt='ro-',
                                 color=f.shinyColors[ii],
                                 label=f.shinyLegend[ii], linewidth=3.0,
                                 ecolor='k', elinewidth=1.0)
            if f.name == 'tabular' and plot_inset:
                a.errorbar(f.x_axis[ii], f.mean_r[ii],
                           yerr=f.std_err[ii], fmt='ro-',
                           color=f.shinyColors[ii],
                           label=f.shinyLegend[ii], linewidth=3.0,
                           ecolor='k', elinewidth=1.0)
        else:
            handle = ax.errorbar(f.x_axis[ii], f.mean_r[ii],
                                 yerr=f.std_err[ii], fmt='ro--',
                                 color=f.shinyColors[ii],
                                 label=f.shinyLegend[ii], linewidth=3.0,
                                 alpha=0.4,
                                 ecolor='k', elinewidth=1.0)
            if f.name == 'tabular' and plot_inset:
                a.errorbar(f.x_axis[ii], f.mean_r[ii],
                           yerr=f.std_err[ii], fmt='ro--',
                           color=f.shinyColors[ii],
                           label=f.shinyLegend[ii], linewidth=3.0,
                           alpha=0.4,
                           ecolor='k', elinewidth=1.0)

    true_opt = np.mean(f.opt_values)
    # plot line showing optimal policy
    ax.plot([f.x_axis[0][0], f.x_axis[0][-1]], [true_opt, true_opt], 'k--')
    # plot line showing optimal policy

    if plot_inset:
        a.plot([f.x_axis[0][0], f.x_axis[0][-1]], [true_opt, true_opt], 'k--')

    ax.legend(loc='best', prop={'size':20}, fancybox=True)
    ax.set_xlabel(r'Full Puddle ($\boldsymbol{\Sigma_3}$) Samples', size=25)
    ax.set_ylabel('Average Cumulative Reward', size=25)
    ax.set_xlim(f.xlim)
    #ax.set_ylim(f.ylim)
#     ax.grid(b=True, which='both')

    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(22)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(22)

    if f.aspect is not None:
        ax.set_aspect(f.aspect/ax.get_data_ratio())

    if f.name == 'tabular' and plot_inset:
        x1, x2, y1, y2 = 2000, 3300, -40, 0

        # draw lines showing where the inset came from -- real hacky
        ax.plot([x1, 2190], [true_opt, -180], 'k', alpha=0.4, linewidth=1.3)
        ax.plot([x2, 3255], [true_opt, -180], 'k', alpha=0.4, linewidth=1.3)

        # create inset image
        # plot line showing optimal policy
        a.plot([f.x_axis[0][0], f.x_axis[0][-1]], [-11,
                                                   -11], 'k:')
        a.text(2050, -10, 'Optimal policy for $\Sigma_1$, $\Sigma_2$')
        a.set_xlim(x1, x2)
        a.set_ylim(y1, y2)
        a.set_yticks([-10, -20, -30])

        plt.setp(a, xticks=[])

    plt.savefig(package_address + '/figures/learning_curve_' + f.name
                + '_' + str(f.learning_runs) + '_runs.pdf',
                format='pdf', transparent=True, bbox_inches='tight')
