#!/usr/bin/env python
'''
Description: Plots for car RL paper
Note: This script requires that you run process_sensistivity_analysis.py first

Created on Jan 9, 2014

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

package_address = roslib.packages.get_pkg_dir('puddle_sim')
f = open(package_address + '/log/sensitivity_analysis/sensitivity_data.p', 'rb')
p = pickle.load(f)

class Figure:
    pass

c = ['b', 'r', 'g']
c2 = ['m', 'c']
alpha = 0.28
linestyle = ['-', '--', '-.']
legend = [r'$\boldsymbol{\Sigma_1}$', r'$\boldsymbol{\Sigma_2}$',
          r'$\boldsymbol{\Sigma_3}$']
legend2 = [r'$\boldsymbol{\Sigma_1} \leftrightarrow \boldsymbol{\Sigma_2}$',
           r'$\boldsymbol{\Sigma_2} \leftrightarrow \boldsymbol{\Sigma_3}$']

legend_fontsize = 20
axis_label_fontsize = 22
linewidth = 1.5
axis_tick_fontsize = 18
annotation_fontsize = 20

# figure 1
f1 = Figure()
f1.name = 'beta'
f1.xaxis = [0, 0.5, 1, 1.5, 2, 3]
f1.xlim = [-0.75, 3]
f1.xlabel = r'$\text{Optimality Tolerance,~}\boldsymbol{\beta}$'
f1.subplots = [True, False, False]
f1.default_val = 0
f1.top_percent = 0.98
f1.text_x = -85
f1.text_y = 10
f1.ybins = None

# figure 2
f2 = Figure()
f2.name = 'mknown'
f2.xaxis = [5, 15, 30, 45, 60, 75, 90, 105, 120]
f2.xlim = [5, 120]
f2.xlabel = r'$\text{MPD Convergence Tolerance,~}\boldsymbol{m_{known}}$'
f2.subplots = [True, True, True]
f2.default_val = 75
f2.top_percent = 0.9
f2.text_x = -90
f2.text_y = -10
f2.ybins = 4

# figure 3
f3 = Figure()
f3.name = 'munknown'
f3.xaxis = [1, 5, 10, 15, 20, 25, 30, 40]
f3.xlim = [1, 40]
f3.xlabel = r'$\text{Switching Parameter,~}\boldsymbol{m_{unknown}}$'
f3.subplots = [True, True, False]
f3.default_val = 20
f3.top_percent = 0.75
f3.text_x = -90
f3.text_y = 10
f3.ybins = 5

figs = [f1, f2, f3]
#figs = [f2, f3]

labelx = -0.11
labely = 0.5

for f in figs:
    # plt.figure()

    fig, AX = plt.subplots(sum(f.subplots), sharex=True)  # , sharex=True)

    if (f.subplots[0]):
        if (sum(f.subplots) > 1):
            ax = AX[0]
        else:
            ax = AX

        samples = p[f.name + '_samples']  # mean
        samples_dev = p[f.name + '_samples_dev']  # std deviation
        samples_err = p[f.name + '_samples_err']  # std error

        for i in range(samples.shape[1]):
            handle = ax.plot(f.xaxis, samples[:, i], color=c[i],
                             linestyle=linestyle[i], #label=legend[i],
                             linewidth=linewidth)
            ax.fill_between(f.xaxis, samples[:, i] - samples_dev[:, i],
                            samples[:, i] + samples_dev[:, i], alpha=alpha,
                            facecolor=c[i], edgecolor=c[i])

        p1 = plt.Rectangle((0, 0), 1, 1, fc=c[0], ec=c[0], alpha=alpha)
        p2 = plt.Rectangle((0, 0), 1, 1, fc=c[1], ec=c[1], alpha=alpha)
        p3 = plt.Rectangle((0, 0), 1, 1, fc=c[2], ec=c[2], alpha=alpha)
        ax.legend([p1, p2, p3], [legend[0], legend[1], legend[2]],
                  loc='upper right', fancybox=True, fontsize=legend_fontsize)

        #ax.legend(loc='upper right', fancybox=True, fontsize=legend_fontsize)
        ax.set_ylabel('Samples', size=axis_label_fontsize)

        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
        if f.ybins is not None:
            ax.locator_params(axis='y', nbins=f.ybins)

        limits = ax.axis()
        ax.axvline(f.default_val, color='k', linestyle='--')
        ax.annotate('Default', xy=(f.default_val, f.top_percent*(limits[3]-limits[2])),
                    xycoords='data', xytext=(f.text_x, f.text_y),
                    textcoords='offset points',
                    arrowprops=dict(arrowstyle="->"), size=annotation_fontsize)
        ax.yaxis.set_label_coords(labelx, labely)


        ax.set_xlim(f.xlim)
    #     plt.ylim(f.ylim)

#     ax2 = ax.twinx()

    if (f.subplots[1]):
        ax = AX[1]
        switches = p[f.name + '_switches']
        switches_dev = p[f.name + '_switches_dev']
        switches_err = p[f.name + '_switches_err']  # std error

        for i in range(switches.shape[1]):
            handle = ax.plot(f.xaxis, switches[:, i], color=c2[i],
                             linestyle=linestyle[i], label=legend2[i],
                             linewidth=linewidth)
            ax.fill_between(f.xaxis, switches[:, i] - switches_dev[:, i],
                               switches[:, i] + switches_dev[:, i], alpha=alpha,
                               facecolor=c2[i], edgecolor=c2[i])

        p1 = plt.Rectangle((0, 0), 1, 1, fc=c2[0], ec=c2[0], alpha=alpha)
        p2 = plt.Rectangle((0, 0), 1, 1, fc=c2[1], ec=c2[1], alpha=alpha)
        ax.legend([p1, p2], [legend2[0], legend2[1]],loc='best',
                  fancybox=True, fontsize=legend_fontsize)


        ax.axvline(f.default_val, color='k', linestyle='--')
        ax.set_ylabel('Switches', size=axis_label_fontsize)
        ax.yaxis.set_label_coords(labelx, labely)
        ax.set_xlim(f.xlim)

        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
        if f.ybins is not None:
            ax.locator_params(axis='y', nbins=f.ybins)

    if (f.subplots[2]):
        ax = AX[2]
        reward = p[f.name + '_reward']
        reward_dev = p[f.name + '_reward_dev']
        reward_err = p[f.name + '_reward_err']  # std error


        handle = ax.plot(f.xaxis, reward, color='b', linewidth=linewidth)
        ax.fill_between(f.xaxis, reward - reward_dev,
                           reward + reward_dev, alpha=alpha,
                           facecolor='b', edgecolor='b')
        ax.axvline(f.default_val, color='k', linestyle='--')
        ax.set_ylabel('Reward', size=axis_label_fontsize)
        ax.legend(loc='best', fancybox=True, fontsize=legend_fontsize)
        ax.yaxis.set_label_coords(labelx, labely)
        ax.set_xlim(f.xlim)

        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(axis_tick_fontsize)
        if f.ybins is not None:
            ax.locator_params(axis='y', nbins=f.ybins)

    ax.set_xlabel(f.xlabel, size=axis_label_fontsize)

    plt.savefig(package_address + '/figures/sensitivity_' + f.name + '.pdf',
                 format='pdf', transparent=True, bbox_inches='tight')
