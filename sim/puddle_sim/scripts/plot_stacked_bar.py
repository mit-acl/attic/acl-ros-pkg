#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib import rc
from matplotlib import rcParams
rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
# # for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


def plotArrow(ax, x, y, c):
    ax.annotate('', xy=(i + x[0], y), xycoords='data',
                xytext=(i + x[1], y), textcoords='data',
                arrowprops=dict(width=3, headwidth=6, frac=0.04, color=c))

f = open('/home/mark/acl-ros-pkg/sim/puddle_sim/log/mfrl_example_tab_bar.p', 'rb')
# f = open('/home/mark/acl-ros-pkg/sim/puddle_sim/log/mfrl_12.p', 'rb')

# f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/state_r3_v3_2/mfrl_0.p', 'rb')
# f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/state_r3_v3_uni_2/mfrl_0.p', 'rb')
# f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/state_r3_v2_uni_1/mfrl_0.p', 'rb')
# f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/state_r3_v2_uni_2/mfrl_0.p', 'rb')
# f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/state_r3_v2_dbn_uni_1/mfrl_0.p', 'rb')

# Labels for the ticks on the x axis.  It needs to be the same length
# as y (one label for each bar)
group_labels = [r'$\text{No Puddle~}(\boldsymbol{\Sigma_1})$',
                r'$\text{Some Puddle~}(\boldsymbol{\Sigma_2})$',
                r'$\text{Full Puddle~}(\boldsymbol{\Sigma_3})$']

# group_labels = ['Input Only', 'Dynamics Sim', 'Real Car']

p = pickle.load(f)
# try:
numIter = p["level_iterations"]
level_dir = p["level_directions"]

fig, ax = plt.subplots()
# c = ['r','b','g']
c = np.array([0, 0, 1, 1])
alpha = [1, 1, 1.0]
level_index = [0, 0, 0]
b = 0
dir = 1
i = 0
while dir != 0:
    barlist = ax.bar(i, numIter[i][level_index[i]], bottom=b, color=c,
                     alpha=alpha[i], linewidth=2., align='center')
    dir = level_dir[i][level_index[i]]
#     if dir == -1:
#         plotArrow(ax, [-0.47, 0.4], b + numIter[i][level_index[i]], np.array([0. / 255, 0, 0]))
#     elif dir == 1:
#         plotArrow(ax, [0.47, -0.4], b + numIter[i][level_index[i]], np.array([0, 0. / 255, 0. / 255]))

    b += numIter[i][level_index[i]]
    level_index[i] += 1

    i += dir
    print i


print 'Base level: ' + str(sum(numIter[0]))
print 'Mid level: ' + str(sum(numIter[1]))
print 'Top level: ' + str(sum(numIter[2]))
print

ax.set_ylabel('MFRL Steps', size=20)

# This sets the ticks on the x axis to be exactly where we put
# the center of the bars.
ax.set_xticks(range(3))

# Set the x tick labels to the group_labels defined above.
ax.set_xticklabels(group_labels, size=20)

fig.autofmt_xdate()

ax.set_aspect(1.15/ax.get_data_ratio())

for tick in ax.yaxis.get_major_ticks():
    tick.label.set_fontsize(15)

fig.canvas.draw()
plt.savefig('/home/mark/acl-ros-pkg/sim/puddle_sim/figures/mfrl_stacked_bar.pdf',
          format='pdf', transparent=True , bbox_inches='tight')


plt.show()
