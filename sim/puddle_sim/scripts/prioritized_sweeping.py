#!/usr/bin/env python
'''
Description: Implements the prioritized sweeping algorithm from this book:

http://webdocs.cs.ualberta.ca/~sutton/book/ebook/node98.html

Uses a tabular representation of the transition and reward functions

Created on Nov 12, 2014

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import numpy as np
import heapq
import copy

class PrioritizedSweeping:

    def __init__(self, S, A, U, terminal_states, epsilon=0.1,
                 updates_per_step=100, min_sweep_delta=0.1,
                 gamma=0.95, rand_state=None):

        # seed random number generator
        self.rand_state = rand_state
        if self.rand_state == None:
            self.rand_state = np.random.RandomState()

        self.epsilon = epsilon
        self.updates_per_step = updates_per_step
        self.min_sweep_delta = min_sweep_delta

        # Initialize variables (lines 1 through 8)
        self.Q = np.zeros(U.shape)
        self.r = np.zeros(U.shape)
        self.nr = np.zeros(U.shape)
        self.nt = np.zeros(U.shape)
        self.nt_inv = np.zeros(U.shape)
        self.Rhat = np.zeros(U.shape)
        self.gamma = copy.copy(gamma)
        self.terminal_states = copy.copy(terminal_states)  # set of terminal states, each
        # state is a tuple

        self.S = copy.copy(S)
        self.A = copy.copy(A)
        self.numA = len(A)
        # Use dictionary for n_prime and That because they will be so sparse
        self.n_prime = {}
        self.That = {}
        # Use dictionary for sa -> s' mapping
        self.sa2s_prime = {}
        self.s_primea2s = {}

#-----------------------------------------------------------------------------
# Eps-Greedy Policy
# Given the Q values and a state s, return the action with the best reward with
#    probability 1-epsilon
#    If there are multiple actions with the same reward, randomly return one
#    of them (or random with probability epsilon)
#-----------------------------------------------------------------------------
    def choose_action(self, s):
        if self.rand_state.rand() < self.epsilon:
            # choose random action
            a = np.array([self.A[0][self.rand_state.randint(self.numA)]])
        else:
            # choose best action
            # get Q', the possible state action pairs from which to choose
            Qprime = self.Q[tuple(s)]
    
            # Randomly select among the best actions
            a_index = np.where(Qprime == Qprime.max())  # all possible best actions
    
            a = np.zeros(self.numA, dtype=np.int)  # somewhere to store best action
            rand_ind = self.rand_state.randint(a_index[0].size)

            for i in np.arange(self.numA):
                a[i] = a_index[i][rand_ind]
        return a

#-----------------------------------------------------------------------------
# Update our estimated reward function -- tabular representation
#-----------------------------------------------------------------------------
    def updateReward(self, sa, r):

        self.nr[sa] += 1  # update count of that state action pair
        self.r[sa] += r  # record immediate reward

        # update rhat
        self.Rhat[sa] = float(self.r[sa]) / self.nr[sa]

#-----------------------------------------------------------------------------
# Update our estimated transition dynamics -- tabular representation
#-----------------------------------------------------------------------------
    def updateTransition(self, s, a, s_prime):
        # update tabular That
        sa = s + a
        sas = sa + tuple(s_prime)
        self.nt[sa] += 1
        
        spa = tuple(s_prime) + a
        self.nt_inv[spa] += 1

        # add to the sa -> s' dictionary.  If an element already exists,
        # append the new element to an array
        if sa in self.sa2s_prime:
            s_prime_list = self.sa2s_prime[sa]
            if tuple(s_prime) not in s_prime_list:
                self.sa2s_prime[sa].append(tuple(s_prime))
        else:
            self.sa2s_prime[sa] = [tuple(s_prime)]
            
        # add to the s'a -> s dictionary.  If an element already exists,
        # append the new element to an array
        if spa in self.s_primea2s:
            s_list = self.s_primea2s[spa]
            if s not in s_list:
                self.s_primea2s[spa].append(s)
        else:
            self.s_primea2s[spa] = [s]

        # update immediate next-state (using dict)
        if sas in self.n_prime:
            self.n_prime[sas] += 1
        else:
            self.n_prime[sas] = 1

        # update That
        s_prime_list = self.sa2s_prime[sa]
        for s_prime_iter in s_prime_list:
            sas_prime = sa + s_prime_iter
            # update That
            self.That[sas_prime] = float(self.n_prime[sas_prime]) / self.nt[sa]


#-----------------------------------------------------------------------------
# Run prioritized sweeping
# Main computation loop: should be a separate thread or action
# note, code based heavily on code from http://sourceforge.net/projects/mmlf/?source=typ_redirect
    def plan(self, s, a):

        heap = []
        openPairs = dict()
        closedPairs = set()
        
        sa = s + a
        P = self.full_backup(sa)
        delta = np.abs(P - self.Q[sa])
        
        # If the change is above a threshold
        if delta > self.min_sweep_delta:
            # We add the state, action pair to the priority queue with
            # priority -delta (Min heap!)
            heapq.heappush(heap, (-delta, sa))
            openPairs[sa] = -delta
                    
        # We perform self.updatesPerStep updates of state-action pairs 
        for i in range(self.updates_per_step):
            # If the heap is empty, we stop prioritized sweeping
            if len(heap) == 0:
                #print "num_updates: " + str(i)
                break
            
            # Get the next state action pair that should be update
            delta, sa = heapq.heappop(heap)
            openPairs.pop(sa, None)
            closedPairs.add(sa)

            # Compute target (for the first iteration, use the externally 
            # provided reward)
            if i != 0:
                P = self.full_backup(sa)
            
            self.Q[sa] = P

            # For all actions
            for a_pred in self.A[0]: #WARNING: TODO: this assumes just one action
                # Determine the predecessor states of state under predAction
                # loop over dictionary of s'a -> s
                # treat s as s' now
                a_pred = tuple([a_pred])
                sa_pred = s + a_pred
                if sa_pred in self.s_primea2s:
                    s_list = self.s_primea2s[sa_pred]
                    for s_pred in s_list:
                        sas = s_pred + a_pred + s
                        spap = s_pred + a_pred
                
                        # States that are already in the queue are ignored
                        if spap in openPairs:
                            continue                        
                        
                        # States that have already been updated
                        if spap in closedPairs:
                            continue
                        
                        ## TODO: maybe need to check for terminal states here (?)
                        
                        # Compute the estimated change of this state
                        P = self.full_backup(spap)
                        delta = np.abs(P - self.Q[spap])
                        
                        # If the change is above a threshold
                        if delta > self.min_sweep_delta:
                            # We add the predecessor state, action pair to the priority 
                            # queue with priority -delta (Min heap!)      
                            heapq.heappush(heap, (-delta, spap))
                            openPairs[spap] = -delta


    def full_backup(self, sa):
        P = 0
        # loop over dictionary of sa -> s'
        if sa in self.sa2s_prime:
            s_prime_list = self.sa2s_prime[sa]
            for s_prime_iter in s_prime_list:
                sas_prime = sa + s_prime_iter
                P += self.That[sas_prime] * self.Q[tuple(s_prime_iter)].max()

        # update Q (line 21 of alg. in paper)
        P = self.Rhat[sa] + self.gamma * P
        return P

