#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 13, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import cPickle as pickle
import copy
from scipy import stats
from matplotlib import rc
from matplotlib import rcParams

class Figure:
    def __init__(self):
        self.aspect = None
        self.mean_r = []
        self.std_dev = []
        self.std_err = []
        self.x_axis = []
        self.opt_values = None
        self.learning_runs = 0

def process_reward(r):
    learning_runs = len(r)
    if learning_runs > 1000:
        learning_runs = 1000
    maxLen = 0
    minLen = np.Inf
    print "Learning runs: " + str(learning_runs)
    for i in np.arange(learning_runs):
        tmpL = len(r[i])
        # print "tmpL: " + str(tmpL)
        if tmpL > maxLen:
            maxLen = tmpL
        if tmpL < minLen:
            minLen = tmpL
    cum_reward = np.zeros((learning_runs, maxLen))
    print "Num Q: " + str(maxLen)
    for i in np.arange(learning_runs):
        specificQ = maxLen  # len(r[i])
        for j in np.arange(specificQ):
             try:
                 trials_of_Q = len(r[i][j])
                 avg_r = 0
                 for k in np.arange(trials_of_Q):
                     avg_r += r[i][j][k][0]
                 avg_r /= trials_of_Q
                 cum_reward[i][j] = copy.copy(avg_r)
             except:
                 cum_reward[i][j] = cum_reward[i][len(r[i]) - 1]  # copy last value

    return cum_reward, specificQ, learning_runs

def main_fnc():
    package_address = roslib.packages.get_pkg_dir('puddle_sim')
    folder = '/log/'

    # figure 1
    f1 = Figure()
    f1.name = 'tabular'
    f1.file = [folder + 'total_reward_1000.p',
            folder + 'total_reward_uni_1000.p',
            folder + 'total_reward_just_top_1000.p',
            folder + 'total_reward_ps_eps1_1000.p',
            folder + 'total_reward_levels13_1000.p',
            folder + 'total_reward_levels23_1000.p',
            folder + 'total_reward_levels132_1000.p']

    # figure 2
    f2 = Figure()
    f2.name = 'dbn'
    f2.file = [folder + 'total_reward_1000.p',
            folder + 'total_reward_dbn_1000.p',
            folder + 'total_reward_uni_1000.p',
            folder + 'total_reward_dbn_uni_1000.p']

    # figure 3
    f3 = Figure()
    f3.name = 'gen'
    f3.file = [folder + 'total_reward_1000.p',
            folder + 'total_reward_gen_1000.p',
            folder + 'total_reward_dbn_1000.p',
            folder + 'total_reward_dbn_gen_1000.p']

    # figure 4
    f4 = Figure()
    f4.name = 'levels'
    f4.file = [folder + 'total_reward_levels132_1000.p',
            folder + 'total_reward_levels321_1000.p',
            folder + 'total_reward_levels21_1000.p',
            folder + 'total_reward_levels12_1000.p',
            folder + 'total_reward_levels1_1000.p',
            folder + 'total_reward_levels2_1000.p',
            folder + 'total_reward_just_top_1000.p']

    # figure 5
    f5 = Figure()
    f5.name = 'ps'
    f5.file = [folder + 'total_reward_ps_eps2_1000.p',
            folder + 'total_reward_ps_eps1_1000.p',
            folder + 'total_reward_ps_eps15_1000.p']



    figs = [f1, f2, f3, f4, f5]

    for f in figs:

        opt_values = np.zeros(len(f.file))
        for ii in np.arange(len(f.file)):
            reward_file = open(package_address + f.file[ii], 'rb')

            print package_address + f.file[ii]
            p = pickle.load(reward_file)

            cum_reward, specificQ, learning_runs = process_reward(p['reward_list'])

            # calculate mean and standard error
            mean_r = np.mean(cum_reward, 0)
            std_dev = np.std(cum_reward, 0)
            std_err = stats.sem(cum_reward, 0)
            # print mean_r
            # print std_err

            x = np.arange(0, specificQ * 100, 100)

            # get converged optimal policy
            opt_values[ii] = mean_r[-1]

            f.mean_r.append(mean_r)
            f.std_dev.append(std_dev)
            f.std_err.append(std_err)
            f.x_axis.append(x)

        f.opt_values = opt_values
        f.learning_runs = learning_runs

    processed_data_file = open(package_address + '/scripts/policy_data.p', 'wb')
    pickle.dump(figs,processed_data_file)


if __name__ == "__main__":
    main_fnc()
