#!/usr/bin/env python
'''
Description: Process and save sensitivity analysis data

Created on Jan 9, 2014

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import numpy as np
import cPickle as pickle
from scipy import stats
import plotPolicy

package_address = roslib.packages.get_pkg_dir('puddle_sim')

# input folder
# folder = '/log/5times_neg_reward/sensitivity_analysis/total_reward_'
#folder = '/log/3times_neg_reward/sensitivity_analysis/total_reward_'
#folder = '/log/2times_neg_reward/sensitivity_analysis/total_reward_'
folder = '/log/sensitivity_analysis/total_reward_'

# output folder
sensitivity_data = open(package_address +
                        '/log/sensitivity_analysis/sensitivity_data.p', 'wb')


class Sweep:
    pass

# beta sweep
s1 = Sweep()
s1.name = 'beta'
s1.file = ['beta_0p0_1000.p',
           'beta_0p5_1000.p',
           'beta_1p0_1000.p',
           'beta_1p5_1000.p',
           'beta_2p0_1000.p',
           'beta_3p0_1000.p'] #,
           # 'beta_4p0_1000.p',
           # 'beta_5p0_1000.p',
           # 'beta_6p0_1000.p',
           # 'beta_7p0_1000.p',
           # 'beta_8p0_1000.p',
           # 'beta_9p0_1000.p',
           #'beta_10p0_1000.p']
s1.mean_samples = np.zeros((len(s1.file), 3))
s1.std_dev_samples = np.zeros((len(s1.file), 3))
s1.std_err_samples = np.zeros((len(s1.file), 3))
s1.mean_switches = np.zeros((len(s1.file), 2))
s1.std_dev_switches = np.zeros((len(s1.file), 2))
s1.std_err_switches = np.zeros((len(s1.file), 2))
s1.mean_reward = np.zeros(len(s1.file))
s1.std_dev_reward = np.zeros(len(s1.file))
s1.std_err_reward = np.zeros(len(s1.file))

# mknown sweep
s2 = Sweep()
s2.name = 'mknown'
s2.file = ['mknown_5_1000.p',
           'mknown_15_1000.p',
           'mknown_30_1000.p',
           'mknown_45_1000.p',
           'mknown_60_1000.p',
           'mknown_75_1000.p',
           'mknown_90_1000.p',
           'mknown_105_1000.p',
           'mknown_120_1000.p']
s2.mean_samples = np.zeros((len(s2.file), 3))
s2.std_dev_samples = np.zeros((len(s2.file), 3))
s2.std_err_samples = np.zeros((len(s2.file), 3))
s2.mean_switches = np.zeros((len(s2.file), 2))
s2.std_dev_switches = np.zeros((len(s2.file), 2))
s2.std_err_switches = np.zeros((len(s2.file), 2))
s2.mean_reward = np.zeros(len(s2.file))
s2.std_dev_reward = np.zeros(len(s2.file))
s2.std_err_reward = np.zeros(len(s2.file))

# munknown sweep
s3 = Sweep()
s3.name = 'munknown'
s3.file = ['munknown_1_1000.p',
           'munknown_5_1000.p',
           'munknown_10_1000.p',
           'munknown_15_1000.p',
           'munknown_20_1000.p',
           'munknown_25_1000.p',
           'munknown_30_1000.p',
           'munknown_40_1000.p']
s3.mean_samples = np.zeros((len(s3.file), 3))
s3.std_dev_samples = np.zeros((len(s3.file), 3))
s3.std_err_samples = np.zeros((len(s3.file), 3))
s3.mean_switches = np.zeros((len(s3.file), 2))
s3.std_dev_switches = np.zeros((len(s3.file), 2))
s3.std_err_switches = np.zeros((len(s3.file), 2))
s3.mean_reward = np.zeros(len(s3.file))
s3.std_dev_reward = np.zeros(len(s3.file))
s3.std_err_reward = np.zeros(len(s3.file))

sweep = [s1, s2, s3]
for s in sweep:
    for k in range(len(s.file)):
        f = open(package_address + folder +
                 s.file[k], 'rb')
        p = pickle.load(f)

        numIter = p["level_sample_list"]

        # process reward information first
        cum_reward, specificQ, learning_runs = plotPolicy.process_reward(p['reward_list'])

        # calculate mean and standard error
        mean_r = np.mean(cum_reward, 0)
        std_dev = np.std(cum_reward, 0)
        std_err = stats.sem(cum_reward, 0)
        s.mean_reward[k] = mean_r[-1]
        s.std_dev_reward[k] = std_dev[-1]
        s.std_err_reward[k] = std_err[-1]

        learning_runs = len(numIter[0])
        if learning_runs > 1000:
            learning_runs = 1000

        samples = np.zeros((3, learning_runs))
        switches = np.zeros((2, learning_runs))
        for j in np.arange(learning_runs):
            for i in np.arange(3):
                samples[i, j] = sum(numIter[i][j])
            switches[0, j] = 2 * len(numIter[0][j]) - 1  # switches from level 0 to 1 (switches
                            # up and back down (hence 2), except for start)
            switches[1, j] = 2 * len(numIter[2][j]) - 1  # 2 switches execpt for end
            if sum(switches[:, j]) != 2 * len(numIter[1][j]):
                print "Error in counting number of switches!"

        mean_samples = np.mean(samples, 1)
        std_dev_samples = np.std(samples, 1)
        std_err_samples = stats.sem(samples, 1)

        print 'samples: ' + str(mean_samples)
        print 'std_dev: ' + str(std_dev_samples)
        print 'std_err: ' + str(std_err_samples)

        mean_switches = np.mean(switches, 1)
        std_dev_switches = np.std(switches, 1)
        std_err_switches = stats.sem(switches, 1)

        print 'switches: ' + str(mean_switches)
        print 'std_dev: ' + str(std_dev_switches)
        print 'std_err: ' + str(std_err_switches)

        s.mean_samples[k, :] = mean_samples
        s.std_dev_samples[k, :] = std_dev_samples
        s.std_err_samples[k, :] = std_err_samples
        s.mean_switches[k, :] = mean_switches
        s.std_dev_switches[k, :] = std_dev_switches
        s.std_err_switches[k, :] = std_err_switches

data = {"beta_samples":s1.mean_samples,
        "beta_samples_dev":s1.std_dev_samples,
        "beta_samples_err":s1.std_err_samples,
        "beta_switches":s1.mean_switches,
        "beta_switches_dev":s1.std_dev_switches,
        "beta_switches_err":s1.std_err_switches,
        "beta_reward":s1.mean_reward,
        "beta_reward_dev":s1.std_dev_reward,
        "beta_reward_err":s1.std_err_reward,
        "mknown_samples":s2.mean_samples,
        "mknown_samples_dev":s2.std_dev_samples,
        "mknown_samples_err":s2.std_err_samples,
        "mknown_switches":s2.mean_switches,
        "mknown_switches_dev":s2.std_dev_switches,
        "mknown_switches_err":s2.std_err_switches,
        "mknown_reward":s2.mean_reward,
        "mknown_reward_dev":s2.std_dev_reward,
        "mknown_reward_err":s2.std_err_reward,
        "munknown_samples":s3.mean_samples,
        "munknown_samples_dev":s3.std_dev_samples,
        "munknown_samples_err":s3.std_err_samples,
        "munknown_switches":s3.mean_switches,
        "munknown_switches_dev":s3.std_dev_switches,
        "munknown_switches_err":s3.std_err_switches,
        "munknown_reward":s3.mean_reward,
        "munknown_reward_dev":s3.std_dev_reward,
        "munknown_reward_err":s3.std_err_reward}
pickle.dump(data, sensitivity_data)
sensitivity_data.close()
