#!/usr/bin/env python
'''
Description: Prioritized sweeping in the puddle world domain

Created on Nov 13, 2014

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
from __future__ import division
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import sets
import matplotlib.pyplot as plt
import cPickle as pickle
import copy
import signal
import sys
import os


import prioritized_sweeping as ps

import PuddleWorld as pw
import PuddleWorldGrid as pwg
from puddle_sim.msg import State


class PSPuddle():

    def __init__(self, epsilon=0.15, updates_per_step=5000, min_sweep_delta=0.1, 
                 gamma=0.95, showDomain=True, grid_world=False, logFileIndex=0):

        # seed random number generator
        #self.rand_state = np.random.RandomState(1)
        self.rand_state = None

        self.showDomain = showDomain

        # set up puddle world (always use full world)
        if grid_world:
            puddle_noise = 0.2 / 3
            self.pw = pwg.PuddleWorldGrid(puddle_noise, 2, self.rand_state)
        else:
            puddle_noise = 0.02
            self.pw = pw.PuddleWorld(puddle_noise, 2, self.rand_state)

        self.reward_fig = None
        self.q_fig = None
        plt.ion()

        # variables specific to puddle world
        self.package_address = roslib.packages.get_pkg_dir('puddle_sim')
        self.bestQ = []  # save best Q for highest level every few iterations
        self.bestQ_samples = []  # real world sample number at which Q was saved
        self.saveQRate = 100  # save Q at top level every this many environment interactions
        self.logFile = self.openLogFile(logFileIndex)

        # State and action space discretization
        self.numX = 10
        self.numY = 10
        self.numA = 4

        # create the discretized states and actions
        self.xbins = np.linspace(0, 1, self.numX, False)
        self.ybins = np.linspace(0, 1, self.numY, False)
        self.xbins = self.xbins[1:]  # has to do with how np.digitize assigns bins
        self.ybins = self.ybins[1:]

        # initialize prioritized sweeping algorithm
        S = [np.arange(self.numX), np.arange(self.numY)]
        A = [np.arange(self.numA)]
        U = np.zeros((self.numX, self.numY, self.numA))

        self.ts = self.getTerminalStates()

        self.ps = ps.PrioritizedSweeping(S, A, U, self.ts, 
                                         epsilon, updates_per_step, 
                                         min_sweep_delta, gamma, self.rand_state)

    def runPS(self, num_steps=4000):
        
        episode_num = 0
        self.s = self.initState()
        for i in range(num_steps):
            a = self.ps.choose_action(self.cont2discState(self.s))

            # execute action in current state                        
            ns, r, t = self.getNextState(self.s, a)
            
            # save Q
            if np.mod(i, self.saveQRate) == 0:
                self.bestQ.append(copy.copy(self.ps.Q))
                self.bestQ_samples.append(i)
            
            # get state-action tuple
            a = tuple(a)
            s = tuple(self.cont2discState(self.s))
            sa = s + a
            s_prime = self.cont2discState(ns)
            
            # update model
            self.ps.updateReward(sa, r)
            self.ps.updateTransition(s, a, s_prime)
            
            # plan
            self.ps.plan(s, a)
            
            if i % 100 == 0:
                print "iterations:" + str(i)
            
            # check for terminal states
            if t:
                self.s = self.initState()
                episode_num += 1
                print episode_num
            else:
                self.s = ns
                
        self.saveLog()
        self.logFile.close()
        return self.logFile.name
                
                
#-----------------------------------------------------------------------------
# Open log files -- save in /package_path/log/
# File will have current time tacked onto it so we don't overwrite other files
#-----------------------------------------------------------------------------
    def openLogFile(self, logFileIndex):
        if logFileIndex < 0 :
            # use time as a random file extender
            logFileIndex = int(rospy.get_time() * 10000)
        # get the file path for puddle_sim
        logfile_dir = self.package_address + '/log/'
        if not os.path.exists(logfile_dir):
            os.makedirs(logfile_dir)
        self.logFileAddress = logfile_dir + 'mfrl_' + str(logFileIndex) + '.p'
        return open(self.logFileAddress, 'wb')
    
#-----------------------------------------------------------------------------
# Set initial state
#-----------------------------------------------------------------------------
    def initState(self):


        s = State(0.05, 0.05)
        # s.x += np.random.randn() * 0.03
        # s.y += np.random.randn() * 0.03
        if s.x < 0.0:
            s.x = 0.0
        if s.y < 0.0:
            s.y = 0.0
        return s

#-----------------------------------------------------------------------------
# Reset the state
#-----------------------------------------------------------------------------
    def resetState(self, a):
        pass

#-----------------------------------------------------------------------------
# Run a single step
#-----------------------------------------------------------------------------
    def getNextState(self, s, a, showDomain=None):
        if showDomain == None:
            showDomain = self.showDomain
        sp, r, term = self.pw.puddleStep(np.array([s.x, s.y]), a, showDomain)
        return State(sp[0], sp[1]), r, term

#-----------------------------------------------------------------------------
# Set a set of terminal states
#-----------------------------------------------------------------------------
    def getTerminalStates(self):
        ts = sets.Set([])

        # construct set of terminal states
        for i in np.arange(self.numX):
            for j in np.arange(self.numY):
                s = self.disc2contState(np.array([i, j]))
                sp, r, t = self.getNextState(s, -1, False)
                if t:
                    ts.add((i, j))

        return ts

#-----------------------------------------------------------------------------
# converts 2d continuous states to discrete
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def cont2discState(self, cs):
#         print "xbins"
#         print self.xbins
#         print cs
        x = np.digitize(np.array([cs.x]), self.xbins)
        y = np.digitize(np.array([cs.y]), self.ybins)
        return np.array([x[0], y[0]])

#-----------------------------------------------------------------------------
# Save Q
#-----------------------------------------------------------------------------
    def saveLog(self):
        # save pertinent data
        self.logFile.seek(0)
        log = {"bestQ":self.bestQ,
               "bestQ_samples":self.bestQ_samples}
        pickle.dump(log, self.logFile)


##############
# New functions only for puddle-world
##############

#-----------------------------------------------------------------------------
# Evaluate policy Q m times to get an estimate of how many runs it takes to get to the goal
#-----------------------------------------------------------------------------

    def evalQ(self, logFileIndex, bestQ=None):

        if bestQ == None:
            f = open(self.logFileAddress, 'rb')
            p = pickle.load(f)
            bestQ = p["bestQ"]

        numTrials = 60
        numQ = len(bestQ)
        maxSteps = 600

        reward = np.zeros((numQ, numTrials, 2))
        rospy.loginfo("There are " + str(numQ) + " sets of Q values")
        for i in range(numQ):
            # get Q values
            rospy.loginfo("Starting Q value " + str(i))
            self.ps.Q = copy.copy(bestQ[i])
            self.ps.epsilon = 0.0 # use a greedy policy for evaluation purposes
#            print "newQ"
#            print reward
            for j in range(numTrials):
                self.s = self.initState()  # self.init_state
                reward_sum = 0
                for k in range(maxSteps):
                    a = self.ps.choose_action(self.cont2discState(self.s))
                    ns, r, t = self.getNextState(self.s, a)
                    reward_sum += r

                    self.s = ns
                    if tuple(self.cont2discState(self.s)) in self.ts:
                        break
                reward[i, j, 0] = reward_sum
                reward[i, j, 1] = k
                rospy.loginfo("Trial " + str(j) + ": " + str(k) + " steps, " + str(reward_sum) + " reward")

#         pickle.dump(reward, fout)
#         fout.close()
        return reward

#-----------------------------------------------------------------------------
# converts 2d discrete states to continuous (using state centers)
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def disc2contState(self, ds):
        x = float(ds[0]) / self.numX + 1. / self.numX / 2.
        y = float(ds[1]) / self.numY + 1. / self.numY / 2.
        return State(x, y)


if __name__ == "__main__":
    try:
        rospy.init_node('ps_puddle', anonymous=True)
        argv = rospy.myargv(argv=sys.argv)
        if len(argv) == 2:
            if argv[1] in ['False', 'false', '0', 'no', 'not']:
                c = PSPuddle(showDomain=False)
            else:
                c = PSPuddle(showDomain=True)
        else:
            c = PSPuddle()
        self.runPS()

    except rospy.ROSInterruptException:
        pass
