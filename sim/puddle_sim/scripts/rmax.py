#!/usr/bin/env python
'''
Description: Implements the rmax algorithm from this paper:

Algorithm 1
Reinforcement Learning in Finite MDPs: PAC Analysis
http://research.microsoft.com/pubs/178881/strehl09a.pdf

Uses a tabular representation of the transition and reward functions

Created on Aug 26, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import numpy as np
import copy

class Rmax:

    def __init__(self, S, A, U, terminal_states, logFile=None, savedLog=None,
                 maxDelta=0.000001,
                 gamma=0.95, mr=5, mt=5, rand_state=None):

        # seed random number generator
        self.rand_state = rand_state
        if self.rand_state == None:
            self.rand_state = np.random.RandomState()

        # Initialize variables (lines 1 through 8)
        self.Q = copy.copy(U)
        self.r = np.zeros(U.shape)
        self.nr = np.zeros(U.shape)
        self.nt = np.zeros(U.shape)
        self.Rhat = np.zeros(U.shape)
        self.gamma = copy.copy(gamma)
        self.mr = copy.copy(mr)
        self.mt = copy.copy(mt)
        self.terminal_states = copy.copy(terminal_states)  # set of terminal states, each
        # state is a tuple
        self.maxDelta = copy.copy(maxDelta)

        self.S = copy.copy(S)
        self.A = copy.copy(A)
        self.numS = len(S)
        self.numA = len(A)
        # Use dictionary for n_prime and That because they will be so sparse
        self.n_prime = {}
        self.That = {}
        # Use dictionary for sa -> s' mapping
        self.sa2s_prime = {}

        # if applicable, load log file
        if savedLog != None:
            self.loadData(savedLog)

        # set up log file for saving pertinent data
        if logFile == None:
            self.logFile = None
        else:
            self.logFile = open(logFile, 'w')

#-----------------------------------------------------------------------------
# Given the Q values and a state s, return the action with the best reward.
#    If there are multiple actions with the same reward, randomly return one
#    of them.
#-----------------------------------------------------------------------------
    def choose_action(self, s):

        # get Q', the possible state action pairs from which to choose
        Qprime = self.Q[tuple(s)]

        # Randomly select among the best actions
        a_index = np.where(Qprime == Qprime.max())  # all possible best actions

        a = np.zeros(self.numA, dtype=np.int)  # somewhere to store best action
        rand_ind = self.rand_state.randint(a_index[0].size)
        #print rand_ind
        for i in np.arange(self.numA):
            a[i] = a_index[i][rand_ind]
        if Qprime[tuple(a)] > Qprime.max():
            pass
        return a

#-----------------------------------------------------------------------------
# Update our estimated reward function -- tabular representation
#-----------------------------------------------------------------------------
    def updateReward(self, sa, r):

        self.nr[sa] += 1  # update count of that state action pair
        self.r[sa] += r  # record immediate reward

        # update rhat
        if self.nr[sa] >= self.mr:
            self.Rhat[sa] = float(self.r[sa]) / self.nr[sa]

#-----------------------------------------------------------------------------
# Update our estimated transition dynamics -- tabular representation
#-----------------------------------------------------------------------------
    def updateTransition(self, sa, s_prime):
        # update tabular That
        sas = sa + tuple(s_prime)
        self.nt[sa] += 1

        # add to the sa -> s' dictionary.  If an element already exists,
        # append the new element to an array
        if sa in self.sa2s_prime:
            s_prime_list = self.sa2s_prime[sa]
            if tuple(s_prime) not in s_prime_list:
                self.sa2s_prime[sa].append(tuple(s_prime))
        else:
            self.sa2s_prime[sa] = [tuple(s_prime)]

        # update immediate next-state (using dict)
        if sas in self.n_prime:
            self.n_prime[sas] += 1
        else:
            self.n_prime[sas] = 1

        # update That
        s_prime_list = self.sa2s_prime[sa]
        for s_prime_iter in s_prime_list:
            sas_prime = sa + s_prime_iter
            # update That
            self.That[sas_prime] = float(self.n_prime[sas_prime]) / self.nt[sa]

#-----------------------------------------------------------------------------
# Run value iteration
# Main computation loop: should be a separate thread or action
    def valIter(self):

        maxDelta = 0
        for i in np.arange(5000):

            maxDelta = self.updateQ(i, maxDelta)

            if maxDelta < self.maxDelta:  # stopping condition -- nothing is changing
                break
            maxDelta = 0

        if i > 0:
            self.saveData()
        return i

#-----------------------------------------------------------------------------
# Update Q^
#-----------------------------------------------------------------------------
    def updateQ(self, i, maxDelta):
        for sa_index, n in np.ndenumerate(self.nr):
            if sa_index[0:self.numS] in self.terminal_states:
                self.Q[sa_index] = 0  # episodic version -- no reward in goal

            elif self.stateActionKnown(sa_index):
                # calc expectation of Q
                expectQ = 0
                # loop over dictionary of sa -> s'
                if sa_index in self.sa2s_prime:
                    s_prime_list = self.sa2s_prime[sa_index]
                    for s_prime_iter in s_prime_list:
                        sas_prime = sa_index + s_prime_iter
                        expectQ += self.That[sas_prime] * self.Q[tuple(s_prime_iter)].max()

                # update Q (line 21 of alg. in paper)
                tmp1 = self.Rhat[sa_index] + self.gamma * expectQ
                delta = np.abs(self.Q[sa_index] - tmp1)
                if delta > maxDelta:
                    maxDelta = copy.copy(delta)
                self.Q[sa_index] = copy.copy(tmp1)

        return maxDelta

#-----------------------------------------------------------------------------
# Quick method for determining if reward is known
#-----------------------------------------------------------------------------
    def rewardKnown(self, sa):
        return self.nr[sa] >= self.mr

#-----------------------------------------------------------------------------
# Quick method for determining if reward just became known
#-----------------------------------------------------------------------------
    def rewardKnownExact(self, sa):
        return self.nr[sa] == self.mr

#-----------------------------------------------------------------------------
# Quick method for determining if transition is known
#-----------------------------------------------------------------------------
    def transitionKnown(self, sa):
        return self.nt[sa] >= self.mt

#-----------------------------------------------------------------------------
# Quick method for determining if transition just became known
#-----------------------------------------------------------------------------
    def transitionKnownExact(self, sa):
        return self.nt[sa] == self.mt

#-----------------------------------------------------------------------------
# Quick method for determining if all the factors are known for a specific s,a
#-----------------------------------------------------------------------------
    def stateActionKnown(self, sa):
        return self.rewardKnown(sa) and self.transitionKnown(sa)

#-----------------------------------------------------------------------------
# Save pertinent data to file.  Data structs are saved as npz files
# Get's called every time Q is updated
#-----------------------------------------------------------------------------
    def saveData(self, eps_d=0):
        pass
#         if self.logFile != None:
#             self.logFile.seek(0)
#             np.savez(self.logFile, Q=self.Q, nr=self.nr, nt=self.nt, r=self.r, Rhat=self.Rhat,
#                      That=self.That, gamma=self.gamma, mr=self.mr, mt=self.mt,
#                      n_prime=self.n_prime, sa2s_prime=self.sa2s_prime, eps_d=eps_d)

#-----------------------------------------------------------------------------
# Load pertinent data from a file
#-----------------------------------------------------------------------------
    def loadData(self, logFile):
        l = open(logFile, 'r')
        try:
            npzfile = np.load(l)
            self.Q = npzfile['Q']
            self.n = npzfile['n']
            self.r = npzfile['r']
            self.Rhat = npzfile['Rhat']
            self.That = npzfile['That']
            self.gamma = npzfile['gamma']
            self.mr = npzfile['mr']
            self.mt = npzfile['mt']
            self.n_prime = npzfile['n_prime']
            self.sa2s_prime = npzfile['sa2s_prime']
            self.eps_d = npzfile['eps_d']

            # temporary hack until I start using pickle
            self.That = self.That.reshape(-1)[0]
            self.n_prime = self.n_prime.reshape(-1)[0]
            self.sa2s_prime = self.sa2s_prime.reshape(-1)[0]
        except Exception:
            print 'File ' + logFile + ' could not be opened'
