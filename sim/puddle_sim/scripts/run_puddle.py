#!/usr/bin/env python
'''
Description: Run the rmax_puddle algorithm multiple times for an evaluation phase.

Created on Sept 12, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import mfrl_puddle as mfrl
import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
import copy
import raven_utils
import sys
from std_msgs.msg import Float64MultiArray

from puddle_sim.msg import Reward

def runMFRL(numruns, unidirectional, generative_access, useDBN, levels,
            beta1, beta2, mKnown, mUnknown, real_world_samples):
    file_names = []
    for i in range(numruns):
        c = mfrl.MFRLPuddle(logFileIndex= -1, unidirectional=unidirectional,
                            generative_access=generative_access, useDBN=useDBN,
                            levels=levels, beta=[beta1, beta2],
                            mKnown=[mKnown, mKnown, mKnown],
                            mUnknown=[mUnknown, mUnknown],
                            showDomain=False)
        fn = c.runMFRL(real_world_samples)
        file_names.append(fn)
    
    runEvals(file_names, c)

def runEvals(file_names, c):
    maxLen = 30  # at least 30*100 = 3,000 steps
    # first, get each learning run to have the same number of Q values
    for i in np.arange(numruns):
        f = open(file_names[i], 'rb')
        p = pickle.load(f)
        bestQ = p["bestQ"]
        if len(bestQ) > maxLen:
            maxLen = len(bestQ)

    reward_pub = rospy.Publisher('puddle_reward', Reward)
    for i in np.arange(numruns):
        f = open(file_names[i], 'rb')
        p = pickle.load(f)
        level_samples = p["level_iterations"]
        bestQ = p["bestQ"]
        for j in np.arange(len(bestQ), maxLen):
            bestQ.append(bestQ[-1])

        # now run the evaluation phase
        print "running eval phase for learning iteration " + str(i)
        
        r = c.evalQ(i, bestQ)
        ma = Float64MultiArray()
        raven_utils.numpyArray2MultiArray(r, ma)
        reward_msg = Reward()
        reward_msg.reward = ma
        try:
            reward_msg.level1_samples = level_samples[0]
        except:
            pass
        try:
            reward_msg.level2_samples = level_samples[1]
        except:
            pass
        try:
            reward_msg.level3_samples = level_samples[2]
        except:
            pass
        reward_pub.publish(reward_msg)

if __name__ == "__main__":
    rospy.init_node('run_puddle')
    #######################
    # default user inputs
    numruns = 1  # 50
    unidirectional = False
    generative_access = False
    useDBN = False
    levels = [1,2,3]
    beta1 = 0.0
    beta2 = 0.0
    mKnown = 75
    mUnknown = 20
    real_world_samples = 0
    #######################

    # process user inputs
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) == 11:
        numruns = int(argv[1])
        if argv[2] in ['True', 'true', '1']:
            unidirectional = True
        if argv[3] in ['True', 'true', '1']:
            generative_access = True
        if argv[4] in ['True', 'true', '1']:
            useDBN = True
        levels = eval(argv[5])
        beta1 = float(argv[6])
        beta2 = float(argv[7])
        mKnown = int(argv[8])
        mUnknown = int(argv[9])
        real_world_samples = int(argv[10])

    package_address = roslib.packages.get_pkg_dir('puddle_sim')
    node_name = rospy.names.get_name()
    rospy.loginfo("node name: " + node_name)
    rospy.loginfo("Starting MFRL with\nnumruns: " + str(numruns) +
                  "\nunidirectional: " + str(unidirectional) +
                  "\ngenerative_access: " + str(generative_access) +
                  "\nuseDBN: " + str(useDBN) +
                  "\nlevels: " + str(levels))

    runMFRL(numruns, unidirectional, generative_access, useDBN,
                         levels, beta1, beta2, mKnown, mUnknown, real_world_samples)
