#!/usr/bin/env python
'''
Description: Run the rmax_puddle algorithm multiple times for an evaluation phase.

Created on Sept 12, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import ps_puddle as psp
import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
import copy
import raven_utils
import sys
from std_msgs.msg import Float64MultiArray

#import ps_grid as psp

from puddle_sim.msg import Reward

def runPS(numruns, epsilon, updates_per_step, min_sweep_delta,
          gamma, grid_world, num_steps):
    file_names = []
    for i in range(numruns):
        c = psp.PSPuddle(logFileIndex=-1, epsilon=epsilon,
                         updates_per_step=updates_per_step,
                         min_sweep_delta=min_sweep_delta,
                         gamma=gamma, grid_world=grid_world,
                         showDomain=False)
        fn = c.runPS(num_steps=num_steps)
        file_names.append(fn)
    
    runEvals(file_names, c)

def runEvals(file_names, c):
    maxLen = 30  # at least 30*100 = 3,000 steps
    # first, get each learning run to have the same number of Q values
    print file_names
    for i in np.arange(numruns):
        print file_names[i]
        f = open(file_names[i], 'rb')
        p = pickle.load(f)
        bestQ = p["bestQ"]
        if len(bestQ) > maxLen:
            maxLen = len(bestQ)

    reward_pub = rospy.Publisher('puddle_reward', Reward, queue_size=50)
    for i in np.arange(numruns):
        f = open(file_names[i], 'rb')
        p = pickle.load(f)
        bestQ = p["bestQ"]
        for j in np.arange(len(bestQ), maxLen):
            bestQ.append(bestQ[-1])

        # now run the evaluation phase
        print "running eval phase for learning iteration " + str(i)
            
        r = c.evalQ(i, bestQ)
        ma = Float64MultiArray()
        raven_utils.numpyArray2MultiArray(r, ma)
        reward_msg = Reward()
        reward_msg.reward = ma
        reward_pub.publish(reward_msg)

if __name__ == "__main__":
    rospy.init_node('run_puddle_ps')
    #######################
    # default user inputs
    numruns = 1  # 50
    epsilon = 0.1
    updates_per_step = 500
    min_sweep_delta = 0.001
    gamma = 0.95
    grid_world = False
    num_steps = 4000
    #######################

    # process user inputs
    argv = rospy.myargv(argv=sys.argv)
    if len(argv) == 7:
        numruns = int(argv[1])
        epsilon = float(argv[2])
        updates_per_step = int(argv[3])
        min_sweep_delta = float(argv[4])
        gamma = float(argv[5])
        if argv[6] in ['True', 'true', '1']:
            grid_world = True
        #num_steps = int(argv[7])

    package_address = roslib.packages.get_pkg_dir('puddle_sim')
    node_name = rospy.names.get_name()
    rospy.loginfo("node name: " + node_name)
    rospy.loginfo("Starting PSP with\nnumruns: " + str(numruns) +
                  "\nepsilon: " + str(epsilon) +
                  "\nupdates_per_step " + str(updates_per_step) +
                  "\nmin_sweep_delta: " + str(min_sweep_delta) +
                  "\ngamma: " + str(gamma))

    file_names = runPS(numruns, epsilon, updates_per_step, min_sweep_delta,
                       gamma, grid_world, num_steps)
    #runEvals(file_names)
