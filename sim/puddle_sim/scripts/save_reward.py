#!/usr/bin/env python
'''
Description: Save puddle reward values generated on remote computers

Created on Sept 12, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('puddle_sim')
import rospy
import numpy as np
import cPickle as pickle
import signal
import raven_utils
# from std_msgs.msg import Float64MultiArray
from puddle_sim.msg import Reward


class SaveReward:

    def __init__(self):
        rospy.init_node('save_reward', disable_signals=True)
        package_address = roslib.packages.get_pkg_dir('puddle_sim')
        self.reward_list = []
        self.level_sample_list = [[], [], []]
        self.reward_file = open(package_address + '/log/total_reward.p', 'wb')
        rospy.Subscriber("puddle_reward", Reward, self.rewardCB)
        self.start_time = rospy.Time.now()
        self.timeout = rospy.Duration(60 * 60 * 2)  # two hours
        rospy.Timer(rospy.Duration(10), self.watchdogTimer)

#-----------------------------------------------------------------------------
# Simple timer watchdog that will timeout and end the program after two hours
#-----------------------------------------------------------------------------
    def watchdogTimer(self, event):
        elapsed_time = event.current_real - self.start_time
#         print elapsed_time.to_sec()
        if elapsed_time > self.timeout:
            self.saveData()


#-----------------------------------------------------------------------------
# Everytime we get new data, convert to numpy array and append it to a list
#-----------------------------------------------------------------------------
    def rewardCB(self, data):
        np_reward = raven_utils.multiArray2NumpyArray(data.reward)
        self.reward_list.append(np_reward)
        self.level_sample_list[0].append(data.level1_samples)
        self.level_sample_list[1].append(data.level2_samples)
        self.level_sample_list[2].append(data.level3_samples)
        rospy.loginfo("Got new reward data -- number packets received: "
                      + str(len(self.reward_list)))
        if len(self.reward_list) == 1000:
            self.saveData()


#-----------------------------------------------------------------------------
# Save the data to file
#-----------------------------------------------------------------------------
    def saveData(self):
        if len(self.reward_list) > 0:
            log = {"reward_list":self.reward_list,
                   "level_sample_list":self.level_sample_list}

            pickle.dump(log, self.reward_file)
            self.reward_file.close()
            rospy.logwarn("Data successfully saved.  Shutting down.")
        rospy.signal_shutdown('Shutdown requested')

#-----------------------------------------------------------------------------
# Reward list data is only saved to file on control-c
#-----------------------------------------------------------------------------
    def signal_handler(self, signal, frame):
        self.saveData()



if __name__ == "__main__":
    c = SaveReward()
    signal.signal(signal.SIGINT, c.signal_handler)
    rospy.spin()

