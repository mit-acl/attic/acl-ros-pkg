/*
 * OnBoardController.cpp
 *
 *  Created on: Oct 1, 2012
 *      Author: grieneis
 */

#include "OnBoardController.h"

#include "acl/Quaternion.h"

OnBoardController::OnBoardController()
{

	controlDT = 0.001;
	setGains();

	srand(time(NULL));
}

OnBoardController::~OnBoardController()
{

}

/*---------------------------------------------------------------------
 Function Name: Controller_Update
 Description:   Run the attitude controller, should execute at 1KHz
 Set motor values
 Inputs:        None
 Returns:       None
 -----------------------------------------------------------------------*/
Eigen::Vector4d OnBoardController::runController(Eigen::Quaterniond att,
		Eigen::Vector3d rate, Eigen::Quaterniond att_cmd,
		Eigen::Vector3d rate_cmd, double throttle, int AttCmd)
{

	// Quaternion attitude error
	Eigen::Quaterniond qerror = att.inverse() * att_cmd;

	// New Quaternion Controller from Frazzoli
//	double pErr = rate(0) - rate_cmd(0);
//	double qErr = rate(1) - rate_cmd(1);
//	double rErr = rate(2) - rate_cmd(2);
//	double rollCmd = -Kp_roll * qerror.x();
//	double pitchCmd = -Kp_pitch * qerror.y();
//	double yawCmd = -Kp_yaw * qerror.z();
//
//	if (qerror.w() < 0)
//	{
//		rollCmd = -rollCmd;
//		pitchCmd = -pitchCmd;
//		yawCmd = -yawCmd;
//	}
//
//	// Increment Integrators if motor commands are being sent
//	if (1 == AttCmd || 2 == AttCmd)
//	{
//		IntRoll += rollCmd * controlDT;
//		IntPitch += pitchCmd * controlDT;
//		IntYaw += yawCmd * controlDT;
//	}
//
//	rollCmd += Ki_roll * IntRoll - Kd_roll * pErr;
//	pitchCmd += Ki_pitch * IntPitch - Kd_pitch * qErr;
//	yawCmd += Ki_yaw * IntYaw - Kd_yaw * rErr;
	// End New Quaternion controller

	Eigen::Matrix3d Kp, Kd;
	Kp << Kp_roll, 0, 0, 0, Kp_pitch, 0, 0, 0, Kp_yaw;
	Kd << Kd_roll, 0, 0, 0, Kd_pitch, 0, 0, 0, Kd_yaw;
	Eigen::Vector3d qe;
	qe << qerror.x(), qerror.y(), qerror.z();

	Eigen::Vector3d M;
	if (qerror.w() >= 0)
		M = Kp*qe - Kd*(rate - rate_cmd);
	else
		M = -Kp*qe - Kd*(rate - rate_cmd);

	double rollCmd = M[0];
	double pitchCmd = M[1];
	double yawCmd = M[2];

	// Form motor commands
	double m1 = -pitchCmd - yawCmd;
	double m2 = -rollCmd + yawCmd;
	double m3 = pitchCmd - yawCmd;
	double m4 = rollCmd + yawCmd;

	Eigen::Vector4d motorcmd = Eigen::Vector4d::Zero();
	if (1 == AttCmd)
	{

		motorcmd(0) = m1 + throttle;
		motorcmd(1) = m2 + throttle;
		motorcmd(2) = m3 + throttle;
		motorcmd(3) = m4 + throttle;

	}
	return motorcmd;
}

void OnBoardController::setGains(void)
{
	// mQxx default values
	Kp_roll = (100.0)/255;
	Ki_roll = (0.0)/255;
	Kd_roll = (22.0)/255;

	Kp_pitch = (100.0)/255;
	Ki_pitch = (0.0)/255;
	Kd_pitch = (22.0)/255;

	Kp_yaw = (153.0)/255;
	Ki_yaw = (0.0)/255;
	Kd_yaw = (25.5)/255;

	IntRoll = (0.0)/255;
	IntPitch = (0.0)/255;
	IntYaw = (0.0)/255;
	controlDT = (1.0 / 1000.0);
}


