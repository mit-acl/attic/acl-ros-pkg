/*
 * OnBoardController.h
 *
 *  Created on: Oct 1, 2012
 *      Author: grieneis
 */

#ifndef ONBOARDCONTROLLER_H_
#define ONBOARDCONTROLLER_H_

#include <stdlib.h>
#include <ctime>

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Wrench.h>
#include <tf/tf.h>

#include <eigen3/Eigen/Geometry>

class OnBoardController {
public:
	OnBoardController();
	virtual ~OnBoardController();


	Eigen::Vector4d runController(Eigen::Quaterniond att,
		Eigen::Vector3d rate, Eigen::Quaterniond att_cmd,
		Eigen::Vector3d rate_cmd, double throttle, int AttCmd);

	// getters and setters
	void setGains(void);
	void setControlDT(double dt){controlDT = dt;};
	void getControlDT(double &dt){dt = controlDT;};


private:

	double Kp_roll, Kp_pitch, Kp_yaw;
	double Ki_roll, Ki_pitch, Ki_yaw;
	double Kd_roll, Kd_pitch, Kd_yaw;
	double IntRoll, IntPitch, IntYaw;

	double controlDT;

};

#endif /* ONBOARDCONTROLLER_H_ */
