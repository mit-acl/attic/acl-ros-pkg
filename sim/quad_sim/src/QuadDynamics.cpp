/*!
 * \file QuadDynamics.cpp
 *
 * @todo Brief file description
 *
 *  Created on: Mar 22, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#include "QuadDynamics.hpp"

namespace acl {


QuadDynamics::QuadDynamics() {

    // Initialize all variables
    simTime = 0.0;

    // Set default initial parameters

    // BQxx
    param.m = 0.416; // kg mass of BQ04
    param.J = Eigen::Matrix3d::Zero();
    param.J(0,0) = 0.0042; // Jxx
    param.J(1,1) = 0.0042; // Jyy
    param.J(2,2) = 0.0082; // Jzz
    param.l = 0.152;
    param.Kmotor = 0.01843;
    param.drag = 0.14;
    param.Kmx = param.J(0,0)/(param.l*param.Kmotor*2);
    param.Kmz = param.J(2,2)/(param.l*4*param.drag);
    param.motor_scale = 4.1667;  // scaling from BQ04 measured values

    // Set default initial state
    state.pos.setZero();
    state.vel.setZero();
    state.Q = Eigen::Quaterniond::Identity();
    state.rate.setZero();

    // construct a trivial random generator engine from a time-based seed:
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    generator.seed(seed);

    // Set up noise values
    noise.velocity_variance = 0.8;
    updateNoise();

}

QuadDynamics::~QuadDynamics() {
    // TODO Auto-generated destructor stub
}

/**
 *
 * @return
 */
struct sQuadState QuadDynamics::getState(void) {
    return this->state;
}

/**
 * Update the internal noise generators with the correct variances
 */
void QuadDynamics::updateNoise(void){
    velocity_noise = std::normal_distribution<double>(0.0,noise.velocity_variance);
}

/**
 *
 * @return
 */
struct sQuadParam QuadDynamics::getParams(void) {
    return this->param;
}

/**
 *
 * @return
 */
struct sNoiseParam QuadDynamics::getNoise(void) {
    return this->noise;
}

/**
 * Initialize state and reset time to zero
 * @param initState Initial state value
 */
void QuadDynamics::setInitialState(struct sQuadState initState){
    state = initState;
    simTime = 0;
}

/**
 * Set noise parameters for simulation
 * @param n
 */
void QuadDynamics::setNoiseStruct(struct sNoiseParam n){
    noise = n;
    updateNoise();
}

/**
 * Set quad parameters
 * @param p
 */
void QuadDynamics::setParamStruct(struct sQuadParam p) {
    param = p;
}

/**
 * Set thrust and moment values
 */
void QuadDynamics::setThrustMoments(double F[3], double M[3])
{
	this->F(0) = F[0];
	this->F(1) = F[1];
	this->F(2) = F[2];
	this->M(0) = M[0];
	this->M(1) = M[1];
	this->M(2) = M[2];
}

/**
 * Integrate the system dynamics one time step
 * @param dt Time step
 */
void QuadDynamics::integrateStep(double dt)
{
    std::valarray<double> currentState(STATE_LENGTH);
    std::valarray<double> nextState(STATE_LENGTH);

    currentState[0] = state.pos(0);
    currentState[1] = state.pos(1);
    currentState[2] = state.pos(2);
    currentState[3] = state.vel(0);
    currentState[4] = state.vel(1);
    currentState[5] = state.vel(2);
    currentState[6] = state.Q.w();
    currentState[7] = state.Q.x();
    currentState[8] = state.Q.y();
    currentState[9] = state.Q.z();
    currentState[10] = state.rate(0);
    currentState[11] = state.rate(1);
    currentState[12] = state.rate(2);

    // run the RK4 integration step
    nextState = acl::rk4(simTime, currentState, dt, boost::bind(&QuadDynamics::dynamics, this, _1, _2));
    simTime += dt;

    state.pos(0)  = nextState[0];
    state.pos(1)  = nextState[1];
    state.pos(2)  = nextState[2];
    state.vel(0)  = nextState[3];
    state.vel(1)  = nextState[4];
    state.vel(2)  = nextState[5];
    state.Q.w() = nextState[6];
    state.Q.x() = nextState[7];
    state.Q.y() = nextState[8];
    state.Q.z() = nextState[9];
    state.rate(0)  = nextState[10];
    state.rate(1)  = nextState[11];
    state.rate(2)  = nextState[12];

    if (state.pos(2) <= 0)
    {
    	state.pos(2) = 0;
    	state.vel(0) /= 1000.;
    	state.vel(1) /= 1000.;
    	state.vel(2) = 0;
    	state.Q.w() = 1;
    	state.Q.x() = 0;
    	state.Q.y() = 0;
    	state.Q.z() = 0;
    	state.rate(0) = 0;
    	state.rate(1) = 0;
    	state.rate(2) = 0;
    }
}

/**
 * System dynamics
 * @param dt Time step
 * @param s State
 * @return Derivative of state
 */
std::valarray<double> QuadDynamics::dynamics(double dt, std::valarray<double> s){

    //double x  = s[0];
    //double y  = s[1];
    //double z  = s[2];
    Eigen::Vector3d posdot(s[3], s[4], s[5]);
    Eigen::Quaterniond Q(s[6], s[7], s[8], s[9]);
    Eigen::Vector3d omega(s[10], s[11], s[12]);

    // System dynamics
    Eigen::Quaterniond Fquat(0, F[0], F[1], F[2]);
    Eigen::Quaterniond veldot = Q*Fquat*Q.conjugate();
    veldot.coeffs() *= 1/param.m;
    veldot.z() -= GRAVITY;
    Eigen::Quaterniond Qdot = Q*Eigen::Quaterniond(0, omega(0), omega(1), omega(2));
    Qdot.coeffs() *= 0.5;
    Eigen::Vector3d ratedot = param.J.inverse()*(M - omega.cross(param.J*omega));

    std::valarray<double> out(STATE_LENGTH);
    out[0] = posdot(0);
    out[1] = posdot(1);
    out[2] = posdot(2);
    out[3] = veldot.x() + velocity_noise(generator);
    out[4] = veldot.y() + velocity_noise(generator);
    out[5] = veldot.z() + velocity_noise(generator);
    out[6] = Qdot.w();
    out[7] = Qdot.x();
    out[8] = Qdot.y();
    out[9] = Qdot.z();
    out[10] = ratedot[0];
    out[11] = ratedot[1];
    out[12] = ratedot[2];


    return out;
}

} /* namespace acl */
