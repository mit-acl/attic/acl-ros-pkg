/*!
 * \file QuadDynamics.hpp
 *
 * Relatively simple quad dynamics based on the following paper:
 *  "Steady-State Cornering Equilibria and Stabilization for a
Vehicle During Extreme Operating Conditions" by Velenis, Frazzoli, and Tsiotras
 *
 *  Created on: Mar 22, 2013
 *      Author: Mark Cutler
 *     Contact: markjcutler@gmail.com
 *
 */

#ifndef QUADDYNAMICS_HPP_
#define QUADDYNAMICS_HPP_

// Global includes
#include <valarray>
#include <boost/bind.hpp>
#include <random>
#include <chrono>

// Eigen includes
#include <eigen3/Eigen/Geometry>

// Local includes
#include "acl/utils.hpp"

namespace acl {

const unsigned int STATE_LENGTH = 13;

/// parameter struct
struct sQuadParam{
    Eigen::Matrix3d J; ///< Mass Moment of Inertia
	double m; ///< Vehicle mass (kg)
    double l; ///< Arm length (m)
    double drag; ///< drag coefficient
    double Kmotor; ///< motor coefficient
    double Kmx; ///< motor coefficient
    double Kmz; ///< motor coefficient
    double motor_scale; ///< scalling from BQ04 measured values

};

/// parameter struct
struct sNoiseParam{
    double velocity_variance; ///< Guassian variance on velocity states
};

/// state struct
struct sQuadState{
    Eigen::Vector3d pos; ///< Inertial position
    Eigen::Vector3d vel; ///< Inertial velocity
    Eigen::Quaterniond Q; ///< Attitude quaternion
    Eigen::Vector3d rate; ///< Body-frame rate
};

/**
 *  Quad dynamics
 */
class QuadDynamics {
public:
    QuadDynamics();
    virtual ~QuadDynamics();

    void setMFParams(double b, double c, double d);
    void setParamStruct(struct sQuadParam param);
    void setNoiseStruct(struct sNoiseParam n);
    void setThrustMoments(double F[3], double M[3]);
    void setInitialState(struct sQuadState initState);

    struct sQuadState getState(void);
    struct sQuadParam getParams(void);
    struct sNoiseParam getNoise(void);

    void integrateStep(double dt);


private:

    std::string name;   ///< quad name
    struct sQuadState state;     ///< state
    struct sQuadParam param; ///< param struct

    Eigen::Vector3d F, M;

    std::default_random_engine generator;
    std::normal_distribution<double> velocity_noise; ///< Guassian noise on velocity states
    struct sNoiseParam noise; ///< simulator noise

    double simTime;  ///< simulation time

    void updateNoise(void);
    std::valarray<double> dynamics(double dt, std::valarray<double> s);
};


} /* namespace acl */
#endif /* QUADDYNAMICS_HPP_ */
