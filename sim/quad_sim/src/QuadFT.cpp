/*
 * QuadFT.cpp
 *
 *  Created on: Oct 1, 2012
 *      Author: grieneis
 */

#include "QuadFT.h"

// using coordinate frame N-W-U

QuadFT::QuadFT()
{

	// initialize to default values.
	flying = false;

	// Initialize random time generator
	srand(time(NULL));

}

QuadFT::~QuadFT()
{
	// TODO Auto-generated destructor stub
}

void QuadFT::getMotorFT(Eigen::Vector4d motorcmd, geometry_msgs::Wrench& wrench)
{

	// calculate the wrench due to the quad's motors
	wrench.force.x = 0;
	wrench.force.y = 0;
	wrench.force.z = param.motor_scale
			* (motorcmd(0) + motorcmd(1) + motorcmd(2) + motorcmd(3));

	if (flying)
	{ // only compute torque if the vehicle is in the air
		wrench.torque.x = (motorcmd(3) - motorcmd(1)) * param.motor_scale
				* param.l;
		wrench.torque.y = (motorcmd(2) - motorcmd(0)) * param.motor_scale
				* param.l;
		wrench.torque.z =
				(motorcmd(3) - motorcmd(2) + motorcmd(1) - motorcmd(0))
						* param.motor_scale * param.drag;
	}
	else
	{
		wrench.torque.x = 0;
		wrench.torque.y = 0;
		wrench.torque.z = 0;
	}
}

void QuadFT::getGravityFT(geometry_msgs::Wrench& wrench, geometry_msgs::Quaternion att)
{

	double qx = att.x;
	double qy = att.y;
	double qz = att.z;
	double q0 = att.w;

	wrench.force.x = -param.m * GRAVITY * 2 * (qx * qz - qy * q0);
	wrench.force.y = -param.m * GRAVITY * 2 * (qy * qz + qx * q0);
	wrench.force.z = -param.m * GRAVITY * (qz * qz + q0 * q0 - qx * qx - qy * qy);

	wrench.torque.x = 0;
	wrench.torque.y = 0;
	wrench.torque.z = 0;

}

void QuadFT::getFloorFT(geometry_msgs::Wrench& wrench,
		geometry_msgs::Wrench& wm, geometry_msgs::Wrench& wg,
		acl::sQuadState state)
{
	double qx = state.Q.x();
	double qy = state.Q.y();
	double qz = state.Q.z();
	double q0 = state.Q.w();

	//flying = true;
	if (flying)
	{
		if (state.pos(2) <= 0)
		{
			double Ff = param.m * GRAVITY * (-state.pos(2) * 100);
			double Dr = 100; // drag constant for when the vehicle is in the ground (stop it hard)
			wrench.force.x = Ff * 2 * (qx * qz - qy * q0) - Dr * state.vel(0)/10;
			wrench.force.y = Ff * 2 * (qy * qz + qx * q0) - Dr * state.vel(1)/10;
			wrench.force.z = Ff * (qz * qz + q0 * q0 - qx * qx - qy * qy)
					- Dr * state.vel(2);
			flying = false;
		}
		else
		{
			wrench.force.x = 0;
			wrench.force.y = 0;
			wrench.force.z = 0;
		}
	}
	else
	{ // assume quad is level if takeoff hasn't occured.
		wrench.force.x = 0;
		wrench.force.y = 0;
		double Ff = (-wm.force.z - wg.force.z);
		if (Ff < param.m * GRAVITY)
		{
			Ff = 0;
			flying = true;
		}
		wrench.force.z = param.m*GRAVITY;
	}

	wrench.torque.x = 0;
	wrench.torque.y = 0;
	wrench.torque.z = 0;

}

void QuadFT::getNoiseFT(geometry_msgs::Wrench& wrench)
{

	// noise magnitude
	double Nf = 0.1;
	double Nt = 1;
	// add an AWGN vector to represent motor noise, disturbances, etc. (not a good model)
	wrench.force.x = (((double) rand()) / RAND_MAX - 0.5) * Nf;
	wrench.force.y = (((double) rand()) / RAND_MAX - 0.5) * Nf;
	wrench.force.z = (((double) rand()) / RAND_MAX - 0.5) * Nf;
	wrench.torque.x = (((double) rand()) / RAND_MAX - 0.5) * Nt;
	wrench.torque.y = (((double) rand()) / RAND_MAX - 0.5) * Nt;
	wrench.torque.z = (((double) rand()) / RAND_MAX - 0.5) * Nt;
}

void QuadFT::getDisturbanceFT(geometry_msgs::Wrench& wrench)
{

	// noise magnitude
	double l = 1;
	double m = 5;
	// add a mass on a stick as a disturbance
	wrench.force.x = 0;
	wrench.force.y = 0;

	wrench.torque.y = 0;
	wrench.torque.z = 0;

	bool disturb = false;
	//if (flying) disturb = true;

	static double timed = ros::Time::now().toSec();
	double newtime = ros::Time::now().toSec();

	bool timecheck = false;
	static bool triggered = false;
	if ((newtime - timed > 15) && !triggered)
	{
		timecheck = true;
		triggered = true;
	}

	if (flying && timecheck)
		disturb = true;
	if (disturb)
	{
		wrench.force.z = 0; //-m*GRAVITY;
		wrench.torque.x = -m * GRAVITY * l;
		disturb = false;
	}
	else
	{
		wrench.force.z = 0;
		wrench.torque.x = 0;
	}
}

geometry_msgs::Wrench QuadFT::getTotalFT(Eigen::Vector4d motorcmd, acl::sQuadState state)
{

	geometry_msgs::Wrench wrench, wm, wg, wf, wo;

	getMotorFT(motorcmd, wm);
	//getGravityFT(wg, pose.orientation);
	getFloorFT(wf, wm, wg, state);

	wrench.force.x = wm.force.x + wg.force.x + wf.force.x;
	wrench.force.y = wm.force.y + wg.force.y + wf.force.y;
	wrench.force.z = wm.force.z + wg.force.z + wf.force.z;

	wrench.torque.x = wm.torque.x + wg.torque.x + wf.torque.x;
	wrench.torque.y = wm.torque.y + wg.torque.y + wf.torque.y;
	wrench.torque.z = wm.torque.z + wg.torque.z + wf.torque.z;

	if (not flying)
	{
		wrench.force.x = 0;
		wrench.force.y = 0;
		wrench.force.z = param.m*GRAVITY;

		wrench.torque.x = 0;
		wrench.torque.y = 0;
		wrench.torque.z = 0;
	}

//	getDisturbanceFT(wo);
//
//	wrench.force.x += wo.force.x;
//	wrench.force.y += wo.force.y;
//	wrench.force.z += wo.force.z;
//	wrench.torque.x += wo.torque.x;
//	wrench.torque.y += wo.torque.y;
//	wrench.torque.z += wo.torque.z;

	return wrench;

}
