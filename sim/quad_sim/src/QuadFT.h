/*
 * QuadFT.h
 *
 *  Created on: Oct 1, 2012
 *      Author: grieneis
 */

#ifndef QUADFT_H_
#define QUADFT_H_

// Global includes
#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Wrench.h>
#include <cstdlib> // Needed for rand()
#include <ctime> // Needed to seed random number generator with a time value

#include <eigen3/Eigen/Geometry>

// ACL Utils Library
//#include "acl/QuadDynamics.hpp"
#include "QuadDynamics.hpp"
#include "acl/utils.hpp"

class QuadFT
{
public:
	QuadFT();
	virtual ~QuadFT();

	geometry_msgs::Wrench getTotalFT(Eigen::Vector4d motorcmd, acl::sQuadState state);

	void setParam(acl::sQuadParam param)
	{
		this->param = param;
	}

private:

	bool flying;
	acl::sQuadParam param;

	void getMotorFT(Eigen::Vector4d motorcmd, geometry_msgs::Wrench& wrench);
	void getGravityFT(geometry_msgs::Wrench& wrench,
			geometry_msgs::Quaternion att);
	void getFloorFT(geometry_msgs::Wrench& wrench, geometry_msgs::Wrench& wm,
			geometry_msgs::Wrench& wg, acl::sQuadState state);
	void getNoiseFT(geometry_msgs::Wrench& wrench);
	void getDisturbanceFT(geometry_msgs::Wrench& wrench);

};

#endif /* QUADFT_H_ */
