/*
 * QuadSim.cpp
 *
 *  Created on: Jun 11, 2014
 *      Author: mark
 */

#include "QuadSim.h"

QuadSim::QuadSim()
{

	dynamics.setInitialState(getInitialState());
	qft.setParam(dynamics.getParams());

	cmd.AttCmd = 0;
	cmd.Q = Eigen::Quaterniond::Identity();
	cmd.rate = Eigen::Vector3d::Zero();
	cmd.throttle = 0;

}

QuadSim::~QuadSim()
{
	// TODO Auto-generated destructor stub
}

/**
 * @return An initial state struct with all zero elements
 */
acl::sQuadState QuadSim::getInitialState(void)
{
	acl::sQuadState s;
	s.Q = Eigen::Quaterniond::Identity();
	s.pos.setZero();
	s.vel.setZero();
	s.rate.setZero();

	return s;
}

/**
 * Main function for running the simulation at a fixed rate of 1/DT Hz
 * @param e
 */
void QuadSim::runSim(const ros::TimerEvent& e)
{

	// run onboard controller
	Eigen::Vector4d motorcmd = obc.runController(state.Q, state.rate,
			cmd.Q, cmd.rate, cmd.throttle, cmd.AttCmd);

	// get resulting forces and moments
	geometry_msgs::Wrench wrench = qft.getTotalFT(motorcmd, state);

	// integrate forward dynamics and send state
	double F[3] = {wrench.force.x, wrench.force.y, wrench.force.z};
	double M[3] = {wrench.torque.x, wrench.torque.y, wrench.torque.z};
	dynamics.setThrustMoments(F, M);
	dynamics.integrateStep(DT);
	state = dynamics.getState();

}


void QuadSim::cmdCB(const acl_msgs::QuadCmd& cmd)
{
	this->cmd.Q = Eigen::Quaterniond(cmd.pose.orientation.w, cmd.pose.orientation.x,
			cmd.pose.orientation.y, cmd.pose.orientation.z);
	this->cmd.rate = Eigen::Vector3d(cmd.twist.angular.x, cmd.twist.angular.y,
			cmd.twist.angular.z);
	this->cmd.throttle = cmd.throttle;
	this->cmd.AttCmd = cmd.att_status; // TODO:fix this -- make part of quadCmd message
}

void QuadSim::broadcastTimeout(const ros::WallTimerEvent& e)
{
	 // If this fires, stop the state timer broadcast
	//broadcastTimer.stop();
}

/**
 * Broadcast the state data in a ros message
 * @param state CarState of data that should be broadcast
 */
void QuadSim::broadcastState(const ros::TimerEvent& e)
{
	geometry_msgs::Pose pose;
	pose.position.x = state.pos(0);
	pose.position.y = state.pos(1);
	pose.position.z = state.pos(2);
	pose.orientation.w = state.Q.w();
	pose.orientation.x = state.Q.x();
	pose.orientation.y = state.Q.y();
	pose.orientation.z = state.Q.z();

	geometry_msgs::Twist twist;
	twist.linear.x = state.vel(0);
	twist.linear.y = state.vel(1);
	twist.linear.z = state.vel(2);
	twist.angular.x = state.rate(0);
	twist.angular.y = state.rate(1);
	twist.angular.z = state.rate(2);

	// publish results
	geometry_msgs::PoseStamped pose_stamped;
	// pull off first and last '/' characters
	pose_stamped.header.frame_id = ros::this_node::getNamespace().substr(1, ros::this_node::getNamespace().size()-1);
	pose_stamped.header.stamp = ros::Time::now();
	pose_stamped.pose = pose;

	geometry_msgs::TwistStamped twist_stamped;
	twist_stamped.header = pose_stamped.header;
	twist_stamped.twist = twist;

	pose_pub.publish(pose_stamped);
	twist_pub.publish(twist_stamped);
}
