/*
 * QuadSim.h
 *
 *  Created on: Jun 11, 2014
 *      Author: mark
 */

#ifndef QUADSIM_H_
#define QUADSIM_H_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"

// local includes
#include "acl_msgs/QuadCmd.h"
#include "QuadFT.h"
#include "OnBoardController.h"

// ACL Utils Library
//#include "acl/QuadDynamics.hpp"
#include "QuadDynamics.hpp"
#include "acl/utils.hpp"

const double DT = 1 / 1000.0; ///< default simulation time step
const double BROADCAST_DT = 1 / 30.0; ///< frame rate of state broadcasting for service mode
const double BROADCAST_TIMEOUT = 2.0; ///< seconds since last call after which state is no longer broadcast

enum CMD_TYPES
{
	NOT_FLYING, ATTITUDE, POSITION, RESET
};

/// command struct
struct sQuadCommand{
    Eigen::Quaterniond Q; ///< Attitude quaternion
    Eigen::Vector3d rate; ///< Body-frame rate
    double throttle;
    int AttCmd;
};

/**
 *  Simple wrapper class for running QuadDynamics in a ros node
 */
class QuadSim
{
public:
	QuadSim();
	virtual ~QuadSim();

	ros::Publisher pose_pub, twist_pub;
	acl::QuadDynamics dynamics;
	ros::Timer controller_timer, broadcast_timer;

	void runSim(const ros::TimerEvent& e);
	void broadcastTimeout(const ros::WallTimerEvent& e);
	void cmdCB(const acl_msgs::QuadCmd& cmd);
	void broadcastState(const ros::TimerEvent& e);
	acl::sQuadState getInitialState(void);

private:
	OnBoardController obc;
	QuadFT qft;
	acl::sQuadState state;
	sQuadCommand cmd;




};

#endif /* QUADSIM_H_ */
