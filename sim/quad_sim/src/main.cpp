// Global includes
#include <iostream>

// Local includes
#include "QuadSim.h"

int main(int argc, char* argv[])
{

    // Initialize vehicle
    QuadSim quad;

    ros::init(argc, argv, "sim");
    ros::NodeHandle n;

    // Check for proper renaming of node namespace
    if (!ros::this_node::getNamespace().compare("/")) {
        ROS_ERROR("Error :: You should be using a launch file to specify the "
                  "node namespace!\n");
        return (-1);
    }

    //## Welcome screen
    ROS_INFO("\n\nStarting ROS quad Dynamics code for for %s...\n\n\n",
             ros::this_node::getNamespace().c_str());

    // Override quad parameters if necessary

    // Set initial state if necessary
    //	acl::sQuadState initState;
    //	initState.omegaF = 0.1;
    //	initState.omegaR = initState.omegaF;
    //	initState.Vx = initState.omegaF*0.035;
    //	quad.dynamics.setInitialState(initState);

    // Set initial noise if necessary
    //	acl::sNoiseParam noise;
    //	noise.velocity_variance = 0;
    //	noise.dpsi_variance = 0;
    //	noise.omega_variance = 0;
    //	quad.dynamics.setNoiseStruct(noise);

    acl::sQuadState initState = quad.getInitialState();

    // process command line arguments
    if (argc > 2) {
        // these should be position intializations
        initState.pos[0] = atof(argv[1]); // x
        initState.pos[1] = atof(argv[2]); // y
    }
    if (argc > 3)
        initState.pos[2] = atof(argv[3]); // z

    quad.dynamics.setInitialState(initState);

    // Set up state publishers
    quad.pose_pub = n.advertise<geometry_msgs::PoseStamped>("pose", 0);
    quad.twist_pub = n.advertise<geometry_msgs::TwistStamped>("vel", 0);

    // Set listener callbacks for command data
    ros::Subscriber sub_cmd = n.subscribe("cmds", 1, &QuadSim::cmdCB, &quad);

    // initialize main controller loop
    quad.controller_timer
        = n.createTimer(ros::Duration(DT), &QuadSim::runSim, &quad);

    quad.broadcast_timer = n.createTimer(ros::Duration(1 / 30.0),
                                         &QuadSim::broadcastState, &quad);

    // run the code
    ros::spin();

    return 0;
}
