/*!
 * \file UpuccSim.cpp
 *
 * UpuccSim class file
 *
 *  Created on: May 9, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#include "UpuccSim.hpp"

UpuccSim::UpuccSim()
{
	dynamics.setInitialState(getInitialState());
	dynamicsStep.setInitialState(getInitialState());
	broadcastSrvState = false;
	service_mode = true;

}

UpuccSim::~UpuccSim()
{
	// TODO Auto-generated destructor stub
}

/**
 * Listen to CmdMessages (fom joystick or other program)
 * @param msg
 */
void UpuccSim::cmdCallback(const std_msgs::Float64MultiArray& msg)
{
	if (msg.data.size() == 3)
	{
		bool driving = (bool) msg.data[0];
		if (driving)
		{
			vL = msg.data[1];
			vR = msg.data[2];
		}
		else{
			vL = vR = 0;
		}
		dynamics.setV(vL, vR);
	}
}

/**
 * @return An initial state struct with all zero elements
 */
acl::sUpuccState UpuccSim::getInitialState(void)
{
	acl::sUpuccState s;
	s.x = s.y = 0.0;
	s.psi = 0.0;
	//s.x = -0.5;
	//s.psi = -PI / 2; // point to the right initially

	return s;
}

/**
 * Run a single time step of the dynamics
 * @param req Initial state, time step, and action
 * @param res Final state
 * @return True
 */
bool UpuccSim::runSimStep(upucc_sim::RunStep::Request &req,
		upucc_sim::RunStep::Response &res)
{
	timeoutTimer.stop();
	broadcastTimer.start();

	// set initial state
	dynamicsStep.setInitialState(rosState2state(req.start_pose));

	// set actions
	vL = req.vL;
	vR = req.vR;
	dynamicsStep.setV(vL, vR);

	// integrate forward dynamics
	dynamicsStep.integrateStep(req.dt);

	// get new state
	res.final_pose = state2RosState(dynamicsStep.getState());

	// choose whether or not the broadcast state will come from these dynamics or not
	broadcastSrvState = req.showVis;

	timeoutTimer.start();

	return true;
}

/**
 * Simple service to reset the real-time version to a fixed inital location
 * @param req
 * @param res
 * @return
 */
bool UpuccSim::resetState(upucc_sim::ResetState::Request &req,
		upucc_sim::ResetState::Response &res)
{
	dynamics.setInitialState(rosState2state(req.start_pose));
	return true;
}

/**
 * Main function for running the simulation at a fixed rate of 1/DT Hz
 * @param e
 */
void UpuccSim::runSim(const ros::TimerEvent& e)
{
	// integrate forward dynamics and send state
	dynamics.integrateStep(DT);
	broadcastState(state2RosState(dynamics.getState()));

}

void UpuccSim::broadcastTimeout(const ros::WallTimerEvent& e)
{
	 // If this fires, stop the state timer broadcast
	broadcastTimer.stop();
}

/**
 * Simple function to broadcast the service mode state at a fixed rate
 * @param e
 */
void UpuccSim::broadcastStateTimer(const ros::TimerEvent& e)
{
	broadcastState(state2RosState(dynamicsStep.getState()));
}

/**
 * Broadcast the state data in a ros message
 * @param state UpuccState of data that should be broadcast
 */
void UpuccSim::broadcastState(geometry_msgs::PoseStamped pose)
{

	// publish results
	pose.header.frame_id = ros::this_node::getNamespace().substr(1, 4);
	pose.header.stamp = ros::Time::now();

	pose_pub.publish(pose);

	geometry_msgs::TwistStamped twist;
	twist.header = pose.header;
	double yaw = getYaw(pose.pose.orientation);
	double v = 0.5*(vL+vR);
	double dv = vL - vR;
	twist.twist.linear.x = v*cos(yaw);
	twist.twist.linear.y = v*sin(yaw);
	twist.twist.angular.z = dv/0.083; // TODO: should get this value from UpuccDynamics.cpp
	twist_pub.publish(twist);
}

/**
 * Simple helper function for converting acl::sUpuccState to ros message UpuccState
 * @param in
 * @return
 */
geometry_msgs::PoseStamped UpuccSim::state2RosState(acl::sUpuccState in)
{
	geometry_msgs::PoseStamped pose;
	pose.pose.position.x = in.x;
	pose.pose.position.y = in.y;
	pose.pose.orientation = setOrientation(in.psi);

	return pose;
}

/**
 * Simple helper function for converting ros message UpuccState to acl::sUpuccState
 * @param in
 * @return
 */
acl::sUpuccState UpuccSim::rosState2state(geometry_msgs::PoseStamped in)
{
	acl::sUpuccState out;
	out.x = in.pose.position.x;
	out.y = in.pose.position.y;
	out.psi = getYaw(in.pose.orientation);

	return out;
}

/**
 * Get yaw from quaternion
 * @param q Quaternion
 * @return Yaw in radians
 */
double UpuccSim::getYaw(geometry_msgs::Quaternion q)
{
	return atan2(2 * (q.w * q.z + q.x * q.y), 1 - 2 * (q.y * q.y + q.z * q.z));
}

/**
 * Set quaternion from just yaw
 * @param yaw Yaw angle in radians
 * @return Quaternion
 */
geometry_msgs::Quaternion UpuccSim::setOrientation(double yaw)
{
	// from wikipedia: http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
	geometry_msgs::Quaternion q;
	q.w = cos(yaw / 2.0);
	q.x = 0.0;
	q.y = 0.0;
	q.z = sin(yaw / 2.0);
	return q;
}
