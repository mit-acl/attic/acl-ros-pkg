/*!
 * \file UpuccSim.hpp
 *
 * UpuccSim header
 *
 *  Created on: May 9, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

#ifndef UPUCCSIM_HPP_
#define UPUCCSIM_HPP_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "std_msgs/Float64MultiArray.h"

// Local Messages/Services
#include "upucc_sim/RunStep.h"
#include "upucc_sim/ResetState.h"

// ACL Utils Library
#include "acl/dynamics/UpuccDynamics.hpp"
#include "acl/utils.hpp"

const double DT = 1 / 200.0; ///< default simulation time step
const double BROADCAST_DT = 1 / 30.0; ///< frame rate of state broadcasting for service mode
const double BROADCAST_TIMEOUT = 2.0; ///< seconds since last call after which state is no longer broadcast

/**
 *  Simple wrapper class for running UpuccDynamics in a ros node
 */
class UpuccSim
{
public:
	UpuccSim();
	virtual ~UpuccSim();

	ros::Publisher pose_pub, twist_pub, state_pub;
	acl::UpuccDynamics dynamics;
	acl::UpuccDynamics dynamicsStep;
	bool service_mode;
	ros::Timer broadcastTimer, controllerTimer;
	ros::WallTimer timeoutTimer;
	ros::ServiceServer service;

	void cmdCallback(const std_msgs::Float64MultiArray& msg);
	void runSim(const ros::TimerEvent& e);
	void integrateStep(double dt);
	bool runSimStep(upucc_sim::RunStep::Request &req,
			upucc_sim::RunStep::Response &res);
	bool resetState(upucc_sim::ResetState::Request &req,
		upucc_sim::ResetState::Response &res);
	void broadcastStateTimer(const ros::TimerEvent& e);
	void broadcastTimeout(const ros::WallTimerEvent& e);
	acl::sUpuccState getInitialState(void);

private:

	geometry_msgs::PoseStamped state2RosState(acl::sUpuccState);
	acl::sUpuccState rosState2state(geometry_msgs::PoseStamped);
	void broadcastState(geometry_msgs::PoseStamped);

	double getYaw(geometry_msgs::Quaternion q);
	geometry_msgs::Quaternion setOrientation(double yaw);
	bool broadcastSrvState;
	double vL, vR;

};

#endif /* UPUCCSIM_HPP_ */
