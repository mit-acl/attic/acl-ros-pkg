/*!
 * \file main.cpp
 *
 * Main function for the simple upucc simulator
 *
 *  Created on: May 9, 2013
 *      Author: Mark Cutler
 *	   Contact: markjcutler@gmail.com
 *
 */

// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "UpuccSim.hpp"

int main(int argc, char* argv[])
{

	ros::init(argc, argv, "sim");
	ros::NodeHandle n;

	// parse command line arguements -- Note: these are typically set via launch file
	bool service_mode = true;
	if (argc > 1)
	{
		std::cout <<argv[1] << std::endl;

		if (std::strcmp(argv[1], "false") == 0 or std::strcmp(argv[1], "False") == 0 or std::strcmp(argv[1], "0") == 0)
		{
			service_mode = false;
			ROS_INFO("Starting in real time mode");
		}
	}

	// Initialize vehicle
	UpuccSim upucc;
	upucc.service_mode = service_mode;

	// Check for proper renaming of node namespace
	if (!ros::this_node::getNamespace().compare("/"))
	{
		ROS_ERROR(
				"Error :: You should be using a launch file to specify the node namespace!\n");
		return (-1);
	}

	//## Welcome screen
	ROS_INFO(
			"\n\nStarting ROS Upucc Dynamics code for for %s...\n\n\n", ros::this_node::getNamespace().c_str());

	// Override upucc parameters if necessary

	// Set initial state if necessary
//	acl::sUpuccState initState;
//	initState.omegaF = 0.1;
//	initState.omegaR = initState.omegaF;
//	initState.Vx = initState.omegaF*0.035;
//	upucc.dynamics.setInitialState(initState);

	// Set initial noise if necessary
//	acl::sNoiseParam noise;
//	noise.velocity_variance = 0;
//	noise.dpsi_variance = 0;
//	noise.omega_variance = 0;
//	upucc.dynamics.setNoiseStruct(noise);

	acl::sUpuccState initState = upucc.getInitialState();

    // process command line arguments
	if (argc > 3){
            // these should be position intializations
            initState.x = atof(argv[2]); // x
            initState.y = atof(argv[3]); // y
    }
	if (argc > 4)
			initState.psi = atof(argv[4]); //psi	
	upucc.dynamics.setInitialState(initState);

// Set up state publishers
	upucc.pose_pub = n.advertise<geometry_msgs::PoseStamped>("pose", 0);
	upucc.twist_pub = n.advertise<geometry_msgs::TwistStamped>("vel", 0);

	// Set listener callbacks for command data
	ros::Subscriber sub_cmd = n.subscribe("upucc_cmd", 1, &UpuccSim::cmdCallback,
			&upucc);

	if (upucc.service_mode)
	{
		// Start service for requesting a single integrated step
		upucc.service = n.advertiseService("run_step", &UpuccSim::runSimStep, &upucc);

		// Initialize timer for broadcasting state information
		upucc.broadcastTimer = n.createTimer(ros::Duration(BROADCAST_DT),
				&UpuccSim::broadcastStateTimer, &upucc);

		upucc.timeoutTimer = n.createWallTimer(
				ros::WallDuration(BROADCAST_TIMEOUT), &UpuccSim::broadcastTimeout,
				&upucc, true);
	}
	else
	{
		// Start service for reseting a vehilce
		upucc.service = n.advertiseService("reset", &UpuccSim::resetState, &upucc);

		// initialize main controller loop
		upucc.controllerTimer = n.createTimer(ros::Duration(DT), &UpuccSim::runSim,
				&upucc);

	}

// run the code
	ros::spin();

	return 0;
}

