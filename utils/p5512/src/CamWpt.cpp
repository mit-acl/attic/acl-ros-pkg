#include "CamWpt.h"

/*
 * This file contains a whole assload of camera waypoint initializations and operators.
 * Nothing really interesting here, to be honest.
 */

CamWpt::CamWpt(){
	this->pan = 0;
	this->tilt =0;
	this->x =0;
	this->y = 0;
	this->z = 0;
	this->wpttype = CAMWPT_PTZ;
	this->w = 0;
	this->zoom = 0;
	this->dt =0;
	this->timeout = 0;
	this->forceflip = 0;
	this->clear = false;
}

CamWpt::CamWpt(CamWpt_Type wpttype, double arg1, double arg2, double arg3, double dt, double timeout, bool clear){
	this->pan = 0;
	this->tilt =0;
	this->x =0;
	this->y = 0;
	this->z = 0;
	this->w = 0;
	this->zoom = 0;
	this->clear = clear;
	this->wpttype = wpttype;
	this->dt = dt;
	this->timeout = timeout;
	if (this->timeout < this->dt){
		printf("CamWpt::CamWpt Error: Created a waypoint with timeout < dt\n");
		printf("Setting timeout to dt*2.0\n");
		this->timeout = this->dt*2.0;
	}
	switch (wpttype){
		case CAMWPT_PTZ:
			setPTZ(arg1, arg2, arg3);
			break;
		case CAMWPT_2W:
			set2W(arg1, arg2, arg3);
			break;
		case CAMWPT_2Z:
			set2Z(arg1, arg2, arg3);
			break;
		default:
			printf("CamWpt::CamWpt: ERROR: Incorrect constructor called for waypoint type.\n");
			break;
	}
}

CamWpt::CamWpt(CamWpt_Type wpttype, double arg1, double arg2, double arg3, double arg4, double dt, double timeout, bool clear){
	this->pan = 0;
	this->tilt =0;
	this->x =0;
	this->y = 0;
	this->z = 0;
	this->w = 0;
	this->zoom = 0;
	this->clear = clear;
	this->wpttype = wpttype;
	this->dt = dt;
	this->timeout = timeout;
	if (this->timeout < this->dt){
		printf("CamWpt::CamWpt Error: Created a waypoint with timeout < dt\n");
		printf("Setting timeout to dt*2.0\n");
		this->timeout = this->dt*2.0;
	}
	switch (wpttype){
		case CAMWPT_3W:
			set3W(arg1, arg2, arg3, arg4);
			break;
		case CAMWPT_3Z:
			set3Z(arg1, arg2, arg3, arg4);
			break;
		default:
			printf("CamWpt::CamWpt: ERROR: Incorrect constructor called for waypoint type.\n");
			break;
	}
}

CamWpt::CamWpt(const CamWpt& rhs){
	this->pan = rhs.pan;
	this->tilt = rhs.tilt;
	this->x = rhs.x;
	this->y = rhs.y;
	this->z = rhs.z;
	this->w = rhs.w;
	this->wpttype = rhs.wpttype;
	this->zoom = rhs.zoom;
	this->dt = rhs.dt;
	this->timeout = rhs.timeout;
	this->forceflip = rhs.forceflip;
	this->clear = rhs.clear;
}

CamWpt& CamWpt::operator=(const CamWpt& rhs){
	if (&rhs != this){
		this->pan = rhs.pan;
		this->tilt = rhs.tilt;
		this->x = rhs.x;
		this->y = rhs.y;
		this->z = rhs.z;
		this->w = rhs.w;
		this->wpttype = rhs.wpttype;
		this->zoom = rhs.zoom;
		this->dt = rhs.dt;
		this->timeout = rhs.timeout;
		this->forceflip = rhs.forceflip;
		this->clear = rhs.clear;
	}
	return *this;
}

void CamWpt::set2W(double x, double y, double w){
	this->x = x;
	this->y = y;
	this->w = w;
}
void CamWpt::set2Z(double x, double y, double zoom){
	this->x = x;
	this->y = y;
	this->zoom = zoom;
}
void CamWpt::set3W(double x, double y, double z, double w){
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}
void CamWpt::set3Z(double x, double y, double z, double zoom){
	this->x = x;
	this->y = y;
	this->z = z;
	this->zoom = zoom;
}
void CamWpt::setPTZ(double pan, double tilt, double zoom){
	this->pan = pan;
	this->tilt = tilt;
	this->zoom = zoom;
}

