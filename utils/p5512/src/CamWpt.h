#ifndef __CAMWPT_H
//standard includes
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <list>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>


//there are different waypoint types - the relevant ones are listed in this enum
enum CamWpt_Type{
			CAMWPT_PTZ = 0, //pan tilt zoom waypoint
			CAMWPT_2W = 1, //x, y + width
			CAMWPT_2Z = 2, //x, y + zoom
			CAMWPT_3W = 3, //x, y, z + with
			CAMWPT_3Z = 4// x, y, z + zoom
}; 

/*
 * The CamWpt class is a container for different types of waypoints that the P5512s listen to.
 * Waypoints can be specified as any of the CamWpt_Types.
 * The class contains information like
 * the zoom level, pan/tilt angles,
 *  the x y z view coordinates (the point that the cam should look at)
 *  the width of the view window AT the xyz point (w)
 *  the clearflag, telling the camera whether this waypoint overrides all previous ones sent
 *  dt is the amount of time the camera should take to complete the waypoint
 *  timeout is how long the camera tries to complete a waypoint before it gives up
 *  forceflip tells the camera to flip its tilt - it's only used when the camera is starting up and needs to figure out
 *  the state of its pan carriage
 */
class CamWpt{
	friend class P5512;
	public:
		CamWpt(CamWpt_Type wpttype, double arg1, double arg2, double arg3, double dt, double timeout, bool clear);
		CamWpt(CamWpt_Type wpttype, double arg1, double arg2, double arg3, double arg4, double dt, double timeout, bool clear);//all units in degrees and seconds except zoom, which is from 1-10000 for optical (1x - 12x)

		CamWpt(const CamWpt& rhs);
		CamWpt& operator=(const CamWpt& rhs);
	private:
		int forceflip; //this is used ONLY by the P5512::calibrate() routine to force flips
				//-1 forces a NONflipped carriage position, 1 forces flipped, 0 does nothing.
		double zoom, dt, timeout;
		double x, y, z, pan, tilt;
		double w;
		bool clear; //the clear flag, used to force empty any previous points from the waypoint queue
		CamWpt(); //only P5512 can use this
		CamWpt_Type wpttype;
		void set2W(double x, double y, double w);
		void set2Z(double x, double y, double zoom);
		void set3W(double x, double y, double z, double w);
		void set3Z(double x, double y, double z, double zoom);
		void setPTZ(double pan, double tilt, double zoom);
};
#define __CAMWPT_H
#endif /* __CAMWPT_H */
