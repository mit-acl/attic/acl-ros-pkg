#include "LSPosCalib.h"

//This stuff is all for the purpose of solving the least squares problem


double f_partobj(Vector3d xc, Vector3d xi, Vector3d xj, double cth){
	double res = cth - (xi-xc).dot((xj-xc))/((xi-xc).norm()*(xj-xc).norm());
	res = res*res;
	return res;
}

Vector3d g_partobj(Vector3d xc, Vector3d xi, Vector3d xj, double cth){
	double scale = -2.0*(cth - (xi-xc).dot((xj-xc))/((xi-xc).norm()*(xj-xc).norm()) );
	scale = scale/((xi-xc).norm()*(xj-xc).norm()*(xi-xc).norm()*(xj-xc).norm());
	Vector3d vec = (2*xc -(xi+xj))*(xi-xc).norm()*(xj-xc).norm()
			- (xi-xc).dot((xj-xc))*( (xi-xc).norm()*(xc-xj)/(xc-xj).norm() + (xj-xc).norm()*(xc-xi)/(xc-xi).norm());
	return vec*scale;
}
double f_obj(Vector3d xc, MatrixXd glob, MatrixXd cths){
	int ransacsize = glob.cols();
	double res = 0;
	for (int i = 0; i < ransacsize; i++){
		res+= f_partobj(xc, glob.block<3,1>(0,i), glob.block<3,1>(0, i+1 == ransacsize? 0: i+1), cths(i));
	}
	return res;
}

Vector3d g_obj(Vector3d xc, MatrixXd glob, MatrixXd cths){
	int ransacsize = glob.cols();
	Vector3d res;
	res << 0,0,0;
	for (int i = 0; i < ransacsize; i++){
		res+= g_partobj(xc, glob.block<3,1>(0,i), glob.block<3,1>(0, i+1 == ransacsize? 0: i+1), cths(i));
	}
	return res;
}



bool solveLS(int opttries, MatrixXd glob, MatrixXd camunit, Vector3d& position, double& fval){
	const double positiontol = 1e-4;
	int ransacsize = glob.cols();
	MatrixXd cths(1, ransacsize);
	
	for (int i = 0; i < ransacsize; i++){
		cths(i) = (camunit.block<3,1>(0,i)).dot( camunit.block<3,1>(0, i+1 == ransacsize ? 0 : i+1) );
	}
	Vector3d xBest;
	xBest << 0, 0, 0;
	double fBest = 1e20;
	Vector3d xc;
	for (int i = 0; i < opttries; i++){
		//xc << 5.0, 2.0, 3.0; //initialize perfectly
		xc << -4 + 12*(double)(rand()%1000)/1000.0,
		      -4 + 12*(double)(rand()%1000)/1000.0, 
		      -4 + 12*(double)(rand()%1000)/1000.0; //initialize randomly
		int numitr = 0;
		bool MINLINESEARCHERROR = false;
		bool MAXLINESEARCHERROR = false;
		while(true){
			Vector3d grad = this->g_obj(xc, glob, cths);
			//line search
			//using -grad as direction
			//first bracket the minimum
			//start stepping at most 0.01 at a time, increasing by factors of 2
			//until the first increase happens
			const double tau0 = min(0.01, grad.norm());
			const double tauMIN = tau0/1e9;
			const double tauMAX = 50;
			double tau = tau0;
			Vector3d pk = -grad/grad.norm();
			double fL, fM, fR;
			Vector3d xL, xM, xR;
			xL = xc;
			fL = this->f_obj(xL, glob, cths);
			tau *= 2.0;
			do{
				tau /= 2.0;
				if (tau < tauMIN){
					MINLINESEARCHERROR = true;
					break;
				}
				xM = xc + tau*pk;
				fM = this->f_obj(xM, glob, cths);
			} while (fM > fL);
			//now we have an xL and an xM where fM < fL
			//start moving forward to find an xR that brackets a minimum
			if (MINLINESEARCHERROR){
				break;
			}
			tau *= 2.0;
			xR = xc + tau*pk;
			fR = f_obj(xR, glob, cths);
			while(fR < fM){
				xL = xM;
				fL = fM;
				xM = xR;
				fM = fR;
				tau *= 2.0;
				xR = xc + tau*pk;
				fR = this->f_obj(xR, glob, cths);
				if (tau > tauMAX){
					MAXLINESEARCHERROR = true;
					break;
				}
			}
			if (MAXLINESEARCHERROR){
				break;
			}
			//we've now bracketed a minimum, do binary search
			while((xR-xL).norm() > positiontol){
				if ((xR-xM).norm() > (xL-xM).norm()){
					Vector3d xRM = (xR+xM)/2.0;
					double fRM = this->f_obj(xRM, glob, cths);
					if (fRM < fM){
						xL = xM;
						fL = fM;
						xM = xRM;
						fM = fRM;
					} else {
						xR = xRM;
						fR = fRM;
					}
				} else {
					Vector3d xLM = (xL+xM)/2.0;
					double fLM = this->f_obj(xLM, glob, cths);
					if (fLM < fM){
						xR = xM;
						fR = fM;
						xM = xLM;
						fM = fLM;
					} else {
						xL = xLM;
						fL = fLM;
					}
				}
			}
			//we're done the line search
			//take the step
			Vector3d xnew = (xR+xL)/2.0;
			if ((xnew - xc).norm() < positiontol){
				xc = xnew;
				break;
			} else {
				xc = xnew;
			}
			numitr++;
		} //outer optimization wihle loop
		if (MINLINESEARCHERROR){
			printf("Optimization try %d had a tau < min line search error.\n", i);
			continue;
		} else if (MAXLINESEARCHERROR){
			printf("Optimization try %d had a tau > max line search error.\n", i);
			continue;
		}
		printf("Optimization try %d finished after %d outer iterations.\n", i, numitr);
		double fc = this->f_obj(xc, glob, cths);
		if (fc < fBest){
			fBest = fc;
			xBest = xc;
		}
	}//for opt tries
	position = xBest;
	fval = fBest;
	if (fBest > 1e19){
		return false;
	} else {
		return true;
	}
}
