#ifndef __LSPOSCALIB_H

/*
 * solveLS is the main position calibration function. It takes in a bunch of global vectors (glob),
 * and a bunch of camera base frame unit vectors (camunit) and outputs the least squares position 
 * as a 3d vector, with objective value fval, after opttries attempts to optimize the position (it
 * occasionally fails)
 */
using namespace Eigen;
bool solveLS(int opttries, MatrixXd glob, MatrixXd camunit, Vector3d& position, double& fval);
double f_partobj(Vector3d xc, Vector3d xi, Vector3d xj, double cth);
Vector3d g_partobj(Vector3d xc, Vector3d xi, Vector3d xj, double cth);
double f_obj(Vector3d xc, MatrixXd glob, MatrixXd cths);
Vector3d g_obj(Vector3d xc, MatrixXd glob, MatrixXd cths);
#define __LSPOSCALIB_H
#endif /* __LSPOSCALIB_H */
