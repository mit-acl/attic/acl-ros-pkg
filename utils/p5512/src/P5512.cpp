#include "P5512.h"

int P5512::CURLGLOBAL = 0;//the number of curl connections


//README FIRST BEFORE CHANGING ANY OF THE COORDINATE FRAME STUFF
//Note on coordinate frames:
//The "camera image" frame in this code is taken to be the frame centered on the P5512 camera with
//z pointing into the camera image, x pointing to the right, and y down
//Keep in mind; the camera flips its image whenever tilt surpasses -100 degrees, therefore the "camera image" frame flips too
//The "camera base" frame is the frame centered on the P5512 camera which coincides with the "camera image" frame
//when pan = 0 and tilt = 0

//Thus, if Vb is a vector in the base frame, and Vc is a vector in the image frame,
// Vc = RotX(tilt)*RotY(pan)*Vb
//where pan and tilt are the raw pan/tilt values from the P5512.


P5512::P5512(string name, string ip_addr) : imgworker(ip_addr), ptzworker(ip_addr){
	if (CURLGLOBAL == 0){
		curl_global_init(CURL_GLOBAL_ALL);
		CURLGLOBAL++;
	} else {
		CURLGLOBAL++;
	}
	this->name = name;
	this->ip_addr = ip_addr;

	#ifdef DEBUGPRINTS
	this->verbose = true;
	#else
	this->verbose = false;
	#endif

	this->idle = true;
	this->wptthread=NULL;
}


string P5512::getName(){
	return this->name;
}


void P5512::setBaseExtrinsics(Matrix<double, 3, 4> extrinsicmat){
	this->calib_baseExtrinsicMat = extrinsicmat;
	//cout << "The base position is: " << endl << calib_basept << endl;
	//cout << "The base rotation is: " << endl << calib_baserot << endl;
}

void P5512::setSpeeds(double (&calib_axisspeeds)[11], double (&calib_panspeeds)[11], double (&calib_tiltspeeds)[11]){
	for (int i = 0; i < 11; i++){
		this->calib_axisspeeds[i] = calib_axisspeeds[i];
		this->calib_panspeeds[i] = calib_panspeeds[i];
		this->calib_tiltspeeds[i] = calib_tiltspeeds[i];
		//cout << "Speeds: Axis: " << calib_axisspeeds[i] << " Pan: " << calib_panspeeds[i] << " Tilt: " << calib_tiltspeeds[i] << endl;
	}
}

void P5512::setIntrinsics(double (&calib_axiszooms)[11], boost::array<Matrix3d, 11>& calib_intrinsicMats, boost::array<Matrix<double, 1, 5>, 11>& calib_distortionCoeffMats){
	for (int i = 0; i < 11; i++){
		this->calib_axiszooms[i] = calib_axiszooms[i];
		this->calib_intrinsicMats[i] = calib_intrinsicMats[i];
		this->calib_distortionCoeffMats[i] = calib_distortionCoeffMats[i];
		//cout << "Intrinsics: Axis zoom: " << calib_axiszooms[i] << endl << "Mat: " << endl << calib_intrinsicMats[i] << endl;
	}
}

Matrix3d P5512::getBaseToImageFrame(double pandeg, double tiltdeg){
	double panrad = M_PI/180.0*pandeg;
	double tiltrad = M_PI/180.0*tiltdeg;
	Matrix3d rotY;
	rotY << cos(panrad), 0, -sin(panrad),
		0	   , 1,        0    ,
		sin(panrad), 0, cos(panrad);
	Matrix3d rotX;
	rotX << 1,	0, 	0,
		0, cos(tiltrad), sin(tiltrad),
		0, -sin(tiltrad), cos(tiltrad);
	return rotX*rotY;
}
//this function gets the current pan/tilt state of the camera and produces a total extrinsic
//transformation from global vectors to camera image vectors based on the base extrinsic
//see the comment at the top of the file for coordinate frame conventions
Matrix<double, 3, 4> P5512::getTotalExtrinsic(){
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);

	Matrix<double, 3, 4> globToBase = this->calib_baseExtrinsicMat;
	//cout << "Global to Base (calib): " << endl << globToBase << endl;
	Matrix3d baseToImg = this->getBaseToImageFrame(tmppan, tmptilt);
	//cout << "Base to Img: " << endl << baseToImg << endl;
	//cout << "Total Extrinsic " << endl << baseToImg*globToBase << endl;
	return baseToImg*globToBase;
}

void P5512::getCamGroundPt(double (&grndPt)[2]){
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	Matrix3d perspec = convertZoomToPerspectiveMat(tmpzoom);
	Matrix<double, 3, 4> totalex = this->getTotalExtrinsic();
	
	Matrix<double, 3, 4> transformmat = perspec*totalex;

	//since we're looking for the points on the Z=0 plane corresponding to the center of the image,
	//remove the third column of the transformation mat
	Matrix3d z0planeTransform;
	z0planeTransform.block<3,2>(0,0) = transformmat.block<3,2>(0,0);
	z0planeTransform.block<3,1>(0,2) = transformmat.block<3,1>(0,3);

	int imwidth, imheight;
	this->imgworker.getInfo(imwidth, imheight);
	Vector3d centerpt_img; centerpt_img << imwidth/2.0, imheight/2.0, 1;
	Vector3d centerpt_gnd = z0planeTransform.fullPivLu().solve(centerpt_img);

	//now in each of the solution vectors, we have X/zc, Y/zc and 1/zc
	//so just divide all of the answers by the third component and fill grndCorners

	grndPt[0] = centerpt_gnd(0)/centerpt_gnd(2);
	grndPt[1] = centerpt_gnd(1)/centerpt_gnd(2);
}

void P5512::getCamGroundCorners(double (&grndCorners)[8]){
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	int width, height;
	this->imgworker.getInfo(width, height);

	Matrix3d perspec = convertZoomToPerspectiveMat(tmpzoom);
	Matrix<double, 3, 4> totalex = this->getTotalExtrinsic();
	
	Matrix<double, 3, 4> transformmat = perspec*totalex;

	//since we're looking for the points on the Z=0 plane corresponding to the corners of the image,
	//remove the third column of the transformation mat
	Matrix3d z0planeTransform;
	z0planeTransform.block<3,2>(0,0) = transformmat.block<3,2>(0,0);
	z0planeTransform.block<3,1>(0,2) = transformmat.block<3,1>(0,3);

	//solve the system for the 4 corners starting from top left and proceeding counter clockwise
	Vector3d TLRHS; TLRHS << 0, 0, 1;
	Vector3d BLRHS; BLRHS << 0, height, 1;
	Vector3d BRRHS; BRRHS << width, height, 1;
	Vector3d TRRHS; TRRHS << width, 0, 1;
	Vector3d TL = z0planeTransform.fullPivLu().solve(TLRHS);
	Vector3d BL = z0planeTransform.fullPivLu().solve(BLRHS);
	Vector3d BR = z0planeTransform.fullPivLu().solve(BRRHS);
	Vector3d TR = z0planeTransform.fullPivLu().solve(TRRHS);

	//now in each of the solution vectors, we have X/zc, Y/zc and 1/zc
	//so just divide all of the answers by the third component and fill grndCorners

	grndCorners[0] = TL(0)/TL(2);
	grndCorners[1] = TL(1)/TL(2);

	grndCorners[2] = BL(0)/BL(2);
	grndCorners[3] = BL(1)/BL(2);

	grndCorners[4] = BR(0)/BR(2);
	grndCorners[5] = BR(1)/BR(2);

	grndCorners[6] = TR(0)/TR(2);
	grndCorners[7] = TR(1)/TR(2);
}

void P5512::getCamInfo(Matrix<double, 3, 4>& extrinsic, Matrix<double, 3, 3>& intrinsic, double& ipan, double& itilt, double& izoom, int& width, int&height){
	this->imgworker.getInfo(width, height);
	double tmpiris;
	bool tmpflip;
	this->ptzworker.get(ipan, itilt, izoom, tmpiris, tmpflip);
	intrinsic = convertZoomToPerspectiveMat(izoom);
	extrinsic = getTotalExtrinsic();
}

bool P5512::getIdle(){
	bool tmp;
	wptmutex.lock();
	tmp = this->idle;
	wptmutex.unlock();
	return tmp;
}

void P5512::getImage(cv::Mat& img){
	this->imgworker.getImage(img);
	return;
}

void P5512::getUndistortedImage(cv::Mat& img){
	cv::Mat tmpimg;
	this->imgworker.getImage(tmpimg);
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	//grab the intrinsic data for the current zoom
	// and convert it to cv::Mat format
	Matrix3d tmpIntrinsic_eig = convertZoomToPerspectiveMat(tmpzoom);
	cv::Mat tmpIntrinsic(3, 3, CV_64FC1);
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			tmpIntrinsic.at<double>(i,j) = tmpIntrinsic_eig(i,j); 
		}
	}
	Matrix<double, 1, 5> tmpDist_eig = convertZoomToDistortionMat(tmpzoom);
	cv::Mat tmpDist(1,5, CV_64FC1);
	for (int i = 0; i < 5; i++){
		tmpDist.at<double>(0,i) = tmpDist_eig(0,i);
	}
	/*Debugging
	cout << "Intrinsic: " << endl << tmpIntrinsic_eig << endl << "Distortion: " << endl << tmpDist_eig << endl;
	cout << "CVIntrinsic: " << endl << tmpIntrinsic << endl << "CVDistortion: " << endl << tmpDist << endl;
	*/
	cv::undistort(tmpimg,img, tmpIntrinsic, tmpDist);
}

//gets transformations from detected april tags
//the transformations take vectors in the Tag frame to the Camera frame
std::map<int, Eigen::Matrix4d> P5512::getAprilTagTransforms(double tagSize){
	//get an image from the camera, convert it to grayscale
	cv::Mat img, grayimg;
	this->getUndistortedImage(img);
	cv::cvtColor(img, grayimg, CV_BGR2GRAY);
	//get the tag detections
	//set up the detector
	AprilTags::TagDetector tag_detector(AprilTags::tagCodes36h11);
	std::vector<AprilTags::TagDetection> detections = tag_detector.extractTags(grayimg);
	//get camera state
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	Matrix3d tmpintrinsic = convertZoomToPerspectiveMat(tmpzoom);
	//cout << "Zoom: " << tmpzoom << endl;
	//cout << "Intrinsic: " << endl << tmpintrinsic << endl;
	// output detections
	std::map<int, Eigen::Matrix4d> tagTransforms;
	for (int i=0; i<detections.size(); i++) {
	 	 // recovering the relative pose requires camera calibration;
	 	 const double fx = tmpintrinsic(0,0); // camera focal length
	 	 const double fy = tmpintrinsic(1,1);
	 	 const double px = tmpintrinsic(0,2); // camera principal point
	 	 const double py = tmpintrinsic(1,2); 
	 	 Eigen::Matrix4d tmpT = detections[i].getRelativeTransform(tagSize, fx, fy, px, py);
		 tagTransforms[detections[i].id] = tmpT;
		/*if (detections[i].id == 0){
			cout << "Relative Transform: " << endl << tmpT << endl;
		}*/
	}
	return tagTransforms;
}

std::map<int, Eigen::Vector3d > P5512::getAprilTagLocs(double tagSize){
	std::map<int, Eigen::Matrix4d> tagTransforms = this->getAprilTagTransforms(tagSize);
	//get position of Tag in April camera frame (z along look, x to right, y down)
	//which is also the camera image frame
	//get the total extrinsic matrix (transforms global vectors to image frame vectors)
	Eigen::Matrix<double, 3, 4> tmpext = getTotalExtrinsic();
	std::map<int, Eigen::Vector3d > tagLocs;
	for (std::map<int, Eigen::Matrix4d>::iterator it = tagTransforms.begin(); it != tagTransforms.end(); it++){
		Eigen::Vector4d originHomogCoords;
		originHomogCoords << 0, 0, 0, 1;
		Eigen::Vector3d tagLocInAprilCamFrame = (it->second*originHomogCoords).block<3,1>(0,0);
		//since tmpext takes vectors from global frame to opencv cam frame, use inverse transform
		Eigen::Vector3d tagLocInGlobalFrame = 
		       tmpext.block<3,3>(0,0).transpose()*(tagLocInAprilCamFrame - tmpext.block<3,1>(0,3));
		//store in tagLocs
		tagLocs[it->first] = tagLocInGlobalFrame;
		//print out the global frame tag location
	//	cout << "Location: " << tagLocInGlobalFrame(0) << " "
	//				 << tagLocInGlobalFrame(1) << " " << tagLocInGlobalFrame(2) << endl;
	}
	return tagLocs;
}

int P5512::check(){
	//print curl version info
	curl_version_info_data* curlversion = curl_version_info(CURLVERSION_NOW);
	printf("---Checking P5512 Connectivity and Responsiveness---\n");
	printf("IP Address: %s\n", ip_addr.c_str());
	printf("cURL version: %s (This code debugged using v7.22.0)\n", curlversion->version);
	printf("cURL verbose - %s\n", (verbose ? "yes": "no"));

	//test connection and get VAPIX version
	CURL* curl = curl_easy_init();
	if (!curl){
		printf("P5512::check(): failed to setup curl with curl_easy_init().\n");
		return -1;
	} else {
		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L); //no progress bar to be displayed
		curl_easy_setopt(curl, CURLOPT_VERBOSE, (verbose ? 1 : 0) ); //decide whether to be verbose
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 5);
		printf("Checking connection to device & supported VAPIX version: \n");
		string url("http://root:uavswarm@");
		url.append(ip_addr);
		url.append("/axis-cgi/param.cgi?action=list&group=Properties.API.HTTP.Version");
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &curl_callback);
		stringstream respstream;
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &respstream);
		CURLcode res = curl_easy_perform(curl);
		if (res != CURLE_OK){
			printf("P5512::check(): libCURL error while checking VAPIX version: \n");
			printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
			curl_easy_cleanup(curl);
			return -1;
		}
		printf("Connection to %s OK.\n", ip_addr.c_str());

		string strresp = respstream.str();
		if (strresp.find("Properties.API.HTTP.Version") == string::npos){
			printf("P5512::check(): Could not get the Vapix version number.\n");
			printf("Possibly an authentication issue?\n");
			return -1;
		}
		printf("Vapix Version: %s\n", strresp.c_str());
		return 0;
	}
	return 0;
}

int P5512::connect(){
	//print curl version info
	printf("---Starting P5512 Controller---\n");
	printf("IP Address: %s\n", ip_addr.c_str());

	if( !this->imgworker.start() || !this->ptzworker.start()){
		printf("P5512::connect(): Error - couldn't start one/both of imgworker/ptzworker");
	}
	this->home();
	boost::mutex readymutex;
	//bool isready = false;
	//boost::condition_variable readycond;
	this->wptthread = new boost::thread(boost::bind(&P5512::wpt_worker, this));
	/*{//scoped lock scope
		//wait on the condition variable
		boost::mutex::scoped_lock readylock(readymutex);		
		while (!isready){
			readycond.wait(readylock);
		}
	}*/
	return 0;
}

void P5512::disconnect(){
	printf("P5512::disconnect: Disconnecting from %s.\n", this->name.c_str());
	if (!this->imgworker.stop(1) || !this->ptzworker.stop(1)){
		printf("P5512::disconnect(): Error - couldn't stop one/both of imgworker/ptzworker");
	}
	this->wptthread->interrupt();
	this->wptthread->timed_join(boost::posix_time::time_duration(0, 0, 1, 0));
	printf("P5512::disconnect: Disconnected from %s.\n", this->name.c_str());
}

P5512::~P5512(){
	CURLGLOBAL--;
	if (CURLGLOBAL == 0){
		curl_global_cleanup();
	}
}

//AXIS P5512 Weird Facts:
//the coordinate description flips when the camera gets past -100 degrees (goes to -80)
//actually the above is useful for testing when the camera is "flipped" or not
//the pan unit actually only has a range of about [-172.2188 to 172.2188]
//i.e. there's a dead zone
//ALSO when the camera has been flipped, the "dead zone" becomes centered around 0 degrees
//i.e. the camera can now go from [-7.7812 -> -180 -> 180 -> 7.7812]

//VERY important: When tilting, if you tilt past -100 degrees the coordinate system flips,
//BUT the COMMAND coordinate system doesn't until the carriage stops moving.
//Thus, I've added a 50ms usleep if we're about to send a new waypoint and the carriage is near a flip point.

void P5512::wpt_worker(){
	std::cout << "P5512::wptworker: Starting up ...                        \r" << std::flush;

	//Note: the wpt thread should be properly exited when interrupted by P5512::disconnect
	//if it is blocking on the resource condition variable
	while(true){
		boost::this_thread::interruption_point();
		list<CamWpt> tmpwpts;
		{//scoped lock scope
			//wait on the condition variable
			boost::mutex::scoped_lock wptlock(wptmutex);
			while (wpts.empty()){
				this->idle = true;
				wptcond.notify_all(); //tell anyone waiting that we're idle	
				wptcond.wait(wptlock); //wait for wpts
			}
			//we have the lock again after waking up
			//and wpts is not empty (due to falling back asleep on wpts.empty above)
			tmpwpts.insert(tmpwpts.end(), this->wpts.begin(), this->wpts.end());
			this->wpts.clear();
		} //release the scoped lock

		//we now have our own list of waypoints.
		//first search for all "clearing" waypoints in the list, and remove all ones before it
		list<CamWpt>::iterator listit = tmpwpts.begin();
		while (listit != tmpwpts.end()){
			if (listit->clear){
				tmpwpts.erase(tmpwpts.begin(), listit);
				listit = tmpwpts.begin();
			}
			listit++;
		}

		//start a thread for the waypoint sentinel
		//it waits on wptcond, and any time it's woken up, it looks through the list of waypoints
		//in this->wpts. If any one of them contain the "clear" flag, it sets "foundclear" and halts the while loop below
		boost::mutex clear_mutex;
		bool clear_interrupt = false;
		boost::thread wptsentinelthread(boost::bind(&P5512::wpt_sentinel, this, boost::ref(clear_mutex), clear_interrupt));

		//now start sending waypoints one by one, each time waiting for the ptz to finish
		//the curl perform does not block, nor does the camera keep a queue of waypoints
		//in other words, we need to keep track of it ourselves.
		double timeelapsed = 0;
		bool resendingwpt = false;
		double wptselpan = 0, wptseltilt = 0;
		double wptpan1 = 0, wpttilt1 = 0, wptpan2 = 0, wpttilt2 = 0;
		CamWpt curwpt = tmpwpts.front();
		while (!tmpwpts.empty() || resendingwpt){
			//check if sentinel interrupted us
			clear_mutex.lock();
			if (clear_interrupt){
				break;
			}
			clear_mutex.unlock();

		
			//usleep(50000); //this delay can help the stability of the camera flipping nonsense
					//if the cams are acting weird, just uncomment this I guess.
			//get the next wpt
			
			if (!resendingwpt){	
				curwpt = tmpwpts.front();		
				tmpwpts.pop_front();
				timeelapsed = 0;		
			
				//print a warning if the waypoint is not in the range pan=[-180, 180] and tilt=[0.-90]
				if (curwpt.tilt < -180 || curwpt.tilt > 0){
					std::cout << "P5512::wptworker: Error: Waypoint specified outside of range." << std::endl;
					std::cout << "P5512::wptworker: Error: Tilt must be within:[-180, 0]" << std::endl;
				}

				//put the waypoint angles in the proper range
				if (curwpt.tilt < -90){
					//right it
					curwpt.tilt = -180 - curwpt.tilt;
					curwpt.pan += 180; // fix the pan angle
				}
				while (curwpt.pan > 180)
					curwpt.pan -= 360;
				while (curwpt.pan < -180)
					curwpt.pan += 360;

				//get the two possible waypoints (in terms of *actual* ptz carriage positions)
				wptpan1 = curwpt.pan; //the nonflipped one
				wpttilt1 = curwpt.tilt;
				wptpan2 = wptpan1 + 180; //the flipped one
				while (wptpan2 > 180)
					wptpan2 -= 360;
				wpttilt2 = -180 - wpttilt1;
			}

			//get the camera state
			double curpan, curtilt, curzoom, curiris;
			bool curflipped;
			this->ptzworker.get(curpan, curtilt, curzoom, curiris, curflipped);

			//change it to the actual carriage position
			//are we flipped?
			double curtilt_carriage, curpan_carriage;
			if (curflipped){
				curtilt_carriage = -180 - curtilt;
				curpan_carriage = curpan + 180;
				while (curpan_carriage > 180)
					curpan_carriage -= 360;		
			} else {
				curtilt_carriage = curtilt;
				curpan_carriage = curpan;
			}
		
			//decide whether the wpt pan angle is in the dead zone; if so, we're forced
			//also if the last waypoint failed, we want to keep the same selection
			double pancmd, panspeed, tiltcmd, tiltspeed;
			if (!resendingwpt){
				if ( fabs(wptpan1 - 180) < DEADZONE || fabs(wptpan1 + 180) < DEADZONE || curwpt.forceflip == 1){
					//we have to use wpt2
					wptselpan = wptpan2;
					wptseltilt = wpttilt2;
				
				} else if ( fabs(wptpan2 - 180) < DEADZONE || fabs(wptpan2 + 180) < DEADZONE || curwpt.forceflip == -1){
					//we have to use wpt1
					wptselpan = wptpan1;
					wptseltilt = wpttilt1;
				} else {
					//we can choose based on distance.
					double wpt1dist = fabs(wptpan1 - curpan_carriage) + fabs(wpttilt1 - curtilt_carriage);
					double wpt2dist = fabs(wptpan2 - curpan_carriage) + fabs(wpttilt2 - curtilt_carriage);
					if (wpt1dist < wpt2dist){
						wptselpan = wptpan1;
						wptseltilt = wpttilt1;
					} else {
						wptselpan = wptpan2;
						wptseltilt = wpttilt2;
					}
				}
			}
			
			pancmd = curpan + (wptselpan - curpan_carriage);
			panspeed = (curwpt.dt-timeelapsed <= 0 ? MAXSPEED-.01 : fabs( (wptselpan - curpan_carriage)/(curwpt.dt-timeelapsed)) );
			tiltcmd = curtilt + (curflipped ? -1 : 1)*(wptseltilt - curtilt_carriage);
			tiltspeed = (curwpt.dt-timeelapsed <= 0 ? MAXSPEED-.01 : fabs( (wptseltilt - curtilt_carriage)/(curwpt.dt-timeelapsed)) );

			//translate the speeds 
			panspeed = convertDegPerSecToAxis_Pan(panspeed);
			if (panspeed == 1.0 || panspeed == 100.0){
				std::cout << "P5512::wptworker: Warning - Pan saturating on speed." << std::endl;
				/*if (panspeed == 100.0){ //correct the time it will take to prevent timeout unnecessarily
					curwpt.time = max(curwpt.time,  fabs(wptselpan - curpan_carriage)/93.167096);
				}*/
			}
			tiltspeed = convertDegPerSecToAxis_Tilt(tiltspeed);
			if (tiltspeed == 1.0 || tiltspeed == 100.0){
				std::cout << "P5512::wptworker: Warning - Tilt saturating on speed." << std::endl;
				/*if (tiltspeed == 100.0){ //correct the time it will take to prevent timeout unnecessarily
					curwpt.time = max(curwpt.time,  fabs(wptseltilt - curtilt_carriage)/93.167096);
				}*/
			}			
			
			//send to the camera
			#ifdef DEBUGPRINTS
			printf("P5512::wptworker: About to send a waypoint.\n");			
			printf("P5512::wptworker: I think the state of the camera is:\n");
			printf("Pan: %f Tilt: %f Flipped: %s\n", curpan, curtilt, curflipped ? "true":"false");
			printf("PanCarriage: %f TiltCarriage: %f\n", curpan_carriage, curtilt_carriage);
			printf("WptPanCarriage: %f WptTiltCarriage: %f\n", wptselpan, wptseltilt);
			#endif
			//std::cout << "P5512::wptworker: sending a waypoint. # wpts in tmp queue: " << tmpwpts.size() << std::endl;

			this->ptzworker.sendCmd(pancmd, panspeed, tiltcmd, tiltspeed, curwpt.zoom);
	
			//track the completion of the waypoint 
			timespec starttime, curtime;
			clock_gettime(CLOCK_REALTIME, &starttime); 			

			//now poll and check whether the camera has finished its waypoint 
		//	double tilts[3];
		//	double pans[3];
		//	double times[3];
			double prevpandist = 1e20, prevtiltdist = 1e20;
			int stoppedcount = 0;
			while(true){
				this->ptzworker.getNew(curpan, curtilt, curzoom, curiris, curflipped);
				//get the current time
				clock_gettime(CLOCK_REALTIME, &curtime); 
				double tmptimeelapsed = timeelapsed + curtime.tv_sec + curtime.tv_nsec/1e9 
									- (starttime.tv_sec + starttime.tv_nsec/1e9);			

				//calculate carriage distance
				if (curflipped){
					curtilt_carriage = (-180 - curtilt);
					curpan_carriage = (curpan + 180);
					while (curpan_carriage > 180)
						curpan_carriage -= 360;		
				} else {
					curtilt_carriage = curtilt;
					curpan_carriage = curpan;
				}


				//angular manhattan
				double pandist = fabs(curpan_carriage-wptselpan);	
				double tiltdist = fabs(curtilt_carriage-wptseltilt);
				double zoomdist = fabs(curzoom - curwpt.zoom);
				//calculate great-circle distance
				/*double greatcircle = 
				fabs(				
				atan2(
				sqrt( 
					 (cos(wpttilt)*sin(wptpan-tmppan))
					*(cos(wpttilt)*sin(wptpan-tmppan))
					+
					  (cos(tmptilt)*sin(wpttilt) - sin(tmptilt)*cos(wpttilt)*cos(wptpan-tmppan))
					 *(cos(tmptilt)*sin(wpttilt) - sin(tmptilt)*cos(wpttilt)*cos(wptpan-tmppan))
				), 
					sin(tmptilt)*sin(wpttilt) + cos(tmptilt)*cos(wpttilt)*cos(wptpan-tmppan)
				));*/
				


				//there's an issue using this camera where if you have a waypoint that ends near a flip point
				//and you start moving too early afterwards, there's a chance the camera might flip its coordinates
				//before you have time to respond and you'll start moving in the wrong direction
				//check here if the pan or tilt is going the opposite direction
				//if it is, we done goofed and we should send the waypoint again (with a new updated ptz measurement)
				
				if (prevpandist < pandist || prevtiltdist < tiltdist){
					std::cout << "P5512::wptworker: Resending the waypoint, the camera flipped" << std::endl;
					resendingwpt = true;
					timeelapsed = tmptimeelapsed; //used to change the desired speed to reflect time elapsed
					break;
				}
				
				if (prevpandist == pandist && prevtiltdist == tiltdist) stoppedcount++;
				else stoppedcount = 0;
				
				//here we use an accuracy for zoom that scales with value because the p5512 zoom accuracy is non constant.
				if (pandist < 0.05 && tiltdist < 0.05 && zoomdist < curzoom/300.0 && stoppedcount >= STOPPEDTHRESH ){
					//printf("P5512::wptworker: Finished a waypoint\n");
				#ifdef DEBUGPRINTS
					printf("Pan: %f Tilt: %f Zoom: %f Flipped: %s\n", curpan, curtilt, curzoom, curflipped ? "true":"false");
					printf("PanCarriage: %f TiltCarriage: %f\n", curpan_carriage, curtilt_carriage);
					printf("WptPanCarriage: %f WptTiltCarriage: %f\n", wptselpan, wptseltilt);
				#endif
					resendingwpt = false;
					break;
				}

				prevpandist = pandist; prevtiltdist = tiltdist;
				if (tmptimeelapsed > curwpt.timeout){
					printf("P5512::wptworker: Waypoint timed out! We may have lost the flip state. Need to home.\n");
					printf("The carriage is at: \n");
					printf("Pan: %f  Tilt: %f Zoom: %f  Flip: %s\n", curpan_carriage, curtilt_carriage, curzoom, curflipped ? "true" : "false");
					printf("And we requested: \n");
					printf("Pan: %f  Tilt: %f Zoom: %f\n", wptselpan, wptseltilt, curwpt.zoom);
					resendingwpt = false;	
					break;
				} 

				//check if sentinel interrupted us
				clear_mutex.lock();
				if (clear_interrupt){
					break;
				}
				clear_mutex.unlock();
			}//polling while loop
		}//while the waypoint queue is nonempty
		wptsentinelthread.interrupt();
		wptsentinelthread.join();
	}//overall thread loop
}


//this function is called by wpt_worker 
//it orchestrates the following of 
void P5512::wpt_sentinel(boost::mutex& clearmutex, bool& clearinterrupt){
	while(true){
		boost::this_thread::interruption_point();
		{//scoped lock scope
			//wait on the condition variable
			boost::mutex::scoped_lock wptlock(wptmutex);
			while (wpts.empty()){
				wptcond.notify_all(); 	
				wptcond.wait(wptlock); //wait for wpts
			}
			//we have the lock again after waking up
			//and wpts is not empty (due to falling back asleep on wpts.empty above)
			//check if any of the waypoints has the clear flag
			list<CamWpt>::iterator listit = this->wpts.begin();
			while (listit != this->wpts.end()){
				if (listit->clear){
					clearmutex.lock();
					clearinterrupt = true;
					clearmutex.unlock();
					return; //we're done here
				}
				listit++;
			}
		} //release the scoped lock
	}
}


void P5512::addWaypoints(list<CamWpt> addwpts){
	this->wptmutex.lock();

	while(addwpts.begin() != addwpts.end()){
		this->wpts.push_back(this->convertWaypoint(addwpts.front()));
		addwpts.pop_front();
	}
	this->wptmutex.unlock();	
	wptcond.notify_all(); //notify anyone waiting on the waypoint queue to become nonempty
}

CamWpt P5512::convertWaypoint(CamWpt& toConvert){
		//if it's already a ptz command, just use it
		if (toConvert.wpttype == CAMWPT_PTZ){
			return toConvert;
		}
		
		Vector4d lookpt;
		if (toConvert.wpttype == CAMWPT_2W || toConvert.wpttype == CAMWPT_2Z){
			lookpt << toConvert.x, toConvert.y, 0.0, 1.0;
		} else {
			lookpt << toConvert.x, toConvert.y, toConvert.z, 1.0;
		}
		Vector3d camToLookPt = this->calib_baseExtrinsicMat*lookpt; //transforms a global frame vector into camera base frame
		double tmppan = atan2(camToLookPt(0), camToLookPt(2));
		double tmptilt = -asin(camToLookPt(1)/camToLookPt.norm()); //asin returns the angle from -pi/2 to pi/2
		if (tmptilt > 0){
			tmptilt = 0;//cap it at zero.
		}
		tmppan *= 180.0/M_PI;
		tmptilt *= 180.0/M_PI;

		//if the command involves zoom, we don't need to do any more work, return
		if (toConvert.wpttype == CAMWPT_2Z || toConvert.wpttype == CAMWPT_3Z){
			CamWpt ret(CAMWPT_PTZ, tmppan, tmptilt, toConvert.zoom, toConvert.dt, toConvert.timeout, toConvert.clear);
			return ret;
		}
		//if we're here, the command involves a width at distance, so we need to convert that to a zoom	
		//create a hypothetical two vectors and find the scaling that makes them more or less in the right position on the screen
		//and then use that zoom

		double ptdist = camToLookPt.norm();
		Vector3d lookpt_left, lookpt_right, testvec;
		lookpt_left << -toConvert.w/2.0, 0.0, ptdist;
		lookpt_right << toConvert.w/2.0, 0.0, ptdist;
		testvec = lookpt_left/lookpt_left(2) - lookpt_right/lookpt_right(2);
			
		//get the width with 1x zoom
		double minzoom = 1.0;
		//get the width with 12x zoom
		double maxzoom = 10000.0;
		//do bisection search
		int imw, imh;
		this->imgworker.getInfo(imw, imh);
		while(fabs(maxzoom - minzoom)/minzoom > .01){
			double tmpzoom = (maxzoom+minzoom)/2.0;
			Matrix3d perspec = convertZoomToPerspectiveMat(tmpzoom);
			double tmpwidth = (perspec*testvec).norm();
			if (tmpwidth - imw> 0){
				maxzoom = tmpzoom;
			} else {
				minzoom = tmpzoom;
			}
		}
		CamWpt ret(CAMWPT_PTZ, tmppan, tmptilt, (maxzoom+minzoom)/2.0, toConvert.dt, toConvert.timeout, toConvert.clear);
		return ret;
}


double P5512::convertDegPerSecToAxis_Tilt(double degpersec){
	if (degpersec <= this->calib_tiltspeeds[0]){
		return (calib_axisspeeds[0]/calib_tiltspeeds[0])*degpersec;
	} else if (degpersec >= this->calib_tiltspeeds[10]){
		return 100.0;
	} else {
		int ind = -1;
		for (int i = 0; i < 10; i++){
			if (degpersec >= this->calib_tiltspeeds[i] && degpersec < this->calib_tiltspeeds[i+1]){
				ind = i;
				break;
			}
		}
		return (calib_axisspeeds[ind+1] - calib_axisspeeds[ind])/(calib_tiltspeeds[ind+1] - calib_tiltspeeds[ind]) * (degpersec - calib_tiltspeeds[ind]) + calib_axisspeeds[ind];
	}
	
}

double P5512::convertDegPerSecToAxis_Pan(double degpersec){
	if (degpersec <= this->calib_panspeeds[0]){
		return (calib_axisspeeds[0]/calib_panspeeds[0])*degpersec;
	} else if (degpersec >= this->calib_panspeeds[10]){
		return 100.0;
	} else {
		int ind = -1;
		for (int i = 0; i < 10; i++){
			if (degpersec >= this->calib_panspeeds[i] && degpersec < this->calib_panspeeds[i+1]){
				ind = i;
				break;
			}
		}
		return (calib_axisspeeds[ind+1] - calib_axisspeeds[ind])/(calib_panspeeds[ind+1] - calib_panspeeds[ind]) * (degpersec - calib_panspeeds[ind]) + calib_axisspeeds[ind];
	}
}

Matrix<double, 1, 5> P5512::convertZoomToDistortionMat(double axiszoom){
	//std::cout << "axiszoom " << axiszoom << std::endl;
	if (axiszoom <= 1.0){
		return this->calib_distortionCoeffMats[0];
	} else if (axiszoom >= 10000.0){
		return this->calib_distortionCoeffMats[10];
	} else {
		int ind = -1;
		for (int i = 0; i < 10; i++){
			if (axiszoom >= this->calib_axiszooms[i] && axiszoom < this->calib_axiszooms[i+1]){
				ind = i;
				break;
			}
		}
		return (calib_distortionCoeffMats[ind+1] - calib_distortionCoeffMats[ind])/(double)(calib_axiszooms[ind+1] - calib_axiszooms[ind]) * (double)(axiszoom - calib_axiszooms[ind]) + calib_distortionCoeffMats[ind];
	}
}

Matrix3d P5512::convertZoomToPerspectiveMat(double axiszoom){
	//std::cout << "axiszoom " << axiszoom << std::endl;
	if (axiszoom <= 1.0){
		return this->calib_intrinsicMats[0];
	} else if (axiszoom >= 10000.0){
		return this->calib_intrinsicMats[10];
	} else {
		int ind = -1;
		for (int i = 0; i < 10; i++){
			if (axiszoom >= this->calib_axiszooms[i] && axiszoom < this->calib_axiszooms[i+1]){
				ind = i;
				break;
			}
		}
		return (calib_intrinsicMats[ind+1] - calib_intrinsicMats[ind])/(double)(calib_axiszooms[ind+1] - calib_axiszooms[ind]) * (double)(axiszoom - calib_axiszooms[ind]) + calib_intrinsicMats[ind];
	}
}


bool P5512::home(){ //this should only be called by wptworker and calibrate
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	printf("P5512::home(): Attempting to home...(may timeout if flip state is wrong)");
	this->ptzworker.sendCmd(0, 100, 0, 100, 1);
	sleep(3);
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	if (fabs(tmppan) > 1){
		//it was flipped
		printf("P5512::home: Tilt unit was flipped. Unflipping.");
		this->ptzworker.sendCmd(0, 100, -180, 100, 1);
		sleep(3);
		this->ptzworker.sendCmd(0, 100, 0, 100, 1);
		sleep(3);
		this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	}
	if (fabs(tmppan) > 1){
		//serious error. Cannot figure out the flip state.
		printf("P5512::home: Cannot figure out flip state. Error.");
		return false;
	} 
	this->ptzworker.setFlipState(false); //ensure the ptzworker knows the camera is homed
	this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	printf("P5512::home: PTZ of the camera should be at pan=0 and tilt=0, unflipped.\n");
	printf("P5512::home: Pan: %f  Tilt: %f", tmppan, tmptilt);
	return true;
}

bool P5512::calibrateSpeeds(double (&axisspeeds)[11], double (&panspeeds)[11], double (&tiltspeeds)[11]){
	printf("P5512::calibrateSpeeds: Homing...\n");
	if (!this->home()){
		printf("P5512::calibrateSpeeds: Homing didn't work...\n");
		return false;
	}
	printf("P5512::calibrateSpeeds: Getting pan/tilt speeds...\n");
	
	wptmutex.lock();
	//grab control of the mutex and just send commands directly here
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	printf("P5512::calibrateSpeeds: Getting pan speeds...\n");
	this->ptzworker.sendCmd(-90, 100, 0, 100, 1);
	do {
		printf("P5512::calibrateSpeeds: Sleeping for 3 seconds while the camera pans to -90 degrees...\n");
		sleep(3);
		this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	} while(fabs(tmppan + 90) > 0.3);

	//get speeds for 1, 25, 50, 75, 100 for pan
	for (int i = 0, j=0; i <= 100; i+= 10, j++){
		sleep(3); //this is somewhat unnecessary, more useful for tilt below
		double testspeed;
		if (i == 0){
			testspeed = 1;
		} else{
			testspeed = i;
		}
		printf("P5512::calibrateSpeeds: Getting pan speed %f\n", testspeed);
		double pangoal = tmppan < 0 ? 90 : -90;
		this->ptzworker.sendCmd(pangoal, testspeed, 0, 0, 1);

		timespec starttime, curtime;
		clock_gettime(CLOCK_REALTIME, &starttime);
		while(true){
			//get the next new available ptz recorded pt
			this->ptzworker.getNew(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
			clock_gettime(CLOCK_REALTIME, &curtime);
			if (fabs(pangoal - tmppan) < 0.3){
				break;
			}
		}
		double elapsedtime = curtime.tv_sec + curtime.tv_nsec/1e9 - (starttime.tv_sec + starttime.tv_nsec/1e9);
		axisspeeds[j] = (double)testspeed;
		panspeeds[j] = 180.0/elapsedtime;
	}

	printf("P5512::calibrateSpeeds: Getting tilt speeds...\n");
	this->ptzworker.sendCmd(0, 100, 0, 100, 1);
	do {
		printf("P5512::calibrateSpeeds: Sleeping for 3 seconds while the camera pans to 0 degrees...\n");
		sleep(3);
		this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	}while(fabs(tmppan) > 0.3);
	//get speeds for 1, 25, 50, 75, 100 for tilt
	for (int i = 0, j=0; i <= 100; i+= 10, j++){
		sleep(3); //need to sleep to make sure the reference frame has flipped
		double testspeed;
		if (i == 0){
			testspeed = 1;
		} else{
			testspeed = i;
		}
		printf("P5512::calibrateSpeeds: Getting tilt speed %f\n", testspeed);
		this->ptzworker.sendCmd(0, 0, -180, testspeed, 1);
		timespec starttime, curtime;
		clock_gettime(CLOCK_REALTIME, &starttime);
		bool paststart = false;
		while(true){
			this->ptzworker.getNew(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
			clock_gettime(CLOCK_REALTIME, &curtime);
			if (tmptilt < -1){
				paststart = true;
			}
			if (fabs(tmptilt) < 0.3 && paststart){
				break;
			}		
		}
		double elapsedtime = curtime.tv_sec + curtime.tv_nsec/1e9 - (starttime.tv_sec + starttime.tv_nsec/1e9);
		tiltspeeds[j] = 180.0/elapsedtime;
	}

	printf("P5512::calibrateSpeeds: Done, sending back home.\n");
	this->ptzworker.sendCmd(0, 100, 0, 100, 1);
	do{
		this->ptzworker.sendCmd(0, 100, 0, 100, 1);
		printf("P5512::calibrateSpeeds: Sleeping for 3 seconds while the camera tilts to 0 degrees...\n");
		sleep(3);
		this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
	}while(fabs(tmptilt) > 0.5);
	//relinquish control of the waypoint and calibcurl
	wptmutex.unlock();
	return true;
}

bool P5512::calibrateIntrinsics(int numBoards, int numInnerCornersHor, int numInnerCornersVer, double distHor, double distVer, double calibPan, double calibTilt, double (&axiszooms)[11], boost::array<Matrix3d, 11>& intrinsicMatArray, boost::array<Matrix<double, 1, 5>, 11>& distortionCoeffArray){
	

	int numCorners = numInnerCornersHor*numInnerCornersVer;
	vector<cv::Point3f> obj;
	for(int j=0;j<numCorners;j++){
		obj.push_back(cv::Point3f( distVer*(j/numInnerCornersHor), distHor*(j%numInnerCornersHor), 0.0f));
	}
	cv::Size board_sz = cv::Size(numInnerCornersHor, numInnerCornersVer);

	//loop over the various zoom states
	for (int zm = 0, i = 0; zm <= 10000; zm+= 1000, i++){
		printf("P5512::calibrateIntrinsics: Doing intrinsic calibration for zoom = %d\n", zm);
		{ //start lock scope
			boost::mutex::scoped_lock wptlock(wptmutex);		
			while (!this->idle){	//wait until the ptz is idle	
				wptcond.wait(wptlock);
			}
			printf("P5512::calibrateIntrinsics: Zooming camera.\n");
			this->wpts.clear(); //make sure there are no waypoints in the queue
			if (zm == 0){
				CamWpt wpt1(CAMWPT_PTZ, calibPan, calibTilt, 1, 0.1, 5, false);
				this->wpts.push_back(wpt1);
			} else {
				CamWpt wpt1(CAMWPT_PTZ, calibPan, calibTilt, zm, 0.1, 5, false);
				this->wpts.push_back(wpt1);
			}
		} //end lock scope
		//send it home by notifying on wptcond
		wptcond.notify_all();
		sleep(1); //let the waypoint cmd worker grab the waypoint
		{ //start lock scope
			boost::mutex::scoped_lock wptlock(wptmutex);		
			while (!this->idle){	//wait until the ptz is idle	
				wptcond.wait(wptlock);
			}
		}
		vector<vector<cv::Point3f> > object_points;
		vector<vector<cv::Point2f> > image_points;
		int successes = 0;
		cv::Mat image, grayscaleImage, undistortedImage; //the mats to store our images
		this->imgworker.getImage(image); //just to get the size of it for moving the windows
		printf("P5512::calibrateIntrinsics: Getting image every 2 seconds...\n");
		timespec prevtime, curtime;
		clock_gettime(CLOCK_REALTIME, &prevtime);
		cv::namedWindow("Calibration Video");
		cv::moveWindow("Calibration Video", 0, 0);
		cv::namedWindow("Calibration Corners");
		cv::moveWindow("Calibration Corners", image.size().width, 0);
		while (successes < numBoards){
			this->imgworker.getImage(image);
			cv::cvtColor(image, grayscaleImage, CV_BGR2GRAY);
			cv::imshow("Calibration Video", grayscaleImage);
			char resp = cv::waitKey(1);
			if (resp == 'q'){
				printf("P5512::calibrateIntrinsics: Leaving calibration.\n");
				return false;
			}
			clock_gettime(CLOCK_REALTIME, &curtime);
			if (curtime.tv_sec + curtime.tv_nsec/1e9 - prevtime.tv_sec - prevtime.tv_nsec/1e9 > 2.0){
				vector<cv::Point2f> corners;
				bool found = cv::findChessboardCorners(image, board_sz, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
				if(found) {
					printf("P5512::calibrateIntrinsics: Chessboard found!\n");
					cv::cornerSubPix(grayscaleImage, corners, cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
					cv::drawChessboardCorners(grayscaleImage, board_sz, corners, found);
					cv::imshow("Calibration Corners", grayscaleImage);
					cv::waitKey(1);
					image_points.push_back(corners);
					object_points.push_back(obj);
					successes++;
		
					if(successes>=numBoards)
						break;
				} else {
					printf("P5512::calibrateIntrinsics: Didn't find chessboard.\n");
				}
				prevtime = curtime;
			}
		}
		cv::destroyAllWindows();
		cv::waitKey(50);
		cv::Mat intrinsic(3, 3, CV_32FC1);
		cv::Mat distCoeffs;
		vector<cv::Mat> rvecs;
		vector<cv::Mat> tvecs;
		cv::calibrateCamera(object_points, image_points, image.size(), intrinsic, distCoeffs, rvecs, tvecs);
		//check the calibration at this zoom
		printf("P5512::calibrateIntrinsics: Does the calibration at this level look OK (y/n/q)?");
		fflush(stdout);
		sleep(2);
		cv::namedWindow("Undistorted", CV_WINDOW_AUTOSIZE);
		cv::namedWindow("Distorted", CV_WINDOW_AUTOSIZE);
		cv::moveWindow("Distorted", 0, 0);
		cv::moveWindow("Undistorted", image.size().width, 0);
		bool calib_ok = false;
		
		while (true){
			this->imgworker.getImage(image);
			cv::undistort(image, undistortedImage, intrinsic, distCoeffs);
			cv::imshow("Undistorted", undistortedImage);
			cv::imshow("Distorted", image);
			char resp = cv::waitKey(1);
			if (resp == 'y'){
				calib_ok = true;
				break;
			}else if (resp == 'n'){
				calib_ok = false;
				break;
			} else if (resp == 'q'){
				printf("P5512::calibrateIntrinsics: Leaving calibration.\n");
				return false;
				break;
			} else {
				continue;
			}
		}
	
		cv::destroyAllWindows();
		cv::waitKey(50);
		if (!calib_ok){
			printf("P5512::calibrateIntrinsics: Calibration not OK, trying again at this zoom.\n");
			zm-= 500;
			i--;
			continue;
		}	
		printf("P5512::calibrateIntrinsics: Calibration OK, moving to next zoom level.\n");
		Matrix3d eig_intrinsic; 
		Matrix<double, 1, 5> eig_distortion;
		for(int j = 0; j < 9; j++){
			eig_intrinsic(j/3, j%3) = intrinsic.at<double>(j/3, j%3);
		}
		for (int j = 0; j < 5; j++){
			eig_distortion(j) = distCoeffs.at<double>(0, j);
		}
		distortionCoeffArray[i] = eig_distortion;
		intrinsicMatArray[i] = eig_intrinsic;
		axiszooms[i] = (double) (zm == 0 ? 1 : zm);		
	}
	printf("P5512::calibrateIntrinsics: Intrinsic calibration done.\n");
	return true;
}

bool P5512::calibratePositionWithApril(int numSamples, int tagID, Vector3d& refpos, boost::mutex& refpos_mutex, Matrix<double, 3, 4>& results_baseExtrinsic){
	double tmppan, tmptilt, tmpzoom, tmpiris;
	bool tmpflip;
	printf("Please move the vicon-dotted tag around in RAVEN in view of the camera...\n");
	printf("Note: Tags *must* have side lengths 0.16m\n");
	Vector4d homogOrigin;
	homogOrigin << 0, 0, 0, 1;
	MatrixXd globalFrameVectors(numSamples, 4);
	MatrixXd baseFrameVectors(numSamples, 4);
	int samplesCollected = 0;
	while(samplesCollected < numSamples){
		//get vicon data
		refpos_mutex.lock();
		Vector3d tagPosVicon = refpos;
		refpos_mutex.unlock();
		//get camera state and detect tags
		this->ptzworker.get(tmppan, tmptilt, tmpzoom, tmpiris, tmpflip);
		double tagSize = 0.16;
		std::map<int, Eigen::Matrix4d> tagTransforms = this->getAprilTagTransforms(tagSize);
		std::map<int, Eigen::Matrix4d>::iterator it = tagTransforms.find(tagID);
		//if the reference tag wasn't found, try again
		if (it == tagTransforms.end()){
			continue;
		}
		//now it->second is a transform from april tag coordinates to camera image coordinates (homogeneous coordinates)
		Vector4d tagPosInCamFrame = it->second*homogOrigin;
		//cout << "Tag Pos Cam Frame: " << tagPosInCamFrame.transpose() << endl;
		//also need the transform from Base coords to Image frame coords (3d coordinates)
		Matrix3d baseToImg = getBaseToImageFrame(tmppan, tmptilt);
		//expand it to a 4d homog transform
		Matrix4d baseToImgHomog = Matrix4d::Zero();
		baseToImgHomog.block<3,3>(0,0) = baseToImg;
		baseToImgHomog(3,3) = 1;
		//transform to form a noisy measurement in the estimation problem
		Vector4d tagPosInBaseFrame = baseToImgHomog.transpose()*tagPosInCamFrame;
		//cout << "Tag Pos in Base Frame: " << tagPosInBaseFrame.transpose() << endl;
		Vector4d tagPosInGlobalFrame;
		tagPosInGlobalFrame.block<3,1>(0,0) = tagPosVicon;
		tagPosInGlobalFrame(3) = 1.0;
		//cout << "Tag Pos in Global Frame: " << tagPosInGlobalFrame.transpose() << endl;
		//put them in the big matrices
		globalFrameVectors.block<1,4>(samplesCollected, 0) = tagPosInGlobalFrame.transpose();
		baseFrameVectors.block<1,4>(samplesCollected, 0) = tagPosInBaseFrame.transpose();
		samplesCollected++;
		printf("Samples Collected: %d / %d\n", samplesCollected, numSamples);
		usleep(250000);//sleep for 1/4 second
	}
//	ofstream fileout;
//	fileout.open("BaseFrameVectors.log");
//	fileout << baseFrameVectors;
//	fileout.close();
//	fileout.open("GlobalFrameVectors.log");
//	fileout << globalFrameVectors;
//	fileout.close();
	//cout << "Base Frame Vectors" << endl << baseFrameVectors << endl << "Global Frame Vectors" << endl << globalFrameVectors << endl;
	//we now have numSamples vectors in the global frame with corresponding noisy ones in the base frame
	//oo baby it's least squares time up in heeeeeere
	Matrix4d leastSquaresGlobalToBase = (globalFrameVectors.jacobiSvd(ComputeThinU | ComputeThinV).solve(baseFrameVectors)).transpose();
	results_baseExtrinsic = leastSquaresGlobalToBase.block<3,4>(0,0);	
	cout << "Base Extrinsic: " << endl << results_baseExtrinsic << endl;
	return true;
}







/*
 * This function is deprecated. Use calibratePositionWithApril above.
 */
/*
bool P5512::calibratePosition(int ransactsts, int ransacsize, double ransacthresh, int opttries, Vector3d& refpos, boost::mutex& refpos_mutex, Vector3d& results_position, Matrix3d& results_rotation){

	printf("Please complete the following steps:\n");
	printf("1) Open up a web browser to IP Address: %s\n", ip_addr.c_str());
	printf("2) Place the VICON marker in a location (lots of variation in height and 2D position is good)\n");
	printf("3) Hit enter to record the position of the marker.\n");
	printf("4) Use the web browser camera interface to make the camera centered on the reference marker (zoom in to get more accuracy)\n");
	printf("4) Once the camera is centered on the point and has fully stopped moving, please press enter (I will prompt you)\n");
	printf("5) I will record the corresponding pan/tilt. I will prompt you if you are done at this point (minimum 10 points).\n");
	printf("6) Once you are done, I will solve for the camera's position and base orientation.\n");
	printf("Note: Please input a maximum of 1000 reference points.\n");
	double pans[1000], tilts[1000];
	Matrix<double, 3, 1000> pts;
	int numrefpts = 0;
	int numread = 0;
	while(true){
		printf("Please place the reference point, center the camera on it, and enter 'done' when done.\n");
		printf("The 'done' bit is a kludge to get stupid multithreaded io problems resolved quickly for the demo and should be removed shortly.\n");
		string tmpresp;
		while(tmpresp.compare("done") != 0){
			cin.sync();
			cin.clear();
			cin >> tmpresp;
		}
		refpos_mutex.lock();
		pts.block<3,1>(0,numrefpts) = refpos;
		refpos_mutex.unlock();
		this->ptzmutex.lock();
		pans[numrefpts] = this->pan;
		tilts[numrefpts] = this->tilt;
		this->ptzmutex.unlock();
		printf("Got the following: ");
		cout << "ref pt: " << pts.block<3,1>(0,numrefpts) << " pan " << 
			pans[numrefpts] << " tilt  " << tilts[numrefpts] << std::endl;
		numread = 0;
		bool DONE = false;
		char resp;
		if (numrefpts >= ransacsize-1){
			printf("Done entering reference points (y/n)?");
			while (true){
				cin.sync();
				cin.clear();
				cin >> resp;
				if (resp == 'y'){
					DONE = true;
					break;
				} else if (resp == 'n'){
					DONE = false;
					break;
				} else {
					printf("\n");
					continue;
				}
			}
		}
		if (DONE){
			break;
		}
		numrefpts++;
	}

	//go through and calculate all unit vectors
	//the frame that these unit vectors are in
	//is Z pointing out of the camera lens at pan, tilt = 0, X right looking at the image, and Y down.
	MatrixXd globalframe_origintopts = pts.block(0,0,3,numrefpts);
	MatrixXd camframe_unit_camtopts(3,numrefpts);
	for (int i = 0; i < numrefpts; i++){
		double tmptilt = tilts[i], tmppan = pans[i];
		//in the image frame, the unit vector is [0,0,1];
		Vector3d camFrameUnit;
		camFrameUnit << 0, 0, 1;
		Matrix3d baseToImg = this->getBaseToImageFrame(tmppan, tmptilt);
	//	 this was commented out after realizing that camera images flip
	//	   it shouldn't actually matter for this part of the code
   	//	   because the unit vector is along z axis 
	//	   but I removed it for uniformity	
	//	if (tmptilt < -90){
	//		tmptilt = -180 - tmptilt;
	//		tmppan += 180;
	//		while(tmppan > 180)
	//			tmppan -= 360;
	//	}
	//	//flip tilts to be positive
	//	tmptilt = -tmptilt;
	//	tmptilt *= M_PI/180.0;
	//	tmppan *= M_PI/180.0;
		
		//now insert the unit vector in the base camera frame
		//see comment at top of file
		//since rotX*rotY(pan)
		camframe_unit_camtopts.block<3,1>(0,i) = baseToImg.transpose()*camFrameUnit; // << -cos(tmptilt)*sin(tmppan), -sin(tmptilt) , cos(tmptilt)*cos(tmppan);
		
	}

	//time to figure out what parameters our algorithm will be using.
	printf("Done getting reference points. Starting RANSAC.\n");
	
	//we're done inputting points
	//time to figure out what the position/angle of the camera is
	//we have at least 4 points
	//algorithm:
	//minimize a function to solve for the location of the camera
	//solve for the angle of the camera given the point
	//use ransac to determine which orientation is the best
	//use eigen library
	
	//start ransac
	srand(time(NULL));
	vector<int> votes;
	vector< vector< int> > voters;
	vector<Matrix3d> rotations;
	vector<Vector3d> positions;
	for (int i = 0; i < ransactsts; i++){
		cout << "RANSAC try " << i << endl;
		cout << "Getting sample set of size " << ransacsize << endl;
		MatrixXd tst_camframe_unit_camtopts(3,ransacsize); //unit vector from camera to a point expressed in cam frame
		MatrixXd tst_globalframe_origintopts(3,ransacsize); //vector from origin to point in global frame
		//pick ransacsize reference points at random
		int indices[ransacsize];
		int ind;
		for (int j = 0; j < ransacsize; j++){
			while(true){
				ind = rand()%numrefpts;
				bool found = false;
				for (int k = 0; k < j; k++){
					if (indices[k] == ind)
						found = true;
				}
				if (!found)
					break;
			}
			indices[j] = ind;
			tst_camframe_unit_camtopts.block<3,1>(0, j) = camframe_unit_camtopts.block<3,1>(0, ind);
			tst_globalframe_origintopts.block<3,1>(0, j) = globalframe_origintopts.block<3,1>(0, ind);
		}
		//we now have 4 points, 4 indices, 4 unit vectors
		//solve the least squares problem to get the distances to the points
		Vector3d position;
		double fval;
		cout << "Entering position optimization..." << endl;
		if (!this->solveLS(opttries, tst_globalframe_origintopts, tst_camframe_unit_camtopts, position, fval)){
			cout << "Problem during position optimization " << i << ", skipping this trial." << endl;
			continue;
		}
		cout << "RANSAC try " << i << endl << "Objective: " << fval << endl << "Guessed position: " << endl << position << endl;
		//position now contains the estimated camera position, from global origin to camera in global frame
		//get the tst subset and full set of vectors from the camera to the points in the cam/global frame
		MatrixXd tst_globalframe_camtopts = tst_globalframe_origintopts;
		MatrixXd tst_camframe_camtopts = tst_camframe_unit_camtopts;
		for (int j = 0; j < ransacsize; j++){
			tst_globalframe_camtopts.block<3,1>(0,j) -= position;
			tst_camframe_camtopts.block<3,1>(0,j) *= tst_globalframe_camtopts.block<3,1>(0,j).norm();
		}
		MatrixXd globalframe_camtopts = globalframe_origintopts;
		MatrixXd camframe_camtopts = camframe_unit_camtopts;
		for (int j = 0; j < numrefpts; j++){
			globalframe_camtopts.block<3,1>(0,j) -= position;
			camframe_camtopts.block<3,1>(0,j) *= globalframe_camtopts.block<3,1>(0,j).norm();
		}
		
		//solve for the rotation matrix
		Matrix3d rotationmatrix;
		rotationmatrix = tst_camframe_camtopts.transpose().
						//fullPivLu() //better if ransacsize = 3
						jacobiSvd(ComputeThinU | ComputeThinV) //for least squares ransac size > 3
						.solve(tst_globalframe_camtopts.transpose());
		//cout << "Camtopts^T: " << endl << tst_camframe_camtopts.transpose() << endl;
		//cout << "Globl^T: " << endl << tst_globalframe_camtopts.transpose() << endl;
		cout << "Rotation Matrix: " << endl << rotationmatrix << endl;
		//now do voting
		//if the difference between the known global vector and its camera frame transformation is greater than 15cm, no vote.
		votes.push_back(0);
		vector<int> i_voters;
		voters.push_back(i_voters);
		rotations.push_back(rotationmatrix);
		positions.push_back(position);		
		for (int j = 0; j < numrefpts; j++){
			if(
			( rotationmatrix*globalframe_camtopts.block<3,1>(0,j)- camframe_camtopts.block<3,1>(0,j) ).norm()
		 	< ransacthresh){
				votes[i]++;
				voters[i].push_back(j);
			}
		}
		cout << "Votes: " << votes[i] << endl;
	} //end of ransac
	
	//find the position/rotation with the maximum votes.
	int maxvotes = 0;
	int bestcandidate = 0;
	for (int i = 0; i < ransactsts; i++){
		if (votes[i] > maxvotes){
			maxvotes = votes[i];
			bestcandidate = i;
		}
	}
	printf("The best raw candidate was:\n");
	cout << "Votes: " << maxvotes << endl;
	cout << "Position: " << endl << positions[bestcandidate] << endl;
	cout << "Rotation Mat: " << endl << rotations[bestcandidate] << endl;
	if (maxvotes <= 4){
		cout << "The best candidate had less than 4 votes..." << endl;
		cout << "Try using more reference points or changing the RANSAC parameters to get a better result." << endl;
		return false;
	}

	printf("Calculating the smoothed position...\n");
	
	//solve the least squares problem to get the distances to the points
	//get the voters for the best candidate
	MatrixXd globalframe_origintopts_voters(3, votes[bestcandidate]);
	MatrixXd camframe_unit_camtopts_voters(3, votes[bestcandidate]);
	for (int i = 0; i < votes[bestcandidate]; i++){
		globalframe_origintopts_voters.block<3,1>(0,i) = globalframe_origintopts.block<3,1>(0, voters[bestcandidate][i]);
		camframe_unit_camtopts_voters.block<3,1>(0,i) = camframe_unit_camtopts.block<3,1>(0, voters[bestcandidate][i]);
	}
	Vector3d smoothposition;
	double smoothfval;
	solveLS(opttries, globalframe_origintopts_voters, camframe_unit_camtopts_voters, smoothposition, smoothfval);
	cout << "Objective: " << smoothfval << endl << "Smoothed position: " << endl << smoothposition << endl;
	
	printf("Calculating the smoothed rotation...\n");

	MatrixXd globalframe_camtopts_voters = globalframe_origintopts_voters;
	MatrixXd camframe_camtopts_voters = camframe_unit_camtopts_voters;
	for (int j = 0; j < numrefpts; j++){
		globalframe_camtopts_voters.block<3,1>(0,j) -= smoothposition;
		camframe_camtopts_voters.block<3,1>(0,j) *= globalframe_camtopts_voters.block<3,1>(0,j).norm();
	}

	Matrix3d smoothrotation = camframe_camtopts_voters.transpose().
						jacobiSvd(ComputeThinU | ComputeThinV).
					solve(globalframe_camtopts_voters.transpose());

	results_position = smoothposition; //this is the position of the camera w.r.t. the global frame origin expressed in the global frame
	results_rotation = smoothrotation; //this matrix rotates vectors from the global frame to the camera frame
	//therefore, M = [rot -rot*pos] such that M(x) = rot*x - rot*pos = rot*(x-pos) = vector from camera to object in camera frame
	return true;
}
*/









