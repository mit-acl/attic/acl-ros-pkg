#ifndef P5512_H
#define P5512_H
//standard includes
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <list>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

//libcurl
#include <curl/curl.h>
//Eigen
#include <eigen3/Eigen/Dense>

//OpenCV
#include <opencv2/opencv.hpp>

//April tags
#include <AprilTags/TagDetector.h>
#include <AprilTags/Tag36h11.h>

//Boost
//#include <boost/thread/condition_variable.hpp>
//#include <boost/thread/thread.hpp>
//#include <boost/thread/mutex.hpp>
//#include <boost/thread/barrier.hpp>
#include <boost/thread.hpp>
//#include <boost/random/normal_distribution.hpp>
//#include <boost/random/mersenne_twister.hpp>
//#include <boost/random/variate_generator.hpp>
#include <boost/random.hpp>
#include <boost/array.hpp>

#include "CamWpt.h"
#include "P5512ImageWorker.h"
#include "P5512PTZWorker.h"
#include "LSPosCalib.h"

using namespace std;
using namespace Eigen;

#define XMIN   -3.0   
#define XMAX    7.5
#define YMIN   -2.4
#define YMAX    2.4
#define ZMIN	0.0
#define ZMAX	5.0


//NB: The camera frame here is consistent with OpenCV: Z out of the lens (into the image), X to the right (when looking at an image), and Y down

#define MAXSPEED 93.167096 //maximum pan/tilt speed in deg/s
#define DEADZONE 7.7812  //1/2 width of the unreachable zone by the pan unit
#define STOPPEDTHRESH 2 //the number of times the ptz must have consecutively reported "no motion" until 
						//we are satisfied that the waypoint is done. A higher value here will make 
						//the PTU control more stable and accurate, but cause it to "lag" in between 
						//waypoints. With a value of 1, there is an occasional mistake made by the controller when near 
						//flip points - it fixes them itself in realtime, but they do cause nonsmooth paths between 
						//waypoints

//#define DEBUGPRINTS //if defined, a lot of crap gets printed

//the relation between angular and positional states for the P5512
//is derived by assuming the position is an x,y location on the plane z=0
//base psi, theta, phi are yaw, pitch and roll of the base of the camera
//with 0 reference point is with camera at 0 pan 0 tilt pointing along the xaxis, z axis perpendicular to the base.
class P5512{
	public:
		//Constructor/destructor
		P5512(string name, string ip_addr);
		~P5512();

		//CURL/threading stuff
		int connect();
		int check();
		void disconnect();

		//Calibration functions
		bool calibrateSpeeds(double (&axisspeeds)[11], double (&panspeeds)[11], double (&tiltspeeds)[11]);
		bool calibrateIntrinsics(int numBoards,int  numInnerCornersHor, int numInnerCornersVer, double distHor, double distVer,double calibPan, double calibTilt, double (&axiszooms)[11], boost::array<Matrix3d, 11>& intrinsicMatArray, boost::array<Matrix<double, 1, 5>, 11>& distortionArray);
		bool calibratePositionWithApril(int numSamples, int tagID, Vector3d& refpos, boost::mutex& refpos_mutex, Matrix<double, 3, 4>& results_baseExtrinsic);
//DEPRECATED		bool calibratePosition(int ransactsts, int ransacsize, double ransacthresh, int opttries, Vector3d& refpos, boost::mutex& refpos_mutex, Vector3d& results_position, Matrix3d& results_rotation);
		//Getters
		void getCamGroundCorners(double (&grndCorners)[8]);
		void getCamGroundPt(double (&grndPt)[2]);
		void getCamInfo(Matrix<double, 3, 4>& extrinsic, Matrix<double, 3, 3>& intrinsic, double& ipan, double& itilt, double& izoom, int& width, int&height);
		void getUndistortedImage(cv::Mat& img);
		void getImage(cv::Mat& img);
		bool getIdle();
		std::map<int, Eigen::Vector3d> getAprilTagLocs(double tagSize);
		std::map<int, Eigen::Matrix4d> getAprilTagTransforms(double tagSize);

		//Setters/Getters
		string getName();
		void setBaseExtrinsics(Matrix<double, 3, 4> extrinsicmat);
		void setIntrinsics(double (&axiszooms)[11], boost::array<Matrix3d, 11>& intrinsicMats, boost::array<Matrix<double, 1, 5>, 11>& distortionCoeffMats);
		void setSpeeds(double (&axisspeeds)[11], double (&panspeeds)[11], double (&tiltspeeds)[11]);

		//Add waypoints to the queue
		void addWaypoints(list<CamWpt> addwpts);
		
	private:
		P5512ImageWorker imgworker;
		P5512PTZWorker ptzworker;
		//Camera Information that does not change
		string name; //camera name
		string ip_addr;
		bool verbose;
		double calib_axisspeeds[11];
		double calib_panspeeds[11];
		double calib_tiltspeeds[11];
		Matrix<double, 3, 4> calib_baseExtrinsicMat;
		double calib_axiszooms[11];
		boost::array<Matrix3d, 11> calib_intrinsicMats;
		boost::array<Matrix<double, 1, 5>, 11> calib_distortionCoeffMats;
		boost::mt19937 RNG;

		//Camera Information that changes
		//NB: a lot of these things are mutexed - see the threading objects
		list<CamWpt> wpts; //list of waypoints to do
		bool idle;//whether the camera is currently pursuing a waypoint

		//Threading objects for waypoints (mutexes and cond vars)
		boost::mutex wptmutex;
		boost::condition_variable wptcond; //for producer consumer on waypoint queue
						   //used to wake up the cmd thread
						  //also used to wake up anyone waiting for the ptz to become idle
		boost::thread *wptthread;//the three threads (command, state update, image get)

		static int CURLGLOBAL; //the number of curl connections active

		//Conversions between SAU and NPU (Stupid Axis Units and Normal People Units)
		double convertDegPerSecToAxis_Tilt(double degpersec);
		double convertDegPerSecToAxis_Pan(double degpersec);
		Matrix3d convertZoomToPerspectiveMat(double axiszoom);
		Matrix<double, 1, 5> convertZoomToDistortionMat(double axiszoom);

		//Conversions between camera state and rotation matrices
		Matrix<double, 3, 4> getTotalExtrinsic();
		Matrix3d getBaseToImageFrame(double pandeg, double tiltdeg);
		CamWpt convertWaypoint(CamWpt& toConvert); //used to convert all types of waypoint to CAMWPT_PTZ


		//Command functions
		bool home();//sends the device to 0 pan 0 tilt and ensures flip state = false


		//Threading functions
		void wpt_sentinel(boost::mutex& clearmutex, bool& clearintterupt);
		void wpt_worker();
};

#endif //header guard end
