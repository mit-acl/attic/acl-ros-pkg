#include "P5512ImageWorker.h"

P5512ImageWorker::P5512ImageWorker(string ip_addr) : maximgbufsize(MAXIMGBUFFERSIZE){
	this->im_width = 704; 
	this->im_height = 480;  //these numbers are replaced by the actual camera data if hardware mode is on.
	this->imgbufsize = 10000;
	this->imgbuf = new char[10000];
	this->imgend = imgbuf;
	
	this->mutexed_imgbufsize = 10000;
	this->mutexed_imgbuf = new char[10000];
	this->mutexed_imgend = mutexed_imgbuf;

	this->ip_addr = ip_addr;

	this->running = false;
}


P5512ImageWorker::~P5512ImageWorker(){
	delete[] imgbuf;
	delete[] mutexed_imgbuf;
	delete this->imggetthread;
}


void P5512ImageWorker::getImage(cv::Mat& img){
	//Get an image from the buffer and decode it
	this->imgmutex.lock();
	std::vector<char> tmpbuf(this->mutexed_imgbuf, this->mutexed_imgend);
	this->imgmutex.unlock();
	if (tmpbuf.size() != 0){ //it can be 0 sometimes right when the thread starts up; here just don't modify img
		img = cv::imdecode(tmpbuf, -1); //-1 means just decode the image based on whatever format it was found in
	}
	return;
}

void P5512ImageWorker::saveimg(const string& filename){
		cv::Mat image;
		this->getImage(image);
		vector<int> compression_params;
		compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
		compression_params.push_back(9);
		try {
			imwrite(filename,  image, compression_params );
		} catch (runtime_error& ex){
			fprintf(stderr, "Exception converting image to PNG format: %s\n", ex.what());
		}
}

bool P5512ImageWorker::start(){
	//estup Image get curl and get image resolution
	printf("Starting up Image Worker...\n");
	printf("Initializing imggetcurl...\n");
	this->imggetcurl = curl_easy_init();
	if (!imggetcurl){
		printf("P5512::connect(): failed to setup imggetcurl with curl_easy_init().\n");
		return false;
	} else {
		//start the thread
		boost::mutex readymutex;
		boost::condition_variable readycond;
		bool isready = false;
		this->imggetthread = new boost::thread(boost::bind(&P5512ImageWorker::imgget_worker, this, boost::ref(readymutex), boost::ref(readycond), boost::ref(isready)));
		{//scoped lock scope
			//wait on the condition variable
			boost::mutex::scoped_lock readylock(readymutex);		
			while (!isready){
				readycond.wait(readylock);
			}
		}
	}
	this->running = true;
	return this->running;
}

bool P5512ImageWorker::stop(int timeout){
	if (this->running){
		this->imggetthread->interrupt();
		this->running = !this->imggetthread->timed_join(boost::posix_time::time_duration(0, 0, timeout, 0));
		if (!this->running){
			curl_easy_cleanup(this->imggetcurl);
			return true;
		}
		return false;
	} else {
		return true;
	}
}

void P5512ImageWorker::getInfo(int& imw, int& imh){
	imw = this->im_width;
	imh = this->im_height;
}

void P5512ImageWorker::imgget_worker(boost::mutex& readymutex, boost::condition_variable& readycond, bool& isready){
	stringstream respstream;
	curl_easy_setopt(this->imggetcurl, CURLOPT_NOSIGNAL, 1); //this line deals with a bug that occurs in multithreaded code on shutdown.
	curl_easy_setopt(this->imggetcurl, CURLOPT_NOPROGRESS, 1L); //no progress bar to be displayed
	curl_easy_setopt(this->imggetcurl, CURLOPT_VERBOSE, (VERBOSE ? 1 : 0) ); //decide whether to be verbose
	curl_easy_setopt(this->imggetcurl, CURLOPT_CONNECTTIMEOUT, 5);
	curl_easy_setopt(this->imggetcurl, CURLOPT_POST, 1);
	//first use this curl to get image resolution data
	printf("Getting image resolution...\n");
	string url("http://root:uavswarm@");
	url.append(this->ip_addr);
	url.append("/axis-cgi/imagesize.cgi");
	curl_easy_setopt(this->imggetcurl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(this->imggetcurl, CURLOPT_POSTFIELDS, "camera=1");
	curl_easy_setopt(this->imggetcurl, CURLOPT_WRITEFUNCTION, &getdims_curl_callback);
	curl_easy_setopt(this->imggetcurl, CURLOPT_WRITEDATA, &respstream);
	CURLcode res = curl_easy_perform(this->imggetcurl);
	if (res != CURLE_OK){
		printf("P5512::imgget_worker(): libCURL error while getting image resolution: \n");
		printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	}
	//extract resolution
	string respline;
	getline(respstream, respline);	
	if (respline.find("image width = ") == string::npos){
	
	}
	this->im_width = atoi((respline.substr(14, respline.length())).c_str());
	getline(respstream, respline);
	if (respline.find("image height = ") == string::npos){
	//error
	}
	this->im_height = atoi((respline.substr(15, respline.length())).c_str());

	printf("Image width: %d Image height: %d\n", this->im_width, this->im_height);
	//setup the curl and start receiving the mjpg stream
	
	url = "http://root:uavswarm@";
	url.append(ip_addr);
	url.append("/axis-cgi/mjpg/video.cgi");
	curl_easy_setopt(imggetcurl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(imggetcurl, CURLOPT_POSTFIELDS, "camera=1");
	curl_easy_setopt(imggetcurl, CURLOPT_WRITEFUNCTION, &imgstream_curl_callback);
	curl_easy_setopt(imggetcurl, CURLOPT_WRITEDATA, this);

	//signal that this thread is ready to roll
	readymutex.lock();
	isready = true;
	readymutex.unlock();
	readycond.notify_all();

	res = curl_easy_perform(imggetcurl); //this function blocks the thread 
	if (res != CURLE_OK){
		printf("P5512::imgget_worker(): libCURL error while getting image\n");
		printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
	}
}

/*
 * This function receives image data stream collected by curl,
 * and processes it into jpgs. Of course axis can't just
 * use a open format for its image stream, it has to be all stupid about it.
 * Hence, a LOT of weird black-magicy pointer arithmetic and hexy stuff is in
 * this function. It took me a lot of byte editing to find out what the hell
 * format the Axis P5512 camera stream data was in.
 *
 * It saves the raw data at imgbuf, and parses it into individual images which 
 * are then saved in mutexedimgbuf
 *
 * ...good luck debugging this if you find a problem.
 *
 *  :)
 */

size_t imgstream_curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata){
	boost::this_thread::interruption_point();
	P5512ImageWorker* obj = (P5512ImageWorker*) userdata;
	int curimgsize = obj->imgend - obj->imgbuf;

	if (obj->imgbufsize < curimgsize + size*nmemb){ //if the current image buffer needs to be resized
		////printf("Resizing img buffer: Old size %d bytes, New size %d bytes\n",obj->imgbufsize, curimgsize+size*nmemb*2);
		if (curimgsize + size*nmemb*2 > obj->maximgbufsize){
			printf("imgstream_curl_callback: Error: Exceeded maximum img buffer size of %d bytes\n", obj->maximgbufsize);
		}
		char* tmpbuf = new char[curimgsize + size*nmemb*2]; //create a new buffer (add a factor of 2 to stop it
									// from constantly reallocating with little changes
		obj->imgbufsize = curimgsize + size*nmemb*2;
		memcpy(tmpbuf, obj->imgbuf, curimgsize); //copy the contents over to the new buffer
		delete[] obj->imgbuf; //delete the old buffer
		obj->imgbuf = tmpbuf; //change imgbuf to point to tmpbuf
		obj->imgend = obj->imgbuf + curimgsize;
	}
	
	/*
	//Debugging printouts -- NOT THREAD SAFE
	printf("Current sizes of things:\n");
	printf("imgbufsize = %d, curimgsize = %d\n", obj->imgbufsize, obj->curimgsize);
	//THE BELOW PRINT STATEMENT IS NOT THREAD SAFE BUT IS USED FOR DEBUGGING
	////printf("mutexed_imgbufsize = %d, mutexed_imgsize = %d\n", obj->mutexed_imgbufsize, obj->mutexed_curimgsize);
	*/
	
	//copy the new image content
	memcpy(obj->imgend, ptr, size*nmemb);
	obj->imgend += size*nmemb;
	
	//find image boundary
	char bdrystr[28] = "--myboundary";
	char* curptr = obj->imgbuf;
	int res = -1;
	while (curptr < obj->imgend){
		curptr = (char*)memchr(curptr, '-', obj->imgend-curptr); //we use '-' to quickly search for "--myboundary"
		if (curptr == NULL || curptr+strlen(bdrystr) > obj->imgend){ // '-' wasn't found or we're too close to the end of the buffer
									    //for "--myboundary" to exist
			break;
		} else {
			res = memcmp(bdrystr, curptr, strlen(bdrystr)); //see how it compares
			if (res == 0){ //they are equal, we found the boundary
				//find the jpg start/end tags: FF D8 and FF D9
				//this shit is black magic
				char* jpgstart = obj->imgbuf, *jpgend = curptr;
				while (jpgstart < curptr){
					if (*(unsigned char*)jpgstart == (unsigned char)0xFF && *(unsigned char*)(jpgstart+1) == (unsigned char)0xD8){
						break;
					}
					jpgstart++;
				}
				while (jpgend > obj->imgbuf+1){
					if (*(unsigned char*)(jpgend-1) == (unsigned char)0xD9 && *(unsigned char*)(jpgend-2) == (unsigned char)0xFF){
						break;
					}
					jpgend--;
				}
				//if there were problems, just dump this part of the image buffer and don't modify the mutexed buffer
				if (jpgstart >= jpgend){
					curptr += strlen(bdrystr);
					memmove(obj->imgbuf, curptr, obj->imgend - curptr); //need to use memmove because the src and dest overlap
					obj->imgend = obj->imgbuf + (obj->imgend - curptr);
					curptr = obj->imgbuf;
					continue;
				}
				
				//we good, let's save this jpg 
				obj->imgmutex.lock();
				//remove the image from the buffer, get a mutex on the full image storage, put into that storage.
				if (obj->mutexed_imgbufsize < jpgend-jpgstart){ //if the current mutexed image buffer needs to be resized
					////printf("Resizing mutexed img buffer: Old size %d bytes, New size %d bytes\n", obj->mutexed_imgbufsize, jpgend-jpgstart);
					if (jpgend-jpgstart > obj->maximgbufsize){
						printf("imgstream_curl_callback: Error: Exceeded maximum img buffer size of %d bytes\n", obj->maximgbufsize);
					}
					//don't need to preserve this one because we're going to overwrite anyway, so just kill and replace
					delete[] obj->mutexed_imgbuf; //delete the old buffer
					obj->mutexed_imgbuf = new char[jpgend-jpgstart];
					obj->mutexed_imgbufsize = jpgend-jpgstart;
				}
				//copy the contents
				memcpy(obj->mutexed_imgbuf, jpgstart, jpgend-jpgstart); //we're only copying until just before curptr
				obj->mutexed_imgend = obj->mutexed_imgbuf + (jpgend-jpgstart); //set the new mutexed image size
				obj->imgmutex.unlock(); //unlock as early as possible
				//reshuffle the streaming image buffer
				curptr += strlen(bdrystr); //now curptr points to the new beginning of imgbuf
				if (obj->imgend - curptr <= 0){ //imgbuf is going to be empty
					assert(obj->imgend - curptr == 0); //we should have never tried to search past the end of the block
					obj->imgend = obj->imgbuf; //just set the new image size to 0 and away we go
				} else { //there is stuff that needs to be copied
					memmove(obj->imgbuf, curptr, obj->imgend - curptr); //need to use memmove because the src and dest overlap
					obj->imgend = obj->imgbuf + (obj->imgend - curptr);
				}
				curptr = obj->imgbuf;
				res = -1;
			} else { //they are not equal, move on.
				curptr = curptr+1;
			}
		}
	}
	
	return size*nmemb;
}


size_t getdims_curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata){

	stringstream* respvec = (stringstream*)userdata;
	respvec->write(ptr, size*nmemb);
	return size*nmemb;
}
