#ifndef __P5512IMAGEWORKER_H
//standard includes
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <list>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

//nonstandard
#include <curl/curl.h>
#include <boost/thread.hpp>
#include <boost/ref.hpp>
#include <opencv2/opencv.hpp>


using namespace std;

#define IMGRATE 30 //30 hz images
#define MAXIMGBUFFERSIZE 100000 //100kb image buffer
#define VERBOSE false

size_t imgstream_curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata);
size_t getdims_curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata);


class P5512ImageWorker{
	public:
		P5512ImageWorker(string ip_addr);
		~P5512ImageWorker();
		void saveimg(const string& filename);
		void getImage(cv::Mat& img);
		bool start();
		bool stop(int timeout);
		void getInfo(int& imw, int& imh);
	private:
		string ip_addr;
		bool running;
		const int maximgbufsize;
		int im_width, im_height;//the image width/height
		char* imgbuf, *imgend; //imgend points to the next char AFTER the imgbuf data (i.e. image length = imgend-imgbuf)
		int imgbufsize; //note that imgbuf points to garbage - it is cleaned up by imgstream_curl_callback
						//and the clean data is inserted into mutexed_imgbuf
		char* mutexed_imgbuf, *mutexed_imgend; //mutexed_imgend behaves the same way as imgend
		int mutexed_imgbufsize;
		CURL* imggetcurl;
		boost::mutex imgmutex;
		boost::thread *imggetthread;//the three threads (command, state update, image get)

		void imgget_worker(boost::mutex& readymutex, boost::condition_variable& readycond, bool& isready);
		friend size_t imgstream_curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata);
		friend size_t getdims_curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata);
};
#define __P5512IMAGEWORKER_H
#endif /* __P5512IMAGEWORKER_H */
