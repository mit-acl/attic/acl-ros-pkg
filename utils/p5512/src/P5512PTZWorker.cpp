#include "P5512PTZWorker.h"

P5512PTZWorker::P5512PTZWorker(string ip_addr){
	this->pan = 0;
	this->tilt = 0;
	this->zoom = 0; 
	this->iris = 0; 
	this->ptzgetrate = PTZGETRATE;
	this->ptzrunning = false;
	this->flipped = false;
	this->newptzpt = false;//whether a new ptz state is ready to update with
	this->ip_addr = ip_addr;
}

bool P5512PTZWorker::start(){
	//setup curls
	printf("Starting up PTZ Worker...\n");
	printf("Initializing ptzgetcurl...\n");
	this->ptzgetcurl = curl_easy_init();
	if (!ptzgetcurl){
		printf("P5512PTZWorker::start(): failed to setup ptzgetcurl with curl_easy_init().\n");
		return false;
	} else {
		//start the thread
		boost::mutex readymutex;
		boost::condition_variable readycond;
		bool isready = false;
		this->ptzgetthread = new boost::thread(boost::bind(&P5512PTZWorker::ptzget_worker, this, boost::ref(readymutex), boost::ref(readycond), boost::ref(isready)));
		{//scoped lock scope
			//wait on the condition variable
			boost::mutex::scoped_lock readylock(readymutex);		
			while (!isready){
				readycond.wait(readylock);
			}
		}
	}
	this->ptzrunning = true;
	printf("Initializing ptzcmdcurl...\n");
	this->ptzcmdcurl = curl_easy_init();
	//curl_easy_setopt(this->ptzcmdcurl, CURLOPT_WRITEDATA, &respstream);	
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_NOSIGNAL, 1); //this line deals with a bug that occurs in multithreaded code on shutdown.
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_NOPROGRESS, 1L); //no progress bar to be displayed
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_VERBOSE, (VERBOSE ? 1 : 0) ); //decide whether to be verbose
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_CONNECTTIMEOUT, 5);
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_POST, 1);
	string url("http://root:uavswarm@");
	url.append(ip_addr);
	url.append("/axis-cgi/com/ptz.cgi");
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(this->ptzcmdcurl, CURLOPT_WRITEFUNCTION, &curl_callback);
	if (!this->ptzcmdcurl){
		printf("P5512PTZWorker::start(): failed to setup ptzcmdcurl with curl_easy_init().\n");
		return false;
	}
	return this->ptzrunning;
}

bool P5512PTZWorker::stop(int timeout){
	if (this->ptzrunning){
		this->ptzgetthread->interrupt();
		this->ptzrunning = !this->ptzgetthread->timed_join(boost::posix_time::time_duration(0, 0, timeout, 0));
		if (!this->ptzrunning){
			curl_easy_cleanup(this->ptzgetcurl);
			curl_easy_cleanup(this->ptzcmdcurl);
		}
		return true;
	} else {
		return false;
	}
}

void P5512PTZWorker::getNew(double& p, double& t, double& z, double& i, bool& f){
	{//scoped lock scope
		//wait on the condition variable
		boost::mutex::scoped_lock ptzlock(this->ptzmutex);		
		while (!this->newptzpt){
			ptzcond.wait(ptzlock);
		}
		p = this->pan;
		t = this->tilt;
		z = this->zoom;
		i = this->iris;
		f = this->flipped;
		this->newptzpt = false;
	}
	return;
}

void P5512PTZWorker::get(double& p, double& t, double& z, double& i, bool& f){
	this->ptzmutex.lock();
		p = this->pan;
		t = this->tilt;
		z = this->zoom;
		i = this->iris;
		f = this->flipped;
		this->newptzpt = false;
	this->ptzmutex.unlock();
	return;
}



P5512PTZWorker::~P5512PTZWorker(){
	delete this->ptzgetthread;
}



void P5512PTZWorker::ptzget_worker(boost::mutex& readymutex, boost::condition_variable& readycond, bool& isready){
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_NOSIGNAL, 1); //this line deals with a bug that occurs in multithreaded code on shutdown.
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_NOPROGRESS, 1L); //no progress bar to be displayed
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_VERBOSE, (VERBOSE ? 1 : 0) ); //decide whether to be verbose
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_CONNECTTIMEOUT, 5);
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_POST, 1);
	string url("http://root:uavswarm@");
	url.append(this->ip_addr);
	url.append("/axis-cgi/com/ptz.cgi");
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_WRITEFUNCTION, &curl_callback);
	curl_easy_setopt(this->ptzgetcurl, CURLOPT_POSTFIELDS, "query=position");
		//curl_easy_setopt(this->ptzgetcurl, CURLOPT_POSTFIELDS, "query=speed");
	//curl_easy_setopt(this->ptzgetcurl, CURLOPT_POSTFIELDS, "query=position");
	const int nanostep = 1000000000/ptzgetrate;
	timespec tsloopstart, tsloopend;
	
	//signal that this thread is ready to roll
	readymutex.lock();
	isready = true;
	readymutex.unlock();
	readycond.notify_all();

	double prevtilt = 0;
	double tmppan = 0, tmptilt = 0, tmpzoom = 0, tmpiris = 0;
	while(true){
		clock_gettime(CLOCK_REALTIME, &tsloopstart); 
		boost::this_thread::interruption_point();
		stringstream respstream;
		curl_easy_setopt(this->ptzgetcurl, CURLOPT_WRITEDATA, &respstream);
		curl_easy_perform(this->ptzgetcurl);
		while(respstream){
			string s;
			if (!getline(respstream, s, '\n')) break;
			int eqind = s.find("=");

			if ((s.substr(0, eqind)).compare("pan") == 0){
				tmppan = atof((s.substr(eqind+1)).c_str());
			} else if ((s.substr(0, eqind)).compare("tilt") == 0) {
				tmptilt = atof((s.substr(eqind+1)).c_str());
			} else if ((s.substr(0, eqind)).compare("zoom")== 0) { 
				tmpzoom = atof((s.substr(eqind+1)).c_str());
			} else if ((s.substr(0, eqind)).compare("iris")== 0) {
				tmpiris = atof((s.substr(eqind+1)).c_str());
			} else if ((s.substr(0, eqind)).compare("autofocus")== 0) {
				//do nothing we don't need this
			} else {
				printf("P5512PTZWorker::ptzget_worker() Error: Unknown ptzget parameter.\n");
				printf("Got: %s\n", s.c_str());
			}
		}

		ptzmutex.lock();
		this->pan = tmppan;
		this->tilt = tmptilt;
		this->zoom = tmpzoom;
		this->iris = tmpiris;
		this->newptzpt = true;
		this->flipped = (prevtilt <= -96 && tmptilt >= -80 ? !this->flipped : this->flipped);
		ptzmutex.unlock();
		ptzcond.notify_all();
		
		prevtilt = tmptilt;
		#ifdef DEBUGPRINTS
		printf("PTZ:\n");
		printf("Pan: %f\n", this->pan);
		printf("Tilt: %f\n", this->tilt);
		printf("Zoom: %f\n", this->zoom);
		printf("Iris: %f\n", this->iris);
		#endif

		clock_gettime(CLOCK_REALTIME, &tsloopend); 
		int nanosleep = (tsloopstart.tv_sec*1000000000+tsloopstart.tv_nsec) 
				+ nanostep - (tsloopend.tv_sec*1000000000 + tsloopend.tv_nsec);
		if (nanosleep > 0){
			usleep(nanosleep/1000);//boost::this_thread::sleep(boost::posix_time::nanoseconds(nanosleep)); 
		}
	}
}


void P5512PTZWorker::setFlipState(bool flip){
	this->ptzmutex.lock();
	this->flipped = flip;
	this->ptzmutex.unlock();
}


size_t curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata){
	stringstream* respvec = (stringstream*)userdata;
	respvec->write(ptr, size*nmemb);
	return size*nmemb;
}

void P5512PTZWorker::sendCmd(double pancmd, double panspeed, double tiltcmd, double tiltspeed, double zoom){
			char pancmd_str[100], panspeed_str[100], tiltcmd_str[100], tiltspeed_str[100], zoom_str[100];
			sprintf(pancmd_str, "pan=%f", pancmd);	
			sprintf(panspeed_str, "speed=%f", panspeed);
			sprintf(tiltcmd_str, "tilt=%f", tiltcmd);
			sprintf(tiltspeed_str, "speed=%f", tiltspeed);
			sprintf(zoom_str, "zoom=%f", zoom);
			#ifdef DEBUGPRINTS
			printf("P5512::ptzcmdworker: Sending %s\n", panspeed_str);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, panspeed_str);
			curl_easy_perform(ptzcmdcurl);
			printf("P5512::ptzcmdworker: Sending %s\n", pancmd_str);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, pancmd_str);
			curl_easy_perform(ptzcmdcurl);
			printf("P5512::ptzcmdworker: Sending %s\n", tiltspeed_str);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, tiltspeed_str);
			curl_easy_perform(ptzcmdcurl);
			printf("P5512::ptzcmdworker: Sending %s\n", tiltcmd_str);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, tiltcmd_str);
			curl_easy_perform(ptzcmdcurl);
			printf("P5512::ptzcmdworker: Sending %s\n", zoom_str);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, zoom_str);
			curl_easy_perform(ptzcmdcurl);
			printf("P5512::ptzcmdworker: Done sending!\n");
			#else
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, panspeed_str);
			curl_easy_perform(ptzcmdcurl);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, pancmd_str);
			curl_easy_perform(ptzcmdcurl);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, tiltspeed_str);
			curl_easy_perform(ptzcmdcurl);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, tiltcmd_str);
			curl_easy_perform(ptzcmdcurl);
			curl_easy_setopt(ptzcmdcurl, CURLOPT_POSTFIELDS, zoom_str);
			curl_easy_perform(ptzcmdcurl);
			#endif
			return;
}
