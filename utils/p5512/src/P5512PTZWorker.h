#ifndef __P5512PTZTRACKER_H
//standard includes
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <list>
#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

//libcurl
#include <curl/curl.h>

//boost
#include <boost/thread.hpp>

#define PTZGETRATE 100 //frequency of ptz get request
#define VERBOSE false

using namespace std;

size_t curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata);

class P5512PTZWorker{
	public:
		P5512PTZWorker(string ip_addr);
		~P5512PTZWorker();
		bool start();
		bool stop(int timeout);
		void getNew(double& pan, double& tilt, double& zoom, double& iris, bool& flipped);
		void get(double& pan, double& tilt, double& zoom, double& iris, bool& flipped);
		void sendCmd(double pancmd, double panspeed, double tiltcmd, double tiltspeed, double zoom);
		void setFlipState(bool flip);
	private:
		string ip_addr;
		bool ptzrunning;
		bool flipped;
		bool newptzpt;//whether a new ptz state is ready to update with
		int ptzgetrate;
		double pan, tilt, zoom, iris;
		boost::thread *ptzgetthread;
		boost::mutex ptzmutex;
		boost::condition_variable ptzcond;
		CURL *ptzgetcurl, *ptzcmdcurl;
		void ptzget_worker(boost::mutex& readymutex, boost::condition_variable& readycond, bool& isready);
		friend size_t curl_callback(char* ptr, size_t size, size_t nmemb, void *userdata);
};
#define __P5512PTZTRACKER_H
#endif /* __P5512PTZTRACKER_H */
