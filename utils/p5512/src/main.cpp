#include "P5512.h"
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include "p5512/waypoint.h"
#include "p5512/camera_state.h"
#include "p5512/apriltags.h"
#include <geometry_msgs/Vector3.h>
#include <sensor_msgs/image_encodings.h>
#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <boost/ref.hpp>
#include <boost/bind.hpp>



template <class D>
std::vector<D> getRosParam(string paramname, int len, XmlRpc::XmlRpcValue::Type type){
	XmlRpc::XmlRpcValue paramXML;
	std::vector<D> ret;
	if (!ros::param::get(paramname, paramXML) ){
		ROS_FATAL("Could not find %s on the param server!", paramname.c_str());
		ROS_FATAL("Are you sure you loaded the parameter on the server at some point?");
		return ret;
	} else {
		ROS_ASSERT(paramXML.getType() == XmlRpc::XmlRpcValue::TypeArray);
		if (paramXML.size() != len){
			ROS_FATAL("Param for %s is not length %d!", paramname.c_str(), len);
			return ret;
		}
		for(int i = 0; i < paramXML.size(); i++){
			if (paramXML[i].getType() != type){
				ROS_FATAL("Element %d for parameter %s is not *explicitly* a double!\n", i, paramname.c_str());
				ROS_FATAL("It may be an int - make sure it has a decimal in it.");
				return ret;
			}
			ret.push_back(paramXML[i]);	
		}
	}
	return ret;
}

//callback for sending camera state
void camstateCallback(const ros::TimerEvent&, P5512& camera, ros::Publisher& camstatepub){
		//publish the current state of this camera
		p5512::camera_state caminfomsg;
		caminfomsg.header.stamp = ros::Time::now();
		caminfomsg.name = camera.getName();
		Matrix<double, 3, 4> extrinsic;
		Matrix<double, 3, 3> intrinsic;
		int width, height;
		double pan, tilt, zoom;
		camera.getCamInfo(extrinsic, intrinsic, pan, tilt, zoom, width, height);
		caminfomsg.idle = camera.getIdle();
		caminfomsg.width = width;
		caminfomsg.height = height;
		for (int i = 0; i < 9; i++){
			caminfomsg.intrinsic[i] = intrinsic(i/3, i%3);
		}
		for (int i = 0; i < 12; i++){
			caminfomsg.extrinsic[i] = extrinsic(i/4, i%4);
		}
		double grndcorners[8];
		camera.getCamGroundCorners(grndcorners);
		for (int i = 0; i < 8; i++){
			caminfomsg.grndCorners[i] = grndcorners[i];
		}
		double grndpt[2];
		camera.getCamGroundPt(grndpt);
		for (int i = 0; i < 2; i++){
			caminfomsg.grndPt[i] = grndpt[i];
		}

		Vector3d camglobalpos = -extrinsic.block<3,3>(0,0).inverse()*extrinsic.block<3,1>(0,3);
		for (int i = 0; i < 3; i++){
			caminfomsg.pos[i] = camglobalpos(i);
		}

		caminfomsg.pan = pan;
		caminfomsg.tilt = tilt;
		caminfomsg.zoom = zoom;

		camstatepub.publish(caminfomsg);

		return;


}
//callback for sending image data/april tags
void imgCallback(const ros::TimerEvent&, P5512& camera, image_transport::Publisher& imgpub, ros::Publisher& aprilpub){
	//publish an image
	cv::Mat img;
	camera.getImage(img);
	cv_bridge::CvImage cvbridgeimg;
	cvbridgeimg.image = img;
	cvbridgeimg.header.stamp = ros::Time::now();
	cvbridgeimg.header.frame_id = camera.getName();
	cvbridgeimg.encoding = sensor_msgs::image_encodings::BGR8;
	imgpub.publish(cvbridgeimg.toImageMsg());

	//publish April Tags
	std::map<int, Eigen::Vector3d> taglocs = camera.getAprilTagLocs(0.16);
	if (!taglocs.empty()){
		p5512::apriltags tagmsg;
		tagmsg.header.stamp = ros::Time::now();
		tagmsg.name = camera.getName();
		for (std::map<int, Eigen::Vector3d>::iterator it = taglocs.begin(); it != taglocs.end(); it++){
			tagmsg.ids.push_back(it->first);
			geometry_msgs::Vector3 vec;
			vec.x = it->second(0);
			vec.y = it->second(1);
			vec.z = it->second(2);
			tagmsg.vecs.push_back(vec);	
		}
		aprilpub.publish(tagmsg);
	}
	return;
}


//receive waypoint updates
void wayptCallback(const p5512::waypoint::ConstPtr& msg, P5512& camera){
	//saturate the x,y so that the camera only ever looks at points in the vicon space
	//xmax/xmin/etc are defined in RAVEN_defines.hpp
	double x = msg->x, y = msg->y, z = msg->z;
	double zoom = msg->zoom;
	if (x > XMAX)
		x = XMAX;
	if (x < XMIN)
		x = XMIN;
	if (y > YMAX)
		y = YMAX;
	if (y < YMIN)
		y = YMIN;
	if (z < ZMIN)
		z = ZMIN;
	if (z > ZMAX)
		z = ZMAX;
	if (zoom < 1.0)
		zoom = 1.0;
	if (zoom > 10000.0)
		zoom = 10000.0;
		
	if (camera.getName().compare(msg->name) == 0){
		if (msg->wpttype.compare("CAMWPT_PTZ")== 0){
			CamWpt wpt(CAMWPT_PTZ, msg->pan, msg->tilt, zoom, msg->dt, msg->timeout, msg->clear);
			list<CamWpt> wpts;
			wpts.push_back(wpt);
			camera.addWaypoints(wpts);
		} else if (msg->wpttype.compare("CAMWPT_2W")== 0){
			CamWpt wpt(CAMWPT_2W, x, y, msg->w, msg->dt, msg->timeout, msg->clear);
			list<CamWpt> wpts;
			wpts.push_back(wpt);
			camera.addWaypoints(wpts);
		} else if (msg->wpttype.compare("CAMWPT_2Z")== 0){
			CamWpt wpt(CAMWPT_2Z, x, y, zoom, msg->dt, msg->timeout, msg->clear);
			list<CamWpt> wpts;
			wpts.push_back(wpt);
			camera.addWaypoints(wpts);
		} else if (msg->wpttype.compare("CAMWPT_3W")== 0){
			CamWpt wpt(CAMWPT_3W, x, y, z, msg->w, msg->dt, msg->timeout, msg->clear);
			list<CamWpt> wpts;
			wpts.push_back(wpt);
			camera.addWaypoints(wpts);
		} else if (msg->wpttype.compare("CAMWPT_3Z")== 0){
			CamWpt wpt(CAMWPT_3Z, x, y, z, zoom, msg->dt, msg->timeout, msg->clear);
			list<CamWpt> wpts;
			wpts.push_back(wpt);
			camera.addWaypoints(wpts);
		} else {
			printf("wayptCallback: Unknown waypoint type received for %s\n", msg->name.c_str()); 
			return;
		}
	}
}
	
int main(int argc, char** argv){
	 
	
	if (argc != 2){ 
		ROS_FATAL("Syntax for calling p5512_controller: rosrun p5512 p5512_controller [cam name]");
		ROS_FATAL("You called p5512_controller with argc=%d:", argc);
		for (int i = 0; i < argc; i++){
			ROS_FATAL("argv %d = %s", i, argv[i]);
		}
		return -1;
	}
	
	//get the name of the camera
	string camname;
	camname = argv[1];

	string nodename = "p5512_controller_";
	nodename.append(camname);
	ros::init(argc, argv, nodename);
	ros::NodeHandle nhandle;

	//find the camera base location in yml files
	ROS_INFO("Getting camera extrinsics for %s...", camname.c_str());
	string extrinsicparam = "/P5512extrinsics/";
	extrinsicparam.append(camname);
	Matrix<double, 3, 4> extrinsicMat; //row major from xml file
	std::vector<double> tmpext =  getRosParam<double>(extrinsicparam, 12, XmlRpc::XmlRpcValue::TypeDouble);
	if (tmpext.size() == 0){
		std::cout << "Fatal error -shutting down." << std::endl;
		return 0;
	}
	for (int i = 0; i < tmpext.size(); i++){
		extrinsicMat(i/4, i%4) = tmpext[i];
	}

	//find the camera intrinsics in yml files
	ROS_INFO("Getting camera intrinsics for %s...", camname.c_str());
	string intrinsicsparam = "/P5512intrinsics/";
	intrinsicsparam.append(camname);
	std::vector<double> tmpint = getRosParam<double>(intrinsicsparam, 165, XmlRpc::XmlRpcValue::TypeDouble);
	if (tmpint.size() == 0){
		std::cout << "Fatal error -shutting down." << std::endl;
		return 0;
	}
	double axiszooms[11];
	boost::array<Matrix3d, 11> intrinsicMats;
	boost::array<Matrix<double, 1, 5>, 11> distortionCoeffMats;
	for (int i = 0; i < 11; i++){
		axiszooms[i] = tmpint[i];
	}
	for (int i = 11; i < 110; i++){
		intrinsicMats[(i-11)/9](  ((i-11)/3)%3, (i-11)%3 ) = tmpint[i];
	}
	for (int i = 110; i < tmpint.size(); i++){
		distortionCoeffMats[(i-110)/5](  0, (i-110)%5 ) = tmpint[i];
	}
//	for (int i = 0; i < 11; i++){
//		intrinsicMats[i] = Matrix3d::Zero();
//		distortionCoeffMats[i] = Matrix<double, 1, 5>::Zero();
//	}
	

	//find the camera speedmaps in yml files
	ROS_INFO("Getting speeds for %s...", camname.c_str());
	string speedparam = "/P5512speeds/";
	speedparam.append(camname);
	std::vector<double> tmpspd = getRosParam<double>(speedparam, 33, XmlRpc::XmlRpcValue::TypeDouble);
	double axisspeeds[11], panspeeds[11], tiltspeeds[11];
	for (int i = 0; i < 11; i++){
		axisspeeds[i] = tmpspd[i];
	}
	for (int i = 11; i < 22; i++){
		panspeeds[i-11] = tmpspd[i];
	}
	for (int i = 22; i < 33; i++){
		tiltspeeds[i-22] = tmpspd[i];
	}
	

	//get the ip address
	string ipaddr = "";
	ROS_INFO("Getting IP address for %s...", camname.c_str());
	string ipaddrparam = "/P5512addrs/";
	ipaddrparam.append(camname);
	bool res = ros::param::get(ipaddrparam, ipaddr);
	if (!res){
		ROS_FATAL("ERROR: Could not find %s on the param server!\n Did you make sure to load p5512/config/ipaddrs.yml?", ipaddrparam.c_str());
		return -1;
	}				


	//our camera object
	P5512 camera(camname, ipaddr);
	camera.setBaseExtrinsics(extrinsicMat);
	camera.setIntrinsics(axiszooms, intrinsicMats, distortionCoeffMats);
	camera.setSpeeds(axisspeeds, panspeeds, tiltspeeds);

	//check that the camera is alive and connect to it
	ROS_INFO("Connecting to %s at IP address %s...", camname.c_str(), ipaddr.c_str());
	camera.check();
	camera.connect();
	

	ros::Subscriber wptsub;
	ros::Publisher camstatepub, aprilpub;//, velpub;
	string topicname = camname;
	topicname += "/camera_state";
	camstatepub = nhandle.advertise<p5512::camera_state>(topicname, 1000);

	topicname = camname;
	topicname += "/waypts";
	wptsub = nhandle.subscribe<p5512::waypoint>(topicname, 1000, boost::bind(wayptCallback, _1, boost::ref(camera))  );

	topicname = camname;
	topicname += "/april_tags";
	aprilpub = nhandle.advertise<p5512::apriltags>(topicname, 1000);	

	topicname = camname;
	topicname += "/images";
	image_transport::ImageTransport imtrans(nhandle);
	image_transport::Publisher imgpub = imtrans.advertise(topicname, 20);

	ROS_INFO("Starting p5512_controller for %s...", camname.c_str() );

	ros::Timer camstateCallbackTimer = nhandle.createTimer(ros::Duration(1.0/((PTZGETRATE)/2.0)), 
													  boost::bind(camstateCallback, _1, 
														  						boost::ref(camera),
																				boost::ref(camstatepub)
																  ));
	ros::Timer imgCallbackTimer = nhandle.createTimer(ros::Duration(1.0/((IMGRATE)/2.0)), 
													  boost::bind(imgCallback, _1, 
														  						boost::ref(camera), 
																				boost::ref(imgpub), 
																				boost::ref(aprilpub)
																 ));
	ros::spin();
	/*ros::Time curTime, prevTime;
	prevTime = ros::Time::now();
	ros::Rate loop_timer(PTZGETRATE/2.0); //PTZGETRATE is in P5512.h
	while( ros::ok() ) {
		ros::spinOnce(); //do callbacks
		curTime = ros::Time::now();

		//publish the current state of this camera
		p5512::camera_state caminfomsg;
		caminfomsg.header.stamp = ros::Time::now();
		caminfomsg.name = camname;
		Matrix<double, 3, 4> extrinsic;
		Matrix<double, 3, 3> intrinsic;
		int width, height;
		camera.getCamInfo(extrinsic, intrinsic, width, height);
		caminfomsg.idle = camera.getIdle();
		caminfomsg.width = width;
		caminfomsg.height = height;
		for (int i = 0; i < 9; i++){
			caminfomsg.intrinsic[i] = intrinsic(i/3, i%3);
		}
		for (int i = 0; i < 12; i++){
			caminfomsg.extrinsic[i] = extrinsic(i/4, i%4);
		}
		double grndcorners[8];
		camera.getCamGroundCorners(grndcorners);
		for (int i = 0; i < 8; i++){
			caminfomsg.grndCorners[i] = grndcorners[i];
		}
		double grndpt[2];
		camera.getCamGroundPt(grndpt);
		for (int i = 0; i < 2; i++){
			caminfomsg.grndPt[i] = grndpt[i];
		}

		Vector3d camglobalpos = -extrinsic.block<3,3>(0,0).inverse()*extrinsic.block<3,1>(0,3);
		for (int i = 0; i < 3; i++){
			caminfomsg.pos[i] = camglobalpos(i);
		}
		camstatepub.publish(caminfomsg);
	
		//get April Tags
		std::map<int, Eigen::Vector3d> taglocs = camera.getAprilTagLocs(0.16);
		if (!taglocs.empty()){
			p5512::apriltags tagmsg;
			tagmsg.header.stamp = ros::Time::now();
			tagmsg.name = camname;
			for (std::map<int, Eigen::Vector3d>::iterator it = taglocs.begin(); it != taglocs.end(); it++){
				tagmsg.ids.push_back(it->first);
				geometry_msgs::Vector3 vec;
				vec.x = it->second(0);
				vec.y = it->second(1);
				vec.z = it->second(2);
				tagmsg.vecs.push_back(vec);	
			}
			aprilpub.publish(tagmsg);
		}
		prevTime = curTime;
		loop_timer.sleep();
	}*/

	ROS_INFO("Shutting down P5512 camera %s", camname.c_str());
	camera.disconnect();
	return 0;
}




