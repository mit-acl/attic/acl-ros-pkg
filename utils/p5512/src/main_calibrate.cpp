#include <eigen3/Eigen/Dense>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <ros/ros.h>
#include <ros/package.h>
#include "P5512.h"
#include "geometry_msgs/PoseStamped.h"
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/barrier.hpp>

using namespace std;
using namespace Eigen;


/*
 * This is a function that is used a lot in calibration. It just
 * asks the user to input something, and makes sure it's between min and max values.
 */
template<class D>
D queryUser(string querystr, D min, D max){
	D retval;
	while (true){
		cout << querystr << endl;
		string str;
		getline(cin, str);
		stringstream ss(str);
		if (ss >> retval){
			if (retval >= min && retval <= max){
				return retval;
			}
		}
		cout << "Input must be <= " << max << " and >= " << min << endl;
	}
}


/*
 * This is the camera position calibration algorithm, using apriltags. Essentially this just
 * calls a function in the P5512 library, and then prints the results in the calibration
 * files in the config folder.
 */
void calibratePositionWithApril(string camname, string ip_addr, Vector3d& refpos, boost::mutex& refpos_mutex){
	ROS_INFO("Getting camera intrinsics for %s...", camname.c_str());
	string intrinsicsparam = "/P5512intrinsics/";
	intrinsicsparam.append(camname);
	XmlRpc::XmlRpcValue intrinsicsXML;
	double axiszooms[11];
	boost::array<Matrix3d, 11> intrinsicMats;
	boost::array<Matrix<double, 1, 5>, 11> distortionCoeffMats;
	for (int i = 0; i < 11; i++){
		intrinsicMats[i] = Matrix3d::Zero();
		distortionCoeffMats[i] = Matrix<double, 1, 5>::Zero();
	}
	if (!ros::param::get(intrinsicsparam, intrinsicsXML) ){
		ROS_FATAL("Could not find %s on the param server!", intrinsicsparam.c_str());
		ROS_FATAL("Are you sure you loaded intrinsics.yml?");
		return;
	} else {
		ROS_ASSERT(intrinsicsXML.getType() == XmlRpc::XmlRpcValue::TypeArray);
		if (intrinsicsXML.size() != 165){
			ROS_FATAL("Base pose for %s is not length 165!", intrinsicsparam.c_str());
			ROS_FATAL("It needs to be of format [axis zooms (11), intrinsicmats (11 row major 3x3 mats)");
			return;
		}
		for (int i = 0; i < 11; i++){
			if (intrinsicsXML[i].getType() != XmlRpc::XmlRpcValue::TypeDouble){
				ROS_FATAL("Intrinsics element %d for parameter %s is not *explicitly* a double!", i, intrinsicsparam.c_str());
				ROS_FATAL("It may be an int - make sure it has a decimal in it.");
				return;
			}
			axiszooms[i] = intrinsicsXML[i];
		}
		for(int i = 11; i < 110; i++){
			if (intrinsicsXML[i].getType() != XmlRpc::XmlRpcValue::TypeDouble){
				ROS_FATAL("Intrinsics element %d for parameter %s is not *explicitly* a double!", i, intrinsicsparam.c_str());
				ROS_FATAL("It may be an int - make sure it has a decimal in it.");
				return;
			}
			intrinsicMats[(i-11)/9](  ((i-11)/3)%3, (i-11)%3 ) = intrinsicsXML[i];
		}
		for(int i = 110; i < intrinsicsXML.size(); i++){
			if (intrinsicsXML[i].getType() != XmlRpc::XmlRpcValue::TypeDouble){
				ROS_FATAL("Intrinsics element %d for parameter %s is not *explicitly* a double!", i, intrinsicsparam.c_str());
				ROS_FATAL("It may be an int - make sure it has a decimal in it.");
				return;
			}
			distortionCoeffMats[(i-110)/5](  0, (i-110)%5 ) = intrinsicsXML[i];

		}
	}
	
	cout<<"Starting April calibration routine." << endl;

	P5512 camera(camname, ip_addr);
	camera.setIntrinsics(axiszooms, intrinsicMats, distortionCoeffMats);

	if (camera.check() != 0){
		cout<<"Error connecting to " << camname << endl;
		return;
	}
	camera.connect();
	/* Debugging for undistortion of images
	cv::namedWindow("UndistortedImage");
	cv::Mat img;
	while(true){
	camera.getUndistortedImage(img);
	cv::imshow("UndistortedImage", img);
	char resp = cv::waitKey(20);
		if (resp == 'q'){
			return;
		}
	}
	*/
	Matrix<double, 3, 4> results_baseExtrinsic;
	int numSamples = 100;
	int tagID = 0;
	
	bool success = camera.calibratePositionWithApril(numSamples, tagID, refpos, refpos_mutex, results_baseExtrinsic);

	camera.disconnect();
	if (success){
		cout<<"Appending the extrinsic transformation estimate to extrinsics.yml" << endl;
		std::string packagepath = ros::package::getPath("p5512");
		packagepath.append("/config/extrinsics.yml");
		FILE* outfile = fopen(packagepath.c_str(), "a");
		fprintf(outfile, "%s: [%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf]" ,camname.c_str(), 
				results_baseExtrinsic(0,0), results_baseExtrinsic(0,1), results_baseExtrinsic(0,2), results_baseExtrinsic(0,3),
				results_baseExtrinsic(1,0), results_baseExtrinsic(1,1), results_baseExtrinsic(1,2), results_baseExtrinsic(1,3),
				results_baseExtrinsic(2,0), results_baseExtrinsic(2,1), results_baseExtrinsic(2,2), results_baseExtrinsic(2,3));
		fclose(outfile);
	}
}

/*
 * This function calibrates the camera speeds. Axis uses arbitrary numbers to denote
 * pan/tilt speeds (yay axis!), so this learns the conversion
 */
void calibrateSpeed(string camname, string ip_addr){

	//our camera object
	cout<<"Calibrate Speeds: Starting up..." << endl;
	P5512 camera(camname, ip_addr);
	if (camera.check() != 0){
		cout<<"Error connecting to " << camname << endl;
		return;
	}
	camera.connect();
	double axisspeeds[11];
	double panspeeds[11], tiltspeeds[11];
	bool success = camera.calibrateSpeeds(axisspeeds, panspeeds, tiltspeeds);
	camera.disconnect();
	if (success){
		cout<<"Appending the speed calibration to speedmaps.yml" << endl;
		std::string packagepath = ros::package::getPath("p5512");
		packagepath.append("/config/speedmaps.yml");
		FILE* outfile = fopen(packagepath.c_str(), "a");

		fprintf(outfile, "%s: [", camname.c_str() );
		for (int i = 0; i < 11; i++){
			fprintf(outfile, "%lf,", axisspeeds[i]);
		}
		for (int i = 0; i < 11; i++){
			fprintf(outfile, "%lf,", panspeeds[i]);
		}
		for (int i = 0; i < 10; i++){
			fprintf(outfile, "%lf,", tiltspeeds[i]);
		}
		fprintf(outfile, "%lf]", tiltspeeds[10]);
		fclose(outfile);
	}
}

/*
 * This function calibrates the intrinsic parameters of the cameras by using the chessboard
 * algorithm from opencv.
 */
void calibrateIntrinsics(string camname, string ip_addr){
	cout<<"Calibrate Intrinsics: Starting up..." << endl;
	P5512 camera(camname, ip_addr);
	if (camera.check() != 0){
		cout<<"Error connecting to " << camname << endl;
		return;
	}
	camera.connect();

	string q1 = "How many chess boards per zoom level are we going to use? (30 should give good accuracy): ";
	string q2 = "How many inner corners are there horizontally on the chess board?: ";
	string q3 = "How many inner corners are there vertically on the chess board?: ";
	string q4 = "What is the distance between each corner horizontally (in m)?: ";
	string q5 = "What is the distance between each corner vertically (in m)?: ";
	string q6 = "What pan angle (deg) should be used for calibration?: "; 
	string q7 = "What tilt angle (deg) should be used for calibration?: ";
	int numBoards = queryUser<int>(q1, 0, 10000);
	int numInnerCornersHor = queryUser<int>(q2, 0, 10000);
	int numInnerCornersVer = queryUser<int>(q3, 0, 10000);
	double distHor = queryUser<double>(q4, 0, 10000), distVer = queryUser<double>(q5, 0, 10000);
	double calibPan = queryUser<double>(q6, -180, 180), calibTilt = queryUser<double>(q7, -180, 180);
	
	double axiszooms[11];
	boost::array<Matrix3d, 11> intrinsicMatArray;
	boost::array<Matrix<double, 1, 5>, 11> distortionArray;
	bool success = camera.calibrateIntrinsics(numBoards, numInnerCornersHor, numInnerCornersVer, distHor, distVer,calibPan, calibTilt, axiszooms, intrinsicMatArray, distortionArray);
	camera.disconnect();
	if (success){
		cout<<"Appending the intrinsics calibration to intrinsics.yml" << endl;
		std::string packagepath = ros::package::getPath("p5512");
		packagepath.append("/config/intrinsics.yml");
		FILE* outfile = fopen(packagepath.c_str(), "a");	
		fprintf(outfile, "%s: [", camname.c_str() );
		//print zooms
		for (int i = 0; i < 11; i++){
			fprintf(outfile, "%lf,", axiszooms[i]);
		}
		//print intrinsic matrices
		for (int i = 0; i < 11; i++){
			for (int j = 0; j < 9; j++){
					fprintf(outfile, "%lf,", (intrinsicMatArray[i])(j/3, j%3) );
			}
		}
		//print distortion coefficients
		for (int i = 0; i < 11; i++){
			for (int j = 0; j < 5; j++){
				if (i == 10 && j == 4){
					fprintf(outfile, "%lf]", (distortionArray[i])(0,j) );
				} else {
					fprintf(outfile, "%lf,", (distortionArray[i])(0,j) );
				}
			}
		}
		fclose(outfile);
	}
}




void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg, Vector3d& calib_refpos, boost::mutex& calib_refposmutex){
		calib_refposmutex.lock();
		calib_refpos << msg->pose.position.x, msg->pose.position.y,msg->pose.position.z;
		calib_refposmutex.unlock();
}

void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg){
}

/*
 * This is the loop for processing ros callbacks for the refpos object (used to calibrate position
 */
void rosloop(){
	ros::Rate loop_timer(30);
	while(ros::ok()){
		//process all callbacks	
		boost::this_thread::interruption_point();
		ros::spinOnce();
		loop_timer.sleep();
	}
}

int main(int argc, char** argv){
	
	//get camera name, get reference object name
	string camname, refmarker;
	bool calibrate_speed = false, calibrate_position = false, calibrate_intrinsics = false;
	string rosrunstr = "rosrun";
	if (rosrunstr.compare(argv[0])==0){
		if (argc <= 5){
			cout<<"Incorrect calling syntax." << endl;
			cout<<"Should be: rosrun p5512 p5512_calibration [-s] [-p] [-i] <camname> <refmarkername>" << endl;
			return -1;
		}
		
	} else {
		if (argc <= 3){
			cout<<"Incorrect calling syntax." << endl;
			cout<<"Should be: rosrun p5512 p5512_calibration [-s] [-p] [-i] <camname> <refmarkername>" << endl;
			return -1;
		}
	}
	//check what the user wants to do
	camname = argv[argc-2];
	refmarker = argv[argc-1];
	for (int i = 0; i < argc; i++){
		string flag = argv[i];
		if (flag.compare("-s") == 0){
			calibrate_speed = true;
		} else if (flag.compare("-p") == 0){
			calibrate_position = true;
		} else if (flag.compare("-i") == 0){
			calibrate_intrinsics = true;
		}
	}

	//if nothing was asked for, the user is dumb
	if (!calibrate_speed && !calibrate_position && !calibrate_intrinsics){
		cout<<"Incorrect calling syntax." << endl;
		cout<<"Should be: rosrun p5512 p5512_calibration <camname> <refmarkername> [-s] [-p] [-i]" << endl;
		cout<<"Please use the switches -s/-p to indicate calibration of speed, position, intrinsics or some combination" << endl;
		return -1;
	}
	
	
	//connect and subscribe	to RAVEN
	string nodename = "p5512_calibrate_";
	nodename.append(camname);
	ros::init(argc, argv, nodename);
	ros::NodeHandle nhandle;

	ROS_INFO("Getting IP address for %s...", camname.c_str());
	string ipaddrparam = "/P5512addrs/";
	ipaddrparam.append(camname);
	string ipaddr;
	bool res = ros::param::get(ipaddrparam, ipaddr);
	if (!res){
		ROS_FATAL("ERROR: Could not find %s on the param server!\n Did you make sure to load p5512/config/ipaddrs.yml?", ipaddrparam.c_str());
		return -1;
	}

	//start up the rosloop
	boost::thread rosloopthread(&rosloop);

	//calibrate whatever the user wanted to calibrate
	if (calibrate_speed){
		calibrateSpeed(camname, ipaddr);
	}
	if (calibrate_intrinsics){
		calibrateIntrinsics(camname, ipaddr);
	}
	if (calibrate_position){
		string topicname = "/";
		topicname.append(refmarker);
		topicname.append("/pose");
		ROS_INFO("Subscribing to: %s", topicname.c_str());
		boost::mutex refposmutex;
		Vector3d refpos;
	 	ros::Subscriber refposesub = nhandle.subscribe<geometry_msgs::PoseStamped>(topicname, 1000, 
					boost::bind(&poseCallback, _1, boost::ref(refpos), boost::ref(refposmutex))
						);
		// DEPRECATED calibratePosition(camname, ipaddr, refpos, refposmutex);
		calibratePositionWithApril(camname,  ipaddr,  refpos, refposmutex);

	}
	rosloopthread.interrupt();
	cout << "Done calibration, shutting down." << endl;
	return 0;
}



/*
 * This function calibrates the position of a p5512 camera. It requires a separate Vicon object (refpos, locked by refpos_mutex)
 * and backs out its own position from that.
 *
 * NOTE: THIS FUNCTION IS DEPRECATED. PLEASE USE CALIBRATEPOSITIONWITHAPRIL BELOW
 *
 */
/* DEPRECATED
void calibratePosition(string camname, string ip_addr, Vector3d& refpos, boost::mutex& refpos_mutex){
	int numread = 0;
	cout << "Calibrate Base Position: Starting up..." << endl;
	cout << "First I need some information on how you want me to figure out the camera position given the reference measurements (which we will collect in a moment)." << endl;
	cout << "The algorithm I will use to determine this is RANSAC, which requires a few parameters:" << endl;

	string q1 = "How many points in a RANSAC sample? (6 is probably good, more will be slow and has a risk of including outliers, but if you were really precise with the camera a higher number will get a better result: ";
	string q2 = "How many RANSAC samples do you want to try? ( min[50, (num ref pts) Choose (ransac sample size)] is probably good, more gives a better chance of finding a good solution, but will be slower): ";
	string q3 = "What is the RANSAC voting tolerance on position? (0.3m radius is probably good, higher/lower makes voting less/more precise, respectively): ";
	string q4 = "How many random restarts of the position optimization should I do? (10 is good, more will be slower but has more chance of finding global optimum): ";
	int ransacsize = queryUser<int>(q1, 0, 10000);	
	int ransactsts = queryUser<int>(q2, 0, 10000);
	double ransacthresh = queryUser<double>(q3, 0, 10000);
	int opttries = queryUser<int>(q4, 0, 10000);

	cout << "Done getting input. Starting calibration routine." << endl;

	P5512 camera;
	camera.setIP(ip_addr);
	camera.setName(camname);
	if (camera.check() != 0){
		cout <<"Error connecting to " << camname << endl;
		return;
	}
	camera.connect();

	Vector3d results_position;
	Matrix3d results_rotation;
	bool success = camera.calibratePosition(ransactsts, ransacsize, ransacthresh, opttries, refpos, refpos_mutex, results_position, results_rotation);
	camera.disconnect();
	if (success){
		cout << "The smoothed estimate is:" << endl;
		cout << "Position: " << endl << results_position << endl;
		cout << "Rotation Mat: " << endl << results_rotation << endl;
		cout << "Therefore, the extrinsic transformation for the camera is:" << endl;
		
		Matrix<double, 3, 4> extrinsic;
		extrinsic.block<3,3>(0,0) = results_rotation;
		extrinsic.block<3,1>(0,3) = -results_rotation*results_position;
		cout << extrinsic << endl;

		cout <<"Appending the extrinsic transformation estimate to baselocs.yml" << endl;
		std::string packagepath = ros::package::getPath("p5512");
		packagepath.append("/config/extrinsics.yml");
		FILE* outfile = fopen(packagepath.c_str(), "a");
		fprintf(outfile, "%s: [%lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf, %lf]",camname.c_str(), 
				extrinsic(0,0), extrinsic(0,1), extrinsic(0,2), extrinsic(0,3),
				extrinsic(1,0), extrinsic(1,1), extrinsic(1,2), extrinsic(1,3),
				extrinsic(2,0), extrinsic(2,1), extrinsic(2,2), extrinsic(2,3));
		fclose(outfile);
	}
}*/




