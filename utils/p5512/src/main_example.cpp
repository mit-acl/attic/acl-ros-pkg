#include <ros/ros.h>
#include "p5512/waypoint.h"
#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <image_transport/image_transport.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <cv_bridge/CvBridge.h>

using namespace std;

std::vector<p5512::waypoint> genSquarePath(string);
std::vector<p5512::waypoint> genLinePath2Z(string);

void newMeasurement(const sensor_msgs::ImageConstPtr& msg){
	sensor_msgs::CvBridge bridge;
  	try{
		std::cout<<"got new iamge"<<std::endl;
    		cvShowImage("view", bridge.imgMsgToCv(msg, "bgr8"));
		int a;
		std::cin>>a;
  	}
  	catch (sensor_msgs::CvBridgeException& e){
	    	ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
  	}
}

int main(int argc, char** argv){

	if (argc != 2){ 
		ROS_FATAL("Syntax for calling: rosrun p5512 exampleWptPub [cam name]");
		ROS_FATAL("You called exampleWptPub with argc=%d:", argc);
		for (int i = 0; i < argc; i++){
			ROS_FATAL("argv %d = %s", i, argv[i]);
		}
		return -1;
	}

	//get the name of the camera
	string camname;
	camname = argv[1];

	string nodename = "exampleWptPub_";
	nodename.append(camname);
	ros::init(argc, argv, nodename);
	ros::NodeHandle nhandle;
	//ros::Subscriber imagesub;
	ros::Publisher wptpub;

	string topicname;
	topicname = camname;
	topicname += "/waypts";
	wptpub = nhandle.advertise<p5512::waypoint>(topicname, 1000);

	topicname = "/";
	topicname += camname;
	topicname += "/images";
	std::cout<<"topic name:"<<topicname<<std::endl;
	nhandle.subscribe(topicname, 1000, &newMeasurement);

	//std::vector<p5512::waypoint> path = genSquarePath(camname);
	std::vector<p5512::waypoint> path = genLinePath2Z(camname);
	ros::Rate loop_timer(0.4);
	int wptidx = 0;
	while(ros::ok()){
		wptpub.publish(path[wptidx]);
		wptidx++;
		wptidx = (wptidx == path.size() ? 0 : wptidx);
		loop_timer.sleep();
	}
	return 0;
}



std::vector<p5512::waypoint> genSquarePath(string camname){
	//set up a square path
	p5512::waypoint blwpt, tlwpt, brwpt, trwpt;

	blwpt.wpttype = "CAMWPT_PTZ";
	brwpt.wpttype = "CAMWPT_PTZ";
	tlwpt.wpttype = "CAMWPT_PTZ";
	trwpt.wpttype = "CAMWPT_PTZ";

	blwpt.name = camname;
	tlwpt.name = camname;
	brwpt.name = camname;
	trwpt.name = camname;

	blwpt.pan = 0;
	blwpt.tilt = 0;
	blwpt.zoom = 0;
	tlwpt.pan = 30;
	tlwpt.tilt = 0;
	tlwpt.zoom = 0;
	trwpt.pan = 30;
	trwpt.tilt = -30;
	trwpt.zoom = 0;
	brwpt.pan = 0;
	brwpt.tilt = -30;
	brwpt.zoom = 0;

	blwpt.dt = 2;
	tlwpt.dt = 2;
	brwpt.dt = 2;
	trwpt.dt = 2;

	blwpt.timeout = 10;
	tlwpt.timeout = 10;
	brwpt.timeout = 10;
	trwpt.timeout = 10;

	blwpt.clear = 0;
	tlwpt.clear = 0;
	brwpt.clear = 0;
	trwpt.clear = 0;

	std::vector<p5512::waypoint> squarepath;
	squarepath.push_back(blwpt);
	squarepath.push_back(tlwpt);
	squarepath.push_back(trwpt);
	squarepath.push_back(brwpt);
	return squarepath;
}


std::vector<p5512::waypoint> genLinePath2Z(string camname){
	//set up a line
	p5512::waypoint swpt, fwpt;

	swpt.wpttype = "CAMWPT_2Z";
	fwpt.wpttype = "CAMWPT_2Z";

	swpt.name = camname;
	fwpt.name = camname;
	
	swpt.x = 0;
	swpt.y = 0;
	swpt.zoom = 0;
	fwpt.x = 0;
	fwpt.y = -1;
	fwpt.zoom = 0;

	swpt.dt = 2;
	fwpt.dt = 2;

	swpt.timeout = 10;
	fwpt.timeout = 10;

	swpt.clear = 0;
	fwpt.clear = 0;

	std::vector<p5512::waypoint> linepath;
	linepath.push_back(swpt);
	linepath.push_back(fwpt);
	return linepath;
}
