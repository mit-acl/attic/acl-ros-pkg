Description:
This package provides both automatic and manual control for the rate table, as well as processing of gyro feedback from the autopilot to determine the scale by which its measurements need to be multiplied to be accurate.

Rate Table Microcontroller wiring:
The wiring depends on the code on the microcontroller. Check it to verify that these instructions are still up to date.
- Small motor step is OC2/RD1.
- Small motor direction is OC3/RD2.
- Big motor step is OC8/RD7.
- Big motor direction is OC7/RD6.

Instructions for usage:
Once this package compiles, it will create 4 executables. To run the automatic calibration process, perform the following steps in order. (Note: The microcontroller with the control software for the rate table will be referred to here as table-controller)
- Make sure the wires going into the motor spinning the smaller platform are not braided tightly. Spin the large platform to untangle them if necessary. In general, be weary of the large platform spinning too much in one direction because the wires may be cut from the tension, which also is very likely to break the small motor driver.
- Place the autopilot to be calibrated on the smaller rotating platform with its x-axis along the axis of the larger platform and its z-axis perpendicular to the larger platform on the opposite side from the motor controlling the smaller platform. Also place a 5V battery on the same platform and connect it to the autopilot. Secure both, so that they don't fall off when the rate table starts spinning.
- Make sure both motor drivers are appropriately connected to their respective motors (wiring available from driver documentation), as well as to the table-controller.
- Provide a 5V supply for both the table-controller and the larger motor's driver.
- Establish a wired serial connection between the table-controller and the computer you are running this package on. Make sure the usb port from which the serial connection is established matches the one referred to in the code (The global port variable in auto.cpp and manual.cpp). If necessary, change the code.
- Turn on the autopilot. Make sure it is running code that is sending a SensorPacket.
- Establish a wireless serial connection between the autopilot and the computer you are running this package on.Make sure the usb port from which the serial connection is established matches the one referred to in the code (The global port variable in feedbackHandler.cpp). If necessary, change the code.
- Turn on the table-controller.
- Provide a 13.8V supply for both motor drivers. It is advisable to place a fan above the larger driver to prevent overheating.
- Run the listener executable.
- Run the feedback executable. If you have correctly established a connection with the autopilot, the output should be non-zero.
- Run the auto executable. This will move the rate table for about 3 minutes. During this time, make sure that the wires going into the small motor are not close to being cut (if they are, immediately disconnect the 13.8V power supply). You may also run rqt from the command line and select the plot plugin. You can plot the topics plot/gyro/--x or y or z here-- and plot/table/--x or y or z here-- to see for yourself how accurate the autopilot's gyro is.
- Once the rate table is done turning, the auto executable will output "done" and terminate. You may then disconnect the 13.8V power supply and the 5V power supply in that order.
- You may also terminate the feedback executable and turn off the autopilot.
- The listener executable will have output the correct scaling coefficient. Apply this coefficient to the autopilot's code to get more accurate measurements.

Manual mode:
Once you have correctly set up your drivers and table-controller as described above, you can also run the rate table in manual mode, which exists mainly for debugging purposes. To do this, run the manual executable. It works with a series of prompts and allows customizable movement for the rate table. It refers to the small motor as motor 1 and the large motor as motor 2. When you select a motor, you are asked to select a mode. Mode 0 stops the motor. Mode 1 allows you to spin the motor at a constant frequency, though there are limits as to how fast (about 3-4 Hz) or slowly (0.06 Hz) it can spin. Mode 2 will turn the motor by an angle you provide and then stop. Mode 3 allows you to select the direction in which the motor should spin.
