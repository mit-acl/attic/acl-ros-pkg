/*
 * auto.cpp
 *
 *  Created on: Jul 10, 2013
 *      Author: swarm
 */


// local includes
#include "ratetable.hpp"
#include <string>
#include <sstream>
#include <boost/thread.hpp>
#include "std_msgs/String.h"
#include "globals.hpp"
#include <unistd.h>





int argc1;
char **argv1;
std::string port = "/dev/ttyUSB0";
int axis;
int end = 0;

void talk()
{
	ros::init(argc1, argv1, "talker");
	ros::NodeHandle talker;
	ros::Publisher pub = talker.advertise<std_msgs::String>("table", 1000);
	ros::Rate loop_rate(100);
	while (ros::ok()){
		std_msgs::String msg;
		std::stringstream ss;
		double freq = (50000000.0/((double)(period*800*TIMER3_SCALE)))*2*PI;
		double freq2 = (50000000.0/((double)(period2*800*TIMER3_SCALE)))*2*PI;
		ss << axis << " " << freq*(-2*dir+1)*(1-stop) << " " << freq2*(2*dir2-1)*(1-stop2);
		msg.data = ss.str();
		pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
		if (end)
		{
			std_msgs::String msg;
			std::stringstream ss;
			ss << "done";
			msg.data = ss.str();
			pub.publish(msg);
			ros::spinOnce();
			loop_rate.sleep();
			std::cout << "done";
			break;
		}
	}

}

void testAxis(RateTable rt, int motor)
{
	rt.mot = motor;

	rt.m = 3;
	rt.direction = DEFAULT_DIR;
	rt.sendCmd();


		rt.m = 1;
		rt.T = 50;
		rt.sendCmd();
		std::cout << "+fast\n";

		sleep(11);

		rt.T = 250;
		rt.sendCmd();
		std::cout << "+medium\n";

		sleep(7);

		rt.T = 2000;
		rt.sendCmd();
		std::cout << "+slow\n";

		sleep(5);

		rt.mot = 0;
		rt.sendCmd();
		std::cout << "stop\n";

		sleep(3);

		rt.mot = motor;
		rt.m = 3;
		rt.direction = 1 - DEFAULT_DIR;
		rt.sendCmd();

		rt.m = 1;
		rt.sendCmd();
		std::cout << "-slow\n";

		sleep(3);

		rt.T = 250;
		rt.sendCmd();
		std::cout << "-medium\n";

		sleep(5);

		rt.T = 50;
		rt.sendCmd();
		std::cout << "-fast\n";

		sleep(8);

		rt.mot = 0;
		rt.sendCmd();
		std::cout << "stop\n";

		sleep(11);
}

void switchAxis(RateTable rt)
{
	rt.mot = 1;

	rt.m = 3;
	rt.direction = 1 - DEFAULT_DIR;
	rt.sendCmd();

	rt.m = 2;
	rt.deg = 900; //900 tenths of degrees a.k.a. 90 degrees
	rt.sendCmd();

	sleep(2);

}

void returnToInitPos(RateTable rt)
{
	rt.mot = 2;
	rt.m = 3;
	if (revs2 >= 0)
	{
		rt.direction = 1 - DEFAULT_DIR;
		rt.sendCmd();
		rt.m = 2;
		rt.deg = revs2*360*10 + (int16_t)(revrad2*180/PI*10);
		rt.sendCmd();
		sleep(revs2*2 + 2);
	}
	else if (revs2 < 0)
	{
		rt.direction = DEFAULT_DIR;
		rt.sendCmd();
		rt.m = 2;
		rt.deg = -revs2*360*10 + (int16_t)(-revrad2*180/PI*10);
		rt.sendCmd();
		sleep(-revs2*2 + 2);
	}
}



int main(int argc, char **argv) {
	argc1 = argc;
	argv1 = argv;

	ros::init(argc, argv, "rate_table");
	ros::NodeHandle n;

	boost::thread t(&talk);



	//## Welcome screen
	ROS_INFO(
			"\n\nStarting rate table auto control code\n\n\n");


	// initialize rate table class
	RateTable rt;

	//int mode;



	std::cout << "1st axis\n";
	axis = X_AXIS;
	testAxis(rt, 2);	//1st axis
	returnToInitPos(rt);
	axis = Z_AXIS;
	switchAxis(rt);
	std::cout << "2nd axis\n";
	axis = Y_AXIS;
	testAxis(rt, 2);	//2nd axis
	returnToInitPos(rt);
	std::cout << "3rd axis\n";
	axis = Z_AXIS;
	testAxis(rt, 1);	//3rd axis
	end = 1;
	rt.mot = 0;
	rt.sendCmd();
	sleep(1);
	t.join();






//	while(1){
//
////		rt.turn_on = false;
////		char input;
////		std::cout << "turn on?, (y)es or (n)o: ";
////		std::cin >> input;
////		std::cout << std::endl << std::endl;
////		if (input == 'y')
////			rt.turn_on = true;
////		else if (input == 'n')
////			rt.turn_on = false;
////		else
////			std::cout << "wrong command!" << std::endl << std::endl;
////
////		rt.sendCmd();
//
//
//
//
//		std::string instr;
//		double input;
//		int inmot;
//
//		std::cout << "Select motor (0 to stop both motors): ";
//		getline(std::cin, instr);
//		std::stringstream(instr) >> inmot;
//		if (inmot < 0 || inmot > 2){
//			std::cout << "Motor must be between 0 and 2\n";
//		}
//		else if (inmot == 0){
//			if (!instr.compare(std::string("0"))){
//							rt.mot = 0;
//							rt.sendCmd();
////							vel1 = 0;
////							vel2 = 0;
//						}
//		}
//		else if (inmot == 1){
//			rt.mot = 1;
//			while (1)
//			{
//			std::cout << "Select mode (-1 to switch motor): ";
//					int inmode;
//					getline(std::cin, instr);
//					std::stringstream(instr) >> inmode;
//					if (inmode > 3){
//						std::cout << "Mode must be between 0 and 3\n";
//					}
//					else if (inmode < 0){
//						break;
//					}
//					else if (inmode == 0){
//						if (!instr.compare(std::string("0"))){
//							mode = 0;
//							rt.m = 0;
//							rt.sendCmd();
////							vel1 = 0;
//						}
//					}
//					else if (inmode == 1){
//
//						while (1){
//							std::cout << "Insert frequency in Hz (-1 to switch mode): ";
//							getline(std::cin, instr);
//							//** Insert check for validity here **
//							std::stringstream(instr) >> input;
//
//							if (input < 0){
//								break;
//							}
//							if (input == 0){
//								if (!instr.compare(std::string("0"))){
//									rt.m = 0;
//									rt.sendCmd();
//									//vel1 = 0;
//								}
//								continue;
//							}
//							mode = 1;
//							rt.m = 1;
//
//							//vel1 = input;
//
//							int32_t newperiod = (int32_t) 50000000/((int32_t)(input*800*TIMER3_SCALE));
//
//							if (newperiod > 10 && newperiod < 2400){
//								std::cout << newperiod;
//								std::cout << "\n";
//								rt.T = (int) newperiod;
//							}
//							else{
//								std::cout << "Out of bounds: ";
//								std::cout << newperiod;
//								std::cout << "\n";
//							}
//							rt.sendCmd();
//						}
//					}
//					else if (inmode == 2){
//						mode = 2;
//						rt.m = 2;
//						while (1){
//							std::cout << "Insert angle in degrees (-1 to switch mode): ";
//							getline(std::cin, instr);
//							std::stringstream(instr) >> input;
//							if (input < 0){
//								break;
//							}
//
//							rt.deg = (int16_t) (10*input);
//							rt.sendCmd();
//							//vel1 = -1;
//						}
//					}
//					else{//inmode == 3
//						mode = 0;
//						rt.m = 3;
//						while (1){
//							std::cout << "Insert 0 for CW or 1 for CCW (-1 to switch mode): ";
//							getline(std::cin, instr);
//							std::stringstream(instr) >> input;
//							if (input < 0){
//								rt.m = 0;
//								break;
//							}
//							else if (input == 0){
//								if (!instr.compare(std::string("0"))){
//									rt.direction = 0;
//									rt.sendCmd();
//									rt.m = 0;
//									//dir1 = 0;
//									//vel1 = 0;
//									break;
//								}
//								continue;
//							}
//							else if (input == 1){
//								rt.direction = 1;
//								rt.sendCmd();
//								rt.m = 0;
//								//dir1 = 1;
//								//vel1 = 0;
//								break;
//							}
//
//						}
//
//					}
//			}
//		}
//
//		else if (inmot == 2){
//					rt.mot = 2;
//					while (1)
//					{
//					std::cout << "Select mode (-1 to switch motor): ";
//							int inmode;
//							getline(std::cin, instr);
//							std::stringstream(instr) >> inmode;
//							if (inmode > 3){
//								std::cout << "Mode must be between 0 and 3\n";
//							}
//							else if (inmode < 0){
//								break;
//							}
//							else if (inmode == 0){
//								if (!instr.compare(std::string("0"))){
//									mode = 0;
//									rt.m = 0;
//									rt.sendCmd();
//									//vel2 = 0;
//								}
//							}
//							else if (inmode == 1){
//
//								while (1){
//									std::cout << "Insert frequency in Hz (-1 to switch mode): ";
//									getline(std::cin, instr);
//									//** Insert check for validity here **
//									std::stringstream(instr) >> input;
//
//									if (input < 0){
//										break;
//									}
//									if (input == 0){
//										if (!instr.compare(std::string("0"))){
//											rt.m = 0;
//											rt.sendCmd();
//											//vel2 = 0;
//										}
//										continue;
//									}
//									mode = 1;
//									rt.m = 1;
//
//									//vel2 = input;
//
//									int32_t newperiod = (int32_t) 50000000/((int32_t)(input*800*TIMER3_SCALE));
//
//									if (newperiod > 10 && newperiod < 2400){
//										std::cout << newperiod;
//										std::cout << "\n";
//										rt.T = (int) newperiod;
//									}
//									else{
//										std::cout << "Out of bounds: ";
//										std::cout << newperiod;
//										std::cout << "\n";
//									}
//									rt.sendCmd();
//								}
//							}
//							else if (inmode == 2){
//								mode = 2;
//								rt.m = 2;
//								while (1){
//									std::cout << "Insert angle in degrees (-1 to switch mode): ";
//									getline(std::cin, instr);
//									std::stringstream(instr) >> input;
//									if (input < 0){
//										break;
//									}
//
//									rt.deg = (int16_t) (10*input);
//									rt.sendCmd();
//									//vel2 = -1;
//								}
//							}
//							else{//inmode == 3
//								mode = 0;
//								rt.m = 3;
//								while (1){
//									std::cout << "Insert 0 for CW or 1 for CCW (-1 to switch mode): ";
//									getline(std::cin, instr);
//									std::stringstream(instr) >> input;
//									if (input < 0){
//										rt.m = 0;
//										break;
//									}
//									else if (input == 0){
//										if (!instr.compare(std::string("0"))){
//											rt.direction = 0;
//											rt.sendCmd();
//											rt.m = 0;
//											//dir2 = 0;
//											//vel2 = 0;
//											break;
//										}
//										continue;
//									}
//									else if (input == 1){
//										rt.direction = 1;
//										rt.sendCmd();
//										rt.m = 0;
//										//dir2 = 1;
//										//vel2 = 0;
//										break;
//									}
//
//								}
//
//							}
//					}
//				}
//
//
//
//
//
//	}

	return 0;
}

