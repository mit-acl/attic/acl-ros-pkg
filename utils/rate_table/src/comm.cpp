/*
 * comm.cpp
 *
 *  Created on: March 26, 2011
 *      Author: mark
 */

#include "ratetable.hpp"
#include "globals.hpp"



void RateTable::sendCmd()
{
  // Convert command data to a command packet - scale the data

	tRateTableCmdPacket pkt;

	pkt.period = T;
	pkt.tenthsofdegrees = deg;
	pkt.mode = m;
	pkt.dir = direction;
	pkt.motor = mot;

	SendPacket(PACKETID_RATETABLECMD, sizeof(tRateTableCmdPacket), (uint8_t *)&pkt);

}

int16_t volt_tmp;
void RateTable::parsePacket(uint8_t ID, uint8_t length, uint8_t * data)
{
	gyroX=1;
  switch (ID)
  {
//    case PACKETID_VOLTAGE:
//      // voltage packet
//      memcpy(&volt_tmp, data, sizeof(tVoltagePacket));
//      healthData.voltage = (double)volt_tmp / 1000.0;
//      break;
//
//    case PACKETID_AHRS:
//      // AHRS data packetSendPacket
//      memcpy(&ahrs, data, sizeof(tAHRSpacket));
//      healthData.rate.x = ((double)ahrs.p) / ROT_SCALE;
//      healthData.rate.y = ((double)ahrs.q) / ROT_SCALE;
//      healthData.rate.z = ((double)ahrs.r) / ROT_SCALE;
//      healthData.att.w = ((double)ahrs.qo_est) / QUAT_SCALE;
//      healthData.att.x = ((double)ahrs.qx_est) / QUAT_SCALE;
//      healthData.att.y = ((double)ahrs.qy_est) / QUAT_SCALE;
//      healthData.att.z = ((double)ahrs.qz_est) / QUAT_SCALE;
//      healthData.att_meas.w = ((double)ahrs.qo_meas) / QUAT_SCALE;
//      healthData.att_meas.x = ((double)ahrs.qx_meas) / QUAT_SCALE;
//      healthData.att_meas.y = ((double)ahrs.qy_meas) / QUAT_SCALE;
//      healthData.att_meas.z = ((double)ahrs.qz_meas) / QUAT_SCALE;
//
//      break;
//
//    case PACKETID_HEALTH:
//      // AHRS data packetSendPacket
//      memcpy(&healthPacket, data, sizeof(tHealthPacket)); // todo: implement proper scaling of current and temp values
//      healthData.current[0] = (double)healthPacket.current1;
//      healthData.current[1] = (double)healthPacket.current2;
//      healthData.current[2] = (double)healthPacket.current3;
//      healthData.current[3] = (double)healthPacket.current4;
//      healthData.temperature[0] = (double)healthPacket.temp1;
//      healthData.temperature[1] = (double)healthPacket.temp2;
//      healthData.temperature[2] = (double)healthPacket.temp3;
//      healthData.temperature[3] = (double)healthPacket.temp4;
//
//      break;
//
    case PACKETID_SENSORS:
    	tSensorsPacket sensorsPacket;
      memcpy(&sensorsPacket, data, sizeof(tSensorsPacket));
//      sensors.gyro.x = (double)sensorsPacket.gyroX;
//      sensors.gyro.y = (double)sensorsPacket.gyroY;
//      sensors.gyro.z = (double)sensorsPacket.gyroZ;
//      sensors.accel.x = (double)sensorsPacket.accelX;
//      sensors.accel.y = (double)sensorsPacket.accelY;
//      sensors.accel.z = (double)sensorsPacket.accelZ;
//      sensors.mag.x = (double)sensorsPacket.magX;
//      sensors.mag.y = (double)sensorsPacket.magY;
//      sensors.mag.z = (double)sensorsPacket.magZ;
//      sensors.sonar = (double)sensorsPacket.sonarZ;
//      sensors.sonar /= 1000; // into meters
//      sensors.pressure = (double)sensorsPacket.pressure;
//
//      *********** Sensor Data ********
//      static int count = 0;
//      count++;
//      sensors.header.stamp = ros::Time::now();
//      sensors.header.frame_id = name;
//      sensors.true_pose.position.x = state.pos.getX();
//      sensors.true_pose.position.y = state.pos.getY();
//      sensors.true_pose.position.z = state.pos.getZ();
//      tf::quaternionTFToMsg(state.att, sensors.true_pose.orientation);
//      tf::vector3TFToMsg(state.vel, sensors.true_vel.linear);
//      tf::vector3TFToMsg(state.rate, sensors.true_vel.angular);
//      if (count > 1000)
//        sensorPub.publish(sensors);
    	gyroX = sensorsPacket.gyroX;
    	gyroY = sensorsPacket.gyroY;
    	gyroZ = sensorsPacket.gyroZ;
    	accelX = sensorsPacket.accelX;
    	accelY = sensorsPacket.accelY;
    	accelZ = sensorsPacket.accelZ;
      break;

    case PACKETID_RTFEEDBACK:
    	tRTFeedbackPacket feedbackPkt;
    	memcpy(&feedbackPkt, data, sizeof(tRTFeedbackPacket));
    	period = feedbackPkt.period;
    	period2 = feedbackPkt.period2;
    	stop = feedbackPkt.stop;
    	stop2 = feedbackPkt.stop2;
    	dir = feedbackPkt.dir;
    	dir2 = feedbackPkt.dir2;
    	revs = feedbackPkt.revs;
    	revs2 = feedbackPkt.revs2;
    	revrad = ((double)feedbackPkt.revrad)/1000;
    	revrad2 = ((double)feedbackPkt.revrad2)/1000;
    	break;


//
//    case PACKETID_ALTITUDE:
//      memcpy(&altitudePacket, data, sizeof(tAltitudePacket));
//      sensors.sonar = (double)altitudePacket.sonarZ;
//      sensors.sonar /= 1000; // into meters
//      sensors.pressure = (double)altitudePacket.pressure;
//      sensors.temperature = (double)altitudePacket.temperature;
//      sensors.accel.x = ((double)altitudePacket.accelX) / 1000;
//      sensors.accel.y = ((double)altitudePacket.accelY) / 1000;
//      sensors.accel.z = ((double)altitudePacket.accelZ) / 1000;
//      break;

    default:
      ROS_INFO("Packet ID not recognized!\n");
      break;
  }

}

void RateTable::SendPacket(uint8_t ID, uint8_t length, uint8_t * data)
{
  // Calculate the checksum
  uint8_t checkSum = length + ID;
  for (int i = 0; i < length; i++)
  {
    checkSum += data[i];
  }
  //checkSum = (uint8_t)(256-checkSum);
  checkSum ^= 0xFF;
  checkSum += 1;

  // Send start sequence, ID, length
  uint8_t header[4];
  header[0] = 0xFF;
  header[1] = 0xFE;
  header[2] = ID;
  header[3] = length;
  ser.spSend(header, 4);

  // Send data
  ser.spSend(data, length);

  // Send check sum
  ser.spSend(&checkSum, 1);

}

void *serListen(void *param)
{
  RateTable * quad = (RateTable *)param;
  uint8_t packetID;
  uint8_t packetLength;
  uint8_t packetData[256];
  uint8_t checksum;

  uint8_t thisByte = 0;
  uint8_t lastByte = 0;

  uint8_t STX1 = 0xFF;
  uint8_t STX2 = 0xFE;

  while (1)
  {

    // Wait for packet start
    while (!(thisByte == STX2 && lastByte == STX1))
    {
      lastByte = thisByte;
      thisByte = quad->ser.spReceiveSingle();
    }
    thisByte = lastByte = 0;

    // Get packet ID and length
    packetID = quad->ser.spReceiveSingle();
    packetLength = quad->ser.spReceiveSingle();
    checksum = packetLength + packetID;

    // Get packet data
    for (int i = 0; i < packetLength; i++)
    {
      packetData[i] = quad->ser.spReceiveSingle();
      checksum += packetData[i];
    }

    // Get checksum
    checksum += quad->ser.spReceiveSingle();

    // Parse packet if checksum correct
    if (checksum == 0)
    {
      quad->parsePacket(packetID, packetLength, packetData);
    }
    else
    {
      ROS_INFO("Bad checksum! \n");
    }
  }

}

