/*
 * feedbackHandler.cpp
 *
 *  Created on: Jul 1, 2013
 *      Author: swarm
 */

#include "comm.hpp"
#include "globals.hpp"
#include "ratetable.hpp"
#include "std_msgs/String.h"
#include <sstream>

#define PERFECT_GYRO_PROP	PI/14.375/180.0

std::string port = "/dev/ttyUSB1";


int main(int argc, char **argv)
{



	ros::init(argc, argv, "talker");
	ros::NodeHandle n;
	RateTable rt;
	ros::Publisher gyro_pub = n.advertise<std_msgs::String>("gyro", 1000);
	ros::Rate loop_rate(100);



	while (ros::ok())
	  {
	    std_msgs::String msg;

	    std::stringstream ss;
	    ss << gyroX*PERFECT_GYRO_PROP << " " << gyroY*PERFECT_GYRO_PROP << " " << gyroZ*PERFECT_GYRO_PROP << " " << accelX << " " << accelY << " " << accelZ;
	    msg.data = ss.str();

	    ROS_INFO("%s", msg.data.c_str());

	    gyro_pub.publish(msg);

	    ros::spinOnce();

	    loop_rate.sleep();
	  }

	return 0;

}
