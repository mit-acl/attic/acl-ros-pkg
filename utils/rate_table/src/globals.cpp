/*
 * globals.cpp
 *
 *  Created on: Jul 3, 2013
 *      Author: swarm
 */

#include "globals.hpp"

int16_t gyroX=0;
int16_t gyroY=0;
int16_t gyroZ=0;
int16_t accelX=0;
int16_t accelY=0;
int16_t accelZ=0;

int16_t period = -1;
int16_t period2 = -1;
int16_t stop = -1;
int16_t stop2 = -1;
int16_t dir = -1;
int16_t dir2 = -1;

int16_t revs = 0;
int16_t revs2 = 0;
double revrad = -1;
double revrad2 = -1;
