/*
 * globals.h
 *
 *  Created on: Jun 28, 2013
 *      Author: swarm
 */

#include "ratetable.hpp"

#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_

extern int16_t gyroX;
extern int16_t gyroY;
extern int16_t gyroZ;
extern int16_t accelX;
extern int16_t accelY;
extern int16_t accelZ;

extern int16_t period;
extern int16_t period2;
extern int16_t stop;
extern int16_t stop2;
extern int16_t dir;
extern int16_t dir2;

extern std::string port;

extern int16_t revs;
extern int16_t revs2;
extern double revrad;
extern double revrad2;


#define DEFAULT_DIR			0
#define X_AXIS				0
#define Y_AXIS				1
#define Z_AXIS				2


#endif /* GLOBALS_HPP_ */
