/*
 * listener.cpp
 *
 *  Created on: Jun 26, 2013
 *      Author: swarm
 */


#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float32.h"
#include <boost/thread.hpp>
#include <string>
#include <sstream>

double ang_vel_x;
double ang_vel_y;
double ang_vel_z;

double gyro_vel_x;
double gyro_vel_y;
double gyro_vel_z;

int axis;
double vel1;
double vel2;
double sumx = 0;
double sumy = 0;
double sumxx = 0;
double sumxy = 0;

int n = 0;
double prop;
bool end;
void processData();


int argc1;
char **argv1;

#define X_AXIS				0
#define Y_AXIS				1
#define Z_AXIS				2

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void tableCallback(const std_msgs::String::ConstPtr& msg)
{
  //ROS_INFO("table: [%s]", msg->data.c_str());
  std::string s = std::string(msg->data.c_str());
  if (s.compare(std::string("done")))
  {
	  std::istringstream iss(s);
	  iss >> axis;
	  iss >> vel1;
	  iss >> vel2;
	  ang_vel_z = vel1;
	  if (axis == X_AXIS)
	  {
		  ang_vel_x = vel2;
		  ang_vel_y = 0;
		  sumx += gyro_vel_x;
		  sumy += ang_vel_x;
		  sumxx += gyro_vel_x*gyro_vel_x;
		  sumxy += gyro_vel_x*ang_vel_x;
		  	  }
	  else if (axis == Y_AXIS)
	  {
		  ang_vel_x = 0;
		  ang_vel_y = vel2;
		  sumx += gyro_vel_y;
		  sumy += ang_vel_y;
		  sumxx += gyro_vel_y*gyro_vel_y;
		  sumxy += gyro_vel_y*ang_vel_y;
	  }
	  else
	  {
		  ang_vel_x = 0;
		  ang_vel_y = 0;
		  sumx += gyro_vel_z;
		  sumy += ang_vel_z;
		  sumxx += gyro_vel_z*gyro_vel_z;
		  sumxy += gyro_vel_z*ang_vel_z;

	  }
	  n++;




  }
  else
  {
//	  end = true;
//	  ROS_INFO("here");
//	  ROS_INFO("%d", end);
	  processData();
  }

}

void gyroCallback(const std_msgs::String::ConstPtr& msg)
{
	//ROS_INFO("gyro: [%s]", msg->data.c_str());
	std::istringstream iss(msg->data.c_str());
	iss >> gyro_vel_x;
	iss >> gyro_vel_y;
	iss >> gyro_vel_z;

}

void listen()
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line. For programmatic
   * remappings you can use a different version of init() which takes remappings
   * directly, but for most command-line programs, passing argc and argv is the easiest
   * way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc1, argv1, "tableListener");
  ros::NodeHandle m;
  ros::init(argc1, argv1, "gyroListener");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ros::Subscriber tsub = n.subscribe("table", 1000, tableCallback);
  ros::Subscriber gsub = n.subscribe("gyro", 1000, gyroCallback);

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

}

void processData()
{
	prop = (sumx*sumy - n*sumxy)/(sumx*sumx - n*sumxx);
	ROS_INFO("%f", prop);
}


int main(int argc, char **argv)
{
	argc1 = argc;
	argv1 = argv;
	end = false;

	boost::thread t(&listen);
	ros::init(argc, argv, "plotter");
	ros::NodeHandle p;
	ros::Publisher gyro_pub_x = p.advertise<std_msgs::Float32>("plot/gyro/x", 1000);
	ros::Publisher table_pub_x = p.advertise<std_msgs::Float32>("plot/table/x", 1000);
	ros::Publisher gyro_pub_y = p.advertise<std_msgs::Float32>("plot/gyro/y", 1000);
	ros::Publisher table_pub_y = p.advertise<std_msgs::Float32>("plot/table/y", 1000);
	ros::Publisher gyro_pub_z = p.advertise<std_msgs::Float32>("plot/gyro/z", 1000);
	ros::Publisher table_pub_z = p.advertise<std_msgs::Float32>("plot/table/z", 1000);
	ros::Rate loop_rate(100);
	while(ros::ok()){
		std_msgs::Float32 gyromsg;
		std_msgs::Float32 tablemsg;
		gyromsg.data = gyro_vel_x;
		tablemsg.data = ang_vel_x;
		gyro_pub_x.publish(gyromsg);
		table_pub_x.publish(tablemsg);
		gyromsg.data = gyro_vel_y;
		tablemsg.data = ang_vel_y;
		gyro_pub_y.publish(gyromsg);
		table_pub_y.publish(tablemsg);
		gyromsg.data = gyro_vel_z;
		tablemsg.data = ang_vel_z;
		gyro_pub_z.publish(gyromsg);
		table_pub_z.publish(tablemsg);
		ros::spinOnce();
		loop_rate.sleep();

	}

	return 0;
}

