/*
 * quadrotor.cpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#include "ratetable.hpp"
#include "globals.hpp"
#include <string>

// Initialize RateTable Vehicle
RateTable::RateTable() {

	//turn_on = false;
	T = 2000;
	deg = 0;
	m = 0;
	direction = 0;
	mot = 0;

	serialPort = port;
	baudRate = 57600;

	 //## Start serial
	if (!ser.spInitialize(serialPort.c_str(), baudRate, true))
	ROS_ERROR("Serial port failed to open");

	//## Start a serial listen thread
	pthread_t threads;
	if (pthread_create(&threads, NULL, serListen, (void *) this))
	ROS_ERROR("Serial listen thread failed to start");

}

