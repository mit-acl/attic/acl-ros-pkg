/*
 * quadrotor.hpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef QUADROTOR_HPP_
#define QUADROTOR_HPP_

#define PI				3.14159
#define TIMER3_SCALE	512 //must be same as TIMER3_SCALE in defines.h of uberpilot

// ROS includesperiod
#include "ros/ros.h"

// ACL shared library
#include "acl/serialPort.hpp"
#include "acl/utils.hpp"

// Global includes
#include <iostream>

// Local includes
#include "comm.hpp"

//## Serial listen thread
void *serListen(void *param);

class RateTable
{
public:
  RateTable();

  //## Communication with RateTable
  acl::SerialPort ser;
  void parsePacket(uint8_t ID, uint8_t length, uint8_t * data);

  //## Communication with RateTable
  void SendPacket(uint8_t ID, uint8_t length, uint8_t * data);

  //## Communication with RateTable
  void sendCmd(void);

  //bool turn_on;
  int32_t T;
  float deg;
  int m;
  int8_t mot;
  int8_t direction;

private:

  std::string serialPort;
  int baudRate;

  //## Comm structs
  tRateTableCmdPacket rateTableCmdPacket;

};

#endif /* QUADROTOR_HPP_ */
