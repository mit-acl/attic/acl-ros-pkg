/*
 * utils.hpp
 *
 *  Created on: May 7, 2015
 *      Author: mark
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <ros/ros.h>
#include <acl/comms/wifly.hpp>

/// raven namespace for all library functions and classes
namespace raven {

// utils for getting parameters from the param server
double param_double(std::string n, bool cached=false);
bool param_bool(std::string n, bool cached=false);
int param_int(std::string n, bool cached=false);
std::vector<int> param_vint(std::string n, bool cached=false);
std::string param_string(std::string n, bool cached=false);
bool set_wifly_host_ip_telnet(std::string host, int port);
};



#endif /* UTILS_HPP_ */
