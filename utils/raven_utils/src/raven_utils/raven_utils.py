#!/usr/bin/env python
'''
Description: Simple utilities that rely on ROS.  Non-ROS utilities will reside
in the acl_utils library.

Created on Dec 12, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('raven_utils')
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayDimension
from operator import mul

#-----------------------------------------------------------------------------
# multiarray to numpy array
#-----------------------------------------------------------------------------
def multiArray2NumpyArray(ma):
    numDim = len(ma.layout.dim)
    s = np.zeros(numDim)
    for i in np.arange(numDim):
        s[i] = ma.layout.dim[i].size
    na = np.reshape(ma.data, tuple(s))
    return na

#-----------------------------------------------------------------------------
# numpy array to multiarray
# TODO: not sure if the stride calculations are correct, although they aren't used
# in the multiArray2NumpyArray function so it might not matter
#-----------------------------------------------------------------------------
def numpyArray2MultiArray(na, ma):
    s = na.shape
    for i in np.arange(len(s)):
        d = MultiArrayDimension()
        d.size = s[i]
        d.stride = reduce(mul, s[i:len(s)])  # = s[0]*s[1]*s[2]*...
        ma.layout.dim.append(d)
    ma.data = np.reshape(na, -1)
