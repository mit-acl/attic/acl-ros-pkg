/*
 * setup_wifly.cpp
 *
 *  Created on: May 7, 2015
 *      Author: mark
 */

#include <acl/comms/wifly.hpp>

#include <raven_utils/utils.hpp>

int main(int argc, char* argv[])
{
	// initialize node
	ros::init(argc, argv, "setup_wifly");

	// load parameters from the yaml file
	int wifly_num = raven::param_int("wifly_num");
	std::string serialPort = raven::param_string("serialPort");
	int initial_baud_rate = raven::param_int("initial_baud_rate");
	std::string ssid = raven::param_string("ssid");
	std::string phrase = raven::param_string("phrase");
	int target_baud_rate = raven::param_int("target_baud_rate");
	int remote_port = raven::param_int("remote_port");
	int local_port = raven::param_int("local_port");
	std::string remote_address = raven::param_string("remote_address");
	std::vector<int> expected_address = raven::param_vint("raven_expected_address");

	// configure the parameters based on the wifly number
	remote_port += wifly_num;
	local_port += wifly_num;

	std::stringstream s(remote_address);
	int n1, n2, n3, n4; //to store the 4 ints
	char ch; //to temporarily store the '.'
	s >> n1 >> ch >> n2 >> ch >> n3 >> ch >> n4;
	n4 += wifly_num;
	s.clear();
	s << n1 << "." << n2 << "." << n3 << "." << n4;
	remote_address = s.str(); // update the remote address with the proper last number

	acl::wifly wf;

	// get the local ip address, or use the user-supplied one
	std::string local_address;
	if (ros::param::has("local_address"))
		local_address = raven::param_string("local_address");
	else
		wf.get_acl_ip(expected_address, local_address);


	// Flash the wifly device using the serial port
	wf.setup_wifly(ssid, remote_port, local_port, local_address,
			remote_address,	phrase, serialPort, initial_baud_rate,
			target_baud_rate);


	return 0;
}
