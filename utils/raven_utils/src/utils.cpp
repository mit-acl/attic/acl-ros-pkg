/*
 * utils.cpp
 *
 *  Created on: May 7, 2015
 *      Author: mark
 */

#include "raven_utils/utils.hpp"

namespace raven {

double param_double(std::string n, bool cached)
{
	double out = 0.0;
	bool result;
	if (cached)
		result = ros::param::getCached(n, out);
	else
		result = ros::param::get(n, out);

	if (result)
		if (cached)
			ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
		else
			ROS_INFO_STREAM("Got param: '" << n << "', " << out);
	else
		ROS_ERROR_STREAM("Failed to get param: '" << n << "'");

	return out;
}

int param_int(std::string n, bool cached)
{
	int out = 0;
	bool result;
	if (cached)
		result = ros::param::getCached(n, out);
	else
		result = ros::param::get(n, out);

	if (result)
		if (cached)
			ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
		else
			ROS_INFO_STREAM("Got param: '" << n << "', " << out);
	else
		ROS_ERROR_STREAM("Failed to get param: '" << n << "'");

	return out;
}

std::vector<int> param_vint(std::string n, bool cached)
{
	std::vector<int> out;
	bool result;
	if (cached)
		result = ros::param::getCached(n, out);
	else
		result = ros::param::get(n, out);

	if (result)
		if (cached)
			ROS_DEBUG_STREAM("Got param: '" << n << "' of length " << out.size());
		else
			ROS_INFO_STREAM("Got param: '" << n << "' of length " << out.size());
	else
		ROS_ERROR_STREAM("Failed to get param: '" << n << "'");

	return out;
}

bool param_bool(std::string n, bool cached)
{
	bool out;
	bool result;
	if (cached)
		result = ros::param::getCached(n, out);
	else
		result = ros::param::get(n, out);

	if (result)
		if (cached)
			ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
		else
			ROS_INFO_STREAM("Got param: '" << n << "', " << out);
	else
		ROS_ERROR_STREAM("Failed to get param: '" << n << "'");

	return out;
}

std::string param_string(std::string n, bool cached)
{
	std::string out;
	bool result;
	if (cached)
		result = ros::param::getCached(n, out);
	else
		result = ros::param::get(n, out);

	if (result)
		if (cached)
			ROS_DEBUG_STREAM("Got param: '" << n << "', " << out);
		else
			ROS_INFO_STREAM("Got param: '" << n << "', " << out);
	else
		ROS_ERROR_STREAM("Failed to get param: '" << n << "'");

	return out;
}

bool set_wifly_host_ip_telnet(std::string host, int port)
{
	std::vector<int> expected_address = raven::param_vint("/raven_expected_address");

	acl::wifly wf;

	wf.set_host_ip(host, port, expected_address);

	return true;
}

}

