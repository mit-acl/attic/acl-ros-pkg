# Copyright (c) 2011, Dirk Thomas, TU Darmstadt
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#   * Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#   * Redistributions in binary form must reproduce the above
#     copyright notice, this list of conditions and the following
#     disclaimer in the documentation and/or other materials provided
#     with the distribution.
#   * Neither the name of the TU Darmstadt nor the names of its
#     contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from __future__ import division
import os
import rospkg

from geometry_msgs.msg import Vector3
import rospy
from python_qt_binding import loadUi
from python_qt_binding.QtCore import Qt, QTimer, Slot
from python_qt_binding.QtGui import QKeySequence, QShortcut, QWidget
from rqt_gui_py.plugin import Plugin


class PendulumCmd(Plugin):

    slider_factor = 1000.0

    def __init__(self, context):
        super(PendulumCmd, self).__init__(context)
        self.setObjectName('PendulumCmd')

        self._param_publisher = None
        self._ref_publisher = None

        self._widget = QWidget()
        rp = rospkg.RosPack()
        ui_file = os.path.join(rp.get_path('rqt_pendulum_cmd'), 'resource', 'PendulumCmd.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('PendulumCmdUi')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        context.add_widget(self._widget)

        self._widget.topic_line_edit.textChanged.connect(self._on_topic_changed)
        self._widget.stop_push_button.pressed.connect(self._on_stop_pressed)
        self._widget.start_push_button.pressed.connect(self._on_start_pressed)
        self._widget.save_data_push_button.pressed.connect(self._on_save_data_pressed)
        self._widget.reset_integrator_push_button.pressed.connect(self._on_reset_pressed)

        self._widget.pitch_slider.valueChanged.connect(self._on_pitch_slider_changed)
        self._widget.kp_slider.valueChanged.connect(self._on_kp_slider_changed)
        self._widget.ki_slider.valueChanged.connect(self._on_ki_slider_changed)
        self._widget.kd_slider.valueChanged.connect(self._on_kd_slider_changed)

        self._widget.increase_pitch_push_button.pressed.connect(self._on_strong_increase_pitch_pressed)
        self._widget.reset_pitch_push_button.pressed.connect(self._on_reset_pitch_pressed)
        self._widget.decrease_pitch_push_button.pressed.connect(self._on_strong_decrease_pitch_pressed)
        self._widget.increase_kp_push_button.pressed.connect(self._on_strong_increase_kp_pressed)
        self._widget.reset_kp_push_button.pressed.connect(self._on_reset_kp_pressed)
        self._widget.decrease_kp_push_button.pressed.connect(self._on_strong_decrease_kp_pressed)

        self._widget.increase_ki_push_button.pressed.connect(self._on_strong_increase_ki_pressed)
        self._widget.reset_ki_push_button.pressed.connect(self._on_reset_ki_pressed)
        self._widget.decrease_ki_push_button.pressed.connect(self._on_strong_decrease_ki_pressed)

        self._widget.increase_kd_push_button.pressed.connect(self._on_strong_increase_kd_pressed)
        self._widget.reset_kd_push_button.pressed.connect(self._on_reset_kd_pressed)
        self._widget.decrease_kd_push_button.pressed.connect(self._on_strong_decrease_kd_pressed)

        self._widget.max_pitch_double_spin_box.valueChanged.connect(self._on_max_pitch_changed)
        self._widget.min_pitch_double_spin_box.valueChanged.connect(self._on_min_pitch_changed)
        self._widget.throttle_double_spin_box.valueChanged.connect(self._on_throttle_changed)

        # timer to consecutively send twist messages
        self.currently_flying = False

    @Slot(str)
    def _on_topic_changed(self, topic):
        topic = str(topic)
        self._unregister_publisher()
        self._param_publisher = rospy.Publisher(topic+'/param', Vector3)
        self._ref_publisher = rospy.Publisher(topic+'/ref', Vector3)

    def _on_stop_pressed(self):
        # TODO: send zero propeller throttle command here
        self.currently_flying = False
        self._send_ref(0, 0)

    def _on_start_pressed(self):
        self.currently_flying = True
        self._on_parameter_changed()
        self._on_ref_changed()

    def _on_save_data_pressed(self):
        if self._widget.save_data_push_button.isChecked():
            rospy.set_param('save_data', False)
        else:
            rospy.set_param('save_data', True)

    def _on_reset_pressed(self):
        if self.currently_flying is True:
            # send zero ki gain which will reset integrator state on board
            for i in range(3):
                self._send_param(self._widget.kp_slider.value() / PendulumCmd.slider_factor,
                                 0.0,
                                 self._widget.kd_slider.value() / PendulumCmd.slider_factor)
                rospy.sleep(0.1)
            # send true gains
            for i in range(2):
                self._send_param(self._widget.kp_slider.value() / PendulumCmd.slider_factor,
                                 self._widget.ki_slider.value() / PendulumCmd.slider_factor,
                                 self._widget.kd_slider.value() / PendulumCmd.slider_factor)
                rospy.sleep(0.1)


    def _on_pitch_slider_changed(self):
        self._widget.current_pitch_label.setText('Pitch Command:\n%0.2f degrees' % (self._widget.pitch_slider.value() / PendulumCmd.slider_factor))
        self._on_ref_changed()

    def _on_throttle_changed(self):
        self._on_ref_changed()

    def _on_kp_slider_changed(self):
        self._widget.current_kp_label.setText('Kp: %0.2f' % (self._widget.kp_slider.value() / PendulumCmd.slider_factor))
        self._on_parameter_changed()

    def _on_ki_slider_changed(self):
        self._widget.current_ki_label.setText('Ki: %0.2f' % (self._widget.ki_slider.value() / PendulumCmd.slider_factor))
        self._on_parameter_changed()

    def _on_kd_slider_changed(self):
        self._widget.current_kd_label.setText('Kd: %0.2f' % (self._widget.kd_slider.value() / PendulumCmd.slider_factor))
        self._on_parameter_changed()


    def _on_min_pitch_changed(self, value):
        self._widget.pitch_slider.setMinimum(value * self.slider_factor)

    def _on_max_pitch_changed(self, value):
        self._widget.pitch_slider.setMaximum(value * self.slider_factor)

    def _on_increase_pitch_pressed(self):
        self._widget.pitch_slider.setValue(self._widget.pitch_slider.value() + self._widget.pitch_slider.singleStep())

    def _on_reset_pitch_pressed(self):
        self._widget.pitch_slider.setValue(0)

    def _on_decrease_pitch_pressed(self):
        self._widget.pitch_slider.setValue(self._widget.pitch_slider.value() - self._widget.pitch_slider.singleStep())

    def _on_increase_kp_pressed(self):
        self._widget.kp_slider.setValue(self._widget.kp_slider.value() + self._widget.kp_slider.singleStep())

    def _on_reset_kp_pressed(self):
        self._widget.kp_slider.setValue(0)

    def _on_decrease_kp_pressed(self):
        self._widget.kp_slider.setValue(self._widget.kp_slider.value() - self._widget.kp_slider.singleStep())


    def _on_increase_ki_pressed(self):
        self._widget.ki_slider.setValue(self._widget.ki_slider.value() + self._widget.ki_slider.singleStep())

    def _on_reset_ki_pressed(self):
        self._widget.ki_slider.setValue(0)

    def _on_decrease_ki_pressed(self):
        self._widget.ki_slider.setValue(self._widget.ki_slider.value() - self._widget.ki_slider.singleStep())


    def _on_increase_kd_pressed(self):
        self._widget.kd_slider.setValue(self._widget.kd_slider.value() + self._widget.kd_slider.singleStep())

    def _on_reset_kd_pressed(self):
        self._widget.kd_slider.setValue(0)

    def _on_decrease_kd_pressed(self):
        self._widget.kd_slider.setValue(self._widget.kd_slider.value() - self._widget.kd_slider.singleStep())



    def _on_strong_increase_pitch_pressed(self):
        self._widget.pitch_slider.setValue(self._widget.pitch_slider.value() + self._widget.pitch_slider.pageStep())

    def _on_strong_decrease_pitch_pressed(self):
        self._widget.pitch_slider.setValue(self._widget.pitch_slider.value() - self._widget.pitch_slider.pageStep())

    def _on_strong_increase_kp_pressed(self):
        self._widget.kp_slider.setValue(self._widget.kp_slider.value() + self._widget.kp_slider.pageStep())

    def _on_strong_decrease_kp_pressed(self):
        self._widget.kp_slider.setValue(self._widget.kp_slider.value() - self._widget.kp_slider.pageStep())



    def _on_strong_increase_ki_pressed(self):
        self._widget.ki_slider.setValue(self._widget.ki_slider.value() + self._widget.ki_slider.pageStep())

    def _on_strong_decrease_ki_pressed(self):
        self._widget.ki_slider.setValue(self._widget.ki_slider.value() - self._widget.ki_slider.pageStep())


    def _on_strong_increase_kd_pressed(self):
        self._widget.kd_slider.setValue(self._widget.kd_slider.value() + self._widget.kd_slider.pageStep())

    def _on_strong_decrease_kd_pressed(self):
        self._widget.kd_slider.setValue(self._widget.kd_slider.value() - self._widget.kd_slider.pageStep())



    def _on_parameter_changed(self):
        self._send_param(self._widget.kp_slider.value() / PendulumCmd.slider_factor,
                         self._widget.ki_slider.value() / PendulumCmd.slider_factor,
                         self._widget.kd_slider.value() / PendulumCmd.slider_factor)

    def _on_ref_changed(self):
        self._send_ref(self._widget.pitch_slider.value() / PendulumCmd.slider_factor,
                         self._widget.throttle_double_spin_box.value())

    def _send_param(self, kp,ki,kd):
        if self._param_publisher is None:
            return
        p = Vector3()
        p.x = kp
        p.y = ki
        p.z = kd

        self._param_publisher.publish(p)

    def _send_ref(self, pitch, throttle):
        if self._ref_publisher is None:
            return
        p = Vector3()
        p.x = pitch
        p.y = throttle
        p.z = self.currently_flying

        self._ref_publisher.publish(p)


    def _unregister_publisher(self):
        if self._ref_publisher is not None:
            self._ref_publisher.unregister()
            self._ref_publisher = None

        if self._param_publisher is not None:
            self._param_publisher.unregister()
            self._param_publisher = None


    def shutdown_plugin(self):
        self._unregister_publisher()

    def save_settings(self, plugin_settings, instance_settings):
        instance_settings.set_value('topic' , self._widget.topic_line_edit.text())
        instance_settings.set_value('Kp', self._widget.kp_slider.value())
        instance_settings.set_value('Ki', self._widget.ki_slider.value())
        instance_settings.set_value('Kd', self._widget.kd_slider.value())
        instance_settings.set_value('throttle', self._widget.throttle_double_spin_box.value())
        instance_settings.set_value('pitch_max', self._widget.max_pitch_double_spin_box.value())
        instance_settings.set_value('pitch_min', self._widget.min_pitch_double_spin_box.value())

    def restore_settings(self, plugin_settings, instance_settings):

        value = instance_settings.value('topic', "mQ01")
        value = rospy.get_param("~default_topic", value)
        self._widget.topic_line_edit.setText(value)

        value = self._widget.kp_slider.value()
        value = instance_settings.value( 'Kp', value)
        value = rospy.get_param("~default_Kp", value)
        self._widget.kp_slider.setValue(float(value))

        value = self._widget.ki_slider.value()
        value = instance_settings.value( 'Ki', value)
        value = rospy.get_param("~default_Ki", value)
        self._widget.ki_slider.setValue(float(value))

        value = self._widget.kd_slider.value()
        value = instance_settings.value( 'Kd', value)
        value = rospy.get_param("~default_Kd", value)
        self._widget.kd_slider.setValue(float(value))

        value = self._widget.throttle_double_spin_box.value()
        value = instance_settings.value( 'throttle', value)
        value = rospy.get_param("~default_throttle", value)
        self._widget.throttle_double_spin_box.setValue(float(value))


        value = self._widget.max_pitch_double_spin_box.value()
        value = instance_settings.value( 'pitch_max', value)
        value = rospy.get_param("~default_pitch_max", value)
        self._widget.max_pitch_double_spin_box.setValue(float(value))

        value = self._widget.min_pitch_double_spin_box.value()
        value = instance_settings.value( 'pitch_min', value)
        value = rospy.get_param("~default_pitch_min", value)
        self._widget.min_pitch_double_spin_box.setValue(float(value))
