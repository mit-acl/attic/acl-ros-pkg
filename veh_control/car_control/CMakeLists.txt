cmake_minimum_required(VERSION 2.8.3)
project(car_control)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
	roscpp
	rospy
	std_msgs
	geometry_msgs
	actionlib_msgs
	rosbag
	roslib
	acl_msgs
	raven_utils
	)


###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES quad_control
	CATKIN_DEPENDS std_msgs geometry_msgs
#  DEPENDS system_lib
)

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  ${catkin_INCLUDE_DIRS}
  include
  ${raven_utils_INCLUDE_DIRS}
)

add_executable(car src/main.cpp src/car.cpp src/utils.cpp 
    src/getParameters.cpp src/comm.cpp)
target_link_libraries(car ${catkin_LIBRARIES} acl_utils raven_utils)
add_dependencies(car ${catkin_EXPORTED_TARGETS})

# include PCL definitions and library locations
find_package(PCL 1.7 REQUIRED)
include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

add_executable(sts src/sts_main.cpp src/state_track_server.cpp)
target_link_libraries(sts  ${catkin_LIBRARIES} ${PCL_LIBRARIES} acl_utils raven_utils)
add_dependencies(sts ${catkin_EXPORTED_TARGETS})
