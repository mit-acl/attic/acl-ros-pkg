function sim = CarSim()

% Set default initial parameters
% RC02 -- rubber tires
% param.B = 10.31;	% From initial optimization
% param.C = 1.07;     % From initial optimization
% param.D = 0.340;	% From initial optimization

% RC03 -- drift tires
param.B = 3;	% From initial optimization
param.C = 1.5;     % From initial optimization
param.D = 0.2;	% From initial optimization

param.rF = 0.0606 / 2.0; % measured
param.rR = param.rF;
param.lF = 0.1;	% measured
param.lR = 0.107;	% measured
param.MaxWheelAngle = 18*pi/180.0;
param.MaxWheelSpeed = 352;          % roughly measured
param.BackEMFGain_up = 20.0; % estimated using step data from none-loaded wheels
param.TorqueOffset = 0.115; % estimated using step data from none-loaded wheels
param.TorqueMin = 0.12; % estimated using step data from none-loaded wheels
param.BackEMFGain_down = 1.6; % estimated using step data from none-loaded wheels
param.m = 1.143; % measured
param.Iz = 1.0 / 12.0 * param.m ...
           * (0.1 * 0.1 + 0.2 * 0.2); % estimated by bar of width 10cm,
                                      % length 20cm, mass 1143 g
param.IwF = 0.8*3.2e-5; % approximate %wheel weight = 0.044 kg
param.IwR = param.IwF;
param.h = 0.02794; % roughly measured


param.dt = 0.01;

% Set default initial state
state.x = 0.0; state.y = 0.0; state.Vx = 0.0; state.Vy = 0.0;
state.dx = 0.0; state.dy = 0.0;
state.psi = 0.0; state.dpsi = 0.0; state.omegaF = 0.0; state.omegaR = 0.0;

% Set default initial action
action.throttle = 0.0;
action.turn = 0.0;

sim.param = param;
sim.state = state;
sim.action = action;
sim.simulate = @simulate;
sim.simulate_global = @simulate_global;
sim.dynamics = @dynamics;
sim.dynamics_global = @dynamics_global;

% Simulate forward, updating sim.state, for time t
function sim = simulate( sim, t )

% Simulate using param.dt steps
for i=1:floor(t/sim.param.dt)
    sim = dynamicsStep( sim, sim.param.dt, 0);
end

% Simulate the leftover time
sim = dynamicsStep( sim,  t - sim.param.dt*floor(t/sim.param.dt), 0 );


% Simulate forward, updating sim.state, for time t
function sim = simulate_global( sim, t )

% Simulate using param.dt steps
for i=1:floor(t/sim.param.dt)
    sim = dynamicsStep( sim, sim.param.dt, 1);
end

% Simulate the leftover time
sim = dynamicsStep( sim,  t - sim.param.dt*floor(t/sim.param.dt), 1);


% Get the derivatives of the car dynamics
function xdot = dynamics(sim)

% Set steer angle (delta)
delta = -sim.action.turn*sim.param.MaxWheelAngle;
throttle = sim.action.throttle;

% Calculate component velocities
beta = atan2(sim.state.Vy,sim.state.Vx);
V = sqrt(sim.state.Vx.^2+sim.state.Vy.^2);

V_Fx = V.*cos(beta - delta) + sim.state.dpsi.*sim.param.lF.*sin(delta);
V_Fy = V.*sin(beta - delta) + sim.state.dpsi.*sim.param.lF.*cos(delta);
V_Rx = V.*cos(beta);
V_Ry = V.*sin(beta) - sim.state.dpsi*sim.param.lR;

% Calculate friction coefficients
s_Fx = xSlip(V_Fx,sim.state.omegaF,sim.param.rF);
s_Fy = ySlip(V_Fy,sim.state.omegaF,sim.param.rF);
s_F = sqrt(s_Fx.^2+s_Fy.^2);

s_Rx = xSlip(V_Rx,sim.state.omegaR,sim.param.rR);
s_Ry = ySlip(V_Ry,sim.state.omegaR,sim.param.rR);
s_R = sqrt(s_Rx.^2+s_Ry.^2);

mu_Fx = -s_Fx./s_F.*MF(sim.param.B, sim.param.C, sim.param.D, s_F);
mu_Fy = -s_Fy./s_F.*MF(sim.param.B, sim.param.C, sim.param.D, s_F);
mu_Fx(isinf(mu_Fx))=0;
mu_Fx(isnan(mu_Fx))=0;
mu_Fy(isinf(mu_Fy))=0;
mu_Fy(isnan(mu_Fy))=0;

mu_Rx = -s_Rx./s_R.*MF(sim.param.B, sim.param.C, sim.param.D, s_R);
mu_Ry = -s_Ry./s_R.*MF(sim.param.B, sim.param.C, sim.param.D, s_R);
mu_Rx(isinf(mu_Rx))=0;
mu_Rx(isnan(mu_Rx))=0;
mu_Ry(isinf(mu_Ry))=0;
mu_Ry(isnan(mu_Ry))=0;


% Calculate friction forces
GRAVITY = 9.81;
num = sim.param.lR*sim.param.m*GRAVITY - sim.param.h*sim.param.m*GRAVITY*mu_Rx;
den = sim.param.h*(mu_Fx.*cos(delta) - mu_Fy.*sin(delta) - mu_Rx);
f_Fz = num./(sim.param.lF + sim.param.lR + den);
f_Rz = sim.param.m*GRAVITY - f_Fz;
f_Fx = mu_Fx.*f_Fz;
f_Fy = mu_Fy.*f_Fz;
f_Rx = mu_Rx.*f_Rz;
f_Ry = mu_Ry.*f_Rz;

% System dynamics
xdot.x = sim.state.Vx.*cos(sim.state.psi) - sim.state.Vy.*sin(sim.state.psi); % inertial x
xdot.y = sim.state.Vx.*sin(sim.state.psi) + sim.state.Vy.*cos(sim.state.psi); % inertial y
xdot.Vx = 1/sim.param.m*(sim.param.m*sim.state.Vy.*sim.state.dpsi + f_Fx.*cos(delta) - f_Fy.*sin(delta) + f_Rx); % body x velocity
xdot.Vy = 1/sim.param.m*(-sim.param.m*sim.state.Vx.*sim.state.dpsi + f_Fx.*sin(delta) + f_Fy.*cos(delta) + f_Ry); % body y velocity
xdot.psi = sim.state.dpsi; % yaw
xdot.dpsi = 1/sim.param.Iz*((f_Fy.*cos(delta) + f_Fx.*sin(delta))*sim.param.lF- f_Ry.*sim.param.lR); % yaw rate
xdot.wF = wheelspeed(throttle, sim.state.omegaF, (f_Fx+f_Rx)/2);
xdot.wR = xdot.wF; %wheelspeed(throttle, wR, f_Rx);


% Get the derivatives of the car dynamics
function xdot = dynamics_global(sim)

% Set steer angle (delta)
delta = -sim.action.turn*sim.param.MaxWheelAngle;
throttle = sim.action.throttle;

% Calculate component velocities
beta = atan2(sim.state.dy,sim.state.dx) - sim.state.psi;
V = sqrt(sim.state.dx.^2+sim.state.dy.^2);

V_Fx = V.*cos(beta - delta) + sim.state.dpsi.*sim.param.lF.*sin(delta);
V_Fy = V.*sin(beta - delta) + sim.state.dpsi.*sim.param.lF.*cos(delta);
V_Rx = V.*cos(beta);
V_Ry = V.*sin(beta) - sim.state.dpsi*sim.param.lR;

% Calculate friction coefficients
s_Fx = xSlip(V_Fx,sim.state.omegaF,sim.param.rF);
s_Fy = ySlip(V_Fy,sim.state.omegaF,sim.param.rF);
s_F = sqrt(s_Fx.^2+s_Fy.^2);

s_Rx = xSlip(V_Rx,sim.state.omegaR,sim.param.rR);
s_Ry = ySlip(V_Ry,sim.state.omegaR,sim.param.rR);
s_R = sqrt(s_Rx.^2+s_Ry.^2);

mu_Fx = -s_Fx./s_F.*MF(sim.param.B, sim.param.C, sim.param.D, s_F);
mu_Fy = -s_Fy./s_F.*MF(sim.param.B, sim.param.C, sim.param.D, s_F);
mu_Fx(isinf(mu_Fx))=0;
mu_Fx(isnan(mu_Fx))=0;
mu_Fy(isinf(mu_Fy))=0;
mu_Fy(isnan(mu_Fy))=0;

mu_Rx = -s_Rx./s_R.*MF(sim.param.B, sim.param.C, sim.param.D, s_R);
mu_Ry = -s_Ry./s_R.*MF(sim.param.B, sim.param.C, sim.param.D, s_R);
mu_Rx(isinf(mu_Rx))=0;
mu_Rx(isnan(mu_Rx))=0;
mu_Ry(isinf(mu_Ry))=0;
mu_Ry(isnan(mu_Ry))=0;


% Calculate friction forces
GRAVITY = 9.81;
num = sim.param.lR*sim.param.m*GRAVITY - sim.param.h*sim.param.m*GRAVITY*mu_Rx;
den = sim.param.h*(mu_Fx.*cos(delta) - mu_Fy.*sin(delta) - mu_Rx);
f_Fz = num./(sim.param.lF + sim.param.lR + den);
f_Rz = sim.param.m*GRAVITY - f_Fz;
f_Fx = mu_Fx.*f_Fz;
f_Fy = mu_Fy.*f_Fz;
f_Rx = mu_Rx.*f_Rz;
f_Ry = mu_Ry.*f_Rz;

% System dynamics
psi = sim.state.psi;
xdot.x = sim.state.Vx.*cos(psi) - sim.state.Vy.*sin(psi); % inertial x
xdot.y = sim.state.Vx.*sin(psi) + sim.state.Vy.*cos(psi); % inertial y
xdot.dx = 1/sim.param.m*(f_Fx.*cos(psi + delta) - f_Fy.*sin(psi + delta) + f_Rx.*cos(psi) - f_Ry.*sin(psi)); % body x velocity
xdot.dy = 1/sim.param.m*(f_Fx.*sin(psi + delta) + f_Fy.*cos(psi + delta) + f_Rx.*sin(psi) + f_Ry.*cos(psi)); % body y velocity
xdot.psi = sim.state.dpsi; % yaw
xdot.dpsi = 1/sim.param.Iz*((f_Fy.*cos(delta) + f_Fx.*sin(delta))*sim.param.lF- f_Ry.*sim.param.lR); % yaw rate
xdot.wF = wheelspeed(throttle, sim.state.omegaF, (f_Fx+f_Rx)/2);
xdot.wR = xdot.wF; %wheelspeed(throttle, wR, f_Rx);




% Perform a single Euler integration step of the vehicle dynamics
function sim = dynamicsStep( sim, dt, global_int)

if global_int
    xdot = dynamics_global(sim);
else
    xdot = dynamics(sim);
end

% Integrate
sim.state.x = sim.state.x + dt*xdot.x;
sim.state.y = sim.state.y + dt*xdot.y;
if global_int
    sim.state.dx = sim.state.dx + dt*xdot.dx;
    sim.state.dy = sim.state.dy + dt*xdot.dy;
else
    sim.state.Vx = sim.state.Vx + dt*xdot.Vx;
    sim.state.Vy = sim.state.Vy + dt*xdot.Vy;
end

sim.state.psi = sim.state.psi + dt*xdot.psi;
sim.state.dpsi = sim.state.dpsi + dt*xdot.dpsi;
sim.state.omegaF = sim.state.omegaF + dt*xdot.wF;
sim.state.omegaR = sim.state.omegaR + dt*xdot.wR;


function slip = xSlip( Vx, omega, r)
slip = (Vx - omega*r)./abs(omega*r);
slip(isinf(slip))=0;
slip(isnan(slip))=0;

function slip = ySlip( Vy, omega, r)
slip = Vy./abs(omega*r);
slip(isinf(slip))=0;
slip(isnan(slip))=0;

function ret = MF( B, C, D, s)
ret = D*sin(C*atan(B*s));
