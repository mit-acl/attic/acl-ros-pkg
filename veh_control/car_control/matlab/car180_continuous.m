function output = car180_continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
sim = input.auxdata;    

% % % %Call continuous dynamics function:
% % % % Xd = Car_Dynamics_Forced(X',F',P)';

%Define input states:
sim.state.dx = X(:,1)';
sim.state.dy = X(:,2)';
sim.state.dpsi = X(:,3)';
sim.state.omegaF = X(:,4)';
sim.state.omegaR = sim.state.omegaF;
sim.state.psi = X(:,5)';

sim.action.turn = F(:,1)';
sim.action.throttle = F(:,2)';
% sim.action.throttle = sim.ss.T*ones(size(sim.action.turn));

%Call continuous dynamics function:

% Xd = sim.dynamics(sim);
Xd = sim.dynamics_global(sim);

%Get the cost integrand:   (force-squared for now)
adx = 1;
ady = 1;
omega_c = 0.5;
costIntergrand = adx*(sim.state.dx-sim.xf.dx).^2/(2*omega_c^2) + ...
    ady*(sim.state.dy-sim.xf.dy).^2/(2*omega_c^2);
    
% costIntergrand = 1-exp(-costIntergrand);

%Pack up the output:
% output.dynamics = [Xd.Vx; Xd.Vy; Xd.dpsi; Xd.wF; Xd.psi]';
output.dynamics = [Xd.dx; Xd.dy; Xd.dpsi; Xd.wR; Xd.psi]';
output.integrand = costIntergrand';

end