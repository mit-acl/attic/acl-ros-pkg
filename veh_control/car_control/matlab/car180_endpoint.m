function output = car180_endpoint(input)

%The cost function is the integral of force.^2
output.objective = input.phase(1).integral;
% output.objective = input.phase(1).finaltime;

end