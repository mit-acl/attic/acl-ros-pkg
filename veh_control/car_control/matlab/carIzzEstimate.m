clc; clear all;

% measure mass moment of inertia of rc car
% following method here: http://www.youtube.com/watch?v=m9iHEanmNWc

% base
m1 = 0.1576;
a1 = 0.2032;
b1 = 0.254;

% wood test block
m2 = 0.11376;
a2 = 0.036;
b2 = 0.17;

% car
m3 = 0.20516;

% analytic estimates of base and wood block
I1 = 1/12*m1*(a1^2 + b1^2)
I2 = I1 + 1/12*m2*(a2^2 + b2^2)

% measured time periods of bifular pendulum
T1 = 2.12;
T2 = 1.74;
T3 = 1.56;

b = 0.245/2;
L = 1.7272;
g = 9.81;
I1est = m1*g*T1^2*b^2/(4*pi^2*L)
I2est = (m2+m1)*g*T2^2*b^2/(4*pi^2*L)
I3est = (m3+m1)*g*T3^2*b^2/(4*pi^2*L)

IzzCar = I3est - I1est % = 3.7672e-04