function output = car_continuous(input)

%Dynamical state
X = input.phase(1).state;  

%Control force
F = input.phase(1).control;

%Define Parameters
sim = input.auxdata;    

% % % %Call continuous dynamics function:
% % % % Xd = Car_Dynamics_Forced(X',F',P)';

%Define input states:
sim.state.Vx = X(:,1)';
sim.state.Vy = X(:,2)';
sim.state.dpsi = X(:,3)';
sim.state.omegaF = X(:,4)';
sim.state.omegaR = sim.state.omegaF;

sim.action.turn = F(:,1)';
% sim.action.throttle = F(:,2)';
sim.action.throttle = sim.ss.T*ones(size(sim.action.turn));

%Call continuous dynamics function:

Xd = sim.dynamics(sim);

%Get the cost integrand:   (force-squared for now)
% costIntegrand = F(:,2).^2; %ones(size(F)); %
% costIntegrand = X(:,1).^2; % anguluar position
% costIntegrand = X(:,1).^2 + X(:,2).^2; % anguluar position + rate
aVx = 1;
aVy = 1;
adpsi = 1;
omega_c = 0.5;
costIntergrand = aVx*(sim.state.Vx-sim.ss.Vx).^2/(2*omega_c^2) + aVy*(sim.state.Vy-sim.ss.Vy).^2/(2*omega_c^2) + ...
    adpsi*(sim.state.dpsi-sim.ss.dpsi).^2/(2*omega_c^2);
    
% costIntergrand = 1-exp(-costIntergrand);

%Pack up the output:
output.dynamics = [Xd.Vx; Xd.Vy; Xd.dpsi; Xd.wF]';
output.integrand = costIntergrand';

end