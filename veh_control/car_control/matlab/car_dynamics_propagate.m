function data = car_dynamics_propagate(t, delta, T, state_init)

sim = CarSim();

if nargin > 3
    sim.state.Vx = state_init.Vx;
    sim.state.Vy = state_init.Vy;
    sim.state.dpsi = state_init.dpsi;
    try
        sim.state.omegaF = state_init.omega;
        sim.state.omegaR = state_init.omega;
    catch
        sim.state.omegaF = state_init.omegaF;
        sim.state.omegaR = state_init.omegaR;
    end
    sim.state.psi = state_init.psi;
end
sim.action.turn = delta;
sim.action.throttle = T;



if length(t) == 1
    dt = 0.02;
    tf = t;
    t = 0:dt:tf;
end

if length(delta) == 1
    delta = delta*ones(size(t));
end
if length(T) == 1
    T = T*ones(size(t));
end

for i=1:length(t);
    data.t(i) = t(i);
    sim.action.turn = delta(i);
    sim.action.throttle = T(i);
    data.state(i) = sim.state;
    data.action(i) = sim.action;
    if i>1
        sim = sim.simulate_global(sim, t(i)-t(i-1));
    end
end