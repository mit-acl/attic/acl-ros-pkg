
function crop_data_set

clc; clear all;

%% IMPORT DATA
delay = 0;
file = 'carlogdrift1';

start_time =2;
end_time = 10;

data = parseData(file,'.txt', delay, start_time, end_time);

data = crop_data(data, start_time, end_time);

figure(1);clf;
plotData( data , 1, '-', 2);



function out = crop_data(data, start_time, end_time)

ind = find(data.t > start_time & data.t < end_time);
out.t = data.t(ind);
out.state = data.state(ind);
out.action = data.action(ind);


function SA = parseData( file, ext, delay, start_time, end_time )
%% IMPORT DATA
logfile = importdata(strcat(file,ext),',',1);

t = logfile.data(:,23);
x = logfile.data(:,5);
y = logfile.data(:,6);
qw = logfile.data(:,8); qx = logfile.data(:,9); qy = logfile.data(:,10); qz = logfile.data(:,11);
psi = -atan2(2.0*(qw.*qz + qx.*qy), 1.0 - 2.0*(qy.*qy + qz.*qz));
dpsi = logfile.data(:,18);
dx = logfile.data(:,13); dy = logfile.data(:,14);
Vx = dx.*cos(psi) - dy.*sin(psi);
Vy = dx.*sin(psi) + dy.*cos(psi);
omegaF = logfile.data(:,34);
omegaR = logfile.data(:,35);
throttle = logfile.data(:,2);
delta = logfile.data(:,3);

t = t - t(1);

ind = find(t > start_time & t < end_time);
dlmwrite(strcat(file,'_crop',ext),logfile.data(ind,:),'delimiter',',','precision',20);

for i=1:length(t)
    s.x = x(i); s.y = y(i); s.Vx = Vx(i); s.Vy = Vy(i);
    s.psi = psi(i); s.dpsi = dpsi(i); s.omegaF = omegaF(i); s.omegaR = omegaR(i);
%     if omegaR(i) < 5;
%         throttle(i) = 0.01;
%     end
    a.throttle = throttle(i);
    a.turn = delta(i);
    SA.state(i) = s;
    SA.action(i) = a;
    SA.t(i) = t(i);
end

% Account for command delay
for i=length(t):-1:delay+1
    SA.action(i) = SA.action(i-delay);
end


function plotData( SA , fig, style, width)

for i=1:length(SA.t)
    data.t(i) = SA.t(i);
    data.x(i) = SA.state(i).x;
    data.y(i) = SA.state(i).y;
    data.Vx(i) = SA.state(i).Vx;
    data.Vy(i) = SA.state(i).Vy;
    data.omegaR(i) = SA.state(i).omegaR;
    data.throttle(i) = SA.action(i).throttle;
    data.delta(i) = SA.action(i).turn;
    data.dpsi(i) = SA.state(i).dpsi;
end

figure(fig);
subplot(2,1,1);hold on;
plot(data.t,data.Vx,['g' style],'linewidth', width)
plot(data.t,data.Vy,['b' style],'linewidth', width)
plot(data.t,data.omegaR./50,['r' style],'linewidth', width)
plot(data.t,data.throttle*5,['k' style],'linewidth', width)
legend('Vx','Vy', 'omegaR', 'throttle'); %, 'no slip Vx');
grid on;

subplot(2,1,2);hold on;
plot(data.t,-data.delta*4,['g' style],'linewidth', width); grid on;
plot(data.t,data.dpsi,['b' style],'linewidth', width);
legend('turn cmd', 'dpsi')

figure(fig+1);
plot(data.x,data.y,'linewidth', width)