clc;
clear all;

% this data comes from the carlogwheelspeed*.txt data files -- just trying
% to find the dc mapping from throttle to no-load wheel speed

throttle = [0.12,0.12,0.13,0.13,0.13,0.13,0.13,0.18,0.24,0.24,0.24,0.36,...
    0.36,0.36,0.42,0.48,0.48,0.60,0.60,0.60,0.66,0.72,0.84,0.84,0.96,1.08];
omega = [120,107,147,133,135,135,126,199,260,251,240,304,300,295,314,...
    326,317,336,336,331,341,338,355,351,352,352];

% ind = find(throttle>=0.5 & throttle<=0.85); % linear
% ind = find(throttle<=0.6); % cubic
% throttle = throttle(ind);
% omega = omega(ind);

t1 = 0:0.01:0.55;
t2 = 0.55:0.01:1.08;

w2 = 75*t2 + 290;
w2(w2>352) = 352;

w1 = 3.79e3*t1.^3 - 5.41e3*t1.^2 + 2.66e3*t1 - 1.29e2;

figure(1); clf;
scatter(throttle,omega)
hold all
plot(t1,w1);
plot(t2,w2);