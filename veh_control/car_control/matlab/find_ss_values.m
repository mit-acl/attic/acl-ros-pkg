%---------------------------------------------------%
% Get car steady-state values                       %
%---------------------------------------------------%

function find_ss_values
clear; clc; 

sim = CarSim();
T_ss = 0.5; % Throttle command, between 0 and 1
R_ss = 1.5; % m

% x0 = [Vx, Vy, omega, delta] % initial guess
x0 = [1.0917   -2.2894  127.5719    -0.453];

% rng(4,'twister') % for reproducibility


f = @(x) find_vals(x, T_ss, R_ss, sim);
options = optimoptions('fsolve','Display','iter', 'MaxIter', 10000, 'MaxFunEvals', 100000);
[out, fval] = fsolve(f, x0, options)

% xmulti
Vx_ss = out(1)
Vy_ss = out(2)
V_ss = sqrt(Vx_ss^2 + Vy_ss^2)
dpsi_ss = V_ss/R_ss
omega_ss = out(3)
delta_ss = out(4)

xdot = find_vals(out, T_ss, R_ss, sim)



function xdot = find_vals(x, T, R, sim)

sim.state.Vx = x(1);
sim.state.Vy = x(2);
sim.state.omegaF = x(3);
sim.state.omegaR = sim.state.omegaF;
sim.action.turn = x(4);
V = sqrt(sim.state.Vx^2 + sim.state.Vy^2);
sim.state.dpsi = V/R;
sim.action.throttle = T;

xdot = sim.dynamics(sim);
xdot = [xdot.Vx; xdot.Vy; xdot.dpsi; xdot.wF];
