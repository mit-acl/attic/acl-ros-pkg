% Plot Rosbag converted Log File

clc; clear all;

%% IMPORT DATA
% logfile = importdata('carlog.txt',',',1);
logfile = importdata('carlog_RC02_newESC_omegaFFest.txt',',',1);
% logfile = importdata('carlog_RC02_reverse.txt',',',1);
% logfile = importdata('carlog_RC02_omegaFFest.txt',',',1);


%% Choose data to use
% RC02_newESC
t_start = 17;
t_end = 90;
% t_start = 172;        % Coeffs using this second piece of data: Coeffs=[0.0427    0.0103   -0.014];
% t_end = 209;          % throttle = c(1)*exp(c(2)*omegaR) + c(3)*abs(turn)

% RC02
% t_start = 4;
% t_end = 56;

% down
% t_start = 30;
% t_end = 45;
% 
time = logfile.data(:,23);
time = time - time(1);
index = find(time > t_start & time < t_end);
% logfile.data = logfile.data(index,:);
% time = time(index);

col = 1;
% Command Data
t_c = logfile.data(:,col); col=col+1;
throttle_c = logfile.data(:,col); col=col+1;
turn_c = logfile.data(:,col); col=col+1;

% Vicon Data - pose
t_p = logfile.data(:,col); col=col+1;
x = logfile.data(:,col); col=col+1;
y = logfile.data(:,col); col=col+1;
z = logfile.data(:,col); col=col+1;
qw = logfile.data(:,col); col=col+1;
qx = logfile.data(:,col); col=col+1;
qy = logfile.data(:,col); col=col+1;
qz = logfile.data(:,col); col=col+1;

% Vicon Data - vel
t_v = logfile.data(:,col); col=col+1;
dx = logfile.data(:,col); col=col+1;
dy = logfile.data(:,col); col=col+1;
dz = logfile.data(:,col); col=col+1;
p = logfile.data(:,col); col=col+1;
q = logfile.data(:,col); col=col+1;
r = logfile.data(:,col); col=col+1;

% Vicon Data -accel (ignore for now)
col = col+4;

% State information
t_s = logfile.data(:,col); col=col+1;
x_s = logfile.data(:,col); col=col+1;
y_s = logfile.data(:,col); col=col+1;
z_s = logfile.data(:,col); col=col+1;
qw_s = logfile.data(:,col); col=col+1;
qx_s = logfile.data(:,col); col=col+1;
qy_s = logfile.data(:,col); col=col+1;
qz_s = logfile.data(:,col); col=col+1;
Vx = logfile.data(:,col); col=col+1;
Vy = logfile.data(:,col); col=col+1;
r_s = logfile.data(:,col); col=col+1;
omegaF = logfile.data(:,col); col=col+1;
omegaR = logfile.data(:,col); col=col+1;
throttle_c = logfile.data(:,col); col=col+1;



% for i=1:length(throttle_c)
%     if throttle_c(i) > 0.8;
%         throttle_c(i) = 0.8;
%     end
% end


fitType = 'exp1';%'exp1' or 'poly1';
myFit = fit(omegaR,throttle_c,fitType)

if strcmp(fitType,'exp1') == 1
    fitData = myFit.a*exp(myFit.b*omegaR);
elseif strcmp(fitType,'poly1') == 1
    fitData = myFit.p1*omegaR + myFit.p2;
elseif strcmp(fitType, 'exp2') == 1
    fitData = myFit.a*exp(myFit.b*omegaR) + myFit.c*exp(myFit.d*omegaR);
elseif strcmp(fitType, 'poly2') == 1
    fitData = myFit.p1*omegaR.^2 + myFit.p2*omegaR + myFit.p3;
end

% want a function that predicts throttle command given omega
figure(1); clf;
plot(omegaR, throttle_c);
hold all
plot(omegaR, fitData);


% new ESC

% Define the function
% fun = inline('c(1)*exp(c(2)*tt(:,1))+c(3)*exp(c(4)*tt(:,1))+c(5)*abs(tt(:,2))', 'c','tt');
fun = inline('c(1)*exp(c(2)*tt(:,1))+c(3)*abs(tt(:,2))', 'c','tt');
% fun = inline('c(1)*tt(:,1)+c(2)+c(3)*abs(tt(:,2))', 'c','tt');

% Find the coefficients -- note the [x y] third parameter
Coeffs=lsqcurvefit(fun,[0.02 00.01 -10],[omegaR turn_c],throttle_c,[],[],optimset('Display', 'iter'))
Coeffs=[0.0427    0.0103   -0.014];
fitData = Coeffs(1)*exp(Coeffs(2)*omegaR) + Coeffs(3)*abs(turn_c);
% Coeffs=lsqcurvefit(fun,[0.02 00.01 0 0 -10],[omegaR turn_c],throttle_c,[],[],optimset('Display', 'iter'))
% fitData = Coeffs(1)*exp(Coeffs(2)*omegaR) + Coeffs(3)*exp(Coeffs(4)*omegaR) + Coeffs(5)*abs(turn_c);

figure(2); clf;
plot(time,throttle_c,time,omegaR/200,time, turn_c)
hold all
plot(time,fitData)