%---------------------------------------------------%
% Car Problem:                   %
%---------------------------------------------------%

clear; % clc; 

% prior_solution = 'car180.mat';

%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%

Vg = 2.0;
T_ss = 0.25;
delta_ss = 0;

sim = CarSim();

%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; 
dx0 = 0; dy0 = Vg; dpsi0 = 0; omega0 = Vg/sim.param.rF; psi0 = pi/2;
dxf = 0; dyf = Vg; dpsif = 0; omegaf = Vg/sim.param.rF; psif = -pi/2;

sim.xf.dx = dxf;
sim.xf.dy = dyf;

sim.action.turn = 0;
sim.action.throttle = T_ss;

%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0.1; tfmax = 4.0;
dxmax = 5; dxmin = -5;
dymax = 5; dymin = -5;
dpsimax = 6; dpsimin = -6;
omegamax = 400; omegamin = 0;
psimax = 2*pi; psimin = -2*pi;
deltamax = 1; deltamin = -1;
torquemax = 1; torquemin = 0;

%Integral parameters:
integralBoundLow = 0;
integralBoundUpp = 20e3;


%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmax; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = [dx0, dy0, dpsi0, omega0, psi0]; 
bounds.phase.initialstate.upper = [dx0, dy0, dpsi0, omega0, psi0]; 
bounds.phase.state.lower = [dxmin, dymin, dpsimin, omegamin, psimin]; 
bounds.phase.state.upper = [dxmax, dymax, dpsimax, omegamax, psimax];
% bounds.phase.finalstate.lower = [dxf, dyf, dpsimin, omegamin, psimin]; 
% bounds.phase.finalstate.upper = [dxf, dyf, dpsimax, omegamax, psimax];
bounds.phase.finalstate.lower = [dxmin, dymin, dpsimin, omegamin, psimin]; 
bounds.phase.finalstate.upper = [dxmax, dymax, dpsimax, omegamax, psimax];
bounds.phase.control.lower = [deltamin, torquemin]; 
bounds.phase.control.upper = [deltamax, torquemax];
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;
setup.nlp.ipoptoptions.tolerance = 1e-3;
setup.nlp.ipoptoptions.maxiterations = 500;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%

if exist('prior_solution')
    prior = load(prior_solution);
    guess.phase.time = prior.Time';
    guess.phase.state = prior.State';
    guess.phase.control = prior.Force';
%     guess.phase.control(:,2) = T_ss*ones(size(prior.Force'));
%     bounds.phase.finaltime.upper = prior.Time(end)+0.8;

    aVx = 1;
    aVy = 1;
    omega_c = 0.5;
    n = length(guess.phase.time);
    costIntergrandguess = zeros(1,n);
    for i=1:n

        costIntergrandguess(i) = aVx*(guess.phase.state(i,1)-sim.xf.dx).^2/(2*omega_c^2) + ...
            aVy*(guess.phase.state(i,2)-sim.xf.dy).^2/(2*omega_c^2); 
        
%         costIntergrandguess(i) = 1-exp(-costIntergrandguess(i));
    end
    guess.phase.integral = sum(costIntergrandguess);

else

    sim_init = CarSim();
    sim_init.state.dy = dy0;
    sim_init.state.psi = psi0;
    sim_init.state.omega = omega0;

    t_init = linspace(0, tfmax, 100*tfmax/3);
    delta1 = -1;
    delta1n = 20;
    delta2 = .32;
    delta2n = 9 + delta1n;
    delta3 = 0;
    delta3n = 7 + delta2n;
    delta = delta_ss*ones(size(t_init));
    delta(1:delta1n) = delta1*ones(size(delta(1:delta1n)));
    delta(delta1n+1:delta2n) = delta2*ones(size(delta(delta1n+1:delta2n)));
    delta(delta2n+1:delta3n) = delta3*ones(size(delta(delta2n+1:delta3n)));

    data = car_dynamics_propagate_global(t_init, delta, T_ss, sim_init.state);
    n = length(data.t);

    guess.phase.time = zeros(n,1);
    guess.phase.state = zeros(n,length(bounds.phase.finalstate.lower));
    guess.phase.control = zeros(n,length(bounds.phase.control.lower));
    x_guess = zeros(n,1);
    y_guess = zeros(n,1);
    aVx = 1;
    aVy = 1;
    omega_c = 0.5;
    costIntergrandguess = zeros(1,n);
    for i=1:n
        guess.phase.time(i)    = data.t(i); 
        guess.phase.state(i,:) = [data.state(i).dx, data.state(i).dy, ...
            data.state(i).dpsi, data.state(i).omegaR, data.state(i).psi]; 
        guess.phase.control(i,:) = [data.action(i).turn, data.action(i).throttle];
        x_guess(i) = data.state(i).x;
        y_guess(i) = data.state(i).y;

        costIntergrandguess(i) = aVx*(data.state(i).dx-sim.xf.dx).^2/(2*omega_c^2) + ...
            aVy*(data.state(i).dy-sim.xf.dy).^2/(2*omega_c^2);
        
%         costIntergrandguess(i) = 1-exp(-costIntergrandguess(i));
    end
    guess.phase.integral = sum(costIntergrandguess);

    plot_gpops_car_global(guess.phase.time', guess.phase.state', ...
        guess.phase.control', 'car_guess.mat')
    figure(6); clf;
    plot(x_guess,y_guess)
end
return

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-PattersonRao';
mesh.tolerance                  = 1e-1;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'Car_';
setup.functions.continuous = @car180_continuous;
setup.functions.endpoint = @car180_endpoint;
setup.auxdata = sim;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseFD';
setup.derivatives.derivativelevel = 'first';
setup.scales.method = 'none';
setup.method = 'RPM-Differentiation';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

% solution = guess;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

%%
Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

plot_gpops_car_global(Time, State, Force, 'car180.mat')