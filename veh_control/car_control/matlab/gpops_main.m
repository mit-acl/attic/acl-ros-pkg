%---------------------------------------------------%
% Car Problem:                   %
%---------------------------------------------------%

% clear; clc; 


%-------------------------------------------------------------------------%
%------------------ Provide Auxiliary Data for Problem -------------------%
%-------------------------------------------------------------------------%

Vx_ss = 0.3198;
Vy_ss = -1.1836;
dpsi_ss = 1.5326;
omega_ss = 184.5386;
delta_ss = -0.2650;
R_ss = 0.8;
T_ss = 0.5;

% Vx_ss = 0.2148;
% Vy_ss = -0.9446;
% dpsi_ss = 1.9375;
% omega_ss = 183.4706;
% delta_ss = -0.3005;
% R_ss = 0.5;
% T_ss = 0.5;

sim = CarSim();
sim.ss.Vx = Vx_ss;
sim.ss.Vy = Vy_ss;
sim.ss.dpsi = dpsi_ss;
sim.ss.omega = omega_ss;
sim.ss.delta = delta_ss;
sim.ss.T = T_ss;

sim.action.turn = delta_ss;
sim.action.throttle = T_ss;

%-------------------------------------------------------------------%
%----------------------- Boundary Conditions -----------------------%
%-------------------------------------------------------------------%
t0 = 0; 
Vx0 = 0; Vy0 = 0; dpsi0 = 0; omega0 = 0;

%-------------------------------------------------------------------%
%----------------------- Limits on Variables -----------------------%
%-------------------------------------------------------------------%
tfmin = 0.1; tfmax = 3;
Vxmax = 5; Vxmin = -2;
Vymax = 1; Vymin = -5;
dpsimax = 5; dpsimin = -1;
omegamax = 400; omegamin = 0;
deltamax = -0.1; deltamin = -1;
torquemax = 1; torquemin = 0;

%Integral parameters:
integralBoundLow = 0;
integralBoundUpp = 1000;


%-------------------------------------------------------------------------%
%----------------------- Setup for Problem Bounds ------------------------%
%-------------------------------------------------------------------------%
iphase = 1;
bounds.phase.initialtime.lower = t0; 
bounds.phase.initialtime.upper = t0;
bounds.phase.finaltime.lower = tfmax; 
bounds.phase.finaltime.upper = tfmax;
bounds.phase.initialstate.lower = [Vx0, Vy0, dpsi0, omega0]; 
bounds.phase.initialstate.upper = [Vx0, Vy0, dpsi0, omega0]; 
bounds.phase.state.lower = [Vxmin, Vymin, dpsimin, omegamin]; 
bounds.phase.state.upper = [Vxmax, Vymax, dpsimax, omegamax];
% bounds.phase.finalstate.lower = [Vxmin, Vymin, dpsimin, omegamin]; 
% bounds.phase.finalstate.upper = [Vxmax, Vymax, dpsimax, omegamax];
bounds.phase.finalstate.lower = [Vx_ss, Vy_ss, dpsi_ss, omegamin]; 
bounds.phase.finalstate.upper = [Vx_ss, Vy_ss, dpsi_ss, omegamax];
bounds.phase.control.lower = [deltamin]; %, torquemin]; 
bounds.phase.control.upper = [deltamax]; %, torquemax];
bounds.phase.integral.lower = integralBoundLow;
bounds.phase.integral.upper = integralBoundUpp;
setup.nlp.ipoptoptions.tolerance = 1e-3;
setup.nlp.ipoptoptions.maxiterations = 500;

%-------------------------------------------------------------------------%
%---------------------- Provide Guess of Solution ------------------------%
%-------------------------------------------------------------------------%
data = car_dynamics_propagate(tfmax, delta_ss, T_ss);
n = length(data.t);

guess.phase.time = zeros(n,1);
guess.phase.state = zeros(n,length(bounds.phase.finalstate.lower));
guess.phase.control = zeros(n,length(bounds.phase.control.lower));

aVx = 1;
aVy = 1;
adpsi = 1;
costIntergrandguess = zeros(1,n);
for i=1:n
    guess.phase.time(i)    = data.t(i); 
    guess.phase.state(i,:) = [data.state(i).Vx, data.state(i).Vy, ...
        data.state(i).dpsi, data.state(i).omegaF]; 
    guess.phase.control(i,:) = [data.action(i).turn]; %, data.action(i).throttle];
    

    costIntergrandguess(i) = aVx*(data.state(i).Vx-sim.ss.Vx).^2 + ...
        aVy*(data.state(i).Vy-sim.ss.Vy).^2 + ...
        adpsi*(data.state(i).dpsi-sim.ss.dpsi).^2; 
    
    costIntergrandguess(i) = 1-exp(-costIntergrandguess(i));
end
guess.phase.integral = sum(costIntergrandguess);

figure(5); clf; 
plot(costIntergrandguess)

%-------------------------------------------------------------------------%
%----------Provide Mesh Refinement Method and Initial Mesh ---------------%
%-------------------------------------------------------------------------%
mesh.maxiteration               = 10;
mesh.method                     = 'hp-LiuRao';
mesh.tolerance                  = 1e-1;

%-------------------------------------------------------------------------%
%------------- Assemble Information into Problem Structure ---------------%        
%-------------------------------------------------------------------------%
setup.name = 'Car_';
setup.functions.continuous = @car_continuous;
setup.functions.endpoint = @car_endpoint;
setup.auxdata = sim;
setup.bounds = bounds;
setup.guess = guess;
setup.displaylevel = 2;
setup.mesh = mesh;
setup.nlp.solver = 'ipopt'; % {'ipopt','snopt'}
setup.derivatives.supplier = 'sparseCD';
setup.derivatives.derivativelevel = 'second';
setup.scales.method = 'automatic-bounds';
setup.method = 'RPM-Integration';

%-------------------------------------------------------------------------%
%------------------------- Solve Problem Using GPOP2 ---------------------%
%-------------------------------------------------------------------------%
output = gpops2(setup);
solution = output.result.solution;

% solution = guess;

%--------------------------------------------------------------------------%
%------------------------------- Plot Solution ----------------------------%
%--------------------------------------------------------------------------%

%%
Time = solution.phase(1).time';
State = solution.phase(1).state';
Force = solution.phase(1).control';

Vx = State(1,:); Vy = State(2,:); dpsi = State(3,:);
omegaF = State(4,:); omegaR = omegaF;
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2 + Vy.^2);

delta = Force(1,:);
% torque = Force(2,:);
torque = sim.ss.T*ones(size(delta));

figure(1); clf;
mSize = 10;
subplot(3,1,1); hold on;
    plot(Time, V,'k.','MarkerSize',mSize)
    plot(Time, V);
    xlabel('time (s)')
    ylabel('V (m/s)')
    
subplot(3,1,2); hold on;
    plot(Time, beta*180/pi,'k.','MarkerSize',mSize)
    plot(Time, beta*180/pi);
    xlabel('time (s)')
    ylabel('beta (deg)')
    
subplot(3,1,3); hold on;
    plot(Time, dpsi*180/pi,'k.','MarkerSize',mSize)
    plot(Time, dpsi*180/pi);
    xlabel('time (s)')
    ylabel('rate (deg/s)')

figure(3); clf; hold all
plot(Time, omegaF, 'k.','MarkerSize',mSize)
plot(Time, omegaF);
plot(Time, omegaR, 'k.','MarkerSize',mSize)
plot(Time, omegaR);
    xlabel('time (s)')
    ylabel('omega (rad/s)')
    
figure(2); clf; hold all
subplot(2,1,1)
    plot(Time, delta,'k.','MarkerSize',mSize)
    plot(Time, delta);
    xlabel('time (s)')
    ylabel('delta')
    
    subplot(2,1,2)
    plot(Time, torque,'k.','MarkerSize',mSize)
    plot(Time, torque);
    xlabel('time (s)')
    ylabel('throttle')
    
figure(4); clf;
mSize = 10;
subplot(3,1,1); hold on;
    plot(Time, Vx,'k.','MarkerSize',mSize)
    plot(Time, Vx);
    xlabel('time (s)')
    ylabel('Vx (m/s)')
    
subplot(3,1,2); hold on;
    plot(Time, Vy,'k.','MarkerSize',mSize)
    plot(Time, Vy);
    xlabel('time (s)')
    ylabel('Vy (m/s)')
    
subplot(3,1,3); hold on;
    plot(Time, dpsi,'k.','MarkerSize',mSize)
    plot(Time, dpsi);
    xlabel('time (s)')
    ylabel('rate (rad/s)')
    
save('training_car.mat','Time','State','Force');   
 
% % %Save the solution if desired:
% % outputPrev = output;
% % save('oldSoln.mat','outputPrev');
