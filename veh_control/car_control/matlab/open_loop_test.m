function open_loop_test

clc; clear all;

Vx_ss = 0.3892;
Vy_ss = -1.3149;
dpsi_ss = 1.3713;
omega_ss = 185.2546;
delta_ss = -0.2533;
T_ss = 0.52;

state_init.Vx = Vx_ss;
state_init.Vy = Vy_ss;
state_init.dpsi = dpsi_ss;
state_init.omega = omega_ss;

% data = car_dynamics_propagate(3, delta_ss, T_ss, state_init);
data = car_dynamics_propagate(3, delta_ss, T_ss);

figure(1); clf;
plotData(data, 1, '-', 3);

function plotData( SA , fig, style, width)

for i=1:length(SA.t)
    data.t(i) = SA.t(i);
    data.x(i) = SA.state(i).x;
    data.y(i) = SA.state(i).y;
    data.Vx(i) = SA.state(i).Vx;
    data.Vy(i) = SA.state(i).Vy;
    data.omegaR(i) = SA.state(i).omegaR;
    data.throttle(i) = SA.action(i).throttle;
    data.delta(i) = SA.action(i).turn;
    data.dpsi(i) = SA.state(i).dpsi;
end

figure(fig);
subplot(2,1,1);hold on;
plot(data.t,data.Vx,['g' style],'linewidth', width)
plot(data.t,data.Vy,['b' style],'linewidth', width)
plot(data.t,data.omegaR./50,['r' style],'linewidth', width)
plot(data.t,data.throttle*5,['k' style],'linewidth', width)
legend('Vx','Vy', 'omegaR', 'throttle'); %, 'no slip Vx');
grid on;

subplot(2,1,2);hold on;
plot(data.t,-data.delta*4,['g' style],'linewidth', width); grid on;
plot(data.t,data.dpsi,['b' style],'linewidth', width);
legend('turn cmd', 'dpsi')

figure(fig+1);
plot(data.x,data.y,'*','linewidth', width)