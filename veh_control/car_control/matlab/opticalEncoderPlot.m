%%
% Generates a plot for the car optical encoder
% opticalEncoderPlot.m
%%

clc; clear all;

%% Inputs %%%%
% dimension in cm
r1 = 5.7/2; %5.3/2; % outside wheel radius
r2 = 4.3/2; %2; % inside radius
n = 10; % number of black and white segments
%% End Inputs %%%%

% error checking
if mod(n,2) ~= 0
    display('Error: n must be odd!');
    return;
end
if r2 >= r1
    display('Error: r2 must be strictly smaller than r1');
    return;
end

%% start program
res = 100;
i = 0:n-1;
a = 2*pi*i/n;

% angle of black segments
blackSeg = zeros(n/2, res);
pntr = 1;
for i = 1:2:n
    blackSeg(pntr,:) = linspace(a(i),a(i+1),res);
    pntr = pntr + 1;
end

% x-y coordinates of black segments
x = zeros(n/2, 2*res);
y = x;
for i = 1:n/2
    x(i,:) = [r1*cos(blackSeg(i,:)) fliplr(r2*cos(blackSeg(i,:)))];
    y(i,:) = [r1*sin(blackSeg(i,:)) fliplr(r2*sin(blackSeg(i,:)))];
end

% generate plots
figure(1); clf(1);
hold on
for i = 1:n/2
    fill(x(i,:),y(i,:),'k');
end

print -dpdf test.pdf
set(gca,'Units','centimeters',...
  'Position',[0 0 2.2*r1 2.2*r1],...
  'XLim',[-r1 r1],...
  'YLim',[-r1 r1])
% set(gcf, 'Units','centimeters', 'Position',[0 0 r1*10 r1*10])
set(gcf,'PaperUnits','centimeters',...
  'PaperPosition',5+[0 0 2.2*r1 2.2*r1])
print -dpdf test.pdf
% print -dtiff -r0 out.tiff