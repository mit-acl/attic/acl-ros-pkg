% Plot Rosbag converted Log File

clc; clear all;

%% IMPORT DATA
% logfile = importdata('carlogwheelspeed5.txt',',',1);
logfile = importdata('carlogfull8.txt',',',1);
% logfile = importdata('carlogstraight2.txt',',',1);
% logfile = importdata('carlog_omegaEst.txt',',',1);


%% Choose data to use
% up
% t_start = 15;
% t_end = 35;

% down
% t_start = 30;
% t_end = 45;
% 
% time = logfile.data(:,1);
% time = time - time(1);
% index = find(time > t_start & time < t_end);
% logfile.data = logfile.data(index,:);

col = 1;
% Command Data
t_c = logfile.data(:,col); col=col+1;
throttle_c = logfile.data(:,col); col=col+1;
turn_c = logfile.data(:,col); col=col+1;

% Vicon Data - pose
t_p = logfile.data(:,col); col=col+1;
x = logfile.data(:,col); col=col+1;
y = logfile.data(:,col); col=col+1;
z = logfile.data(:,col); col=col+1;
qw = logfile.data(:,col); col=col+1;
qx = logfile.data(:,col); col=col+1;
qy = logfile.data(:,col); col=col+1;
qz = logfile.data(:,col); col=col+1;

% Vicon Data - vel
t_v = logfile.data(:,col); col=col+1;
dx = logfile.data(:,col); col=col+1;
dy = logfile.data(:,col); col=col+1;
dz = logfile.data(:,col); col=col+1;
p = logfile.data(:,col); col=col+1;
q = logfile.data(:,col); col=col+1;
r = logfile.data(:,col); col=col+1;

% Vicon Data -accel (ignore for now)
col = col+4;

% State information
t_s = logfile.data(:,col); col=col+1;
x_s = logfile.data(:,col); col=col+1;
y_s = logfile.data(:,col); col=col+1;
z_s = logfile.data(:,col); col=col+1;
qw_s = logfile.data(:,col); col=col+1;
qx_s = logfile.data(:,col); col=col+1;
qy_s = logfile.data(:,col); col=col+1;
qz_s = logfile.data(:,col); col=col+1;
Vx = logfile.data(:,col); col=col+1;
Vy = logfile.data(:,col); col=col+1;
r_s = logfile.data(:,col); col=col+1;
omegaF = logfile.data(:,col); col=col+1;
omegaR = logfile.data(:,col); col=col+1;
% throttle_s = logfile.data(:,col); col=col+1;

%% PROCESS DATA
% normalize times to agree with state data
t = t_s;
start_time = t(1);
t = t - start_time;

% for i = 1:length(t)
%     if omegaF(i) > 120
%         omegaF(i) = 0;
%     end
%     if omegaR(i) > 120
%         omegaR(i) = 0;
%     end
% end


%% PLOT DATA
% close all

clr = colormap('lines');

dt = 0.02;
t = 0:dt:(length(throttle_c)-1)*dt;

fignum = 1;
% Position
% f(fignum)=figure(fignum); clf; fignum = fignum+1;
% plot(t,dx,t,dy)

% ind = find(t>18);
% throttle_c(ind) = throttle_c(ind)*0;

% Position
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(t,omegaF,t,omegaR,t,throttle_c*100)
hold all

% throttle_c = throttle_c-0.13;



K = 430;
aup = 9;
offset = 0.205;
delay = 0.08;
Gup = tf(K*aup,[1,aup],'InputDelay',delay);
y = lsim(Gup,throttle_c+offset,t);
% plot(t,y)

adown = 1.4;
Gdown = tf(K*adown,[1,adown],'InputDelay',delay);
y = lsim(Gdown,throttle_c+offset,t);
% plot(t,y)

Tspan = [0, t(end)];
IC = 0;
[T, ysim] = ode45(@(tt,yy) wheelspeed(tt,yy,0,t,throttle_c),Tspan,IC);

plot(T,ysim);


% ind1 = find(omegaR > 100);
% [omegaRsort, ind2] = sort(omegaR(ind1));
% tmp = throttle_s(ind1);
% throttlesort = tmp(ind2);
% logthrottle = log(throttlesort);
% coeffs = polyfit(omegaRsort,logthrottle,1);
% % throttlefit = exp(coeffs(1)*omegaRsort + coeffs(2));
% expFit = fit(omegaRsort,throttlesort,'exp1');
% throttlefit = expFit.a*exp(expFit.b*omegaRsort);
% 
% fignum = 1;
% % Position
% f(fignum)=figure(fignum); clf; fignum = fignum+1;
% % plot(t,omegaR,t,throttle_s)
% % plot(omegaR,throttle_s)
% plot(omegaRsort,throttlesort)
% hold all
% plot(omegaRsort,throttlefit)
% plot(expFit,omegaRsort,throttlesort)

% for i=1:length(omegaR)
%     if omegaR(i) > 600 | omegaR(i) < -600
%         omegaR(i) = 0;
%     end
% end
% 
% d = fdesign.lowpass('Fp,Fst,Ap,Ast',3,5,0.5,40,100);
% Hd = design(d,'equiripple');
% omegaRfilt = filtfilt(Hd.numerator,1,omegaR);
% for i=1:length(omegaR)
%     if omegaR(i) ==0
%         omegaRfilt(i) = 0;
%     end
% end
% 
% fignum = 1;
% f(fignum)=figure(fignum); clf; fignum = fignum+1;
% hold all
% plot(t,omegaR)
% plot(t,omegaRfilt,'LineWidth',1.5)
% plot(t,throttle_c)
% 
% %% get data for system id of speed control loop
% ind = find (t < 161);
% output = omegaRfilt(ind);
% input = throttle_c(ind);
% 
% % from this data, best fit model for wheel speed transfer function is:
% % (using ident -- system identification toolbox to find best one pole
% % system)
% % y/x = 0.08923/(s + 0.08618)
% a = 0.08923;
% b = 0.08618;
% omegaRmodel = zeros(size(omegaR));
% for i=2:length(omegaR)
%     omegaRmodel(i) = (1-b)*omegaRmodel(i-1) + a*throttle_c(i);
% end
% plot(t,omegaRmodel)


%% Link x axes
ax = [];
for ii=1:length(f)
    ax = [ax; get(f(ii),'children')];
end
linkaxes(ax,'x');
