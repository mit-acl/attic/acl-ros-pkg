function plot_gpops_car(Time, State, Force, T_ss, data_file)

Vx = State(1,:); Vy = State(2,:); dpsi = State(3,:);
omegaF = State(4,:); omegaR = omegaF;
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2 + Vy.^2);

n = size(State, 1);
plot_psi = 0;
if n > 4
    psi = State(5,:);
    plot_psi = 1;
end

delta = Force(1,:);
if T_ss == 0
    torque = Force(2,:);
else
    torque = T_ss*ones(size(delta));
end

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
plot(Time, V,'k.','MarkerSize',mSize)
plot(Time, V);
xlabel('time (s)')
ylabel('V (m/s)')

subplot(2,1,2); hold on;
plot(Time, beta*180/pi,'k.','MarkerSize',mSize)
plot(Time, beta*180/pi);
xlabel('time (s)')
ylabel('beta (deg)')

figure(2); clf;
subplot(2,1,1); hold on;
plot(Time, dpsi*180/pi,'k.','MarkerSize',mSize)
plot(Time, dpsi*180/pi);
xlabel('time (s)')
ylabel('rate (deg/s)')

if plot_psi
    subplot(2,1,2); hold on;
    plot(Time, psi*180/pi,'k.','MarkerSize',mSize)
    plot(Time, psi*180/pi);
    xlabel('time (s)')
    ylabel('heading (deg)')
end

figure(3); clf; hold all
plot(Time, omegaF, 'k.','MarkerSize',mSize)
plot(Time, omegaF);
plot(Time, omegaR, 'k.','MarkerSize',mSize)
plot(Time, omegaR);
xlabel('time (s)')
ylabel('omega (rad/s)')

figure(4); clf; hold all
subplot(2,1,1)
plot(Time, delta,'k.','MarkerSize',mSize)
plot(Time, delta);
xlabel('time (s)')
ylabel('delta')

subplot(2,1,2)
plot(Time, torque,'k.','MarkerSize',mSize)
plot(Time, torque);
xlabel('time (s)')
ylabel('throttle')

figure(5); clf;
mSize = 10;
subplot(3,1,1); hold on;
plot(Time, Vx,'k.','MarkerSize',mSize)
plot(Time, Vx);
xlabel('time (s)')
ylabel('Vx (m/s)')

subplot(3,1,2); hold on;
plot(Time, Vy,'k.','MarkerSize',mSize)
plot(Time, Vy);
xlabel('time (s)')
ylabel('Vy (m/s)')

subplot(3,1,3); hold on;
plot(Time, dpsi,'k.','MarkerSize',mSize)
plot(Time, dpsi);
xlabel('time (s)')
ylabel('rate (rad/s)')

if data_file ~= 0
    save(data_file,'Time','State','Force');
end
