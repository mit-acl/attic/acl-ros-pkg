function plot_gpops_car_global(Time, State, Force, data_file)

dx = State(1,:); dy = State(2,:); dpsi = State(3,:);
omegaF = State(4,:); omegaR = omegaF;
psi = wrapToPi(State(5,:));
beta = wrapToPi(atan2(dy,dx) - psi);
V = sqrt(dx.^2 + dy.^2);

delta = Force(1,:);
torque = Force(2,:);

figure(1); clf;
mSize = 10;
subplot(2,1,1); hold on;
plot(Time, V,'k.','MarkerSize',mSize)
plot(Time, V);
xlabel('time (s)')
ylabel('V (m/s)')

subplot(2,1,2); hold on;
plot(Time, beta*180/pi,'k.','MarkerSize',mSize)
plot(Time, beta*180/pi);
xlabel('time (s)')
ylabel('beta (deg)')

figure(2); clf;
subplot(2,1,1); hold on;
plot(Time, dpsi*180/pi,'k.','MarkerSize',mSize)
plot(Time, dpsi*180/pi);
xlabel('time (s)')
ylabel('rate (deg/s)')

subplot(2,1,2); hold on;
plot(Time, psi*180/pi,'k.','MarkerSize',mSize)
plot(Time, psi*180/pi);
xlabel('time (s)')
ylabel('heading (deg)')

figure(3); clf; hold all
plot(Time, omegaF, 'k.','MarkerSize',mSize)
plot(Time, omegaF);
plot(Time, omegaR, 'k.','MarkerSize',mSize)
plot(Time, omegaR);
xlabel('time (s)')
ylabel('omega (rad/s)')

figure(4); clf; hold all
subplot(2,1,1)
plot(Time, delta,'k.','MarkerSize',mSize)
plot(Time, delta);
xlabel('time (s)')
ylabel('delta')

subplot(2,1,2)
plot(Time, torque,'k.','MarkerSize',mSize)
plot(Time, torque);
xlabel('time (s)')
ylabel('throttle')

figure(5); clf;
mSize = 10;
subplot(3,1,1); hold on;
plot(Time, dx,'k.','MarkerSize',mSize)
plot(Time, dx);
xlabel('time (s)')
ylabel('dx (m/s)')

subplot(3,1,2); hold on;
plot(Time, dy,'k.','MarkerSize',mSize)
plot(Time, dy);
xlabel('time (s)')
ylabel('dy (m/s)')

subplot(3,1,3); hold on;
plot(Time, dpsi,'k.','MarkerSize',mSize)
plot(Time, dpsi);
xlabel('time (s)')
ylabel('rate (rad/s)')

if data_file ~= 0
    save(data_file,'Time','State','Force');
end
