% compute_poly_paths_cf.m

%% TODOs
% - make the initial and final y vel and accel free parameters that must
% match with each other (assuming a closed course)

function [T C] = polygen(varargin)

% nth order closed form solution for polynomial generation

% T - trajectory
% C - polynomial coefficients of the trajectory
if nargin > 0
    wp  = varargin{1};
    wp0 = varargin{2};
    wpf = varargin{3};
    wpm_acc = varargin{4};
    wpm_elm = varargin{5};
    sigma = varargin{6};
    n   = varargin{7};
    tt  = varargin{8};
    res = varargin{9};
    plotfigures = varargin{10};
else
    clc;
    n = 4;
    xcross = 0.2;
    wp = [-xcross -xcross xcross xcross -xcross;...
           2 -2 -2 2 2;...
           0  0 0 0 0]; % waypoints
    wp0 = [0;... % initial x vel
        -.5;... % initial y vel
        0];   % initial z vel
    wpf = [0;... % final x vel
        -.5;... % final y vel
        0];   % final z vel
    wpm_acc = [0 0 0;0 0 0;0 0 0]; % middle acceleration constraint
    wpm_elm =[2 3 4]; % wp element that acceleration constraint will replace
    sigma = [1 1 1 1];  % upright 1 or inverted 0 for that time segment
    tt = [3 1 3 1]
%     tt = [.6 .6 0.6 0.4]; % time vector must have size(wp,2)-1 elements - each
    % element is the time for that respective segment
    res = 30;
    plotfigures = true;
%     clc;
%     n = 4;
%     wp = [-1 1  ;...
%           -2 -2 ;...
%            0 0  ]; % waypoints
%     wp0 = [0;... % initial x vel
%         -1;... % initial y vel
%         0];   % initial z vel
%     wpf = [0;... % final x vel
%         1;... % final y vel
%         0];   % final z vel
%     wpm_acc = [-5;0;0]; % middle acceleration constraint
%     wpm_elm =[]; % wp element that acceleration constraint will replace
%     sigma = [1 1 1 1];  % upright 1 or inverted 0 for that time segment
%     tt = [.3 0.4 0.6 0.4]; % time vector must have size(wp,2)-1 elements - each
%     % element is the time for that respective segment
%     res = 30;
%     plotfigures = true;
end

num_seg = size(wp,2)-1; % number of segments
m = n*num_seg; % number of constraints

% convert relative time to absolute time -- assumed that t0 = 0
t = zeros(1,num_seg+1);
for kk = 1:num_seg
    t(kk+1) = tt(kk) + t(kk);
end

% basis vectors
numder = n-1;
x = zeros(numder,n);
x(1,:) = ones(1,n);

for jj = 2:numder
    x(jj,jj:n) = polyder_mark(x(jj-1,jj-1:n));
end

% tic
[Ax,bx] = findX(wp(1,:),wp0(1,:),wpf(1,:),wpm_acc(1,:),wpm_elm,x,n,m);
[Ay,by] = findX(wp(2,:),wp0(2,:),wpf(2,:),wpm_acc(2,:),[],x,n,m);
[Az,bz] = findX(wp(3,:),wp0(3,:),wpf(3,:),wpm_acc(3,:),[],x,n,m);
% toc

% tic
XX = fill_A(Ax,bx,wpm_elm,n,m,t);
YY = fill_A(Ay,by,[],n,m,t);
ZZ = fill_A(Az,bz,[],n,m,t);
% toc

% XX = findX(wp(1,:),wp0(1,:),wpf(1,:),n,m);
% YY = findX(wp(2,:),wp0(2,:),wpf(2,:),n,m);
% ZZ = findX(wp(3,:),wp0(3,:),wpf(3,:),n,m);

TT = zeros(1,num_seg+1);
T.t = [];
T.upright = [];
T.x = [];
T.dx = [];
T.d2x = [];
T.d3x = [];
T.d4x = [];
T.y = [];
T.dy = [];
T.d2y = [];
T.d3y = [];
T.d4y = [];
T.z = [];
T.dz = [];
T.d2z = [];
T.d3z = [];
T.d4z = [];
C.t = 0;
C.x = XX;
C.dx = [];
C.d2x = [];
C.d3x = [];
C.d4x = [];
C.y = YY;
C.dy = [];
C.d2y = [];
C.d3y = [];
C.d4y = [];
C.z = ZZ;
C.dz = [];
C.d2z = [];
C.d3z = [];
C.d4z = [];

for i=1:num_seg
    TT(i+1) = tt(i) + TT(i);
    time = linspace(TT(i), TT(i+1), res);
    if i~=num_seg
        time = time(1:end-1);
    end
    T.t = [T.t time];
    C.t = [C.t TT(i+1)];
    
    % for commanding upside down
    T.upright = [T.upright ones(size(time))*sigma(i)];
    
    X = XX(i,:);
    DX = polyder_mark(X);
    D2X = polyder_mark(DX);
    D3X = polyder_mark(D2X);
    D4X = polyder_mark(D3X);
    
    Y = YY(i,:);
    DY = polyder_mark(Y);
    D2Y = polyder_mark(DY);
    D3Y = polyder_mark(D2Y);
    D4Y = polyder_mark(D3Y);
    
    Z = ZZ(i,:);
    DZ = polyder_mark(Z);
    D2Z = polyder_mark(DZ);
    D3Z = polyder_mark(D2Z);
    D4Z = polyder_mark(D3Z);
    
    C.dx = [C.dx; DX];
    C.d2x = [C.d2x; D2X];
    C.d3x = [C.d3x; D3X];
    C.d4x = [C.d4x; D4X];
    
    C.dy = [C.dy; DY];
    C.d2y = [C.d2y; D2Y];
    C.d3y = [C.d3y; D3Y];
    C.d4y = [C.d4y; D4Y];
    
    C.dz = [C.dz; DZ];
    C.d2z = [C.d2z; D2Z];
    C.d3z = [C.d3z; D3Z];
    C.d4z = [C.d4z; D4Z];
    
    T.x = [T.x polyval(X,time)];
    T.dx = [T.dx polyval(DX,time)];
    T.d2x = [T.d2x polyval(D2X,time)];
    T.d3x = [T.d3x polyval(D3X,time)];
    T.d4x = [T.d4x polyval(D4X,time)];
    
    T.y = [T.y polyval(Y,time)];
    T.dy = [T.dy polyval(DY,time)];
    T.d2y = [T.d2y polyval(D2Y,time)];
    T.d3y = [T.d3y polyval(D3Y,time)];
    T.d4y = [T.d4y polyval(D4Y,time)];
    
    T.z = [T.z polyval(Z,time)];
    T.dz = [T.dz polyval(DZ,time)];
    T.d2z = [T.d2z polyval(D2Z,time)];
    T.d3z = [T.d3z polyval(D3Z,time)];
    T.d4z = [T.d4z polyval(D4Z,time)];
        
end

T.d4xnorm = norm(T.d4x);
T.d4ynorm = norm(T.d4y);
T.d4znorm = norm(T.d4z);

%% plot result

if plotfigures
    % plot
    figure(1); clf;
    subplot(5,1,1)
    plot(T.t,T.x,T.t,T.y,T.t,T.z);
    ylabel('Position');
    legend('x','y','z');
    
    subplot(5,1,2)
    plot(T.t,T.dx,T.t,T.dy,T.t,T.dz);
    ylabel('Velocity');
    
    subplot(5,1,3)
    plot(T.t,T.d2x,T.t,T.d2y,T.t,T.d2z);
    ylabel('Acceleration');
    
    subplot(5,1,4)
    plot(T.t,T.d3x,T.t,T.d3y,T.t,T.d3z);
    ylabel('Jerk');
    xlabel('Time (s)');
    
    subplot(5,1,5)
    plot(T.t,T.d4x,T.t,T.d4y,T.t,T.d4z);
    ylabel('Snap');
    xlabel('Time (s)');
    
    
    
    figure(2);clf;
    hold all
    plot3(T.x,T.y,T.z,'LineWidth',2.5);
    hold on
    plot3(wp(1,:),wp(2,:),wp(3,:),'r*','MarkerSize',15);
    xlabel('X Position (m)','fontsize',14);
    ylabel('Y Position (m)','fontsize',14);
    zlabel('Z Position (m)','fontsize',14);
    axis([-2 2 -4 4])
    view(-90,90);
    legend('Path','Waypoints','Location','Best');
    grid on

    %     print -depsc path.ps
    
    
    % DEBUG - plot only x-y plane
    %     plot(T.x,T.y,'LineWidth',2.5);
    %     hold on
    %     plot(wp(1,:),wp(2,:),'r*','MarkerSize',15);
    %     xlabel('X Position (m)','fontsize',14);
    %     ylabel('Y Position (m)','fontsize',14);
    %     legend('Path','Waypoints','Location','Best');
    %     axis([-0.2 1.6 -5 1])
    %     printFigureToCroppedPdf(gcf,'path.pdf',[5.5 4],'inches');
    
    % DEBUG - plot only x-z plane
    %     plot(T.x,T.z,'LineWidth',2.5);
    %     hold on
    %     plot(wp(1,:),wp(3,:),'r*','MarkerSize',15);
    %     xlabel('X Position (m)','fontsize',14);
    %     ylabel('Z Position (m)','fontsize',14);
    %     legend('Path','Waypoints','Location','Best');
    %     axis([-0.2 1.6 -5 1])
    %     printFigureToCroppedPdf(gcf,'path.pdf',[5.5 4],'inches');
    
    
end

end % end polygen

function [Aeq,beq] = findX(wp,x0,xf,xm_acc,wpm_elm,x,n,m)

%% Form constraints
Aeq = zeros(m);

% initial conditions - pos, vel, acc, jerk, snap
A1 = zeros(n/2,n);
for k=0:n/2-1
    A1(k+1,n-k)   = factorial(k); % initial position constraint (assumed t0 = 0)
end
%         A1(1,n)   = 1; % initial position constraint (assumed t0 = 0)
%         A1(2,n-1) = 1; % initial velocity constraint
%         A1(3,n-2) = 2; % initial acceleration constraint
%         A1(4,n-3) = 6; % initial jerk constraint
%         A1(5,n-4) = 24;% initial snap constraint

% final conditions - pos, vel, acc, jerk, snap
An = zeros(n/2,n);
for k=1:n/2
    An(k,1:n-k+1) = x(k,k:n); %fill_poly(x(k,k:n),t(end));
end

% assign initial and final conditions to big A matrix
Aeq(1:n/2, 1:n) = A1;
Aeq(m-n/2+1:m, m-n+1:m) = An;

% middle continuity constraints and position constraints
for k=2:m/n
    if any(k == wpm_elm)
        cntr = 4;
        Aij = zeros(n,2*n);
        Aij(1,1:n)         =  x(1,:);% position constraint x1(t1) = x1_f
        Aij(2,n+1:2*n)     =  x(1,:);% position constraint x2(t1) = x2_0
        Aij(3,1:n-1)         =  x(2,2:n);% velocity constraint
        Aij(4,n+1:2*n-1)     =  x(2,2:n);% velocity constraint
        
%         for p=3:n
%             if p~=3
%                 Aij(cntr,1:n-p+2)     =  x(p-1,p-1:n);%fill_poly(x(p-1,p-1:n),t(k));% continuous velocity
%                 Aij(cntr,n+1:2*n-p+2) = -x(p-1,p-1:n);%-fill_poly(x(p-1,p-1:n),t(k));% continuous velocity
%                 cntr = cntr+1;
%             end
%         end
    else
        Aij = zeros(n,2*n);
        Aij(1,1:n)         =  x(1,:);%fill_poly(x(1,:),t(k));% position constraint x1(t1) = x1_f
        Aij(2,n+1:2*n)     =  x(1,:);%fill_poly(x(1,:),t(k));% position constraint x2(t1) = x2_0
        for p=3:n
            Aij(p,1:n-p+2)     =  x(p-1,p-1:n);%fill_poly(x(p-1,p-1:n),t(k));% continuous velocity
            Aij(p,n+1:2*n-p+2) = -x(p-1,p-1:n);%-fill_poly(x(p-1,p-1:n),t(k));% continuous velocity
        end
    end
    % add Aij to big A matrix
    Aeq(n/2+1+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n+n) = Aij;
end

beq = zeros(m,1);
beq(1) = wp(1);
beq(2:n/2) = x0';
cntr = 1;
for k=0:m/n-2
    if any(k == wpm_elm-2)
        beq(k*n+n/2+1) = wp(k+2);
        beq(k*n+n/2+2) = wp(k+2);
        beq(k*n+n/2+3) = xm_acc(cntr);
        beq(k*n+n/2+4) = xm_acc(cntr);
        cntr = cntr+1;
    else
        beq(k*n+n/2+1) = wp(k+2);
        beq(k*n+n/2+2) = wp(k+2);
    end
end
beq(m-n/2+1) = wp(m/n+1);
beq(m-n/2+2:m) = xf';

end

function out = fill_A(Aeq,beq,wpm_elm,n,m,t)

% fill Aeq
% final conditions
Aeq(m-n/2+1:m, m-n+1:m) = fill_poly(Aeq(m-n/2+1:m, m-n+1:m),t(end));
% intermediate conditions - no good way to explain the indexing,
% sorry!


for k=2:m/n
    if any(k == wpm_elm)
        Aeq(n/2+1+(k-2)*n, (k-2)*n+1:(k-1)*n)             = fill_poly(Aeq(n/2+1+(k-2)*n, (k-2)*n+1:(k-1)*n),t(k)); % first pos constraint
        Aeq(n/2+2+(k-2)*n, (k-1)*n+1:k*n)                 = fill_poly(Aeq(n/2+2+(k-2)*n, (k-1)*n+1:k*n),t(k)); % second pos constraint
        Aeq(n/2+3+(k-2)*n, (k-2)*n+1:(k-1)*n-1)           = fill_poly(Aeq(n/2+3+(k-2)*n, (k-2)*n+1:(k-1)*n-1),t(k)); % first vel constraint
        Aeq(n/2+4+(k-2)*n, (k-1)*n+1:k*n-1)               = fill_poly(Aeq(n/2+4+(k-2)*n, (k-1)*n+1:k*n-1) ,t(k)); % second vel constraint
%         Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n-3) = fill_poly(Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n-3),t(k));
%         Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-1)*n+1:k*n-3)     = fill_poly(Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-1)*n+1:k*n-3),t(k));
    else
        Aeq(n/2+1+(k-2)*n, (k-2)*n+1:(k-1)*n)             = fill_poly(Aeq(n/2+1+(k-2)*n, (k-2)*n+1:(k-1)*n),t(k));
        Aeq(n/2+2+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n) = fill_poly(Aeq(n/2+2+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n),t(k));
        Aeq(n/2+2+(k-2)*n:n/2+(k-1)*n, (k-1)*n+1:k*n)     = fill_poly(Aeq(n/2+2+(k-2)*n:n/2+(k-1)*n, (k-1)*n+1:k*n),t(k));
    end
end

out = Aeq\beq; % closed-form solution!

out = vec2mat(out,n);
end


function out = fill_poly(p,t)
% fill a polynomial vector with argument t

n = size(p,1);
m = size(p,2);
pow = zeros(n,m);
for i=1:n
    pow(i,1:m-i+1) = m-i:-1:0;
end
out = p.*bsxfun(@power,t,pow);

end

function dp = polyder_mark(p)

n = length(p);
pow = n-1:-1:0;
dp = zeros(1,n-1);
for i=1:n-1
    dp(i) = p(i)*pow(i);
end

end

