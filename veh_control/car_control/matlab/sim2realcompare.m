% Simulate data and compare to real data log for comparisons

function sim2realcompare

% clc; clear all;

%% IMPORT DATA
delay = 0;
data = parseData('carlog.txt', delay);
% data = parseData('carlogstraight2.txt', delay);
% data = parseData('carlogfull8.txt', delay);
% data = parseData('carlogfullslow2.txt', delay);
% data = parseData('carlogdrift8_crop.txt', delay);

compareToSim( data );

% Simulate from demo actions and compare
function compareToSim( demo )



% Simulate from demo data and plot
sim = CarSim();

sim.param.MaxWheelAngle = 18*pi/180.0;
% sim.param.B = 3;	% From initial optimization
% sim.param.C = 1.5;     % From initial optimization
% sim.param.D = 0.20;	% From initial optimization

sim.param.B = 3;	% From initial optimization
sim.param.C = 1.5;     % From initial optimization
sim.param.D = 0.2;	% From initial optimization

demo.param = sim.param;
demo_sim = demo;
tic
for i=2:length(demo_sim.t)
    sim.state = demo_sim.state(i-1);
    sim.action = demo_sim.action(i-1);
    dt = demo_sim.t(i)-demo_sim.t(i-1);
    sim = sim.simulate( sim, dt);
    
    wF = sim.state.omegaF;
    if isnan(wF)
        stop_here=1;
    end
    
    demo_sim.state(i) = sim.state;
    if ( demo_sim.state(i).Vy > 10)
        demo_sim.state(i).Vy
        break;
    end
end
toc

figure(1);clf;
plotData( demo , 1, '-', 2);
plotData( demo_sim , 1, '--', 2);

function SA = parseData( file, delay )
%% IMPORT DATA
logfile = importdata(file,',',1);

t = logfile.data(:,23);
x = logfile.data(:,5);
y = logfile.data(:,6);
qw = logfile.data(:,8); qx = logfile.data(:,9); qy = logfile.data(:,10); qz = logfile.data(:,11);
psi = -atan2(2.0*(qw.*qz + qx.*qy), 1.0 - 2.0*(qy.*qy + qz.*qz));
dpsi = logfile.data(:,18);
dx = logfile.data(:,13); dy = logfile.data(:,14);
Vx = dx.*cos(psi) - dy.*sin(psi);
Vy = dx.*sin(psi) + dy.*cos(psi);
omegaF = logfile.data(:,34);
omegaR = logfile.data(:,35);
throttle = logfile.data(:,2);
delta = logfile.data(:,3);

max(abs(omegaR))


% for i = 1:length(t)
%     if omegaF(i) > 120 || omegaF(i) < -120
%         omegaF(i) = 0;
%     end
%     if omegaR(i) > 120 || omegaR(i) < -120
%         omegaR(i) = 0;
%     end
% end

t = t - t(1);

for i=1:length(t)
    s.x = x(i); s.y = y(i); s.Vx = Vx(i); s.Vy = Vy(i);
    s.psi = psi(i); s.dpsi = dpsi(i); s.omegaF = omegaF(i); s.omegaR = omegaR(i);
%     if omegaR(i) < 5;
%         throttle(i) = 0.01;
%     end
    a.throttle = throttle(i);
    a.turn = delta(i);
    SA.state(i) = s;
    SA.action(i) = a;
    SA.t(i) = t(i);
end

% Account for command delay
for i=length(t):-1:delay+1
    SA.action(i) = SA.action(i-delay);
end


function plotData( SA , fig, style, width)

for i=1:length(SA.t)
    data.t(i) = SA.t(i);
    data.Vx(i) = SA.state(i).Vx;
    data.Vy(i) = SA.state(i).Vy;
    data.omegaR(i) = SA.state(i).omegaR;
    data.throttle(i) = SA.action(i).throttle;
    data.delta(i) = SA.action(i).turn;
    data.dpsi(i) = SA.state(i).dpsi;
    data.noslipVx(i) = SA.param.rR*SA.state(i).omegaR;
end

figure(fig);
subplot(2,1,1);hold on;
plot(data.t,data.Vx,['g' style],'linewidth', width)
plot(data.t,data.Vy,['b' style],'linewidth', width)
plot(data.t,data.omegaR./50,['r' style],'linewidth', width)
plot(data.t,data.throttle*5,['k' style],'linewidth', width)
% plot(data.t,data.noslipVx,['c' style],'linewidth', width)
legend('Vx','Vy', 'omegaR', 'throttle'); %, 'no slip Vx');
grid on;

subplot(2,1,2);hold on;
plot(data.t,-data.delta*4,['g' style],'linewidth', width); grid on;
plot(data.t,data.dpsi,['b' style],'linewidth', width);
legend('turn cmd', 'dpsi')

% %% Link x axes
% ax = [];
% for ii=1:length(f)
%     ax = [ax; get(f(ii),'children')];
% end
% linkaxes(ax,'x');
