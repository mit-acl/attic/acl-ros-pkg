clc; clear all;

XMAX = 3;
XMIN = -XMAX;
YMAX = 3;
YMIN = -7;
BOUND = 0.7;

XMAXSOFT = XMAX - BOUND;
XMINSOFT = XMIN + BOUND;
YMAXSOFT = YMAX - BOUND;
YMINSOFT = YMIN + BOUND;

R = 0.75;

xs = 0;
ys = 1;
b = rand;
if b > 0.5
    xf = XMINSOFT;
else
    xf = XMAXSOFT;
end
yf = rand*4 - 6;
thetaf = rand*2*pi - pi;

xf = 1;
yf = YMINSOFT;
thetaf = 0;

% find transition points
if xf > 0
    x1 = xs + 2*R;
    x2 = xs + 2*R;
    if thetaf < pi/2 && thetaf > -pi/2
        x3 = xs + R;
    else
        x3 = xs + 3*R;
    end
else
    x1 = xs - 2*R;
    x2 = xs - 2*R;
    if thetaf < pi/2 && thetaf > -pi/2
        x3 = xs - 3*R;
    else
        x3 = xs - R;
    end
end

y1 = ys;
y2 = yf + R;
y3 = yf;

d0 = 0:-0.01:(0-ys);
if xf > 0
    s1 = 0:0.01:pi;
    d2 = 0:0.01:(y1-y2);
    if thetaf < pi/2 && thetaf > -pi/2
        s3 = 3/2*pi:0.01:2*pi;
    else
        s3 = 3/2*pi:-0.01:pi;
    end
    d4 = 0:-0.01:(x3-xf);
else
    s1 = pi:-0.01:0;
    d2 = 0:0.01:(y1-y2);
    if thetaf < pi/2 && thetaf > -pi/2
        s3 = 3/2*pi:0.01:2*pi;
    else
        s3 = 3/2*pi:-0.01:pi;
    end
    d4 = 0:0.01:(x3-xf);
end

X0 = xs*ones(size(d0));
if xf > 0
    X1 = R*cos(s1) + xs + R;
    X2 = x2*ones(size(d2));
    X3 = R*cos(s3) + x3;
    X4 = xf + d4;
else
    X1 = R*cos(s1) + xs - R;
    X2 = x2*ones(size(d2));
    X3 = R*cos(s3) + x3;
    X4 = xf + d4;
end

Y0 = ys + d0;
Y1 = R*sin(s1) + ys;
Y2 = y2 + d2;
Y3 = R*sin(s3) + y2;
Y4 = yf*ones(size(d4));


figure(1); clf;
plot([YMIN,YMAX],[XMAX,XMAX],'k','LineWidth',4);
hold on;
plot([YMIN,YMAX],[XMIN,XMIN],'k','LineWidth',4);
plot([YMIN,YMIN],[XMIN,XMAX],'k','LineWidth',4);
plot([YMAX,YMAX],[XMIN,XMAX],'k','LineWidth',4);

plot([YMINSOFT,YMAXSOFT],[XMAXSOFT,XMAXSOFT],'k--','LineWidth',3);
plot([YMINSOFT,YMAXSOFT],[XMINSOFT,XMINSOFT],'k--','LineWidth',3);
plot([YMINSOFT,YMINSOFT],[XMINSOFT,XMAXSOFT],'k--','LineWidth',3);
plot([YMAXSOFT,YMAXSOFT],[XMINSOFT,XMAXSOFT],'k--','LineWidth',3);
set(gca,'XDir','reverse')
xlabel('Y (m)'); ylabel('X (m)')

plot([Y4, Y3, Y2, Y1, Y0],[X4, X3, X2, X1, X0]);