% test_plot_position_car180.m

clear all; clc;

load('car180.mat');

t = Time;
delta = Force(1,:);
torque = Force(2,:);
Vx = State(1,:); Vy = State(2,:); dpsi = State(3,:);
omegaF = State(4,:); omegaR = omegaF;
psi = State(5,:);
beta = atan2(Vy,Vx);
V = sqrt(Vx.^2 + Vy.^2);

x = zeros(size(Vx));
y = zeros(size(Vy));

for i=2:length(x)
    dx = Vx(i)*cos(psi(i)) - Vy(i)*sin(psi(i));
    dy = Vx(i)*sin(psi(i)) + Vy(i)*cos(psi(i));
    x(i) = x(i-1) + dx*(t(i)-t(i-1));
    y(i) = y(i-1) + dy*(t(i)-t(i-1));
end

rate_est = diff(psi)./diff(t);


%% subsample the control at 10Hz, zero order hold
delta_new = delta;
torque_new = torque;
dt = 0.1;
target_time = dt;
for i=2:length(delta_new)
    if t(i) < target_time
        delta_new(i) = delta_new(i-1);
        torque_new(i) = torque_new(i-1);
    else
        target_time = target_time + dt;
    end
end


%% test the input
sim_init = CarSim();
sim_init.state.Vx = Vx(1);
sim_init.state.psi = psi(1);
sim_init.state.omega = omegaF(1);

data = car_dynamics_propagate(t, delta_new, torque_new, sim_init.state);
n = length(data.t);

guess.phase.time = zeros(n,1);
guess.phase.state = zeros(n,size(State, 1));
guess.phase.control = zeros(n,size(Force, 1));
x_guess = zeros(n,1);
y_guess = zeros(n,1);
for i=1:n
    guess.phase.time(i)    = data.t(i);
    guess.phase.state(i,:) = [data.state(i).Vx, data.state(i).Vy, ...
        data.state(i).dpsi, data.state(i).omegaF, data.state(i).psi];
    guess.phase.control(i,:) = [data.action(i).turn, data.action(i).throttle];
    x_guess(i) = data.state(i).x;
    y_guess(i) = data.state(i).y;
    
    %     costIntergrandguess(i) = aVx*(data.state(i).Vx-sim.ss.Vx).^2 + ...
    %         aVy*(data.state(i).Vy-sim.ss.Vy).^2 + ...
    %         adpsi*(data.state(i).dpsi-sim.ss.dpsi).^2;
    %
    %     costIntergrandguess(i) = 1-exp(-costIntergrandguess(i));
end
% guess.phase.integral = sum(costIntergrandguess);

plot_gpops_car(guess.phase.time', guess.phase.state', ...
    guess.phase.control', 0, 0)
figure(6); clf;
plot(x_guess,y_guess)


figure(7); clf;
plot(x,y)
axis 'equal'

figure(8); clf;
plot(dpsi);
hold all
plot(rate_est);

plot_gpops_car(Time, State, Force, 0, 0)
figure(4)
subplot(211)
hold all
plot(t,delta_new)
subplot(212)
hold all
plot(t,torque_new)