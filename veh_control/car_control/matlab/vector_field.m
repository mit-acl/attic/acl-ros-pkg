% vector_field.m

function vector_field

% % create circle
% theta = -pi:0.01:pi;
% r = 1;
% 
% x = r*cos(theta);
% y = r*sin(theta);
% 
% % create vector field
% k = 4.5;
% d = 0.1:0.1:3;
% gamma = -pi:0.2:pi;
% 
% chi = zeros(length(d),length(gamma));
% x1 = zeros(size(chi));
% y1 = zeros(size(chi));
% u = zeros(size(chi));
% v = zeros(size(chi));
% for i=1:length(gamma)
%     chi(:,i) = gamma(i) - pi/2 - atan2(k*(d-r), r);
%     x1(:,i) = d*cos(gamma(i));
%     y1(:,i) = d*sin(gamma(i));
% end
% 
% u = cos(chi);
% v = sin(chi);







n = 4;
xcross = 0.2;
wp = [-xcross -xcross xcross xcross -xcross;...
       2 -2 -2 2 2;...
       0  0 0 0 0]; % waypoints
wp0 = [0;... % initial x vel
    -.5;... % initial y vel
    0];   % initial z vel
wpf = [0;... % final x vel
    -.5;... % final y vel
    0];   % final z vel
wpm_acc = [0 0 0;0 0 0;0 0 0]; % middle acceleration constraint
wpm_elm =[2 3 4]; % wp element that acceleration constraint will replace
sigma = [1 1 1 1];  % upright 1 or inverted 0 for that time segment
tt = [3 1 3 1]
%     tt = [.6 .6 0.6 0.4]; % time vector must have size(wp,2)-1 elements - each
% element is the time for that respective segment
res = 30;
plotfigures = true;

[T C] = polygen(wp,wp0,wpf,wpm_acc,wpm_elm,sigma,n,tt,res,plotfigures);


x = -0.5:0.05:0.5;
y = -2.3:0.05:2.3;

for i=1:length(x)
    for j=1:length(y)
        
        if y(j) > -2 && y(j) < 2
            if x(i) > 0
                seg = 3;
            else
                seg = 1;
            end
        elseif y(j) > 0
            seg = 4;
        else
            seg = 2;
        end
        px = C.x(seg,:);
        py = C.y(seg,:);
        t = C.t(seg):0.1:C.t(seg+1);
        
        if seg == 1
            chi(j,i) = get_vector_straight(x(i),y(j),px,py,t,-pi/2);
        elseif seg == 2
            chi(j,i) = get_vector_curve(x(i),y(j),px,py,t);
        elseif seg == 3
            chi(j,i) = get_vector_straight(x(i),y(j),px,py,t,pi/2);
        else
            chi(j,i) = get_vector_curve(x(i),y(j),px,py,t);
        end
    end
end

u = cos(chi);
v = sin(chi);



figure(1); clf;

quiver(x,y,u,v)
hold on
for i = 1:4
    px = C.x(i,:);
    py = C.y(i,:);
    t = C.t(i):0.1:C.t(i+1);
    x = polyval(px,t);
    y = polyval(py,t);
    plot(x,y,'linewidth',3)
end
% axis equal
view(-90,90);

% plot(x,y)
% hold all
% plot(x(ind),y(ind),'*')
% plot(xc,yc,'*')
% plot(x_act,y_act,'*')
% plot([x_act xc],[y_act,yc])
% plot(xa,ya)
% axis equal

function chi = get_vector_straight(x_act,y_act,px,py,t,nom_chi)

x = polyval(px,t);
y = polyval(py,t);

ind = find_closest_point(x_act,y_act,x,y);

xerr = x_act - x(ind);

chi_inf = 75*pi/180;

k = 4.0;
chi = chi_inf*2/pi*atan(k*xerr);
if nom_chi < 0
    chi = -chi;
end
chi = chi + nom_chi;


function chi = get_vector_curve(x_act,y_act,px,py,t)

x = polyval(px,t);
y = polyval(py,t);
dx = polyval(polyder(px),t);
dy = polyval(polyder(py),t);
ddx = polyval(polyder(polyder(px)),t);
ddy = polyval(polyder(polyder(py)),t);

ind = find_closest_point(x_act,y_act,x,y);

dxk = dx(ind);
dyk = dy(ind);
ddxk = ddx(ind);
ddyk = ddy(ind);
k = (dxk*ddyk - dyk*ddxk)/(dxk^2 + dyk^2)^(3/2);
r = abs(1/k);

% tangent direction
theta = atan2(dyk,dxk);
gamma = theta+pi/2;

if k < 0
    xc = -r*cos(gamma);
    yc = -r*sin(gamma);
else
    xc = r*cos(gamma);
    yc = r*sin(gamma);
end

xc = xc + x(ind);
yc = yc + y(ind);

d = dist2(x_act,y_act,xc,yc);

K = 3.5;
chi = gamma - pi/2 + atan2(K*(d-r),r);

function ind = find_closest_point(x_act,y_act,x,y)

minD = Inf;
ind = 0;
for i=1:length(x)
    d = dist2(x_act,y_act,x(i),y(i));
    if d < minD
        ind = i;
        minD = d;
    end
end

function d = dist2(x1,y1,x2,y2)

d = sqrt((x2-x1)^2 + (y2-y1)^2);