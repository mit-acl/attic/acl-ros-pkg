function dw = wheelspeed(t,w,f_x,Tt,T)

MaxWheelSpeed = 352; 
BackEMFGain_up = 9.0; 
BackEMFGain_down = 1.4;
Tmin = 0.05; %0.12;
Iw = 0.8*3.2e-5;
r = 0.0606 / 2.0;

if nargin > 3
    T = interp1(Tt,T,t);
else
    T = t;
end

K = TorqueGain(T, Tmin);

K = K./T;
K(isinf(K))=0;
K(isnan(K))=0;

dw = K.*BackEMFGain_up.*T - BackEMFGain_up.*w - 1/Iw*f_x.*r;

for i=1:length(dw)
    if w(i) > MaxWheelSpeed && dw(i) > 0
        dw(i) = 0;
    end

    if dw(i) < 0
        dw(i) = K(i)*BackEMFGain_down*T(i) - BackEMFGain_down*w(i) - 1/Iw*f_x(i)*r;
    end
end


function K = TorqueGain(t_array, t_min)

K = zeros(size(t_array));
for i=1:length(t_array)
    t = t_array(i);
    if t > 0.55
        K(i) = 75*t + 290;
    else
        K(i) = 3.79e3*t^3 - 5.41e3*t^2 + 2.66e3*t - 1.29e2;
    end

    if t < t_min
        K(i) = 0;
    end
end