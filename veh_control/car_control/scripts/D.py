#!/usr/bin/env python
'''
Description: Simulation of car dynamics using a fixed input u
Needed for implemented ILC algorithm from this paper:
"Iterative Learning of Feed-Forward Corrections for
High-Performance Tracking" by
Fabian L. Mueller, Angela P. Schoellig and Raffaello DAndrea

Created on Oct 21, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('car_control')
import rospy
import numpy as np
import math
import sys
import copy

# ros imports
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist

# custom imports
import projectOnPath
import turn_controller
import polyPath
import params
from car_control.srv import SingleSegment
from car_control.srv import ResetCar
from car_control.srv import ReturnToBase
from car_control.msg import SimpleState
from car_sim.msg import CarState
from car_sim.srv import RunStep
from car_control.msg import PolyInputs
from aclpy import utils


#==============================================================================
# Class for implementing a service call to run a track segment
#==============================================================================
class RunSim:

    def __init__(self):

        self.p = projectOnPath.projectOnPath()

        self.current_state = CarState()

        self.tc = turn_controller.TurnController()


#-----------------------------------------------------------------------------
# Run a single integration step of the car simulator
#-----------------------------------------------------------------------------
    def run_step(self, state, dt, omegaDes, turn, vis):
#         rospy.wait_for_service('/RC02s/run_step')
        try:
            run_step = rospy.ServiceProxy('/RC02s/run_step', RunStep)
            resp1 = run_step(state, dt, omegaDes, turn, vis)
            return resp1.finalState
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e

#-----------------------------------------------------------------------------
# run a single segment of the sim and return the time
#-----------------------------------------------------------------------------
    def run_sim(self, path):

        N = len(path)

        ybar = np.zeros((7, N))
        cbar = np.zeros((2, N))

        self.current_state = self.initState()
        self.updateCBs()
        self.tc.verr_int = 0
        self.tc.t_prev = rospy.Time.now().to_sec()
        self.tc.delta_prev = 0
        iterCnt = 0
        sampleCnt = 0
        startTime = 0

        self.p.createBarePath(path)

        simTime = copy.copy(startTime)
        old_y = -np.Inf

#         print self.p.percent_complete
        while(sampleCnt < N):
#             print "simTime: " + str(simTime - startTime)
#            print self.current_state

            # run controller at CNTRL_RATE
            if np.mod(iterCnt, params.CNTRL_RATE / params.DT) == 0:
                # get current goal and run controller
                self.p.findGoalCallback(0)
                self.tc.goalCB(self.p.goal)  # "broadcast" goal to tc

            # run step
            omegaDes = self.tc.cmd.data[1]
            turn = self.tc.cmd.data[2]
            self.current_state = self.run_step(self.current_state, params.DT, omegaDes, turn, True)

            # update turn_controller and path projection callbacks
            if np.mod(iterCnt, params.CNTRL_RATE / params.DT) == 0:
                self.updateCBs()

            # keep important variables
            if np.mod(iterCnt, params.SAMPLE_RATE / params.DT) == 1:
                ybar[0, sampleCnt] = self.current_state.pose.position.x
                ybar[1, sampleCnt] = self.current_state.pose.position.y
                ybar[2, sampleCnt] = self.current_state.Vx
                ybar[3, sampleCnt] = self.current_state.Vy
                ybar[4, sampleCnt] = utils.quat2yaw(self.current_state.pose.orientation)
                ybar[5, sampleCnt] = self.current_state.r
                ybar[6, sampleCnt] = self.current_state.omegaR

                cbar[0, sampleCnt] = turn
                cbar[1, sampleCnt] = omegaDes

                sampleCnt += 1

            iterCnt += 1
            simTime += params.DT


        return ybar, cbar  # time expired



#-----------------------------------------------------------------------------
# Path percent complete callback
#-----------------------------------------------------------------------------
    def path_percentCB(self, data):
        self.p.percent_complete = data.data



#-----------------------------------------------------------------------------
# Update turn controller and path projection callbacks, giving them access
# to the current state data
#-----------------------------------------------------------------------------
    def updateCBs(self):
        # get psi
        q0 = self.current_state.pose.orientation.w
        q1 = self.current_state.pose.orientation.x
        q2 = self.current_state.pose.orientation.y
        q3 = self.current_state.pose.orientation.z
        psi = math.atan2(2 * (q0 * q3 + q1 * q2),
                              1 - 2 * (q2 ** 2 + q3 ** 2))

        dx = self.current_state.Vx * np.cos(psi) - self.current_state.Vy * np.sin(psi)
        dy = self.current_state.Vx * np.sin(psi) + self.current_state.Vy * np.cos(psi)
        p = PoseStamped()
        p.pose = self.current_state.pose
        t = TwistStamped()
        t.twist.linear.x = dx
        t.twist.linear.y = dy
        t.twist.angular.z = self.current_state.r
        self.p.poseCallBack(p)
        self.p.velCallBack(t)
        self.tc.poseCB(p)
        self.tc.velCB(t)

#-----------------------------------------------------------------------------
# Initialize state
#-----------------------------------------------------------------------------
    def initState(self):
        s = CarState()
        s.pose.position.x = -0.8 + params.XCENTER
        s.pose.position.y = params.Y1
        psi = -np.pi / 2.0
        s.pose.position.z = 0
        s.pose.orientation.w = np.cos(psi / 2.0)
        s.pose.orientation.x = 0
        s.pose.orientation.y = 0
        s.pose.orientation.z = np.sin(psi / 2.0)
        s.Vx = 0
        s.Vy = 0
        s.r = 0
        v = np.sqrt(s.Vx ** 2 + s.Vy ** 2)
        s.omegaF = s.omegaR = v / params.RADIUS  # clearly an approximation

        return s


if __name__ == '__main__':
    ns = rospy.get_namespace()
    try:
        rospy.init_node('single_segment_server')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control D.py")
        else:
            c = RunSim()
            c.run_sim()


    except rospy.ROSInterruptException:
        pass
