#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
import os
import sys
import numpy as np
import random
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist

# local imports
import polyPath
from car_control.srv import SingleTrack
from car_control.srv import ReturnToBase

# TODOs:
# make sure when you get the high level the second time you can still try all
#    of the actions you did the first time around

# new UCB algorithm:
#    - use min (cap+eps,xbar) - DONE
#    - ensure cap gets transferred between sims and xbar gets reset - DONE
#    - keep track of which action gets run at which level - DONE
#    - don't run an action that was run a level above you
#    - increase or ignore out of bounds for simulated trials - DONE

# translate path by meter to the right - DONE
# reset function is just straight lines with constant velocity - DONE
# pause function - DONE
# test UCB with better initialization - DONE
# run joystick continuously - DONE
# better car
# battery info from car
# better wheel speed estimation
# log pertinent data -- what data is pertinent? - DONE


#==============================================================================
# Implements the UCB vanilla algorithm on the car simulator
#==============================================================================
class UCB:

#-----------------------------------------------------------------------------
# Initialize class
#-----------------------------------------------------------------------------
    def __init__(self, simType, prevSimFile='none', logFile='none',
                 nextSimFile='none'):

        if simType == 'true' or simType == 'True' or simType == '1':
            simType = 'midsim'
        else:
            simType = 'realcar'

        # TODO: make this file relative to ROS package location
        packagePath = roslib.packages.get_dir_pkg('car_control')
        # this doesn't work from launchfile
        packagePath = '/home/mark/acl-ros-pkg/veh_control/car_control'

        # number of laps to average each run
        self.num_laps = 2

        self.logFile = open(packagePath + '/log/UCBlog_' + simType + '.npz',
                            'w')
        self.simModel = open(packagePath + '/log/UCB_' + simType + '.npz', 'w')

        # Previous sim file
        try:
            with open(packagePath + '/log/' + prevSimFile) as prevSimFile:
                npzfile = np.load(prevSimFile)
                self.t13 = npzfile['t13']
                self.t24 = npzfile['t24']
                self.xcross = npzfile['xcross']
                self.minTime = npzfile['minTime']
                self.prevSimReward = npzfile['xbar']
                self.xbar = self.prevSimReward.copy()
                self.possibleActions = npzfile['act_tried']
                print "possible actions: " + str(self.possibleActions)
                self.epsilon = npzfile['epsilon']
                rospy.loginfo('Successfully opened previous sim file %s',
                              prevSimFile)

                # initialize n based on xbar
                self.n = np.ones((len(self.t13), len(self.t24),
                                       len(self.xcross)))
                self.n[self.xbar == 0] = 0
#                 # account for one dimensional possible actions (just a single action)
#                 if self.possibleActions.ndim == 1:
#                     self.possibleActions = np.array([self.possibleActions])
#
#                 for i in range(self.possibleActions.shape[0]):
#                     self.n[self.possibleActions[i, 0], self.possibleActions[i, 1],
#                            self.possibleActions[i, 2]] = 1
#                 self.n[self.xbar == 0] = 0  # zero any actions that got zero
#                 # reward the level below where we currently are

        except IOError:
            rospy.logerr('No existing previous simulation file to open')
            return

        # Log file for current sim
        try:
            with open(packagePath + '/log/' + logFile) as logFile:
                npzfile = np.load(logFile)
                self.n = npzfile['n']
                self.xbar = npzfile['xbar']
                self.iternum = npzfile['iter']
                self.iternum += 1  # new iteration will be one greater than last
                # successfully run iteration
                self.actions_tried = npzfile['act_tried']
                rospy.loginfo('Successfully opened log file %s', logFile)
        except IOError:
            rospy.loginfo('No existing log file to open, starting this algorithm from scratch')
            self.actions_tried = np.array([])
            self.iternum = 0

        # File from actions run at the next level up
        try:
            with open(packagePath + '/log/' + nextSimFile) as nextSimFile:
                npzfile = np.load(nextSimFile)
                actions_tried_higher_up = npzfile['act_tried']
                # prune these actions from the higher level from the list of
                # possible actions
                print "possible actions: " + str(self.possibleActions)
                for i in range(actions_tried_higher_up.shape[0]):
                    row = np.where((self.possibleActions ==
                                    actions_tried_higher_up[i, :]).all(axis=1))
                    if row[0].size == 1:
                        self.possibleActions = np.delete(self.possibleActions,
                                                         row[0][0], 0)
                print "possible actions after pruning: " + str(self.possibleActions)

                rospy.loginfo('Successfully opened log file %s', nextSimFile)
        except IOError:
            rospy.loginfo('No existing next sim file to open')
#             self.actions_tried_higher_up = np.array([])


#-----------------------------------------------------------------------------
# Run the UCB algorithm
# Implemented from Thm 1 at http://lane.compbio.cmu.edu/courses/slides_ucb.pdf
#-----------------------------------------------------------------------------
    def runUCB(self):

#         ## DEBUG!!!
#         p0 = Pose()
#         t0 = Twist()
#         pf = Pose()
#         pf.position.x = 1
#         pf.position.y = 2
#         tf = Twist()
#         tf.linear.x = 1
#         tf.linear.y = -1
#
#         self.returnToBase(p0, t0, pf, tf)
#         return 0

#         n = self.n[self.possibleActions[:, 0], self.possibleActions[:, 1], self.possibleActions[:, 2]]
#         xbar = self.xbar[self.possibleActions[:, 0], self.possibleActions[:, 1],
#                    self.possibleActions[:, 2]]
#         cap = self.prevSimReward[self.possibleActions[:, 0], self.possibleActions[:, 1],
#                    self.possibleActions[:, 2]] + self.epsilon

        self.epsilon = 0.05
        self.variance = self.n * self.epsilon
        cap = self.prevSimReward + self.epsilon

        for i in range(self.iternum, 1000):
            self.iternum = i
            print "\n\nIteration number: " + str(i)

#             # IE variance
#             delta = 0.3
#             zdelta = np.sqrt(0.5 * np.log(2 / delta))
#             var = zdelta / np.sqrt(self.n)


#             print self.possibleActions[:, 0], self.possibleActions[:, 1], self.possibleActions[:, 2]

            nsum = np.sum(self.n)

            if self.n[np.nonzero(self.n)].size == 1:
                rospy.logwarn("Only one action left to take.")

            if nsum == 0:
                rospy.logerr("No more actions to take!")
                return

            # UCB variance
            var = np.sqrt(2 * np.log(nsum) / self.n)

            # DEBUG!
            # making initial variance equal to epsilon
            R1 = self.xbar + self.variance  # var
            R1[R1 == np.Inf] = 0
            R = np.minimum(R1, cap)
#             R = R1
            print "R1"
            print R1
            print "R"
            print R
#             note: using masked_invalid array to deal with dividing by zero
#             from the pruned actions
            a = np.ma.masked_invalid(R).argmax()  # flattened index
            a = np.unravel_index(a, self.n.shape)  # unravel index
#             a = self.possibleActions[index, :]
            print "a: " + str(a)

            # check if action is one that we allowed to try at this level
            if self.possibleActions.size:
                row = np.where((self.possibleActions == a).all(axis=1))
                if row[0].size != 1:
                    rospy.logwarn("Attempting to select an action that shouldn't be tried here.  Quitting!\n")
                    return
            else:
                rospy.logerr("There are no possible actions.  This shouldn't happen.  Quitting!\n")

            # get new action
            xcross = self.xcross[a[2]]
            t13 = self.t13[a[0]]
            t24 = self.t24[a[1]]

            # if in real-time mode call service to get to the starting location
            # first, figure out where and at what speed you should be starting
            # the next track

            # do action -- call service request
            reward = self.getReward(xcross, t13, t24)
#             print "\n\nReturned reward: " + str(reward) + "\n\n"

            print "actions tried: " + str(self.actions_tried)
            # out of bounds or time expired (probably track too slow)
            if self.actions_tried.size:
                row = np.where((self.actions_tried == a).all(axis=1))
            else:
                row = [np.array([])]
            if reward == -1 or reward == -2:
                print "reward: " + str(reward)
                self.xbar[a] = 0  # this node should be pruned, sim didn't
                                    # finish
                self.n[a] = 0
            else:  # good reward
                print "reward: " + str(reward)
                reward = self.minTime / reward
                print "normalized reward: " + str(reward)
#                 update n and xbar
                if row[0].size == 1:  # n[index] == 1:
                    self.xbar[a] = ((self.xbar[a] *
                                                    self.n[a] +
                                                    reward) /
                                                   (self.n[a] + 1))
                    self.n[a] += 1  # only increment n after the first time you run it
                else:
                    self.xbar[a] = reward

            # update actions tried
            if row[0].size == 0:
                self.variance[a] = 0
                if self.actions_tried.size:
                    self.actions_tried = np.append(self.actions_tried,
                                                   np.array([a]), 0)
                else:
                    self.actions_tried = np.array([a])

            # update self.n and self.xbar
#             self.n[a] = n[index].copy()
#             self.xbar[a] = xbar[index].copy()

            print "n: " + str(self.n)
#             print "self.n: " + str(self.n)
#             print "a: " + str(a)
#             print "self.n[a]: " + str(self.n[a])
            print "xbar: " + str(self.xbar)
#             print "self.xbar: " + str(self.xbar)

            # save to file
            self.write2file()

#-----------------------------------------------------------------------------
# Write data to file -- gets called every iteration of the UCB algorithm
#-----------------------------------------------------------------------------
    def write2file(self):
        self.logFile.seek(0)
        np.savez(self.logFile, n=self.n, xbar=self.xbar, iter=self.iternum,
                 act_tried=self.actions_tried)


#         print "actions tried index: " + str(self.actions_tried_index)
#         if self.actions_tried_index.size:
#             actions_tried = self.possibleActions[self.actions_tried_index, :]
#         else:
#             actions_tried = np.array([])
        print "actions_tried: " + str(self.actions_tried)
        # write sim model to file
        self.simModel.seek(0)
        np.savez(self.simModel, t13=self.t13, t24=self.t24,
                  xcross=self.xcross, xbar=self.xbar,
                  act_tried=self.actions_tried, minTime=self.minTime,
                  epsilon=0.9)

#-----------------------------------------------------------------------------
# Run a single track circuit of the car simulator
#-----------------------------------------------------------------------------
    def getReward(self, xcross, t13, t24):
        rospy.wait_for_service('/RC01/single_track')
        try:
            # DEBUG!!!
#             xcross = 0.2
#             t13 = 2.5
#             t24 = 0.75
#             print "\nStarting single track service request"
#             print "xcross: " + str(xcross)
#             print "t13: " + str(t13)
#             print "t24: " + str(t24)
#             print "\n"
            st = rospy.ServiceProxy('/RC01/single_track', SingleTrack)
            resp = st(xcross, t13, t24, self.num_laps)
            reward = resp.time
            return reward
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e

#-----------------------------------------------------------------------------
# Return the car to a specific base
#-----------------------------------------------------------------------------
    def returnToBase(self, p0, t0, pf, tf):
        rospy.wait_for_service('/RC01/return_to_base')
        try:
            r2b = rospy.ServiceProxy('/RC01/return_to_base', ReturnToBase)
            resp = r2b(p0, t0, pf, tf)
            return resp.success

        except rospy.ServiceException, e:
            print "Service call failed: %s" % e


if __name__ == '__main__':
    try:
        rospy.init_node('UCB_vannila')
        argv = rospy.myargv(argv=sys.argv)
        if len(argv) == 2:
            c = UCB(argv[1])
        elif len(argv) == 3:
            c = UCB(argv[1], argv[2])
        elif len(argv) == 4:
            c = UCB(argv[1], argv[2], argv[3])
        elif len(argv) == 5:
            c = UCB(argv[1], argv[2], argv[3], argv[4])
        else:
            rospy.logerr("Incorrect number of input parameters specified")

        c.runUCB()
    except rospy.ROSInterruptException:
        pass
