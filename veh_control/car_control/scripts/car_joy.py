#!/usr/bin/env python
import rospy
import sys
import math
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray
from acl_msgs.msg import CarGoal
from acl_msgs.msg import JoyDef

PI = 3.14159
NOT_DRIVING = 0
THROTTLE = 1
SIMPLE_CONTROLLER = 2
WHEEL_SPEED = 3
RESET = 4
TURN_CONTROLLER = 5
CHIRP = 6
STEP = 7


class CarJoy:

    cmd = Float64MultiArray()

    def __init__(self, joytype):
        self.throttle = 0
        self.turn = 0
        self.transmitting = True
        self.status = NOT_DRIVING
        self.joytype = joytype
        self.pub = rospy.Publisher('carCmd', Float64MultiArray, queue_size=1)
        self.pubGoal = rospy.Publisher('goal', CarGoal, queue_size=1)
        self.goal = CarGoal()

        # chirp data
        self.chirp_f0 = 6  # .1
        self.chirp_ff = 10
        self.chirp_frate = 0.2
        self.chirp_min_throttle = 0.13

        self.chirp_f = self.chirp_f0
        self.chirp_throttle = 0
        self.chirp_start_time = 0

        self.step_max_throttle = 0.6
        self.step_min_throttle = 0.13
        self.step_start_time = 0

    def joyCallBack(self, data):

        if joytype.lower() == "rc":
            if data.axes[3] > 0:
                self.status = WHEEL_SPEED
            else:
                self.status = THROTTLE

            self.throttle = data.axes[2]
            self.turn = -data.axes[0] * 1.2

            if self.status == WHEEL_SPEED:
                self.cmd.data = [self.status, self.throttle * 500, self.turn]
            else:
                self.cmd.data = [self.status, self.throttle, self.turn]

        else:  # Default is xbox controller
            # get throttle and turn commands from joystick, then publish them
            if data.buttons[JoyDef.A]:
                self.status = THROTTLE
            if data.buttons[JoyDef.B]:
                self.status = NOT_DRIVING
            # if data.buttons[JoyDef.X]:
                # self.status = SIMPLE_CONTROLLER
            # if data.buttons[JoyDef.Y]:
            #     self.status = WHEEL_SPEED
            if data.buttons[JoyDef.CENTER]:
                self.status = RESET
            if data.buttons[JoyDef.RB]:
                self.status = TURN_CONTROLLER
#             if data.buttons[START]:
#                 self.status = CHIRP
#                 self.chirp_start_time = rospy.Time.now()
#             if data.buttons[END]:
#                 self.status = STEP
#                 self.step_start_time = rospy.Time.now()

            self.throttle = data.axes[JoyDef.LEFT_Y]
            self.turn = -data.axes[JoyDef.RIGHT_X]
            if self.status == SIMPLE_CONTROLLER:
                self.cmd.data = [self.status, self.throttle * 2.0, -self.turn * 3.14]  # in this case you are commanding velocity and heading
            elif self.status == WHEEL_SPEED:
                self.cmd.data = [self.status, self.throttle * 345, self.turn]
            elif self.status == TURN_CONTROLLER:
                self.goal.v = self.throttle * 4.0  # max speed is 4 m/s
                self.goal.r = -self.turn * 2.5
                self.goal.psi = -self.turn * math.pi
                self.goal.header.stamp = rospy.Time.now()
                self.pubGoal.publish(self.goal)
                return
            else:
                self.cmd.data = [self.status, self.throttle, self.turn]

        if self.status == CHIRP:
            t = rospy.Time.now().to_sec() - self.chirp_start_time.to_sec()
            self.chirp_f = t*self.chirp_frate + self.chirp_f0
            if self.chirp_f > self.chirp_ff:
                self.status = NOT_DRIVING
            self.throttle = math.sin(t*self.chirp_f-math.pi/2.0)
            self.throttle += 1
            diff = 1-self.chirp_min_throttle
            self.throttle /= 2.0
            self.throttle *= diff
            self.throttle += self.chirp_min_throttle
            self.cmd.data = [THROTTLE, self.throttle, self.turn]

        if self.status == STEP:
            t = rospy.Time.now().to_sec() - self.step_start_time.to_sec()
            self.throttle = self.step_min_throttle
            delay = 5
            if t > delay and t < 2*delay:
                self.throttle = self.step_max_throttle*0.2
            if t > 3*delay and t < 4*delay:
                self.throttle = self.step_max_throttle*0.4
            if t > 5*delay and t < 6*delay:
                self.throttle = self.step_max_throttle*0.6
            if t > 7*delay and t < 8*delay:
                self.throttle = self.step_max_throttle*0.8
            if t > 9*delay and t < 10*delay:
                self.throttle = self.step_max_throttle*1.0
            if t > 11*delay and t < 12*delay:
                self.throttle = self.step_max_throttle*1.2
            if t > 13*delay and t < 14*delay:
                self.throttle = self.step_max_throttle*1.4
            if t > 15*delay and t < 16*delay:
                self.throttle = self.step_max_throttle*1.6
            if t > 17*delay and t < 18*delay:
                self.throttle = self.step_max_throttle*1.8
            if t > 19*delay:
                self.status = NOT_DRIVING

            self.cmd.data = [THROTTLE, self.throttle, self.turn]

        if self.status == NOT_DRIVING:
            self.cmd.data = [0, 0, 0]

        # stop transmitting data from joystick
        if data.buttons[JoyDef.BACK] and self.transmitting:
            self.transmitting = False
            self.status = NOT_DRIVING
            self.cmd.data = [0, 0, 0]
            for i in range(3):
                self.pub.publish(self.cmd)

        # start transmitting data from joystick
        if data.buttons[JoyDef.START] and not self.transmitting:
            self.status = THROTTLE
            self.throttle = 0
            self.turn = 0
            self.transmitting = True

        if self.transmitting:
            self.pub.publish(self.cmd)


def joyListen(joytype):
    c = CarJoy(joytype)
    rospy.Subscriber("/joy", Joy, c.joyCallBack)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('car_joy')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control car_joy.py xbox")
        else:
            if len(sys.argv) < 2:
                print "Error: Specify either \"xbox\" or \"rc\" for joystick type after name argument"
            else:
                joytype = sys.argv[1]
                print "Starting joystick teleop node for: " + ns
                joyListen(joytype)
    except rospy.ROSInterruptException:
            pass
