#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
import sys
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray
from car_sim.msg import CarState
from car_sim.srv import RunStep

PI = 3.14159
NOT_DRIVING = 0
THROTTLE = 1
SIMPLE_CONTROLLER = 2
WHEEL_SPEED = 3
RESET = 4
A = 0
B = 1
X = 2
Y = 3
CENTER = 8

class CarJoy:
    
    cmd = Float64MultiArray()
    
    def __init__(self,name,joytype):
        self.throttle = 0
        self.turn = 0
        self.status = NOT_DRIVING
        self.name = name
        self.joytype = joytype
        self.pub = rospy.Publisher(self.name + '/carCmd', Float64MultiArray)
        self.currentState = CarState()
        self.timeOld = rospy.get_time()
    
    def run_step(self, dt, omegaDes, turn, vis):
        
        rospy.wait_for_service('/RC01/run_step')
        try:
            run_step = rospy.ServiceProxy('/RC01/run_step', RunStep)
            resp1 = run_step(self.currentState,dt,omegaDes, turn, vis)
            self.currentState = resp1.finalState
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
    
    def joyCallBack(self, data):
        
        if joytype.lower() == "rc":
            if data.axes[3] > 0:
                self.status = WHEEL_SPEED
            else:
                self.status = THROTTLE

            self.throttle = data.axes[2]
            self.turn = -data.axes[0]*1.2

            if self.status == WHEEL_SPEED:
                self.cmd.data = [self.status, self.throttle*500, self.turn]
            else:
                self.cmd.data = [self.status, self.throttle, self.turn]

        else: # Default is xbox controller
            # get throttle and turn commands from joystick, then publish them
            if data.buttons[A]:
                self.status = THROTTLE
            if data.buttons[B]:
                self.status = NOT_DRIVING
            if data.buttons[X]:
                self.status = SIMPLE_CONTROLLER
            if data.buttons[Y]:
                self.status = WHEEL_SPEED
            if data.buttons[CENTER]:
                self.status = RESET

            self.throttle = data.axes[1]
            self.turn = -data.axes[3]
            if self.status == SIMPLE_CONTROLLER:
                self.cmd.data = [self.status, self.throttle*2.0, -self.turn*3.14] # in this case you are commanding velocity and heading
            elif self.status == WHEEL_SPEED:
                self.cmd.data = [self.status, self.throttle*500, self.turn]
                t = rospy.get_time()
                dt = t - self.timeOld
                self.timeOld = t
#                 print dt
                self.run_step(dt,self.throttle*500, self.turn, True)
#                 self.run_step(dt, 20, .2, True)
            else:
                self.cmd.data = [self.status, self.throttle, self.turn]


        self.pub.publish(self.cmd)



def joyListen(name,joytype):
    rospy.init_node('car_joy')
    c = CarJoy(name,joytype)
    rospy.Subscriber("joy", Joy, c.joyCallBack)
    rospy.spin()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Error: need to specify vehicle name as input argument"
    else:
        vehName = sys.argv[1]
	if len(sys.argv) < 3:
	    print "Error: Specify either \"xbox\" or \"rc\" for joystick type after name argument"
	else:
	    joytype = sys.argv[2]
	    print "Starting joystick teleop node for: " + vehName
	    try:
		    joyListen(vehName,joytype)
	    except rospy.ROSInterruptException:
		    pass
