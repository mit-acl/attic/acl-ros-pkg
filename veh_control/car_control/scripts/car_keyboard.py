#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
import sys, select, termios, tty

msg = """
Reading from the keyboard  and Publishing to carCmd
---------------------------
Moving around:
   u    i    o
   j    k    l
   m    ,    .

Reset simulation:
   r

anything else : stop

CTRL-C to quit
"""

NOT_DRIVING = 0
# THROTTLE = 1
# SIMPLE_CONTROLLER = 2
WHEEL_SPEED = 3
RESET = 4

moveBindings = {
		'i':(1, 0),
		'o':(1, 1),
		'j':(0, -1),
		'l':(0, 1),
		'u':(1, -1),
		',':(-1, 0),
		'.':(-1, -1),
		'm':(-1, 1),
	       }

statusBindings = {
		'r':(RESET),
	       }

def getKey():
	tty.setraw(sys.stdin.fileno())
	select.select([sys.stdin], [], [], 0)
	key = sys.stdin.read(1)
	termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
	return key

class CarKeyboard():

    cmd = Float64MultiArray()

    def __init__(self, name):
        self.throttle = 0
        self.turn = 0
        self.name = name;
        self.pub = rospy.Publisher(self.name + '/carCmd', Float64MultiArray)

    def sendCmd(self):
        self.cmd.data = [self.status, self.throttle, self.turn]
        self.pub.publish(self.cmd)


if __name__ == '__main__':
    settings = termios.tcgetattr(sys.stdin)
    if len(sys.argv) != 2:
        print "Error: need to specify vehicle name as input argument"
    else:
        vehName = sys.argv[1]
        print "Starting keyboard teleop node for: " + vehName

        try:
            c = CarKeyboard(vehName)
            rospy.init_node('car_keyboard')

            print msg
            cnt = 0
            while(1):
                key = getKey()
                if key in moveBindings.keys():
                    c.throttle = moveBindings[key][0]
                    c.turn = moveBindings[key][1]
                    c.status = THROTTLE
                elif key in statusBindings.keys():
                    c.throttle = 0
                    c.turn = 0
                    c.status = statusBindings[key]
                else:
                    c.throttle = 0
                    c.turn = 0
                    if (key == '\x03'):
                        break
                s = 'Throttle: ' + str(c.throttle) + ', Turn: ' + str(c.turn)
                print s
                if cnt > 20:
                    print msg
                    cnt = 0
                cnt += 1
                c.sendCmd()

        except rospy.ROSInterruptException:
            pass
