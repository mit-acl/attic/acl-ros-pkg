#!/usr/bin/env python

###################################################
# hardware_gpops.py -- run gpops openloop policy on car
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday, 26 June 2015.
###################################################

import rospy
import scipy.io
from std_msgs.msg import Float64MultiArray


class Rollout:

    def __init__(self):
        self.throttle = 0
        self.turn = 0
        self.status = 0
        self.start_time = 0
        self.ind = 0
        self.max_int = 0
        self.pub = rospy.Publisher('carsimCmd', Float64MultiArray,
                                   queue_size=1)

    def load_params(self):
        gpops = scipy.io.loadmat('/home/mark/gaussian-process/pilcoV0.9/scenarios/car/training_car.mat')
        self.t = gpops['Time'][0]
        self.u = gpops['Force'][0]

        print
        print
        print self.u
        print
        print

        self.status = 1
        self.start_time = rospy.get_time()
        self.ind = 0
        self.max_ind = len(self.t)

    def send_cmds(self, event):

        if (self.ind < self.max_ind-1):
            current_time = rospy.get_time() - self.start_time
            print current_time
            while (self.ind < self.max_ind-1 and current_time > self.t[self.ind]):
                self.ind += 1
                print current_time

            self.turn = self.u[self.ind]
            self.throttle = 0.5
        else:
            self.status = 0
            self.turn = 0
            self.throttle = 0

        cmd = Float64MultiArray()
        cmd.data = [self.status, self.throttle, self.turn]
        self.pub.publish(cmd)


if __name__ == "__main__":
    try:
        rospy.init_node('gpops_car')
        c = Rollout()
        c.load_params()
        rospy.Timer(rospy.Duration(1/100.0), c.send_cmds)
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
