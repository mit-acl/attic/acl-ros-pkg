#!/usr/bin/env python
'''
Description: Run a single rollout on the car.

Created on June 25, 2015

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import rospy
import numpy as np
import scipy.io
import sys
import rospkg

# custom imports
from acl_msgs.srv import PilcoRollout
from acl_msgs.msg import GPparams


class Rollout:

    def __init__(self):
        rospack = rospkg.RosPack()
        self.pkg_path = rospack.get_path('car_control') + "/"
        rospy.loginfo("[Rollout] Path = %s"%(self.pkg_path))

    def load_params(self, base, file_names, sim=False, H=0):
        rospack = rospkg.RosPack()
        rospack.get_path('car_control')
        gp_params = []
        for i in range(len(file_names)):
            params = scipy.io.loadmat(self.pkg_path + base + file_names[i])
            n = params['n'][0]
            D = params['D'][0]
            E = params['E'][0]
            if H == 0:
                H = params['H'][0]
            dt = params['dt'][0]
            random = params['random'][0]
            input = params['inputs']
            beta = params['beta']
            iL2 = params['iL2']
            dy0 = params['dy0']

            print 'n:'
            print n
            print 'D:'
            print D
            print 'E:'
            print E
            print 'H:'
            print H
            print 'dt:'
            print dt
            print 'random:'
            print random
            print 'input:'
            print input
            print 'beta'
            print beta
            print 'iL2'
            print iL2
            print 'dy0'
            print dy0
            print
            print

            input_vector = np.zeros(n*D)
            cnt = 0
            for i in range(n):
                for j in range(D):
                    input_vector[cnt] = input[i, j]
                    cnt += 1

            gp = GPparams()
            gp.beta = beta
            gp.input = input_vector
            gp.iL2 = iL2
            gp_params.append(gp)

        self.start_rollout(gp_params, n, D, E, H, dt, random, sim, dy0, base)

    def start_rollout(self, gp_params, n, D, E, H, rate, random, sim,
                      dy0, base):

        x_array, y_array = self.run(gp_params, 2*H, n, D, E,
                                    rate, random, sim, dy0)

        x = np.zeros((H, D+E))
        y = np.zeros((H, D))
        cntx = 0
        cnty = 0
        for i in range(H):
            for j in range(D+E):
                x[i, j] = x_array.data[cntx]
                cntx += 1
                if j < D:
                    y[i, j] = y_array.data[cnty]
                    cnty += 1

        x_full = np.zeros((2*H, D+E))
        y_full = np.zeros((2*H, D))
        cntx = 0
        cnty = 0
        for i in range(2*H):
            for j in range(D+E):
                x_full[i, j] = x_array.data[cntx]
                cntx += 1
                if j < D:
                    y_full[i, j] = y_array.data[cnty]
                    cnty += 1

        #print x_full
        print x_full
        scipy.io.savemat(self.pkg_path + base + 'rollout.mat', dict(xx=x, yy=y))
        scipy.io.savemat(self.pkg_path + base + 'rollout_full.mat', dict(xx=x_full, yy=y_full))

        # Run rollout
# gp_params, 2*H, n, D, E,
#                                     rate, random, sim, dy0
    def run(self, gp_params, H, n, D, E, rate, random, onboard, dy0):
        if onboard:
            serv = '/RC03/run_car_rollout_onboard'
        else:
            serv = '/RC03s/run_car_rollout'
        print 'Waiting for service: ' + serv
        rospy.wait_for_service(serv)
        print 'Service found'
        try:
            st = rospy.ServiceProxy(serv, PilcoRollout)
            resp = st(gp_params, H, n, D, E, rate, random, dy0)
            return resp.x, resp.y
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e


if __name__ == "__main__":
    try:
        rospy.init_node('mfrl_car')
        c = Rollout()
        # files = ['gp_params1.mat', 'gp_params2.mat']
        # base = 'car180/'

        files = ['gp_params1.mat']
        base = 'data/'

        print len(sys.argv)
        for i in range(len(sys.argv)):
            print sys.argv[i]
        if len(sys.argv) == 2:
            sim = int(sys.argv[1])
            c.load_params(base, files, sim)
        elif len(sys.argv) == 3:
            sim = int(sys.argv[1])
            H = int(sys.argv[2])
            c.load_params(base, files, sim, H)
        else:
            c.load_params(base, files)

    except rospy.ROSInterruptException:
        pass
