#!/usr/bin/env python
'''
Description: Finds the F and L matrices required for running the ILC algorithm
from this paper:
"Iterative Learning of Feed-Forward Corrections for
High-Performance Tracking" by
Fabian L. Mueller, Angela P. Schoellig and Raffaello DAndrea

Created on Oct 22, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import roslib; roslib.load_manifest('car_control')
import rospy
import numpy as np
import copy
import cPickle as pickle

# custom imports
import D
import params
import projectOnPath

"""
Algorithm inputs
ubar = [x_cmd, y_cmd]
ybar = [x, y, Vx, Vy, psi, dpsi, omega]
cbar = [delta, omega_des]
"""

#==============================================================================
# Class for identifying F and L matrices
#==============================================================================
class Ident:
    def __init__(self):

        self.deltaU = 0.5
        self.ny = 7
        self.nu = 2
        self.nc = 2
        self.N = 50

        # allocate memory for F and L
        self.F = np.zeros((self.ny, self.N, self.nu, self.N))
        self.L = np.zeros((self.nc, self.N, self.nu, self.N))

#-----------------------------------------------------------------------------
# Calculate the initial path and initial return
#-----------------------------------------------------------------------------
    def initPath(self):
        xcross = 0.8
        t13 = 1.3
        t24 = 1.3

        pop = projectOnPath.projectOnPath()
        self.sim = D.RunSim()
        poly = pop.createOval(xcross, t13, t24)
        poly.discretizePath(50)

        # compute speed commands along path
        dx, dy, dz, t = poly.discretizePath(50, 1)
        speed = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
        max, min = poly.findMaxMinVelocity()

        # Initialize L1 controller
        self.path_nom = np.zeros((len(poly.x), 4))
        self.path_nom[:, 0] = poly.x
        self.path_nom[:, 1] = poly.y
        self.path_nom[:, 2] = poly.z
        self.path_nom[:, 3] = speed

        self.path_nom = self.path_nom[:50, :]

        self.ybar_nom, self.cbar_nom = self.sim.run_sim(self.path_nom)
        pass

#-----------------------------------------------------------------------------
# Identification routine -- takes a LONG time to run!
#-----------------------------------------------------------------------------
    def findFL(self):
        pass
#         Algorithm 1 from the paper
        for i in np.arange(self.nu):
            for j in np.arange(self.N):
                u1 = copy.copy(self.path_nom)
                u2 = copy.copy(self.path_nom)
                u1[j, i] += self.deltaU
                u2[j, i] -= self.deltaU
                y1_bar, c1_bar = self.sim.run_sim(u1)
                y2_bar, c2_bar = self.sim.run_sim(u2)

                print np.max(np.abs(y2_bar - y1_bar))

                # update F and L
                self.F[:, :, i, j] = (y2_bar - y1_bar) / (2 * self.deltaU)
                self.L[:, :, i, j] = (c2_bar - c1_bar) / (2 * self.deltaU)

#-----------------------------------------------------------------------------
# Save ybar_nom, cbar_nom, ubar_nom, F, and L
#-----------------------------------------------------------------------------
    def saveLog(self):

        logFile = '/home/mark/acl-ros-pkg/veh_control/car_control/log/FL.p'
        self.logFile = open(logFile, 'wb')
        log = {"F":self.F, "L":self.L, "ybar_nom":self.ybar_nom,
               "cbar_nom":self.cbar_nom, "ubar_nom":self.path_nom}
        pickle.dump(log, self.logFile)

if __name__ == '__main__':
    ns = rospy.get_namespace()
    try:
        rospy.init_node('single_segment_server')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control identifcation_routine.py")
        else:
            c = Ident()
            c.initPath()
            c.findFL()
            c.saveLog()


    except rospy.ROSInterruptException:
        pass
