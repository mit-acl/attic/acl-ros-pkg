#!/usr/bin/env python

###################################################
# lead_truck_comman.py -- simple function for sending commands to the lead vehicle for drifting
#
# Written by Mark Cutler -- markjcutler@gmail.com
#
# Created on Friday,  3 July 2015.
###################################################

import numpy as np
import rospy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped


class TruckCmd():

    def get_circle(self, x0, y0, R, num_steps):
        # create circle around origin
        theta = np.linspace(-np.pi, np.pi, num_steps)
        x = R*np.cos(theta)
        y = R*np.sin(theta)

        # translate circle by center point
        x += x0
        y += y0

        # convert to polar coordinates
        phi = np.arctan2(y, x) - np.pi/2

        return x, y, theta, phi

    def send_pose(self):

        R = 1.5
        x0 = 0
        y0 = -3
        des_speed = 0.15  # m/s
        num_steps = 500

        omega = des_speed/R  # rad/s

        x, y, theta, phi = self.get_circle(x0, y0, R, num_steps)
        theta += np.pi/2

        delay = 2*np.pi/(num_steps*omega)

        r = rospy.Rate(1.0/delay)
        pubp = rospy.Publisher('/RC04/pose', PoseStamped, queue_size=1)
        pubt = rospy.Publisher('/RC04/vel', TwistStamped, queue_size=1)

        ind = 0

        while not rospy.is_shutdown():
            p = PoseStamped()
            p.pose.position.x = x[ind]
            p.pose.position.y = y[ind]
            p.pose.orientation.w = np.cos(theta[ind]/2.0)
            p.pose.orientation.z = np.sin(theta[ind]/2.0)

            v = TwistStamped()
            v.twist.linear.x = des_speed*np.cos(theta[ind])
            v.twist.linear.y = des_speed*np.sin(theta[ind])

            ind += 1
            if ind >= len(x):
                ind = 0

            pubp.publish(p)
            pubt.publish(v)

            r.sleep()


if __name__ == "__main__":
    try:
        rospy.init_node('truck_sim')
        c = TruckCmd()
        c.send_pose()

    except rospy.ROSInterruptException:
        pass
