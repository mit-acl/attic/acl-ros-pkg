#!/usr/bin/env python
import roslib
roslib.load_manifest('car_control')
import rospy
import numpy as np
import cPickle as pickle
from std_msgs.msg import Bool
from car_sim.msg import CarState
from aclpy import utils


class LapData:

    def __init__(self, lap_cnt):
        self.lap_cnt = lap_cnt
        self.i = 0
        self.max_size = 5000
        self.x = np.zeros(self.max_size)
        self.y = np.zeros(self.max_size)
        self.psi = np.zeros(self.max_size)
        self.vx = np.zeros(self.max_size)
        self.vy = np.zeros(self.max_size)
        self.v = np.zeros(self.max_size)

    def writeData(self, car_state):
        if self.i < self.max_size:
            self.x[self.i] = car_state.pose.position.x
            self.y[self.i] = car_state.pose.position.y
            self.psi[self.i] = utils.quat2yaw(car_state.pose.orientation)
            self.vx[self.i] = car_state.Vx
            self.vy[self.i] = car_state.Vy
            self.v[self.i] = utils.norm(car_state.Vx, car_state.Vy)
            self.i += 1


class LogData:

    def __init__(self):
        self.state = CarState()
        self.lap_counter = 0
        self.log_lap_data = False
        self.lap_data = []
        package_address = roslib.packages.get_pkg_dir('car_control')
        self.log_file = open(package_address + '/log/lap_data.pkl', 'wb')

    def lapLogCB(self, data):
        self.log_lap_data = data.data

        if self.log_lap_data is True:
            self.lap_counter += 1
            self.lap_data.append(LapData(self.lap_counter))
        # else:
        #     self.log_file.seek(0)
        #     pickle.dump(self.lap_data, self.log_file)

    def stateCB(self, data):

        self.state = data

        if self.log_lap_data is True:
            #self.printData()
            #print self.lap_data[self.lap_counter].i
            print self.lap_data[self.lap_counter-1].i

            self.lap_data[self.lap_counter-1].writeData(data)

    def saveData(self):
        self.log_file.seek(0)
        pickle.dump(self.lap_data, self.log_file)
        print "Shutting down!"

    def printData(self):

        print 'x position: ' + str(self.state.pose.position.x)
        print 'y position: ' + str(self.state.pose.position.y)


        print 'heading: ' + str(utils.quat2yaw(self.state.pose.orientation))

        print 'x velocity (body frame): ' + str(self.state.Vx)
        print 'y velocity (body frame): ' + str(self.state.Vy)

        print 'yaw rate: ' + str(self.state.r)
        print 'wheel speed: ' + str(self.state.omegaR)

        print
        print


if __name__ == '__main__':
    ns = rospy.get_namespace()
    try:
        rospy.init_node('log_data')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control log_data.py")
        else:
            print "Starting data logging node for: " + str(ns)
            c = LogData()
            rospy.on_shutdown(c.saveData)
            rospy.Subscriber("state", CarState, c.stateCB)
            rospy.Subscriber("log_lap_data", Bool, c.lapLogCB)
            rospy.spin()

    except rospy.ROSInterruptException:
        pass
