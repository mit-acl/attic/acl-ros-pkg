#!/usr/bin/env python
'''
Description: Reinforcement Learning using Multi-Fidelity Simulators

See ICRA 2014 paper by the above title for algorithm details

This class won't run on it's own.  It several functions need to be implemented
in a derived class.

Created on Nov 27, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('car_control')
import rospy
import actionlib
import numpy as np
import sets
import matplotlib.pyplot as plt
import cPickle as pickle
import copy
import signal
import sys
from std_msgs.msg import Bool

# custom imports
from car_control.srv import SingleSegment
from car_control.srv import ResetCar
from car_control.msg import SimpleState
from car_control.msg import *
import params

from aclpy import utils
from aclpy import mfrl
# import mfrl


# TODO:
# Set out of bounds state to Rmin
# potentially ignore first out of bounds transition and reward
# test slow fixed lap to see if sim is giving correct information or not

class MFRLCar(mfrl.MFRL):

    def __init__(self, name=None,
                 unidirectional=False, generative_access=True, useDBN=True,
                 just_top=False, bandit=False, manual_start=True,
                 logFileIndex=0, logFile0=None, logFile1=None, logFile2=None):

        # seed random number generator
        #self.rand_state = np.random.RandomState(1)
        self.rand_state = None

        mr = [1, 1, 1]
        mt = [1, 3, 3]
        if bandit:
            mr = [1, 1, 2]
            mt = [1, 1, 2]

        # variables specific to car
        self.cf = CarFlags()
        self.cf.num_laps = 3
        self.cf.demonstration = False
        self.shouldDrivePub = rospy.Publisher('/' + str(name) +
                                              '/should_drive',
                                              Bool, latch=True)
        self.logLapPub = rospy.Publisher('/' + str(name) +
                                         '/log_lap_data',
                                         Bool, latch=True)
        self.package_address = roslib.packages.get_pkg_dir('car_control')
        self.manual_start = manual_start
        self.r2s = actionlib.SimpleActionClient('/' + str(name) +
                                                '/r2s', Return2StartAction)
        self.r2s.wait_for_server()
        self.r2s_goal = Return2StartGoal()

        # call MFRL constructor
        mfrl.MFRL.__init__(self, name, unidirectional,
                           generative_access, useDBN,
                           just_top, bandit, mr, mt, logFileIndex,
                           logFile0, logFile1, logFile2)

        # these values will probably be overridden in derived classes
        #self.numConvStates = [100, 100, 50]
        self.numConvStates = [200, 200, 100]
        self.mDown = [np.Inf, 10, 5] #10]
        if bandit:
            self.numConvStates = [1, 1, 1]
            self.mDown = [np.Inf, 1, 1]
        self.beta = [0.0, 0.0]
        # save Q at top level every this many environment interactions
        self.saveQRate = 50
        self.showDomain = False
        self.bandit = bandit

###########
# Functions to be implemented in derived classes
###########

#-----------------------------------------------------------------------------
# Initialize state-based state-action space
#-----------------------------------------------------------------------------
    def initStateBased(self, savedLog):
        # State and action space discretization
        self.numSegments = 2
        self.numPsidot = 3
        self.numR = 3
        self.numV = 3

        # harder problem for dbn in journal version
        self.numPsidot = 3
        self.numR = 4
        self.numV = 6

        minVel = 2.0  # 2.1  # 2.4
        maxVel = 3.2  # 3.4
        minR = 0.5  # 0.35
        maxR = 1.2
        minPsidot = -1.0 #2.0
        maxPsidot = 3.5 #4.5

        # create the discretized states and actions
        self.Vxbins = np.linspace(minVel, maxVel, self.numV, False)
        self.Psidotbins = np.linspace(minPsidot, maxPsidot,
                                      self.numPsidot, False)
        self.Rbins = np.linspace(minR, maxR, self.numR, False)

        # has to do with how np.digitize assigns bins
        self.Vxbins = self.Vxbins[1:]
        self.Psidotbins = self.Psidotbins[1:]
        self.Rbins = self.Rbins[1:]

        self.RbinsAction = np.linspace(minR, maxR, self.numR)
        self.VbinsAction = np.linspace(minVel, maxVel, self.numV)

        self.s = self.initState()

        logFile = self.package_address + '/log/'
        for i in np.arange(self.numD):
            if savedLog[i] is not None:
                savedLog[i] = logFile + savedLog[i] + '.npz'

        # initialize RMax algorithm for each simulation level
        S = [np.arange(self.numSegments), np.arange(self.numV),
             np.arange(self.numPsidot), np.arange(self.numR)]
             #np.arange(self.numR)]
        A = [np.arange(self.numR), np.arange(self.numV)]

        # get max reward -> depending on track description,
        # the shortest segment could be a turn or a straight
        dist = np.array([np.pi * self.RbinsAction.min(), params.YDIST -
                         2 * self.RbinsAction.max()])
        # min dist segment / max velocity
        self.max_reward = -dist.min() / self.VbinsAction.max()

        # get min reward -> depending on track description,
        # the longest segment could be a turn or a straight
        dist = np.array([np.pi * self.RbinsAction.max(), params.YDIST -
                         2 * self.RbinsAction.min()])
        # max dist segment / min velocity
        self.min_reward = -dist.max() / self.VbinsAction.min()

        U = self.max_reward * np.ones((self.numSegments, self.numV,
                                       self.numPsidot, self.numR,
                                       #self.numR,
                                       self.numR, self.numV))
        U = U / (1.0 - self.gamma)

        if self.useDBN:
            # Set up generalized mapping for transition dynamics
            s_prime2sa = np.array([[1, 0, 0, 0, 0, 0],  # seg depends on seg
                                   [1, 0, 0, 0, 1, 1],  # vx depends on seg, vx_cmd and r_cmd
                                   [1, 0, 0, 0, 1, 1],  # psidot depends on seg, vx_cmd, r_cmd
                                   [1, 0, 0, 0, 1, 1]])  # r depends on seg, vx_cmd, r_cmd
#            s_prime2sa = np.array([[1, 0, 0, 0, 0],  # seg depends on seg
#                                   [1, 0, 0, 1, 1],  # vx depends on seg, vx_cmd and r_cmd
#                                   [1, 0, 0, 1, 1]])  # r depends on seg, vx_cmd, r_cmd
            self.rm = self.initRmax(S, A, U, savedLog, logFile, s_prime2sa, self.max_reward)
        else:
            self.rm = self.initRmax(S, A, U, savedLog, logFile)

#-----------------------------------------------------------------------------
# Initialize bandit state-action space
#-----------------------------------------------------------------------------
    def initBandit(self, savedLog):

        self.gamma = 0

        # State and action space discretization
        self.numS = 2
        self.numR = 5
        self.numV1 = 5
        self.numV2 = 5

        # create the discretized states and actions
        self.V1bins = np.linspace(2.0, 3.5, self.numV1)  # 3.4, self.numV1)
        self.V2bins = np.linspace(2.0, 3.5, self.numV1)  # 3.4, self.numV2)
        self.Rbins = np.linspace(0.5, 1.2, self.numR)

        self.s = self.initState()

        logFile = self.package_address + '/log/'
        for i in np.arange(self.numD):
            if savedLog[i] is not None:
                savedLog[i] = logFile + savedLog[i] + '.npz'

        # initialize RMax algorithm for each simulation level
        S = [np.arange(self.numS)]
        A = [np.arange(self.numR), np.arange(self.numV1),
             np.arange(self.numV2)]

        # get max reward
        curve = 2 * np.pi * self.Rbins.min() / self.V2bins.max()
        straight = 2 * (params.YDIST -
                        2 * self.Rbins.min()) / self.V1bins.max()
        self.max_reward = (-curve - straight) * self.cf.num_laps

        U = self.max_reward * np.ones((self.numS, self.numR,
                                       self.numV1, self.numV2))
        U[1, ] = 0  # all terminal states should have initial U = 0

        U = U / (1.0 - self.gamma)

        self.initRmax(S, A, U, savedLog, logFile)

#-----------------------------------------------------------------------------
# Set initial state
#-----------------------------------------------------------------------------
    def initState(self):

        self.flags.reset_state = True
        self.cf.first_turn = True
        self.cf.lap_cnt = 0
        self.cf.lap_time = 0

        s = SimpleState()
        s.prev_radius = 0.8  # initial radius
        s.Vx = 2.2  # reset velocity is 1.0 m/s
        s.Vy = 0.0
        s.psidot = 0.0
        s.omega = s.Vx / params.RADIUS
        s.segment = 2 # starting at top right corner of track
        s.x = params.X2 + s.prev_radius*params.TURN_DIR #params.X1 - s.prev_radius * params.TURN_DIR
        s.y = params.Y2 + s.prev_radius #params.Y1 - s.prev_radius
        s.psi = np.pi / 2.0 #-np.pi / 2.0

        s.x, s.y = utils.rotate2D(s.x - params.XCENTER,
                                  s.y - params.YCENTER, params.OVERALL_ROT)
        s.x += params.XCENTER
        s.y += params.YCENTER
        s.psi += params.OVERALL_ROT

        if self.bandit:
            s = 0

        return s

#-----------------------------------------------------------------------------
# Check if it is ok to run value iteration right now
# Should either run value iteration and return False, or return True
#-----------------------------------------------------------------------------
    def shouldRunValIteration(self):
        if self.flags.state_was_reset:
            i = self.rmax[self.d].valIter()
            print "ran " + str(i + 1) + " value iterations"
            # print "Num known states visited in a row: " +
            # str(self.flags.update_counter)
            print
            return False
        return True

#-----------------------------------------------------------------------------
# Get the car to return to the starting position
#-----------------------------------------------------------------------------
    def resetState(self, a):

        self.cf.lap_cnt = 0
        self.cf.lap_time = 0
        self.logLapPub.publish(False)

        if self.d == self.numD - 1:  # top level
            # tell the car server it's ok to drive
            self.shouldDrivePub.publish(True)

            if self.bandit:
                r = self.Rbins[a[0]]
                v = self.V1bins[a[1]]
            else:
                r, v = self.actionIndex2Val(a[0], a[1])
                r = 0.8
#                 v = 2.1

            # start the return to starting position action
            self.r2s_goal.r = r
            self.r2s_goal.v = v
            try:
                self.r2s.send_goal(self.r2s_goal)
            except rospy.ServiceException, e:
                print "Service call failed: %s" % e

#             rospy.wait_for_service('/' + str(self.name) + '/return2start')  # always real car
#             try:
#                 st = rospy.ServiceProxy('/' + str(self.name) + '/return2start', ResetCar)
#                 resp = st(r, v)
#             except rospy.ServiceException, e:
#                 print "Service call failed: %s" % e


#-----------------------------------------------------------------------------
# Copy missing variables at bottom level to the middle level
#-----------------------------------------------------------------------------
    def copyMissingVars(self):
        # copy Q and n over variables that don't get used
        if self.d == 0 and not self.bandit:
#            if self.useDBN:
#                # copy factor probability tables
#                for ii in np.arange(self.numPsidot):
#                    self.rmax[self.d].factors[2].pr[ii] = copy.copy(self.rmax[self.d].factors[2].pr[0])
#                for sa, n in np.ndenumerate(self.rmax[self.d].nr):
#                    ind_parents = tuple(np.array(sa)[self.rmax[self.d].factors[2].parents])
#                    ind = (Ellipsis,) + ind_parents
#                    self.rmax[self.d].factors[2].pr_col_sum[ind_parents] = np.sum(self.rmax[self.d].factors[2].pr[ind])
                

            # set for psidot
            for jj in np.arange(self.numPsidot):
                #self.rmax[self.d].Q[:, :, jj, ] = copy.copy(self.rmax[self.d].Q[:, :, 0, ])
                self.rmax[self.d].nr[:, :, jj, ] = copy.copy(self.rmax[self.d].nr[:, :, 0, ])
                #self.rmax[self.d].nt[:, :, jj, ] = copy.copy(self.rmax[self.d].nt[:, :, 0, ])
                
            self.rmax[self.d].valIter()

#-----------------------------------------------------------------------------
# Call any custom plotting or other functions when moving up a level
#-----------------------------------------------------------------------------
    def moveUpCustom(self):

        # wait for user to allow you to go up
        if self.d == 1:
            input = 0
            while input != 'u' and input != 'r':
                if self.manual_start:
                    input = raw_input('\Enter "u" to go up, "r" to repeat current best policy\n')
                else:
                    input = 'u'

            if input == 'u':
                self.shouldDrivePub.publish(True)
                # save pertinent data
#                 self.saveLog()
            elif input == 'r':
                rospy.loginfo("\n\nRepeating last policy at current level\n\n")
                self.con[self.d] = False
                self.d -= 1

                # don't need to call saveLog here because we aren't moving a level
        # else:
            # save pertinent data
#             self.saveLog()

#-----------------------------------------------------------------------------
# Call any custom plotting or other functions when moving down a level
#-----------------------------------------------------------------------------
    def moveDownCustom(self):
        if self.d == 2:
            self.shouldDrivePub.publish(False)

#-----------------------------------------------------------------------------
# Call any custom functions when finished learning
#-----------------------------------------------------------------------------
    def closeCustom(self):
        self.shouldDrivePub.publish(False)  # make sure the car stops

#-----------------------------------------------------------------------------
# Run a single step
#-----------------------------------------------------------------------------
    def getNextState(self, s, a, d):
        term = False
        serv = '/' + str(self.name) + 's/single_segment_cpp'  # simulated vehicle
        if d == 2:  # self.numD - 1:
#             d = 1
            serv = '/' + str(self.name) + '/single_segment'  # real vehicle
            # wait for return2goal action to complete if it is still going
            self.r2s.wait_for_result()
        rospy.wait_for_service(serv)
        try:
            st = rospy.ServiceProxy(serv, SingleSegment)
            if self.bandit:
                ns = 1
                r = self.Rbins[a[0]]
                v1 = self.V1bins[a[1]]
                v2 = self.V2bins[a[2]]

                # run three laps
                reward = 0
                state = SimpleState()
                state.prev_radius = r
                state.segment = 0
                state.Vx = 1.0
                reset = True
#                 resp = st(state, r, v1, True, False, d)
#                 reward += resp.reward
#                 state = resp.s_prime
                for i in np.arange(4 * self.cf.num_laps):
                    if state.segment == 0 or state.segment == 2:
                        resp = st(state, r, v1, reset, False, d)
                        if reset:
                            reset = False
                    else:
                        resp = st(state, r, v2, False, False, d)
                    if resp.reward == 200000:  # spin out or time out
                        reward = -resp.reward
                        break
                    reward += -resp.reward
#                     print resp.reward
                    state = resp.s_prime

                self.cf.lap_time = -reward
                if reward >= self.max_reward:
                    pass

            else:
                # convert action integers to actual values
                r, v = self.actionIndex2Val(a[0], a[1])

                # start logging data
                if s.segment == 2 and self.cf.lap_cnt == 0 and d == self.numD-1:
                    self.logLapPub.publish(True)

#                  DEBUG
                if self.cf.demonstration and d == 2:
                    r = 1.2
                    v = 2.1
                    if s.segment == 0 or s.segment == 2:
                        v = 3.4
                        r = 0.5
                        if self.cf.first_turn:
                            r = 1.2
                            self.cf.first_turn = False

                resp = st(s, r, v, self.flags.reset_state, self.cf.use_start_state, d)
                reward = resp.reward
                self.cf.lap_time += reward
                reward = -reward
                if reward >= self.max_reward:
                    pass

                reward = utils.saturate(reward, self.max_reward, -np.Inf)
                ns = resp.s_prime

                #if ns.segment == 0 and s.segment == 3:
                # now starting at top left corner of track
                if ns.segment == 2 and s.segment == 1:
                    self.cf.lap_cnt += 1
                    rospy.loginfo('Lap Count: ' + str(self.cf.lap_cnt))

            if self.cf.lap_cnt >= self.cf.num_laps or self.bandit:  # finished laps
                rospy.loginfo("Current Lap Time: " + str(self.cf.lap_time / self.cf.num_laps))
                if self.cf.lap_time < self.cf.best_lap_time[self.d]:
                    self.cf.best_lap_time[self.d] = copy.copy(self.cf.lap_time)
                rospy.loginfo("Best Lap Time: " + str(self.cf.best_lap_time[self.d] / self.cf.num_laps))
                rospy.loginfo("UpdateCounter: " + str(self.flags.update_counter))
                term = True

            if reward == -200000:  # timed out, reset
                self.flags.update_counter = 0
                term = True

            # log reward
            if d == 2:
                self.flags.top_level_reward[-1].append(reward)
                if term:
                    self.flags.top_level_reward.append([])

            return ns, reward, term  # last is terminal state #TODO: do lap counting here?
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e

#-----------------------------------------------------------------------------
# Set a set of terminal states
#-----------------------------------------------------------------------------
    def getTerminalStates(self, d):
        ts = sets.Set([])
        return ts

#-----------------------------------------------------------------------------
# converts 2d continuous states to discrete
# Assumes grid world is [0 -> 1] in both directions
# Disc: (0,0) is bottom left, (maxX-1,maxY-1) is upper right
#-----------------------------------------------------------------------------
    def cont2discState(self, cs):
        if self.bandit:
            return [cs]  # already a discrete state
        else:
            # digitize segments:
            vx = np.digitize(np.array([cs.Vx]), self.Vxbins)
            psidot = np.digitize(np.array([cs.psidot]), self.Psidotbins)
            radius = np.digitize(np.array([cs.prev_radius]), self.Rbins)
            segment = copy.copy(cs.segment)
            if segment > 1:
                segment -= 2  # accounts for symmetry

            if cs.Vx == cs.psidot == cs.prev_radius == cs.segment == 0:
                return np.array([-1, -1, -1, -1, -1])  # out of bounds state
            else:
                return np.array([segment, vx, psidot, radius ])
                #return np.array([segment, vx, radius ])


##############
# New functions only for car
##############

#-----------------------------------------------------------------------------
# Convert action indicies to real values
#-----------------------------------------------------------------------------
    def actionIndex2Val(self, r_index, v_index):
        r = self.RbinsAction[r_index]
        v = self.VbinsAction[v_index]
        return r, v

#-----------------------------------------------------------------------------
# Custom control c handler so we can shut down gracefully
#-----------------------------------------------------------------------------
    def signal_handler(self, signal, frame):
        self.shouldDrivePub.publish(False)
        currentLevelIter = self.saveLog(0)
        rospy.signal_shutdown('Ctrl+C shutdown requested')
        sys.exit(0)

##############################################################################
# Simple class for holding flags and counters specific for the car
##############################################################################
class CarFlags:
    def __init__(self):
        self.lap_cnt = 0
        self.num_laps = 0
        self.lap_time = 0
        self.demonstration = False
        self.first_turn = False
        self.best_lap_time = 3 * [10000]
        self.use_start_state = True


if __name__ == "__main__":
   try:
       rospy.init_node('mfrl_car')
       c = MFRLCar(name='RC02')
       signal.signal(signal.SIGINT, c.signal_handler)
       c.runMFRL()

   except rospy.ROSInterruptException:
       pass
