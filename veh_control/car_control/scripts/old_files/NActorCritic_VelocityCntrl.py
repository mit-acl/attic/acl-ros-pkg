#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import sys
import numpy as np
import rospy
import random
import math
import matplotlib.pyplot as plt
from car_sim.srv import RunStep
from car_sim.msg import CarState

"""
State = [V, w]
phi = [V, w]
"""

STATEDIM = 2
PHIDIM = 2
THETADIM = 3
KDIM = 2
MAXITERATION = 5000
RADIUS = 0.035/2.0 # wheel radius
NOISE = 10
class NAC():
    
    def __init__(self,name):
        
        self.updateCnt = 0
        self.atSpeedCnt = 0
        
        self.name = name;
        self.theta0 = np.array([[1.],[0.],[0]])#*np.ones((THETADIM,1))#np.array([[1.],[0.001],[0.001],[0],[0],[0],[0],[0],[0],[0]])
        self.ref = 3.0
        
        # parameters
        self.Lambda = 0.     # z rate
        self.alpha = 0.5    # learning rate
        self.beta = 0.9     # forgetting factor
        self.gamma = 0.99   # discount factor
        self.h = 1         # window over which we check w
        self.epsilon = math.pi/180  # check on angle between w vectors
        
        # initialize variables
        self.currentState = CarState()
        self.x = np.zeros((STATEDIM,MAXITERATION))
        #self.x[:,0:1] = np.array([[0.1],[0],[0]])
#         self.x[:,0] = self.initialState()
        self.theta = np.zeros((THETADIM,MAXITERATION))
        self.theta[:,0:1] = self.theta0
#         self.currentState.Vx = self.x[0,0]
#         self.currentState.Vy = self.x[1,0]
#         self.currentState.r = self.x[2,0]
        self.A = np.zeros((PHIDIM+THETADIM,PHIDIM+THETADIM,MAXITERATION))
        self.b = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.z = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.u = np.zeros((MAXITERATION))
        self.r = np.zeros((MAXITERATION))
        self.phiTilde = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.phiHat = np.zeros((PHIDIM+THETADIM,MAXITERATION))
        self.v = np.zeros((PHIDIM,MAXITERATION))
        self.w = np.zeros((THETADIM,MAXITERATION))
        print 'Starting state: ' + str(self.x[:,0:1])
        
#     def initialState(self):
#         x = np.zeros(STATEDIM)
#         x[0:STATEDIM] = getRandomState(0.0,0,0,0.,0,0.).transpose()
#         return x
    
    def runAlg(self):
        for t in range(0,MAXITERATION-1):
            self.Execute(t)
            self.CriticEvaluation(t)
            self.ActorUpdate(t)
#                         check if state is valid
#             if not self.checkState():
                # reset state
                
#                 if self.ref == 0:
#                     self.ref = 3.0
#                 else:
#                     self.ref = 0
                
                #self.currentState = CarState()
#                 print self.x[:,t]
                #self.Execute(t)
#                 self.u[t] = getControl(self.x[0:STATEDIM-1,t], self.theta[:,t])
#                 self.x[:,t+1:t+2] = self.run_step(self.x[:,t:t+1], DT, self.u[t], True)
#            print t
#            print 'Control: ' + str(self.u[t])
#            print 'State: ' + str(self.x[:,t+1:t+2])
#            print 'Reward: ' + str(self.r[t])
#            print 'Theta: ' + str(self.theta[:,t])

        self.plotResults()

    def plotResults(self):            
        x = np.arange(0, len(self.u))
        plt.plot(x, self.u.transpose(), label='Omega Desired')
        plt.plot(x, self.r.transpose(), label='Reward')
        plt.plot(x, self.x[0,:].transpose(), label='V_x')
        plt.plot(x, self.theta[0,:].transpose(), label='Theta_1')
        plt.legend()
        plt.show()

    def ActorUpdate(self,t):
        # in case nothing else, set new theta to old -- this may get overwritten below
        self.theta[:,t+1:t+2] = self.theta[:,t:t+1]
        self.updateCnt += 1
        
        # for first h runs, do nothing
        if t > self.h:
            update = True
            # check all the previous h w vectors
            for tau in range(self.h):
                if angleBetweenVectors(self.w[:,t+1], self.w[:,t-tau]) > self.epsilon:
                    update = False
            # if all of the w vectors are within tolerance, update
            if update == True and self.updateCnt > 30:
                self.updateCnt = 0
                # update parameters
                self.theta[:,t+1:t+2] = self.theta[:,t:t+1] + self.alpha*self.w[:,t+1:t+2]
                eta = self.theta[-1,t+1]
                if eta > 50:
                    self.theta[-1:t+1] = 50
                elif eta < -50:
                    self.theta[-1:t+1] = -50
                print "theta: " + str(self.theta[:,t+1])
                print "w: " + str(self.w[:,t+1])
                print "reward: " + str(self.r[t])
                # forget sufficient statistics
                self.z[:,t+1:t+2] *= self.beta
                self.A[:,:,t+1] *= self.beta
                self.b[:,t+1:t+2] *= self.beta
            

    def CriticEvaluation(self,t):
        # step 4.1 -- update basis functions
        self.phiTilde[0:PHIDIM,t:t+1] = self.getPhi(t+1)
        self.phiHat[0:PHIDIM,t:t+1] = self.getPhi(t)
        self.phiHat[PHIDIM:PHIDIM+THETADIM,t] = self.getThetaGrad(t,self.theta[:,t])
        
        # step 4.2 -- Update sufficient statistics
        self.z[:,t+1:t+2] = self.Lambda*self.z[:,t:t+1] + self.phiHat[:,t:t+1]
        tmp = self.phiHat[:,t:t+1] - self.gamma*self.phiTilde[:,t:t+1]
        self.A[:,:,t+1] = self.A[:,:,t] + np.dot(self.z[:,t+1:t+2], tmp.transpose())
        self.b[:,t+1:t+2] = self.b[:,t:t+1] + self.z[:,t+1:t+2]*self.r[t]
        
        # step 4.3 -- Update critic parameters
        if (np.linalg.det(self.A[:,:,t+1])) == 0:
            # psuedo inverse
            tmp = np.linalg.lstsq(self.A[:,:,t+1], self.b[:,t+1:t+2])[0]
        else:
            # true inverse
            tmp = np.linalg.solve(self.A[:,:,t+1], self.b[:,t+1:t+2])
        self.v[:,t+1:t+2] = tmp[0:PHIDIM]
        self.w[:,t+1:t+2] = tmp[PHIDIM:PHIDIM+THETADIM]
#         print "w: " + str(self.w[:,t+1:t+2])
        

    def Execute(self,t):
        # step 3, run the simulation
        #self.u[t] = getControl(self.x[:,t:t+1], self.ref, self.theta[:,t:t+1])
        
#         x = self.x[0:STATEDIM,t]
        x = np.zeros(KDIM)
        x[0] = self.currentState.Vx - self.ref
        x[1] = self.currentState.Vx - self.currentState.omegaR*RADIUS
        u = getControl(x, self.ref, self.theta[:,t])
        
        # add in feedforward velocity portion
#         K = getK(self.theta[:,t])
#         u = u + self.ref/RADIUS - K[0]*self.ref
        
#         print "u: " + str(u)
        self.u[t] = u
        for kk in range(20):
            self.run_step(self.x[:,t:t+1], 0.01, self.u[t], 0.0, True)
        self.r[t] = self.getReward(self.x[:,t:t+1])
        

    def checkState(self):
        valid = True
        Vx = self.currentState.Vx
        if Vx > self.ref-0.2 or Vx < self.ref+0.2:
            self.atSpeedCnt += 1
            
        if self.atSpeedCnt > 30:
            self.atSpeedCnt = 0
            valid = False
            
        return valid


    """ custom phi function """
    def getPhi(self,t):
#         Vx = self.x[0,t]
#         Vy = self.x[1,t]
#         psidot = self.x[2,t]

        Vx = self.currentState.Vx
        omega = self.currentState.omegaR

        phi = np.array([[Vx, omega]])
        return phi.transpose()
    
    """ custom theta gradient function """
    def getThetaGrad(self,t,theta):
        # theta = [K^t, nu]^t
        # pi = Normal(K^t*x, sigma^2)
        # d/dK log(pi) = 
        K = theta[0:STATEDIM]
        eta = theta[-1]
        sigma = eta2sigma(eta)
        
        u = self.u[t]
        Vx = self.currentState.Vx
        omega = self.currentState.omegaR
        mu = self.ref/RADIUS + K[0]*(Vx - self.ref) + K[1]*(Vx - omega*RADIUS)
        
        dlnpdf_dmu = (u - mu)/sigma**2
        dmu_dk1 = Vx - self.ref
        dmu_dk2 = Vx - omega*RADIUS
        dlnpdf_dk1 = dlnpdf_dmu*dmu_dk1
        dlnpdf_dk2 = dlnpdf_dmu*dmu_dk2
        
        dlnpdf_dsigma = ((u - mu)**2 - sigma**2)/sigma**3
        dsigma_deta = -NOISE*math.exp(eta)/(1 + math.exp(eta))**2
        dlnpdf_deta = dlnpdf_dsigma*dsigma_deta
        
        dlogpi_dtheta = np.zeros(THETADIM)
        dlogpi_dtheta[0] = dlnpdf_dk1
        dlogpi_dtheta[1] = dlnpdf_dk2
        dlogpi_dtheta[2] = dlnpdf_deta
        
        
#         tmp = self.u[t] + np.dot(K.transpose(),x)
#         dlogpi_dK = -x/sigma**2 * tmp
#         dsigma_deta = -math.exp(eta)/(math.exp(eta) + 1)**2
#         dlogpi_deta = (1/sigma**3 * tmp**2 - 1/sigma) * dsigma_deta
# #         dlogpi_deta = dlogpi_deta[0]
#         
# #         dlogpi_dtheta = np.array([[dlogpi_dK],[dlogpi_deta]])
#         dlogpi_dtheta = np.zeros(THETADIM)
# #         print dlogpi_dtheta
#         dlogpi_dtheta[0:KDIM] = dlogpi_dK.transpose()
#         dlogpi_dtheta[-1] = dlogpi_deta
#         print "dlogpi_dK"
#         print dlogpi_dK
#         print "dsigma_deta"
#         print dsigma_deta
#         print "dlogpi_deta"
#         print dlogpi_deta
#         print "dlogpi_dtheta"
#         print dlogpi_dtheta
        
        return dlogpi_dtheta
    
    
    def getReward(self,state):
        """ define custom reward function here """
#         Vx = state[0]
#         Vy = state[1]
#         psidot = state[2]

        Vx = self.currentState.Vx

        xerr = Vx - self.ref
        r = -xerr**2*10
        return r

#         if (Vx > self.ref[0]-0.1) and (Vx < self.ref[0]+0.1):
#             return 0
#         else:
#             return -1

    def run_step(self, x, dt, omegaDes, turn, vis):
        
#         s = self.currentState
#         s.Vx = x[0]
#         s.Vy = x[1]
#         s.r = x[2]
        rospy.wait_for_service('/RC01/run_step')
        try:
            run_step = rospy.ServiceProxy('/RC01/run_step', RunStep)
            resp1 = run_step(self.currentState,dt,omegaDes, turn, vis)
#             print omegaDes
            self.currentState = resp1.finalState
#             Vx = resp1.finalState.Vx
#             Vy = resp1.finalState.Vy
#             psidot = resp1.finalState.r
#             return np.array([[Vx], [Vy], [psidot]])
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

""" u = Normal(K^T*x, sigma^2) """
def getControl(x, xref,theta):
    K = theta[0:KDIM]
    eta = theta[-1]
    sigma = eta2sigma(eta)
    mu = 2.0*xref/RADIUS + K[0]*x[0] + K[1]*x[1]
    
#     xerr = np.zeros(len(x))
#      
#     for i in range(len(x)):
#         xerr[i] = x[i] - xref[i]
#     
#     xcontrol = np.zeros(KDIM)
#     xcontrol[0] = self.
    
#     print "K: " + str(K)
#     print "eta: " + str(eta)
#     print "x: " + str(x)
#     print "Kx: " + str(np.dot(K.transpose(),x))
    
    u = random.normalvariate(mu, sigma)
    return u

#""" full state feedback, linear control """
#def getControl(state,ref,theta):
#    ff = ref[0]/(0.035/2.0) # feedforward wheel speed for reference body velocity
#    s = np.array(state)
#    r = np.array(ref)
#    t = np.array(theta)
#    u = -np.dot(t.transpose(),(s - r)) + ff
#    return u

def getK(theta):
    b = theta[0]
    c = theta[1]
    
    k_slip = c/RADIUS
    kp = b - k_slip
    return [kp, k_slip]

def eta2sigma(eta):
    sigma = 0.1 + NOISE/(1 + math.exp(eta))
    return sigma

def angleBetweenVectors(a,b):
    tmp = np.dot(a,b)/(np.linalg.norm(a)*np.linalg.norm(b))
#    print 'tmp: ' + str(tmp)
    tmp = sat(tmp,1.0,-1.0)
    return math.acos(tmp)

def getRandomState(mu1,sigma1,mu2,sigma2,mu3,sigma3):
    Vx = random.normalvariate(mu1,sigma1)
    Vy = random.normalvariate(mu2,sigma2)
    psidot = random.normalvariate(mu3,sigma3)
    x = np.array([[Vx],[Vy],[psidot]])
    return x

def sat(val,hi,low):
    if val > hi:
        val = hi
    elif val < low:
        val = low
    return val

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Error: need to specify vehicle name as input argument"
    else:
        vehName = sys.argv[1]
        print "Starting Natural Actor-Critic Algorithm node for: " + vehName
        
        try:
            c = NAC(vehName)
            rospy.init_node('NActorCritic')
            c.runAlg()
        except rospy.ROSInterruptException:
            pass
