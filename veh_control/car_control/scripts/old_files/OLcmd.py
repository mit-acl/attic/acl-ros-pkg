#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from car_control.srv import *
import sys

def cmd(t,a,b):
    return t*a/b

def handle_OL_cmd(req):
    goalpub = rospy.Publisher('carCmd', Float64MultiArray)
    goal = Float64MultiArray()
    t = 0
    startTime = rospy.get_time()
    r = rospy.Rate(100)
    while t <= req.time:
        t = rospy.get_time() - startTime
        goal.data = [3, cmd(t,req.max_omega,req.time), cmd(t,req.max_turn,req.time)]
        goalpub.publish(goal)
        r.sleep()

    goal.data = [3, 0, 0]
    goalpub.publish(goal)
    
    return True

def openLoopCmdServer():
    rospy.init_node('OL_cmd_server')
    s = rospy.Service('OL_cmd', OLcmd, handle_OL_cmd)
    print "Ready to generate open loop commands"
    rospy.spin()

if __name__ == '__main__':
    try:
        openLoopCmdServer()
    except rospy.ROSInterruptException:
        pass
