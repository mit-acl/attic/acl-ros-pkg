#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from car_sim.msg import CarState
from random import randint

moveBindings = (
		(1,0),
		(1,1),
		(0,-1),
		(0,1),
	    (1,-1),
		(-1,0),
		(-1,-1),
		(-1,1),
)

#does random discrete actions, just setting up the interface here
class RandPlanner_DA(object):
    pub = rospy.Publisher('RC01/carCmd', Float64MultiArray)
    cmd = Float64MultiArray()
   
    #keep these around since we will need to store them later
    def __init__(self):
        self.throttle = 0
        self.turn = 0
        self.status = 1
        self.planning = False #keeping an eye on this 
        self.curState = None

    def sendCmd(self):
        
        s = 'Throttle: ' + str(self.throttle) + ', Turn: ' + str(self.turn)
        print(s)
        self.cmd.data = [self.status, self.throttle, self.turn]
        self.pub.publish(self.cmd)

    def runPol(self):
        sendCmd();

    def planActions(self):
        self.planning = True
        act = randint(0, len(moveBindings)-1)
        self.throttle = moveBindings[act][0]
        self.turn = moveBindings[act][1]
        self.planning = False

    def setState(self, s):
        self.curState = s        
        #st = 'state: ' + str(s.Vx) + ' , ' + str(s.Vy)  
        #print st

if __name__ == '__main__':
    
    rospy.init_node('RandPlanner_DA')
    try:
        r = RandPlanner_DA()
        rospy.Subscriber("RC01/state", CarState, r.setState) 
        while(1):    
            r.planActions()
            r.runPol()
    except rospy.ROSInterruptException:
        pass
