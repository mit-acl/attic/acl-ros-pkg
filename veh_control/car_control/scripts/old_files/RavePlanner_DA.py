#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from car_sim.msg import CarState
from car_sim.srv import RunStep
from random import randint
from RandPlanner_DA import RandPlanner_DA
from time import sleep
from math import *

#UCT implementation BUT I completely ignoed the depth when hashing the Q values,
#so this is completely biased towards the RAVE heuristic, just wanted to see what theat would do...

moveBindings = (
          (1,0),
          (1,1),
          (0,-1),
          (0,1),
          (1,-1),
          (-1,0),
          (-1,-1),
          (-1,1),
)

#does random discrete actions, just setting up the interface here
class RavePlanner_DA(RandPlanner_DA):
   
    delim = '!!!'
    maxDepth = 10 
    dt = 0.1 #10 Hz for now
    stateDim = 5
    discLevel = 20.0 
    maxs = (7.0,7.0,7.0,7.0,7.0) #TODO: find out some real values for these
    mins = (-7.0,-7.0,-7.0,-7.0,-7.0)
    gamma = 0.9
    numRollouts = 1000 

    #keep these around since we will need to store them later
    def __init__(self):
        super(RavePlanner_DA, self).__init__()
        #storage for the q-hash
        self.q_hash = dict()
        self.cs_hash = dict()
        self.csa_hash = dict()

    def getStringState(self, fs):
        st = '';
        for i in range(0, self.stateDim):
            st += str(fs[i]) + self.delim   
        return st;

    def getFactoredDisState(self,s):
        fs = self.getFactoredState(s)
        for i in range(0, self.stateDim):
            fs[i] = floor((fs[i]  - self.mins[i]) / ((self.maxs[i] - self.mins[i])/self.discLevel)) 
        return fs

    def getFactoredState(self,s):
        #rounding to the hundreths because this thing does not seem reliable
        factoredState = [0.0] * self.stateDim
        factoredState[0] = round(s.Vx,2)
        factoredState[1] = round(s.Vy,2)
        factoredState[2] = round(s.pose.orientation.x,2)
        factoredState[3] = round(s.pose.orientation.y,2)
        factoredState[4] = round(s.r,2)
        return factoredState

    def runPol(self):
        maxAct = -1;
        maxVal = 0.0;
        for i in range(0,10000):
            stateKey = str(self.getStringState(self.getFactoredDisState(self.curState)))
            for a in range(0, len(moveBindings)):
                key = stateKey + self.delim + str(a)   
                if(self.csa_hash.get(key) == None):
                    maxAct = -1
                    break
                elif(maxAct == -1 or self.q_hash.get(key) > maxVal): 
                    maxAct = a
                    maxVal = self.q_hash.get(key)
            #if so, for now just do the best action you can find for that
            if(maxAct > -1):
                self.throttle = moveBindings[maxAct][0]
                self.turn = moveBindings[maxAct][1]
                self.sendCmd()
            else:
                print('Need to replan')
                self.planActions()  #otherwise replan, don't run an action, the state probably already changed

    def planActions(self):
        self.planning = True
        print('making a plan')
        fs = self.getFactoredState(self.curState)
        for j in range(0,self.stateDim):
            print(fs[j])
        #do rollouts, store values
        self.q_hash = dict() #wipe out old values
        self.cs_hash = dict()
        self.csa_hash = dict()
        s0 = self.curState
        print('doing rollouts from ' + self.getStringState(self.getFactoredDisState(s0)))
        for i in range(0,self.numRollouts):  #ought to be around 2000
            print(i)
            self.rollouts(s0,0)
        #reset the car
        #self.throttle = 0
        #self.turn = 0
        #self.status = 3
        #self.sendCmd()
        print('I have a plan')
        self.planning = False

    def rollouts(self, s0, depth):
        #print('at depth ' + str(depth));
        qup = -100000000.0
        maxAction = 0;
        stateKey = self.getStringState(self.getFactoredDisState(s0))
        for a in range(0, len(moveBindings)):
            key = stateKey + self.delim + str(a)
            first = False
            if(self.q_hash.get(key) is None):
                first = True 
            else:
                qup2 = self.q_hash.get(key) + sqrt(2.0 * log(self.cs_hash.get(stateKey)) / self.csa_hash.get(key))
            if(first or qup2 > qup):
                maxAction = a
            if(not first):    
                qup = qup2
            else:
                break
        backup = 0.0;
        key = stateKey + self.delim + str(maxAction)
        if(key in self.csa_hash):
            self.csa_hash[key] = self.csa_hash[key] + 1.0
        else:
            self.csa_hash[key] = 1.0
            print('added key ' + key)
        if(stateKey in self.cs_hash):
            self.cs_hash[stateKey] = self.cs_hash[stateKey] + 1.0
        else:
            self.cs_hash[stateKey] = 1.0

        if(depth < self.maxDepth):
            rospy.wait_for_service('/RC01/run_step')
            runStep = rospy.ServiceProxy('/RC01/run_step', RunStep)
            s1 = runStep(s0, self.dt, moveBindings[maxAction][0], moveBindings[maxAction][1]).finalState
            backup = self.rollouts(s1, depth+1)
        # q_hash update
        if(self.q_hash.get(key) is None):
            self.q_hash[key] = 0.0
        self.q_hash[key] = (self.q_hash[key] * (self.csa_hash[key] -1.0) + (self.getReward(s0) + self.gamma * backup)) /  self.csa_hash[key]
        return self.q_hash[key]

    def getReward(self, state):
        #TODO: define a reward function
        return 10.0 * state.Vx 

def l2sqdist(s1, s2):
    #l2 distance without the root
    #for now just doing Vx, Vy and pose orientation x and y, #TODO: capture real state
    dist = 0.0
    dist += (s1.Vx - s2.Vx) * (s1.Vx - s2.Vx)
    dist += (s1.Vy - s2.Vy) * (s1.Vy - s2.Vy)
    dist += (s1.pose.orientation.x - s2.pose.orientation.x) * (s1.pose.orientation.x - s2.pose.orientation.x) 
    dist += (s1.pose.orientation.y - s2.pose.orientation.y) * (s1.pose.orientation.y - s2.pose.orientation.y) 
    return dist

if __name__ == '__main__':
    
    rospy.init_node('RavePlanner_DA')
    try:
        r = RavePlanner_DA()
        rospy.Subscriber("RC01/state", CarState, r.setState) 
        while(r.curState is None):
            sleep(.1)
        r.planActions()
        #while(1):    
        r.runPol()
        rospy.spin    
    except rospy.ROSInterruptException:
        pass
