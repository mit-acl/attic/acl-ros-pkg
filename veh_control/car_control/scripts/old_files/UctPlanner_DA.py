#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from car_sim.msg import CarState
from car_sim.srv import RunStep
from random import randint
from RavePlanner_DA import RavePlanner_DA
from time import sleep
from math import *

#UCT vanilla implementation where the depth of nodes matters for values

moveBindings = (
          (1,0),
          (1,1),
          (0,-1),
          (0,1),
          (1,-1),
          (-1,0),
          (-1,-1),
          (-1,1),
)

#does random discrete actions, just setting up the interface here
class UctPlanner_DA(RavePlanner_DA):
   

    #keep these around since we will need to store them later
    def __init__(self):
        super(UctPlanner_DA, self).__init__()

    def runPol(self):
        maxAct = -1;
        maxVal = 0.0;
        for d in range(0, self.maxDepth):
            #TODO: need a delay in here, actually need to move this whole mess over
            stateKey = str(self.getStringState(self.getFactoredDisState(self.curState))) + self.delim + str(d)
            for k in self.csa_hash.keys():
                print('have ' + k)
            for a in range(0, len(moveBindings)):
                key = stateKey + self.delim + str(a)   
                print('looking at Q ' + key + ' ' + str(self.q_hash.get(key)))
                if(self.csa_hash.get(key) == None):
                    maxAct = -1
                    break
                elif(maxAct == -1 or self.q_hash.get(key) > maxVal): 
                    maxAct = a
                    maxVal = self.q_hash.get(key)
            #if so, for now just do the best action you can find for that
            if(maxAct > -1):
                self.throttle = moveBindings[maxAct][0]
                self.turn = moveBindings[maxAct][1]
                self.sendCmd()
            else:
                print('Need to replan')
                self.planActions()  #otherwise replan, don't run an action, the state probably already changed
            sleep(self.dt)

    def rollouts(self, s0, depth):
        #print('at depth ' + str(depth));
        qup = -100000000.0
        maxAction = 0
        stateKey = self.getStringState(self.getFactoredDisState(s0)) + self.delim + str(depth)
        for a in range(0, len(moveBindings)):
            key = stateKey + self.delim + str(a)
            first = False
            if(self.q_hash.get(key) is None):
                first = True 
            else:
                qup2 = self.q_hash.get(key) + sqrt(2 * log(self.cs_hash.get(stateKey)) / self.csa_hash.get(key))
            if(first or qup2 > qup):
                maxAction = a
                if(not first):    
                    qup = qup2
                else:
                    break
        backup = 0.0;
        key = stateKey + self.delim + str(maxAction)
        if(key in self.csa_hash):
            self.csa_hash[key] = self.csa_hash[key] + 1.0
        else:
            self.csa_hash[key] = 1.0
            print('added key ' + key)
        if(stateKey in self.cs_hash):
            self.cs_hash[stateKey] = self.cs_hash[stateKey] + 1.0
        else:
            self.cs_hash[stateKey] = 1.0

        if(depth < self.maxDepth):
            rospy.wait_for_service('/RC01/run_step')
            runStep = rospy.ServiceProxy('/RC01/run_step', RunStep)
            s1 = runStep(s0, self.dt, moveBindings[maxAction][0], moveBindings[maxAction][1]).finalState
            backup = self.rollouts(s1, depth+1)
        # q_hash update
        if(self.q_hash.get(key) is None):
            self.q_hash[key] = 0.0 
        self.q_hash[key] = (self.q_hash[key] * (self.csa_hash[key] -1.0) + (self.getReward(s0) + self.gamma * backup)) /  self.csa_hash[key]
        return self.q_hash[key]

if __name__ == '__main__':
    
    rospy.init_node('UctPlanner_DA')
    try:
        r = UctPlanner_DA()
        rospy.Subscriber("RC01/state", CarState, r.setState) 
        while(r.curState is None):
            sleep(.1)
        r.planActions()
        #while(1):    
        r.runPol()
        rospy.spin    
    except rospy.ROSInterruptException:
        pass
