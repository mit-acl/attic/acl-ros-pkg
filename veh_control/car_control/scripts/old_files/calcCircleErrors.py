#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import Marker
from std_msgs.msg import Float64MultiArray
import math
import numpy as np

VELCMD = 0.5

class calcCircleErrors:

    
    def __init__(self, radius, x0, y0, vd):
        self.pose = PoseStamped()
        self.pub = rospy.Publisher('/goal_marker', Marker)
        self.marker = Marker(type=Marker.ARROW,action=Marker.ADD)
        self.R = radius
        self.x0 = x0
        self.y0 = y0
        self.vd = vd

    def initGoalMarker(self):
        self.marker.header.frame_id = "/world"
        self.marker.header.stamp = rospy.Time.now()
        self.marker.pose.position.x = 0.0
        self.marker.pose.position.y = 0.0
        self.marker.pose.position.z = 0.0
        
        self.marker.ns = "goals"
        self.marker.id = 1;
        
        self.marker.pose.orientation.x = 0
        self.marker.pose.orientation.y = 0
        self.marker.pose.orientation.z = 0.0
        self.marker.pose.orientation.w = 1
        self.marker.scale.x = 0.5
        self.marker.scale.y = 0.5
        self.marker.scale.z = 0.5
        self.marker.color.r = 0.0
        self.marker.color.g = 1.0
        self.marker.color.b = 0.0
        self.marker.color.a = 0.7
        self.marker.lifetime = rospy.Duration()

    def poseCallBack(self,data):
        self.pose = data
    
    def dist(self,p1,p2):
        x = p1[0] - p2[0]
        y = p1[1] - p2[1]
        return math.sqrt(x*x + y*y)
    
    # Assumes we are tracking a circle
    def findClosestPointonPath(self):        
        # find theta, angle of current position relative to circle center
        theta = math.atan2(self.pose.pose.position.y - y0, self.pose.pose.position.x - x0)
        
        # get desired inertial position
        xd = self.R*math.cos(theta) + self.x0
        yd = self.R*math.sin(theta) + self.y0
        
        return [xd, yd, theta]
    
    # get cross track error
    def crossTrackError(self,desiredPosition):
        currentPoint = [0,0]
        currentPoint[0] = self.pose.pose.position.x
        currentPoint[1] = self.pose.pose.position.y
        
        return dist(desiredPosition, currentPoint)
       
    # get trajectory yaw error 
    def psiError(self, theta):
        # get vehicle yaw
        q = [0, 0, 0, 0]
        q[0] = self.pose.pose.orientation.w
        q[1] = self.pose.pose.orientation.x
        q[2] = self.pose.pose.orientation.y
        q[3] = self.pose.pose.orientation.z
        yaw = self.yawFromQuat(q)
        
        psi = theta - yaw
        return psi
    
    # get path rotational rate
    def pathYawRate(self):
        return self.vd/self.R
    
    # yaw from quaternion
    def yawFromQuat(self,q):
        t1 = 2*(q[0]*q[3] + q[1]*q[2])
        t2 = 1 - 2*(q[2]**2 + q[3]**2)
        return math.atan2(t1, t2)

    def broadcastGoal(self):
        self.pub.publish(self.marker)
