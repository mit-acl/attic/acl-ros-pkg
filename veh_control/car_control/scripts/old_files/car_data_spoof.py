#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
import math

class DataSpoof:
    pub1 = rospy.Publisher('RC01/pose', PoseStamped)
    pub2 = rospy.Publisher('ViconData', String)
    
    def __init__(self):
        self.pose = Pose()
        self.pose.position.x = 0.0
        self.pose.position.y = 0.0
        self.pose.position.z = 0.0
        self.pose.orientation.w = 1.0
        self.pose.orientation.x = 0.0
        self.pose.orientation.y = 0.0
        self.pose.orientation.z = 0.0
        self.yaw = 0;
        self.time = rospy.Time.now().to_sec()
    
    def joyCallBack(self, data):
        
        pose = PoseStamped()
        pose.header.frame_id = "RC01"
        pose.header.stamp = rospy.Time.now()
        
        self.yaw += data.axes[3]/20.0
        vel = -data.axes[1]/0.3
        while self.yaw > math.pi:
            self.yaw -= 2*math.pi
        while self.yaw < -math.pi:
            self.yaw += 2*math.pi
        
        dt = self.time - rospy.Time.now().to_sec()
        self.time = rospy.Time.now().to_sec()
        self.pose.position.x += vel*math.cos(self.yaw)*dt
        self.pose.position.y += vel*math.sin(self.yaw)*dt
            
        self.pose.orientation.w = math.cos(self.yaw/2.0)
        self.pose.orientation.z = math.sin(self.yaw/2.0)
        
        pose.pose = self.pose
        
        s = String()
        s.data = "RC01,"
        
        self.pub1.publish(pose)
        self.pub2.publish(s)



def mainFunc():
    rospy.init_node('car_data_spoof')
    c = DataSpoof()
    rospy.Subscriber("joy", Joy, c.joyCallBack)
    rospy.spin()

if __name__ == '__main__':
    try:
        mainFunc()
    except rospy.ROSInterruptException:
        pass