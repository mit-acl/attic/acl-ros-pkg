#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from car_sim.msg import CarState
import sys, select, termios, tty
import csv
import numpy as np
import math
import random

msg = """
Reading saved policy and publish to carCmd
"""

NOT_DRIVING = 0
THROTTLE = 1
SIMPLE_CONTROLLER = 2
WHEEL_SPEED = 3
RESET = 4

class ExecutePolicy():
    
    cmd = Float64MultiArray()
    
    def __init__(self,name,qvalFile):
        self.throttle = 0
        self.turn = 0
        self.name = name;
        self.pub = rospy.Publisher(self.name + '/carCmd', Float64MultiArray)
        self.qvalFile = qvalFile
        self.pose = Pose()
        self.twist = Twist()
        self.carState = CarState()
        self.loadQvals()
    
    def loadQvals(self):
#         fo = open(self.policyFile,"r")
        with open(self.qvalFile) as f:
            content = f.readlines()
        
        # get header and actions
        num_actions_states = content[1].split(',')
        self.num_actions = int(num_actions_states[0])
        self.num_states = int(num_actions_states[1])
        
        self.actions = np.zeros((self.num_actions,51))
        self.states = np.zeros((self.num_states,21))
        self.qvals = np.zeros((20,20,51))
        
        # fourth line is start of actions
        action_start = 3
        for i in np.arange(self.num_actions):
            tmp = content[action_start+i].split(',')
            self.actions[i,:] = tmp[0:-1]
        
        # sixth line is start of states
        state_start = 4 + self.num_actions
        for i in np.arange(self.num_states):
            tmp = content[state_start+i].split(',')
            self.states[i,:] = tmp[0:-1]
        
        # ninth line is start of qvals
        qval_start = 5+self.num_actions+self.num_states
        for i in np.arange(qval_start,len(content)):
            tmp = content[i].split(',')
            ii = i-qval_start
            ind1 = math.floor(ii/20)
            ind2 = ii % 20
            self.qvals[ind1,ind2,:] = tmp[0:-1]
            
        print self.actions
        print self.states
        print self.qvals
        
        
    def alborzrandint(self,low,high,m=1,n=1):
        # Use random.rand_int instead unless you want to debug and would like to generate same random numbers as matlab
        # Generates a random integer matrix m-by-n where elements are in [low,high]
        res = np.zeros((m,n),'int64')
        d = high - low
        for i in np.arange(m):
            for j in np.arange(n):
                coin = np.random.rand()
                res[i,j] = np.round(coin*d)+low
        return res
    def randSet(self,x):
        #Returns a random element of a list uniformly.
        #i = random.random_integers(0,size(x)-1)
        i = self.alborzrandint(0,np.size(x)-1)[0,0]
    #    print x
    #    print('in randSet: %d' % i)
        return x[i]
    
    # TODO: implement
    def returnBestAction(self,s):
        
        # first, check that s resides within the bounded allowable states
        svalid = True
        for i in np.arange(self.num_states):
            if s[i] < self.states[i][0] or s[i] > self.states[i][-1]:
                svalid = False
                return 0
        
        # find indices
#         index = zeros((self.num_states,1))
        index = np.zeros(self.num_states)
        for i in np.arange(self.num_states):
            tmp = np.nonzero(self.states[i,:] <= s[i])
            print tmp[0][-1]
            index[i] = tmp[0][-1]
        
        
        q = self.qvals[index[0],index[1],:]
#         print q.shape
        aindex = np.array(np.nonzero(q == q.max()))#np.argmax(q)
        aindex = aindex[0]
#         print aindex
#         print self.actions.shape
#         print "len(aindex):"
#         print len(aindex)
#         print aindex.shape
#         print aindex
        if len(aindex) == 0:
            return 0
        elif len(aindex) > 1:
            aindex = self.randSet(aindex)
            
        a = self.actions[0][aindex]
#         print a
        return a
    
    def poseCB(self, data):
        self.pose = data.pose
        
    def velCB(self, data):
        self.twist = data.twist
        
    def carStateCB(self, data):
        self.carState = data

    def sendCmd(self,type):
        #current state
        s = np.array([self.carState.Vx, self.carState.omegaR])
        a = self.returnBestAction(s)
        print a
        print s
#         a = 40
        self.cmd.data = [type, a, 0.0]
        self.pub.publish(self.cmd)
    
    def runController(self,event):
        self.sendCmd(WHEEL_SPEED)


if __name__ == '__main__':
#     settings = termios.tcgetattr(sys.stdin)
#     if len(sys.argv) != 3:
#         print "Error: need to specify vehicle name and policy file as input arguments"
#     else:
#         vehName = sys.argv[1]
#         policyFile = sys.argv[2]
        vehName = "RC01"
        qvalFile = "/home/mark/RLPy/qval_car3.txt"
        print "Starting policy execution node for: " + vehName
        
        try:
            c = ExecutePolicy(vehName,qvalFile)
            rospy.init_node('execute_policy')
            rospy.Subscriber(vehName+"/pose", PoseStamped, c.poseCB)
            rospy.Subscriber(vehName+"/vel", TwistStamped, c.velCB)
            rospy.Subscriber(vehName+"/state", CarState, c.carStateCB)
            
            rospy.sleep(0.2)
            for i in np.arange(10):
                c.sendCmd(RESET)
                rospy.sleep(0.01)
            
#             s = np.array([0., 0.])
#             c.returnBestAction(s)
                    
            # set up 50 Hz callback timer
            rospy.Timer(rospy.Duration(1.0/50.0), c.runController)
            
            rospy.spin()
    
        except rospy.ROSInterruptException:
            pass
