#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from car_sim.msg import CarState
import sys, select, termios, tty
import csv
import numpy as np
import math
import random
import signal

"""
Execute learned policy from Natural Actor Critic and publish to carCmd
"""

NOT_DRIVING = 0
THROTTLE = 1
SIMPLE_CONTROLLER = 2
WHEEL_SPEED = 3
RESET = 4

RADIUS = 0.035/2.0 # wheel radius
NOISE = 10
KDIM = 2

class ExecutePolicy():
    
    cmd = Float64MultiArray()
    
    def __init__(self,name,qvalFile):
        self.throttle = 0
        self.turn = 0
        self.ref = 1
        self.name = name;
        self.pub = rospy.Publisher(self.name + '/carCmd', Float64MultiArray)
        self.pose = Pose()
        self.twist = Twist()
        self.carState = CarState()
        self.shutdown_requested = False
            
    def poseCB(self, data):
        self.pose = data.pose
        
    def velCB(self, data):
        self.twist = data.twist
        
    def carStateCB(self, data):
        self.carState = data

    def sendCmd(self,type):
        #current state
        s = np.array([self.carState.Vx - self.ref, 
                      self.carState.Vx - self.carState.omegaR*RADIUS])
        theta = np.array([1.77136231, -29.81248081,   2.451615131]) # learned control gains
        a = getControl(s, self.ref, theta)
        print a*RADIUS
        print s
#         a = 40
        self.cmd.data = [type, a, 0.0]
        self.pub.publish(self.cmd)
    
    def runController(self,event):
        if not self.shutdown_requested:
            self.sendCmd(WHEEL_SPEED)

    def signal_handler(self, signal, frame):
        self.shutdown_requested = True
        for i in range(10):
            self.sendCmd(NOT_DRIVING)
            rospy.sleep(0.02)
        rospy.signal_shutdown('Ctrl+C shutdown requested')
        sys.exit(0)

def getControl(x, xref,theta):
    K = theta[0:KDIM]
    eta = theta[-1]
    sigma = eta2sigma(eta)
    mu = xref/RADIUS + K[0]*x[0] + K[1]*x[1]
    
#     xerr = np.zeros(len(x))
#      
#     for i in range(len(x)):
#         xerr[i] = x[i] - xref[i]
#     
#     xcontrol = np.zeros(KDIM)
#     xcontrol[0] = self.
    
#     print "K: " + str(K)
#     print "eta: " + str(eta)
#     print "x: " + str(x)
#     print "Kx: " + str(np.dot(K.transpose(),x))
    
    u = random.normalvariate(mu, sigma)
    return u

def eta2sigma(eta):
    sigma = 0.1 + NOISE/(1 + math.exp(eta))
    return sigma


if __name__ == '__main__':    
#     settings = termios.tcgetattr(sys.stdin)
#     if len(sys.argv) != 3:
#         print "Error: need to specify vehicle name and policy file as input arguments"
#     else:
#         vehName = sys.argv[1]
#         policyFile = sys.argv[2]
    vehName = "RC01"
    qvalFile = "/home/mark/RLPy/qval_car3.txt"
    print "Starting policy execution node for: " + vehName
    
    try:
        c = ExecutePolicy(vehName,qvalFile)
        signal.signal(signal.SIGINT, c.signal_handler)
        rospy.init_node('execute_policy',disable_signals=True)
        rospy.Subscriber(vehName+"/pose", PoseStamped, c.poseCB)
        rospy.Subscriber(vehName+"/vel", TwistStamped, c.velCB)
        rospy.Subscriber(vehName+"/state", CarState, c.carStateCB)
        
        rospy.sleep(0.2)
        for i in np.arange(10):
            c.sendCmd(RESET)
            rospy.sleep(0.01)
        
#             s = np.array([0., 0.])
#             c.returnBestAction(s)
                
        # set up 50 Hz callback timer
        rospy.Timer(rospy.Duration(1.0/50.0), c.runController)
        
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
