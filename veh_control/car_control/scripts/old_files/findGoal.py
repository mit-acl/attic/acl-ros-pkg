#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from visualization_msgs.msg import Marker
from std_msgs.msg import Float64MultiArray
import math
from numpy import *
from numpy.linalg import *

VELCMD = 0.5

class findGoal:

    
    def __init__(self, path):
        self.pose = PoseStamped()
        self.path = path
        self.pub = rospy.Publisher('/goal_marker', Marker)
        self.goalpub = rospy.Publisher('carCmd', Float64MultiArray)
        self.goal = Float64MultiArray()
        self.marker = Marker(type=Marker.ARROW,action=Marker.ADD)
        self.i = 0
        self.pathDone = False
        self.vInt = 0
        self.yInt = 0

    def initGoalMarker(self):
        self.marker.header.frame_id = "/world"
        self.marker.header.stamp = rospy.Time.now()
        self.marker.pose.position.x = 0.0
        self.marker.pose.position.y = 0.0
        self.marker.pose.position.z = 0.0
        
        self.marker.ns = "goals"
        self.marker.id = 1;
        
        self.marker.pose.orientation.x = 0
        self.marker.pose.orientation.y = 0
        self.marker.pose.orientation.z = 0.0
        self.marker.pose.orientation.w = 1
        self.marker.scale.x = 0.5
        self.marker.scale.y = 0.5
        self.marker.scale.z = 0.5
        self.marker.color.r = 0.0
        self.marker.color.g = 1.0
        self.marker.color.b = 0.0
        self.marker.color.a = 0.7
        self.marker.lifetime = rospy.Duration()

    def poseCallBack(self,data):
        self.pose = data
    
    def dist(self,p1,p2):
        x = p1[0] - p2[0]
        y = p1[1] - p2[1]
        return math.sqrt(x*x + y*y)
    
    # Super naive way of finding the closest point on the path to where you are
    # Just iterate through a discritized version of the path, checking the
    # distance at every point
    def findClosestPointonPath(self):
        currentPoint = [0,0]
        currentPoint[0] = self.pose.pose.position.x
        currentPoint[1] = self.pose.pose.position.y
        
        minDist = 10000000;
        bestInd = 0
        for i in range(0,len(self.path.poses)):
            p = self.path.poses[i]
            pPath = [0,0]
            pPath[0] = p.pose.position.x
            pPath[1] = p.pose.position.y
            
            d = self.dist(pPath,currentPoint)
            if d < minDist:
                minDist = d
                bestInd = i
                
        return bestInd
    
    def getStartLocation(self):
        bestInd = self.findClosestPointonPath()
        bestInd = bestInd+15
        if bestInd < 0:
            bestInd = 0
        elif bestInd > len(self.path.poses)-1:
            bestInd = len(self.path.poses)-1
        p = self.path.poses[bestInd]
        #p = self.path.poses[self.i]
        self.marker.pose = p.pose
        q0 = p.pose.orientation.w
        q1 = p.pose.orientation.x
        q2 = p.pose.orientation.y
        q3 = p.pose.orientation.z
        yaw = math.atan2(2.0*(q0*q3 + q1*q2), 1.0 - 2.0*(q2*q2 + q3*q3))
        while yaw > math.pi:
            yaw -= 2.0*math.pi
        while yaw < -math.pi:
            yaw += 2.0*math.pi
        
        xerr = p.pose.position.x - self.pose.pose.position.x
        yerr = p.pose.position.y - self.pose.pose.position.y
        # transform error into command frame
        velErr = xerr*math.cos(yaw) + yerr*math.sin(yaw)
        yawErr = -xerr*math.sin(yaw) + yerr*math.cos(yaw) 

        self.vInt += velErr*0.01
        self.yInt += yawErr*0.01
        
        kpvel = 0.3
        kivel = 0.05
        kpyaw = 1.5
        kiyaw = 0.5
        velc = VELCMD# + kpvel*velErr + kivel*self.vInt
        yawc = yaw + kpyaw*yawErr + kiyaw*self.yInt
        
        self.goal.layout.data_offset = ord(p.header.frame_id) # convert to ascii representation
        
        if p.header.seq:
            self.goal.data = [2, velc, yawc] # in this case you are commanding velocity and heading
        else:
            self.goal.data = [2, -velc, yawc] # in this case you are commanding velocity and heading
            
        if bestInd >= len(self.path.poses)-1:
            self.pathDone = True
            self.goal.data = [3, 0, 0]


    def broadcastGoal(self):
        self.pub.publish(self.marker)
        self.goalpub.publish(self.goal)

