#!/usr/bin/env python
'''
Description: Implements the pure math model of the single circuit rc car
            experiment

Created on Aug 14, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

# ROS imports
import roslib; roslib.load_manifest('car_control')
import rospy

# global imports
import random
import numpy as np

# local imports
import projectOnPath
import params

#==============================================================================
# Math model class -- prunes potential paths based on max velocity path time
#==============================================================================
class MathModel:

    def __init__(self):

        # set up default action space -- using three actions
        max_t13 = 4.0
        min_t13 = 0.1
        max_t24 = 4.0
        min_t24 = 0.1
        max_xcross = 1.0
        min_xcross = 0.1
        num_actions = 5

        # epsilon to pass up to the next level
        self.epsilon = 0.2

        # discretize the space
        self.t13 = np.linspace(min_t13, max_t13, num_actions)
        self.t24 = np.linspace(min_t24, max_t24, num_actions)
        self.xcross = np.linspace(min_xcross, max_xcross, num_actions)


#-----------------------------------------------------------------------------
# loop through the possible actions and prune out any that aren't feasible
# feasible times are initialized
#-----------------------------------------------------------------------------
    def pruneInitialActions(self):
        print "Starting initial action space pruning...\n"
        firstIndex = False
        self.actions_tried = np.array([])
        self.xbar = np.zeros((len(self.t13), len(self.t24), len(self.xcross)))
        self.n = np.ones((len(self.t13), len(self.t24),
                                       len(self.xcross)))
        pp = projectOnPath.projectOnPath()
        for i in range(len(self.t13)):
            for j in range(len(self.t24)):
                for k in range(len(self.xcross)):
                    # generate a polynomial with these inputs
                    poly = pp.createOval(self.xcross[k], self.t13[i], self.t24[j])
                    # find the max velocity for this polynomial path
                    max, min = poly.findMaxMinVelocity()
                    if (max < params.MAXVEL) and ((self.t13[i] + self.t24[j]) * 4 <
                                          params.MAXTIME):

                        # initial reward function is theoretical time to
                        # complete one circuit
                        self.xbar [i, j, k] = poly.T[-1]

                        if firstIndex:
                            self.actions_tried = np.append(self.actions_tried,
                                                np.array([[i, j, k]]), 0)
                        else:
                            self.actions_tried = np.array([[i, j, k]])
                            firstIndex = True


        # zero all of the actions that were not "tried" at the math model level
        self.n[self.xbar == 0] = 0

        # Normalize the times so the reward is between 0 and 1
        self.minTime = np.min(self.xbar [np.nonzero(self.xbar)])
        self.xbar [np.nonzero(self.xbar)] = (self.minTime /
                                            self.xbar [np.nonzero(self.xbar)])

        # add just a little noise to the initial xbar so we don't do actions in
        # numerical order -- essentially simulates using a random argmax for
        # equal initial reward functions in the runUCB algorithm
        noise = 0.01
        for i in range(self.xbar.size):
            i = np.unravel_index(i, self.xbar.shape)
            if self.xbar[i] != 0:
                self.xbar[i] += random.normalvariate(0, noise)

        self.numActions = len(self.t13) * len(self.t24) * len(self.xcross)
        self.numPruned = self.numActions - self.actions_tried.shape[0]

        print "n: " + str(self.n)
        print "xbar: " + str(self.xbar)
        print "Possible Actions: \n" + str(self.actions_tried)

        print ("\nPruning " + str(self.numPruned) + " of " + str(self.numActions)
                 + " (" + str(float(self.numPruned) / self.numActions * 100) + "%) actions")
        print "Min Time: " + str(self.minTime)


#-----------------------------------------------------------------------------
# Write pertinent data to file
#-----------------------------------------------------------------------------
    def write2file(self):

        # TODO: make this file relative to ROS package location
        packagePath = roslib.packages.get_dir_pkg('car_control')  # this doesn't work from launchfile
        packagePath = '/home/mark/acl-ros-pkg/veh_control/car_control'

        pureMathModel = open(packagePath + '/log/UCBpuremath.npz', 'w')

        # write pure math model to file
        np.savez(pureMathModel, t13=self.t13, t24=self.t24,
                  xcross=self.xcross, xbar=self.xbar, n=self.n,
                  act_tried=self.actions_tried, minTime=self.minTime,
                  epsilon=self.epsilon)

if __name__ == '__main__':
    try:
        rospy.init_node('math_model')
        c = MathModel()
        c.pruneInitialActions()
        c.write2file()
    except rospy.ROSInterruptException:
        pass
