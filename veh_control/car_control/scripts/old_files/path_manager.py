#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
from car_control.srv import *
from std_srvs.srv import *
import rospy
import sys
from wx._misc import Sleep

def OL_cmd_client(veh,t,omega,turn):
    rospy.wait_for_service('/' + veh + '/OL_cmd')
    try:
        OL_cmd = rospy.ServiceProxy('/' + veh + '/OL_cmd', OLcmd)
        resp = OL_cmd(t,omega,turn)
        return resp.finish
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e
        
def return_path_client(veh):
    rospy.wait_for_service('/' + veh + '/return_path')
    try:
        return_path = rospy.ServiceProxy('/' + veh + '/return_path', Empty)
        return_path()
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def mainFunc(vehName):
    for i in range(0,10):
        OL_cmd_client(vehName,4,100,0.1)
        rospy.sleep(2)
        return_path_client(vehName)
        rospy.sleep(2)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Error: need to specify vehicle name as input argument"
    else:
        vehName = sys.argv[1]
        try:
            mainFunc(vehName)
        except rospy.ROSInterruptException:
            pass
