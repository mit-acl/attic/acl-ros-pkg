#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from std_srvs.srv import Empty
import sys
import simple_path
import findGoal


def handle_return_path(req):
    
    # First, generate the path and broadcast it for rviz visualization
    s = simple_path.SimplePath()
    rospy.Subscriber("pose", PoseStamped, s.poseCallBack)
    while not s.foundPath:
        cnt =1
    r = rospy.Rate(10)
    i = 0
    while i < 3:
        s.broadcastPath()
        i += 1;
        r.sleep()
    
    # Second, parse that path and send individual commands to the vehicle
    c = findGoal.findGoal(s.path)
    c.initGoalMarker()
    rospy.Subscriber("pose", PoseStamped, c.poseCallBack)
    r = rospy.Rate(25)
    while not c.pathDone:
        c.getStartLocation()  
        c.broadcastGoal()
        r.sleep()
        
    return []

def returnPathServer():
    rospy.init_node('return_path_server')
    s = rospy.Service('return_path', Empty, handle_return_path)
    print "Ready to generate return to home paths"
    rospy.spin()

if __name__ == '__main__':
    try:
        returnPathServer()
    except rospy.ROSInterruptException:
        pass



