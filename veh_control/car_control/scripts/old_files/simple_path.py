#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
import math
from numpy import *
from numpy.linalg import *

XMAX = 3
XMIN = -XMAX
YMAX = 3
YMIN = -7
BOUND = 0.7

XMAXSOFT = XMAX - BOUND
XMINSOFT = XMIN + BOUND
YMAXSOFT = YMAX - BOUND
YMINSOFT = YMIN + BOUND

R = 0.8

SPACING = 0.02  # spacing in centimeters

class SimplePath:
    pub = rospy.Publisher('/simplePath', Path)
    path = Path()
    
    def __init__(self):
        self.x = []
        self.y = []
        self.xs = 0.0
        self.ys = 1.0
        self.xf = XMINSOFT
        self.yf = -2.4
        self.thetaf = 2
        self.pose = PoseStamped()
        self.foundPath = False
    
    def poseCallBack(self,data):
        self.pose = data
        if not self.foundPath:
            self.xf = self.pose.pose.position.x
            self.yf = self.pose.pose.position.y
            q0 = self.pose.pose.orientation.w
            q1 = self.pose.pose.orientation.x
            q2 = self.pose.pose.orientation.y
            q3 = self.pose.pose.orientation.z
            yaw = math.atan2(2.0*(q0*q3 + q1*q2), 1.0 - 2.0*(q2*q2 + q3*q3))
            self.thetaf = yaw
            self.returnPath()
            self.foundPath = True

    def returnPath(self):
        reverse = False
        if self.xf > 0:
            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
                reverse = True
        else:
            if (self.thetaf > math.pi/2.0) | (self.thetaf < -math.pi/2.0):
                reverse = True
        
        if self.xf > 0:
            x1 = self.xs + 2.0*R
            x2 = self.xs + 2.0*R
            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
                x3 = self.xs + R
            else:
                x3 = self.xs + 3.0*R
        else:
            x1 = self.xs - 2.0*R
            x2 = self.xs - 2.0*R
            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
                x3 = self.xs - 3.0*R
            else:
                x3 = self.xs - R
        
        y1 = self.ys
        y2 = self.yf + R
        y3 = self.yf
        
        d0 = arange(0.0, 0.0-self.ys, -SPACING)
        if self.xf > 0:
            s1 = arange(0.0, math.pi, SPACING/R)
            d2 = arange(0.0, y1-y2, SPACING)
            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
                s3 = arange(3.0/2.0*math.pi, 2.0*math.pi, SPACING/R)
                d4 = arange(0.0, x3-self.xf, -SPACING)
            else:
                s3 = arange(3.0/2.0*math.pi, math.pi, -SPACING/R)
                d4 = arange(x3-self.xf, 0.0, -SPACING)
        else:
            s1 = arange(math.pi, 0.0, -SPACING/R)
            d2 = arange(0.0, y1-y2, SPACING)
            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
                s3 = arange(3.0/2.0*math.pi, 2.0*math.pi, SPACING/R)
                d4 = arange(x3-self.xf, 0.0, -SPACING)
            else:
                s3 = arange(3.0/2.0*math.pi, math.pi, -SPACING/R)
                d4 = arange(0.0, x3-self.xf, SPACING)
        
#        print d0.shape
#        print s1.shape
#        print d2.shape
#        print s3.shape
#        print d4.shape
        
#        d0 = linspace(0.0, 0.0-self.ys, num=SPACING)
#        if self.xf > 0:
#            s1 = linspace(0.0, math.pi, num=SPACING)
#            d2 = linspace(0.0, y1-y2, num=SPACING)
#            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
#                s3 = linspace(3.0/2.0*math.pi, 2.0*math.pi, num=SPACING)
#                d4 = linspace(0.0, x3-self.xf, num=SPACING)
#            else:
#                s3 = linspace(3.0/2.0*math.pi, math.pi, num=SPACING)
#                d4 = linspace(x3-self.xf, 0.0, num=SPACING)
#        else:
#            s1 = linspace(math.pi, 0.0, num=SPACING)
#            d2 = linspace(0.0, y1-y2, num=SPACING)
#            if (self.thetaf < math.pi/2.0) & (self.thetaf > -math.pi/2.0):
#                s3 = linspace(3.0/2.0*math.pi, 2.0*math.pi, num=SPACING)
#                d4 = linspace(x3-self.xf, 0.0, num=SPACING)
#            else:
#                s3 = linspace(3.0/2.0*math.pi, math.pi, num=SPACING)
#                d4 = linspace(0.0, x3-self.xf, num=SPACING)
        
        X0 = self.xs*ones(d0.shape)
        if self.xf > 0:
            X1 = R*cos(s1) + self.xs + R
            X2 = x2*ones(d2.shape)
            X3 = R*cos(s3) + x3
            X4 = self.xf + d4
        else:
            X1 = R*cos(s1) + self.xs - R
            X2 = x2*ones(d2.shape)
            X3 = R*cos(s3) + x3
            X4 = self.xf + d4
        
        Y0 = self.ys + d0
        Y1 = R*sin(s1) + self.ys
        Y2 = y2 + d2
        Y3 = R*sin(s3) + y2
        Y4 = self.yf*ones(d4.shape)
        
        YAW0 = array([-math.pi/2.0*ones(d0.shape)])  # straight ahead
        Z0 = zeros(YAW0.shape)
        if self.xf > 0:
            YAW1 = array([s1])+math.pi/2.0
            Z1 = -ones(YAW1.shape)
        else:
            YAW1 = array([s1])-math.pi/2.0
            Z1 = ones(YAW1.shape)
        YAW2 = array([math.pi/2.0*ones(d2.shape)]) # straight
        Z2 = zeros(YAW2.shape)
        YAW3 = array([s3])-math.pi/2.0
        Z3 = ones(YAW3.shape)
        if (reverse & (self.xf > 0)) | ((reverse != True) & (self.xf < 0)):
            YAW3 = array([s3])+math.pi/2.0
            Z3 = -ones(YAW3.shape)
        YAW4 = array([math.pi*ones(d4.shape)])
        Z4 = zeros(YAW4.shape)
        if reverse & (self.xf > 0):
            YAW4 = array([0.0*ones(d4.shape)])
        elif (reverse == False) & (self.xf < 0):
            YAW4 = array([0.0*ones(d4.shape)])
        
        X = array([X4])
        X = append(X,[X3],axis=1)
        X = append(X,[X2],axis=1)
        X = append(X,[X1],axis=1)
        X = append(X,[X0],axis=1)
        
        Y = array([Y4])
        Y = append(Y,[Y3],axis=1)
        Y = append(Y,[Y2],axis=1)
        Y = append(Y,[Y1],axis=1)
        Y = append(Y,[Y0],axis=1)
        
        # use the z position to indicate if we are turning or not
        # 0 -> not turning
        # 1 -> turning to the right
        #-1 -> turning to the left
        Z = array(Z4)
        Z = append(Z,Z3,axis=1)
        Z = append(Z,Z2,axis=1)
        Z = append(Z,Z1,axis=1)
        Z = append(Z,Z0,axis=1)
        
        YAW = array(YAW4)
        YAW = append(YAW,YAW3,axis=1)
        YAW = append(YAW,YAW2,axis=1)
        YAW = append(YAW,YAW1,axis=1)
        YAW = append(YAW,YAW0,axis=1)
        
#        print X.shape
#        print Y.shape
#        print YAW.shape
        
        # reverse, 0 - reverse, 1 - forward
        if reverse:
            REV = array([zeros(d4.shape)])
        else:
            REV = array([ones(d4.shape)])
        REV = append(REV,[ones(s3.shape)],axis=1)
        REV = append(REV,[ones(d2.shape)],axis=1)
        REV = append(REV,[ones(s1.shape)],axis=1)
        REV = append(REV,[ones(d0.shape)],axis=1)
        
        self.path.header.frame_id = "/world"
        self.path.header.stamp = rospy.Time.now()
        for i in range(0,X.shape[1]):
            p = PoseStamped()
            p.pose.position.x = X[0][i]
            p.pose.position.y = Y[0][i]
            p.pose.orientation.w = math.cos(YAW[0][i]/2.0)
            p.pose.orientation.z = math.sin(YAW[0][i]/2.0)
            p.header.stamp = rospy.Time.now()
            if Z[0][i] == 0:
                p.header.frame_id = "s" # straight
            elif Z[0][i] == 1:
                p.header.frame_id = "r" # right
            elif Z[0][i] == -1:
                p.header.frame_id = "l" # left
            p.header.seq = REV[0][i]
            self.path.poses.append(p)

        
    def broadcastPath(self):
        self.pub.publish(self.path)
