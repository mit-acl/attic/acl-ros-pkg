#!/usr/bin/env python
'''
Description: Examine log files from mfrl data

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import numpy as np

f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log/log1.npz', 'r')

npzfile = np.load(f)
Q = npzfile['Q']
nr = npzfile['nr']
nt = npzfile['nt']
r = npzfile['r']
Rhat = npzfile['Rhat']
That = npzfile['That']
gamma = npzfile['gamma']
mr = npzfile['mr']
mt = npzfile['mt']
n_prime = npzfile['n_prime']
sa2s_prime = npzfile['sa2s_prime']
eps_d = npzfile['eps_d']

pass

