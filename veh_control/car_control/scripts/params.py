#!/usr/bin/env python
'''
Description: Commonly used parameters for the UCB hierachical learner

Created on Aug 14, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import math

# globals
YCENTER = -2.0
YDIST = 4.0
XCENTER = .3  # -6.0
X1 = XCENTER
X2 = XCENTER
Y1 = YCENTER + YDIST / 2.0
Y2 = YCENTER - YDIST / 2.0
OVERALL_ROT = 0 * math.pi / 2.0
TURN_DIR = 1  # 1 for counter clockwise, -1 for clockwise
MAXVEL = 4.5  # 5  # m/s
MINVEL = 0.5
DISC = 100
MAX_SEGMENT = 4

# RC01
# RADIUS = (0.035 / 2.0)  # wheel radius
# RC02
RADIUS = (0.0606 / 2.0)

DT = 1.0 / 200.0
CNTRL_RATE = 1.0 / 50.0
MAXTIME = 20
MAXSEGTIME = 3.0

# sample rate for ILC algorithm
SAMPLE_RATE = 1.0 / 50.0

# boundary box for out of bounds driving
YOUTOFBOUNDS1 = 6  # 4  # dist from YCENTER
XOUTOFBOUNDS1 = 3  # 1.8  # dist from XCENTER

YOUTOFBOUNDS2 = 4  # dist from YCENTER
XOUTOFBOUNDS2 = 2.2  # 1.8  # dist from XCENTER
