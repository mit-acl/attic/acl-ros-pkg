#!/usr/bin/env python
'''
Description: Plots for ICRA2014 car RL paper

Created on Sep 4, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

for i in range(2):
    if i == 0:
        f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log_r3_v2/mfrmax.p', 'rb')
    else:
        f = open('/home/mark/acl-ros-pkg/veh_control/car_control/log_r3_v2_just_top/mfrmax.p', 'rb')

    nMiss = 0
    p = pickle.load(f)
    r = p["reward"]
    n = len(r)
    x = np.arange(n)
    sumR = np.zeros(n)
    for i in np.arange(n):
        lenSeg = len(r[i])
        reward = np.sum(r[i])/3.0
        if lenSeg != 12:
            reward = -10
            nMiss += 1
        elif reward < -1000:
            reward = -10
            nMiss += 1

        sumR[i] = reward

    print "missed " + str(nMiss)+ " out of " + str(n) + " segments"
    print sumR
    plt.plot(x,sumR)
plt.show()
