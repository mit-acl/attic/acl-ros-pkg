#!/usr/bin/env python
import numpy as np
import cPickle as pickle
from log_data import LapData
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.offsetbox as osb
import matplotlib as mpl

import params
from aclpy import utils

# load the lap_data from running the car, plot the laps in a top down view

CONE1 = [-0.5, 0.2]
CONE2 = [-3.5, 0.2]


def plot_figure(data, lap_start, lap_end, file_name):
    fig, ax = plt.subplots()

    for lap in range(lap_start, lap_end):
        N = data[lap].i
        x = data[lap].x[0:N]
        y = data[lap].y[0:N]
        v = data[lap].v[0:N]
        v = v/vmax

        for i in xrange(N-1):
            ax.plot(y[i:i+2], x[i:i+2], color=plt.cm.jet(v[i]), linewidth=3.0)

    # plot boundary
    # bounding box points low left, high left, high right, low right
    x = np.array([params.XOUTOFBOUNDS2, -params.XOUTOFBOUNDS2])
    y = np.array([params.YOUTOFBOUNDS2, -params.YOUTOFBOUNDS2])
    x, y = utils.rotate2D(x, y, params.OVERALL_ROT)

    xmax = x[0] + params.XCENTER
    xmin = x[1] + params.XCENTER
    ymax = y[0] + params.YCENTER
    ymin = y[1] + params.YCENTER

    ax.plot([ymax, ymin], [xmax, xmax], color='gray', linewidth=4.0)
    ax.plot([ymax, ymin], [xmin, xmin], color='gray', linewidth=4.0)
    ax.plot([ymax, ymax], [xmax, xmin], color='gray', linewidth=4.0)
    ax.plot([ymin, ymin], [xmax, xmin], color='gray', linewidth=4.0)
    xbuffer = (xmax-xmin)/14.0
    ybuffer = (ymax-ymin)/14.0
    ax.set_ylim(xmin-xbuffer, xmax+xbuffer)
    ax.set_xlim(ymin-ybuffer, ymax+ybuffer)
    ax.set_ylabel('X Position (m)', size=20)
    ax.set_xlabel('Y Position (m)', size=20)
    ax.fill_between([ymax, ymin], [xmin,xmin], [xmin-xbuffer,xmin-xbuffer],
                    facecolor='gray', alpha=0.5, linewidth=0.0, edgecolor="none")
    ax.fill_between([ymax, ymin], [xmax+xbuffer,xmax+xbuffer],[xmax,xmax],
                    facecolor='gray', alpha=0.5, linewidth=0.0, edgecolor="none")
    ax.fill_between([ymax+ybuffer, ymax], [xmax+xbuffer,xmax+xbuffer],[xmin-xbuffer,xmin-xbuffer],
                    facecolor='gray', alpha=0.5, linewidth=0.0, edgecolor="none")
    ax.fill_between([ymin,ymin-ybuffer], [xmax+xbuffer,xmax+xbuffer],[xmin-xbuffer,xmin-xbuffer],
                    facecolor='gray', alpha=0.5, linewidth=0.0, edgecolor="none")

    plt.gca().invert_xaxis()

    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(18)


    # create a second axes for the colorbar
    ax2 = fig.add_axes([0.75, 0.2, 0.02, 0.6])
    cb = mpl.colorbar.ColorbarBase(ax2, cmap=plt.cm.jet,
                                   spacing='proportional',
                                   format='%1.1f',
                                   boundaries=np.linspace(vmin,
                                                          vmax, 256))

    ax2.set_ylabel('Velocity (m/s)', size=18)

    # plot cones
    cone_file = 'cone_side.png'
    cone_img = plt.imread(cone_file)
    imagebox = osb.OffsetImage(cone_img, zoom=0.11)
    ab = osb.AnnotationBbox(imagebox, CONE1, frameon=False)
    ax.add_artist(ab)
    ab = osb.AnnotationBbox(imagebox, CONE2, frameon=False)
    ax.add_artist(ab)

    # add a note showing where the drivable region is
    ax.annotate('Allowable Area', xy=(1.75, 2.2),
                fontsize=18)

    plt.savefig(file_name, format='pdf', transparent=True, bbox_inches='tight')
    #plt.show()



# load lap data
pkl = open('../log/state_r4_v6_dbn_2/lap_data.pkl', 'rb')
data = pickle.load(pkl)

num_laps =  len(data)

# fig= plt.figure()
# ax = fig.gca(projection='3d')

lap = 2

vmin = 100
vmax = 0

lap_start1 = 0 #num_laps-4
lap_end1 = 10 #num_laps-1
for lap in range(lap_start1, lap_end1):
    print lap
    N = data[lap].i
    v = data[lap].v[0:N]
    if max(v) > vmax:
        vmax = max(v)
    if min(v) < vmin:
        vmin = min(v)

lap_start2 = num_laps-4
lap_end2 = num_laps-1
for lap in range(lap_start2, lap_end2):
    print lap
    N = data[lap].i
    v = data[lap].v[0:N]
    if max(v) > vmax:
        vmax = max(v)
    if min(v) < vmin:
        vmin = min(v)

plot_figure(data, lap_start1, lap_end1, '../figures/car_start_learning.pdf')
plot_figure(data, lap_start2, lap_end2, '../figures/car_end_learning.pdf')
