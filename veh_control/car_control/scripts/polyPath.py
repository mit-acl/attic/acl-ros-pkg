#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from nav_msgs.msg import Path
import polygen
import numpy as np
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import MultiArrayDimension
import matplotlib.pyplot as plt
from car_control.msg import PolyInputs


class PolyPath:


    def __init__(self, polyInputs):

        # process PolyInputs message
        n, wp, wp0, wpf, wp_vel, wp_elm, tt = processPolyInputs(polyInputs)

        self.T = np.zeros(len(tt) + 1)
        for i in range(len(tt)):
            self.T[i + 1] = self.T[i] + tt[i]

        self.polyS = np.poly1d([])
        # generate the polynomials
        if n == 4:
            self.polyX, self.polyY, self.polyZ, self.polyS = polygen.generatePolynomial(n, wp, wp0, wpf, wp_vel, wp_elm, tt)
        else:
            self.polyX, self.polyY, self.polyZ = polygen.generatePolynomial(n, wp, wp0, wpf, wp_vel, wp_elm, tt)

        self.x = np.array([])
        self.y = np.array([])
        self.z = np.array([])
        self.time = np.array([])

    # use the polynomial coefficients to discretize the path so it can be plotted
    # return the nth derivative of the path (if n is not zero)
    # num_seg - Number of segments to discretize that particular segment into
    # n - derivative of the path to return (1 = vel, 2 = accel, etc)
    def discretizePath(self, num_seg, n=0):
        X = np.array([])
        Y = np.array([])
        Z = np.array([])
        T = np.array([])
        for i in range(len(self.T) - 1):
            t = np.linspace(self.T[i], self.T[i + 1], num_seg, False)  # discretized time along this segment
            x = np.polyder(self.polyX[i], n)(t)
            y = np.polyder(self.polyY[i], n)(t)
            z = np.polyder(self.polyZ[i], n)(t)

            # increment the total x,y,z vectors
            X = np.append(X, x)
            Y = np.append(Y, y)
            Z = np.append(Z, z)
            T = np.append(T, t)

        if n == 0:
            self.x = X
            self.y = Y
            self.z = Z
            self.time = T
        else:
            return X, Y, Z, T

    # create a numeric representation of speed
    def discretizeSpeed(self, n):
        # make sure polyS got calculated
        S = np.array([])
        if self.polyS.size:
            for i in range(len(self.T) - 1):
                t = np.linspace(self.T[i], self.T[i + 1], n, False)
                s = np.sqrt(self.polyS[i](t))
                S = np.append(S, s)
            return S
        else:
            return 0

    # create a numeric representation of linear acceleration
    def discretizeLinAccel(self, n):
        # make sure polyS got calculated
        A = np.array([])
        if self.polyS.size:
            for i in range(len(self.T) - 1):
                t = np.linspace(self.T[i], self.T[i + 1], n, False)
                num = np.polyder(self.polyS[i])(t)
                den = 2.0 * np.sqrt(self.polyS[i](t))
                a = num / den
                A = np.append(A, a)
            return A
        else:
            return 0

    # finding when the acceleration is zero helps us know candidate positions for max/min velocities
    def findAccelRoots(self):
        velCheckPnts = np.array([])
        for segment in range(len(self.T) - 1):
            segStart = self.T[segment]
            segEnd = self.T[segment + 1]

            # solve for the roots of the acceleration polynomial (assuming cubic splines)
            roots = np.roots(np.polyder(self.polyS[segment]))

            # check if these roots within the allowable segment time window
            # if not, output the segment end points instead
            pnts = np.array([])
            for i in range(len(roots)):
                if roots[i] >= segStart and roots[i] <= segEnd and np.fabs(np.imag(roots[i])) < 0.1:
                    pnts = np.append(pnts, np.real(roots[i]))

            if pnts.size < 3:
                # add endpoints
                pnts = np.append(pnts, np.array([segStart, segEnd]))

            # add to list of velocity check points
            velCheckPnts = np.append(velCheckPnts, pnts)

        # get rid of duplicate entries
        velCheckPnts = np.unique(velCheckPnts)

        return velCheckPnts

    # use the acceleration zeros and endpoints to the find the max/min velocity of a path
    def findMaxMinVelocity(self):

        # find possible max's and min's by finding roots of acceleration
        velCheckPnts = self.findAccelRoots()

        # check speed at these roots and return the max and min
        maxVel = 0.0
        minVel = 1000.0
        for i in range(len(velCheckPnts)):
            # get segment to which this checkPoint belongs
            segment = np.argwhere(self.T <= velCheckPnts[i])
            segment = segment[-1]
            # saturate the segments (only happens for very last endpoint)
            if segment == len(self.T) - 1:
                segment = segment - 1

            # get speed at this checkpoint
            speed = np.sqrt(self.polyS[segment[0]](velCheckPnts[i]))
            if speed > maxVel:
                maxVel = speed
            if speed < minVel:
                minVel = speed

        return maxVel, minVel

#-----------------------------------------------------------------------------
# use matplotlib to plot the discretized version of the path (including
# velocity, acceleration, and jerk)
#-----------------------------------------------------------------------------
    def plotPath(self):
        plt.subplot(311)
        plt.plot(self.time, self.x, label='x')
        plt.plot(self.time, self.y, label='y')
        plt.plot(self.time, self.z, label='z')
        plt.legend()
        plt.subplot(312)
        dx, dy, dz, t = self.discretizePath(100, 1)
        s = self.discretizeSpeed(100)
        plt.plot(t, dx, label='dx')
        plt.plot(t, dy, label='dy')
        plt.plot(t, dz, label='dz')
        plt.plot(t, s, label='speed')
        plt.legend()
        plt.subplot(313)
        ddx, ddy, ddz, t = self.discretizePath(100, 2)
        a = self.discretizeLinAccel(100)
        plt.plot(t, ddx, label='ddx')
        plt.plot(t, ddy, label='ddy')
        plt.plot(t, ddz, label='ddz')
        plt.plot(t, a, label='accel')
        plt.legend()

        # turn on interactive mode and plot the figure
#         plt.ion()
        plt.show()

#-----------------------------------------------------------------------------
# Convert standard inputs to PolyInputs message type
#-----------------------------------------------------------------------------
def createPolyInputs(n, wp, wp0, wpf, wp_vel, wp_elm, tt):
    # form inputs into PolyInputs
    polyInputs = PolyInputs()
    polyInputs.n = n
    numpyArray2DToMultiarray(wp, polyInputs.wp)
    numpyArray2DToMultiarray(wp0, polyInputs.wp0)
    numpyArray2DToMultiarray(wpf, polyInputs.wpf)
    numpyArray2DToMultiarray(wp_vel, polyInputs.wp_vel)
    polyInputs.wp_elm = wp_elm
    polyInputs.tt = tt

    return polyInputs

#-----------------------------------------------------------------------------
# Convert PolyInputs message type to numpy arrays in standard format
#-----------------------------------------------------------------------------
def processPolyInputs(polyInputs):
    n = polyInputs.n
    wp = multiarray2DToNumpyArray(polyInputs.wp)
    wp0 = multiarray2DToNumpyArray(polyInputs.wp0)
    wpf = multiarray2DToNumpyArray(polyInputs.wpf)
    wp_vel = multiarray2DToNumpyArray(polyInputs.wp_vel)
    wp_elm = np.array(polyInputs.wp_elm)
    tt = np.array(polyInputs.tt)

    return n, wp, wp0, wpf, wp_vel, wp_elm, tt

#-----------------------------------------------------------------------------
# 2D Multiarray to numpy array -- convert multiarray data type to a numpy array
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def multiarray2DToNumpyArray(ma):
    I = ma.layout.dim[0].size
    J = ma.layout.dim[1].size
    na = np.empty([I, J])
    for i in range(I):
        for j in range(J):
            index = ma.layout.data_offset + ma.layout.dim[1].stride * i + j
            na[i, j] = ma.data[index]
    return na

#-----------------------------------------------------------------------------
# 2D numpy array to multiarray
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def numpyArray2DToMultiarray(na, ma):
    I = na.shape[0]
    J = na.shape[1]
    d1 = MultiArrayDimension()
    d2 = MultiArrayDimension()
    ma.layout.dim = [d1, d2]
    ma.layout.dim[0].size = I
    ma.layout.dim[0].stride = I * J
    ma.layout.dim[1].size = J
    ma.layout.dim[1].stride = J
    for i in range(I):
        ma.data = np.append(ma.data, na[i, :])

if __name__ == '__main__':

    # # sample inputs
    xcross = 0.5
    t13 = 3.2
    t24 = 1.2

    n = 4
    wp = np.array([[-xcross, -xcross, xcross, xcross, -xcross],
                        [0, -4, -4, 0, 0],
                        [0, 0, 0, 0, 0]])
    wp0 = np.array([[], [], []])
    wpf = np.array([[], [], []])
    wp_vel = np.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
    wp_elm = np.array([0, 1, 2, 3])
    tt = np.array([t13, t24, t13, t24])
    # # end inputs

    polyInputs = createPolyInputs(n, wp, wp0, wpf, wp_vel, wp_elm, tt)

    c = PolyPath(polyInputs, 'test')
    print c.polyX
    print c.polyY
    print c.polyZ
    c.discretizePath(100)
    c.plotPath()
