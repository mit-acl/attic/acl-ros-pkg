#!/usr/bin/env python
import math
import numpy as np

# returns poly1d elements for x, y, and z dimensions
# todo: document parameters
# todo: check inputs for correctness
# todo: make it generic for all polys, not just cubic splines
#        note: most of the code is generic, just parts, like velocity constraints are not
def generatePolynomial(n, wp, wp0, wpf, wp_vel, wp_elm, tt):

    num_seg = wp.shape[1] - 1  # number of segments
    m = n * num_seg  # number of constraints

    # convert relative time to absolute time -- assumed that t0 = 0
    t = np.zeros(num_seg + 1)
    for kk in range(0, num_seg):
        t[kk + 1] = tt[kk] + t[kk]

    # basis vectors
    numder = n - 1
    x = np.zeros((numder, n))
    x[0, :] = np.ones(n)

    for jj in range (1, numder):
        x[jj, jj:n] = _polyder(x[jj - 1, jj - 1:n])

    # form constraint matrices
    Ax, bx = _findX(wp[0, :], wp0[0, :], wpf[0, :], wp_vel[0, :], wp_elm, x, n, m)
    Ay, by = _findX(wp[1, :], wp0[1, :], wpf[1, :], wp_vel[1, :], np.array([]), x, n, m)
    Az, bz = _findX(wp[2, :], wp0[2, :], wpf[2, :], wp_vel[2, :], np.array([]), x, n, m)

    # fill constraint matrices with time elements and solve for unknown coefficients
    XX = _fill_A(Ax, bx, wp_elm, wp0[0, :], n, m, t)
    YY = _fill_A(Ay, by, np.array([]), wp0[1, :], n, m, t)
    ZZ = _fill_A(Az, bz, np.array([]), wp0[2, :], n, m, t)

    # reformat the data as poly1d types
    X = np.empty(num_seg, np.poly1d)
    Y = np.empty(num_seg, np.poly1d)
    Z = np.empty(num_seg, np.poly1d)
    for i in range(0, num_seg):
        X[i] = np.poly1d(XX[i, :])
        Y[i] = np.poly1d(YY[i, :])
        Z[i] = np.poly1d(ZZ[i, :])


    # print the resulting polynomials
#     print "X-dimension: "
#     for i in range(0,num_seg):
#         print X[i]
#     print "Y-dimension: "
#     for i in range(0,num_seg):
#         print Y[i]
#     print "Z-dimension: "
#     for i in range(0,num_seg):
#         print Z[i]

    if n == 4:
        S = np.empty(num_seg, np.poly1d)
        for i in range(0, num_seg):
            S[i] = _speedPoly(X[i], Y[i], Z[i], n)
        return X, Y, Z, S
    else:
        return X, Y, Z

# convert the x,y,z velocity polynomials into a linear speed polynomial
# -- this is used to calculate the max/min speed along a path
# x,y,z - poly1d data types representing x,y,z position of a specific segment
# NOTE1: output of this function is the speed squared polynomial, therefore
#    to get the actual speed, compute
#    speed(t) = sqrt(polyval(_speedPoly(dx,dy,dz)))
# NOTE2: this is explicitly derived for the n=4 case (cubic splines)
#        for other polynomials, computing the numerical speed of the discretized
#        path may be easier
def _speedPoly(dx, dy, dz, n):
    if n != 4:
        print "Error: _speedPoly assumes n = 4"
        return

    # speed = sqrt(dx**2 + dy**2 + dz**2), where dx = 3*ax*t**2 + 2*bx*t + cx
    # get coefficients
    ax = dx[3]; ay = dy[3]; az = dz[3]
    bx = dx[2]; by = dy[2]; bz = dz[2]
    cx = dx[1]; cy = dy[1]; cz = dz[1]

    p4 = 9 * (ax ** 2 + ay ** 2 + az ** 2)
    p3 = 12 * (ax * bx + ay * by + az * bz)
    p2 = 4 * (bx ** 2 + by ** 2 + bz ** 2) + 6 * (ax * cx + ay * cy + az * cz)
    p1 = 4 * (bx * cx + by * cy + bz * cz)
    p0 = cx ** 2 + cy ** 2 + cz ** 2

    speed = np.poly1d([p4, p3, p2, p1, p0])
    return speed

def _findX(wp, x0, xf, xm_acc, wp_elm, x, n, m):

    # Form constraints
    Aeq = np.zeros((m, m))

    # initial conditions - pos, vel, acc, jerk, snap
    A1 = np.zeros((n / 2, n))
    for k in range(0, n / 2):
        A1[k, n - k - 1] = math.factorial(k)  # initial position constraint (assumed t0 = 0)

    #         A1(1,n)   = 1 # initial position constraint (assumed t0 = 0)
    #         A1(2,n-1) = 1 # initial velocity constraint
    #         A1(3,n-2) = 2 # initial acceleration constraint
    #         A1(4,n-3) = 6 # initial jerk constraint
    #         A1(5,n-4) = 24# initial snap constraint

    # final conditions - pos, vel, acc, jerk, snap
    An = np.zeros((n / 2, n))
    for k in range(0, n / 2):
        An[k, 0:n - k] = x[k, k:n]


    # assign initial and final conditions to big A matrix
    Aeq[0:n / 2, 0:n] = A1
    Aeq[m - n / 2:m, m - n:m] = An

    if not x0 and not (0 == wp_elm).any():  # if you are not specifying initial derivative constraints,
                #  make sure initial and final derivative conditions match up
        for k in range(1, n / 2):
            # upper section will contain first derivatives,
            # lower section will contain the rest
            Aeq[k, m - n:m] = -Aeq[m - n / 2 + k, m - n:m].copy()
            Aeq[m - n / 2 + k, n - k - 2] = math.factorial(k + 1)
            Aeq[m - n / 2 + k, m - n:m] = np.zeros((1, n))
            Aeq[m - n / 2 + k, m - n:m - k - 1] = -x[k + 1, k + 1:n]


    # middle continuity constraints and position constraints
    for k in range(1, m / n):  # =2:m/n
        if (k == wp_elm).any():
            cntr = 4
            Aij = np.zeros((n, 2 * n))
            Aij[0, 0:n] = x[0, :]  # position constraint x1[t1] = x1_f
            Aij[1, n:2 * n] = x[0, :]  # position constraint x2[t1] = x2_0
            Aij[2, 0:n - 1] = x[1, 1:n]  # velocity constraint
            Aij[3, n:2 * n - 1] = x[1, 1:n]  # velocity constraint

    #         for p=3:n
    #             if p~=3
    #                 Aij(cntr,1:n-p+2)     =  x(p-1,p-1:n)#_fill_poly(x(p-1,p-1:n),t(k))# continuous velocity
    #                 Aij(cntr,n+1:2*n-p+2) = -x(p-1,p-1:n)#-_fill_poly(x(p-1,p-1:n),t(k))# continuous velocity
    #                 cntr = cntr+1
    #
    #
        else:
            Aij = np.zeros((n, 2 * n))
            Aij[0, 0:n] = x[0, :]  # _fill_poly[x[1,:],t[k]]# position constraint x1[t1] = x1_f
            Aij[1, n:2 * n] = x[0, :]  # _fill_poly[x[1,:],t[k]]# position constraint x2[t1] = x2_0
            for p in range(2, n):  # =3:n
                Aij[p, 0:n - p + 1] = x[p - 1, p - 1:n]  # _fill_poly[x[p-1,p-1:n],t[k]]# continuous velocity
                Aij[p, n:2 * n - p + 1] = -x[p - 1, p - 1:n]  # -_fill_poly(x(p-1,p-1:n],t(k]]# continuous velocity


        # add Aij to big A matrix
        Aeq[n / 2 + (k - 1) * n:n / 2 + k * n, (k - 1) * n:k * n + n] = Aij


    beq = np.zeros(m)
    beq[0] = wp[0]
    if x0:
        beq[1:n / 2] = x0.transpose()
    elif (0 == wp_elm).any():
        beq[1] = xm_acc[0]
    # else -- should remain zero
    cntr = 0
    for k in range(0, m / n - 1):  # =0:m/n-2
        if (k == wp_elm - 1).any():
            beq[k * n + n / 2] = wp[k + 1]
            beq[k * n + n / 2 + 1] = wp[k + 1]
            beq[k * n + n / 2 + 2] = xm_acc[cntr]
            beq[k * n + n / 2 + 3] = xm_acc[cntr]
            cntr = cntr + 1
        else:
            beq[k * n + n / 2] = wp[k + 1]
            beq[k * n + n / 2 + 1] = wp[k + 1]


    beq[m - n / 2] = wp[m / n]
    if xf:
        beq[m - n / 2 + 1:m] = xf.transpose()
    elif (0 == wp_elm).any():
        beq[m - n / 2 + 1] = xm_acc[0]
    # else -- should remain zero

    return Aeq, beq

def _fill_A(Aeq, beq, wp_elm, x0, n, m, t):

    # fill Aeq
    # final conditions
    if x0 or (0 == wp_elm).any():
        Aeq[m - n / 2:m, m - n:m] = _fill_poly(Aeq[m - n / 2:m, m - n:m], t[-1])
    else:  # need to fit final and inital conditions as intermediate conditions
        Aeq[m - n / 2, m - n:m] = _fill_poly(Aeq[m - n / 2, m - n:m], t[-1])  # final position
        for k in range(1, n / 2):
            Aeq[k, m - n:m - k] = _fill_poly(Aeq[k, m - n:m - k], t[-1])
            Aeq[m - n / 2 + k, m - n:m - k - 1] = _fill_poly(Aeq[m - n / 2 + k, m - n:m - k - 1], t[-1])

    # intermediate conditions - no good way to explain the indexing,
    # sorry!
    for k in range(1, m / n):  # =2:m/n
        if (k == wp_elm).any():
            Aeq[n / 2 + 0 + (k - 1) * n, (k - 1) * n:k * n] = _fill_poly(Aeq[n / 2 + 0 + (k - 1) * n, (k - 1) * n:k * n], t[k])  # first pos constraint
            Aeq[n / 2 + 1 + (k - 1) * n, k * n:(k + 1) * n] = _fill_poly(Aeq[n / 2 + 1 + (k - 1) * n, k * n:(k + 1) * n], t[k])  # second pos constraint
            Aeq[n / 2 + 2 + (k - 1) * n, (k - 1) * n:k * n - 1] = _fill_poly(Aeq[n / 2 + 2 + (k - 1) * n, (k - 1) * n:k * n - 1], t[k])  # first vel constraint
            Aeq[n / 2 + 3 + (k - 1) * n, k * n:(k + 1) * n - 1] = _fill_poly(Aeq[n / 2 + 3 + (k - 1) * n, k * n:(k + 1) * n - 1] , t[k])  # second vel constraint
    #         Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n-3) = _fill_poly(Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-2)*n+1:(k-1)*n-3),t(k))
    #         Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-1)*n+1:k*n-3)     = _fill_poly(Aeq(n/2+5+(k-2)*n:n/2+(k-1)*n, (k-1)*n+1:k*n-3),t(k))
        else:
            Aeq[n / 2 + 0 + (k - 1) * n, (k - 1) * n:k * n] = _fill_poly(Aeq[n / 2 + 0 + (k - 1) * n, (k - 1) * n:k * n], t[k])
            Aeq[n / 2 + 1 + (k - 1) * n:n / 2 + k * n, (k - 1) * n:k * n] = _fill_poly(Aeq[n / 2 + 1 + (k - 1) * n:n / 2 + k * n, (k - 1) * n:k * n], t[k])
            Aeq[n / 2 + 1 + (k - 1) * n:n / 2 + k * n, k * n:(k + 1) * n] = _fill_poly(Aeq[n / 2 + 1 + (k - 1) * n:n / 2 + k * n, k * n:(k + 1) * n], t[k])

    out = np.linalg.solve(Aeq, beq)  # closed-form solution!
    out = np.reshape(out, (-1, n))

    return out

# fill a polynomial vector with argument t
def _fill_poly(p, t):
    # p is a two dimensional vector

    n = p.shape[0]
    if len(p.shape) > 1:
        m = p.shape[1]
    else:
        n = 1
        m = p.shape[0]
    pow = np.zeros((n, m))
    for i in range(0, n):
        pow[i, 0:m - i] = np.arange(m - i - 1, -1, -1)

    out = p * t ** pow
    return out



# Compute the derivative of a polynomial
def _polyder(p):
    n = len(p)
    power = range(n - 1, -1, -1)
    dp = np.zeros(n - 1)
    for i in range(0, n - 1):
        dp[i] = p[i] * power[i]
    return dp

if __name__ == '__main__':

    # # sample inputs
    n = 4
    xcross = 0.2
    wp = np.array([[-xcross, -xcross, xcross, xcross],
                        [2, -2, -2, 2],
                        [0, 0, 0, 0]])
    wp0 = np.array([[0], [0], [0]])
    wpf = np.array([[0], [0], [0]])
    wp_vel = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    wp_elm = np.array([1, 2, 3])
    tt = np.array([3, 1, 3, 3])
    # # end inputs

    x, y, z, s = generatePolynomial(n, wp, wp0, wpf, wp_vel, wp_elm, tt)
    print x
    print y
    print z
