#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
import numpy as np
import scipy as sp
import scipy.spatial
import polyPath
import pure_pursuit as pp
import copy
import params
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from std_msgs.msg import Bool
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
from car_control.msg import CarGoal
from car_control.msg import PolyInputs
from nav_msgs.msg import Path
import math
from aclpy import utils

"""Project your current position onto a path parameterized by polynomials"""
class projectOnPath:

    def __init__(self):

        # initialize marker data
        self.marker = Marker(type=Marker.ARROW, action=Marker.ADD)
        self.pathmarker1 = Marker(type=Marker.LINE_STRIP, action=Marker.ADD)
        self.pathmarker2 = Marker(type=Marker.LINE_STRIP, action=Marker.ADD)
        self.initMarker(self.marker)
        self.initMarker(self.pathmarker1)
        self.initMarker(self.pathmarker2)
        self.marker.color.a = 1.0
        self.pathmarker1.id += 1
        self.pathmarker2.id += 2
        self.pathmarker1.scale.x = 0.035
        self.pathmarker1.color.r = 0
        self.pathmarker1.color.g = 1
        self.pathmarker1.color.a = 1
        self.pathmarker2.scale.x = 0.035
        self.pathmarker2.color.a = 0.2
        self.pathmarker2.color.r = .8
        self.pathmarker2.color.g = .8
        self.pathmarker2.color.b = .8
        self.pubMarker = rospy.Publisher('/RAVEN_world', MarkerArray)

        # goal/pose data
        self.pose = PoseStamped()
        self.psi = 0
        self.twist = TwistStamped()
        self.goal = CarGoal()
        self.pubGoal = rospy.Publisher('goal', CarGoal)
        self.pubPathComplete = rospy.Publisher('path_percent', Float64)

        # L1 pure pursuit controller
        self.L1_nom = 1.3
        self.vcmd_min = 2.2 #3.0
        self.L1_gain = self.L1_nom / self.vcmd_min
        self.pp = pp.PurePursuit(np.array([[0, 0, 0, 0]]), self.L1_nom, self.L1_gain)

        # Mutex should prevent us from looking up purepursuit data as
        # the purepursuit module is getting updated in a threaded callback
        self.ppMutex = False

        # allows you to stop the car when you don't want it driving
        ns = rospy.get_namespace()
        if ns[-2] == 's':  # ns = /RC02s/
            self.shouldDrive = True  # simulated vehicle
        else:
            self.shouldDrive = False  # real vehicle

        self.commanded_radius = np.Inf
        self.currentSegment = 0
        self.startTime = 0
        self.percent_complete = 0

        # ensure that path data is available to late subscribers by setting
        # latch to true
        self.pub1 = rospy.Publisher("/polyPath1", Path, latch=True)
        self.pub = rospy.Publisher("/polyPath", Path, latch=True)

    def createOvalPathCB(self, data):

        xcross = data.data[0]
        t13 = data.data[1]
        t24 = data.data[2]

        self.poly = self.createOval(xcross, t13, t24)

        self.poly.discretizePath(params.DISC)
        path = createPath(self.poly.x, self.poly.y, self.poly.z)
        broadcastPath(self.pub, path)

        # compute speed commands along path
        dx, dy, dz, t = self.poly.discretizePath(params.DISC, 1)
        speed = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
        max, min = self.poly.findMaxMinVelocity()

#         self.poly.plotPath()

        # Initialize L1 controller
        data = np.zeros((len(self.poly.x), 4))
        data[:, 0] = self.poly.x
        data[:, 1] = self.poly.y
        data[:, 2] = self.poly.z
        data[:, 3] = speed
        s = rospy.get_time()
        self.ppMutex = True
        self.pp = pp.PurePursuit(data, self.L1_nom, self.L1_gain)
        self.ppMutex = False
        rospy.logdebug("KD-tree build takes %s seconds", str(rospy.get_time() - s))

    def createOval(self, xcross, t13, t24):

        # # inputs
        n = 4
        ymax = params.YCENTER + params.YDIST / 2
        ymin = params.YCENTER - params.YDIST / 2
        wp = np.array([[-xcross + params.XCENTER, -xcross + params.XCENTER,
                        xcross + params.XCENTER,
                         xcross + params.XCENTER, -xcross + params.XCENTER],
                            [ymax, ymin, ymin, ymax, ymax],
                            [0, 0, 0, 0, 0]])
        wp0 = np.array([[], [], []])
        wpf = np.array([[], [], []])
        wp_vel = np.array([[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
        wp_elm = np.array([0, 1, 2, 3])
        tt = np.array([t13, t24, t13, t24])
        # # end inputs

        # form inputs into PolyInputs
        polyInputs = polyPath.createPolyInputs(n, wp, wp0, wpf, wp_vel, wp_elm, tt)

        # run and discretize path
        return polyPath.PolyPath(polyInputs)

    def createPolyPathCB(self, data):

        firstHalf = False
        if data.n == -1:
            firstHalf = True
            data.n = 4

        # run and discretize path
        self.poly = polyPath.PolyPath(data)
        self.poly.discretizePath(params.DISC)
        path = createPath(self.poly.x, self.poly.y, self.poly.z)
        broadcastPath(self.pub1, path)

#         compute speed commands along path
        dx, dy, dz, t = self.poly.discretizePath(params.DISC, 1)
        speed = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
        max, min = self.poly.findMaxMinVelocity()

        # Initialize L1 controller
        if firstHalf:
            data = np.zeros((len(self.poly.x) / 2, 4))
            data[:, 0] = self.poly.x[0:len(self.poly.x) / 2]
            data[:, 1] = self.poly.y[0:len(self.poly.x) / 2]
            data[:, 2] = self.poly.z[0:len(self.poly.x) / 2]
            data[:, 3] = speed[0:len(self.poly.x) / 2]
            self.ppMutex = True
            self.pp = pp.PurePursuit(data, self.L1_nom, self.L1_gain)
            self.ppMutex = False
        else:
            data = np.zeros((len(self.poly.x) * .75, 4))
            data[:, 0] = self.poly.x[len(self.poly.x) / 4:]
            data[:, 1] = self.poly.y[len(self.poly.x) / 4:]
            data[:, 2] = self.poly.z[len(self.poly.x) / 4:]
            data[:, 3] = speed[len(self.poly.x) / 4:]
            self.ppMutex = True
            self.pp = pp.PurePursuit(data, self.L1_nom, self.L1_gain)
            self.ppMutex = False

    def createPathCB(self, data):

        path = polyPath.multiarray2DToNumpyArray(data)

        self.pathmarker1.points = createPath(path[0, :], path[1, :], path[2, :])
        self.pathmarker1.color.g = (2.2 - 2) / 1.5
        # broadcast path marker for visualization purposes
        ma = MarkerArray()
        ma.markers.append(self.pathmarker1)
        self.pubMarker.publish(ma)

        self.ppMutex = True
        self.pp = pp.PurePursuit(path.transpose(), self.L1_nom, self.L1_gain)
        self.ppMutex = False

    def createBarePath(self, data):

        self.pathmarker1.points = createPath(data[0, :], data[1, :], data[2, :])
        self.pathmarker1.color.g = 1.0 #(2.2 - 2) / 1.9
        # broadcast path marker for visualization purposes
        ma = MarkerArray()
        ma.markers.append(self.pathmarker1)
        self.pubMarker.publish(ma)

        self.ppMutex = True
        self.pp = pp.PurePursuit(data, self.L1_nom, self.L1_gain)
        self.ppMutex = False

    def createPathSegmentCB(self, data):

        segment = data.data[0]
        radius = data.data[1]
        prev_radius = data.data[2]
        velocity = data.data[3]
        x, y, dist = self.createPathSegment(segment, radius, prev_radius)
        rospy.logdebug("Writing segment %s", segment)

        path1 = np.array([x, y, np.zeros(x.shape), velocity * np.ones(x.shape)])

        x, y, dist = self.createPathSegment(segment + 1, radius, radius)
        path2 = np.array([x, y, np.zeros(x.shape), velocity * np.ones(x.shape)])
        x, y, dist = self.createPathSegment(segment + 2, radius, radius)
        path3 = np.array([x, y, np.zeros(x.shape), velocity * np.ones(x.shape)])

#        path2 = np.append(path2, path3, 1)

        self.pathmarker1.points = createPath(path1[0, :], path1[1, :], path1[2, :])
        self.pathmarker2.points = createPath(path2[0, :], path2[1, :], path2[2, :])
        self.pathmarker1.color.g = 1.0 #(velocity - 1.8) / 1.9
        # broadcast path marker for visualization purposes
        ma = MarkerArray()
        ma.markers.append(self.pathmarker1)
        ma.markers.append(self.pathmarker2)
        self.pubMarker.publish(ma)

        path = np.append(path1, path2, 1)
#         path = np.append(path, path3, 1)

        self.ppMutex = True
        self.pp.updateTree(path.transpose())
        self.ppMutex = False

    def createPathSegment(self, segment, radius, prev_radius):
        self.commanded_radius = radius
        if segment > 3:
            segment -= 4
        if segment == 0:
            self.commanded_radius = np.Inf
            xstart = params.X1 - prev_radius * params.TURN_DIR
            xend = params.X2 - radius * params.TURN_DIR
            x = np.linspace(xstart, xend, params.DISC)

            ystart = params.Y1 - prev_radius
            yend = params.Y2 + radius
            y = np.linspace(ystart, yend, params.DISC)
            dist = np.sqrt((xstart - xend) ** 2 + (ystart - yend) ** 2)

        elif segment == 1:
            theta = np.linspace(np.pi / 2.0, np.pi, params.DISC)
            x1 = params.X2 - prev_radius * np.sin(theta) * params.TURN_DIR
            y1 = params.Y2 + prev_radius * np.cos(theta) + prev_radius

            theta = np.linspace(np.pi, np.pi / 2.0, params.DISC)
            x2 = params.X2 + radius * np.sin(theta) * params.TURN_DIR
            y2 = params.Y2 + radius * np.cos(theta) + radius

            x = np.append(x1, x2)
            y = np.append(y1, y2)
            dist = 2 * np.pi * radius / 4.0 + 2 * np.pi * prev_radius / 4.0

#             print "x1: " + str(x)
#             print "y1: " + str(y)

        elif segment == 2:
            self.commanded_radius = np.Inf
            xstart = params.X2 + prev_radius * params.TURN_DIR
            xend = params.X1 + radius * params.TURN_DIR
            x = np.linspace(xstart, xend, params.DISC)

            ystart = params.Y2 + prev_radius
            yend = params.Y1 - radius
            y = np.linspace(ystart, yend, params.DISC)
            dist = np.sqrt((xstart - xend) ** 2 + (ystart - yend) ** 2)

        elif segment == 3:
            theta = np.linspace(np.pi / 2.0, 0.0, params.DISC)
            x1 = params.X1 + prev_radius * np.sin(theta) * params.TURN_DIR
            y1 = params.Y1 + prev_radius * np.cos(theta) - prev_radius

            x2 = params.X1 - radius * np.sin(theta) * params.TURN_DIR
            y2 = params.Y1 + radius * np.cos(theta) - radius

            x = np.append(x1, x2[::-1])
            y = np.append(y1, y2[::-1])
            dist = 2 * np.pi * radius / 4.0 + 2 * np.pi * prev_radius / 4.0
#             print "x2: " + str(x)
#             print "y2: " + str(y)

        else:
            rospy.logerr("Wrong segment number!")
            return

        x, y = utils.rotate2D(x - params.XCENTER, y - params.YCENTER, params.OVERALL_ROT)

        return x + params.XCENTER, y + params.YCENTER, dist

#-----------------------------------------------------------------------------
# Simple function to return starting conditions for a new circuit
# Also returns first time segment, position and velocities at end of first
# segment
#-----------------------------------------------------------------------------
    def getStartingConditions(self):
        p0 = np.array([self.poly.x[0], self.poly.y[0], self.poly.z[0]])
        dx, dy, dz, t = self.poly.discretizePath(params.DISC, 1)
        v0 = np.array([dx[0], dy[0], dz[0]])
        p1 = np.array([self.poly.x[params.DISC], self.poly.y[params.DISC],
                       self.poly.z[params.DISC]])
        v1 = np.array([dx[params.DISC], dy[params.DISC], dz[params.DISC]])
        t = t[params.DISC]

        return p0, v0, p1, v1, t

#-----------------------------------------------------------------------------
# main timer for generating goal data
#-----------------------------------------------------------------------------
    def findGoalCallback(self, event):

        if self.shouldDrive and not self.ppMutex:
            try:
                # get current position and velocity in numpy formats
                p = np.array([self.pose.pose.position.x, self.pose.pose.position.y,
                              self.pose.pose.position.z])
                v = np.array([self.twist.twist.linear.x, self.twist.twist.linear.y,
                              self.twist.twist.linear.z])

                # calculate pure pursuit control vectors
                pL1, v_cmd, r_cmd, psi_cmd, index_L1, index_close = self.pp.calcL1Control(p, v, self.psi)

                # saturate velocity goal command
                if v_cmd != 0:
                    v_cmd = sat(v_cmd, params.MAXVEL, params.MINVEL)

                err = 0

    #         compute percent of path complete based on closest point projection
                self.percent_complete = 100.0 * index_L1 / len(self.pp.speed)
                pL1 = self.pp.path[index_L1, 0:-2]

                useClassic = 0
                if useClassic:
                    # IGNORING PURE_PURSUIT HERE!!!
                    # compute vector tangent to current path location
                    pClose = self.pp.path[index_close, 0:-2]  # closest point
                    try:
                        pNext = self.pp.path[index_close + 1, 0:-2]
                        psi_cmd = math.atan2(pNext[1] - pClose[1], pNext[0] - pClose[0])
                    except:
                        psi_cmd = 0

                    # compute cross track error
        #             err = math.sqrt((p[0] - pClose[0]) ** 2 + (p[1] - pClose[1]) ** 2)
                    err = xy2L(p[0], p[1], pClose[0], pClose[1], psi_cmd)

                    # compute r_cmd
                    r_cmd = v_cmd / self.commanded_radius

                    self.percent_complete = 100.0 * index_close / len(self.pp.speed)

                    pL1 = self.pp.path[index_close, 0:-2]

               # broadcast current percent complete
                percent_complete = Float64()
                percent_complete.data = self.percent_complete
                self.pubPathComplete.publish(percent_complete)

                # broadcast goal commands to control software
                self.goal.header.stamp = rospy.Time.now()
                self.goal.psi = psi_cmd
                self.goal.r = r_cmd
                self.goal.v = v_cmd
                self.goal.e = err
                self.goal.reset_v_int = False
                self.pubGoal.publish(self.goal)

                # generate marker data for visualization
                self.marker.pose.position.x = pL1[0]
                self.marker.pose.position.y = pL1[1]
                self.marker.pose.orientation.w = math.cos(psi_cmd / 2.0)
                self.marker.pose.orientation.z = math.sin(psi_cmd / 2.0)

#                 broadcast goal marker for visualization purposes
                ma = MarkerArray()
                ma.markers.append(self.marker)
                self.pubMarker.publish(ma)

            except:
                rospy.logerr("pure pursuit lookup error")

        if not self.shouldDrive:
            # broadcast goal commands to control software
            self.goal.header.stamp = rospy.Time.now()
            self.goal.r = 0
            self.goal.v = 0
            self.goal.e = 0
            self.goal.reset_v_int = False
            self.pubGoal.publish(self.goal)

    # listen to the car pose data
    def poseCB(self, data):
        self.pose = data
        q0 = self.pose.pose.orientation.w
        q1 = self.pose.pose.orientation.x
        q2 = self.pose.pose.orientation.y
        q3 = self.pose.pose.orientation.z
        self.psi = math.atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 ** 2 + q3 ** 2))

    # listen to the car vel data
    def velCB(self, data):
        self.twist = data

    # drive or stop driving
    def shouldDriveCB(self, msg):
        self.shouldDrive = msg.data

    # setup goal marker for visualization in rviz
    def initMarker(self, m):
        m.header.frame_id = "/world"
        m.header.stamp = rospy.Time.now()
        m.pose.position.x = 0.0
        m.pose.position.y = 0.0
        m.pose.position.z = 0.0

        m.ns = "RAVEN_car"
        ns = rospy.get_namespace()
        m.id = 0
        for letter in str(ns):
            m.id += ord(letter)  # cheap way to get a unique marker id

        m.pose.orientation.x = 0
        m.pose.orientation.y = 0
        m.pose.orientation.z = 0.0
        m.pose.orientation.w = 1
        m.scale.x = 0.5
        m.scale.y = 0.03
        m.scale.z = 0.03
        m.color.r = 1.0
        m.color.g = 0.0
        m.color.b = 0.0
        m.color.a = 1.0 #0.7
        m.lifetime = rospy.Duration(3)
        return m

# form the discretized position variables into a ros navigation path message
def createPath(x, y, z):
    points = []
    for i in range(len(x)):
        p = Point()
        p.x = x[i]
        p.y = y[i]
        p.z = z[i]
        points.append(p)
    return points

#     path = Path()
#     path.header.frame_id = "/world"
#     for i in range(len(x)):
#         p = PoseStamped()
#         p.header.stamp = rospy.Time.now()
#         p.header.seq = i
#         p.pose.position.x = x[i]
#         p.pose.position.y = y[i]
#         p.pose.position.z = z[i]
#         path.poses.append(p)
#
#     return path

#-----------------------------------------------------------------------------
# Get L from current point and desired current point
#-----------------------------------------------------------------------------
def xy2L(x, y, x_nom, y_nom, theta):

    L = math.sqrt((x - x_nom) ** 2 + (y - y_nom) ** 2)

    # angle between current point and desired point
    theta_act = math.atan2(y - y_nom, x - x_nom)

    if utils.wrap(theta_act - theta) < 0:
        L *= -1.0

    return L

#-----------------------------------------------------------------------------
# published the filled path over the network
#-----------------------------------------------------------------------------
def broadcastPath(pub, path):
    pub.publish(path)

#-----------------------------------------------------------------------------
# Return desired position, velocity, and acceleration values from a polynomial
# at a specific segment and time
#-----------------------------------------------------------------------------
def eval_poly(poly, t):

    segment = np.argwhere(poly.T <= t)
    segment = segment[-1]  # pull off the last value -- this is the current segment
    seg = segment[0]

    # Position commands
    x = poly.polyX[seg](t)
    y = poly.polyY[seg](t)
    # z = poly.polyZ[seg](t)

    # Velocity commands
    dx = np.polyder(poly.polyX[seg])(t)
    dy = np.polyder(poly.polyY[seg])(t)
    # dz = np.polyder(poly.polyZ[seg])(t)

    # Acceleration commands
    ddx = np.polyder(poly.polyX[seg], 2)(t)
    ddy = np.polyder(poly.polyY[seg], 2)(t)
    # ddz = np.polyder(poly.polyZ[seg], 2)(t)

    return x, y, dx, dy, ddx, ddy

def sat(val, hi, low):
    if val > hi:
        val = hi
    elif val < low:
        val = low
    return val

if __name__ == '__main__':
    try:
        ns = rospy.get_namespace()
        rospy.init_node('projectOnPath')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control projectOnPath.py")
        else:
            rospy.loginfo("Starting project on path for: " + str(ns))
            c = projectOnPath()
            d = Float64MultiArray()
#            d.data = [.8, 1.5, 1.0]
#            c.createOvalPathCB(d)
#             path = np.zeros((100, 4))
#             path[:, 1] = np.linspace(1, -5, 100)
#             path[:, 3] = 2.3 * np.ones(100)
#             c.createBarePath(path)
            rospy.Subscriber("pose", PoseStamped, c.poseCB)
            rospy.Subscriber("vel", TwistStamped, c.velCB)
            rospy.Subscriber("oval_path_params", Float64MultiArray,
                             c.createOvalPathCB)
            rospy.Subscriber("path_params", PolyInputs,
                             c.createPolyPathCB)
            rospy.Subscriber("segment_params", Float64MultiArray,
                             c.createPathSegmentCB)
            rospy.Subscriber("return_path", Float64MultiArray,
                     c.createPathCB)
            rospy.Subscriber("should_drive", Bool, c.shouldDriveCB)
            # set up 50 Hz timer
            rospy.Timer(rospy.Duration(1.0 / 50.0), c.findGoalCallback)
            rospy.spin()
    except rospy.ROSInterruptException:
        pass
