#!/usr/bin/env python
'''
Description: Implements pure pursuit controller based on this paper:
"Performance and Lyapunov Stability of a Nonlinear Path-Following Guidance 
Method" by Park, Deyst, and How
Currently only supports 2D path following

Created on Aug 1, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import numpy as np
import scipy.spatial
import scipy as sp
import math
from aclpy import utils

class PurePursuit:

#-----------------------------------------------------------------------------
# Path consists of distretized position commands with associated time values
# path - numpy n by 4 array consisting of x, y, z, speed commands
# This implementation assumes your path is sufficiently discretized with
# respect to the lookahead distance L1
#-----------------------------------------------------------------------------
    def __init__(self, path, L1_nom, L1_gain, leafsize=100):

        # L1 pure pursuit controller
        self.L1_nom = L1_nom  # 0.5
        self.L1 = self.L1_nom
        self.L1_gain = L1_gain
        self.vcmd_min = self.L1_nom / self.L1_gain
        # self.vcmd_min = 1.05  # 0.78

        # build a kdtree of the path data
        self.updateTree(path, leafsize)


#-----------------------------------------------------------------------------
# Update kdtree data -- marginally faster than just defining a new instance
# of the PurePursuit class
#  - Note: there is no good way to append data to a kdtree, so the only way
#            to add data to it is to rebuild the entire tree
#-----------------------------------------------------------------------------
    def updateTree(self, path, leafsize=100):
        # build a kdtree of the path data
        self.path = path
        self.kdtree = sp.spatial.KDTree(path[:, 0:3], leafsize)
        self.speed = path[:, -1]
        self.n = len(self.speed)

#-----------------------------------------------------------------------------
# main function for computing the L1 control parameters
# # Inputs:
# p = numpy array consisting of [x,y,z] current positions
# v = numpy array consisting of [dx,dy,dz] current velocities
# psi = current heading angle in radians
# # Outputs:
# v_cmd = commanded velocity (speed actually since it is a scalar)
# r_cmd = commanded body turn rate
# psi_cmd = current commanded heading angle
#-----------------------------------------------------------------------------
    def calcL1Control(self, p, v, psi):

        # find the L1 point
        index_close, index_L1 = self._findL1(p)

        # L1 control (in the vehicle body frame)
        try:
            pL1 = self.path[index_L1, 0:-2]
            eta = _findEta(p, v, pL1)
        except:
            pL1 = 0
            eta = 0
        # TODO: fix
        try:
            v_cmd = self.speed[index_close]
        except:
            v_cmd = 1.0

        vnorm = np.sqrt(v[0] ** 2 + v[1] ** 2 + v[2] ** 2)
        vnorm = utils.saturate(vnorm, 4.0, 0)
        a_cmd = 2.0 * vnorm * vnorm / self.L1 * math.sin(eta)
        psi_cmd = wrapPi(eta + psi)

        # Path angular rate command
        # a_cmd = v^2/R = R*omega^2
        # omega = sqrt(a_cmd/R), R = L1/(2*sin(eta))
        # omega = sqrt(a_cmd*2*sin(eta)/L1)
        r_cmd = math.sqrt(a_cmd * 2.0 * math.sin(eta) / self.L1)
        if eta < 0:
            r_cmd *= -1.0

        # update L1 based on magnitude of velocity command
        if v_cmd > self.vcmd_min:
            self.L1 = self.L1_gain * v_cmd
        else:
            self.L1 = self.L1_nom
#        if vnorm > self.vcmd_min:
#            self.L1 = self.L1_gain * vnorm
#        else:
#            self.L1 = self.L1_nom
        
        #print self.L1

        return pL1, v_cmd, r_cmd, psi_cmd, index_L1, index_close

    #--------------------------------------------------------------------------
    # Find the L1 look ahead point for the pure pursuit controller
    #--------------------------------------------------------------------------
    def _findL1(self, p):

        # takes a current position and finds the nearest point in the
        # pre-built kd tree path
        # returns the index of the closest point and the distance value
        d_closest, index_closest = self.kdtree.query(p)

        # return closest point as L1 point if we are more than L1 from the path
        if d_closest >= self.L1:
            return index_closest, index_closest

        # get the nearest neighbors to us, at most L1 distance away
        dist, index = self.kdtree.query(p, None, 0, 2, self.L1)
        dist = np.array(dist)
        index = np.array(index)

        # to account for circular paths, wrap the found neighbors around if you
        # are close to the beginning or end of your path
        # # TODO: account for circular paths in a more systematic method
#         if (index_closest > 3 * self.n / 4) or (index_closest < self.n / 4):
#             indextmp = wrapHightoZero(index + self.n / 2, self.n - 1)
#         else:
#             indextmp = index
        indextmp = index

        # Because the path data is assumed sequential, the largest index will
        # be the point on the path that is L1 away from you in the 'forward'
        # direction
        try:
            L1_index = index[np.argmax(indextmp)]
        except:
            L1_index = 0

        return index_closest, L1_index

#-----------------------------------------------------------------------------
# Find eta, the angle between L1 look ahead point and the current velocity
# vector
#-----------------------------------------------------------------------------
def _findEta(p, v, pL1):
    theta_V = math.atan2(v[1], v[0])
    L1 = np.array([pL1[0] - p[0],
                   pL1[1] - p[1]])
    theta_L1 = math.atan2(L1[1], L1[0])
    eta = theta_L1 - theta_V
    eta = wrapPi(eta)
    return eta

#-----------------------------------------------------------------------------
# Rotate by theta
#-----------------------------------------------------------------------------
def rotate(x, y, theta):
    xout = x * math.cos(theta) - y * math.sin(theta)
    yout = x * math.sin(theta) + y * math.cos(theta)
    return xout, yout

#-----------------------------------------------------------------------------
# wrap a number larger than high back around, starting at zero
#-----------------------------------------------------------------------------
def wrapHightoZero(val, high):
    for i in np.arange(len(val)):
        if val[i] >= high:
            val[i] -= high
    return val

#-----------------------------------------------------------------------------
# wrap a number between pi and -pi
#-----------------------------------------------------------------------------
def wrapPi(val):
    while val > np.pi:
        val -= 2.0 * np.pi
    while val < -np.pi:
        val += 2.0 * np.pi
    return val
