#!/usr/bin/env python
'''
Description: Broadcast visualization data to raven_rviz

Created on Jul 24, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''

import roslib; roslib.load_manifest('car_control')
import rospy
import numpy as np
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
import copy
import params
from aclpy import utils

#==============================================================================
# Add a visual bounding box around the car track -- this is the software
# box outside of which the car should stop operating
#==============================================================================
class RavenBroadcast:

    def __init__(self):
        self.ma = MarkerArray()
        self.initMarkerArray()
        self.pub = rospy.Publisher("RAVEN_world", MarkerArray, queue_size=1)

#-----------------------------------------------------------------------------
# Initialize an individual line-strip marker
# Note:
#     - raven_rviz will display any marker array broadcast on the "RAVEN_world"
#        message topic, provided it has a unique namespace and id number,
#        allowing you to easily break up the sources of visualizations
#-----------------------------------------------------------------------------
    def initMarker(self, m, id):
        m.header.frame_id = "world"
        m.header.stamp = rospy.Time.now()
        m.ns = "RAVEN_car"
        m.id = id
        m.scale.x = 0.1
        m.color.r = 0.0
        m.color.g = 1.0
        m.color.b = 1.0
        m.color.a = 0.5
        m.lifetime = rospy.Duration(3.0)

        # bounding box points low left, high left, high right, low right
        x = np.array([params.XOUTOFBOUNDS2, -params.XOUTOFBOUNDS2])
        y = np.array([params.YOUTOFBOUNDS2, -params.YOUTOFBOUNDS2])
        x, y = utils.rotate2D(x, y, params.OVERALL_ROT)

        xmax = x[0] + params.XCENTER
        xmin = x[1] + params.XCENTER
        ymax = y[0] + params.YCENTER
        ymin = y[1] + params.YCENTER


        p1 = Point()
        p1.x = xmin; p1.y = ymax; p1.z = 0.0
        m.points.append(p1)
        p2 = Point()
        p2.x = xmax; p2.y = ymax; p2.z = 0.0
        m.points.append(p2)
        p3 = Point()
        p3.x = xmax; p3.y = ymin; p3.z = 0.0
        m.points.append(p3)
        p4 = Point()
        p4.x = xmin; p4.y = ymin; p4.z = 0.0
        m.points.append(p4)
        p5 = Point()
        p5.x = xmin; p5.y = ymax; p5.z = 0.0
        m.points.append(p5)

        return m

#-----------------------------------------------------------------------------
# Set up the marker array.  In this case we only need a single element since
# we are using a linestrip marker.  Note that we could have just broadcast a
# marker message instead of a marker array; however, the marker array is
# compatible with the current "RAVEN_world" message being broadcast and so
# doesn't require changing the rviz config file.
#-----------------------------------------------------------------------------
    def initMarkerArray(self):
        # initialize marker data
        m1 = Marker(type=Marker.LINE_STRIP, action=Marker.ADD)
        m1 = self.initMarker(m1, 0)
        self.ma.markers.append(m1)

        m2 = Marker(type=Marker.MESH_RESOURCE, action=Marker.ADD)
        m2.mesh_resource = "package://raven_rviz/vehicle_models/cone.dae"
        m2.mesh_use_embedded_materials = True
        scale = 0.04
        m2.scale.x = scale
        m2.scale.y = scale
        m2.scale.z = scale
        m2.header.frame_id = "world"
        m2.header.stamp = rospy.Time.now()
        m2.ns = "RAVEN_car"
        m2.id = 1
        m2.lifetime = rospy.Duration(3.0)
        m2.color.r = 1
        m2.color.g = 157 / 255.0
        m2.color.b = 0
        m2.color.a = 1
        x = params.X1 - params.XCENTER
        y = params.Y1 - 0.5 - params.YCENTER
        x, y = utils.rotate2D(x, y, params.OVERALL_ROT)

        m2.pose.position.x = x + params.XCENTER
        m2.pose.position.y = y + params.YCENTER

        self.ma.markers.append(m2)

        m3 = copy.deepcopy(m2)

        x = params.X2 - params.XCENTER
        y = params.Y2 + 0.5 - params.YCENTER
        x, y = utils.rotate2D(x, y, params.OVERALL_ROT)

        m3.pose.position.x = x + params.XCENTER
        m3.pose.position.y = y + params.YCENTER

        m3.id = 2
        self.ma.markers.append(m3)


    def broadcastTimer(self, event):
        for i in np.arange(len(self.ma.markers)):
            self.ma.markers[i].header.stamp = rospy.Time.now()

        self.pub.publish(self.ma)




if __name__ == '__main__':
    try:
        rospy.init_node('raven_car_vis')
        c = RavenBroadcast()
        rospy.Timer(rospy.Duration(1 / 60.0), c.broadcastTimer)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
