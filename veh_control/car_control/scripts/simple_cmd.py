#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray

NOT_DRIVING = 0
WHEEL_SPEED = 3

class SimpleCmd:

    def __init__(self):

        self.wheel_speed = 0
        self.turn = 0
        self.status = WHEEL_SPEED  # NOT_DRIVING

        self.pub = rospy.Publisher('/RC02s/carCmd', Float64MultiArray)

    def cmdPublish(self, event):

        self.wheel_speed = 50
        self.turn = 0.5

        cmd = Float64MultiArray()
        cmd.data = [self.status, self.wheel_speed, self.turn]
        self.pub.publish(cmd)

if __name__ == '__main__':

    rospy.init_node('simple_cmd')
    rospy.logwarn('Hardcoded for vehicle RC02s')
    c = SimpleCmd()
    rospy.Timer(rospy.Duration(1 / 50.0), c.cmdPublish)
    rospy.spin()


