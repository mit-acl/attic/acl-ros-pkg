#!/usr/bin/env python
'''
Description: Service handler for running a single circular track simulation

Created on Jul 23, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('car_control')
import rospy
import numpy as np
import math
import sys

# ros imports
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist

# custom imports
import projectOnPath
import turn_controller
import polyPath
import params
from car_control.srv import SingleTrack
from car_control.srv import ReturnToBase
from car_sim.msg import CarState
from car_sim.srv import RunStep
from car_control.msg import PolyInputs


#==============================================================================
# Class for implementing a service call to run a single track simulation
# circuit
#==============================================================================
class SingleTrackSrv:

    def __init__(self, serviceMode):

        self.serviceMode = serviceMode

        self.p = projectOnPath.projectOnPath()

        if self.serviceMode:
            self.tc = turn_controller.TurnController('RC01')
        else:
            self.vel = Twist()
            rospy.Subscriber("RC01/pose", PoseStamped, self.poseCB)
            rospy.Subscriber("RC01/vel", TwistStamped, self.velCB)
            rospy.Subscriber("RC01/path_percent", Float64, self.path_percentCB)
            self.pathOvalParamPub = rospy.Publisher('RC01/oval_path_params',
                                                Float64MultiArray)
            self.pathParamPub = rospy.Publisher('RC01/return_path', Float64MultiArray)
            self.percent_complete = 0


#-----------------------------------------------------------------------------
# Run a single integration step of the car simulator
#-----------------------------------------------------------------------------
    def run_step(self, state, dt, omegaDes, turn, vis):
        rospy.wait_for_service('/RC01/run_step')
        try:
            run_step = rospy.ServiceProxy('/RC01/run_step', RunStep)
            resp1 = run_step(state, dt, omegaDes, turn, vis)
            return resp1.finalState
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e

#-----------------------------------------------------------------------------
# run a single track of the sim and return the time
#-----------------------------------------------------------------------------
    def single_track(self, req):

        # initialize polynomial path projection
        path_params = Float64MultiArray()
        path_params.data = [req.xcross, req.t13, req.t24]

        self.p.createOvalPathCB(path_params)

        if not self.serviceMode:
            # first step, get to initial conditions if running in real-time mode

            pstart, vstart, pstart1, vstart1, tstart1 = self.p.getStartingConditions()
            p0 = Pose()
            pf = Pose()
            pf1 = Pose()

            p0 = self.state.pose
            pf.position.x = pstart[0]
            pf.position.y = pstart[1]
            pf.position.z = pstart[2]

            pf1.position.x = pstart1[0]
            pf1.position.y = pstart1[1]
            pf1.position.z = pstart1[2]


            # hard coded intermediate points

            p1 = Pose()
            p1.position.x = 1.0 + params.XCENTER
            p1.position.y = -1.0 + params.YCENTER + params.YDIST / 2
            p1.position.z = 0.0

            p2 = Pose()
            p2.position.x = 1.0 + params.XCENTER
            p2.position.y = 2.5 + params.YCENTER + params.YDIST / 2
            p2.position.z = 0.0

            p3 = Pose()
            p3.position.x = pf.position.x
            p3.position.y = 2.5 + params.YCENTER + params.YDIST / 2
            p3.position.z = 0.0

            p4 = Pose()
            p4.position.x = pf.position.x
            p4.position.y = 1.5 + params.YCENTER + params.YDIST / 2
            p4.position.z = 0.0

            p = [p0.position, p1.position, p2.position, p3.position, p4.position, pf.position, pf1.position]
            v = [1, 1., 1., 1., math.fabs(vstart[1]), math.fabs(vstart[1])]

            self.return_to_base(p, v)


            self.pathOvalParamPub.publish(path_params)

#             self.state = PoseStamped()
            startTime = rospy.get_time()

        else:
            self.state = self.initState(req.xcross)
            self.updateCBs()
            iterCnt = 0
            startTime = 0


        simTime = startTime
        currentLap = 0
        old_y = -np.Inf

        while((simTime - startTime) < params.MAXTIME):
#             print "simTime: " + str(simTime - startTime)

            if self.serviceMode:
                # run controller at CNTRL_RATE
                if np.mod(iterCnt, params.CNTRL_RATE / params.DT) == 0:
                    # get current goal and run controller
                    self.p.findGoalCallback(0)
                    self.tc.goalCB(self.p.goal)  # "broadcast" goal to tc

            # check terminating conditions
            # check for passing a lap (hack that assumes lap ends at
            # origin)
            ymax = params.YCENTER + params.YDIST / 2
#             print old_y
            if old_y > ymax and self.state.pose.position.y <= ymax:
                # just passed finish line
                currentLap += 1
                print currentLap
                if currentLap == req.num_laps:
                    return (simTime - startTime) / req.num_laps
            old_y = self.state.pose.position.y

            if self.serviceMode:
                # run step
                omegaDes = self.tc.cmd.data[1]
                turn = self.tc.cmd.data[2]
                self.state = self.run_step(self.state, params.DT, omegaDes, turn, True)

                simTime += params.DT
            else:
                simTime = rospy.get_time()

            if self.serviceMode:
                # update turn_controller and path projection callbacks
                if np.mod(iterCnt, params.CNTRL_RATE / params.DT) == 0:
                    self.updateCBs()

                iterCnt += 1
            else:
                #             check if out of bounds
                if turn_controller.checkIfOutofBounds(self.state.pose):
                    return -1

        return -2  # time expired



#-----------------------------------------------------------------------------
# Call the return to base service
#-----------------------------------------------------------------------------
    def return_to_base_service(self, req):
        # for polynomial from starting state to ending state

        # get starting and ending positions and velocities
        p0 = req.pose_init.position
        t0 = req.twist_init.linear
        pf = req.pose_goal.position
        tf = req.twist_goal.linear

        return self.return_to_base(p0, t0, pf, tf)

#-----------------------------------------------------------------------------
# Return2Base -- get from any initial orientation/velocity to a goal.  Useful
# for getting setup for the next path service request
# p - list of geometry_msgs/point data types containing goal waypoints
# v - list of geometry_msgs/vector3 data types containing velocity constraints
#        - NaN implies there is no constraint
# t - numpy array containing time segments
#-----------------------------------------------------------------------------
#     def return_to_base(self, p, v, t):
#
#         n = 4
#         wp = np.zeros((3, len(p)))
#         wp_vel = np.zeros((3, len(p)))
#         for i in range(len(p)):
#             wp[:, i] = np.array([p[i].x, p[i].y, p[i].z])
#             wp_vel[:, i] = np.array([v[i].x, v[i].y, v[i].z])
#
#
#         # TODO: convert the polygen to accept general wp_vel inputs -- here is a quick hack
#         # to avoid this for now
#         wp_elm = np.array([])
#         wp_veltmp_start = False
#         for i in range(1, len(p) - 1):
#             if not np.isnan(wp_vel[0, i]):
#                 wp_elm = np.append(wp_elm, i)
#                 if wp_veltmp_start:
#                     wp_veltmp = np.append(wp_veltmp, wp_vel[:, i:i + 1], 1)
#                 else:
#                     wp_veltmp = wp_vel[:, i:i + 1].copy()
#                     wp_veltmp_start = True
#
#         wp0 = wp_vel[:, 0:1]
#         wpf = wp_vel[:, -1:]
#
#
#         # total hack to send just half at once
#         n = -1
#
#         polyInputs = polyPath.createPolyInputs(n, wp, wp0, wpf, wp_veltmp,
#                                                wp_elm, t)
#
#         self.pathParamPub.publish(polyInputs)
#
#         while self.percent_complete < 75:
#             pass
#
#         self.percent_complete = 0
#         polyInputs.n = 4
#         self.pathParamPub.publish(polyInputs)
#         while self.state.pose.position.y > 0:
#             pass
#
#         return True


# p - array of waypoints
# v - constant speed inbetween waypoints len(v) = len(p) - 1
    def return_to_base(self, p, v):

        disc = 100
        n = len(p)
        path = np.zeros((4, disc * (n - 1) + 1))

        for i in range(n - 1):
            path[0, i * disc:(i + 1) * disc] = np.linspace(p[i].x, p[i + 1].x, disc,
                                                     False)
            path[1, i * disc:(i + 1) * disc] = np.linspace(p[i].y, p[i + 1].y, disc,
                                         False)
            path[2, i * disc:(i + 1) * disc] = np.linspace(p[i].z, p[i + 1].z, disc,
                                                     False)
            path[3, i * disc:(i + 1) * disc] = v[i] * np.ones(disc)

        path[0, -1] = p[-1].x
        path[1, -1] = p[-1].y
        path[2, -1] = p[-1].z
        path[3, -1] = v[-1]

#         while 1:
#             pass

        maPath = Float64MultiArray()
        polyPath.numpyArray2DToMultiarray(path[:, 0:n / 2 * disc], maPath)
        self.pathParamPub.publish(maPath)

        while self.percent_complete < 75:
            pass

        self.percent_complete = 0
        maPath = Float64MultiArray()
        polyPath.numpyArray2DToMultiarray(path[:, 2 * disc:], maPath)
        self.pathParamPub.publish(maPath)
        while self.state.pose.position.y > params.YCENTER + params.YDIST / 2:
            pass

        return True

#-----------------------------------------------------------------------------
# Path percent complete callback
#-----------------------------------------------------------------------------
    def path_percentCB(self, data):
        self.percent_complete = data.data

#-----------------------------------------------------------------------------
# Pose callback -- used only when running in real time mode
#-----------------------------------------------------------------------------
    def poseCB(self, data):
        self.state = data

#-----------------------------------------------------------------------------
# Vel callback -- used only when running in real time mode
#-----------------------------------------------------------------------------
    def velCB(self, data):
        self.vel = data.twist

#-----------------------------------------------------------------------------
# Update turn controller and path projection callbacks, giving them access
# to the current state data
#-----------------------------------------------------------------------------
    def updateCBs(self):
        # get psi
        q0 = self.state.pose.orientation.w
        q1 = self.state.pose.orientation.x
        q2 = self.state.pose.orientation.y
        q3 = self.state.pose.orientation.z
        psi = math.atan2(2 * (q0 * q3 + q1 * q2),
                              1 - 2 * (q2 ** 2 + q3 ** 2))

        dx = self.state.Vx * np.cos(psi) - self.state.Vy * np.sin(psi)
        dy = self.state.Vx * np.sin(psi) + self.state.Vy * np.cos(psi)
        p = PoseStamped()
        p.pose = self.state.pose
        t = TwistStamped()
        t.twist.linear.x = dx
        t.twist.linear.y = dy
        t.twist.angular.z = self.state.r
        self.p.poseCallBack(p)
        self.p.velCallBack(t)
        self.tc.poseCB(p)
        self.tc.velCB(t)

#-----------------------------------------------------------------------------
# Initialize state to psi = -pi/2 and position = (-xcross,0)
#-----------------------------------------------------------------------------
    def initState(self, xcross):
        s = CarState()
        s.pose.position.x = -xcross + params.XCENTER
        s.pose.position.y = params.YCENTER + params.YDIST / 2
        s.pose.position.z = 0
        psi = -np.pi / 2.0
        s.pose.orientation.w = np.cos(psi / 2.0)
        s.pose.orientation.x = 0
        s.pose.orientation.y = 0
        s.pose.orientation.z = np.sin(psi / 2.0)
        s.Vx = s.Vy = s.r = s.omegaF = s.omegaR = s.throttle = 0

        return s


if __name__ == '__main__':
    try:
        rospy.init_node('single_track_server')
        argv = rospy.myargv(argv=sys.argv)
        if len(argv) == 2:
            if argv[1] == 'True' or argv[1] == 'true' or argv[1] == '1':
                rospy.loginfo("Starting Single Track Server in Service mode")
                c = SingleTrackSrv(True)
            else:
                rospy.loginfo("Starting Single Track Server in Real Time mode")
                c = SingleTrackSrv(False)
            s1 = rospy.Service('/RC01/single_track', SingleTrack, c.single_track)
            s2 = rospy.Service('/RC01/return_to_base', ReturnToBase,
                               c.return_to_base_service)
            rospy.spin()
        else:
            rospy.logerr("Error: need to specify True for service mode or False for real-time mode")
    except rospy.ROSInterruptException:
        pass
