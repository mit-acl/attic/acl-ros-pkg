#!/usr/bin/env python
'''
Description: Service handler for running a single circular track simulation

Created on Jul 23, 2013

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('car_control')
import rospy
import actionlib
import numpy as np
import math
import sys
import copy

# ros imports
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import Float64
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist

# custom imports
import projectOnPath
import turn_controller
import polyPath
import params
from car_control.srv import SingleSegment
from car_control.srv import ResetCar
from car_control.srv import ReturnToBase
from car_control.msg import SimpleState
from car_control.msg import CarGoal
from car_sim.msg import CarState
from car_sim.srv import RunStep
from car_control.msg import PolyInputs
from car_control.msg import *

from aclpy import utils


#==============================================================================
# Class for implementing a service call to run a track segment
#==============================================================================
class StateTrackSrv:

    def __init__(self):

        self.serviceMode = True

        self.p = projectOnPath.projectOnPath()

        self.current_state = CarState()

        self.tc = turn_controller.TurnController()
        self.vel = Twist()
        if not self.serviceMode:
            self.poseSub = rospy.Subscriber("pose", PoseStamped, self.poseCB)
            self.velSub = rospy.Subscriber("vel", TwistStamped, self.velCB)
            self.pathSub = rospy.Subscriber("path_percent", Float64, self.path_percentCB)
        self.pathSegmentPub = rospy.Publisher("segment_params",
                                              Float64MultiArray)
        self.pathParamPub = rospy.Publisher('return_path', Float64MultiArray)
        self.pubGoal = rospy.Publisher('goal', CarGoal)
        self.p.percent_complete = 0

        # return to start action
        self.r2s_server = actionlib.SimpleActionServer('r2s', Return2StartAction,
                                                       self.return2start, False)
        self.r2s_server.start()


#-----------------------------------------------------------------------------
# Run a single integration step of the car simulator
#-----------------------------------------------------------------------------
    def run_step(self, state, dt, omegaDes, turn, vis):
#         rospy.wait_for_service('/RC02s/run_step')
        try:
            run_step = rospy.ServiceProxy('/RC02s/run_step', RunStep)
            resp1 = run_step(state, dt, omegaDes, turn, vis)
            return resp1.finalState
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e

#-----------------------------------------------------------------------------
# run a single segment of the sim and return the time
#-----------------------------------------------------------------------------
    def single_segment(self, req):


#             # if this is a reset, send a message to the turn controller to reset the
#             # velocity integrator
#             goal = CarGoal()
#             goal.header.stamp = rospy.Time.now()
#             goal.r = 0
#             goal.v = 0
#             goal.e = 0
#             goal.reset_v_int = True
#             if self.serviceMode:
#                 self.tc.goalCB(goal)
#             else:
#                 self.pubGoal.publish(goal)

        if req.d == 0:  # math model -- return idealized time
            x, y, dist = self.p.createPathSegment(req.s.segment, req.r,
                                                req.s.prev_radius)
            t = dist / req.v
            ns = SimpleState()
            ns.prev_radius = req.r

            ns.slipping = 0
            ns.Vx = req.v
            ns.Vy = -1000  # Vy and psidot don't make much sense in math model,
                        # we'll just copy n and Q over to the unseen values.
                        # set so small values here so we can know that are index
                        # zero in the discretized version
            ns.psidot = -1000
#             print
#             print req.s.segment
            ns.segment = req.s.segment + 1
            if ns.segment > 3:
                ns.segment = 0

#             print ns.segment

            return ns, t
        elif req.d == 1:
            if self.serviceMode == False:
                self.poseSub.unregister()
                self.velSub.unregister()
                self.pathSub.unregister()
            self.serviceMode = True
        elif req.d == 2:
            if self.serviceMode == True:
                self.poseSub = rospy.Subscriber("pose", PoseStamped, self.poseCB)
                self.velSub = rospy.Subscriber("vel", TwistStamped, self.velCB)
                self.pathSub = rospy.Subscriber("path_percent", Float64, self.path_percentCB)
                # self.run_step(self.current_state, params.DT, 0, 0, False)
            self.serviceMode = False

        d = Float64MultiArray()
        d.data = [req.s.segment, req.r, req.s.prev_radius, req.v]

        if not self.serviceMode:

            self.pathSegmentPub.publish(d)
            startTime = rospy.get_time()

        else:
            if req.reset_state:
                self.current_state = self.initState(req.r, req.s)
            if req.use_start_state:
                self.current_state = self.useStartState(req.s)
            self.updateCBs()
            iterCnt = 0
            startTime = 0

            self.p.createPathSegmentCB(d)

        simTime = copy.copy(startTime)
        currentLap = 0
        old_y = -np.Inf

        # get slowing down parameter if running in service mode
        if rospy.has_param('slow_mo'):
            slow_mo = rospy.get_param('slow_mo')
        else:
            slow_mo = 0

#         print self.p.percent_complete
        while((simTime - startTime) < params.MAXSEGTIME):
#             print "simTime: " + str(simTime - startTime)
#            print self.current_state


            # check if you have finished this segment or not.
            # determined by if you have crossed y boundary and are on the right
            # side of the track
            passedYthresh = False
            y = self.current_state.pose.position.y
            x = self.current_state.pose.position.x
            if req.s.segment == 0:
                if y < (params.Y2 + req.r) and x < params.XCENTER:
                    passedYthresh = True
            if req.s.segment == 1:
                if y > (params.Y2 + req.r) and x > params.XCENTER:
                    passedYthresh = True
            if req.s.segment == 2:
                if y > (params.Y1 - req.r) and x > params.XCENTER:
                    passedYthresh = True
            if req.s.segment == 3:
                if y < (params.Y1 - req.r) and x < params.XCENTER:
                    passedYthresh = True


            if passedYthresh:
                s_prime = getSimpleState(self.current_state, req.s, req.r, req.v)
                return s_prime, simTime - startTime


            if self.serviceMode:
                # run controller at CNTRL_RATE
                if np.mod(iterCnt, params.CNTRL_RATE / params.DT) == 0:
                    # get most recent state
                    self.updateCBs()
                    # get current goal and run controller
                    self.p.findGoalCallback(0)
                    # self.tc.verr_int = 0  # reset integrator in sim
                    # print "goal: " + str(self.p.goal)
                    self.tc.goalCB(self.p.goal)  # "broadcast" goal to tc
                    print "turn_des: " + str(self.tc.cmd.data[2])
                    print "omega_des: " + str(self.tc.cmd.data[1])


            if self.serviceMode:
                # run step
                omegaDes = self.tc.cmd.data[1]
                turn = self.tc.cmd.data[2]
                self.current_state = self.run_step(self.current_state, params.DT, omegaDes, turn, True)

                simTime += params.DT
                rospy.sleep(slow_mo)
            else:
                simTime = rospy.get_time()

            if self.serviceMode:
                # update turn_controller and path projection callbacks
                if np.mod(iterCnt, params.CNTRL_RATE / params.DT) == 0:
                    self.updateCBs()

                iterCnt += 1
#             else:
#                             check if out of bounds
            if req.d == 2:
                hardware = True
            else:
                hardware = False
            if turn_controller.checkIfOutofBounds(self.current_state.pose, hardware):
                s_prime = getOutofBoundsState()
                return s_prime, 200000

            print "x: " + str(self.current_state.pose.position.x) + " y: " + str(
                                                                                 self.current_state.pose.position.y)

        s_prime = getOutofBoundsState()
        return s_prime, 200000  # time expired

#-----------------------------------------------------------------------------
# return to the starting position for the next lap
#-----------------------------------------------------------------------------
    def return2start(self, goal):


        if self.serviceMode == True:
            self.poseSub = rospy.Subscriber("pose", PoseStamped, self.poseCB)
            self.velSub = rospy.Subscriber("vel", TwistStamped, self.velCB)
            self.pathSub = rospy.Subscriber("path_percent", Float64, self.path_percentCB)
            # self.run_step(self.current_state, params.DT, 0, 0, False)
        self.serviceMode = False

        # get to initial conditions if running in real-time mode
        cs = SimpleState()
        cs.segment = 2 # starting top right corner
        cs.prev_radius = goal.r
        goal_state = self.initState(goal.r, cs)
        gx = goal_state.pose.position.x
        gy = goal_state.pose.position.y

#        waypoint_x = np.array([self.current_state.pose.position.x,
#                               params.X1 + 1.0, params.X1 + 1.0, gx, gx])
#        waypoint_y = np.array([self.current_state.pose.position.y,
#                               params.Y1 - 1.0, params.Y1 + 2.5,
#                               params.Y1 + 2.5, gy - 3.0])
        waypoint_x = np.array([self.current_state.pose.position.x,
                               params.X2 - 1.0, params.X2 - 1.0, gx, gx])
        waypoint_y = np.array([self.current_state.pose.position.y,
                               params.Y2 + 1.0, params.Y2 - 2.5,
                               params.Y2 - 2.5, gy + 3.0])
        waypoint_v = 2.1 * np.ones(waypoint_x.shape)
        waypoint_v[-1] = goal.v
        waypoint_v[-2] = goal.v

        disc = 100
        n = len(waypoint_x)
        path = np.zeros((4, disc * (n - 1) + 1))

        for i in range(n - 1):
            path[0, i * disc:(i + 1) * disc] = np.linspace(waypoint_x[i], waypoint_x[i + 1], disc,
                                                     False)
            path[1, i * disc:(i + 1) * disc] = np.linspace(waypoint_y[i], waypoint_y[i + 1], disc,
                                         False)
            path[3, i * disc:(i + 1) * disc] = waypoint_v[i] * np.ones(disc)

        path[2, ] = np.zeros(path[1, ].shape)

        path[0, -1] = gx
        path[1, -1] = gy + 3.0 # gy - 3.0
        path[2, -1] = 0
        path[3, -1] = goal.v

        maPath = Float64MultiArray()
        polyPath.numpyArray2DToMultiarray(path[:, 0:n / 2 * disc], maPath)
        self.pathParamPub.publish(maPath)

        self.p.percent_complete = 0
        # while self.p.percent_complete < 75:
#            pass
        # while self.current_state.pose.position.y < 0 or self.p.percent_complete < 75:
        while self.current_state.pose.position.y > -4.5 or self.p.percent_complete < 75:
#            print self.current_state.pose.position.y
            pass

        self.p.percent_complete = 0
        maPath = Float64MultiArray()
        polyPath.numpyArray2DToMultiarray(path[:, (n / 2 - 1) * disc:], maPath)
        self.pathParamPub.publish(maPath)
        #while self.current_state.pose.position.y > gy:
        while self.current_state.pose.position.y < gy:
            pass

        self.r2s_server.set_succeeded()

#-----------------------------------------------------------------------------
# Call the return to base service
#-----------------------------------------------------------------------------
    def return_to_base_service(self, req):
        # for polynomial from starting state to ending state

        # get starting and ending positions and velocities
        p0 = req.pose_init.position
        t0 = req.twist_init.linear
        pf = req.pose_goal.position
        tf = req.twist_goal.linear

        return self.return_to_base(p0, t0, pf, tf)

#-----------------------------------------------------------------------------
# Path percent complete callback
#-----------------------------------------------------------------------------
    def path_percentCB(self, data):
        self.p.percent_complete = data.data

#-----------------------------------------------------------------------------
# Pose callback -- used only when running in real time mode
#-----------------------------------------------------------------------------
    def poseCB(self, data):
        self.current_state.pose = data.pose
#         self.current_state = data

#-----------------------------------------------------------------------------
# Vel callback -- used only when running in real time mode
#-----------------------------------------------------------------------------
    def velCB(self, data):
#         self.vel = data.twist
        q0 = self.current_state.pose.orientation.w
        q1 = self.current_state.pose.orientation.x
        q2 = self.current_state.pose.orientation.y
        q3 = self.current_state.pose.orientation.z
        psi = math.atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 ** 2 + q3 ** 2))
        dx = data.twist.linear.x
        dy = data.twist.linear.y
        beta = math.atan2(dy, dx) - psi
        V = math.sqrt(dx ** 2 + dy ** 2)
        self.current_state.Vx = V * math.cos(beta)
        self.current_state.Vy = V * math.sin(beta)
        self.current_state.r = data.twist.angular.z
#        print "Vx: " + str(self.current_state.Vx)
#        print "Vy: " + str(self.current_state.Vy)

#-----------------------------------------------------------------------------
# Update turn controller and path projection callbacks, giving them access
# to the current state data
#-----------------------------------------------------------------------------
    def updateCBs(self):
        # get psi
        q0 = self.current_state.pose.orientation.w
        q1 = self.current_state.pose.orientation.x
        q2 = self.current_state.pose.orientation.y
        q3 = self.current_state.pose.orientation.z
        psi = math.atan2(2 * (q0 * q3 + q1 * q2),
                              1 - 2 * (q2 ** 2 + q3 ** 2))

        dx = self.current_state.Vx * np.cos(psi) - self.current_state.Vy * np.sin(psi)
        dy = self.current_state.Vx * np.sin(psi) + self.current_state.Vy * np.cos(psi)
        p = PoseStamped()
        p.pose = self.current_state.pose
        t = TwistStamped()
        t.twist.linear.x = dx
        t.twist.linear.y = dy
        t.twist.angular.z = self.current_state.r
        self.p.poseCB(p)
        self.p.velCB(t)
        self.tc.poseCB(p)
        self.tc.velCB(t)
        self.tc.stateCB(self.current_state)

#-----------------------------------------------------------------------------
# Use given starting state
#-----------------------------------------------------------------------------
    def useStartState(self, state):
        s = CarState()
        s.pose.position.x = state.x
        s.pose.position.y = state.y
        s.pose.position.z = 0
        s.pose.orientation.w = np.cos(state.psi / 2.0)
        s.pose.orientation.x = 0.0
        s.pose.orientation.y = 0.0
        s.pose.orientation.z = np.sin(state.psi / 2.0)
        s.omegaF = s.omegaR = state.omega
        s.r = state.psidot
        s.Vx = state.Vx
        s.Vy = state.Vy

        return s

#-----------------------------------------------------------------------------
# Initialize state
#-----------------------------------------------------------------------------
    def initState(self, radius, state):
        s = CarState()
        if state.segment == 0:
            s.pose.position.x = params.X1 - state.prev_radius * params.TURN_DIR
            s.pose.position.y = params.Y1 - state.prev_radius
            psi = -np.pi / 2.0
        elif state.segment == 1:
            s.pose.position.x = params.X2 - state.prev_radius
            s.pose.position.y = params.Y2 + state.prev_radius
            psi = -np.pi / 2.0
        elif state.segment == 2:
            s.pose.position.x = params.X2 + state.prev_radius * params.TURN_DIR
            s.pose.position.y = params.Y2 + state.prev_radius
            psi = np.pi / 2.0
        elif state.segment == 3:
            s.pose.position.x = params.X1 + state.prev_radius
            s.pose.position.y = params.Y1 - state.prev_radius
            psi = np.pi / 2.0
        s.pose.position.z = 0
        s.pose.position.x, s.pose.position.y = utils.rotate2D(s.pose.position.x - params.XCENTER,
                                                              s.pose.position.y - params.YCENTER,
                                                              params.OVERALL_ROT)
        s.pose.position.x += params.XCENTER
        s.pose.position.y += params.YCENTER
        psi += params.OVERALL_ROT
        s.pose.orientation.w = np.cos(psi / 2.0)
        s.pose.orientation.x = 0
        s.pose.orientation.y = 0
        s.pose.orientation.z = np.sin(psi / 2.0)
        s.Vx = state.Vx
        s.Vy = state.Vy
        s.r = state.psidot
        v = np.sqrt(s.Vx ** 2 + s.Vy ** 2)
        s.omegaF = s.omegaR = v / params.RADIUS  # clearly an approximation

        return s

#-----------------------------------------------------------------------------
# Convert carState to simple state for rmax algorithm
#-----------------------------------------------------------------------------
def getOutofBoundsState():
    ns = SimpleState()
    ns.Vx = 0  # car_state.Vx
    ns.Vy = 0
    ns.psidot = 0
    ns.prev_radius = 0
    ns.slipping = 0
    ns.segment = 0

    return ns

#-----------------------------------------------------------------------------
# Convert carState to simple state for rmax algorithm
#-----------------------------------------------------------------------------
def getSimpleState(car_state, simple_state, radius, vel):
    ns = SimpleState()
    ns.x = car_state.pose.position.x
    ns.y = car_state.pose.position.y
    ns.psi = utils.quat2yaw(car_state.pose.orientation)
    ns.omega = car_state.omegaR
    ns.Vx = car_state.Vx
    ns.Vy = car_state.Vy
    ns.psidot = car_state.r
    ns.prev_radius = np.abs(car_state.pose.position.x - params.XCENTER)  # radius

    if np.abs(ns.Vx) > np.abs(car_state.omegaR * params.RADIUS) * 1.1:  # 10 percent slip min
        ns.slipping = 1
    else:
        ns.slipping = 0

    ns.segment = simple_state.segment + 1
    if ns.segment > 3:
        ns.segment = 0

    return ns


if __name__ == '__main__':
    ns = rospy.get_namespace()
    try:
        rospy.init_node('single_segment_server')

#         c = StateTrackSrv()
#         srvcall = SingleSegment()
#         srvcall._request_class.d = 1
#         srvcall._request_class.r = 0.8
#         srvcall._request_class.v = 4.0
#         srvcall._request_class.reset_state = True
#         srvcall._request_class.use_start_state = False
#         srvcall._request_class.s = SimpleState()
#         srvcall._request_class.s.prev_radius = 0.8
#         srvcall._request_class.s.segment = 0;
#         srvcall._request_class.s.Vx = 3.5;
#
#         srvcall._response_class = c.single_segment(srvcall._request_class)


        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control state_track_server.py")
        else:
            argv = rospy.myargv(argv=sys.argv)
            c = StateTrackSrv()
            s1 = rospy.Service('single_segment', SingleSegment, c.single_segment)
#             s2 = rospy.Service('return2start', ResetCar, c.return2start)

            rospy.spin()

    except rospy.ROSInterruptException:
        pass
