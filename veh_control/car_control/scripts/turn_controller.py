#!/usr/bin/env python
import roslib; roslib.load_manifest('car_control')
import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from car_control.msg import CarGoal
from car_sim.msg import CarState
from sensor_msgs.msg import Joy
import sys, select, termios, tty
import csv
import numpy as np
import math
import random
import signal
import params
from aclpy import utils

"""
Compute steering angle commands based on a learned policy
"""

NOT_DRIVING = 0
THROTTLE = 1
SIMPLE_CONTROLLER = 2
WHEEL_SPEED = 3
RESET = 4


NOISE = 10
KDIM = 2

A = 0
B = 1
X = 2
Y = 3
CENTER = 8
RB = 5

MANUAL_CONTROL = 1
TURN_CONTROL = 0

class TurnController():

    cmd = Float64MultiArray()

    def __init__(self):
        self.throttle = 0
        self.turn = 0
        self.psi = 0
        self.ref = 1
        self.pub = rospy.Publisher('carCmd', Float64MultiArray)
        self.pose = PoseStamped()
        self.twist = TwistStamped()
        self.state = CarState()
        self.v = 0
        self.goal = CarGoal()
        self.goal.v = 3.5  # start high because of rate limit
        self.shutdown_requested = False
        self.status = TURN_CONTROL
        self.slipping = False

        # gains
        self.k = 5.0
        self.k_soft = 1.0
        self.k_psi = 0.05
        self.kd = 0.4 * 0.035
        self.kd_steer = 0.2
        self.kp_v = 2.0
        self.ki_v = 0.05

        # integrator value
        self.verr_int = 0

        # store last command
        self.delta_prev = 0

        # constants
        self.mass = 0.906
        self.lF = 0.1
        self.lR = 0.107
        self.Cy = 11.4  # 2.2 * 0.8807
        self.maxSteeringAngle = 20 * np.pi / 180.0
        self.minV = 2.0

    def poseCB(self, data):
        self.pose = data
        q0 = self.pose.pose.orientation.w
        q1 = self.pose.pose.orientation.x
        q2 = self.pose.pose.orientation.y
        q3 = self.pose.pose.orientation.z
        self.psi = math.atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 ** 2 + q3 ** 2))

    def velCB(self, data):
        self.twist = data
        self.v = math.sqrt(self.twist.twist.linear.x ** 2 +
                  self.twist.twist.linear.y ** 2 + self.twist.twist.linear.z ** 2)

    def stateCB(self, state):
        percent_slip = 0.2
        slip1 = np.abs(state.Vx) > np.abs(state.omegaR * params.RADIUS) * (1 + percent_slip)
        slip2 = np.abs(state.Vx) < np.abs(state.omegaR * params.RADIUS) * (1 - percent_slip)
        if slip1 or slip2:  # 10 percent slip min
            self.slipping = True
        else:
            self.slipping = False

    def goalCB(self, data):

        # rate limit the velocity goal command so as to not command steps in v.
        # allow negative steps (hard to brake on car)
        dt = params.CNTRL_RATE
        self.goal.v = utils.rateLimit(data.v, self.goal.v, -np.Inf, 2.0, dt)
        # print self.goal.v

        # assign rest of goal
        self.goal.header = data.header
        self.goal.psi = data.psi
        self.goal.r = data.r
        self.goal.e = data.e
        self.goal.reset_v_int = data.reset_v_int

        # self.goal = data
        if not self.shutdown_requested:
            if self.goal.reset_v_int:
                self.verr_int = 0
            else:
                self.sendCmd(WHEEL_SPEED)

    def joyCB(self, data):
        if data.buttons[B]:
            self.status = TURN_CONTROL
        elif data.buttons[A] or data.buttons[X] or data.buttons[Y] or data.buttons[RB]:
            self.status = MANUAL_CONTROL

    def sendCmd(self, type):

        # current state (controller coming from
        # Autonomous Automobile Trajectory Tracking for Off-Road Driving:
        # Controller Design, Experimental Validation and Racing
        # by Hoffmann, Tomlin, Montemerlo, and Thrun

        psi_control = self.k_psi * wrapPI(self.psi - self.goal.psi)
        psi_ss = self.mass * self.v * self.goal.r / (self.Cy * (1 + self.lF / self.lR))
        cross_term = math.atan(self.k * self.goal.e / (self.k_soft + self.v))

        delta = psi_control - psi_ss + cross_term - self.kd * (self.goal.r -
                                                  self.twist.twist.angular.z)

        # steer damping term (really just a first order filter on the steering signal)
        delta = delta + self.kd_steer * (self.delta_prev - delta)
        self.delta_prev = delta

        # convert delta from radians to -1 to 1, based on max steering angle of car
        delta = delta / self.maxSteeringAngle
        # delta = 0.5

        if math.fabs(self.v) < 0.05:
            delta = 0

        # longitudinal PI controller
        verr = 0
        if self.goal.v >= self.minV:
            verr = self.goal.v - self.v
        dt = params.CNTRL_RATE
        if self.status == TURN_CONTROL and not self.slipping:
            self.verr_int += verr * dt
        v_cmd = 0
        if verr != 0:
            v_cmd = self.kp_v * verr + self.ki_v * self.verr_int

        # convert velocity goal to desired wheel speed
        omegaDes = (self.goal.v + v_cmd) / params.RADIUS

        # send data
        self.cmd.data = [type, omegaDes, delta]
        if self.status == TURN_CONTROL:
            self.pub.publish(self.cmd)

    def signal_handler(self, signal, frame):
        self.shutdown_requested = True
        for i in range(10):
            self.sendCmd(NOT_DRIVING)
            rospy.sleep(0.02)
        rospy.signal_shutdown('Ctrl+C shutdown requested')
        sys.exit(0)

#-----------------------------------------------------------------------------
# Check for out of bounds position of the car
#-----------------------------------------------------------------------------
def checkIfOutofBounds(pose, hardware):
    x = pose.position.x
    y = pose.position.y

    x1 = np.array([params.XOUTOFBOUNDS2, -params.XOUTOFBOUNDS2])
    y1 = np.array([params.YOUTOFBOUNDS2, -params.YOUTOFBOUNDS2])
    x1, y1 = utils.rotate2D(x1, y1, params.OVERALL_ROT)

    xmax = max(x1) + params.XCENTER
    xmin = min(x1) + params.XCENTER
    ymax = max(y1) + params.YCENTER
    ymin = min(y1) + params.YCENTER

#    if hardware:
#        xmax = params.XCENTER + params.XOUTOFBOUNDS2
#        xmin = params.XCENTER - params.XOUTOFBOUNDS2
#        ymax = params.YCENTER + params.YOUTOFBOUNDS2
#        ymin = params.YCENTER - params.YOUTOFBOUNDS2
#    else:
#        xmax = params.XCENTER + params.XOUTOFBOUNDS1
#        xmin = params.XCENTER - params.XOUTOFBOUNDS1
#        ymax = params.YCENTER + params.YOUTOFBOUNDS1
#        ymin = params.YCENTER - params.YOUTOFBOUNDS1

    if x > xmax or x < xmin:
        return True
    if y > ymax or y < ymin:
        return True

    return False

# wrap a function to be within +/-pi
def wrapPI(val):
    while val > math.pi:
        val -= 2 * math.pi
    while val < -math.pi:
        val += 2 * math.pi
    return val

if __name__ == '__main__':


    ns = rospy.get_namespace()


    try:
        rospy.init_node('turn_controller', disable_signals=True)
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=RC01s $ rosrun car_control turn_controller.py")
        else:
            print "Starting policy execution node for: " + str(ns)
            c = TurnController()
            signal.signal(signal.SIGINT, c.signal_handler)
            rospy.Subscriber("pose", PoseStamped, c.poseCB)
            rospy.Subscriber("vel", TwistStamped, c.velCB)
            rospy.Subscriber("goal", CarGoal, c.goalCB)
            rospy.Subscriber("state", CarState, c.stateCB)
            rospy.Subscriber("/joy", Joy, c.joyCB)

            # reset the simulated car before anything else
            rospy.sleep(0.2)
            for i in np.arange(10):
                c.sendCmd(RESET)
                rospy.sleep(0.01)


            rospy.spin()

    except rospy.ROSInterruptException:
        pass
