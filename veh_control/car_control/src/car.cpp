/*
 * quadrotor.cpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#include "car.hpp"

// Initialize RCCar Vehicle
RCCar::RCCar()
{

    // Initialize integrators
    I_x = Integrator();
    I_y = Integrator();
    I_z = Integrator();
    I_yaw = Integrator();
    I_vel = Integrator();

    // Initialize structs
    gains = sOuterGains();
    goal = sGoal();
    state = sState();
    calData = sCalibrationData();
    innerGains = sInnerGains();
    flags = sFlags();

    // Initialize variables
    throttle = 0.0;
    t_old = 0.0;
    voltage = 0.0;
    ux = uy = 0.0;
    run_rollout = false;
    sim = false;
    rollout_data_set = false;
    run_rollout_onboard = false;
    close_position_drift_loop = false;
    use_real_truck = false;
    emergency_stop = false;
    typical_drift = true;
    x0_filt = y0_filt = 0.0;

    // set up random number generator
    distribution = std::uniform_real_distribution<double>(-1.0, 1.0);

    // get parameters from param server
    getParams();

    // first setup the wifly to make sure it is talking to this computer
    //raven::set_wifly_host_ip_telnet(ip_address, txport);

    if (not udp.udpInit(true, (char *) ip_address.c_str(), rxport, txport))
    {
        ROS_ERROR("UDP port failed to open");
        ros::shutdown();
    }

    //## Start a listen thread
    pthread_t threads;
    if (pthread_create(&threads, NULL, listen, (void *) this))
    {
        ROS_ERROR("Listen thread failed to start");
    }

    sendInit();

}

void RCCar::sendInit(void)
{
    // Send on-board gains to car
    for (int i = 0; i < 5; i++)
    {
        sendCal();
        sleep(0.1);
    }

    for (int i = 0; i < 5; i++)
    {
        sendGains();
        sleep(0.1);
    }
}

// Main control loop
void RCCar::runController(const ros::TimerEvent& e)
{

    if (run_rollout and rollout_data_set)
        rollout_controller();

    if ((run_rollout or run_rollout_onboard) and close_position_drift_loop)
      {
            double dt = (e.current_real - e.last_real).toSec();
            driftPositionController(dt);
      }
    else
      {
            ux = uy = 0.0;
      }

    std_msgs::Float64MultiArray simCmd;
    if (status == MANUAL_MODE)
    {
        simCmd.data.push_back(status);
        simCmd.data.push_back(throttle);
        simCmd.data.push_back(turn); // don't need hardware bias in sim
    }
    else if (status == NOT_DRIVING or status == RESET)
    {
        simCmd.data.push_back(status);
        simCmd.data.push_back(0.0);
        simCmd.data.push_back(0.0);
    }
    else if (status == WHEEL_SPEED)
    {
        simCmd.data.push_back(status);
        simCmd.data.push_back(omegaDes);
        simCmd.data.push_back(turn);
    }
//      ROS_INFO_STREAM("status is: " << status);
    simCmdPub.publish(simCmd);

    // // add hardware specific constraints
    // if (turn >= 0.0)
    // {
    //         turn = turn * fabs(gains.maxTurn);
    // }
    // else
    // {
    //         turn = turn * fabs(gains.minTurn);
    // }

    // // add turn bias to turn
    // turn = acl::saturate(turn + gains.turnBias, gains.maxTurn, gains.minTurn);

    omegaDes = acl::saturate(omegaDes, gains.maxOmega, -10000.0);

    // send data
    checkStatus();
    sendCmd();

    // broadcast calculated data as ros messages for loggin' and debuggin'
    broadcastROSdata();

    // print pertinent data to screen
    if (flags.screenPrint)
        screenPrint();

}

void RCCar::rollout_controller()
{
    if (rollout_counter < 0)
    {
        // start motor going
        turn = 0;
        throttle = min_throttle;
        status = MANUAL_MODE;
    }
    else if (rollout_counter < H + 1)
    {
        // run controller
        turn = GPController(num_centers, D, 0);
        if (typical_drift)
            throttle = drift_throttle;
        else
        {
            throttle = GPController(num_centers, D, 1);
            throttle /= 2.0;
            throttle += 0.5;
        }
        std::cout << turn << std::endl;

        if (random_policy)
        {
            turn = distribution(generator);
        }

        turn = acl::saturate(turn, 1.0, -1.0);
        // throttle = drift_throttle;
        status = MANUAL_MODE;

        // save the data
        if (rollout_counter < H)
        {
            if (typical_drift)
            {
                x(rollout_counter, 0) = carState.Vx;
                x(rollout_counter, 1) = carState.Vy;
                x(rollout_counter, 2) = carState.r;
                //x(rollout_counter, 3) = carState.omegaR;
                x(rollout_counter, 3) = turn;
            } else
            {
                x(rollout_counter, 0) = state.vel.getX();
                x(rollout_counter, 1) = state.vel.getY();
                x(rollout_counter, 2) = carState.r;
                x(rollout_counter, 3) = carState.omegaR;
                x(rollout_counter, 4) = tf::getYaw(state.att);
                x(rollout_counter, 5) = turn;
                x(rollout_counter, 6) = throttle - 0.5;
            }
        }
        if (rollout_counter > 0)
        {
            if (typical_drift)
            {
                y(rollout_counter - 1, 0) = carState.Vx;
                y(rollout_counter - 1, 1) = carState.Vy;
                y(rollout_counter - 1, 2) = carState.r;
            } else
            {
                y(rollout_counter - 1, 0) = state.vel.getX();
                y(rollout_counter - 1, 1) = state.vel.getY();
                y(rollout_counter - 1, 2) = carState.r;
                y(rollout_counter - 1, 3) = carState.omegaR;
                y(rollout_counter - 1, 4) = tf::getYaw(state.att);
            }
        }
    }
    else
    {
        // controller off -- waiting
        turn = throttle = 0.0;
        status = NOT_DRIVING;
    }

    rollout_counter++;
    std::cout << rollout_counter << std::endl;

}

// check the status of the vehicle and set desired values to zero if not good
void RCCar::checkStatus()
{
    bool shouldFly = true;
    errorMsg.str(""); // clear string
    warnMsg.str(""); // clear string

    if (status == NOT_DRIVING or emergency_stop)
    {
        turn = throttle = 0;
        run_rollout_onboard = false;
        run_rollout = false;
        status = MANUAL_MODE;
    }

    // check bounding box
//  if (status != MANUAL_MODE && status != NOT_DRIVING)
//  {
//        if (state.pos.getX() > XMAX || state.pos.getY() > YMAX || state.pos.getX() < XMIN || state.pos.getY() < YMIN)
//        {
//                turn = 0;
//                throttle = 0;
//                warnMsg << "Outside allowable driving area"<< std::endl;
//        }
//  }

    // initialization routine
    if (startTime.now().toSec() < (startTime.toSec() + 5.0))
    {

    }

    // check for communication with the quadrotor
//  if (healthData.voltage == 0)
//  {
//    warnMsg << "Not communicating with RCCar -- try checking xbee connection" << std::endl;
//    //shouldFly = false;
//  }

    // Check that we are actually receiving vicon data
//  if (state.vel.getX() == 0.0 && state.vel.getY() == 0.0 && state.vel.getZ() == 0.0)
//  {
//    errorMsg << "Not getting vehicle Vicon data. Is ROSTracker running or is the vehicle occluded?" << std::endl;
//    shouldFly = false;
//  } // Check that the vicon data we have is current
    else if ((ros::Time::now().toSec() - state.time.toSec())
            > MAX_VICON_WAIT_TIME)
    {

    }

}

// broadcast all pertinent control data as ros messages for logging and debugging
void RCCar::broadcastROSdata()
{

    /******** Command Data *********/
//  quad_control::quadCmd cmd;
//
//  // Header
//  cmd.header.stamp = ros::Time::now();
//  cmd.header.frame_id = name;
//
//  // Commanded values
//  cmd.pose.position.x = goal.pos.getX();
//  cmd.pose.position.y = goal.pos.getY();
//  cmd.pose.position.z = goal.pos.getZ();
//  tf::quaternionTFToMsg(goal.att, cmd.pose.orientation);
//  tf::vector3TFToMsg(goal.vel, cmd.twist.linear);
//  tf::vector3TFToMsg(goal.rate, cmd.twist.angular);
//  tf::vector3TFToMsg(goal.accel, cmd.accel);
//  tf::vector3TFToMsg(goal.accel_fb, cmd.accel_fb);
//  tf::vector3TFToMsg(goal.jerk, cmd.jerk);
//  tf::vector3TFToMsg(goal.jerk_fb, cmd.jerk_fb);
//  cmd.pos_integrator.x = I_x.value;
//  cmd.pos_integrator.y = I_y.value;
//  cmd.pos_integrator.z = I_z.value;
//  cmd.f_total = goal.f_total;
//  cmd.throttle = throttle;
//  cmd.collective = coll;
//
//  double r, p, y;
//  quaternion2Euler(goal.att, r, p, y);
//  cmd.rpy.x = r; // roll
//  cmd.rpy.y = p; // pitch
//  cmd.rpy.z = y; // yaw
//
//  cmds.publish(cmd);
    /************ Health Data **********/
//  healthData.header.stamp = ros::Time::now();
//  healthData.header.frame_id = name;
//
//  quaternion2Euler(healthData.att, r, p, y);
//  healthData.rpy.x = r; // roll
//  healthData.rpy.y = p; // pitch
//  healthData.rpy.z = y; // yaw
//
//  if (flags.streamData)       // if we are streaming data, use it, otherwise
//  {                                           // rebroadcast vicon attitude in euler angles
//    quaternion2Euler(healthData.att_meas, r, p, y);
//    healthData.rpy_meas.x = r; // roll
//    healthData.rpy_meas.y = p; // pitch
//    healthData.rpy_meas.z = y; // yaw
//  }
//  else
//  {
//    quaternion2Euler(state.att, r, p, y);
//    healthData.rpy_meas.x = r; // roll
//    healthData.rpy_meas.y = p; // pitch
//    healthData.rpy_meas.z = y; // yaw
//  }
//
//  health.publish(healthData);
    /************ Goal Data (for matlab's sake) **********/
//  geometry_msgs::PoseStamped msg;
//  msg.header.stamp = ros::Time::now();
//  msg.header.frame_id = name;
//
//  msg.pose.position.x = goal.pos.getX();
//  msg.pose.position.y = goal.pos.getY();
//  msg.pose.position.z = goal.pos.getZ();
//  tf::Quaternion qyaw;
//  qyaw.setRPY(0, 0, goal.yaw);
//  tf::quaternionTFToMsg(qyaw, msg.pose.orientation);
//
//  sendGoal4Matlab.publish(msg);
}

void RCCar::screenPrint()
{
    if (not errorMsg.str().empty())
        ROS_ERROR_STREAM_THROTTLE(SCREEN_PRINT_RATE, errorMsg.str());

    if (not warnMsg.str().empty())
        ROS_WARN_STREAM_THROTTLE(SCREEN_PRINT_RATE, warnMsg.str());

    std::string status_msg;
    if (status == NOT_DRIVING)
        status_msg = "NOT_DRIVING";
    else if (status == MANUAL_MODE)
        status_msg = "MANUAL_MODE";
    else if (status == WHEEL_SPEED)
        status_msg = "WHEEL_SPEED";

    std::ostringstream msg;
    msg.setf(std::ios::fixed); // give all the doubles the same precision
    msg.setf(std::ios::showpos); // show +/- signs always
    msg << std::setprecision(4) << std::endl; // set precision to 4 decimal places
    msg << "Act Pos:  x: " << state.pos.getX() << "  y: " << state.pos.getY()
            << "  z: " << state.pos.getZ() << std::endl;
//  msg << "Des Pos:  x: " << goal.pos.getX() << "  y: " << goal.pos.getY() << "  z: " << goal.pos.getZ() << std::endl
//      << std::endl;
//
    msg << "Act Vel:  x: " << state.vel.getX() << "  y: " << state.vel.getY()
            << "  z: " << state.vel.getZ() << std::endl;
    msg << "Des Vel: " << goal.speed << std::endl << std::endl;
    //msg << "OmegaR: " <<

    msg << "Act Yaw:  " << tf::getYaw(state.att) << std::endl;
    msg << "Des Yaw:  " << goal.heading << std::endl;
    msg << "Yaw Err:  " << goal.heading - tf::getYaw(state.att) << std::endl
            << std::endl;
//
//  double r, p, y;
//  quaternion2Euler(state.att, r, p, y);
//  msg << "Act Att:  r: " << r << "  p: " << p << "  y: " << y << std::endl;
//  quaternion2Euler(goal.att, r, p, y);
//  msg << "Des Att:  r: " << r << "  p: " << p << "  y: " << y << std::endl << std::endl;
//
//  msg << "Act Rate: p: " << state.rate.getX() << "  q: " << state.rate.getY() << "  r: " << state.rate.getZ()
//      << std::endl;
//  msg << "Des Rate: p: " << goal.rate.getX() << "  q: " << goal.rate.getY() << "  r: " << goal.rate.getZ() << std::endl
//      << std::endl;

    msg << "Throttle: " << throttle << "  Turn: " << turn << "  OmegaDes: "
            << omegaDes << std::endl;
    msg << "Vx: " << carState.Vx << "  Vy: " << carState.Vy << "  dPsi: "
            << carState.r << std::endl;
    msg << "Voltage:  " << voltage << std::endl;
    msg << "Status:  " << status_msg << " Running Rollout: " << run_rollout <<
            " Closing Position Loop: " << close_position_drift_loop << std::endl << std::endl;

    msg << "Wheel Velocity: " << carState.omegaR * 0.0606 / 2.0 << std::endl;
    msg << "Vx: " << carState.Vx << std::endl;

    msg << "Seconds since last unique state information: "
            << ros::Time::now().toSec() - state.time.toSec();
    msg << std::endl << std::endl;

    ROS_INFO_STREAM_THROTTLE(SCREEN_PRINT_RATE, msg.str());
    // Print at 1/0.5 Hz = 2 Hz

}

void RCCar::driftPositionController(double dt)
{
    // Calculate the current center of rotation
    double V = acl::norm(carState.Vx, carState.Vy);
    double R = 10000;
    bool validR = false;
    if (fabs(carState.r) > 0.1)
    {
        R = V/ carState.r; // radius of rotation is psi_dot*V
        validR = true;
    }
    double xdot = state.vel.getX();
    double ydot = state.vel.getY();

    double theta = atan2(ydot, xdot); // angle of R (polar coordinates)

    // position of the center of rotation
    double x0 = carState.pose.position.x - R * sin(theta);
    double y0 = carState.pose.position.y + R * cos(theta);
    double alpha = 0.05;
    x0_filt = (1-alpha)*x0_filt + alpha*x0;
    y0_filt = (1-alpha)*y0_filt + alpha*y0;

    if (not use_real_truck)
    {
        target_pos.x = 0;
        target_pos.y = 0;
        target_vel.x = 0;
        target_vel.y = 0;
    }

    double goal_x = target_pos.x;
    double goal_y = target_pos.y;

    double max_err = 1.0;
    int xsat = 0;
    int ysat = 0;
    double xerr = acl::saturate(goal_x - x0_filt, max_err, -max_err, xsat);
    double yerr = acl::saturate(goal_y - y0_filt, max_err, -max_err, ysat);

    double kp = 0.0;
    double ki = 0.0;
    if (validR)
    {
        kp = raven::param_double("drift_kp", true); //0.5;
        ki = raven::param_double("drift_ki", true); //0.1;
        if (not xsat)
            I_ux.increment(xerr, dt);
        else
            I_ux.reset();

        if (not ysat)
            I_uy.increment(yerr, dt);
        else
            I_uy.reset();
    } else
    {
        I_ux.reset();
        I_uy.reset();
    }

    ux = kp*xerr + ki*I_ux.value + target_vel.x;
    uy = kp*yerr + ki*I_uy.value + target_vel.y;

    ux = acl::saturate(ux, 0.5, -0.5);
    uy = acl::saturate(uy, 0.5, -0.5);

    // fill member variables
    carState.R = R;
    carState.x_center = x0_filt;
    carState.y_center = y0_filt;

    // TODO: need to numerically differentiate x0, y0 signal

    std::cout << "x_center: " << x0_filt << ", y_center: " << y0_filt << std::endl;
}

// Get Joy messages
void RCCar::joyCallBack(const sensor_msgs::Joy& msg)
{
    if (close_position_drift_loop and msg.buttons[acl_msgs::JoyDef::RB])
        close_position_drift_loop = false;
    else if (not close_position_drift_loop and msg.buttons[acl_msgs::JoyDef::LB])
        close_position_drift_loop = true;
    if (use_real_truck and msg.buttons[acl_msgs::JoyDef::Y])
        use_real_truck = false;
    else if (not use_real_truck and msg.buttons[acl_msgs::JoyDef::X])
        use_real_truck = true;
    if (msg.buttons[acl_msgs::JoyDef::B])
        emergency_stop = true;
    if (msg.buttons[acl_msgs::JoyDef::A])
        emergency_stop = false;
}

// Get POSE messages from Vicon
void RCCar::poseCallback(const geometry_msgs::PoseStamped& msg)
{

    //ros::Time msg_time = msg.header.stamp;
    //double delay = msg_time.now().toSec() - msg_time.toSec();
    //printf("delay: %f\n",delay);

    // check that the data we received is different from the data last time
    if (not ((msg.pose.position.x == state.pos.getX())
            and (msg.pose.position.y == state.pos.getY())))
    {
        state.time = ros::Time::now(); // get current time -- used in check status to see if vicon data is current
        // time is only evaluated if we are getting new data
    }

    // update state variables
    geometry_msgs::Vector3 position;
    position.x = msg.pose.position.x;
    position.y = msg.pose.position.y;
    position.z = msg.pose.position.z;
    tf::vector3MsgToTF(position, state.pos);
    tf::quaternionMsgToTF(msg.pose.orientation, state.att);

}

// Get TWIST messages from Vicon
void RCCar::velCallback(const geometry_msgs::TwistStamped& msg)
{

    // update state variables
    geometry_msgs::Vector3 vel;
    vel.x = msg.twist.linear.x - ux;
    vel.y = msg.twist.linear.y - uy;
    vel.z = msg.twist.linear.z;

    geometry_msgs::Vector3 rate;
    rate.x = msg.twist.angular.x;
    rate.y = msg.twist.angular.y;
    rate.z = msg.twist.angular.z;

    tf::vector3MsgToTF(vel, state.vel);
    tf::vector3MsgToTF(rate, state.rate);

}

// Get ACCEL messages from Vicon
void RCCar::accelCallback(const geometry_msgs::Vector3Stamped& msg)
{
    // update state variables
    geometry_msgs::Vector3 accel;
    accel.x = msg.vector.x;
    accel.y = msg.vector.y;
    accel.z = msg.vector.z;

    tf::vector3MsgToTF(accel, state.accel);

}

void RCCar::stateCallback(const acl_msgs::CarState& msg)
{
    carState = msg;
    double yaw = tf::getYaw(state.att);
    carState.Vx = state.vel.getX() * cos(yaw) +
            state.vel.getY() * sin(yaw);
    carState.Vy = -state.vel.getX() * sin(yaw) +
            state.vel.getY() * cos(yaw);
}

// Get target pose messages from Vicon
void RCCar::poseTargetCallback(const geometry_msgs::PoseStamped& msg)
{

    // update state variables
    target_pos.x = msg.pose.position.x;
    target_pos.y = msg.pose.position.y;

}

// Get target vel messages from Vicon
void RCCar::velTargetCallback(const geometry_msgs::TwistStamped& msg)
{

    // update state variables
    target_vel.x = msg.twist.linear.x;
    target_vel.y = msg.twist.linear.y;

}


// Listen to CmdMessages (from joystick or other program)
// relay command callback to simulator in case we are running in simulated evironment
void RCCar::cmdCallback(const std_msgs::Float64MultiArray& msg)
{
    if (msg.data.size() == 3)
    {
        // if we are getting joystick data, stop any pilco rollouts that are running
        if (run_rollout)
        {
            run_rollout = false;
            controllerTimer.setPeriod(ros::Duration(controlDT));
        }

        if (run_rollout_onboard)
        {
            ROS_ERROR("Exiting the onboard GP controller");
            run_rollout_onboard = false;
            status = MANUAL_MODE;
        }

        status = msg.data[0];
//              ROS_INFO_STREAM("status: " << status);
        if (status == WHEEL_SPEED)
        {
            omegaDes = msg.data[1];
//                      omegaDes = acl::saturate(omegaDes, innerGains.maxOmega,
//                                      -innerGains.maxOmega);
            turn = acl::saturate(msg.data[2], 1.0, -1.0);
            throttle = 0.0;
            I_vel.reset();
            I_yaw.reset();
        }

        else
        {
            throttle = acl::saturate(msg.data[1], 1.0, -1.0);
            turn = acl::saturate(msg.data[2], 1.0, -1.0);
            omegaDes = 0;
            I_vel.reset();
            I_yaw.reset();
        }
    }
}

bool RCCar::rollout(acl_msgs::PilcoRollout::Request &req,
        acl_msgs::PilcoRollout::Response &res)
{
    rollout_data_set = true;

    // get params
    // linear control
//      w_param.setZero();
//      w_param << req.w[0], req.w[1], req.w[2];
//      b_param = req.b;

    // gp control
    num_centers = req.num_centers;
    D = req.D;
    E = req.E;
    if (req.dy0 == 0)
        typical_drift = true;
    else
        typical_drift = false;

    for (int ii=0; ii<E; ii++)
    {
        MatrixXd input_tmp;
        VectorXd iL2_tmp, beta_tmp;

        input_tmp.setZero(num_centers, D);
        beta_tmp.setZero(num_centers);
        iL2_tmp.setZero(D);
        int cnt = 0;
        for (int i = 0; i < num_centers; i++)
        {
            for (int j = 0; j < D; j++)
            {
                input_tmp(i, j) = req.params[ii].input[cnt];
                cnt++;
            }
            beta_tmp[i] = req.params[ii].beta[i];
        }
        for (int i = 0; i < D; i++)
        {
            iL2_tmp[i] = req.params[ii].iL2[i];
        }
        gp_input.push_back(input_tmp);
        beta.push_back(beta_tmp);
        iL2.push_back(iL2_tmp);

            // DEBUGGING
        std::cout << "Inputs: " << std::endl;
        std::cout << input_tmp << std::endl << std::endl;
        std::cout << "Beta: " << std::endl;
        std::cout << beta_tmp << std::endl << std::endl;
        std::cout << "iL2: " << std::endl;
        std::cout << iL2_tmp << std::endl << std::endl;
        std::cout << "random: " << random_policy << std::endl;
    }

    H = req.H;
    random_policy = req.random;



    x = MatrixXd::Zero(H, D + E);
    y = MatrixXd::Zero(H, D);

//      for (int i=0; i<3; i++)
//      {
//              status = MANUAL_MODE;
//              sendCmd();
//              ros::Duration(0.2).sleep();
//      }

    // turn on the controller
    if (typical_drift)
        rollout_counter = -1;
    else
        rollout_counter = -50; //2;

    run_rollout = true;
    controllerTimer.setPeriod(ros::Duration(req.rate));

    should_log = true;
    bool trial_started = false;
    while (rollout_counter < H + 5 and run_rollout)
    {
        if (not typical_drift and (fabs(state.vel.getY()) >= fabs(req.dy0))
            and not trial_started)
        {
            trial_started = true;
            rollout_counter = 0;
        }
        //std::cout << state.vel.getY() << std::endl;
        ros::Duration(0.01).sleep();
    }
    should_log = false;
    run_rollout = false;
    controllerTimer.setPeriod(ros::Duration(controlDT));

    // reset control variables
    gp_input.clear();
    beta.clear();
    iL2.clear();

    // fill output matrices
    res.x.data.clear();
    res.y.data.clear();
    for (int i = 0; i < H; i++)
    {
        for (int j = 0; j < D + E; j++)
        {
            res.x.data.push_back(x(i, j));
            if (j < D)
                res.y.data.push_back(y(i, j));
        }
    }

    std::cout << "y:" << std::endl;
    std::cout << res.y << std::endl << std::endl;

    rollout_data_set = false;
    return true;
}

bool RCCar::rollout_onboard(acl_msgs::PilcoRollout::Request &req,
        acl_msgs::PilcoRollout::Response &res)
{
    rollout_data_set = true;

    // gp control
    num_centers = req.num_centers;
    D = req.D;
    E = req.E;
    if (req.dy0 == 0)
        typical_drift = true;
    else
        typical_drift = false;

    for (int ii=0; ii<E; ii++)
    {
        MatrixXd input_tmp;
        VectorXd iL2_tmp, beta_tmp;

        input_tmp.setZero(num_centers, D);
        beta_tmp.setZero(num_centers);
        iL2_tmp.setZero(D);
        int cnt = 0;
        for (int i = 0; i < num_centers; i++)
        {
            for (int j = 0; j < D; j++)
            {
                input_tmp(i, j) = req.params[ii].input[cnt];
                cnt++;
            }
            beta_tmp[i] = req.params[ii].beta[i];
        }
        for (int i = 0; i < D; i++)
        {
            iL2_tmp[i] = req.params[ii].iL2[i];
        }
        gp_input.push_back(input_tmp);
        beta.push_back(beta_tmp);
        iL2.push_back(iL2_tmp);
    }

    H = req.H;
    random_policy = req.random;
    rollout_counter = -1; //-3; //2;
    status = GP_CONTROL;

    // send data onboard
    for (int i = 0; i < 4; i++) // send multiple times just in case
    {
        for (int j = 0; j < 4; j++) // GP input is broken up into 4 packets since it is quite large
        {
            ros::Duration(0.02).sleep();
            sendGPInput(j);
        }
        ros::Duration(0.02).sleep();
        sendGPBeta();
    }

    // DEBUGGING
//    std::cout << "Inputs: " << std::endl;
//    std::cout << gp_input << std::endl << std::endl;
//    std::cout << "Beta: " << std::endl;
//    std::cout << beta << std::endl << std::endl;
//    std::cout << "iL2: " << std::endl;
//    std::cout << iL2 << std::endl;
//    std::cout << "random: " << random_policy << std::endl << std::endl;

    std::cout << "Rollout counter: " << rollout_counter << std::endl;

    x = MatrixXd::Zero(H, D + E);
    y = MatrixXd::Zero(H, D);

    run_rollout_onboard = true;

    should_log = true;
    double timer = 0;
    while (rollout_counter < H + 5 and run_rollout_onboard and timer < 1000.0)
    {
        ros::Duration(0.1).sleep();
        timer += 0.1;
        //std::cout << rollout_counter << std::endl;
    }
    should_log = false;
    run_rollout_onboard = false;



//      controllerTimer.stop();

    // reset control variables
    gp_input.clear();
    beta.clear();
    iL2.clear();

    // fill output matrices
    res.x.data.clear();
    res.y.data.clear();
    for (int i = 0; i < H; i++)
    {
        for (int j = 0; j < D + 1; j++)
        {
            res.x.data.push_back(x(i, j));
            if (j < D)
                res.y.data.push_back(y(i, j));
        }
    }

//        std::cout << "y:" << std::endl;
//        std::cout << res.y << std::endl << std::endl;

    rollout_data_set = false;
    return true;
}

double RCCar::GPController(int num_centers, int D, int e)
{
    // compute control -- GP
    double u_tmp = 0;
    double mean[D];
    if (typical_drift)
    {
        mean[0] = carState.Vx;
        mean[1] = carState.Vy;
    } else
    {
        mean[0] = state.vel.getX();
        mean[1] = state.vel.getY();
    }
    mean[2] = carState.r;
    if (D > 3)
    {
        mean[3] = carState.omegaR;
        mean[4] = tf::getYaw(state.att);
    }

    int i, j;
    for (i = 0; i < num_centers; i++)
    {
        double u_tmp2 = 1.0;
        for (j = 0; j < D; j++)
        {
            double diff = gp_input.at(e)(i, j) - mean[j];
            diff = diff * diff;
            u_tmp2 = u_tmp2 * exp(-0.5 * diff * iL2.at(e)[j]);
        }
        u_tmp = u_tmp + u_tmp2 * beta.at(e)[i];
    }

    // saturation function
    double u = 9 / 8. * sin(u_tmp) + 1 / 8. * sin(3 * u_tmp);

    std::cout << "Vx: " << mean[0] << ", Vy: " << mean[1] << ", r: " << mean[2]
            << ", u: " << u << std::endl << std::endl;

    return u;

}

// Get the saved bag file at the end of a flight and convert it to a MATLAB-readable file
int RCCar::writeDataLog2File(void)
{
    int i = 0;
    // open /tmp/ directory where bag files are saved
    DIR *dp;
    struct dirent *dirp;
    if ((dp = opendir("/tmp/")) == NULL)
    {
        std::cout << "Error opening /tmp/ " << std::endl;
        return -1;
    }

// get a list of all the sensor bag files in that directory
    std::vector<sensorFile> filesVec;
    std::string name = ros::this_node::getNamespace();
    name.erase(0, 1);
    int hl = name.size() + 7; // length of name + header
    while ((dirp = readdir(dp)) != NULL)
    {
        std::string fileName = dirp->d_name;
        std::string wantedName = name + "LogFile";
        if (wantedName.compare(fileName.substr(0, hl)) == 0)
        {
            // name fomat: BQ05sensors_2012-09-26-10-22-22.bag
            // header: 0,11 or 12 (starting at position 0, 11 chars long
            // year: 12,4
            // month: 17,2
            // day: 20,2
            // hour: 23,2
            // minute: 26,2
            // second: 29,2
            sensorFile sf;
            sf.name = "/tmp/" + fileName;
            sf.date.tm_year = (int) atoi(fileName.substr(hl + 1, 4).c_str())
                    - 1900;
            sf.date.tm_mon = (int) atoi(fileName.substr(hl + 6, 4).c_str()) - 1;
            sf.date.tm_mday = (int) atoi(fileName.substr(hl + 9, 2).c_str());
            sf.date.tm_hour = (int) atoi(fileName.substr(hl + 12, 2).c_str());
            sf.date.tm_min = (int) atoi(fileName.substr(hl + 15, 2).c_str());
            sf.date.tm_sec = (int) atoi(fileName.substr(hl + 18, 2).c_str());
            time_t epochtime;
            epochtime = mktime(&sf.date);
            sf.time = (int) epochtime;
            filesVec.push_back(sf);
        }
    }
    closedir(dp);

    // sort that list to get the one with the latest time stamp
    if (filesVec.empty())
    {
        ROS_ERROR("Couldn't find bag file to process");
        return -1;
    }
    std::sort(filesVec.begin(), filesVec.end(), sensorFile());
    sensorFile file2Parse = filesVec.back();
    std::cout << std::endl << "Parsing bag file " << file2Parse.name
            << std::endl << std::endl;

    rosbag::Bag bag;
    bag.open(file2Parse.name, rosbag::bagmode::Read);

    // Open file for human readable format textfile
    std::string path = ros::package::getPath("car_control");
    path += "/matlab/carlog.txt";
    FILE * writeFile;
    writeFile = fopen(path.c_str(), "w");
    char outStr[512];

    std::vector<std::string> topics;
    topics.push_back(std::string("carCmd"));
    topics.push_back(std::string("pose"));
    topics.push_back(std::string("vel"));
    topics.push_back(std::string("accel"));
    topics.push_back(std::string("state"));

    rosbag::View view(bag, rosbag::TopicQuery(topics));

    std_msgs::Float64MultiArray cmd;
    geometry_msgs::PoseStamped pose;
    geometry_msgs::TwistStamped twist;
    geometry_msgs::Vector3Stamped accel;
    acl_msgs::CarState state;

    int cnt = 0;
    BOOST_FOREACH(rosbag::MessageInstance const m, view)
    {
        std_msgs::Float64MultiArray::ConstPtr q = m.instantiate<
                std_msgs::Float64MultiArray>();
        if (q != NULL)
        {
            cmd = *q;

        }

        geometry_msgs::PoseStamped::ConstPtr p = m.instantiate<
                geometry_msgs::PoseStamped>();
        if (p != NULL)
        {
            pose = *p;
        }

        geometry_msgs::TwistStamped::ConstPtr t = m.instantiate<
                geometry_msgs::TwistStamped>();
        if (t != NULL)
        {
            twist = *t;
        }

        geometry_msgs::Vector3Stamped::ConstPtr a = m.instantiate<
                geometry_msgs::Vector3Stamped>();
        if (a != NULL)
        {
            accel = *a;
        }

        acl_msgs::CarState::ConstPtr s = m.instantiate<acl_msgs::CarState>();
        if (s != NULL)
        {
            state = *s;

            // the pose and twist data is published twice as fast as the command data;  However,
            // only save data when command data is published because this is the most relevant.

            if (cmd.data.size() == 3)
            {
                if (cmd.data.at(0) != NOT_DRIVING) // In this mode throttle/turn data is commanded from joystick or other program
                {
                    sprintf(outStr, "%f,%f,%f,",
                            (double) ros::Time::now().toSec(), // worthless time
                            cmd.data.at(1), cmd.data.at(2));
                    fputs(outStr, writeFile);

                    sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,",
                            (double) pose.header.stamp.toSec(),
                            pose.pose.position.x, pose.pose.position.y,
                            pose.pose.position.z, pose.pose.orientation.w,
                            pose.pose.orientation.x, pose.pose.orientation.y,
                            pose.pose.orientation.z);
                    fputs(outStr, writeFile);

                    sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,",
                            (double) twist.header.stamp.toSec(),
                            twist.twist.linear.x, twist.twist.linear.y,
                            twist.twist.linear.z, twist.twist.angular.x,
                            twist.twist.angular.y, twist.twist.angular.z);
                    fputs(outStr, writeFile);

                    sprintf(outStr, "%f,%f,%f,%f,",
                            (double) accel.header.stamp.toSec(), accel.vector.x,
                            accel.vector.y, accel.vector.z);
                    fputs(outStr, writeFile);

                    sprintf(outStr,
                            "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n",
                            (double) state.header.stamp.toSec(),
                            state.pose.position.x, state.pose.position.y,
                            state.pose.position.z, state.pose.orientation.w,
                            state.pose.orientation.x, state.pose.orientation.y,
                            state.pose.orientation.z, state.Vx, state.Vy,
                            state.r, state.omegaF, state.omegaR,
                            state.throttle, state.turn, state.x_center,
                            state.y_center, state.R);
                    fputs(outStr, writeFile);

                }
            }

            cnt++;

        }
    }

    std::cout << "Processed " << cnt << " lines successfully" << std::endl;
    std::cout << "File saved: " << path << std::endl << std::endl << std::endl;

    fclose(writeFile);
    bag.close();

    return 0;
}
