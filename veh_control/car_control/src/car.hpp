/*
 * quadrotor.hpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef RCCAR_HPP_
#define RCCAR_HPP_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "std_msgs/Float64MultiArray.h"
#include "sensor_msgs/Joy.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include <ros/package.h>

#include "acl_msgs/CarState.h"
#include "acl_msgs/PilcoRollout.h"
#include "acl_msgs/JoyDef.h"

// acl utils library
#include "acl/comms/udpPort.hpp"
#include "acl/utils.hpp"

// ACL ros library
#include "raven_utils/utils.hpp"

// Local includes
#include "structs.h"
#include "comm.hpp"
#include "defines.h"

// Global includes
#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <boost/foreach.hpp>
#include <signal.h>
#include <cmath>

#include <eigen3/Eigen/Dense>
using namespace Eigen;

//## Serial listen thread
void *listen(void *param);

const double WHEEL_RADIUS = 0.035 / 2.0;

class RCCar
{
public:
    RCCar();

    //## ROS message publishing
    ros::Publisher statePub, simCmdPub;
    ros::Timer controllerTimer;
    bool sim;

    // message callbacks
    void poseCallback(const geometry_msgs::PoseStamped& msg);
    void poseTargetCallback(const geometry_msgs::PoseStamped& msg);
    void velCallback(const geometry_msgs::TwistStamped& msg);
    void velTargetCallback(const geometry_msgs::TwistStamped& msg);
    void accelCallback(const geometry_msgs::Vector3Stamped& msg);
    void cmdCallback(const std_msgs::Float64MultiArray& msg);
    void joyCallBack(const sensor_msgs::Joy& msg);
    void stateCallback(const acl_msgs::CarState& msg);
    //void rvizCallback(const visualization_msgs::InteractiveMarkerFeedback& msg);

    // setup functions
    void sendInit(void);

    // convert rosbag file to csv file
    int writeDataLog2File(void);

    // ROS timed functions
    void runController(const ros::TimerEvent&);

    //## Communication with RCCar
    acl::UDPPort udp;
    void parsePacket(uint8_t ID, uint8_t length, uint8_t * data);

    sFlags flags;

    // service
    bool rollout(acl_msgs::PilcoRollout::Request &req,
            acl_msgs::PilcoRollout::Response &res);
    bool rollout_onboard(acl_msgs::PilcoRollout::Request &req,
            acl_msgs::PilcoRollout::Response &res);

    //## Communication with RCCar
    void SendPacket(uint8_t ID, uint8_t length, uint8_t * data);

private:

    double GPController(int num_centers, int D, int e);
    void rollout_controller();

    //## Control functions
    void velHeadingCntl();
    void checkStatus();
    void driftPositionController(double dt);

    //## Logging and Debugging Functions
    void broadcastROSdata();
    void screenPrint();

    //## Parameter functions for accessing param server
    void getParams();
    double param(std::string n);
    void param(std::string n, bool &out);
    void param(std::string n, std::string &out);

    //## Control Structs and msgs
    sGoal goal;
    sState state;
    sOuterGains gains;
    sInnerGains innerGains;
    sCalibrationData calData;
    Integrator I_x, I_y, I_z, I_ux, I_uy;
    acl_msgs::CarState carState;
//  quad_control::quadHealth healthData;
//  quad_control::rawSensors sensors;
//  quad_control::adaptcmd adaptcmd;

//## Communication with RCCar
    void sendCmd(void);
    void sendGains(void);
    void sendCal(void);
    void sendGPBeta(void);
    void sendGPInput(int seq_num);
    std::string ip_address;
    int rxport, txport;

    //## Comm structs
    tWheelSpeedPacket wheelSpeedPacket;
    tWheelSpeed2Packet wheelSpeed2Packet;
//  tSensorCalPacket sensorCal;
//  tAHRSpacket ahrs;
//  tLogPacket logPacket;
//  tHealthPacket healthPacket;
//  tSensorsPacket sensorsPacket;
//  tAltitudePacket altitudePacket;
//  tAdaptCmdPacket adaptcmdPacket;

// control information
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution;
//      Vector3d w_param;
//      double b_param;
    bool random_policy, should_log, run_rollout, rollout_data_set;
    bool run_rollout_onboard;
    bool close_position_drift_loop, typical_drift, use_real_truck, emergency_stop;
    MatrixXd x;
    MatrixXd y;
    std::vector<MatrixXd> gp_input;
    std::vector<VectorXd> beta;
    std::vector<VectorXd> iL2;
    double turn, throttle, omegaDes, drift_throttle, min_throttle;
    int status, H, num_centers, D, E, rollout_counter;
    Integrator I_yaw;
    Integrator I_vel;
    std::ostringstream errorMsg, warnMsg;

    double voltage;
    double ux, uy, x0_filt, y0_filt;
    geometry_msgs::Vector3 target_pos, target_vel;

    // timer for initializing motors
    ros::Time startTime;

    // RCCar Mass
    double mass;

    // for numerical differentiation of acceleration
    double t_old;

    // Utilities

    void quaternion2Euler(tf::Quaternion q, double &roll, double &pitch,
            double &yaw);
    void quaternion2Euler(geometry_msgs::Quaternion q, double &roll,
            double &pitch, double &yaw);
};

#endif /* RCCAR_HPP_ */
