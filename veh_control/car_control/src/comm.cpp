/*
 * comm.cpp
 *
 *  Created on: March 26, 2011
 *      Author: mark
 */

#include "car.hpp"

extern bool ABORT;
extern int ABORT_cnt;

void RCCar::sendCal()
{

    tSensorCalPacket pkt;
    pkt.gyroScale = (int16_t) (calData.gyroScale * QUAT_SCALE * ROT_SCALE);
    pkt.accelScale = (int16_t) (calData.accelScale * QUAT_SCALE * ROT_SCALE);
    pkt.K_Att_Filter =
            (int16_t) (calData.K_Att_Filter * QUAT_SCALE * ROT_SCALE);

    SendPacket(PACKETID_SENSORCAL, sizeof(tSensorCalPacket), (uint8_t *) &pkt);
}

bool writeLogFile = false;
void RCCar::sendCmd()
{
    tCarCmdPacket pkt;
    if (not ABORT)
    {
        pkt.throttle = (int16_t) (throttle * QUAT_SCALE);
        pkt.turn = (int16_t) (turn * QUAT_SCALE);
        // debug
        //omegaDes = 100;
        pkt.omegaRdes = (int16_t) (omegaDes * GAINS_SCALE);
        if (status == WHEEL_SPEED)
        {
            pkt.CLwheel = 1;
        }
        else
        {
            pkt.CLwheel = 0;
        }

        // carState.r = state.rate.getZ();
        double yaw = tf::getYaw(state.att);

        double Vx = state.vel.getX() * cos(yaw) + state.vel.getY() * sin(yaw);
        double Vy = -state.vel.getX() * sin(yaw) + state.vel.getY() * cos(yaw);

        // Vx = 0.34; Vy = -0.54;

        // std::cout << "Vx: " << Vx << ", " << Vy << std::endl;
        pkt.Vx = (int16_t) (Vx * ROT_SCALE);
        pkt.Vy = (int16_t) (Vy * ROT_SCALE);
        // pkt.Vx = (float) Vx;
        // pkt.Vy = (float) Vy;
        pkt.status = (uint8_t) status;

//      std::cout << (double) pkt.omegaRdes / GAINS_SCALE << std::endl;
        /*std::ostringstream msg;
         msg.setf(std::ios::fixed);                                     // give all the doubles the same precision
         msg.setf(std::ios::showpos);                           // show +/- signs always
         msg  << std::setprecision(4) << std::endl;     // set precision to 4 decimal places
         msg << "p: " << pkt.p_cmd << " q: " << pkt.q_cmd << " r: " << pkt.r_cmd << std::endl;
         msg << "qo: " << pkt.qo_cmd  << " qx: " << pkt.qx_cmd  << " qy: " << pkt.qy_cmd  << " qz: " << pkt.qz_cmd << std::endl;
         msg << "qo: " << pkt.qo_meas  << " qx: " << pkt.qx_meas  << " qy: " << pkt.qy_meas  << " qz: " << pkt.qz_meas << std::endl;
         msg << "throttle: " << (double)pkt.throttle << " pitch: " << (double)pkt.pitch << " attcmd: " << (double)pkt.AttCmd << std::endl << std::endl;
         ROS_INFO_THROTTLE(0.5,msg.str().c_str()); // Print at 1/0.5 Hz = 2 Hz*/
    }
    else
    {
        ROS_WARN_STREAM("SHUTTING DOWN");
        ABORT_cnt++;
        if (ABORT_cnt > 0)
            ros::shutdown();
        pkt.throttle = 0;
        pkt.turn = 0;
        pkt.omegaRdes = 0;
        pkt.CLwheel = 0;
    }
    SendPacket(PACKETID_CARCMD, sizeof(tCarCmdPacket), (uint8_t *) &pkt);
}

void RCCar::sendGains()
{

    tGainsPacket pkt;
    pkt.Kp = (int16_t) (innerGains.kp * QUAT_SCALE);
    pkt.Ki = (int16_t) (innerGains.ki * QUAT_SCALE);

    pkt.fitA = (int16_t) (innerGains.fitA * QUAT_SCALE);
    pkt.fitB = (int16_t) (innerGains.fitB * QUAT_SCALE);
    pkt.fitC = (int16_t) (innerGains.fitC * QUAT_SCALE);
    pkt.fitD = (int16_t) (innerGains.fitD * QUAT_SCALE);
    pkt.ws_alpha = (uint16_t) (innerGains.ws_alpha * QUAT_SCALE);
    pkt.r_alpha = (uint16_t) (innerGains.r_alpha * QUAT_SCALE);
    pkt.maxOmega = (int16_t) innerGains.maxOmega;
    pkt.minOmega = (int16_t) innerGains.minOmega;
    pkt.fitType = (uint8_t) innerGains.fitType;
    pkt.throttleDir = (int8_t) innerGains.throttleDir;
    pkt.turnDir = (int8_t) innerGains.turnDir;

    pkt.max_turn = (float) gains.maxTurn;
    pkt.min_turn = (float) gains.minTurn;
    pkt.turn_bias = (float) gains.turnBias;
    pkt.min_throttle = (float) min_throttle;
    pkt.drift_throttle = (float) drift_throttle;

    pkt.lowBatt = (uint16_t) (innerGains.lowBatt * 1000); // convert to millivolts
    pkt.stream_data = (uint8_t) flags.streamData;

    SendPacket(PACKETID_GAINS, sizeof(tGainsPacket), (uint8_t *) &pkt);

}

void RCCar::sendGPBeta()
{

    tGPParamsPacket pkt;
    int e = 0;
    pkt.H = (int16_t) H;
    pkt.controller_counter = (int16_t) rollout_counter;
    pkt.AttCmd = (uint8_t) status;

    for (int i = 0; i < 50; i++)
    {
        pkt.beta[i] = (float) beta.at(e)[i];
    }
    for (int i = 0; i < 3; i++)
    {
        pkt.iL2[i] = (float) iL2.at(e)[i];
    }

    std::cout << "sending counter: " << rollout_counter << std::endl;
    std::cout << "sending status: " << status << std::endl;

    SendPacket(PACKETID_GP_BETA, sizeof(tGPParamsPacket), (uint8_t *) &pkt);

}

void RCCar::sendGPInput(int seq_num)
{

    tGPInputPacket pkt;
    int e = 0;
    int num_send = 15;
    if (seq_num == 3)
        num_send = 5;

    int i_start = seq_num * 15;
    int i_end = 15 * seq_num + num_send;

    pkt.seq_num = uint8_t(seq_num);
    for (int i = i_start; i < i_end; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            pkt.input[i - i_start][j] = (float) gp_input.at(e)(i, j);
        }
    }

    SendPacket(PACKETID_GP_INPUT, sizeof(tGPInputPacket), (uint8_t *) &pkt);

}

int16_t volt_tmp;
void RCCar::parsePacket(uint8_t ID, uint8_t length, uint8_t * data)
{
    double yaw = 0;
    switch (ID)
    {
    case PACKETID_VOLTAGE:
        // voltage packet
        memcpy(&volt_tmp, data, sizeof(tVoltagePacket));
        voltage = (double) volt_tmp / 1000.0;
        break;
        /*
         case PACKETID_WHEELSPEED:
         // wheel speed packet
         memcpy(&wheelSpeedPacket, data, sizeof(tWheelSpeedPacket));
         carState.omegaR = ((double) wheelSpeedPacket.omegaR) / GAINS_SCALE;
         carState.throttle = ((double) wheelSpeedPacket.throttle) / QUAT_SCALE;

         //      fill in rest of carState variables
         carState.pose.position.x = state.pos.getX();
         carState.pose.position.y = state.pos.getY();
         carState.pose.position.z = state.pos.getZ();
         tf::quaternionTFToMsg(state.att, carState.pose.orientation);
         carState.omegaF = carState.omegaR; // clearly an approximation, but probably fairly accurate
         // since the car is 4-wheel drive
         carState.r = state.rate.getZ();
         yaw = tf::getYaw(state.att);
         carState.Vx = state.vel.getX() * cos(yaw) + state.vel.getY() * sin(yaw);
         carState.Vy = state.vel.getX() * sin(yaw) - //carState.omegaR * 0.0606/2.0;//
         state.vel.getY() * cos(yaw);

         carState.header.stamp = ros::Time::now();
         statePub.publish(carState);

         break;*/

    case PACKETID_WHEELSPEED2:
        // wheel speed packet
        memcpy(&wheelSpeed2Packet, data, sizeof(tWheelSpeed2Packet));
        carState.omegaF = ((double) wheelSpeed2Packet.omegaF) / ROT_SCALE;
        carState.omegaR = ((double) wheelSpeed2Packet.omegaR) / ROT_SCALE;
        carState.throttle = 100
                * ((double) wheelSpeed2Packet.throttle)/ QUAT_SCALE;
        carState.turn = ((double) wheelSpeed2Packet.turn) / QUAT_SCALE;
        carState.Vx = ((double) wheelSpeed2Packet.Vx) / ROT_SCALE;
        carState.Vy = ((double) wheelSpeed2Packet.Vy) / ROT_SCALE;
        carState.r = ((double) wheelSpeed2Packet.r) / ROT_SCALE;
        //wheelSpeed2Packet.counter = (int) wheelSpeed2Packet.counter;

//      fill in rest of carState variables
        carState.pose.position.x = state.pos.getX();
        carState.pose.position.y = state.pos.getY();
        carState.pose.position.z = state.pos.getZ();
        tf::quaternionTFToMsg(state.att, carState.pose.orientation);

        // carState.r = state.rate.getZ();
        yaw = tf::getYaw(state.att);
        // carState.Vx = state.vel.getX() * cos(yaw) +
        //     state.vel.getY() * sin(yaw);
        // carState.Vy = -state.vel.getX() * sin(yaw) +
        //                 state.vel.getY() * cos(yaw);

        // save the data
        if (run_rollout_onboard)
        {
            if (wheelSpeed2Packet.counter < H
                    and wheelSpeed2Packet.counter >= 0)
            {
                x(wheelSpeed2Packet.counter, 0) = carState.Vx;
                x(wheelSpeed2Packet.counter, 1) = carState.Vy;
                x(wheelSpeed2Packet.counter, 2) = carState.r;
                // x(wheelSpeed2Packet.counter, 3) = carState.omegaR;
                x(wheelSpeed2Packet.counter, 3) = carState.turn;
            }
            if (wheelSpeed2Packet.counter > 0
                    and wheelSpeed2Packet.counter < H + 1)
            {
                y(wheelSpeed2Packet.counter - 1, 0) = carState.Vx;
                y(wheelSpeed2Packet.counter - 1, 1) = carState.Vy;
                y(wheelSpeed2Packet.counter - 1, 2) = carState.r;
                // y(wheelSpeed2Packet.counter-1, 3) = carState.omegaR;
            }

            if (wheelSpeed2Packet.counter >= H + 1)
            {
                run_rollout_onboard = false;
            }

            //std::cout << "Received counter: " << wheelSpeed2Packet.counter
            //        << ", carState.r: " << carState.r << std::endl;
        }

        carState.header.stamp = ros::Time::now();
        if (statePub)
            statePub.publish(carState);

        break;

//    case PACKETID_AHRS:
//      // AHRS data packetSendPacket
//      memcpy(&ahrs, data, sizeof(tAHRSpacket));
//      healthData.rate.x = ((double)ahrs.p) / ROT_SCALE;
//      healthData.rate.y = ((double)ahrs.q) / ROT_SCALE;
//      healthData.rate.z = ((double)ahrs.r) / ROT_SCALE;
//      healthData.att.w = ((double)ahrs.qo_est) / QUAT_SCALE;
//      healthData.att.x = ((double)ahrs.qx_est) / QUAT_SCALE;
//      healthData.att.y = ((double)ahrs.qy_est) / QUAT_SCALE;
//      healthData.att.z = ((double)ahrs.qz_est) / QUAT_SCALE;
//      healthData.att_meas.w = ((double)ahrs.qo_meas) / QUAT_SCALE;
//      healthData.att_meas.x = ((double)ahrs.qx_meas) / QUAT_SCALE;
//      healthData.att_meas.y = ((double)ahrs.qy_meas) / QUAT_SCALE;
//      healthData.att_meas.z = ((double)ahrs.qz_meas) / QUAT_SCALE;
//
//      break;
//
//    case PACKETID_HEALTH:
//      // AHRS data packetSendPacket
//      memcpy(&healthPacket, data, sizeof(tHealthPacket)); // todo: implement proper scaling of current and temp values
//      healthData.current[0] = (double)healthPacket.current1;
//      healthData.current[1] = (double)healthPacket.current2;
//      healthData.current[2] = (double)healthPacket.current3;
//      healthData.current[3] = (double)healthPacket.current4;
//      healthData.temperature[0] = (double)healthPacket.temp1;
//      healthData.temperature[1] = (double)healthPacket.temp2;
//      healthData.temperature[2] = (double)healthPacket.temp3;
//      healthData.temperature[3] = (double)healthPacket.temp4;
//
//      break;
//
//    case PACKETID_SENSORS:
//      memcpy(&sensorsPacket, data, sizeof(tSensorsPacket));
//      sensors.gyro.x = (double)sensorsPacket.gyroX;
//      sensors.gyro.y = (double)sensorsPacket.gyroY;
//      sensors.gyro.z = (double)sensorsPacket.gyroZ;
//      sensors.accel.x = (double)sensorsPacket.accelX;
//      sensors.accel.y = (double)sensorsPacket.accelY;
//      sensors.accel.z = (double)sensorsPacket.accelZ;
//      sensors.mag.x = (double)sensorsPacket.magX;
//      sensors.mag.y = (double)sensorsPacket.magY;
//      sensors.mag.z = (double)sensorsPacket.magZ;
//      sensors.sonar = (double)sensorsPacket.sonarZ;
//      sensors.sonar /= 1000; // into meters
//      sensors.pressure = (double)sensorsPacket.pressure;
//
//      /************ Sensor Data **********/
//      static int count = 0;
//      count++;
//      sensors.header.stamp = ros::Time::now();
//      sensors.header.frame_id = name;
//      sensors.true_pose.position.x = state.pos.getX();
//      sensors.true_pose.position.y = state.pos.getY();
//      sensors.true_pose.position.z = state.pos.getZ();
//      tf::quaternionTFToMsg(state.att, sensors.true_pose.orientation);
//      tf::vector3TFToMsg(state.vel, sensors.true_vel.linear);
//      tf::vector3TFToMsg(state.rate, sensors.true_vel.angular);
//      if (count > 1000)
//        sensorPub.publish(sensors);
//      break;
//
//    case PACKETID_ALTITUDE:
//      memcpy(&altitudePacket, data, sizeof(tAltitudePacket));
//      sensors.sonar = (double)altitudePacket.sonarZ;
//      sensors.sonar /= 1000; // into meters
//      sensors.pressure = (double)altitudePacket.pressure;
//      sensors.temperature = (double)altitudePacket.temperature;
//      sensors.accel.x = ((double)altitudePacket.accelX) / 1000;
//      sensors.accel.y = ((double)altitudePacket.accelY) / 1000;
//      sensors.accel.z = ((double)altitudePacket.accelZ) / 1000;
//      break;
//
//    case PACKETID_ADAPTCMD:
//      memcpy(&adaptcmdPacket, data, sizeof(tAdaptCmdPacket));
//      adaptcmd.alpha_cr.x = (double)adaptcmdPacket.cr_x / ADAPT_SCALE;
//      adaptcmd.alpha_cr.y = (double)adaptcmdPacket.cr_y / ADAPT_SCALE;
//      adaptcmd.alpha_cr.z = (double)adaptcmdPacket.cr_z / ADAPT_SCALE;
//      adaptcmd.alpha_pd.x = (double)adaptcmdPacket.pd_x / ADAPT_SCALE;
//      adaptcmd.alpha_pd.y = (double)adaptcmdPacket.pd_y / ADAPT_SCALE;
//      adaptcmd.alpha_pd.z = (double)adaptcmdPacket.pd_z / ADAPT_SCALE;
//      adaptcmd.alpha_ad.x = (double)adaptcmdPacket.ad_x / ADAPT_SCALE;
//      adaptcmd.alpha_ad.y = (double)adaptcmdPacket.ad_y / ADAPT_SCALE;
//      adaptcmd.alpha_ad.z = (double)adaptcmdPacket.ad_z / ADAPT_SCALE;
//      adaptcmd.AttCmd = adaptcmdPacket.AttCmd;
//
//      adaptPub.publish(adaptcmd);
//
//      break;
    default:
        printf("Packet ID not recognized!\n");
        break;
    }

}

void RCCar::SendPacket(uint8_t ID, uint8_t length, uint8_t * data)
{
    // Calculate the checksum
    uint8_t checkSum = length + ID;
    for (int i = 0; i < length; i++)
    {
        checkSum += data[i];
    }
    //checkSum = (uint8_t)(256-checkSum);
    checkSum ^= 0xFF;
    checkSum += 1;

    // Send start sequence, ID, length
    int headersize = 4;
    uint8_t header[headersize];
    header[0] = 0xFF;
    header[1] = 0xFE;
    header[2] = ID;
    header[3] = length;

    // merge data into one array
    uint8_t *packet = (uint8_t *) malloc(
            (headersize + length + 1) * sizeof(uint8_t));

    memcpy(packet, header, headersize * sizeof(uint8_t)); // copy header into packet
    memcpy(packet + headersize, data, length * sizeof(uint8_t));
    memcpy(packet + headersize + length, &checkSum, sizeof(uint8_t));

    udp.udpSend(packet, headersize + length + 1);

}

void *listen(void *param)
{
    RCCar * car = (RCCar *) param;
    uint8_t packetID;
    uint8_t packetLength;
    uint8_t packetData[256];
    int packetCnt = 0;
    uint8_t checksum;

    uint8_t thisByte = 0;
    uint8_t lastByte = 0;

    uint8_t STX1 = 0xFF;
    uint8_t STX2 = 0xFE;

    ReceiveState rxstate = RXSTATE_STXA;

    while (1)
    {
        // COMMUNICATION THROUGH EXTERNAL PORT
        uint8_t bufr[2048];
        int received_bytes = 0;
        received_bytes = car->udp.udpReceive((char*) bufr, sizeof(bufr));

        // Try to get a new message
        for (int i = 0; i < received_bytes; i++)
        {
            uint8_t b = bufr[i];

            switch (rxstate)
            {
            case RXSTATE_STXA:
                if (b == STX1)
                    rxstate = RXSTATE_STXB;
                break;
            case RXSTATE_STXB:
                if (b == STX1)
                    rxstate = RXSTATE_STXB;
                else if (b == STX2)
                    rxstate = RXSTATE_PACKETID;
                else
                    rxstate = RXSTATE_STXA;
                break;
            case RXSTATE_PACKETID:
                packetID = b;
                rxstate = RXSTATE_LEN;
                checksum = b;
                break;
            case RXSTATE_LEN:
                packetLength = b;
                rxstate = RXSTATE_DATA;
                checksum += b;
                packetCnt = 0;
                break;
            case RXSTATE_DATA:
                packetData[packetCnt] = b;
                packetCnt++;
                if (packetCnt == packetLength)
                    rxstate = RXSTATE_CHKSUM;
                checksum += b;
                break;
            case RXSTATE_CHKSUM:
                rxstate = RXSTATE_STXA;
                checksum += b;
                // Parse packet if checksum correct
                if (checksum == 0)
                {
                    car->parsePacket(packetID, packetLength, packetData);
                }
                else
                {
                    ROS_INFO("Bad checksum! \n");
                }
                break;
            default:
                break;

            }
        }
    }
}
