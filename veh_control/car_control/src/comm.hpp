/*
 * comm.hpp
 *
 *  Created on: Oct 24, 2012
 *      Author: mark
 */

#ifndef COMM_HPP_
#define COMM_HPP_

enum ReceiveState{
        RXSTATE_STXA,
        RXSTATE_STXB,
        RXSTATE_PACKETID,
        RXSTATE_LEN,
        RXSTATE_SEQ,
        RXSTATE_DATA,
        RXSTATE_CHKSUM
};

//*********************** COMM STRUCTS ***********************//
#define PACKETID_SENSORCAL 0x01
typedef struct sSensorCalPacket
{
  int16_t gyroScale;
  int16_t accelScale;
  int16_t K_Att_Filter;
}__attribute__((__packed__)) tSensorCalPacket;

#define PACKETID_AHRS 0x02
typedef struct __attribute__ ((packed)) sAHRSpacket
{
  int16_t p;
  int16_t q;
  int16_t r;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t qo_meas;
  int16_t qx_meas;
  int16_t qy_meas;
  int16_t qz_meas;
} tAHRSpacket;

#define PACKETID_CMD 0x03
typedef struct __attribute__ ((packed)) sCmdPacket
{
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  int16_t qo_meas;
  int16_t qx_meas;
  int16_t qy_meas;
  int16_t qz_meas;
  uint8_t throttle;
  int8_t pitch;
  uint8_t AttCmd;
} tCmdPacket;

#define PACKETID_GAINS 0x04
typedef struct __attribute__((packed)) sGainsPacket {
    int16_t Kp;
    int16_t Ki;

    int16_t fitA;
    int16_t fitB;
    int16_t fitC;
    int16_t fitD;
    uint16_t ws_alpha;
    uint16_t r_alpha;

    int8_t throttleDir;
    int8_t turnDir;
    int16_t maxOmega;
    int16_t minOmega;
    uint8_t fitType;

    float max_turn;
    float min_turn;
    float turn_bias;
    float min_throttle;
    float drift_throttle;

    uint16_t lowBatt;
    uint8_t stream_data;
}
tGainsPacket;

#define PACKETID_VOLTAGE 0x05
typedef struct __attribute__ ((packed)) sVoltagePacket
{
  int16_t voltage;
} tVoltagePacket;

#define PACKETID_LOG 0x06
typedef struct __attribute__((packed)) sLogPacket
{
  int16_t p_est;
  int16_t q_est;
  int16_t r_est;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  uint8_t throttle;
  int8_t collective;
} tLogPacket;

#define PACKETID_HEALTH 0x07
typedef struct __attribute__ ((packed)) sHealthPacket
{
  int8_t current1;
  int8_t temp1;
  int8_t current2;
  int8_t temp2;
  int8_t current3;
  int8_t temp3;
  int8_t current4;
  int8_t temp4;
} tHealthPacket;

#define PACKETID_SENSORS 0x08
typedef struct __attribute__((packed)) sSensorsPacket
{
  int16_t gyroX;
  int16_t gyroY;
  int16_t gyroZ;
  int16_t accelX;
  int16_t accelY;
  int16_t accelZ;
  int16_t magX;
  int16_t magY;
  int16_t magZ;
  int16_t sonarZ;
  int32_t pressure;
} tSensorsPacket;

#define PACKETID_ALTITUDE 0x09
typedef struct __attribute__((packed)) sAltitudePacket
{
  int16_t sonarZ;
  int32_t pressure;
  int32_t temperature;
  int16_t accelX;
  int16_t accelY;
  int16_t accelZ;
} tAltitudePacket;

#define PACKETID_CARCMD 0x0A
typedef struct __attribute__((packed)) sCarCmdPacket {
    int16_t throttle;
    int16_t turn;
    int16_t omegaRdes;
    int16_t Vx;
    int16_t Vy;
    // float Vx;
    // float Vy;
    uint8_t CLwheel;
    uint8_t status;
}
tCarCmdPacket;

#define PACKETID_WHEELSPEED 0x0B

typedef struct __attribute__((packed)) sWheelSpeedPacket {
        int16_t omegaR;         // measured wheel speed
        int16_t throttle;       // actual set throttle
}
tWheelSpeedPacket;

#define PACKETID_WHEELSPEED2 0x0C

typedef struct __attribute__((packed)) sWheelSpeed2Packet {
        uint16_t omegaF;                // measured wheel speed
        uint16_t omegaR;                // measured wheel speed
        int16_t throttle;       // actual set throttle
        int16_t turn;       // actual set turn
        int16_t Vx;
        int16_t Vy;
        // float Vx;
        // float Vy;
        int16_t r;
        int16_t counter;
}
tWheelSpeed2Packet;

#define PACKETID_GP_BETA 0x0D

typedef struct __attribute__((packed)) sGPParamsPacket {
    float beta[50];
    float iL2[3];
    int16_t H;
    int16_t controller_counter;
    uint8_t AttCmd;
}
tGPParamsPacket;

#define PACKETID_GP_INPUT 0x0E

typedef struct __attribute__((packed)) sGPInputPacket {
    uint8_t seq_num;
    float input[15][3];
}
tGPInputPacket;

#endif /* COMM_HPP_ */
