/*
 * defines.h
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef DEFINES_H_
#define DEFINES_H_

//Defines
#define controlDT                       0.02    // 1/50 Control loop rate [seconds]
// Safety values
#define MAX_VICON_WAIT_TIME             0.5 // wait time in seconds for new vicon data
#define MAX_ACCEL_XY                    2.0
#define MAX_ACCEL_Z                     0.8
#define MAX_VELOCITY            3.0 // m/s

//#define MAX_WHEEL_SPEED               450 // radians/sec
//#define MIN_WHEEL_SPEED               -300
//#define MAX_TURN_ANGLE                20*PI/180.0
//#define MIN_TURN_ANGLE                -MAX_TURN_ANGLE

#define SCREEN_PRINT_RATE       0.1 // Print at 1/0.5 Hz = 2 Hz

//## Status
#define NOT_DRIVING                     0
#define MANUAL_MODE                     1
#define WHEEL_SPEED                     3
#define RESET                           4
#define GP_CONTROL                      5

// ROOM Bounds
#define XMIN  -2.25
#define XMAX   2.25
#define YMIN  -7.0
#define YMAX   3.0
#define ZMIN   0.5
#define ZMAX   2.0


//## communications defines -- must be the same values on embedded code
#define QUAT_SCALE              10000.0         // value to scale quaternions by for sending as int
#define ROT_SCALE               100.0           // value to scale rotations (p,q,r) by for sending as int
#define GAINS_SCALE             10.0            // value to scale gains by for sending as int

#endif /* DEFINES_H_ */
