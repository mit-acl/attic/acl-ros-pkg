/*
 * getParameters.cpp
 *
 *  Created on: Jun 1, 2012
 *      Author: mark
 */

#include "car.hpp"

// Get parameters from ROS Param Server
void RCCar::getParams()
{
  // Flags
  flags.streamData = raven::param_bool("streamData", true);
  flags.screenPrint = raven::param_bool("screenPrint", true);

  // OuterLoop Parameters
  gains.turnBias = raven::param_double("turnBias", true);
  gains.maxOmega = raven::param_double("maxOmega", true);
  gains.minOmega = raven::param_double("minOmega", true);
  gains.maxTurn = raven::param_double("maxTurn", true);
  gains.minTurn = raven::param_double("minTurn", true);

  // InnerLoop Parameters
  innerGains.lowBatt = raven::param_double("lowBatt", true);
  innerGains.kp = raven::param_double("kp", true);
  innerGains.ki = raven::param_double("ki", true);
  innerGains.fitA = raven::param_double("fitA", true);
  innerGains.fitB = raven::param_double("fitB", true);
  innerGains.fitC = raven::param_double("fitC", true);
  innerGains.fitD = raven::param_double("fitD", true);
  innerGains.ws_alpha = raven::param_double("ws_alpha", true);
  innerGains.r_alpha = raven::param_double("r_alpha", true);
  innerGains.throttleDir = raven::param_double("throttleDir", true);
  innerGains.turnDir = raven::param_double("turnDir", true);
  innerGains.maxOmega = raven::param_double("maxOmega", true);
  innerGains.minOmega = raven::param_double("minOmega", true);
  innerGains.fitType = raven::param_double("fitType", true);

  // on-board calibration data
  calData.gyroScale = raven::param_double("gyroScale", true);
  calData.accelScale = raven::param_double("accelScale", true);
  calData.K_Att_Filter = raven::param_double("K_Att_Filter", true) * controlDT;

        // UDP port
        ip_address = raven::param_string("ipAddress", true);
        rxport = (int) raven::param_double("rxport", true);
        txport = (int) raven::param_double("txport", true);

        min_throttle = raven::param_double("min_throttle", true);
        drift_throttle = raven::param_double("drift_throttle", true);


  // RCCar parameters
  mass = raven::param_double("mass", true);

}
