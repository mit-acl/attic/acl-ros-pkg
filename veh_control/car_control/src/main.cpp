/*
 * main.cpp
 *
 *  Created on: Mar 26, 2011
 *      Author: mark
 */

// ROS includes
#include "ros/ros.h"

// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "car.hpp"
#include "defines.h"

// Global vars
bool ABORT = false;
int ABORT_cnt = -20;

// Prototypes
void controlC(int sig);

int main(int argc, char **argv)
{

    // NOTE: Namespace should be remapped to vehicle name from launch file
    // Initialize ROS and node n
    ros::init(argc, argv, "cntrl", ros::init_options::NoSigintHandler);
    ros::NodeHandle n;

    // Check for proper renaming of node
    if (not ros::this_node::getNamespace().compare("/"))
    {
        ROS_ERROR(
                "Error :: You should be using a launch file to specify the node namespace!\n");
        return (-1);
    }

    //## Welcome screen
    ROS_INFO("\n\nStarting ROS_RAVEN_code for %s...\n\n\n",
            ros::this_node::getNamespace().c_str());

    // Initialize vehicle
    RCCar car;

    std::string name = ros::this_node::getNamespace();
    ros::Subscriber sub_car_state;
    if (*name.rbegin() == 's') //For C++03
    {
        car.sim = true;
        ROS_INFO_STREAM(
                "This is a simulated vehicle -- simulating hardware handshake");

        sub_car_state = n.subscribe("state", 1, &RCCar::stateCallback, &car);
    }

    // initialize listener callbacks that pickup the vicon data
    ros::Subscriber sub_pose = n.subscribe("pose", 1, &RCCar::poseCallback,
            &car);
    ros::Subscriber sub_vel = n.subscribe("vel", 1, &RCCar::velCallback, &car);
    ros::Subscriber sub_acc = n.subscribe("accel", 1, &RCCar::accelCallback,
            &car);

    ros::Subscriber sub_tpose = n.subscribe("/RC04/pose", 1,
            &RCCar::poseTargetCallback, &car);
    ros::Subscriber sub_tvel = n.subscribe("/RC04/vel", 1,
            &RCCar::velTargetCallback, &car);

    // initialize listener callbacks for picking up goal data
    ros::Subscriber sub_cmd = n.subscribe("carCmd", 1, &RCCar::cmdCallback,
            &car);

    ros::Subscriber sub_joy = n.subscribe("/joy", 1, &RCCar::joyCallBack, &car);

    // initialize main controller loop
    car.controllerTimer = n.createTimer(ros::Duration(controlDT),
            &RCCar::runController, &car);

    // Set up ros publisher
    car.statePub = n.advertise<acl_msgs::CarState>("state", 0);
    car.simCmdPub = n.advertise<std_msgs::Float64MultiArray>("carsimCmd", 0);

    // set up service
    ros::ServiceServer serv = n.advertiseService("run_car_rollout",
            &RCCar::rollout, &car);
    ros::ServiceServer serv2 = n.advertiseService("run_car_rollout_onboard",
            &RCCar::rollout_onboard, &car);

    //## Setup the CTRL-C trap
    signal(SIGINT, controlC);

    sleep(0.5);

    ros::AsyncSpinner spinner(8);
    spinner.start();
    ros::waitForShutdown();

    sleep(0.5);

    //car.writeDataLog2File();

    return 0;
}

//## Custom Control-C handler
void controlC(int sig)
{
    ABORT = true; // ros::shutdown called in RCCar::sendCmd() function
}
