/*!
 * \file state_track_server.cpp
 *
 * Implements the state track service routine in c++
 *
 * Created on: Jan 22, 2014
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#include "state_track_server.h"

StateTrackServer::StateTrackServer()
{
	// set car params and gains
	// TODO: move this elsewhere
	g.k = 5.0;
	g.k_soft = 1.0;
	g.k_psi = 0.05;
	g.kd = 0.4 * 0.035;
	g.kd_steer = 0.2;
	g.kp_v = 2.0;
	g.ki_v = 0.05;
	g.verr_int = 0.0;

	cp.mass = 0.906;
	cp.lF = 0.1;
	cp.lR = 0.107;
	cp.Cy = 11.4;
	cp.maxSteeringAngle = 20 * PI / 180.0;
	cp.minV = 2.0;
	cp.radius = 0.0606 / 2.0;

	goal.v = 3.5;  // start high so we don't have the rate limiter winding up

	std::vector<std::vector<double> > path;
	std::vector<double> point(4, 0);
	path.push_back(point);

	//acl::PurePursuit pp_tmp(path, 1.3, 1.3 / 3.0);
	//pp = &pp_tmp;
	pp = new acl::PurePursuit(path, 1.3, 1.3 / 3.0);

	// Initialize variables
	delta_prev = 0.0;
	slow_mo = 0.0;

	ros::NodeHandle nh;
	pub_marker = nh.advertise<visualization_msgs::MarkerArray>("/RAVEN_world",
			1);
	pub_state = nh.advertise<acl_msgs::CarState>("state",1);
        pub_pose = nh.advertise<geometry_msgs::PoseStamped>("pose",1);
	marker.type = visualization_msgs::Marker::ARROW;
	marker.action = visualization_msgs::Marker::ADD;
	pathmarker1.type = visualization_msgs::Marker::LINE_STRIP;
	pathmarker1.action = visualization_msgs::Marker::ADD;
	pathmarker2.type = visualization_msgs::Marker::LINE_STRIP;
	pathmarker2.action = visualization_msgs::Marker::ADD;
	initMarker(marker);
	initMarker(pathmarker1);
	initMarker(pathmarker2);
	pathmarker1.id +=1;
	pathmarker2.id+=2;
	pathmarker1.scale.x = 0.035;
	pathmarker1.color.r = 0;
	pathmarker1.color.g = 1;
	pathmarker1.color.a = 1;
	pathmarker2.scale.x = 0.035;
	pathmarker2.color.a = 0.2;
	pathmarker2.color.r = 0.8;
	pathmarker2.color.g = 0.8;
	pathmarker2.color.b = 0.8;


}

StateTrackServer::~StateTrackServer()
{
	delete pp;
	// TODO Auto-generated destructor stub
}

bool StateTrackServer::singleSegment(acl_msgs::SingleSegment::Request &req,
		acl_msgs::SingleSegment::Response &res)
{

//	res.reward = 10;
//	res.s_prime = req.s;
//	return true;

	// Math model -- return idealized time
	if (req.d == 0)
	{
		std::vector<double> x, y;
		double dist;
		createPathSegment(req.s.segment, req.r, req.s.prev_radius, x, y, dist);
		double t = dist / req.v;
        //std::cout << "Dist: " << dist << ", v: " << req.v << ", t: " << t << std::endl;
		acl_msgs::SimpleState ns;
		ns.prev_radius = req.r;
		ns.slipping = 0;
		ns.Vx = req.v;
		ns.Vy = -1000.; // Vy and psidot don't make much sense in math model,
		// we'll just copy n and Q over to the unseen values.
		// set so small values here so we can know that are index
		// zero in the discretized version
		ns.psidot = -1000.;
		ns.segment = req.s.segment + 1;
		if (ns.segment > 3)
			ns.segment = 0;

		res.s_prime = ns;
		res.reward = t;

		return true;
	}

	// update path
	updateNextPath(req.s.segment, req.r, req.s.prev_radius, req.v);

	if (req.reset_state)
		current_state = initState(req.r, req.s);
	if (req.use_start_state)
		current_state = useStartState(req.s);

	// initialize variables
	double start_time = 0;
	double sim_time = 0;
	int iterCnt = 0;

	// get slowing down parameter if it is set
	ros::NodeHandle nh;
	if (nh.hasParam("slow_mo"))
		nh.getParam("slow_mo", slow_mo);

	while ((sim_time - start_time) < MAXSEGTIME)
	{
		bool passedYthresh = false;
		double x = current_state.pose.position.x;
		double y = current_state.pose.position.y;
		if (req.s.segment == 0)
		{
			if (y < (Y2 + req.r) and x < XCENTER)
				passedYthresh = true;
		}
		else if (req.s.segment == 1)
		{
			if (y > (Y2 + req.r) and x > XCENTER)
				passedYthresh = true;
		}
		else if (req.s.segment == 2)
		{
			if (y > (Y1 - req.r) and x > XCENTER)
				passedYthresh = true;
		}
		else if (req.s.segment == 3)
		{
			if (y < (Y1 - req.r) and x < XCENTER)
				passedYthresh = true;
		}

		// terminating conditions
		if (passedYthresh)
		{
			res.s_prime = getSimpleState(current_state, req.s, req.r, req.v);
			res.reward = sim_time - start_time;
			std::cout << std::endl << std::endl << g.verr_int << std::endl;
			return true;
		}

		// update controller ate CNTRL_RATE
		if ((iterCnt % int(double(CNTRL_RATE / (DT)))) == 0)
		{
			if (findGoal(goal))
			{
				std_msgs::Float64MultiArray cmd;
				if (goal.reset_v_int)
					g.verr_int = 0.0;
				else
					cmd = calcCommands(goal, slipping, 3);
				omega_des = cmd.data[1];
				turn_des = cmd.data[2];
				//std::cout << "turn_des: " << turn_des << std::endl;
				//std::cout << "omega_des: " << omega_des << std::endl;
			}
		}

		// Call the dynamics engine
		current_state = runSimStep(current_state, omega_des, turn_des, DT);

		double percent_slip = 0.2;
		bool slip1 = fabs(current_state.Vx)
				> fabs(current_state.omegaR * cp.radius) * (1 + percent_slip);
		bool slip2 = fabs(current_state.Vx)
				< fabs(current_state.omegaR * cp.radius) * (1 - percent_slip);
		if (slip1 or slip2)
			slipping = true;
		else
			slipping = false;

		if (checkIfOutofBounds(current_state.pose, true))
		{
			res.s_prime = getOutofBoundsState();
			res.reward = 200000;
			return true;
		}

		//std::cout << "x: " << current_state.pose.position.x << " y: "
		//		<< current_state.pose.position.y << std::endl;

		sim_time += DT;
		iterCnt += 1;
		usleep(slow_mo*1000*1000);

		if (slow_mo != 0)
		{
			geometry_msgs::PoseStamped ps;
			ps.header.stamp = ros::Time::now();
			ps.pose = current_state.pose;
			pub_pose.publish(ps);
                        pub_state.publish(current_state);
		}

	}

	res.s_prime = getOutofBoundsState();
	res.reward = 200000;
	return true;
}

/**
 * Run a single time step of the dynamics
 * @param req Initial state, time step, and action
 * @param res Final state
 * @return True
 */
acl_msgs::CarState StateTrackServer::runSimStep(acl_msgs::CarState startState,
		double omega_des, double turn_des, double dt)
{
	//timeoutTimer.stop();
	//broadcastTimer.start();

	// set initial state
	acl::sCarState s = rosState2state(startState);
	dynamics.setInitialState(s);

	// set actions
	dynamics.setOmegaDelta(omega_des, turn_des);

	// integrate forward dynamics
	dynamics.integrateStep(dt);

	// get new state
	acl_msgs::CarState finalState = state2RosState(dynamics.getState());

	return finalState;
}

//// Get POSE messages from Vicon
//void StateTrackServer::poseCB(const geometry_msgs::PoseStamped& msg) {
//
//	pose = msg;
//	psi = acl::quat2yaw(pose.pose.orientation.w, pose.pose.orientation.x,
//			pose.pose.orientation.y, pose.pose.orientation.z);
//}
//
//// Get TWIST messages from Vicon
//void StateTrackServer::velCB(const geometry_msgs::TwistStamped& msg) {
//
//	twist = msg;
//	v = acl::norm(twist.twist.linear.x, twist.twist.linear.y,
//			twist.twist.linear.z);
//
//}

int StateTrackServer::findGoal(acl_msgs::CarGoal & goal)
{
	double psi = acl::quat2yaw(current_state.pose.orientation.w, current_state.pose.orientation.x,
		current_state.pose.orientation.y, current_state.pose.orientation.z);

// get current position and velocity in vector format
	std::vector<double> p;
	p.push_back(current_state.pose.position.x);
	p.push_back(current_state.pose.position.y);
	p.push_back(current_state.pose.position.z);
	std::vector<double> v;
	v.push_back(current_state.Vx * cos(psi) - current_state.Vy * sin(psi));
	v.push_back(current_state.Vx * sin(psi) + current_state.Vy * cos(psi));
	v.push_back(0.0);

// Calculate pure pursuit control vectors
	std::vector<double> pL1(3, 0);
	double v_cmd, r_cmd, psi_cmd;
	int index_L1, index_close;
	if (not pp->calcL1Control(p, v, psi, pL1, v_cmd, r_cmd, psi_cmd,
			index_close, index_L1))
		return 0; // ERROR

	if (v_cmd != 0)
		v_cmd = acl::saturate(v_cmd, MAXVEL, MINVEL);

// compute percent of path complete based on closest point projection
	percent_complete = 100.0 * index_L1 / pp->getPathLength();

	double err = 0;

	goal.header.stamp = ros::Time::now();
	goal.psi = psi_cmd;
	goal.r = r_cmd;
	goal.v = acl::rateLimit(v_cmd,goal.v,-10000.,2.0, CNTRL_RATE);
	goal.e = err;
	goal.reset_v_int = false;

	// generate marker data for visualatization
	if (slow_mo != 0)
	{
		marker.pose.position.x = pL1[0];
		marker.pose.position.y = pL1[1];
		marker.pose.orientation.w = cos(psi_cmd/2.0);
		marker.pose.orientation.z = sin(psi_cmd/2.0);

		visualization_msgs::MarkerArray ma;
		ma.markers.push_back(marker);
		pub_marker.publish(ma);
	}

	return 1;

}

std_msgs::Float64MultiArray StateTrackServer::calcCommands(
		acl_msgs::CarGoal goal, bool slipping, int status)
{
// current state (controller coming from
// Autonomous Automobile Trajectory Tracking for Off-Road Driving:
// Controller Design, Experimental Validation and Racing
// by Hoffmann, Tomlin, Montemerlo, and Thrun

	double psi = acl::quat2yaw(current_state.pose.orientation.w, current_state.pose.orientation.x,
			current_state.pose.orientation.y, current_state.pose.orientation.z);
	double v = acl::norm(current_state.Vx, current_state.Vy);

	double psi_control = g.k_psi * acl::wrap(psi - goal.psi);
	double psi_ss = cp.mass * v * goal.r / (cp.Cy * (1.0 + cp.lF / cp.lR));
	double cross_term = atan(g.k * goal.e / (g.k_soft + v));

	double delta = psi_control - psi_ss + cross_term
			- g.kd * (goal.r - current_state.r);

// steer damping term (really just a first order filter on the steering
// signal)
	delta = delta + g.kd_steer * (delta_prev - delta);
	delta_prev = delta;

// convert delta from radians to -1 to 1, based on max steer angle of car
	delta = delta / cp.maxSteeringAngle;

// Longitudinal PI controller
	double verr = 0;
	if (goal.v >= cp.minV)
		verr = goal.v - v;
	double dt = CNTRL_RATE;
	if (not slipping)
		g.verr_int += verr * dt;
	double v_cmd = 0;
	if (verr != 0)
		v_cmd = g.kp_v * verr + g.ki_v * g.verr_int;

// convert velocity goal to desired wheel speed
	double omega_des = (goal.v + v_cmd) / cp.radius;

	std_msgs::Float64MultiArray cmd;
	cmd.data.push_back(status);
	cmd.data.push_back(omega_des);
	cmd.data.push_back(delta);

	return cmd;

}

void StateTrackServer::updateNextPath(int segment, double radius,
		double prev_radius, double velocity)
{
// calculate the current segment and the next one
	std::vector<double> x1, y1, x2, y2;
	double dist1, dist2;
	createPathSegment(segment, radius, prev_radius, x1, y1, dist1);
	createPathSegment(segment + 1, radius, radius, x2, y2, dist2);

	std::vector<std::vector<double> > path;
	for (unsigned int i = 0; i < x1.size(); i++)
	{
		std::vector<double> row;
		row.push_back(x1[i]);
		row.push_back(y1[i]);
		row.push_back(0);
		row.push_back(velocity);
		path.push_back(row);
	}

	for (unsigned int i = 0; i < x2.size(); i++)
	{
		std::vector<double> row;
		row.push_back(x2[i]);
		row.push_back(y2[i]);
		row.push_back(0);
		row.push_back(velocity);
		path.push_back(row);
	}

	pp->updateTree(path);

	if (slow_mo != 0)
	{
		pathmarker1.points = createPath(x1,y1);
		pathmarker2.points = createPath(x2,y2);
		pathmarker1.color.g = (velocity - 1.8)/1.9;

		visualization_msgs::MarkerArray ma;
		ma.markers.push_back(pathmarker1);
		ma.markers.push_back(pathmarker2);
		for (int i=0; i< 10; i++)
			pub_marker.publish(ma);
	}

}

std::vector<geometry_msgs::Point> StateTrackServer::createPath(std::vector<double> x,
		std::vector<double >y)
{
	std::vector<geometry_msgs::Point> points;
	for (unsigned int i=0; i<x.size(); i++)
	{
		geometry_msgs::Point p;
		p.x = x[i];
		p.y = y[i];
		p.z = 0;
		points.push_back(p);
	}
	return points;
}

void StateTrackServer::createPathSegment(int segment, double radius,
		double prev_radius, std::vector<double> &x, std::vector<double> &y,
		double &dist)
{

	x.clear();
	y.clear();
	if (segment > 3)
		segment -= 4;
	if (segment == 0)
	{
		double xstart = X1 - prev_radius * TURN_DIR;
		double xend = X2 - radius * TURN_DIR;
		x = linspace(xstart, xend, DISC);

		double ystart = Y1 - prev_radius;
		double yend = Y2 + radius;
		y = linspace(ystart, yend, DISC);
		dist = acl::norm(xstart - xend, ystart - yend);
	}
	else if (segment == 1)
	{
		std::vector<double> theta = linspace(PI / 2.0, PI, DISC);
		for (unsigned int i=0; i < theta.size(); i++)
		{
			x.push_back(X2 - prev_radius * sin(theta[i]) * TURN_DIR);
			y.push_back(Y2 + prev_radius * cos(theta[i]) + prev_radius);
		}

		theta = linspace(PI, PI / 2.0, DISC);
		for (unsigned int i=0; i < theta.size(); i++)
		{
			x.push_back(X2 + radius * sin(theta[i]) * TURN_DIR);
			y.push_back(Y2 + radius * cos(theta[i]) + radius);
		}

		dist = 2 * PI * radius / 4.0 + 2 * PI * prev_radius / 4.0;

	}
	else if (segment == 2)
	{
		double xstart = X2 + prev_radius * TURN_DIR;
		double xend = X1 + radius * TURN_DIR;
		x = linspace(xstart, xend, DISC);

		double ystart = Y2 + prev_radius;
		double yend = Y1 - radius;
		y = linspace(ystart, yend, DISC);
		dist = acl::norm((xstart - xend), (ystart - yend));
	}
	else if (segment == 3)
	{
		std::vector<double> theta = linspace(PI / 2.0, 0.0, DISC);
		for (unsigned int i=0; i < theta.size(); i++)
		{
			x.push_back(X1 + prev_radius * sin(theta[i]) * TURN_DIR);
			y.push_back(Y1 + prev_radius * cos(theta[i]) - prev_radius);
		}

		for (int i=theta.size()-1; i >= 0; i--)
		{
			x.push_back(X1 - radius * sin(theta[i]) * TURN_DIR);
			y.push_back(Y1 + radius * cos(theta[i]) - radius);
		}

		dist = 2 * PI * radius / 4.0 + 2 * PI * prev_radius / 4.0;
	}
	else
	{
		ROS_ERROR_STREAM("Wrong segment number!");
	}

	for (unsigned int i = 0; i < x.size(); i++)
	{
		acl::rotate2D(x[i] - XCENTER, y[i] - YCENTER, OVERALL_ROT, x[i], y[i]);
		x[i] += XCENTER;
		y[i] += YCENTER;
	}

}

// Convert carState to simple state for rmax algorithm
acl_msgs::SimpleState StateTrackServer::getSimpleState(
		acl_msgs::CarState car_state, acl_msgs::SimpleState simple_state,
		double radius, double vel)
{
	acl_msgs::SimpleState ns;
	ns.x = car_state.pose.position.x;
	ns.y = car_state.pose.position.y;
	ns.psi = acl::quat2yaw(car_state.pose.orientation.w,
			car_state.pose.orientation.x, car_state.pose.orientation.y,
			car_state.pose.orientation.z);
	ns.omega = car_state.omegaR;
	ns.Vx = car_state.Vx; //vel;
	ns.Vy = car_state.Vy;
	ns.psidot = car_state.r;
	ns.prev_radius = fabs(car_state.pose.position.x - XCENTER); //radius;
	if (abs(ns.Vx) > fabs(car_state.omegaR * cp.radius) * 1.1) // 10 percent slip min
		ns.slipping = 1;
	else
		ns.slipping = 0;

	ns.segment = simple_state.segment + 1;
	if (ns.segment > 3)
		ns.segment = 0;

	return ns;
}

//Use given starting state
acl_msgs::CarState StateTrackServer::useStartState(
		acl_msgs::SimpleState state)
{
	acl_msgs::CarState s;
	s.pose.position.x = state.x;
	s.pose.position.y = state.y;
	s.pose.position.z = 0.0;
	s.pose.orientation.w = cos(state.psi / 2.0);
	s.pose.orientation.x = s.pose.orientation.y = 0.0;
	s.pose.orientation.z = sin(state.psi / 2.0);
	s.omegaF = state.omega;
	s.omegaR = state.omega;
	s.Vx = state.Vx;
	s.Vy = state.Vy;
	return s;
}

// Initialize state
acl_msgs::CarState StateTrackServer::initState(double radius,
		acl_msgs::SimpleState state)
{
	acl_msgs::CarState s;
	double psi;
	if (state.segment == 0)
	{
		s.pose.position.x = X1 - state.prev_radius * TURN_DIR;
		s.pose.position.y = Y1 - state.prev_radius;
		psi = -PI / 2.0;
	}
	else if (state.segment == 1)
	{
		s.pose.position.x = X2 - state.prev_radius;
		s.pose.position.y = Y2 + state.prev_radius;
		psi = -PI / 2.0;
	}
	else if (state.segment == 2)
	{
		s.pose.position.x = X2 + state.prev_radius * TURN_DIR;
		s.pose.position.y = Y2 + state.prev_radius;
		psi = PI / 2.0;
	}
	else if (state.segment == 3)
	{
		s.pose.position.x = X1 + state.prev_radius;
		s.pose.position.y = Y1 - state.prev_radius;
		psi = PI / 2.0;
	}
	s.pose.position.z = 0.0;
	acl::rotate2D(s.pose.position.x - XCENTER, s.pose.position.y - YCENTER,
			OVERALL_ROT, s.pose.position.x, s.pose.position.y);
	s.pose.position.x += XCENTER;
	s.pose.position.y += YCENTER;
	psi += OVERALL_ROT;
	s.pose.orientation.w = cos(psi / 2.0);
	s.pose.orientation.x = s.pose.orientation.y = 0.0;
	s.pose.orientation.z = sin(psi / 2.0);
	s.Vx = state.Vx;
	s.Vy = state.Vy;
	s.r = state.psidot;
	double v = acl::norm(s.Vx, s.Vy);
	s.omegaR = s.omegaF = v / cp.radius; // clearly an approximation, but hopefully close

	return s;
}

/**
 * Simple helper function for converting acl::sCarState to ros message CarState
 * @param in
 * @return
 */
acl_msgs::CarState StateTrackServer::state2RosState(acl::sCarState in)
{
	acl_msgs::CarState out;
	out.pose.position.x = in.x;
	out.pose.position.y = in.y;
	out.Vx = in.Vx;
	out.Vy = in.Vy;
	out.pose.orientation.w = cos(in.psi / 2.0);
	out.pose.orientation.x = out.pose.orientation.y = 0.0;
	out.pose.orientation.z = sin(in.psi / 2.0);
	out.r = in.dpsi;
	out.omegaF = in.omegaF;
	out.omegaR = in.omegaR;

	return out;
}

/**
 * Simple helper function for converting ros message CarState to acl::sCarState
 * @param in
 * @return
 */
acl::sCarState StateTrackServer::rosState2state(acl_msgs::CarState in)
{
	acl::sCarState out;
	out.x = in.pose.position.x;
	out.y = in.pose.position.y;
	out.Vx = in.Vx;
	out.Vy = in.Vy;
	out.psi = acl::quat2yaw(in.pose.orientation.w, in.pose.orientation.x,
			in.pose.orientation.y, in.pose.orientation.z);
	out.dpsi = in.r;
	out.omegaF = in.omegaF;
	out.omegaR = in.omegaR;

	return out;
}

// Check for out of bounds position of the car
bool StateTrackServer::checkIfOutofBounds(geometry_msgs::Pose pose,
		bool hardware)
{
	double x = pose.position.x;
	double y = pose.position.y;

	double xp, xm, yp, ym;
	acl::rotate2D(XOUTOFBOUNDS2, YOUTOFBOUNDS2, OVERALL_ROT, xp, yp);
	acl::rotate2D(-XOUTOFBOUNDS2, -YOUTOFBOUNDS2, OVERALL_ROT, xm, ym);

	double xmax = std::max(xp, xm) + XCENTER;
	double xmin = std::min(xp, xm) + XCENTER;
	double ymax = std::max(yp, ym) + YCENTER;
	double ymin = std::min(yp, ym) + YCENTER;

//    if hardware:
//        xmax = params.XCENTER + params.XOUTOFBOUNDS2
//        xmin = params.XCENTER - params.XOUTOFBOUNDS2
//        ymax = params.YCENTER + params.YOUTOFBOUNDS2
//        ymin = params.YCENTER - params.YOUTOFBOUNDS2
//    else:
//        xmax = params.XCENTER + params.XOUTOFBOUNDS1
//        xmin = params.XCENTER - params.XOUTOFBOUNDS1
//        ymax = params.YCENTER + params.YOUTOFBOUNDS1
//        ymin = params.YCENTER - params.YOUTOFBOUNDS1

	if (x > xmax or x < xmin)
		return true;
	if (y > ymax or y < ymin)
		return true;

	return false;
}

// Convert carState to simple state for rmax algorithm
acl_msgs::SimpleState StateTrackServer::getOutofBoundsState(void)
{
	acl_msgs::SimpleState ns;

	ns.Vx = 0;
	ns.Vy = 0;
	ns.psidot = 0;
	ns.prev_radius = 0;
	ns.slipping = 0;
	ns.segment = 0;

	return ns;
}

std::vector<double> StateTrackServer::linspace(double min, double max, int n)
{
	std::vector<double> result;
// vector iterator
	int iterator = 0;

	for (int i = 0; i <= n - 2; i++)
	{
		double temp = min + i * (max - min) / (floor((double) n) - 1);
		result.insert(result.begin() + iterator, temp);
		iterator += 1;
	}

	result.insert(result.begin() + iterator, max);
	return result;
}

void StateTrackServer::initMarker(visualization_msgs::Marker & m)
{
	m.header.frame_id = "/world";
	m.header.stamp = ros::Time::now();
	m.pose.position.x  = 0.0;
	m.pose.position.y = 0.0;
	m.pose.position.z = 0.0;
	m.ns = "RAVEN_car";
	std::string name = ros::this_node::getName();
	m.id = 0;
	// cheap way to get a unique marker id for this node
	for(std::string::size_type i = 0; i < name.size(); ++i) {
		m.id += int(name[i]);
	}

	m.pose.orientation.x = m.pose.orientation.y = m.pose.orientation.z = 0.0;
	m.pose.orientation.w = 1.0;
	m.scale.x = 0.5;
	m.scale.y = 0.03;
	m.scale.z = 0.03;
	m.color.r = 1.0;
	m.color.g = m.color.b = 0.0;
	m.color.a = 1.0; //0.7;
	m.lifetime = ros::Duration(3.0);

}
