/*!
 * \file state_track_server.h
 *
 * Implements the state track service routine in c++
 *
 * Created on: Jan 22, 2014
 * 	   Author: Mark Cutler
 *      Email: markjcutler@gmail.com
 */

#ifndef STATE_TRACK_SERVER_H_
#define STATE_TRACK_SERVER_H_

// Global includes
#include <iostream>
#include <vector>

// ROS includes
#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/Float64MultiArray.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

// Local includes
#include "acl_msgs/CarGoal.h"
#include "acl_msgs/SingleSegment.h"
#include "acl_msgs/SimpleState.h"
#include "acl_msgs/CarState.h"

// ACL includes
#include <acl/control/pure_pursuit.h>
#include <acl/utils.hpp>
#include <acl/dynamics/CarDynamics.hpp>

//TODO: move these elsewhere
#define MAXVEL 4.5
#define MINVEL 0.5
#define CNTRL_RATE 1.0/50.0
#define DT 1.0/200.0
#define TURN_CONTROL 0
#define MANUAL_CONTROL 1
#define YCENTER -2.0
#define YDIST 4.0
#define XCENTER 0.3
#define X1 XCENTER
#define X2 XCENTER
#define Y1 YCENTER+YDIST/2.0
#define Y2 YCENTER-YDIST/2.0
#define OVERALL_ROT 0
#define TURN_DIR 1
#define DISC 100
#define MAX_SEGMENT 4
#define MAXSEGTIME 3.0
// boundary box for out of bounds driving
#define YOUTOFBOUNDS1 6  // 4  // dist from YCENTER
#define XOUTOFBOUNDS1 3  // 1.8  // dist from XCENTER

#define YOUTOFBOUNDS2 4  // dist from YCENTER
#define XOUTOFBOUNDS2 2.2 //1.8  // dist from XCENTER

class Gains {
public:
	double k_psi, k, kd, k_soft, kd_steer, kp_v, ki_v, verr_int;
	Gains() {
		k_psi = k = kd = k_soft = kd_steer = kp_v = ki_v = verr_int = 0.0;
	}
};

class CarParams {
public:
	double radius, mass, Cy, lF, lR, maxSteeringAngle, minV;
	CarParams() {
		radius = mass = Cy = lF = lR = maxSteeringAngle = minV = 0.0;
	}
};

class StateTrackServer {
public:
	StateTrackServer();
	virtual ~StateTrackServer();
	int findGoal(acl_msgs::CarGoal & goal);
	std_msgs::Float64MultiArray calcCommands(acl_msgs::CarGoal goal,
			bool slipping, int status);
	bool singleSegment(acl_msgs::SingleSegment::Request &req,
			acl_msgs::SingleSegment::Response &res);
	void updateNextPath(int segment, double radius, double prev_radius,
			double velocity);
	void createPathSegment(int segment, double radius, double prev_radius,
			std::vector<double> &x, std::vector<double> &y, double &dist);

	void poseCB(const geometry_msgs::PoseStamped& msg);
	void velCB(const geometry_msgs::TwistStamped& msg);

	ros::ServiceServer service;
	acl::PurePursuit *pp; //TODO: make this private
private:
	double delta_prev;
	double omega_des, turn_des;
	acl_msgs::CarGoal goal;
	acl_msgs::CarState current_state;
	visualization_msgs::Marker marker;
	visualization_msgs::Marker pathmarker1;
	visualization_msgs::Marker pathmarker2;

	double percent_complete;
	double slow_mo;
	acl::CarDynamics dynamics;
	bool slipping;

	Gains g;
	CarParams cp;

	ros::Publisher pub_marker;
	ros::Publisher pub_state;
        ros::Publisher pub_pose;

	bool checkIfOutofBounds(geometry_msgs::Pose pose, bool hardware);
	acl_msgs::SimpleState getOutofBoundsState(void);
	acl::sCarState rosState2state(acl_msgs::CarState in);
	acl_msgs::CarState state2RosState(acl::sCarState in);
	acl_msgs::CarState runSimStep(acl_msgs::CarState startState,
			double omega_des, double turn_des, double dt);
	acl_msgs::SimpleState getSimpleState(
		acl_msgs::CarState car_state, acl_msgs::SimpleState simple_state,
		double radius, double vel);
	acl_msgs::CarState useStartState(acl_msgs::SimpleState state);
	acl_msgs::CarState initState(double radius,
			acl_msgs::SimpleState state);
	std::vector<double> linspace(double min, double max, int n);
	void initMarker(visualization_msgs::Marker & m);
	std::vector<geometry_msgs::Point> createPath(std::vector<double> x,
		std::vector<double >y);

};

#endif /* STATE_TRACK_SERVER_H_ */
