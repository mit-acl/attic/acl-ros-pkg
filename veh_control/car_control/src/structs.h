/*
 * structs.h
 *
 *  Created on: Aug 9, 2011
 *      Author: mark
 */

#ifndef STRUCTS_H_
#define STRUCTS_H_

#include <stdint.h>
#include <vector>
#include "tf/transform_datatypes.h"
#include "geometry_msgs/Vector3.h"

//********************* CONTROL STRUCTS **********************//
struct sGoal
{
  double speed;                                         // body-frame forward velocity
  double heading;                                               // heading angle
  double omegaR_des;                            // desired rear wheel speed
  int turnDir;                                          // commanded turn direction (0,1,-1 -> straight,right,left)

  // Constructor
  sGoal()
  {
          speed = heading = omegaR_des = 0.0;
          turnDir = 0;
  }
};

struct sState
{
  tf::Quaternion att;                                   // Attitude
  tf::Vector3 pos, vel, accel, rate;    // Position, velocity, acceleration, rate
  ros::Time time; // last time we received information

  // Constructor
  sState()
  {
    att = att.getIdentity();
    pos.setZero();
    vel.setZero();
    accel.setZero();
    rate.setZero();
    time.fromNSec(0);
  }
};

struct sOuterGains
{
        double turnBias;
        double maxTurn, minTurn;
        double maxOmega, minOmega;

  // Constructor - initialize all values to zero
  sOuterGains()
  {
          turnBias = 0.0;
          maxTurn = minTurn = maxOmega = minOmega = 0.0;
  }
};

struct sInnerGains
{
        double kp,ki;
        double fitA, fitB, fitC, fitD;
    double ws_alpha, r_alpha;
        int throttleDir;
        int turnDir;
        double maxOmega;
        double minOmega;
        int fitType;

  double lowBatt;

  // Constructor:
  sInnerGains()
  {
    lowBatt = kp = ki = fitA = fitB = fitC = fitD = maxOmega = minOmega = 0.0;
    fitType = 0;
    throttleDir = turnDir = 0;
    ws_alpha = r_alpha = 1.0;
  }
};

struct sCalibrationData
{
  double gyroScale;
  double accelScale;
  double K_Att_Filter;

  // Constructor:
  sCalibrationData()
  {
    gyroScale = 0.0;
    accelScale = 0.0;
    K_Att_Filter = 0.0;
  }
  ;
};

// FLAGS ############
struct sFlags
{
  bool streamData;      // stream imu data from autopilot
  bool screenPrint;     // print information to screen

  // Constructor:
  sFlags()
  {
    streamData = false;
    screenPrint = true;
  }
};
//************************************************************//

// File structs
struct sensorFile
{
  std::string name;
  tm date;
  double time;

  bool operator ()(const sensorFile &a, const sensorFile &b) const
  {
    return a.time < b.time;
  }
};

// Integrator
struct Integrator
{
  double value;

  // Constructors:
  Integrator()
  {
    value = 0;
  }
  ;

  // Methods
  void increment(double inc, double dt)
  {
    value += inc * dt;
  }
  void reset()
  {
    value = 0;
  }

};

#endif /* STRUCTS_H_ */
