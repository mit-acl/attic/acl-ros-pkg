/*
 * main.cpp
 *
 *  Created on: Mar 26, 2011
 *      Author: mark
 */

// ROS includes
#include "ros/ros.h"

// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "state_track_server.h"

// Global vars
bool ABORT = false;
int ABORT_cnt = -20;

int main(int argc, char **argv)
{
	// NOTE: Namespace should be remapped to vehicle name from launch file
	// Initialize ROS and node n
	ros::init(argc, argv, "state_track_server_cpp", ros::init_options::NoSigintHandler);

	StateTrackServer sts;

	// debug

//	car_control::SingleSegment::Request req;
//	car_control::SingleSegment::Response res;
//	req.d = 1;
//	req.r = 1.2;
//	req.v = 2.0;
//	req.s.prev_radius = 1.2;
//	req.s.segment = 2;
//	req.s.Vx = 2.0;
//	req.reset_state = true;
//	req.use_start_state = false;
//	sts.singleSegment(req,res);
//	return 0;












	// Check for proper renaming of node
	if (not ros::this_node::getNamespace().compare("/"))
	{
		ROS_ERROR("Error :: You should be using a launch file to specify the node namespace!\n");
		return (-1);
	}

	//## Welcome screen
	ROS_INFO("\n\nStarting ROS_RAVEN_code for %s...\n\n\n", ros::this_node::getNamespace().c_str());

	ros::NodeHandle nh;
	sts.service = nh.advertiseService("single_segment_cpp", &StateTrackServer::singleSegment, &sts);

	// run the code
	ros::spin();

	return 0;
}

