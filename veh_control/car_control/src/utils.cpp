/*
 * utils.cpp
 *
 *  Created on: Jun 1, 2012
 *      Author: mark
 */

#include "car.hpp"

//## From Wikipedia - http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
void RCCar::quaternion2Euler(tf::Quaternion q, double &roll, double &pitch, double &yaw)
{
  double q0, q1, q2, q3;
  q0 = q.getW();
  q1 = q.getX();
  q2 = q.getY();
  q3 = q.getZ();
  roll = atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2)) * 180 / PI;
  pitch = asin(2 * (q0 * q2 - q3 * q1)) * 180 / PI;
  yaw = atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3)) * 180 / PI;
}

//## From Wikipedia - http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
void RCCar::quaternion2Euler(geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw)
{
  double q0, q1, q2, q3;
  q0 = q.w;
  q1 = q.x;
  q2 = q.y;
  q3 = q.z;
  roll = atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2)) * 180 / PI;
  pitch = asin(2 * (q0 * q2 - q3 * q1)) * 180 / PI;
  yaw = atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3)) * 180 / PI;
}

