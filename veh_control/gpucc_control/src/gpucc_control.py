#!/usr/bin/env python
import rospy
import time
import numpy as np
import serial
from geometry_msgs.msg import Vector3
import pici

class GpuccControl(object):
    def __init__(self):
        rospy.init_node('gpucc_control',anonymous=False)
        
        # Read veh name from private parameter
        if not rospy.has_param('~veh_name'):
            rospy.set_param('~veh_name','GP05')
        self.veh_name = rospy.get_param('~veh_name')
        
        # Read serial port from private parameter
        if not rospy.has_param('~port'):
            rospy.set_param('~port','/dev/ttyUSB0')
        self.port = rospy.get_param('~port')

        rospy.loginfo("Connect to %s through %s."%(self.veh_name,self.port))
        
        # Start pici
        self.ser = serial.Serial(self.port,57600,timeout=1.0)
        pici.start(self.ser)
        rospy.loginfo("Started %s" %self.veh_name)

        # Subscribe to controls
        rospy.Subscriber(self.veh_name+'/wheel_speed_control',Vector3,self.callback_wheel_speed)

    def callback_wheel_speed(self,msg):
        vel_l = msg.x
        vel_r = msg.y
        # Limit to [-500,500]
        vel_l = min(vel_l,500) if vel_l > 0 else max(vel_l,-500)
        vel_r = min(vel_r,500) if vel_r > 0 else max(vel_r,-500)
        
        # print msg
        if not rospy.has_param(self.veh_name+"/color"):
            # No grid_color set. Move freely.
            pici.drive_direct(self.ser,int(vel_r),int(vel_l))
        else:
            # Limit to 333 for the speed up to work
            vel_l = min(vel_l,333) if vel_l > 0 else max(vel_l,-333)
            vel_r = min(vel_r,333) if vel_r > 0 else max(vel_r,-333)
            
            # Read the color parameter
            grid_color = rospy.get_param(self.veh_name+"/color")

            if grid_color == "w":
                # Free space
                vel_l_send = vel_l
                vel_r_send = vel_r
            elif grid_color == "k":
                # Almost stop
                vel_l_send = 0.15*vel_l
                vel_r_send = 0.15*vel_r
            elif grid_color == "g":
                # Speed up
                vel_l_send = 1.5*vel_l
                vel_r_send = 1.5*vel_r
            elif grid_color == "r":
                # Half speed
                vel_l_send = 0.5*vel_l
                vel_r_send = 0.5*vel_r
            elif grid_color == "b":
                # No turning
                if ((vel_l + vel_r)/2.0) > 0:
                    vel_l_send = vel_r_send = max(abs(vel_l),abs(vel_r))
                else:
                    vel_l_send = vel_r_send = -max(abs(vel_l),abs(vel_r))
            else:
                vel_l_send = vel_l
                vel_r_send = vel_r

            pici.drive_direct(self.ser,int(vel_r_send),int(vel_l_send))

if __name__ == '__main__':
    gpucc_control = GpuccControl()
    rospy.spin()
