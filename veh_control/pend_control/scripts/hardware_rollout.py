#!/usr/bin/env python
'''
Description: Run a single rollout on the pendulum.

Created on Sept 24, 2014

@author: Mark Cutler
@email: markjcutler@gmail.com
'''
import roslib; roslib.load_manifest('pend_control')
import rospy
import numpy as np
import scipy.io

# custom imports
from pend_control.srv import PendRollout
from pend_control.msg import PendData

from aclpy import utils

class Rollout:

    def __init__(self):
        self.pend_data = PendData()


    def load_params(self):
        params = scipy.io.loadmat('/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/params.mat')
        #w = params['w'][0]
        #b = params['b'][0]
        H = params['H'][0]
        dt = params['dt'][0]
        random = params['random'][0]
        input = params['inputs']
        beta = params['beta']
        iL2 = params['iL2']
        
        print
        print
        print params['beta']
        print
        print
        
        #H = H*5;
        #dt = 1.0/50.0
        
        #self.start_rollout(w, b, H, dt, random)
        self.start_rollout(input, beta, iL2, H, dt, random)

    #def start_rollout(self, w, b, H, rate, random):
    def start_rollout(self, input, beta, iL2, H, rate, random):
        #w = np.array([0.0, 0., 0.0])
        #b = 0.
        #H = 40
        #rate = 1.0/10.0
        #random = True
        
        #x_array, y_array = self.run(w, b, H, rate, random)
        print beta
        input_vector = np.zeros(60)
        cnt = 0
        for i in range(20):
            for j in range(3):
                input_vector[cnt] = input[i,j]
                cnt += 1
                print cnt
                
        x_array, y_array = self.run(input_vector, beta, iL2, 2*H, rate, random)
        
        x = np.zeros((H, 5))
        y = np.zeros((H, 2))
        cntx = 0
        cnty = 0
        for i in range(H):
            for j in range(5):
                x[i, j] = x_array.data[cntx]
                cntx += 1
                if j < 2:
                    y[i, j] = y_array.data[cnty]
                    cnty += 1
                    
        x_full = np.zeros((2*H, 5))
        y_full = np.zeros((2*H, 2))
        cntx = 0
        cnty = 0
        for i in range(2*H):
            for j in range(5):
                x_full[i, j] = x_array.data[cntx]
                cntx += 1
                if j < 2:
                    y_full[i, j] = y_array.data[cnty]
                    cnty += 1
        
        #x_sparse = np.zeros((H/5, 5))
        #y_sparse = np.zeros((H/5, 2))
        #cntx = 0
        #for i in range(0,H,5):
        #    x_sparse[cntx, :] = x[i,:]
        #    y_sparse[cntx, :] = y[i,:]
        #    cntx += 1
                    
        print x
        print y
        scipy.io.savemat('/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/rollout.mat', dict(xx=x, yy=y))
        scipy.io.savemat('/home/mark/gaussian-process/pilcoV0.9/scenarios/pendulum/rollout_full.mat', dict(xx=x_full, yy=y_full))

#-----------------------------------------------------------------------------
# Run rollout
#-----------------------------------------------------------------------------
    #def run(self, w, b, H, rate, random):
    def run(self, input, beta, iL2, H, rate, random):
        serv = 'run_rollout'
        rospy.wait_for_service(serv)
        try:
            st = rospy.ServiceProxy(serv, PendRollout)
            #resp = st(w, b, H, rate, random)
            resp = st(input, beta, iL2, H, rate, random)
            return resp.x, resp.y
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e


if __name__ == "__main__":
   try:
       rospy.init_node('mfrl_car')
       c = Rollout()
       c.load_params()
       #c.start_rollout()

   except rospy.ROSInterruptException:
       pass
