/*
 * comm.cpp
 *
 *  Created on: March 26, 2011
 *      Author: mark
 */

#include "pendulum.hpp"


void Pendulum::sendOffBoardCmd()
{
	tPendPacketOB pkt;
	pkt.m1 = (uint8_t) (m1 * 255.0);
	pkt.m2 = (uint8_t) (m2 * 255.0);
	pkt.AttCmd = (uint8_t) attStatus;
	SendPacket(PACKETID_PEND_OB, sizeof(tPendPacketOB), (uint8_t *) &pkt);
}

void Pendulum::parsePacket(uint8_t ID, uint8_t length, uint8_t * data)
{
	pend_control::PendData data_degree;
	switch (ID)
	{
	case PACKETID_PEND_DATA_OB:
		memcpy(&pendDataPacketOB, data, sizeof(tPendDataPacketOB));
		pend_data.theta = ((double) pendDataPacketOB.theta) / QUAT_SCALE;
		pend_data.theta_meas = ((double) pendDataPacketOB.theta_meas)
				/ QUAT_SCALE;
		pend_data.theta_dot = ((double) pendDataPacketOB.theta_dot) / ROT_SCALE;

		// send control using the new data
		runControllerOffBoard();

		data_degree.theta = pend_data.theta * 180 / PI;
		data_degree.theta_meas = pend_data.theta_meas * 180 / PI;
		data_degree.theta_dot = pend_data.theta_dot * 180 / PI;
		data_degree.theta_des = theta_des * 180 / PI;
		state_pub.publish(data_degree);
		break;
	default:
		ROS_INFO("Packet ID not recognized!\n");
		break;
	}

}

void Pendulum::SendPacket(uint8_t ID, uint8_t length, uint8_t * data)
{
	// Calculate the checksum
	uint8_t checkSum = length + ID;
	for (int i = 0; i < length; i++)
	{
		checkSum += data[i];
	}
	//checkSum = (uint8_t)(256-checkSum);
	checkSum ^= 0xFF;
	checkSum += 1;

	// Send start sequence, ID, length
	uint8_t header[4];
	header[0] = 0xFF;
	header[1] = 0xFE;
	header[2] = ID;
	header[3] = length;
	ser.spSend(header, 4);

	// Send data
	ser.spSend(data, length);

	// Send check sum
	ser.spSend(&checkSum, 1);

}

void *serListen(void *param)
{
	Pendulum * quad = (Pendulum *) param;
	uint8_t packetID;
	uint8_t packetLength;
	uint8_t packetData[256];
	uint8_t checksum;

	uint8_t thisByte = 0;
	uint8_t lastByte = 0;

	uint8_t STX1 = 0xFF;
	uint8_t STX2 = 0xFE;

	int tmp1 = 0;
	while (1)
	{

		// Wait for packet start
		while (!(thisByte == STX2 && lastByte == STX1))
		{
			lastByte = thisByte;
			thisByte = quad->ser.spReceiveSingle();
		}
		thisByte = lastByte = 0;

		// Get packet ID and length
		packetID = quad->ser.spReceiveSingle();
		packetLength = quad->ser.spReceiveSingle();
		checksum = packetLength + packetID;

		// Get packet data
		for (int i = 0; i < packetLength; i++)
		{
			packetData[i] = quad->ser.spReceiveSingle();
			checksum += packetData[i];
		}

		// Get checksum
		checksum += quad->ser.spReceiveSingle();

		// Parse packet if checksum correct
		if (checksum == 0)
		{
			quad->parsePacket(packetID, packetLength, packetData);
		}
		else
		{
			ROS_INFO("Bad checksum! \n");
		}
	}

}

