/*
 * comm.hpp
 *
 *  Created on: Oct 24, 2012
 *      Author: mark
 */

#ifndef COMM_HPP_
#define COMM_HPP_

//## communications defines -- must be the same values on embedded code
#define QUAT_SCALE		10000.0  	// value to scale quaternions by for sending as int
#define ROT_SCALE		100.0		// value to scale rotations (p,q,r) by for sending as int

//*********************** COMM STRUCTS ***********************//
#define PACKETID_SENSORCAL 0x01
typedef struct sSensorCalPacket
{
  int16_t gyroScale;
  int16_t accelScale;
  int16_t K_Att_Filter;
  int16_t kGyroBias;
}__attribute__((__packed__)) tSensorCalPacket;

#define PACKETID_AHRS 0x02
typedef struct __attribute__ ((packed)) sAHRSpacket
{
  int16_t p;
  int16_t q;
  int16_t r;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t qo_meas;
  int16_t qx_meas;
  int16_t qy_meas;
  int16_t qz_meas;
} tAHRSpacket;

#define PACKETID_CMD 0x03
typedef struct __attribute__ ((packed)) sCmdPacket
{
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  int16_t qo_meas;
  int16_t qx_meas;
  int16_t qy_meas;
  int16_t qz_meas;
  uint8_t throttle;
  int8_t pitch;
  uint8_t AttCmd;
} tCmdPacket;

#define PACKETID_GAINS 0x04
typedef struct __attribute__ ((packed)) sGainsPacket
{
  int16_t Kp_roll;
  int16_t Kd_roll;
  int16_t Ki_roll;
  int16_t Kp_pitch;
  int16_t Kd_pitch;
  int16_t Ki_pitch;
  int16_t Kp_yaw;
  int16_t Ki_yaw;
  int16_t Kd_yaw;
  int16_t Servo1_trim;
  int16_t Servo2_trim;
  int16_t Servo3_trim;
  int16_t Servo4_trim;
  int16_t maxang;
  int16_t motorFF;
  uint16_t lowBatt;
  uint8_t stream_data;
} tGainsPacket;

#define PACKETID_VOLTAGE 0x05
typedef struct __attribute__ ((packed)) sVoltagePacket
{
  int16_t voltage;
} tVoltagePacket;

#define PACKETID_LOG 0x06
typedef struct __attribute__((packed)) sLogPacket
{
  int16_t p_est;
  int16_t q_est;
  int16_t r_est;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  uint8_t throttle;
  int8_t collective;
} tLogPacket;

#define PACKETID_HEALTH 0x07
typedef struct __attribute__ ((packed)) sHealthPacket
{
  int8_t current1;
  int8_t temp1;
  int8_t current2;
  int8_t temp2;
  int8_t current3;
  int8_t temp3;
  int8_t current4;
  int8_t temp4;
} tHealthPacket;

#define PACKETID_SENSORS 0x08
typedef struct __attribute__((packed)) sSensorsPacket
{
  int16_t gyroX;
  int16_t gyroY;
  int16_t gyroZ;
  int16_t accelX;
  int16_t accelY;
  int16_t accelZ;
  int16_t magX;
  int16_t magY;
  int16_t magZ;
  int16_t sonarZ;
  int32_t pressure;
} tSensorsPacket;

#define PACKETID_ALTITUDE 0x09
typedef struct __attribute__((packed)) sAltitudePacket
{
  int16_t sonarZ;
  int32_t pressure;
  int32_t temperature;
  int16_t accelX;
  int16_t accelY;
  int16_t accelZ;
} tAltitudePacket;

#define PACKETID_PEND 0x0A

typedef struct __attribute__((packed)) sPendPacket
{
  int16_t w1;
  int16_t w2;
  int16_t w3;
  int16_t b;
  int16_t motor_max;
  int16_t motor_min;
  int16_t H;
  int16_t controller_counter;
  uint8_t AttCmd;
} tPendPacket;

#define PACKETID_PEND_DATA 0x0B

typedef struct __attribute__((packed)) sPendDataPacket
{
  int16_t theta;
  int16_t theta_meas;
  int16_t theta_dot;
  int16_t u;
  int16_t controller_counter;
} tPendDataPacket;

#define PACKETID_PEND_GP 0x0C

typedef struct __attribute__((packed)) sPendGPParamsPacket
{
  float beta[20];
  float iL2[3];
  int16_t motor_max;
  int16_t motor_min;
  int16_t H;
  int16_t controller_counter;
  uint8_t AttCmd;
} tPendGPParamsPacket;

#define PACKETID_PEND_GP_INPUT 0x0D

typedef struct __attribute__((packed)) sPendGPInputPacket
{
  float input[20][3];

} tPendGPInputPacket;

#define PACKETID_PEND_OB 0x0E

typedef struct __attribute__((packed)) sPendPacketOB // off-board control
{
  uint8_t m1;
  uint8_t m2;
  uint8_t AttCmd;
} tPendPacketOB;

#define PACKETID_PEND_DATA_OB 0x0F

typedef struct __attribute__((packed)) sPendDataPacketOB // off-board control
{
  int16_t theta;
  int16_t theta_meas;
  int16_t theta_dot;
} tPendDataPacketOB;

#endif /* COMM_HPP_ */

