/*
 * main.cpp
 *
 *  Created on: Mar 26, 2011
 *      Author: mark
 */


// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "pendulum.hpp"

// Global vars
bool ABORT = false;
int ABORT_cnt = -5;

// Prototypes
void controlC(int sig);

int main(int argc, char **argv)
{

  // Initialize ROS and node n -- generic name is "Quad", overwritten by name remapping from command line
  ros::init(argc, argv, "pend_cntrl", ros::init_options::NoSigintHandler);
  ros::NodeHandle n;

  //## Welcome screen
  ROS_INFO("\n\nStarting ROS_RAVEN_code for %s...\n\n\n", ros::this_node::getNamespace().c_str());

  // Initialize vehicle
  Pendulum pend;

  ros::Subscriber ref_joy = n.subscribe("ref", 1, &Pendulum::referenceCB, &pend);
  ros::Subscriber gains_joy = n.subscribe("param", 1, &Pendulum::gainsCB, &pend);

  // initialize shutoff loop
  ros::Timer shutoff_timer = n.createTimer(ros::Duration(1/10.0), &Pendulum::shutOffTimer, &pend);

  // publish controller and goal commands
  pend.state_pub = n.advertise<pend_control::PendData>("pend_state", 5);

  //## Setup the CTRL-C trap
  signal(SIGINT, controlC);

  // run the code
  ros::AsyncSpinner spinner(4);
  spinner.start();
  ros::waitForShutdown();

  return 0;
}

//## Custom Control-C handler
void controlC(int sig)
{
  ABORT = true;	// ros::shutdown called in Quadrotor::sendCmd() function
}
