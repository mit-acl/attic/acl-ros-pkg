/*
 * pendulum.cpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#include "pendulum.hpp"

extern bool ABORT;
extern int ABORT_cnt;

// Initialize Pendulum Vehicle
Pendulum::Pendulum() {

	attStatus = 0;
	m1 = 0;
	m2 = 0;
	motor_min = 0.25;
	motor_max = 0.99;
	Kp = Ki = Kd = 0.0;
	throttle_nom = 0.0;
	theta_des = 0.0;
	old_time = 0;

	baudRate = 115200;
	serialPort = "/dev/ttyUSB0";

	if (!ser.spInitialize(serialPort.c_str(), baudRate, true))
		ROS_ERROR("Serial port failed to open");

	//## Start a serial listen thread
	pthread_t threads;
	if (pthread_create(&threads, NULL, serListen, (void *) this))
		ROS_ERROR("Serial listen thread failed to start");

}

void Pendulum::shutOffTimer(const ros::TimerEvent& e)
{
	if (ABORT)
    {
	  ROS_WARN_STREAM("SHUTTING DOWN");
      ABORT_cnt++;
      if (ABORT_cnt > 0) {
    	  ros::shutdown();
      }
      m1 = m2 = 0;
      attStatus = 0;
      sendOffBoardCmd();
    }
}

void Pendulum::runControllerOffBoard()
{

	//PID control
	double theta_err = acl::wrap(theta_des - pend_data.theta);
	double time_now = ros::Time::now().toSec();
	double dt = time_now - old_time;
	old_time = time_now;
	if (attStatus)
	{
		I_theta.increment(theta_err, dt);
	}
	if (Ki == 0)
		I_theta.reset();
	double u = Kp*theta_err - Kd*pend_data.theta_dot + Ki*I_theta.value;
	u += throttle_nom;

	// Inverted position
	m2 = u + motor_min;
	m1 = -u + motor_min;
	m2 = acl::saturate(m2,motor_max,motor_min);
	m1 = acl::saturate(m1,motor_max,motor_min);

	if (~ABORT)
		sendOffBoardCmd();

}


void Pendulum::referenceCB(const geometry_msgs::Vector3& ref)
{
	theta_des = ref.x*PI/180.0;
	throttle_nom = ref.y;
	attStatus = (int) ref.z;
	std::cout << "Got new reference: " << attStatus << std::endl;
}

void Pendulum::gainsCB(const geometry_msgs::Vector3& gains)
{
	Kp = gains.x/3.0;
	Ki = gains.y/5.0;
	Kd = gains.z/5.0;
}





