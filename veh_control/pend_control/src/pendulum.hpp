/*
 * pendulum.hpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef PENDULUM_HPP_
#define PENDULUM_HPP_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/Vector3.h"

// custom messages
#include "pend_control/PendData.h"

// ACL shared library
#include "acl/comms/serialPort.hpp"
#include "acl/utils.hpp"

// Local includes
#include "comm.hpp"

//## Serial listen thread
void *serListen(void *param);

// Integrator
struct Integrator
{
  double value;

  // Constructors:
  Integrator()
  {
    value = 0;
  }
  ;

  // Methods
  void increment(double inc, double dt)
  {
    value += inc * dt;
  }
  void reset()
  {
    value = 0;
  }

};


class Pendulum
{
public:
	Pendulum();

	//## ROS message publishing
	ros::Publisher state_pub;

	// message callbacks
	void referenceCB(const geometry_msgs::Vector3& ref);
	void gainsCB(const geometry_msgs::Vector3& gains);

	//## Communication with Pendulum
	acl::SerialPort ser;
	void parsePacket(uint8_t ID, uint8_t length, uint8_t * data);

	//## Communication with Pendulum
	void SendPacket(uint8_t ID, uint8_t length, uint8_t * data);

	void runControllerOffBoard();
	void shutOffTimer(const ros::TimerEvent& e);

private:

	//## Communication with Pendulum
	void sendOffBoardCmd(void);
	std::string serialPort;
	int baudRate;

	//## Comm structs
	tPendDataPacketOB pendDataPacketOB;


	// control information
	pend_control::PendData pend_data;
	double m1, m2, motor_min, motor_max;
	int attStatus;

	// for off-board control
	double theta_des;
	double throttle_nom;
	double Kp, Kd, Ki;
	double old_time;
	Integrator I_theta;

};

#endif /* PENDULUM_HPP_ */
