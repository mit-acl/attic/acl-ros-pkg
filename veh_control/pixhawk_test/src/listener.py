#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import mavros
from mavros.utils import *
from mavros_msgs.msg import State, RadioStatus
from std_msgs.msg import Header, Float64
from geometry_msgs.msg import TwistStamped, PoseStamped, PoseWithCovarianceStamped, \
        Vector3, Vector3Stamped, Point, Quaternion, TransformStamped
from sensor_msgs.msg import Imu
from mavros import command
from mavros import setpoint as SP

def callback(data):
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", data)
    talker(data)

    
def listener():
    rospy.init_node('listener', anonymous=True)
    mavros.set_namespace()
    rospy.Subscriber('/RQ06/pose', PoseStamped, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

def talker(pose):
    pub = rospy.Publisher(mavros.get_topic('mocap','pose'), PoseStamped, queue_size=10)
    #rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    #while not rospy.is_shutdown():
        
    pub.publish(pose)
    #rate.sleep()

if __name__ == '__main__':
    #do_arm(x)

    listener()