#!/usr/bin/python
import rospy
import tf

# ROS messages.
from geometry_msgs.msg import PoseStamped

class QuatToEuler():
    def __init__(self):

        # Create subscribers and publishers.
        sub_odom  = rospy.Subscriber("/RQ06/pose", PoseStamped, self.odom_callback)
        rospy.spin()
        # # Main while loop.
        # while not rospy.is_shutdown():
        #     # Publish new data if we got a new message.
        #     if self.got_new_msg:
        #         pub_euler.publish(self.euler_msg)
        #         self.got_new_msg = False

    # Odometry callback function.
    def odom_callback(self, msg):
        # Convert quaternions to Euler angles.
        # print msg
        (r, p, y) = tf.transformations.euler_from_quaternion([msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w])
        print '----------------'
        print 'R: ', r
        print 'P: ', p 
        print 'Y: ', y

# Main function.    
if __name__ == '__main__':
    # Initialize the node and name it.
    rospy.init_node('quat_to_euler')
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        quat_to_euler = QuatToEuler()
    except rospy.ROSInterruptException: pass