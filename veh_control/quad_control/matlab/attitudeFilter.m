function attitudeFilter( )

%% Set params

fileID = fopen('modifiedSensors1.txt','w');

global param
param.DT = 1/500;
param.G = 9.81;
param.gyro_scale = pi/14.375/180.0*1.0; % Raw gyro -> rad/sec
param.accel_scale = 1/256*param.G; % Raw accel -> m/s^2
param.nAttMeas = 50; % Do attitude measurement every 10*DT seconds
param.K_AttFilter = param.DT*param.nAttMeas*0.1;
param.K_GyroBias = param.DT*param.nAttMeas*0.05;

param.nPosVelMeas = 100; % Do position/velocity measurement every 100*DT seconds

%% IMPORT DATA
file = 'sensorlog1.txt';
%file = 'sensorlog2.txt';
[sensor, truth] = getData(file);



%% State, process noise, and measurement noise definitions
%State:
% x = [ 
%       xpos;ypos;zpos; (m, nav frame)
%       xvel; yvel; zvel; (m/s, nav frame)
%       axbias; aybias; azbias; (m/s^2, accelerometer biases)
%     ]

% Process noise:
% v = [
%       n_ax; n_ay; n_az; (m/s^2, accelerometer noise)
%     ]

% Measurement noise:
% n = [
%       n_xpos; n_ypos; n_zpos; (m, position measurement noise)
%       n_xvel; n_yvel; n_zvel; (m/s, velocity measurement noise)
%     ]

%% Covariance matrices
%Process covariance 
var_acc = 1.0; % Blind guess
Rv = diag( [ var_acc, var_acc, var_acc ] );
filt.Sv = sqrtm(Rv); % Matrix is diagonal, so can just take sqrt of each element

% Measurement covariance
var_pos = 0.001; % Blind guess
var_vel = 0.001; % Blind guess
Rn = diag( [var_pos, var_pos, var_pos, var_vel, var_vel, var_vel] );
filt.Sn = sqrtm(Rn); % Matrix is diagonal, so can just take sqrt of each element

% Initial state covariance
var_pos = 0.1; % Blind guess
var_vel = 0.1; % Blind guess
var_accbias = 0.1; % Blind guess
Rx = diag( [ var_pos, var_pos var_pos, var_vel, var_vel, var_vel, var_accbias, var_accbias, var_accbias] );
filt.Sx = sqrtm(Rx); % Matrix is diagonal, so can just take sqrt of each element


%% Filter parameters
% Sigma point spread param
h = sqrt(3); % Optimal value for Gaussian prior RVs

% Dimensions
filt.Lx =  size(filt.Sx,1);
filt.Lv = size(filt.Sv,1);
filt.Ln = size(filt.Sn,1);
filt.L = filt.Lx + filt.Lv + filt.Ln; % Size of augmented state vector

% Sigma point weights
hh = h^2;
W1 = [(hh - filt.Lx - filt.Lv)/hh   1/(2*hh);                  % sigma-point weights set 1
      1/(2*h)                sqrt(hh-1)/(2*hh)];

W2      = W1;
W2(1,1) = (hh - filt.Lx - filt.Ln)/hh ;                        % sigma-point weights set 2

filt.h = h; filt.W1 = W1; filt.W2 = W2;

% Gaussian noise variance for bias random walk propagation
filt.normalvar_accbias = 0.001; % Blind guess

% Misc
filt.dt = param.DT;
filt.G = param.G;

%% Initialize state vector
filt.x = [ truth.x(1); truth.y(1); truth.z(1);
           truth.dx(1); truth.dy(1); truth.dz(1);
           0; 0; 0; % acc biases assumed to start at zero
         ];
     
filt.att = [truth.qw(1); truth.qx(1); truth.qy(1); truth.qz(1)];
filt.gyrobias = [0;0;0]; % Gyro biases assumed to start at zero

%% Filter loop
tic;
iEnd = length(sensor.t);
for i=2:iEnd%length(sensor.t)
   
    % ***** Time (process) update ********* %
    % Propagate attitude
    gyro_sensor = [sensor.gx(i); sensor.gy(i); sensor.gz(i) ];
    att_new = propAttitude(filt.att(:,i-1),gyro_sensor, filt.gyrobias(:,i-1), filt.dt);
    filt.att = [filt.att, att_new];
    filt.gyrobias = [filt.gyrobias, filt.gyrobias(:,i-1)];
    
    % Calculate sigma points for time update
    x_aug = [filt.x(:,i-1); zeros( filt.Lv, 1 )]; % Process noise expectation is zero
    S_aug = [ filt.Sx,                   zeros(filt.Lx, filt.Lv);
              zeros(filt.Lv, filt.Lx),   filt.Sv; ];

    X = [ x_aug,   repmat(x_aug,1,filt.Lx+filt.Lv)+filt.h*S_aug,  repmat(x_aug,1,filt.Lx+filt.Lv)-filt.h*S_aug];
    
    % Propagate sigma points through non-linear time update
    % For readability this is done one column at a time
    X_x = zeros(filt.Lx, size(X,2));
    
    %% Write to file
%      sensor.t
    acc_sensor = [sensor.ax(i); sensor.ay(i); sensor.az(i) ];
    %%
    
    X_x(:,1) = f_kinematic( X(:,1), acc_sensor, att_new, filt.normalvar_accbias, filt.dt );
    x_new = filt.W1(1,1)*X_x(:,1);
    for j=2:size(X,2)
        X_x(:,j) = f_kinematic( X(:,j), acc_sensor, att_new, filt.normalvar_accbias, filt.dt );
        x_new = x_new+filt.W1(1,2)*X_x(:,j);
    end
    filt.x = [filt.x, x_new];
    
    L = filt.Lx + filt.Lv;
    A = filt.W1(2,1)*( X_x(:,2:L+1) - X_x(:,L+2:2*L+1) );
    B = filt.W1(2,2)*(  X_x(:,2:L+1) + X_x(:,L+2:2*L+1) - 2*repmat(X_x(:,1),1,L) );
    [Q,R] = qr([A B]',0);
    
    filt.Sx = R';
    
    % ***** Measurement update ********* %
    if( mod(i, param.nAttMeas) == 0)
        accel_bias = filt.x(7:9,i);
        [correctedAtt, q_meas, new_bias] = accelCorrection(filt.att(:,i), acc_sensor, filt.gyrobias(:,i-1), accel_bias);
        if( length(correctedAtt) > 1)
            filt.att(:,i) = correctedAtt;
            filt.gyrobias(:,i) = new_bias;
        end
    end
    
    % Position velocity measurement
    %if( mod(i,param.nPosVelMeas) == 0)
        % Calculate sigma points for measurement update
        x_aug = [filt.x(:,i); zeros( filt.Ln, 1 )]; % Measurement noise expectation is zero
        S_aug = [ filt.Sx,                   zeros(filt.Lx, filt.Ln);
                  zeros(filt.Ln, filt.Lx),   filt.Sn; ];
        X = [ x_aug,   repmat(x_aug,1,filt.Lx+filt.Ln)+filt.h*S_aug,  repmat(x_aug,1,filt.Lx+filt.Ln)-filt.h*S_aug];

        % Propagate sigma points through non-linear observation function
        % For readability this is done one column at a time
        Ly = 6; % 3 pos + 3 vel
        Y = zeros(Ly, size(X,2));

        Y(:,1) = h_posvel( X(:,1) );
        y = filt.W2(1,1)*Y(:,1);
        
        for j=2:size(X,2)
            Y(:,j) = h_posvel( X(:,j) );
            y = y+filt.W2(1,2)*Y(:,j);
        end
        L = filt.Lx + filt.Ln;
        C = filt.W2(2,1)*( Y(:,2:L+1) - Y(:,L+2:2*L+1) );
        D = filt.W2(2,2)*(  Y(:,2:L+1) + Y(:,L+2:2*L+1) - 2*repmat(Y(:,1),1,L) );
        [Q,R] = qr([C D]',0);
        filt.Sy = R';
        
        % Correction
        Syx1 = C(:,1:filt.Lx);
        Syw1 = C(:,filt.Lx+1:end);
        Pxy = filt.Sx*Syx1';

        K = (Pxy / filt.Sy') / filt.Sy;  % Note that this is (Pxy*inv(Sy'))*inv(Sy)
        
        
        %% Write to file
        meas = [truth.x(i); truth.y(i); truth.z(i); truth.dx(i); truth.dy(i); truth.dz(i)];
        %%
        
        
        
        filt.x(:,i) = filt.x(:,i) + K*( meas - y );

        % Covariance update
        %[filt.Sx'-Syx1'*K'; Syw1'*K'; D'*K']

        [Q,R] = qr([filt.Sx-K*Syx1, K*Syw1, K*D ]',0);
        filt.Sx = R';
    %end
    
    
    % write to file
    fprintf(fileID,'%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n',...
        truth.x(i),truth.y(i),truth.z(i),...
        truth.dx(i),truth.dy(i),truth.dz(i),...
        sensor.ax(i),sensor.ay(i),sensor.az(i),...
        filt.att(1,i),filt.att(2,i),filt.att(3,i),filt.att(4,i));
    
end


fclose(fileID);

toc
% TODO: Write comparison plotters, check attitude r/p/y, change rotation
% matrix to quaternion that mark used, change quaterion update to simple
% one
figure(1);clf; 
subplot(5,1,1);hold all;
plot(sensor.t(1:iEnd), filt.x(1,1:iEnd),'r',sensor.t(1:iEnd), truth.x(1:iEnd),'r--');
plot(sensor.t(1:iEnd), filt.x(2,1:iEnd),'g',sensor.t(1:iEnd), truth.y(1:iEnd),'g--');
plot(sensor.t(1:iEnd), filt.x(3,1:iEnd),'b',sensor.t(1:iEnd), truth.z(1:iEnd),'b--');
title('Position');legend('xfilt', 'xvicon','yfilt', 'yvicon','zfilt', 'zvicon');
subplot(5,1,2);hold all;
plot(sensor.t(1:iEnd), filt.x(4,1:iEnd),'r',sensor.t(1:iEnd), truth.dx(1:iEnd),'r--');
plot(sensor.t(1:iEnd), filt.x(5,1:iEnd),'g',sensor.t(1:iEnd), truth.dy(1:iEnd),'g--');
plot(sensor.t(1:iEnd), filt.x(6,1:iEnd),'b',sensor.t(1:iEnd), truth.dz(1:iEnd),'b--');
title('Velocity');legend('dxfilt', 'dxvicon','dyfilt', 'dyvicon','dzfilt', 'dzvicon');
subplot(5,1,3);hold all;
[y,p,r] = quat2angle([filt.att(1,:)' filt.att(2,:)' filt.att(3,:)' filt.att(4,:)']);
plot(sensor.t(1:iEnd), r(1:iEnd).*180/pi,'r',sensor.t(1:iEnd), truth.roll(1:iEnd).*180/pi,'r--');
plot(sensor.t(1:iEnd), p(1:iEnd).*180/pi,'g',sensor.t(1:iEnd), truth.pitch(1:iEnd).*180/pi,'g--');
plot(sensor.t(1:iEnd), y(1:iEnd).*180/pi,'b',sensor.t(1:iEnd), truth.yaw(1:iEnd).*180/pi,'b--');
title('Roll/Pitch/Yaw');legend('rfilt', 'rvicon','pfilt', 'pvicon','yfilt', 'yvicon');
subplot(5,1,4);hold all;
plot(sensor.t(1:iEnd), filt.x(7,1:iEnd),'r');
plot(sensor.t(1:iEnd), filt.x(8,1:iEnd),'g');
plot(sensor.t(1:iEnd), filt.x(9,1:iEnd),'b');
title('Estimated Accel Biases');legend('x', 'y','z');
subplot(5,1,5);hold all;
plot(sensor.t(1:iEnd), filt.gyrobias(1,1:iEnd),'r');
plot(sensor.t(1:iEnd), filt.gyrobias(2,1:iEnd),'g');
plot(sensor.t(1:iEnd), filt.gyrobias(3,1:iEnd),'b');
title('Estimated Gyro Biases');legend('x', 'y','z');

end

%% Attitude propagation
function att_new = propAttitude(att, gyro, gyrobias, dt)
e0 = att(1); e1 = att(2); e2 = att(3); e3 = att(4);
gyro=gyro-gyrobias; % Correct for estimated bias
p = gyro(1); q = gyro(2); r = gyro(3);

% Attitude update
att_new = zeros(4,1);
att_new(1) = e0 - dt/2*(e1*p + e2*q + e3*r);
att_new(2) = e1 + dt/2*(e0*p - e3*q + e2*r);
att_new(3) = e2 + dt/2*(e3*p + e0*q - e1*r);
att_new(4) = e3 + dt/2*(e1*q - e2*p + e0*r);

att_new = att_new / norm(att_new,2);

end

%% Non-linear time update function
% Based solely on kinematics
% Takes the augmented state vector, i.e. [x; v] where v contains
% the process noise terms defined above
function x_new = f_kinematic( x_old, acc_sensor, att, normalvar_accbias, dt)
G=9.81;
% Pull out each variable for readability
pos=x_old(1:3);
vel=x_old(4:6);
abias=x_old(7:9);
n_acc=x_old(10:12);

%e = e/norm(e,2);
e0=att(1);e1=att(2);e2=att(3);e3=att(4);

% Position update
pos_new = pos + vel*dt;

% Velocity update
C_b2n = 2*[ 0.5-e2^2-e3^2,   e1*e2-e0*e3,   e1*e3+e0*e2;
            e1*e2+e0*e3,     0.5-e1^2-e3^2, e2*e3-e0*e1;
            e1*e3-e0*e2,     e2*e3+e0*e1,   0.5-e1^2-e2^2;];
acc_corr = acc_sensor - abias - n_acc;  % Corrected for bias and noise
vdot = C_b2n*acc_corr - [0; 0; G]; % Rotate accel vector and correct for gravity
vel_new = vel + vdot*dt;

% Bias update - Gaussian random walk
abias_new = abias ;%+ dt*normrnd(0, sqrt(normalvar_accbias), 3,1);

% Send back updated state
x_new = [ pos_new;
          vel_new;
          abias_new;
         ];

end


%% Position/velocity observation function
% Takes the augmented state vector, i.e. [x; n] where n contains
% the measurement noise terms defined above
function y = h_posvel( x_old )
% Pull out each variable for readability
pos=x_old(1:3);
vel=x_old(4:6);
%abias=x_old(7:9);
n_pos=x_old(10:12);
n_vel=x_old(13:15);

% Position observation
y_pos = pos + n_pos;

% Velocity observation
y_vel = vel + n_vel;


% Send back observation
y = [ y_pos;
      y_vel;
    ];

end



%% Attitude correction based on accelerometer, and update gyro biases
function [correctedAtt, q_meas, gyrobias] = accelCorrection(att, accel, gyrobias, accel_bias)

global param
%accel = accel - accel_bias;
accel = -accel / 9.81;
ax = accel(1);
ay = accel(2);
az = accel(3);

root = sqrt(ax*ax + ay*ay + az*az);
%if root < 0.5 || root > 1.5
if root < 0.7 || root > 1.3
    correctedAtt = -1;
    q_meas = 0;
    return;
end

ax = ax/root;
ay = ay/root;
az = az/root;

if ax > 0.998 || -ax > 0.998
    correctedAtt = -2;
    q_meas = 0;
    return;
end

root = sqrt(ay*ay + az*az);
if root < 0.0001
    root = 0.0001;
end

sinR = -ay/root;
cosR = -az/root;

sinP = ax;
cosP = -(ay*sinR + az*cosR);

cosR2 = sqrt((cosR+1)*0.5);
if cosR2 < 0.0001
    cosR2 = 0.0001;
end

sinR2 = sinR/cosR2*0.5; %WARNING: This step is numerically ill-behaved!

cosP2 = sqrt((cosP+1)*0.5);
if cosP2 < 0.0001
    cosP2 = 0.0001;
end

sinP2 = sinP/cosP2*0.5; %WARNING: This step is numerically ill-behaved!

if (cosR2*cosR2 + sinR2*sinR2) > 1.1 || (cosP2*cosP2 + sinP2*sinP2) > 1.1
    correctedAtt = -3;
    q_meas = 0;
    return;
end

cosY2 = 1.0;
sinY2 = 0;

qyaw = [cosY2 0 0 sinY2];
qpitch = [cosP2 0 sinP2 0];
qroll = [ cosR2 sinR2 0 0];

q_meas = quatmultiply(qyaw,qpitch);
q_meas = quatmultiply(q_meas, qroll);

if dot(q_meas,att) < 0
    q_meas = -q_meas;
end

correctedAtt = att;
for i=1:4
    correctedAtt(i) = att(i) - param.K_AttFilter*(att(i) - q_meas(i));
end

% Estimate the bias
conjatt = [att(1); -1*att(2:4)];
Qbias = quatmultiply(conjatt', q_meas);
gyrobias = gyrobias - param.K_GyroBias*Qbias(2:4)';

end

%% Gets sensor/truth data from log file
function [sensor, truth] = getData(file)

global param

data = importdata(file,',');

%% Choose data to use
% Time
col = 1;
sensor.rost = data(:,col); col = col+1;
sensor.rost = sensor.rost - sensor.rost(1);
sensor.t = 0:1/500:(length(sensor.rost)-1)/500;

% Gyro Data - 500 Hz
sensor.gx = data(:,col); col=col+1;
sensor.gy = data(:,col); col=col+1;
sensor.gz = data(:,col); col=col+1;
% remove initial bias
sensor.gx = sensor.gx - mean(sensor.gx(100:600));
sensor.gy = sensor.gy - mean(sensor.gy(100:600));
sensor.gz = sensor.gz - mean(sensor.gz(100:600));
% Scale to rad/sec in correct orientation
sensor.gx = -sensor.gx*param.gyro_scale;
sensor.gy = -sensor.gy*param.gyro_scale;
sensor.gz =  sensor.gz*param.gyro_scale;

% Accel Data - 500 Hz
sensor.ax = data(:,col); col=col+1;
sensor.ay = data(:,col); col=col+1;
sensor.az = data(:,col); col=col+1;

% Scale to m/s^2 in correct orientation
sensor.ax = -sensor.ax*param.accel_scale;
sensor.ay = -sensor.ay*param.accel_scale;
sensor.az = sensor.az*param.accel_scale;

% Magnetometer Data - 50 Hz
sensor.mx = data(:,col); col=col+1;
sensor.my = data(:,col); col=col+1;
sensor.mz = data(:,col); col=col+1;
% get when magnetometer changes
magx = sensor.mx(1);
i = 1;
while magx == sensor.mx(i)
    i = i+1;
end
index = i:10:length(sensor.mx);
sensor.mx = sensor.mx(index);
sensor.my = sensor.my(index);
sensor.mz = sensor.mz(index);
sensor.tmag = sensor.t(index);

% Pressure Data ~13.2 Hz
sensor.temperature = data(:,col); col=col+1;
sensor.pressure = data(:,col); col=col+1;
% p = sensor.pressure(1);
% i = 1;
% index = 1;
% while i ~= length(sensor.pressure)
%     p = sensor.pressure(i);
%     while p == sensor.pressure(i)
%         i = i+1;
%     end
%     index = [index i];
% end


sensor.sonar = data(:,col); col=col+1;

% Vicon truth data
truth.x = data(:,col); col=col+1;
truth.y = data(:,col); col=col+1;
truth.z = data(:,col); col=col+1;

truth.qw = data(:,col); col=col+1;
truth.qx = data(:,col); col=col+1;
truth.qy = data(:,col); col=col+1;
truth.qz = data(:,col); col=col+1;

truth.dx = data(:,col); col=col+1;
truth.dy = data(:,col); col=col+1;
truth.dz = data(:,col); col=col+1;

truth.p = data(:,col); col=col+1;
truth.q = data(:,col); col=col+1;
truth.r = data(:,col);

[truth.yaw truth.pitch truth.roll] = quat2angle([truth.qw truth.qx truth.qy truth.qz]);

% numerically diff vicon velocity to get accel estimates
truth.d2x = derivative_cwt(truth.dx','gaus1',40,param.DT,1);
truth.d2y = derivative_cwt(truth.dy','gaus1',40,param.DT,1);
truth.d2z = derivative_cwt(truth.dz','gaus1',40,param.DT,1);

end