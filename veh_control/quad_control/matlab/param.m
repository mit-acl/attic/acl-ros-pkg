%% param.m
%% 
%% UberQuad parameter file
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc; clear all;
% figure(1); clf;

% C.name = 'UQ02';
%  C.name = 'mQ01';
C.name = 'BQ04';
C.useAdaptive = 0;  % 0 if on-board PID, 1 if MRAC, 2 if CL-MRAC
                    % 3 if BKR-MRAC, 4 if BKR-CL-MRAC, 5 if off-board PID
                    % (called through RBF_BKRCL_Output)
C.num_iter = 1;         % number of iterations to run
C.wait_time = 0.2; % 3;        % time in seconds to wait between iterations
% delete('concurrent_learning_parameters.mat');
fly_quad = 1;
% maneuver = 'straight line';
% maneuver = 'big flip';
% maneuver = 'flip';
% maneuver = 'flip back';
% maneuver = 'small flip';
% maneuver = 'small flip back';
maneuver = 'up down';
% maneuver = 'fixed-pitch flip';
% maneuver = 'generic path';
% maneuver = 'tt one';
% maneuver = 'tt two';
% maneuver = 'tt three';
% maneuver = 'tt four';
%  maneuver = 'Tom fig 8';
% maneuver = 'Tom helix';


maneuver2 = 'tt one back';


n = 10; % ninth order polynomial
wp0 = zeros(3,n/2-1); % initial x vel, acc, jerk
wpf = zeros(3,n/2-1); % final x vel, acc, jerk

% % straight line
if strcmp(maneuver,'straight line')
%     wp = [0 0; 0 2; 0 0];
    wp = [0 0; 0 -2; 0 0];
    wpm_acc = [0;0;0];
    wpm_elm = [];
    sigma = [1];
    tt = 4.3;
    display(maneuver);
end

if strcmp(maneuver,'big flip')
    wp = [0 0 0; 0 0 -1; 0 0 0];
    wpm_acc = [0;0;-9.81];
    wpm_elm = 2;
    tt = 1.2*[0.76 0.76];
    sigma = [1 0];
    display(maneuver);
end

% flip - worked well before
if strcmp(maneuver,'flip')
    wp = [0 0 0; 0 -0.2 -.4; 0 0.2 0];
    wpm_acc = [0;0;-9.81];
    wpm_elm = 2;
    tt = 0.7*[0.76 0.76];
    sigma = [1 0];
    display(maneuver);
end

% BACKWARDS flip - worked well before
if strcmp(maneuver,'flip back')
    wp = [0 0 0; 0 0.2 .4; 0 0.2 0];
    wpm_acc = [0;0;-9.81];
    wpm_elm = 2;
    tt = 0.7*[0.76 0.76];
    sigma = [0 1];
    display(maneuver);
end

% small flip
if strcmp(maneuver,'small flip')
    wp = [0 0 0; 0 -0.05 -.1; 0 0.2 0];
    wpm_acc = [0;0;-9.81];%
    wpm_elm = 2;
    tt = 0.15*[1 1];
    sigma = [1 0];
    display(maneuver);
end

% % BACKWARDS SMALL flip
if strcmp(maneuver,'small flip back')
    wp = [0 0 0; 0 0.05 .1; 0 0.2 0];
    wpm_acc = [0;0;-9.81];
    wpm_elm = 2;
    tt = 0.15*[1 1];
    sigma = [0 1];
    display(maneuver);
end

% generic path used in youtube video 1
if strcmp(maneuver,'generic path')
    wp = [0 1 -1 0 0;...
          0 -1  -2 -3 0;...
          0 0  0 0 0];
    wpm_acc = [0;0;0];
    wpm_elm = [];
    sigma = 0*ones(1,size(wp,2));
    tt = 1.*[1.1574    0.7080    0.7895    1.3452]; %1.3, 1.9, 2.5
    display(maneuver);
end

if strcmp(maneuver,'up down')
    wp = [0 0 0;...
          0 0 0;...
          0 1 0];
    wpm_acc = [0;0;0];
    wpm_elm = [];
    sigma = ones(1,size(wp,2));
    tt = 1.3*[1 1];
    display(maneuver);
end

if strcmp(maneuver,'fixed-pitch flip')
%     % fixed-pitch quad flip
%     wp = [0  0  0.0 0 0.0;...
%           0  0  -0.01 0 -0.02;...
%           0  0 .4 0 0]; % waypoints
%     wpm_acc = [0 0; .6 -.6; -9.81 -9.81]; % middle acceleration constraint
%     wpm_elm = [2 4]; % wp element that acceleration constraint will replace
%     sigma = [1 0 0 1 ];
%     tt = [0.4965    0.054    0.054    0.4965];

    % fixed-pitch quad flip - tall
    wp = [0  0  0.0 0 0.0;...
          0  0  -0.01 0 -0.02;...
          0  0 1 0 0]; % waypoints
    wpm_acc = [0 0; 1.5 -1.5; -9.81 -9.81]; % middle acceleration constraint
    wpm_elm = [2 4]; % wp element that acceleration constraint will replace
    sigma = [1 0 0 1 ];
    tt =1.68*[0.4965    0.054    0.054    0.4965];

    % fixed-pitch quad flip
    % wp = [0  0  0.15 0 0.3;...
    %       0  0  0 0 0  ;...
    %       0  0 .5 0 0]; % waypoints
    % % wp(1,:) = wp(1,:)*0.8;
    % wpm_acc = [-5 5; 0 0; -9.81 -9.81]; % middle acceleration constraint
    % wpm_elm = [2 4]; % wp element that acceleration constraint will replace
    % sigma = [1 0 0 1 ];
    % tt = 1.1*[0.6    0.0050    0.1601    0.6];

% %     % fixed-pitch quad flip - small
%     wp = [0  0  0.0 0 0.0;...
%           0  0  -0.001 0 -0.002;...
%           0  0 .05 0 0]; % waypoints
%     wpm_acc = [0 0; .3 -.3; -9.81 -9.81]; % middle acceleration constraint
%     wpm_elm = [2 4]; % wp element that acceleration constraint will replace
%     sigma = [1 0 0 1 ];
%     tt = 0.345*[0.4965    0.054    0.054    0.4965];
end

%% TICTOC

% one
% if strcmp(maneuver,'tt one')
%     wp = [0 0 0; 0 0 -1; 0 0 0];
%     wpm_acc = [0; 0; -9.81];%
%     wpm_elm = 2;
%     tt = 3*[0.19 0.19];
%     sigma = [1 0];
%     display(maneuver);
% end
if strcmp(maneuver,'tt one')
    wp = [0 0 0; 0 0 -0.2; 0 0 0];
    wpm_acc = [0; 0; -9.81];%
    wpm_elm = 2;
    tt = 1.3*[0.19 0.19];
    sigma = [1 0];
    display(maneuver);
end

% one back
if strcmp(maneuver2,'tt one back')
    wp2 = [0 0 0; 0 0 1; 0 0 0];
    wpm_acc2 = [0; 0; -9.81];%
    wpm_elm2 = 2;
    tt2 = 3*[0.19 0.19];
    sigma2 = [0 1];
    display(maneuver);
end
% if strcmp(maneuver,'tt one')
%     wp = [0 0 0; 0 0 -.5; 0 0 0];
%     wpm_acc = [0; 0; -9.81];%
%     wpm_elm = 2;
%     tt = 2*[0.19 0.19];
%     sigma = [1 0];
%     display(maneuver);
% end

% if strcmp(maneuver,'tt one')
%     wp = [0 0 0; 0 0 -.5; 0 0 0];
%     wpm_acc = [0; 0; -9.81];%
%     wpm_elm = 2;
%     tt = 3*[0.19 0.19];
%     sigma = [1 0];
%     display(maneuver);
% end

% two
if strcmp(maneuver,'tt two')
    wp = [0 0 0 0 0; 0 0 -0.2 0 0; 0 0 0 0 0];
    wpm_acc = [0 0;0 0;-9.81 -9.81];%
    wpm_elm = [2 4];
    tt = 1.3*[0.19 0.11 0.11 0.19];
    sigma = [1 0 0 1];
    display(maneuver);
end

% three
if strcmp(maneuver,'tt three')
    wp = [0 0 0 0 0 0 0; 0 0 -.46 0 -.04 0 -0.5; 0 0 0.05 0 0.05 0 0];
    wpm_acc = [0 0 0;0 0 0;-9.81 -9.81 -9.81];%
    wpm_elm = [2 4 6];
    tt = 2*[0.19 0.10 0.10 0.10 0.10 0.19];
    sigma = [1 0 0 1 1 0];
    display(maneuver);
end

% % six
if strcmp(maneuver,'tt four')
    wp = [0 0 0 0 0 0 0 0 0 0 0 0 0; 0 0 -.5 0 0 0 -0.5 0 0 0 -0.5 0 0; 0 0 0 0 0 0 0 0 0 0 0 0 0];
    wpm_acc = [0 0 0 0 0 0;0 0 0 0 0 0;-9.81 -9.81 -9.81 -9.81 -9.81 -9.81];%
    wpm_elm = [2 4 6 8 10 12];
    tt = 3*[0.19 0.11 0.11 0.11 0.11 0.11 0.11 0.11 0.11  0.11 0.11 0.19];
    sigma = [1 0 0 1 1 0 0 1 1 0 0 1];
    display(maneuver);
end

%% Tom's stuff

% helix - Tom
if strcmp(maneuver,'Tom helix')
theta = linspace(0, 6*pi, 150);
wp = 1 * [cos(theta) - 1;...
      sin(theta);...
      0 * 0.1 * theta]; % waypoints
wp0 = [0 0 0 0;... % initial x vel, acc, jerk, snap
    0 0 0 0;... % initial y vel, acc, jerk, snap
    0 0 0 0];   % initial z vel, acc, jerk, snap
wpf = [0 0 0 0;... % final x vel, acc, jerk, snap
    0 0 0 0;... % final y vel, acc, jerk, snap
    0 0 0 0];   % final z vel, acc, jerk, snap
wpm_acc = [0;0;0]; % middle acceleration constraint
wpm_elm = []; % wp element that acceleration constraint will replace
sigma = ones(1,length(theta) -1);  % upright 1 or inverted 0 for that time segment
tt = 0.1 * ones(1,length(theta) -1); % time vector must have size(wp,2)-1 elements - each
tt(1) = 1.6; tt(length(theta) -1) = 1.6;
end

% figure 8 repeat - Tom
if strcmp(maneuver,'Tom fig 8')
y = [-1.06 -1.5 -1.06 1.06 1.5 1.06]; y = [0 y y y 0];
x = [-0.75 0 0.75 -0.75 0 0.75]; x = [0 x x x 0];
z = -0.3 * 0 * y;
theta = 0*pi/180;
R = [cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1];
wp = R*[x;y;z];
wpm_acc = [0;0;0];
wpm_elm = [];
gain = 3;
tt = gain*[1    0.33    0.33    1    0.33    0.33]; tt = [tt tt tt gain];
sigma = ones(1,length(x)-1);
end

%%

[C.patheval, C.path] = polygen_acc_const2(wp,wp0,wpf,wpm_acc,wpm_elm,sigma,n,tt,100,0); % compute the desired path
% if C.num_iter > 1
%     [C.patheval2, C.path2] = polygen_acc_const2(wp2,wp0,wpf,wpm_acc2,wpm_elm2,sigma2,n,tt2,100,0); % compute the desired path
% end
C.patheval2 = C.patheval; C.path2 = C.path;


if fly_quad
    send_goal(C);
else
    [C.patheval, C.path] = polygen_acc_const2(wp,wp0,wpf,wpm_acc,wpm_elm,sigma,n,tt,100,1); % compute the desired path
    test_for_sat(C.patheval,1);
end


% Tom added this if statement to plot MRAC's data
if C.useAdaptive && fly_quad
    global cc_param cc_param_temp
    save quaddata cc_param
    save quaddata_CL cc_param_temp
    MRAC_plotter
   
end