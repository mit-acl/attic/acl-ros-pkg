function goal_out = path_gen(t,state,C)

persistent pathIndex totalTime goal
if isempty(pathIndex)
    pathIndex = 1;
    totalTime = 0;
end

t = t-totalTime;
if t < C.path(pathIndex).t(end)
    idx = find(t<C.path(pathIndex).t);
    idx = idx(1)-1;
    goal.x = polyval(C.path(pathIndex).x(idx,:),t);
    goal.dx = polyval(C.path(pathIndex).dx(idx,:),t);
    goal.d2x = polyval(C.path(pathIndex).d2x(idx,:),t);
    goal.d3x = polyval(C.path(pathIndex).d3x(idx,:),t);
    
    goal.y   = polyval(C.path(pathIndex).y(idx,:),t);
    goal.dy  = polyval(C.path(pathIndex).dy(idx,:),t);
    goal.d2y = polyval(C.path(pathIndex).d2y(idx,:),t);
    goal.d3y = polyval(C.path(pathIndex).d3y(idx,:),t);
    
    goal.z   = polyval(C.path(pathIndex).z(idx,:),t);
    goal.dz  = polyval(C.path(pathIndex).dz(idx,:),t);
    goal.d2z = polyval(C.path(pathIndex).d2z(idx,:),t);
    goal.d3z = polyval(C.path(pathIndex).d3z(idx,:),t);
    
    goal.useinfo = true;
    
    idx2 = find(t<C.patheval(pathIndex).t);
    goal.upright = C.patheval(pathIndex).upright(idx2(1));

elseif pathIndex < length(C.path)
    totalTime = totalTime + C.path(pathIndex).t(end);
    pathIndex = pathIndex + 1

else
    goal.x = polyval(C.path(pathIndex).x(end,:),C.path(pathIndex).t(end));
    goal.dx = 0;
    goal.d2x = 0;
    goal.d3x = 0;
    
    goal.y = polyval(C.path(pathIndex).y(end,:),C.path(pathIndex).t(end));
    goal.dy = 0;
    goal.d2y = 0;
    goal.d3y = 0;
    
    goal.z = polyval(C.path(pathIndex).z(end,:),C.path(pathIndex).t(end));
    goal.dz = 0;
    goal.d2z = 0;
    goal.d3z = 0;
    
    goal.useinfo = false;
    
    goal.upright = C.patheval(pathIndex).upright(end);
end

goal_out = goal;

% desired goal assumes you are starting at (0,0,0)
% add starting state to desired goal to get true goal
goal_out.x = goal_out.x + state.x;
goal_out.y = goal_out.y + state.y;
goal_out.z = goal_out.z + state.z;
