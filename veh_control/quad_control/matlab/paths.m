n = 10;

% small flip
smallflip.wp = [0 0.05 .1; 0 0 0; 0 0.2 0];
smallflip.wpm_acc = [0;0;-9.81];%
smallflip.wpm_elm = 2;
smallflip.tt = 0.15*[1 1];
smallflip.sigma = [1 0];
smallflip.n = 10; % ninth order polynomial
smallflip.wp0 = zeros(3,n/2-1); % initial x vel, acc, jerk
smallflip.wpf = zeros(3,n/2-1); % final x vel, acc, jerk

% BACKWARDS SMALL flip
smallflipback.wp = [0 -0.05 -.1; 0 0 0; 0 0.2 0];
smallflipback.wpm_acc = [0;0;-9.81];%
smallflipback.wpm_elm = 2;
smallflipback.tt = 0.15*[1 1];
smallflipback.sigma = [0 1];
smallflipback.n = 10; % ninth order polynomial
smallflipback.wp0 = zeros(3,n/2-1); % initial x vel, acc, jerk
smallflipback.wpf = zeros(3,n/2-1); % final x vel, acc, jerk
