% Plot Rosbag converted Log File

clc; clear all;

%% IMPORT DATA
log = importdata('quadlog.txt',',',1);
% log = importdata('quadlog_generic_up.txt',',',1);
% log = importdata('quadlog_generic_down.txt',',',1);

index = find(log.data(:,34)); % this is the throttle channel -- we are only
% interested in values when there is a non-zero throttle command

log.data = log.data(index,:);

%% Choose data to use
% up
% t_start = 15;
% t_end = 35;

% down
% t_start = 30;
% t_end = 45;
% 
% time = log.data(:,1);
% time = time - time(1);
% index = find(time > t_start & time < t_end);
% log.data = log.data(index,:);

col = 1;
% Command Data
t_c = log.data(:,col); col=col+1;

x_c = log.data(:,col); col=col+1;
y_c = log.data(:,col); col=col+1;
z_c = log.data(:,col); col=col+1;

q0_c = log.data(:,col); col=col+1;
qx_c = log.data(:,col); col=col+1;
qy_c = log.data(:,col); col=col+1;
qz_c = log.data(:,col); col=col+1;

dx_c = log.data(:,col); col=col+1;
dy_c = log.data(:,col); col=col+1;
dz_c = log.data(:,col); col=col+1;

p_c = log.data(:,col); col=col+1;
q_c = log.data(:,col); col=col+1;
r_c = log.data(:,col); col=col+1;

roll_c = log.data(:,col); col=col+1;
pitch_c = log.data(:,col); col=col+1;
yaw_c = log.data(:,col); col=col+1;

d2x_c = log.data(:,col); col=col+1;
d2y_c = log.data(:,col); col=col+1;
d2z_c = log.data(:,col); col=col+1;

d2x_fb_c = log.data(:,col); col=col+1;
d2y_fb_c = log.data(:,col); col=col+1;
d2z_fb_c = log.data(:,col); col=col+1;

d3x_c = log.data(:,col); col=col+1;
d3y_c = log.data(:,col); col=col+1;
d3z_c = log.data(:,col); col=col+1;

d3x_fb_c = log.data(:,col); col=col+1;
d3y_fb_c = log.data(:,col); col=col+1;
d3z_fb_c = log.data(:,col); col=col+1;

int_x = log.data(:,col); col=col+1;
int_y = log.data(:,col); col=col+1;
int_z = log.data(:,col); col=col+1;

% collective_c = log.data(:,col); col=col+1;
throttle_c = log.data(:,col); col=col+1;
f_total_c = log.data(:,col); col=col+1;

% health data
t_h = log.data(:,col); col=col+1;

voltage = log.data(:,col); col=col+1;
current0 = log.data(:,col); col=col+1;
current1 = log.data(:,col); col=col+1;
current2 = log.data(:,col); col=col+1;
current3 = log.data(:,col); col=col+1;

temp0 = log.data(:,col); col=col+1;
temp1 = log.data(:,col); col=col+1;
temp2 = log.data(:,col); col=col+1;
temp3 = log.data(:,col); col=col+1;

q0_h = log.data(:,col); col=col+1;
qx_h = log.data(:,col); col=col+1;
qy_h = log.data(:,col); col=col+1;
qz_h = log.data(:,col); col=col+1;

q0_meas_h = log.data(:,col); col=col+1;
qx_meas_h = log.data(:,col); col=col+1;
qy_meas_h = log.data(:,col); col=col+1;
qz_meas_h = log.data(:,col); col=col+1;

roll_h = log.data(:,col); col=col+1;
pitch_h = log.data(:,col); col=col+1;
yaw_h = log.data(:,col); col=col+1;

roll_meas_h = log.data(:,col); col=col+1;
pitch_meas_h = log.data(:,col); col=col+1;
yaw_meas_h = log.data(:,col); col=col+1;

p_h = log.data(:,col); col=col+1;
q_h = log.data(:,col); col=col+1;
r_h = log.data(:,col); col=col+1;

% vicon pose
t_p = log.data(:,col); col=col+1;

x_p = log.data(:,col); col=col+1;
y_p = log.data(:,col); col=col+1;
z_p = log.data(:,col); col=col+1;

q0_p = log.data(:,col); col=col+1;
qx_p = log.data(:,col); col=col+1;
qy_p = log.data(:,col); col=col+1;
qz_p = log.data(:,col); col=col+1;

% vicon twist
t_t = log.data(:,col); col=col+1;

dx_t = log.data(:,col); col=col+1;
dy_t = log.data(:,col); col=col+1;
dz_t = log.data(:,col); col=col+1;

p_t = log.data(:,col); col=col+1;
q_t = log.data(:,col); col=col+1;
r_t = log.data(:,col); col=col+1;

% adaptive element
% t_a = log.data(:,col); col=col+1;

% acc_cr_x = log.data(:,col); col=col+1;
% acc_cr_y = log.data(:,col); col=col+1;
% acc_cr_z = log.data(:,col); col=col+1;
% 
% acc_pd_x = log.data(:,col); col=col+1;
% acc_pd_y = log.data(:,col); col=col+1;
% acc_pd_z = log.data(:,col); col=col+1;
% 
% acc_ad_x = log.data(:,col); col=col+1;
% acc_ad_y = log.data(:,col); col=col+1;
% acc_ad_z = log.data(:,col); col=col+1;
% 
% acc_AttCmd = log.data(:,col); col=col+1;

%% PROCESS DATA
% normalize times to agree with command data
start_time = t_c(1);
t_c = t_c - start_time;
t_h = t_h - start_time;
t_p = t_p - start_time;
t_t = t_t - start_time;
% t_a = t_a - start_time;

%  Vicon data
pos  = [x_p y_p z_p];
vel  = [dx_t dy_t dz_t];                 % velocity in meters/sec
[yaw, pitch, roll] = quat2angle([q0_p qx_p qy_p qz_p]);
att  = [roll pitch yaw]*180/pi;
rate = [p_t q_t r_t]*180/pi;

% Desired data
pos_des = [x_c y_c z_c];
vel_des = [dx_c dy_c dz_c]; 
[yaw, pitch, roll] = quat2angle([q0_c qx_c qy_c qz_c]);
att_des = [roll pitch yaw]*180/pi;
rate_des = [p_c q_c r_c]*180/pi;
d2_des = [d2x_c d2y_c d2z_c];
d2_fb_des = [d2x_fb_c d2y_fb_c d2z_fb_c];
d3_des = [d3x_c d3y_c d3z_c];
d3_fb_des = [d3x_fb_c d3y_fb_c d3z_fb_c];

% Adaptive loop data
% acc_cr = [acc_cr_x,acc_cr_y,acc_cr_z];
% acc_pd = [acc_pd_x,acc_pd_y,acc_pd_z];
% acc_ad = [acc_ad_x,acc_ad_y,acc_ad_z];

% save('generic_up.mat','pos','pos_des','t_c');
% save('generic_down.mat','pos','pos_des','t_c');

%% PLOT DATA

% Note -- everything has a different time vector; however, they are all
% quite similar and so I plot everything based on the commanded time vector

clr = colormap('lines');

fignum = 1;
% Position
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(t_c, pos(:,ii),'color',clr(1,:),'LineWidth',3); 
        hold all;
        plot(t_c, pos_des(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Position'); ylabel('X Position (m)'); legend('Actual','Desired','Location','NorthWest'); end
        if ii==2, ylabel('Y Position (m)'); end
        if ii==3, xlabel('Time (s)'); ylabel('Z Position (m)'); end
end

% Velocity
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(t_c, vel(:,ii),'color',clr(1,:),'LineWidth',3); 
        hold all;
        plot(t_c, vel_des(:,ii),'color',clr(3,:),'LineWidth',3); 
        if ii==1, title('Velocity'); ylabel('X Velocity (m/s)'); legend('Actual','Desired','Location','NorthWest'); end
        if ii==2, ylabel('Y Velocity (m/s)'); end
        if ii==3, xlabel('Time (s)'); ylabel('Z Velocity (m/s)'); end
end

% Attitude
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(t_c,att(:,ii),'color',clr(1,:),'LineWidth',3);
        hold all;
        plot(t_c,att_des(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Attitude'); ylabel('Roll (deg)'); legend('Actual','Desired','Location','NorthWest'); end
        if ii==2, ylabel('Pitch (deg)'); end
        if ii==3, ylabel('Yaw (deg)'); xlabel('Time (s)'); end
end

% Rate
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(t_c,rate(:,ii),'color',clr(1,:),'LineWidth',3);
        hold all;
        plot(t_c,rate_des(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Rate'); ylabel('Roll Rate'); legend('Actual','Desired','Location','NorthWest'); end
        if ii==2, ylabel('Pitch Rate'); end
        if ii==3, ylabel('Yaw Rate'); xlabel('Time (s)'); end
end

% Acceleration
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(t_c,d2_des(:,ii),'color',clr(1,:),'LineWidth',3);
        hold all;
        plot(t_c,d2_fb_des(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Acceleration'); ylabel('X'); legend('Desired','FeedBack','Location','NorthWest'); end
        if ii==2, ylabel('Y'); end
        if ii==3, ylabel('Z'); xlabel('Time (s)'); end
end

% Jerk
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(t_c,d3_des(:,ii),'color',clr(1,:),'LineWidth',3);
        hold all;
        plot(t_c,d3_fb_des(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Jerk'); ylabel('X'); legend('Desired','FeedBack','Location','NorthWest'); end
        if ii==2, ylabel('Y'); end
        if ii==3, ylabel('Z'); xlabel('Time (s)'); end
end

f(fignum)=figure(fignum); clf; fignum = fignum+1;
subplot(3,1,1)
plot(t_c,throttle_c,'color',clr(1,:),'LineWidth',3);
ylabel('Throttle');

subplot(3,1,2)
plot(t_h,voltage,'color',clr(1,:),'LineWidth',3);
ylabel('Voltage'); xlabel('Time (s)');

subplot(3,1,3)
plot(t_c,f_total_c,'color',clr(1,:),'LineWidth',3);
ylabel('Total Force Commanded'); xlabel('Time (s)');

f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(t_c,int_x,t_c,int_y,t_c,int_z);
xlabel('Time (s)');
ylabel('Position integrator values');
title('Position integrator');
legend('x','y','z');




% f(7)=figure(7); clf;
% for ii=1:2
%         subplot(2,1,ii)
%         
%         if ii==1
%             plot(t,m,'color',clr(1,:),'LineWidth',3);
%             ylabel('Motor'); 
%         end
%         if ii==2 
%             plot(t,s,'color',clr(1,:),'LineWidth',3);
%             ylabel('Servo'); 
%             xlabel('Time (s)');
%         end
% end

% f(6) = figure(6); clf;
% subplot(2,1,1);
% plot(t,status,'color',clr(1,:),'LineWidth',3);
% ylabel('Status');
% subplot(2,1,2);
% plot(t,upright','color',clr(1,:),'LineWidth',3);
% ylabel('Upright');
% xlabel('Time (s)');

%% Link x axes
ax = [];
for ii=1:length(f)
    ax = [ax; get(f(ii),'children')];
end
linkaxes(ax,'x');

% % 2D position
% f(fignum)=figure(fignum); clf; fignum = fignum+1;
% plot(pos(:,1), pos(:,2),'LineWidth',3);
% hold all;
% plot(pos_des(:,1), pos_des(:,2),'LineWidth',3);
% xlabel('X (m)');
% ylabel('Y (m)');
% title('Position');
% legend('Actual','Desired');

% 3D position
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot3(pos(:,1), pos(:,2), pos(:,3),'LineWidth',3);
hold all;
plot3(pos_des(:,1), pos_des(:,2), pos_des(:,3),'LineWidth',3);
xlabel('X (m)');
ylabel('Y (m)');
zlabel('Z (m)');
title('Position');
legend('Actual','Desired');
axis equal
