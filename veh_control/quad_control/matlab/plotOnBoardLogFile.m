% Plot Rosbag converted Log File

clc; clear all;

%% IMPORT DATA
log = importdata('quadOnBoardLog.txt',',',1);

col = 1;
% Sensor data
est.p = log.data(:,col); col=col+1;
est.q = log.data(:,col); col=col+1;
est.r = log.data(:,col); col=col+1;

est.q0 = log.data(:,col); col=col+1;
est.qx = log.data(:,col); col=col+1;
est.qy = log.data(:,col); col=col+1;
est.qz = log.data(:,col); col=col+1;

% Vicon data
v.p = log.data(:,col); col=col+1;
v.q = log.data(:,col); col=col+1;
v.r = log.data(:,col); col=col+1;

v.q0 = log.data(:,col); col=col+1;
v.qx = log.data(:,col); col=col+1;
v.qy = log.data(:,col); col=col+1;
v.qz = log.data(:,col); col=col+1;

%% PROCESS DATA

%  Sensor data
[est.yaw, est.pitch, est.roll] = quat2angle([est.q0 est.qx est.qy est.qz]);
est.att  = [est.roll est.pitch est.yaw]*180/pi;
est.rate = [est.p est.q est.r]*180/pi;

%  Vicon data
[v.yaw, v.pitch, v.roll] = quat2angle([v.q0 v.qx v.qy v.qz]);
v.att  = [v.roll v.pitch v.yaw]*180/pi;
v.rate = [v.p v.q v.r]*180/pi;


%% PLOT DATA


clr = colormap('lines');

fignum = 1;
% Attitude
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(est.att(:,ii),'color',clr(1,:),'LineWidth',3);
        hold all;
        plot(v.att(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Attitude'); ylabel('Roll (deg)'); legend('Estimated','Vicon','Location','NorthWest'); end
        if ii==2, ylabel('Pitch (deg)'); end
        if ii==3, ylabel('Yaw (deg)'); xlabel('Time (s)'); end
end

% Rate
f(fignum)=figure(fignum); clf; fignum = fignum+1;
for ii=1:3
        subplot(3,1,ii)
        plot(est.rate(:,ii),'color',clr(1,:),'LineWidth',3);
        hold all;
        plot(v.rate(:,ii),'color',clr(3,:),'LineWidth',3);
        if ii==1, title('Rate'); ylabel('Roll Rate'); legend('Estimated','Vicon','Location','NorthWest'); end
        if ii==2, ylabel('Pitch Rate'); end
        if ii==3, ylabel('Yaw Rate'); xlabel('Time (s)'); end
end

%% Link x axes
ax = [];
for ii=1:length(f)
    ax = [ax; get(f(ii),'children')];
end
linkaxes(ax,'x');

