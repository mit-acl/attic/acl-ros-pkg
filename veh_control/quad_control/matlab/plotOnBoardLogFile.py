#!/usr/bin/env python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Load the log file
df = pd.read_csv('quadOnBoardLog.txt')
# print df.columns.tolist()

df.plot(y=['p_est','q_est','r_est'])
plt.title('Body Rates')

df.plot(y=['qo_est', 'qx_est', 'qy_est', 'qz_est'])
plt.title('Quaternion')

df.plot(y=['p_cmd', 'q_cmd', 'r_cmd'])
plt.title('Rate command')

df.plot(y=['qo_cmd', 'qx_cmd', 'qy_cmd', 'qz_cmd'])
plt.title('Quaternion command')

df.plot(y='throttle')
plt.title('Throttle')

df.plot(y='attStatus')
plt.title('attStatus')


plt.show()




