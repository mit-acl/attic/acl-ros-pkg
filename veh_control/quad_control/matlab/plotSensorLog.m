% Plot Sensor Log File

function plotSensorLog

%% TODO
% Get the data at the effective rate at which it is generated

%% IMPORT DATA
% file = 'sensorlog.txt';
file = 'sensorlog1.txt';
% file = 'sensorlog2.txt';
[sensor, truth] = getData(file);

global param
param.K_AttFilter = 0.01*0.7;
param.gyro_scale = pi/14.375/180.0*1.0;
param.DT = 1/500;
param.G = 9.81;
param.filterType = 'Left';
param.filterLen = 51;
param.filterLenPres = 501;

%% Filter Data
accel = filterAccel(sensor);
gyro = filterGyro(sensor, accel);

[pos vel accel_rot] = propAccel(accel,[gyro.qw gyro.qx gyro.qy gyro.qz]);


pres_z = filterPressure(sensor);


%% PLOT DATA

% Note -- everything has a different time vector; however, they are all
% quite similar and so I plot everything based on the commanded time vector

clr = colormap('lines');

fignum = 1;
% Rate
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, gyro.p*180/pi, sensor.t, gyro.q*180/pi, sensor.t, gyro.r*180/pi);
hold all
plot(sensor.t, truth.p*180/pi, sensor.t, truth.q*180/pi, sensor.t, truth.r*180/pi);
xlabel('Time (s)');
ylabel('Rate (deg/s)');
legend('p est','q est','r est','p','q','r');

% Attitude
[y,p,r] = quat2angle([gyro.qw gyro.qx gyro.qy gyro.qz]);
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, r*180/pi, sensor.t, p*180/pi, sensor.t, y*180/pi);
hold all
plot(sensor.t, truth.roll*180/pi, sensor.t, truth.pitch*180/pi, sensor.t, truth.yaw*180/pi);
xlabel('Time (s)');
ylabel('Attitude (deg)');
legend('roll est','pitch est','yaw est','roll','pitch','yaw');

% Attitude Error
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, (r-truth.roll)*180/pi, sensor.t, (p-truth.pitch)*180/pi, sensor.t, (y-truth.yaw)*180/pi);
xlabel('Time (s)');
ylabel('Attitude Error (estimate - truth) (deg)');
legend('roll','pitch','yaw');

% Acceleration
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, accel_rot.x, sensor.t, accel_rot.y, sensor.t, accel_rot.z);
hold all
plot(sensor.t, truth.d2x, sensor.t, truth.d2y, sensor.t, truth.d2z);
xlabel('Time (s)');
ylabel('Accel');
legend('x est','y est','z est','x','y','z');

% Velocity
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, vel.x, sensor.t, vel.y, sensor.t, vel.z);
hold all
plot(sensor.t, truth.dx, sensor.t, truth.dy, sensor.t, truth.dz);
xlabel('Time (s)');
ylabel('Velocity');
legend('x est','y est','z est','x','y','z');

% Position
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, pos.x, sensor.t, pos.y, sensor.t, pos.z);
hold all
plot(sensor.t, truth.x-truth.x(1), sensor.t, truth.y-truth.y(1), sensor.t, truth.z-truth.z(1));
xlabel('Time (s)');
ylabel('Position');
legend('x est','y est','z est','x','y','z');

% Sonar
f(fignum)=figure(fignum); clf; fignum = fignum+1;
plot(sensor.t, sensor.sonar, sensor.t, pres_z, sensor.t, truth.z);
xlabel('Time (s)');
ylabel('Altitude (m)');
legend('z est sonar','z est pres','z');

% % Pressure
% f(fignum)=figure(fignum); clf; fignum = fignum+1;
% plot(sensor.t, sensor.pressure);
% xlabel('Time (s)');
% ylabel('Pressure');
% % legend('x est','y est','z est','x','y','z');


%% Link x axes
ax = [];
for ii=1:length(f)
    ax = [ax; get(f(ii),'children')];
end
linkaxes(ax,'x');
end


function pres_z = filterPressure(sensor)

global param

p0 = 101325;
altitude = 44330*(1 - (sensor.pressure/p0).^(1/5.255));

pres_z = altitude - mean(altitude(1:600));

% moving average filter data
pres_z = movavgFilt (pres_z', param.filterLenPres, param.filterType)';

end



function [correctedAtt, q_meas] = accelCorrection(att, accel)

global param

ax = accel.x;
ay = accel.y;
az = accel.z;

root = sqrt(ax*ax + ay*ay + az*az);

if root < 0.5 || root > 1.5
    correctedAtt = -1;
    q_meas = 0;
    return;
end

ax = ax/root;
ay = ay/root;
az = az/root;

if ax > 0.998 || -ax > 0.998
    correctedAtt = -2;
    q_meas = 0;
    return;
end

root = sqrt(ay*ay + az*az);
if root < 0.0001
    root = 0.0001;
end

sinR = -ay/root;
cosR = -az/root;

sinP = ax;
cosP = -(ay*sinR + az*cosR);

cosR2 = sqrt((cosR+1)*0.5);
if cosR2 < 0.0001
    cosR2 = 0.0001;
end

sinR2 = sinR/cosR2*0.5; %WARNING: This step is numerically ill-behaved!

cosP2 = sqrt((cosP+1)*0.5);
if cosP2 < 0.0001
    cosP2 = 0.0001;
end

sinP2 = sinP/cosP2*0.5; %WARNING: This step is numerically ill-behaved!

if (cosR2*cosR2 + sinR2*sinR2) > 1.1 || (cosP2*cosP2 + sinP2*sinP2) > 1.1
    correctedAtt = -3;
    q_meas = 0;
    return;
end

cosY2 = 1.0;
sinY2 = 0;

qyaw = [cosY2 0 0 sinY2];
qpitch = [cosP2 0 sinP2 0];
qroll = [ cosR2 sinR2 0 0];

q_meas = quatmultiply(qyaw,qpitch);
q_meas = quatmultiply(q_meas, qroll);

if dot(q_meas,att) < 0
    q_meas = -q_meas;
end

correctedAtt = att;
for i=1:4
    correctedAtt(i) = att(i) - param.K_AttFilter*(att(i) - q_meas(i));
end

end

function accel = filterAccel(sensor)

global param

% get bias
xbias = mean(sensor.ax(100:600));
ybias = mean(sensor.ay(100:600));
zbias = mean(sensor.az(100:600));

% subtract bias
x = sensor.ax - xbias;
y = sensor.ay - ybias;
z = sensor.az - zbias + 256;

% % scale data
accel_scale = 1/256;
x = x*accel_scale;
y = y*accel_scale;
z = -z*accel_scale;

% moving average filter data
accel.x = movavgFilt (x', param.filterLen, param.filterType)';
accel.y = movavgFilt (y', param.filterLen, param.filterType)';
accel.z = movavgFilt (z', param.filterLen, param.filterType)';

end


function [pos vel acc] = propAccel(accel, att)

global param

% make z acceleration positive
ax = -(accel.x)*param.G;
ay = -(accel.y)*param.G;
az = -accel.z*param.G;
vx = zeros(size(ax));
vx(1) = 0;
vy = vx;
vz = vx;
px = vx;
py = vx;
pz = vx;

% rotate acceleration based on attitude estimates and subtract out gravity
q_accel = [zeros(size(ax)) ax ay az];
q_gravity = zeros(size(q_accel));
q_gravity(:,4) = param.G;     % nominally gravity is pointing in the z direction
q_accel_rot = quatmultiply(att, quatmultiply(q_accel, quatconj(att)));
q_accel_rot = q_accel_rot - q_gravity;  % after rotation, just subtract in the z direction
% q_accel_rot = q_accel;

ax = q_accel_rot(:,2);
ay = q_accel_rot(:,3);
az = q_accel_rot(:,4);

ax(1:100) = 0;
ay(1:100) = 0;
az(1:100) = 0;

% zero acceleration data within a window
window = 1;
% ax(ax < window & ax > -window) = 0;
% ay(ay < window & ay > -window) = 0;
% az(az < window & az > -window) = 0;


% integrate acceleration to get velocities
cnt.x = 0; cnt.y = 0; cnt.z = 0;
maxCnt = 500;
for i=1:length(ax)-1
    vx(i+1) = vx(i) + param.DT*(ax(i) + (ax(i+1) - ax(i))/2);
    vy(i+1) = vy(i) + param.DT*(ay(i) + (ay(i+1) - ay(i))/2);
    vz(i+1) = vz(i) + param.DT*(az(i) + (az(i+1) - az(i))/2);
    if (ax(i+1)< window && ax(i+1) > -window)
        cnt.x = cnt.x + 1;
        if cnt.x > maxCnt
            vx(i+1) = 0;
        end
    else
        cnt.x = 0;
    end
    if (ay(i+1)< window && ay(i+1) > -window)
        cnt.y = cnt.y + 1;
        if cnt.y > maxCnt
            vy(i+1) = 0;
        end
     else
        cnt.y = 0;
    end
    if (az(i+1)< window && az(i+1) > -window)
        cnt.z = cnt.z + 1;
        if cnt.z > maxCnt
            vz(i+1) = 0;
        end
     else
        cnt.z = 0;
    end
end

% integrate velocities to get position
for i=1:length(ax)-1
    px(i+1) = px(i) + param.DT*(vx(i) + (vx(i+1) - vx(i))/2);
    py(i+1) = py(i) + param.DT*(vy(i) + (vy(i+1) - vy(i))/2);
    pz(i+1) = pz(i) + param.DT*(vz(i) + (vz(i+1) - vz(i))/2);
end

acc.x = ax;
acc.y = ay;
acc.z = az;
vel.x = vx;
vel.y = vy;
vel.z = vz;
pos.x = px;
pos.y = py;
pos.z = pz;

end

function gyro = filterGyro(sensor, accel)

global param;

% scale data
p = -sensor.gx*param.gyro_scale;
q = -sensor.gy*param.gyro_scale;
r =  sensor.gz*param.gyro_scale;

% get bias
pbias = mean(p(100:600));
qbias = mean(q(100:600));
rbias = mean(r(100:600));

% subtract bias
p = p - pbias;
q = q - qbias;
r = r - rbias;

qw = zeros(size(p));
qx = qw;
qy = qw;
qz = qw;
qw(1) = 1;

for i=1:length(p)-1
    if mod(i,25) == 0 % 50 Hz
        a.x = accel.x(i);
        a.y = accel.y(i);
        a.z = accel.z(i);
        att = accelCorrection([qw(i) qx(i) qy(i) qz(i)],a);
        if length(att) == 4
            qw(i) = att(1); qx(i) = att(2); qy(i) = att(3); qz(i) = att(4);
        end
    end
    qw(i+1) = qw(i) - param.DT/2*(qx(i)*p(i) + qy(i)*q(i) + qz(i)*r(i));
    qx(i+1) = qx(i) + param.DT/2*(qw(i)*p(i) - qz(i)*q(i) + qy(i)*r(i));
    qy(i+1) = qy(i) + param.DT/2*(qz(i)*p(i) + qw(i)*q(i) - qx(i)*r(i));
    qz(i+1) = qz(i) + param.DT/2*(qx(i)*q(i) - qy(i)*p(i) + qw(i)*r(i));
    
    
end

gyro.p = p;
gyro.q = q;
gyro.r = r;
gyro.qw = qw;
gyro.qx = qx;
gyro.qy = qy;
gyro.qz = qz;

end

function [sensor, truth] = getData(file)

global param

log = importdata(file,',',1);

%% Choose data to use
% up
% t_start = 15;
% t_end = 35;

% down
% t_start = 30;
% t_end = 45;
%
% time = log.data(:,1);
% time = time - time(1);
% index = find(time > t_start & time < t_end);
% log.data = log.data(index,:);

% Time
col = 1;
sensor.rost = log.data(:,col); col = col+1;
sensor.rost = sensor.rost - sensor.rost(1);
sensor.t = 0:1/500:(length(sensor.rost)-1)/500;

% Sensor Data - 500 Hz
sensor.gx = log.data(:,col); col=col+1;
sensor.gy = log.data(:,col); col=col+1;
sensor.gz = log.data(:,col); col=col+1;

sensor.ax = log.data(:,col); col=col+1;
sensor.ay = log.data(:,col); col=col+1;
sensor.az = log.data(:,col); col=col+1;

% Magnetometer Data - 50 Hz
sensor.mx = log.data(:,col); col=col+1;
sensor.my = log.data(:,col); col=col+1;
sensor.mz = log.data(:,col); col=col+1;
% get when magnetometer changes
magx = sensor.mx(1);
i = 1;
while magx == sensor.mx(i)
    i = i+1;
end
index = i:10:length(sensor.mx);
sensor.mx = sensor.mx(index);
sensor.my = sensor.my(index);
sensor.mz = sensor.mz(index);
sensor.tmag = sensor.t(index);

% Pressure Data ~13.2 Hz
sensor.temperature = log.data(:,col); col=col+1;
sensor.pressure = log.data(:,col); col=col+1;
% p = sensor.pressure(1);
% i = 1;
% index = 1;
% while i ~= length(sensor.pressure)
%     p = sensor.pressure(i);
%     while p == sensor.pressure(i)
%         i = i+1;
%     end
%     index = [index i];
% end


sensor.sonar = log.data(:,col); col=col+1;

% Vicon truth data
truth.x = log.data(:,col); col=col+1;
truth.y = log.data(:,col); col=col+1;
truth.z = log.data(:,col); col=col+1;

truth.qw = log.data(:,col); col=col+1;
truth.qx = log.data(:,col); col=col+1;
truth.qy = log.data(:,col); col=col+1;
truth.qz = log.data(:,col); col=col+1;

truth.dx = log.data(:,col); col=col+1;
truth.dy = log.data(:,col); col=col+1;
truth.dz = log.data(:,col); col=col+1;

truth.p = log.data(:,col); col=col+1;
truth.q = log.data(:,col); col=col+1;
truth.r = log.data(:,col);

[truth.yaw truth.pitch truth.roll] = quat2angle([truth.qw truth.qx truth.qy truth.qz]);

% numerically diff vicon velocity to get accel estimates
truth.d2x = derivative_cwt(truth.dx','gaus1',40,param.DT,1);
truth.d2y = derivative_cwt(truth.dy','gaus1',40,param.DT,1);
truth.d2z = derivative_cwt(truth.dz','gaus1',40,param.DT,1);

end

