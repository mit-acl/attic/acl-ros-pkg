function plotquadrotor(xx)
%
% modified 10/16/02 - Randy Beard
%          10/26/07 - RB
%          11/12/07 - RB
%          11/20/07 - RB
%          11/30/07 - RB
%          12/5/07  - RB
%          11/16/11 - MC - modified for plotting vicon data from ROS


persistent init fig_quadrotor;

if isempty(init)
    init = 0;
end


% process inputs
x = xx(1);
y = xx(2);
z = xx(3);
Q = [xx(4) xx(5) xx(6) xx(7)];

R = quat2dcm(Q);

if init==0,
    % initialize the plot
    init = 1;
    a = get(0,'Children');
    if(isempty(a))
        figure(1);
    else
        set(0, 'CurrentFigure', 1);
    end
    figure(1), clf
    
    % plot quadrotor
    axis([-3,9,-3,3,-.1, 2]);
    %     axis([-1.5,1.5,-1.5,1.5,-1, 1]);
    %     axis([-10,10,-10,10,-.1, 10]);
    fig_quadrotor = quadrotorPlot(x, y, z, R, [], 'normal');
    
    title('Attitude (roll, pitch, yaw)')
    xlabel('X')
    ylabel('Y')
    zlabel('Z')
    view(117,41)
    %    view(0,90)  % top down view to check heading hold loop
    %    view(0,0)   % side view to check altitude hold loop
    %    view(90,0)   % side view - facing y
    grid on
    
    
    
else  % do this at every time step
    
    % plot quadrotor
    quadrotorPlot(x, y, z,R, fig_quadrotor);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function handle = quadrotorPlot(x, y, z,R, handle, mode)
  % plot of quadrotor in contraption - rolling motion
  
  %----quadrotor vertices------
  P = defineParameters;
  [Vert_quad, Face_quad, colors_quad]       = quadrotorVertFace(P);
  % rotate vertices by phi, theta, psi
  Vert_quad = rotateVert(Vert_quad, R);
%     Vert_quad = rotateXYZ(Vert_quad, phi, theta, psi);
  % transform vertices from NED to XYZ
  Rxyz = [...
      0, 1, 0;...
      1, 0, 0;...
      0, 0, -1;...
      ];
  Vert_quad = Vert_quad*Rxyz;
  % translate vertices in XYZ
  Vert_quad = translateVert(Vert_quad, [x; y; z]);

 
  % collect all vertices and faces
  V = Vert_quad;
  F = Face_quad;
  patchcolors = colors_quad;


  if isempty(handle),
      handle = patch('Vertices', V, 'Faces', F,...
                     'FaceVertexCData',patchcolors,...
                     'FaceColor','flat',...
                     'EraseMode', mode);
  else
    set(handle,'Vertices',V,'Faces',F);
    drawnow
  end
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% define the quadrotor parameters
function P = defineParameters

% parameters
P.r = 0.1;   % radius of the rotor
P.l = 0.1;   % length of connecting rods
P.lw = 0.01; % width of rod
P.w = 0.1;   % width of the center pod
P.w_rail = 0.01; % width of the rail
P.l_rail = 5; % length of the rail
P.N = 10;     % number of points defining rotor

% define colors for faces
P.myred = [1, 0, 0];
P.mygreen = [0, 1, 0];
P.myblue = [0, 0, 1];
P.myyellow = [1,1,0];

%%%%%%%%%%%%%%%%%%%%%%%
function Vert=rotateVert(Vert,R)

  % rotate vertices
  Vert = Vert*R;
  
% end rotateVert

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% translate vertices by column vector T
function Vert = translateVert(Vert, T)

  Vert = Vert + repmat(T', size(Vert,1),1);

% end translateVert


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Vert_quad, Face_quad, colors_quad] = quadrotorVertFace(P)
% Vertices = [x,y,z] position of each vertex
% Faces = defines how the vertices are connnected for form faces.  Each set
% of our vertices defines one face.


%--------- vertices and faces for center pod ------------
% vertices of the center pod
Vert_center = [...
    P.w/2, P.w/2, P.w/2;...
    P.w/2, P.w/2, -P.w/2;...
    P.w/2, -P.w/2, -P.w/2;...
    P.w/2, -P.w/2, P.w/2;...
    -P.w/2, P.w/2, P.w/2;...
    -P.w/2, P.w/2, -P.w/2;...
    -P.w/2, -P.w/2, -P.w/2;...
    -P.w/2, -P.w/2, P.w/2;...
    ];
% define faces of center pod
Face_center = [...
        1, 2, 3, 4;... % front
        5, 6, 7, 8;... % back
        1, 4, 8, 5;... % top
        8, 4, 3, 7;... % right 
        1, 2, 6, 5;... % left
        2, 3, 7, 6;... % bottom
        ];
    
%--------- vertices and faces for connecting rods ------------    
% vertices for front rod
Vert_rod_front = [...
    P.w/2, P.lw/2, 0;...
    P.w/2, -P.lw/2, 0;...
    P.w/2, 0, P.lw/2;...
    P.w/2, 0, -P.lw/2;...
    P.l+P.w/2, P.lw/2, 0;...
    P.l+P.w/2, -P.lw/2, 0;...
    P.l+P.w/2, 0, P.lw/2;...
    P.l+P.w/2, 0, -P.lw/2;...
    ];
Face_rod_front = 8 + [...
        1, 2, 6, 5;... % x-y face
        3, 4, 8, 7;... % x-z face
    ];
% vertices for right rod
Vert_rod_right = Vert_rod_front*[0,1,0;1,0,0;0,0,1];
Face_rod_right = 16 + [...
        1, 2, 6, 5;... 
        3, 4, 8, 7;... 
    ];
% vertices for back rod
Vert_rod_back = Vert_rod_front*[-1,0,0;0,-1,0;0,0,1];
Face_rod_back = 24 + [...
        1, 2, 6, 5;... 
        3, 4, 8, 7;... 
    ];
% vertices for left rod
Vert_rod_left = Vert_rod_front*[0,-1,0;-1,0,0;0,0,1];
Face_rod_left = 32 + [...
        1, 2, 6, 5;... 
        3, 4, 8, 7;... 
    ];

%--------- vertices and faces for rotors ------------  
Vert_rotor = [];
for i=1:P.N,
    Vert_rotor = [Vert_rotor; P.r*cos(2*pi*i/P.N), P.r*sin(2*pi*i/P.N), 0];
end
for i=1:P.N,
    Vert_rotor = [Vert_rotor; P.r/10*cos(2*pi*i/P.N), P.r/10*sin(2*pi*i/P.N), 0];
end
Face_rotor = [];
for i=1:P.N-1,
    Face_rotor = [Face_rotor; i, i+1, P.N+i+1, P.N+i];
end
Face_rotor = [Face_rotor; P.N, 1, P.N+1, 2*P.N];

% front rotor
Vert_rotor_front = Vert_rotor + repmat([P.w/2+P.l+P.r, 0, 0],2*P.N,1);
Face_rotor_front = 40 + Face_rotor;
% right rotor
Vert_rotor_right = Vert_rotor + repmat([0, P.w/2+P.l+P.r, 0],2*P.N,1);
Face_rotor_right = 40 + 2*P.N + Face_rotor;
% back rotor
Vert_rotor_back = Vert_rotor + repmat([-(P.w/2+P.l+P.r), 0, 0],2*P.N,1);
Face_rotor_back = 40 + 2*P.N + 2*P.N + Face_rotor;
% left rotor
Vert_rotor_left = Vert_rotor + repmat([0, -(P.w/2+P.l+P.r), 0],2*P.N,1);
Face_rotor_left = 40 + 2*P.N + 2*P.N + 2*P.N + Face_rotor;

% collect all of the vertices for the quadrotor into one matrix
Vert_quad = [...
    Vert_center; Vert_rod_front; Vert_rod_right; Vert_rod_back;...
    Vert_rod_left; Vert_rotor_front; Vert_rotor_right;...
    Vert_rotor_back; Vert_rotor_left...
    ];
% collect all of the faces for the quadrotor into one matrix
Face_quad = [...
    Face_center; Face_rod_front; Face_rod_right; Face_rod_back;...
    Face_rod_left; Face_rotor_front; Face_rotor_right;...
    Face_rotor_back; Face_rotor_left...
    ];

myred = [1, 0, 0];
mygreen = [0, 1, 0];
myblue = [0, 0, 1];
myyellow = [1,1,0];

colors_quad = [...
    mygreen;... % fuselage front
    myblue;... % back
    myyellow;... % top
    myblue;... % right
    myblue;... % left
    myblue;... % bottom
    mygreen;... % rod front
    mygreen;... %
    mygreen;... % rod right
    mygreen;... %
    mygreen;... % rod back
    mygreen;... %
    mygreen;... % rod left
    mygreen;... %
    ];
for i=1:P.N,
    colors_quad = [colors_quad; mygreen];   % front rotor
end
for i=1:P.N,
    colors_quad = [colors_quad; myblue];  % right rotor
end
for i=1:P.N,
    colors_quad = [colors_quad; myblue];  % left rotor
end
for i=1:P.N,
    colors_quad = [colors_quad; myblue];  % back rotor
end

% end quadrotorVertFace


