%% rl_fly_quad.m
%% 
%% Fly inverse reinforcement learning algorithm waypoints
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc; clear all;

%% User Params
waypointTimes = load('waypoints.txt'); % file with waypoints and associated times, Format: x1 y1 z1 0
%                                                                                      x2 y2 z2 t1
%                                                                                      x3 y3 z3 t2 ...
% C.name = 'mQ01'; % quadrotor name
C.name = 'BQ04';
fly_quad = 1;    % 1 - fly quad, 0 - simulate trajectory


%% generate commands 
C.wait_time = 0.1;
C.num_iter = 1;

% parse file
x = waypointTimes(:,1); x = x - x(1);
y = waypointTimes(:,2); y = y - y(1);
z = waypointTimes(:,3); z = z - z(1);
t = waypointTimes(:,4);
    
[t,index] = sort(t,1,'ascend');
x = x(index);
y = y(index);
z = z(index);

wp = [x';...
      y';...
      z']; % waypoints
wpm_acc = [0; 0; -9.81]; % middle acceleration constraint
wpm_elm = []; % wp element that acceleration constraint will replace
sigma = ones(size(diff(t)));
tt = diff(t)';
n = 10; % ninth order polynomial
wp0 = zeros(3,n/2-1); % initial x vel, acc, jerk
wpf = zeros(3,n/2-1); % final x vel, acc, jerk

addpath('../../simulation/matlabSim');
for i=1:size(wp,2)-1
    [C.patheval(i), C.path(i)] = polygen_acc_const2(wp(:,i:i+1),wp0,wpf,wpm_acc,wpm_elm,sigma(i),n,tt(i),100,0); % compute the desired path
end

%% send commands
if fly_quad
    send_goal(C);
else
    for i=1:size(wp,2)-1
        [C.patheval(i), C.path(i)] = polygen_acc_const2(wp(:,i:i+1),wp0,wpf,wpm_acc,wpm_elm,sigma(i),n,tt(i),100,0); % compute the desired path
    end
    figure(1);clf;
    hold all
    for i=1:size(wp,2)-1
    plot3(C.patheval(i).x,C.patheval(i).y,C.patheval(i).z,'LineWidth',2.5);
    hold all
    end
    plot3(wp(1,:),wp(2,:),wp(3,:),'r*','MarkerSize',15);
    xlabel('X Position (m)','fontsize',14);
    ylabel('Y Position (m)','fontsize',14);
    zlabel('Z Position (m)','fontsize',14);
    view(-90,0);
    legend('Path','Waypoints','Location','Best');
end
