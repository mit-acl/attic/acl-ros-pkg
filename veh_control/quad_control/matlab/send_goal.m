function status = send_goal(C)
%SEND_GOAL - Create a reference trajectory for the uberquad - send to c++
% 
% Inputs:
%    C - Struct containing output of polynomial path generation
% 
% Outputs:
%    status - 1 if ran successfully, 0 otherwise
% 
% Other m-files required: path_gen.m, compute_poly_paths_cf.m
% Subfunctions: non
% MAT-files required: none
%
 
% Author: Mark Cutler
% Massachusetts Institute of Technology, AeroAstro
% email address: markjcutler@gmail.com
% November 2011; Last revision: 21-November-2011
 
%------------- BEGIN CODE --------------
 
status = 0;

% add the ipc_bridge_matlab binaries to your path
% addpath('/home/swarm/ros_workspace/ipc_msgs/ipc_geometry_msgs/bin');
% addpath('/home/mark/ros_workspace/ipc_msgs/ipc_geometry_msgs/bin');
addpath('/home/swarm/fuerte_workspace/ipc_msgs/ipc_geometry_msgs/bin');

% create an empty geometry_msgs/PoseArray message structure for sending the
% goal positions
goal_msg = geometry_msgs_PoseArray('empty');

% WARNING - abuse of message structure - using orientation field for
% velocities.  The goalx orientation field is velocity.  The goald2x
% orientation field is jerk.
% TODO - make custom message fields for this application
goalx = geometry_msgs_Pose('empty');    % position & velocity
goald2x = geometry_msgs_Pose('empty');   % acceleration & jerk

if ~isfield(C, 'useAdaptive')
    C.useAdaptive = 0;
end

% Subscribe to goal data
if C.useAdaptive
    pid=geometry_msgs_PoseArray('connect','subscriber','example_module1',strcat(C.name,'_state'));
else
    pid=geometry_msgs_PoseStamped('connect','subscriber','example_module1',strcat(C.name,'_goal'));
end

pid3=geometry_msgs_PoseArray('connect','publisher','example_module',strcat(C.name,'_matlab'));

% get initial position
if C.useAdaptive
    start_msg = geometry_msgs_PoseArray('read',pid,100);
else
    start_msg = geometry_msgs_PoseStamped('read',pid,100);
end
% geometry_msgs_PoseStamped('disconnect',pid);

cnt = 0;
while (isempty(start_msg))
    if C.useAdaptive
        start_msg = geometry_msgs_PoseArray('read',pid,100);
    else
        start_msg = geometry_msgs_PoseStamped('read',pid,100);
    end
    display('not connecting');
    cnt = cnt + 1;
    if cnt > 10
        return;
    end
end

% check for valid connection with ROS
if (isempty(start_msg))
    display('Connection with ROS not valid');
    return;
end

if C.useAdaptive
    state.x = start_msg.poses{1}.position.x;
    state.y = start_msg.poses{1}.position.y;
    state.z = start_msg.poses{1}.position.z;
else
    state.x = start_msg.pose.position.x;
    state.y = start_msg.pose.position.y;
    state.z = start_msg.pose.position.z;
end

% NOTE: for any header field, the frame_id field must be filled or you will
% crash matlab!

% Get ros start time
time_start = start_msg.header.stamp;

count = 1;
iteration = 1;

% Main loop
while (1)
    
    if C.useAdaptive
        current_pose = geometry_msgs_PoseArray('read',pid,100);
    else
        current_pose = geometry_msgs_PoseStamped('read',pid,100);
    end

    time = current_pose.header.stamp - time_start; % ros time
    
    % get goal
    goal = path_gen(time,state,C);
    
    if (C.useAdaptive == 1 || C.useAdaptive == 2)
        [acc_x_ad, acc_y_ad, acc_z_ad] = RBF_Concurrent_Output(goal, current_pose, ...
            C.useAdaptive, iteration);
    elseif (C.useAdaptive >= 3 && C.useAdaptive <= 5)
        [acc_x_ad, acc_y_ad, ~] = RBF_BKRCL_Output(goal, current_pose, ...
           C.useAdaptive, iteration);
        [~, ~, acc_z_ad] = RBF_Concurrent_Output(goal, current_pose, ...
            C.useAdaptive, iteration);
    else
        acc_x_ad = 0;
        acc_y_ad = 0;
        acc_z_ad = 0;
    end

    % fill goal messages
    % position
    goalx.position.x = goal.x;
    goalx.position.y = goal.y;
    goalx.position.z = goal.z;
    
    % velocity
    goalx.orientation.x = goal.dx;
    goalx.orientation.y = goal.dy;
    goalx.orientation.z = goal.dz;
    
    % acceleration
    goald2x.position.x = goal.d2x - acc_x_ad;
    goald2x.position.y = goal.d2y - acc_y_ad;
    goald2x.position.z = goal.d2z - acc_z_ad;
    
    % jerk
    goald2x.orientation.x = goal.d3x;
    goald2x.orientation.y = goal.d3y;
    goald2x.orientation.z = goal.d3z;
    
    % Use adaptive controller or not? The answer is either 1 or 0
    goald2x.orientation.w = (C.useAdaptive > 0);
    
    if strcmp(C.name,'UQ02')
        goald2x.orientation.w = goal.useinfo;
    end
    
    % should this path be upside down or not?
    goalx.orientation.w = goal.upright;
    
    % Send goal message
    goal_msg.poses{1} = goalx;
    goal_msg.poses{2} = goald2x;
%     pid3=geometry_msgs_PoseArray('connect','publisher','example_module',strcat(C.name,'_matlab'));
    geometry_msgs_PoseArray('send',pid3,goal_msg);
%     geometry_msgs_PoseArray('disconnect',pid3);

    if ~goal.useinfo
%         count = count + 1;
%         if count > 20
%             display('finished sending command');
%             break;
%         end

        count = count + 1;
        if count > C.wait_time*100;
            display('finished iteration');
            iteration
            if C.useAdaptive
                current_pose = geometry_msgs_PoseArray('read',pid,100);
            else
                current_pose = geometry_msgs_PoseStamped('read',pid,100);
            end
            time_start = current_pose.header.stamp;
            count = 1;
            if (C.num_iter > 1 && ~C.useAdaptive)
                C.patheval = C.patheval2;
                C.path = C.path2;
            end
        %    state.x = goal.x; state.y = goal.y; state.z = goal.z;
                
            
            if iteration == C.num_iter % why not working?
                for i=1:5
                    goald2x.orientation.w = 0; % go back to PID control
                    goal_msg.poses{1} = goalx;
                    goal_msg.poses{2} = goald2x;
                    geometry_msgs_PoseArray('send',pid3,goal_msg);
                end
                break;
            end
            iteration = iteration + 1;
        end
    end 
end

if C.useAdaptive
    geometry_msgs_PoseArray('disconnect',pid);
else
    geometry_msgs_PoseStamped('disconnect',pid);
end
geometry_msgs_PoseArray('disconnect',pid3);
status = 1;  % ran successfully

 
%------------- END OF CODE --------------

