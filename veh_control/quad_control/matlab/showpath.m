function showpath(handles)

% clc; clear all;
% figure(1); clf;

%% plot RAVEN

% outside
xr1 = [-3 -3 -3 -3 -3 7 7 7 7 7 7 -3];
yr1 = [3 3 -3 -3 -3 -3 -3 -3 3 3 3  3];
zr1 = [3 0 0 3 0 0 3 0 0 3 0 0];

% origin
xr2 = [-.5 .5 0 0 0];
yr2 = [0 0 0 -.5 .5];
zr2 = [0 0 0 0 0];

plot3(handles.axes1,xr1,yr1,zr1,'k')
hold on
plot3(handles.axes1,xr2,yr2,zr2,'k')
axis('equal')
view(-18,14);
grid('on');

n = 10; % ninth order polynomial
wp0 = zeros(3,n/2-1); % initial x vel, acc, jerk
wpf = zeros(3,n/2-1); % final x vel, acc, jerk
% generic path for youtube video1
wp = [0 1  2 3 0;...
      0 1 -1 0 0;...
      0 0  0 0 0];
wpm_acc = [0;0;0];
wpm_elm = [];
sigma = ones(1,size(wp,2));
tt = 1.4*[1.1574    0.7080    0.7895    1.3452];
[T, C] = polygen_acc_const2(wp,wp0,wpf,wpm_acc,wpm_elm,sigma,n,tt,100,0); % compute the desired path

% plot
plot3(handles.axes1,T.x,T.y,T.z,'LineWidth',2.5);
hold on
plot3(handles.axes1,wp(1,:),wp(2,:),wp(3,:),'r*','MarkerSize',15);
xlabel('X Position (m)','fontsize',14);
ylabel('Y Position (m)','fontsize',14);
zlabel('Z Position (m)','fontsize',14);
legend('Path','Waypoints','Location','Best');
