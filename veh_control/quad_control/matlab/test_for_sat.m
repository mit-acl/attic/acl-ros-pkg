function force = test_for_sat(path,showfigs)
% TEST_FOR_SAT - run control algorithm on proposed path to check for
% saturations
%
% Other m-files required: compute_poly_paths_cf.m
% Subfunctions: none
% MAT-files required: none
%
% See also: compute_poly_paths_cf

% Author: Mark Cutler
% Massachusetts Institute of Technology, AeroAstro
% email address: markjcutler@gmail.com
% October 2011; Last revision: 5-October-2011

%------------- BEGIN CODE --------------

C.m = 0.64;
C.g = 9.81;
C.J = [0.00417289488 0 0;...
    0 0.00417289488 0;...
    0 0 0.00819861586];
C.L = 0.2;  % length of quad arm

% initialize output variables
force.f = zeros(size(path.t));
force.f1 = zeros(size(force.f));
force.f2 = zeros(size(force.f));
force.f3 = zeros(size(force.f));
force.f4 = zeros(size(force.f));
force.M  = zeros(3,length(force.f));
roll = zeros(size(force.f));
pitch = zeros(size(force.f));
yaw = zeros(size(force.f));
P = zeros(size(force.f));
Q = zeros(size(force.f));
R = zeros(size(force.f));
Pdot = zeros(size(force.f));
Qdot = zeros(size(force.f));
Rdot = zeros(size(force.f));
debugout = zeros(3,length(force.f));

for i=1:length(path.t)
    
    goal.d2x = path.d2x(i);
    goal.d2y = path.d2y(i);
    goal.d2z = path.d2z(i);
    goal.d3x = path.d3x(i);
    goal.d3y = path.d3y(i);
    goal.d3z = path.d3z(i);
    goal.d4x = path.d4x(i);
    goal.d4y = path.d4y(i);
    goal.d4z = path.d4z(i);
    goal.t   = path.t(i);
    goal.upright = path.upright(i);
    
    out = ol_control(goal,C);
    force.M(:,i) = out.M;
    force.f(i) = out.F;
    force.f1(i) = out.f1;
    force.f2(i) = out.f2;
    force.f3(i) = out.f3;
    force.f4(i) = out.f4;
    
    [yaw(i) pitch(i) roll(i)] = quat2angle(out.qmin);
    
    P(i) = out.p;
    Q(i) = out.q;
    R(i) = out.r;
    Pdot(i) = out.dp;
    Qdot(i) = out.dq;
    Rdot(i) = out.dr;
    
end
%
% % Pfit = polyfit(path.t,P,10);
% % dPfit = polyder(Pfit);
% % dPfit = polyfit(path.t,Pdot,30);
% % pfit = polyint(dPfit);
% % hold all;
% % plot(path.t,180/pi*polyval(pfit,path.t));
%
% dp     = 1/C.J(1,1)*((C.J(2,2)-C.J(3,3))*Q.*R + force.M(1,:));
% dq     = 1/C.J(2,2)*((C.J(3,3)-C.J(1,1))*P.*R + force.M(2,:));
% dr     = 1/C.J(3,3)*((C.J(1,1)-C.J(2,2))*P.*Q + force.M(3,:));

if showfigs

    
    %% test output to see if it matches up
    x0 = [0 0 0 0 0 0 1 0 0 0 0 0 0]';
    % x0 = [0 0 0 0 0 0 1 0 0 0]';
    [tout xout] = ode45(@quad_dynamics,path.t,x0);
    test.x = xout(:,2);
    test.y = xout(:,1);
    test.z = -xout(:,3);
    test.u = xout(:,4);
    test.v = xout(:,5);
    test.w = xout(:,6);
    test.q0 = xout(:,7);
    test.qx = xout(:,8);
    test.qy = xout(:,9);
    test.qz = xout(:,10);
    test.p = xout(:,11);
    test.q = xout(:,12);
    test.r = xout(:,13);
    % test.p = P;
    % test.q = Q;
    % test.r = R;
    [test.yaw, test.pitch, test.roll] = quat2angle([test.q0 test.qx test.qy test.qz]);
    
%     figure(2);
%     hold all;
%     plot3(test.x,test.y,test.z);
%     xlabel('X'); ylabel('Y'); zlabel('Z');
    
    % figure(6); clf;
    %
    % subplot(211)
    % plot(tout,test.roll*180/pi,tout,test.pitch*180/pi,tout,test.yaw*180/pi);
    % legend('roll','pitch','yaw','Location','Best');
    % ylabel('Angles (deg)');
    %
    % subplot(212)
    % plot(tout,test.p*180/pi,tout,test.q*180/pi,tout,test.r*180/pi);
    % legend('p','q','r');
    % ylabel('Rates (deg)');
    % xlabel('time (s)');
    
    % plot for 2012-gnc-uber
    figure(4); clf;
    fontsize = 15;
    plot(path.t,force.f1,path.t,force.f2,path.t,force.f3,path.t,force.f4,'LineWidth',2.5);
    ylabel('Force (N)','FontSize',15);
    xlabel('Time (s)','FontSize',15);
    h = legend('f1','f2','f3','f4','Location','Best');
    set(h,'FontSize',fontsize,'Location','SouthEast');
    set(gca,'FontSize',fontsize);
    
    figure(3); clf;
    subplot(4,1,1);
    plot(path.t,force.f);
    ylabel('Force (N)');
    
    subplot(4,1,2);
    plot(path.t,roll*180/pi,path.t,pitch*180/pi,path.t,yaw*180/pi);%,path.t,180/pi*polyval(rollfit,path.t));
    hold all
%     plot(tout,test.roll*180/pi,tout,test.pitch*180/pi,tout,test.yaw*180/pi);
    ylabel('Flight angles (deg)');
    legend('roll','pitch','yaw','roll test','pitch test','yaw test','Location','Best');
    
    subplot(4,1,3);
    plot(path.t,P*180/pi,path.t,Q*180/pi,path.t,R*180/pi);%,path.t,180/pi*polyval(Pfit,path.t));
    hold all
%     plot(tout,test.p*180/pi,tout,test.q*180/pi,tout,test.r*180/pi);
    xlabel('Time (s)');
    ylabel('Rates (deg/s)')
    
    
    subplot(4,1,4);
    plot(path.t,Pdot*180/pi,path.t,Qdot*180/pi,path.t,Rdot*180/pi);
    xlabel('Time (s)');
    ylabel('Acceleration (deg/s^2)')
    
%     %% pretty plot
%     % plot for paper 2012-gnc-uber
%     fontsize = 15;
%     figure(5); clf
%     subplot(3,2,1);
%     plot(path.t,path.x,path.t,path.y,path.t,path.z,'LineWidth',3);
%     ylabel('Position (m)','FontSize',fontsize);
%     h = legend('X','Y','Z');
%     set(h,'FontSize',fontsize,'Location','NorthWest');
%     set(gca,'FontSize',fontsize);
% 
%     subplot(3,2,3);
%     plot(path.t,path.dx,path.t,path.dy,path.t,path.dz,'LineWidth',3);
%     ylabel('Velocity (m/s)','FontSize',fontsize);
%     set(gca,'FontSize',fontsize);
%     
%     subplot(3,2,5);
%     plot(path.t,path.d2x,path.t,path.d2y,path.t,path.d2z,'LineWidth',3);
%     ylabel('Acceleration (m/s^2)','FontSize',fontsize);
%     xlabel('Time (s)','FontSize',fontsize);
%     set(gca,'FontSize',fontsize);
%     
%     subplot(3,2,2);
%     plot(path.t,force.f,'LineWidth',3);
%     ylabel('Force (N)','FontSize',fontsize);
%     set(gca,'FontSize',fontsize);
%     
%     subplot(3,2,4);
%     plot(path.t,roll*180/pi,'LineWidth',3);
%     ylabel('Roll angle (deg)','FontSize',fontsize);
%     set(gca,'FontSize',fontsize);
%     
%     subplot(3,2,6);
%     plot(path.t,P*180/pi,'LineWidth',3);
%     xlabel('Time (s)','FontSize',fontsize);
%     ylabel('Roll Rate (deg/s)','FontSize',fontsize);
%     set(gca,'FontSize',fontsize);
    
%     savefig('flip_traj','pdf');
end

    function xxdot = quad_dynamics(t,xx)
        
        % process states
        x_u  = xx(4);
        x_v  = xx(5);
        x_w  = xx(6);
        x_q0 = xx(7);
        x_qx = xx(8);
        x_qy = xx(9);
        x_qz = xx(10);
        x_p  = xx(11);
        x_q  = xx(12);
        x_r  = xx(13);
        
        x_Q = [x_q0 x_qx x_qy x_qz];
        
        % process inputs
        idx = find(path.t >= t);
        idx = idx(1);
        % linear interpolation
        if path.t(idx) ~= t
            tm = path.t(idx-1);
            tp = path.t(idx);
            Fm = force.f(idx-1);
            Fp = force.f(idx);
            Mm = force.M(:,idx-1);
            Mp = force.M(:,idx);
            x_F = (Fp-Fm)*(t-tm)/(tp-tm) + Fm;
            x_M = (Mp-Mm)*(t-tm)/(tp-tm) + Mm;
        else
            x_F = force.f(idx); %total motor thrust (N)
            x_M = force.M(:,idx); %Motor moment (Nm)
        end
        
        % compute system dynamics
        posdot = quatmultiply(x_Q, quatmultiply([0 x_u x_v x_w], quatconj(x_Q)));
        udot     = C.g*2*(x_qx*x_qz - x_qy*x_q0) + x_r*x_v-x_q*x_w;
        vdot     = C.g*2*(x_qy*x_qz + x_qx*x_q0) + x_p*x_w-x_r*x_u;
        wdot     = C.g*(x_qz^2 + x_q0^2 - x_qx^2 - x_qy^2) - 1/C.m*x_F + x_q*x_u-x_p*x_v;
        q0dot    = 0.5*(-x_qx*x_p - x_qy*x_q - x_qz*x_r);
        qxdot    = 0.5*( x_q0*x_p - x_qz*x_q + x_qy*x_r);
        qydot    = 0.5*( x_qz*x_p + x_q0*x_q - x_qx*x_r);
        qzdot    = 0.5*(-x_qy*x_p + x_qx*x_q + x_q0*x_r);
        pdot     = 1/C.J(1,1)*((C.J(2,2)-C.J(3,3))*x_q*x_r + x_M(1));
        qdot     = 1/C.J(2,2)*((C.J(3,3)-C.J(1,1))*x_p*x_r + x_M(2));
        rdot     = 1/C.J(3,3)*((C.J(1,1)-C.J(2,2))*x_p*x_q + x_M(3));
        
        xxdot = [posdot(2);posdot(3);posdot(4); udot;vdot;wdot; q0dot;qxdot;qydot;qzdot; ...
            pdot;qdot;rdot];
        
    end

    function xxdot = quad_dynamics2(t,xx)
        
        % process states
        x_u  = xx(4);
        x_v  = xx(5);
        x_w  = xx(6);
        x_q0 = xx(7);
        x_qx = xx(8);
        x_qy = xx(9);
        x_qz = xx(10);
        
        x_Q = [x_q0 x_qx x_qy x_qz];
        
        % process inputs
        idx = find(path.t >= t);
        idx = idx(1);
        % linear interpolation
        if path.t(idx) ~= t
            tm = path.t(idx-1);
            tp = path.t(idx);
            pm = P(idx-1);
            pp = P(idx);
            qm = Q(idx-1);
            qp = Q(idx);
            rm = R(idx-1);
            rp = R(idx);
            Fm = force.f(idx-1);
            Fp = force.f(idx);
            x_p = (pp-pm)*(t-tm)/(tp-tm) + pm;
            x_q = (qp-qm)*(t-tm)/(tp-tm) + qm;
            x_r = (rp-rm)*(t-tm)/(tp-tm) + rm;
            x_F = (Fp-Fm)*(t-tm)/(tp-tm) + Fm;
        else
            x_p = P(idx);
            x_q = Q(idx);
            x_r = R(idx);
            x_F = force.f(idx); %total motor thrust (N)
        end
        
        % compute system dynamics
        posdot = quatmultiply(x_Q, quatmultiply([0 x_u x_v x_w], quatconj(x_Q)));
        udot     = C.g*2*(x_qx*x_qz - x_qy*x_q0) + x_r*x_v-x_q*x_w;
        vdot     = C.g*2*(x_qy*x_qz + x_qx*x_q0) + x_p*x_w-x_r*x_u;
        wdot     = C.g*(x_qz^2 + x_q0^2 - x_qx^2 - x_qy^2) - 1/C.m*x_F + x_q*x_u-x_p*x_v;
        q0dot    = 0.5*(-x_qx*x_p - x_qy*x_q - x_qz*x_r);
        qxdot    = 0.5*( x_q0*x_p - x_qz*x_q + x_qy*x_r);
        qydot    = 0.5*( x_qz*x_p + x_q0*x_q - x_qx*x_r);
        qzdot    = 0.5*(-x_qy*x_p + x_qx*x_q + x_q0*x_r);
        
        xxdot = [posdot(2);posdot(3);posdot(4); udot;vdot;wdot; q0dot;qxdot;qydot;qzdot];
        
    end
end

%------------- END OF CODE --------------