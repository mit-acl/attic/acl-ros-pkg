#!/usr/bin/env python
# cartesian joystick waypoint control for quadrotor
import roslib
roslib.load_manifest('quad_control')
import rospy
import math
import copy
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose

# local imports
from quad_control.msg import quadCmd
from quad_control.msg import quadGoal
from aclpy import utils

NOT_FLYING = 0
FLYING = 1

TAKEOFF = 1
DISABLE = 2
RESET_INTEGRATORS = 4

LEFT_X = 0
LEFT_Y = 1
RIGHT_X = 3
RIGHT_Y = 4

A = 0
B = 1
X = 2
Y = 3
BACK = 6
START = 7

CONTROL_DT = 0.01
#MAX_ACCEL_XY = 2.0
#MAX_ACCEL_Z = 0.8


class QuadJoy:

    def __init__(self):

        self.status = NOT_FLYING
        self.transmitting = True
        self.wpType = DISABLE

        self.goal = quadGoal()
        self.goal.waypointType = DISABLE
        self.goal.upright = 1
        self.cmd = quadCmd()
        self.recent_goal = quadGoal()
        self.pose = Pose()
        self.pubGoal = rospy.Publisher('goal', quadGoal)
        self.pubCmd = rospy.Publisher('cmds', quadCmd)

    def poseCB(self, data):
        self.pose = data.pose

    def goalCB(self, data):
        self.recent_goal = data

    def sendGoal(self):
        self.goal.waypointType = self.wpType
        self.pubGoal.publish(self.goal)

    def sendCmd(self):
        self.pubCmd.publish(self.cmd)

    def joyCB(self, data):

        # takeoff
        if data.buttons[A] and self.status == NOT_FLYING:
            self.status = FLYING
            self.wpType = TAKEOFF

            # set initial goal to current pose
            self.goal.pos = copy.copy(self.pose.position)
            self.goal.vel.x = self.goal.vel.y = self.goal.vel.z = 0
            self.goal.yaw = utils.quat2yaw(self.pose.orientation)

        # emergency disable
        elif data.buttons[B] and self.status == FLYING:
            self.status = NOT_FLYING
            self.wpType = DISABLE

        elif self.status == FLYING:
            self.wpType = 0

            roll = -data.axes[RIGHT_X]/3.
            pitch = data.axes[RIGHT_Y]/3.
            yaw = 0
            throttle = data.axes[LEFT_Y]/3.

            self.cmd.pose.orientation.w = (math.cos(roll/2.0)*math.cos(pitch/2.0)*math.cos(yaw/2.0) + 
                                           math.sin(roll/2.0)*math.sin(pitch/2.0)*math.sin(yaw/2.0))
            self.cmd.pose.orientation.x = (math.sin(roll/2.0)*math.cos(pitch/2.0)*math.cos(yaw/2.0) - 
                                           math.cos(roll/2.0)*math.sin(pitch/2.0)*math.sin(yaw/2.0))
            self.cmd.pose.orientation.y = (math.cos(roll/2.0)*math.sin(pitch/2.0)*math.cos(yaw/2.0) + 
                                           math.sin(roll/2.0)*math.cos(pitch/2.0)*math.sin(yaw/2.0))
            self.cmd.pose.orientation.z = (math.cos(roll/2.0)*math.cos(pitch/2.0)*math.sin(yaw/2.0) - 
                                           math.sin(roll/2.0)*math.sin(pitch/2.0)*math.cos(yaw/2.0)) 

            self.cmd.throttle = throttle
            #self.cmd.AttCmd = 1

        # stop transmitting data from joystick
        if data.buttons[BACK] and self.transmitting:
            self.transmitting = False
            self.goal.vel.x = 0
            self.goal.vel.y = 0
            self.goal.vel.z = 0
            self.goal.dyaw = 0
            for i in range(3):
                self.sendGoal()

        # start transmitting data from joystick
        if data.buttons[START] and not self.transmitting:
            self.transmitting = True
            self.goal = self.recent_goal

        if self.transmitting:
            self.sendGoal()
            self.sendCmd()

def startNode():
    c = QuadJoy()
    rospy.Subscriber("/joy", Joy, c.joyCB)
    rospy.Subscriber("pose", PoseStamped, c.poseCB)
    rospy.Subscriber("goal", quadGoal, c.goalCB)
    rospy.spin()

if __name__ == '__main__':

    ns = rospy.get_namespace()
    try:
        rospy.init_node('quad_joy')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=mQ01 $ rosrun quad_control joy.py")
        else:
            print "Starting joystick teleop node for: " + ns
            startNode()
    except rospy.ROSInterruptException:
        pass
