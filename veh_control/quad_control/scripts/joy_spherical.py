#!/usr/bin/env python
# spherical control for quad on a string
import roslib; roslib.load_manifest('quad_control')
import rospy
import math
import numpy as np
from sensor_msgs.msg import Joy
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose

from quad_control.msg import quadGoal

BASEX = 0.065
BASEY = -2.3
BASEZ = 0.01

NOT_FLYING = 0
FLYING = 1

TAKEOFF = 1
DISABLE = 2
RESET_XYINTEGRATORS = 3
RESET_INTEGRATORS = 4

LEFT_X=0
LEFT_Y=1
RIGHT_X=3
RIGHT_Y=4

A=0
B=1
X=2
Y=3

class Spherical:

    def __init__(self):
        
        self.yawOffset = 0 #np.pi/2.0
        self.goalYaw = 0
        self.kIntGain = 0.1
        
        self.theta = 0.0
        self.phi = 0.0
        self.r = 0.0
        self.goalTheta = 0.0
        self.goalPhi = 0.0
        self.goalR = 0.0
        self.thetaInt = 0.0
        self.phiInt = 0.0
        
        self.status = NOT_FLYING
        self.wpType = NOT_FLYING
        
        self.pose = Pose()
        self.pubGoal = rospy.Publisher('goal', quadGoal)

    def poseCB(self, data):
        self.pose = data.pose
        
        # get pose in spherical coordinates
        self.r,self.theta,self.phi = cart2spher(self.pose.position.x, 
                           self.pose.position.y, 
                           self.pose.position.z)
        
        # get desired yaw based on actual position
        self.goalYaw = self.theta + self.yawOffset
        self.goalYaw = wrapPi(self.goalYaw)


    def sendGoal(self, event):
        goal = quadGoal()
        x,y,z = spher2cart(self.goalR, 
                           self.goalTheta+self.thetaInt, 
                           self.goalPhi + self.phiInt)
        goal.pos.x = x
        goal.pos.y = y
        goal.pos.z = z
        goal.yaw = self.goalYaw
        
        goal.upright = 1
        goal.waypointType = self.wpType
        self.pubGoal.publish(goal)

    def joyCB(self, data):

        # takeoff
        if data.buttons[A] and self.status == NOT_FLYING:
            self.status = FLYING
            self.wpType = TAKEOFF
            
            # set initial goal to current pose
            self.goalR = self.r
            self.goalTheta = self.theta
            self.goalPhi = self.phi
            
            # reset integrators
            self.thetaInt = 0
            self.phiInt = 0
        
        # emergency disable
        elif data.buttons[B] and self.status == FLYING:
            self.status = NOT_FLYING
            self.wpType = DISABLE
            
            # reset integrators
            self.thetaInt = 0
            self.phiInt = 0

        elif self.status == FLYING:
            self.wpType = RESET_INTEGRATORS # RESET_XYINTEGRATORS
            dTheta = data.axes[RIGHT_X] * .004 # right_x
            dPhi = -data.axes[RIGHT_Y] * .002 # right y
            dR = data.axes[LEFT_Y] * .003 # left y
    
            self.goalR += dR
            self.goalTheta += dTheta
            self.goalPhi += dPhi
            
            self.goalR = sat(self.goalR, 3.0, 0.0)
            self.goalTheta = wrap2Pi(self.goalTheta)
            self.goalPhi = sat(self.goalPhi, np.pi, 0.0)
            
            # get theta and phi error
            theta_err = self.goalTheta - self.theta
            phi_err = self.goalPhi - self.phi
            
            # integrate error and 
            self.thetaInt += self.kIntGain*theta_err*0.01
            self.phiInt += self.kIntGain*phi_err*0.01
            
            
    def printStatus(self,event):
        print "\nr: " + str(self.r)
        print "rDes: " + str(self.goalR)
        print "\ntheta: " + str(self.theta)
        print "thetaDes: " + str(self.goalTheta)
        print "\nphi: " + str(self.phi)
        print "phiDes: " + str(self.goalPhi)
        print
        print
        
        
def cart2spher(x,y,z):
    x -= BASEX
    y -= BASEY
    z -= BASEZ
    r = np.sqrt(x**2 + y**2 + z**2)
    theta = math.atan2(y,x)
    phi = 0
    if r > 0.01:
        phi = math.acos(z/r)
    
    theta = wrap2Pi(theta)
    phi = sat(phi, np.pi, 0.0)
    
    return r, theta, phi

def spher2cart(r,theta,phi):
    
    theta = wrap2Pi(theta)
    phi = sat(phi, np.pi, 0.0)   
    
    x = r*math.cos(theta)*math.sin(phi)
    y = r*math.sin(theta)*math.sin(phi)
    z = r*math.cos(phi)
    return x+BASEX,y+BASEY,z+BASEZ
    
def wrapPi(val):
    if val > np.pi:
        val -= 2.0*np.pi
    if val < -np.pi:
        val += 2.0*np.pi
    return val

def wrap2Pi(val):
    if val > 2.0*np.pi:
        val -= 2.0*np.pi
    if val < 0:
        val += 2.0*np.pi
    return val

def sat(val,high,low):
    if val > high:
        val = high
    if val < low:
        val = low
    return val;

def startSpherical():
    c = Spherical()
    rospy.Subscriber("/joy", Joy, c.joyCB)
    rospy.Subscriber("pose", PoseStamped, c.poseCB)
    rospy.Timer(rospy.Duration(1/100.0), c.sendGoal)
    rospy.Timer(rospy.Duration(1/5.0), c.printStatus)
    rospy.spin()

if __name__ == '__main__':
    
    ns = rospy.get_namespace()
    try:
        rospy.init_node('spherical')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is tyipcally accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=mQ01 $ rosrun quad_control spherical.py")
        else:
            print "Starting joystick teleop node for: " + ns
            startSpherical()
    except rospy.ROSInterruptException:
        pass
    
