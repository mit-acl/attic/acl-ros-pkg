#!/usr/bin/env python
import rospy
import roslib
import numpy as np
from acl_msgs.msg import QuadHealth
from acl_msgs.msg import QuadCmd
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy

class Cal():
	def __init__(self):
		self.record = False
		self.count = 1000
		self.i = 0
		self.alpha = 0.1
		self.cal = np.zeros(4)
		self.name = rospy.get_namespace()
		self.name = self.name.replace("/","")
		rospy.init_node('motor_cal')
		self.h_s = rospy.Subscriber("/"+self.name+"/health",QuadHealth,self.healthCB)
		self.joy_s = rospy.Subscriber("/joy",Joy,self.joyCB)
		self.filt_pub = rospy.Publisher("/"+self.name+"/filt",Float64MultiArray,queue_size=1)
		self.filt = Float64MultiArray()
		self.filt.data = np.zeros(4)
		rospy.loginfo(self.filt.data)
		rospy.loginfo("Press 'Y' button when ready...")


	def joyCB(self,data):
		if data.buttons[3] > 0 and not self.record:
			rospy.loginfo("Calibrating...")
			rospy.sleep(.5)
			self.record = True
			# self.joy_s.unregister()			


	def healthCB(self,data):

		self.filt.data[0] = self.alpha*data.motor_diff[0] + (1-self.alpha)*self.filt.data[0]
		self.filt.data[1] = self.alpha*data.motor_diff[1] + (1-self.alpha)*self.filt.data[1]
		self.filt.data[2] = self.alpha*data.motor_diff[2] + (1-self.alpha)*self.filt.data[2]
		self.filt.data[3] = self.alpha*data.motor_diff[3] + (1-self.alpha)*self.filt.data[3]
		self.filt_pub.publish(self.filt)

		if self.record and self.i < self.count:
			rospy.loginfo("Iteration: %s",self.i)
			self.cal += self.filt.data
			self.i += 1
		elif self.record:
			self.cal = self.cal/self.count
			if self.name[-1] != 's':
				self.saveFile()
			rospy.loginfo(self.cal)
			rospy.loginfo("### Done ###")
			rospy.loginfo("Repress 'Y' button when ready...")
			self.i = 0
			self.record = False
			# self.h_s.unregister()

	def saveFile(self):
			pkg_dir = roslib.packages.get_pkg_dir('quad_control')
			tfile = open(pkg_dir+"/scripts/"+self.name+"_trim_values.txt",'a')
			tfile.write("m1: %f  m2: %f  m3: %f  m4: %f\n" % (self.cal[0],self.cal[1], self.cal[2], self.cal[3]))
			tfile.close()

if __name__ == '__main__':
	Cal()
	rospy.spin()