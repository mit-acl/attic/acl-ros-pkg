#!/usr/bin/env python

import roslib
roslib.load_manifest('quad_control')
import numpy as np
import math
import matplotlib.pyplot as pl
from mpl_toolkits.mplot3d import Axes3D
import csv

def quat2angle(q0,q1,q2,q3):
    roll = (np.arctan2(2*(q0*q1+q2*q3), 1-2*(q1*q1+q2*q2)))
    pitch = (np.arcsin(2*(q0*q2-q3*q1)))
    yaw = (np.arctan2(2*(q0*q3+q1*q2), 1-2*(q2*q2+q3*q3)))

    return [roll, pitch, yaw]

pkg_dir = roslib.packages.get_pkg_dir('quad_control')
# data = open('../../../../../../Desktop/quadlog.txt','r')
data = open(pkg_dir + '/matlab/quadlog.txt')

csv_reader = csv.reader(data)

# Initialize arrays
t_c = []
x_c = []
y_c = []
z_c = []
q0_c = []
qx_c = []
qy_c = []
qz_c = []

dx_c = []
dy_c = []
dz_c = []
p_c = []
q_c = []
r_c = []
roll_c = []
pitch_c = []
yaw_c = []
d2x_c = []
d2y_c = []
d2z_c = []
d2x_fb_c = []
d2y_fb_c = []
d2z_fb_c = []
d3x_c = []
d3y_c = []
d3z_c = []
d3x_fb_c = []
d3y_fb_c = []
d3z_fb_c = []
int_x = []
int_y =[]
int_z = []

x_p = []
y_p = []
z_p = []
q0_p = []
qx_p = []
qy_p = []
qz_p = []

dx_t = []
dy_t = []
dz_t = []
p_t = []
q_t = []
r_t = []

collective_c = []
throttle_c = []
voltage = []
f_total_c = []

# Load data
for row in csv_reader:
    t_c.append(row[0])
    x_c.append(row[1])
    y_c.append(row[2])
    z_c.append(row[3])

    q0_c.append(row[4])
    qx_c.append(row[5])
    qy_c.append(row[6])
    qz_c.append(row[7])

    dx_c.append(row[8])
    dy_c.append(row[9])
    dz_c.append(row[10])

    p_c.append(row[11])
    q_c.append(row[12])
    r_c.append(row[13])

    roll_c.append(row[14])
    pitch_c.append(row[15])
    yaw_c.append(row[16])

    d2x_c.append(row[17])
    d2y_c.append(row[18])
    d2z_c.append(row[19])

    d2x_fb_c.append(row[20])
    d2y_fb_c.append(row[21])
    d2z_fb_c.append(row[22])

    d3x_c.append(row[23])
    d3y_c.append(row[24])
    d3z_c.append(row[25])

    d3x_fb_c.append(row[26])
    d3y_fb_c.append(row[27])
    d3z_fb_c.append(row[28])

    int_x.append(row[29])
    int_y.append(row[30])
    int_z.append(row[31])

    throttle_c.append(row[32])
    f_total_c.append(row[33])
    voltage.append(row[35])

    x_p.append(row[62])
    y_p.append(row[63])
    z_p.append(row[64])

    q0_p.append(row[65])
    qx_p.append(row[66])
    qy_p.append(row[67])
    qz_p.append(row[68])

    dx_t.append(row[70])
    dy_t.append(row[71])
    dz_t.append(row[72])

    p_t.append(row[73])
    q_t.append(row[74])
    r_t.append(row[75])

# Convert to array
t_c = np.asarray([float(x) for x in t_c])
x_c = np.asarray([float(x) for x in x_c])
y_c = np.asarray([float(x) for x in y_c])
z_c = np.asarray([float(x) for x in z_c])
q0_c = np.asarray([float(x) for x in q0_c])
qx_c = np.asarray([float(x) for x in qx_c])
qy_c = np.asarray([float(x) for x in qy_c])
qz_c = np.asarray([float(x) for x in qz_c])
dx_c = np.asarray([float(x) for x in dx_c])
dy_c = np.asarray([float(x) for x in dy_c])
dz_c = np.asarray([float(x) for x in dz_c])
p_c = np.asarray([float(x) for x in p_c])
q_c = np.asarray([float(x) for x in q_c])
r_c = np.asarray([float(x) for x in r_c])
roll_c = np.asarray([float(x) for x in roll_c])
pith_c = np.asarray([float(x) for x in pitch_c])
yaw_c = np.asarray([float(x) for x in yaw_c])
d2x_c = np.asarray([float(x) for x in d2x_c])
d2y_c = np.asarray([float(x) for x in d2y_c])
d2z_c = np.asarray([float(x) for x in d2z_c])
d2x_fb_c = np.asarray([float(x) for x in d2x_fb_c])
d2y_fb_c = np.asarray([float(x) for x in d2y_fb_c])
d2z_fb_c = np.asarray([float(x) for x in d2z_fb_c])
d3x_c = np.asarray([float(x) for x in d3x_c])
d3y_c = np.asarray([float(x) for x in d3y_c])
d3z_c = np.asarray([float(x) for x in d3z_c])
d3x_fb_c = np.asarray([float(x) for x in d3x_fb_c])
d3y_fb_c = np.asarray([float(x) for x in d3y_fb_c])
d3z_fb_c = np.asarray([float(x) for x in d3z_fb_c])
collective_c = np.asarray([float(x) for x in collective_c])
throttle_c = np.asarray([float(x) for x in throttle_c])
f_total_c = np.asarray([float(x) for x in f_total_c])
voltage = np.asarray([float(x) for x in voltage])
x_p = np.asarray([float(x) for x in x_p])
y_p = np.asarray([float(x) for x in y_p])
z_p = np.asarray([float(x) for x in z_p])
q0_p = np.asarray([float(x) for x in q0_p])
qx_p = np.asarray([float(x) for x in qx_p])
qy_p = np.asarray([float(x) for x in qy_p])
qz_p = np.asarray([float(x) for x in qz_p])
dx_t = np.asarray([float(x) for x in dx_t])
dy_t = np.asarray([float(x) for x in dy_t])
dz_t = np.asarray([float(x) for x in dz_t])
p_t = np.asarray([float(x) for x in p_t])
q_t = np.asarray([float(x) for x in q_t])
r_t = np.asarray([float(x) for x in r_t])

# Shift time
t_c= t_c-t_c[0]

# [roll pitch yaw]
roll_des, pitch_des, yaw_des = quat2angle(q0_c,qx_c,qy_c,qz_c)
roll_act, pitch_act, yaw_act = quat2angle(q0_p,qx_p,qy_p,qz_p)

# Position
pl.figure(1)
pl.subplot(311)
pl.title("Position")
pl.plot(t_c,x_c,t_c,x_p)
pl.legend(['Command','Actual'])
pl.ylabel("x [m]")
pl.subplot(312)
pl.plot(t_c,y_c)
pl.plot(t_c,y_p)
pl.ylabel("y [m]")
pl.subplot(313)
pl.plot(t_c,z_c)
pl.plot(t_c,z_p)
pl.ylabel("z [m]")
pl.xlabel("Time [s]")

# Velocity
pl.figure(2)
pl.subplot(311)
pl.title("Velocity")
pl.plot(t_c,dx_c,t_c,dx_t)
pl.legend(['Command','Actual'])
pl.ylabel("x [m/s]")
pl.subplot(312)
pl.plot(t_c,dy_c,t_c,dy_t)
pl.ylabel("y [m/s]")
pl.subplot(313)
pl.plot(t_c,dz_c,t_c,dz_t)
pl.ylabel("z [m/s]")
pl.xlabel("Time [s]")

# Attitude
pl.figure(3)
pl.subplot(311)
pl.title("Attitude")
pl.plot(t_c,roll_des*180/math.pi,t_c,roll_act*180/math.pi)
pl.legend(['Command','Actual'])
pl.ylabel("Roll [deg]")
pl.subplot(312)
pl.plot(t_c,pitch_des*180/math.pi,t_c,pitch_act*180/math.pi)
pl.ylabel("Pitch [deg]")
pl.subplot(313)
pl.plot(t_c,yaw_des*180/math.pi,t_c,yaw_act*180/math.pi)
pl.ylabel("Yaw [deg]")
pl.xlabel("Time [s]")

# rate
pl.figure(4)
pl.subplot(311)
pl.title("Rate")
pl.plot(t_c,p_c*180/math.pi,t_c,p_t*180/math.pi)
pl.legend(['Command','Actual'])
pl.ylabel("Roll Rate [deg/s]")
pl.subplot(312)
pl.plot(t_c,q_c*180/math.pi,t_c,q_t*180/math.pi)
pl.ylabel("Pitch Rate [deg/s]")
pl.subplot(313)
pl.plot(t_c,r_c*180/math.pi,t_c,r_t*180/math.pi)
pl.ylabel("Yaw Rate [deg/s]")
pl.xlabel("Time [s]")

# Acceleration
pl.figure(5)
pl.subplot(311)
pl.title("Acceleration")
pl.plot(t_c,d2x_c,t_c,d2x_fb_c)
pl.legend(['Command','Feedback'])
pl.ylabel("x [m/s^2]")
pl.subplot(312)
pl.plot(t_c,d2y_c,t_c,d2y_fb_c)
pl.ylabel("y [m/s^2]")
pl.subplot(313)
pl.plot(t_c,d2z_c,t_c,d2z_fb_c)
pl.ylabel("z [m/s^2]")
pl.xlabel("Time [s]")

# Jerk
pl.figure(6)
pl.subplot(311)
pl.title("Jerk")
pl.plot(t_c,d3x_c,t_c,d3x_fb_c)
pl.legend(['Command','Feedback'])
pl.ylabel("x [m/s^3]")
pl.subplot(312)
pl.plot(t_c,d3y_c,t_c,d3y_fb_c)
pl.ylabel("y [m/s^3]")
pl.subplot(313)
pl.plot(t_c,d3z_c,t_c,d3z_fb_c)
pl.ylabel("z [m/s^3]")
pl.xlabel("Time [s]")

pl.figure(7)
pl.subplot(311)
pl.plot(t_c,throttle_c)
pl.ylabel("Throttle")
pl.subplot(312)
pl.plot(t_c,voltage)
pl.ylabel("Voltage")
pl.subplot(313)
pl.plot(t_c,f_total_c)
pl.ylabel("Total Force Comm.")
pl.xlabel("Time [s]")

# Position Integrator Values
pl.figure(8)
pl.plot(t_c,int_x,t_c,int_y,t_c,int_z)
pl.legend(['x','y','z'])
pl.ylabel("Position Integrator Values")
pl.xlabel("Time [s]")

# 3D plot
fig = pl.figure(9)
ax = fig.gca(projection='3d')
com,=ax.plot(x_c,y_c,z_c)
ax.set_xlabel('x [m]')
ax.set_ylabel('y [m]')
ax.set_zlabel('z [m]')
act,=ax.plot(x_p,y_p,z_p)
ax.legend([com,act],['Desired','Actual'])

pl.show()
