/*
 * comm.cpp
 *
 *  Created on: March 26, 2011
 *      Author: mark
 */

#include "quadrotor.hpp"

extern bool ABORT;
extern int ABORT_cnt;

void Quadrotor::sendCal()
{
	// Convert calibration data to a command packet - scale the data
	tSensorCalPacket pkt;
	static uint8_t seq = 0;
	pkt.gyroScale = (int16_t) (calData.gyroScale * QUAT_SCALE * ROT_SCALE);
	pkt.accelScale = (int16_t) (calData.accelScale * QUAT_SCALE * ROT_SCALE);
	pkt.K_Att_Filter =
			(int16_t) (calData.K_Att_Filter * QUAT_SCALE * ROT_SCALE);
	pkt.kGyroBias = (int16_t) (calData.kGyroBias * QUAT_SCALE * ROT_SCALE);

	if (not sim)
		SendPacket(PACKETID_SENSORCAL, sizeof(tSensorCalPacket), seq++, (uint8_t *) &pkt);

}

bool writeLogFile = false;
void Quadrotor::sendCmd()
{
	// Convert command data to a command packet - scale the data
	tCmdPacket pkt;
	static uint8_t seq = 0;
	if (not ABORT)
	{
		pkt.p_cmd = (int16_t) (goal.rate.getX() * ROT_SCALE);
		pkt.q_cmd = (int16_t) (goal.rate.getY() * ROT_SCALE);
		pkt.r_cmd = (int16_t) (goal.rate.getZ() * ROT_SCALE);
		pkt.qo_cmd = (int16_t) (goal.att.getW() * QUAT_SCALE);
		pkt.qx_cmd = (int16_t) (goal.att.getX() * QUAT_SCALE);
		pkt.qy_cmd = (int16_t) (goal.att.getY() * QUAT_SCALE);
		pkt.qz_cmd = (int16_t) (goal.att.getZ() * QUAT_SCALE);
		pkt.qo_meas = (int16_t) (state.att.getW() * QUAT_SCALE);
		pkt.qx_meas = (int16_t) (state.att.getX() * QUAT_SCALE);
		pkt.qy_meas = (int16_t) (state.att.getY() * QUAT_SCALE);
		pkt.qz_meas = (int16_t) (state.att.getZ() * QUAT_SCALE);
		pkt.throttle = (uint8_t) (throttle * 255.0);
		if (pkt.throttle == 0)
			pkt.throttle = 1;
		pkt.pitch = 0;
		pkt.AttCmd = (uint8_t) attStatus;
		/*pkt.p_cmd = 0;
		 pkt.q_cmd = 0;
		 pkt.r_cmd = 0;
		 pkt.qo_cmd = QUAT_SCALE;
		 pkt.qx_cmd = 0;
		 pkt.qy_cmd = 0;
		 pkt.qz_cmd = 0;
		 pkt.qo_meas = QUAT_SCALE;
		 pkt.qx_meas = 0;
		 pkt.qy_meas = 0;
		 pkt.qz_meas = 0;
		 pkt.throttle = 100;
		 pkt.pitch = 0;
		 pkt.AttCmd = 1;*/
		/*std::ostringstream msg;
		 msg.setf(std::ios::fixed); 	  				// give all the doubles the same precision
		 msg.setf(std::ios::showpos);				// show +/- signs always
		 msg  << std::setprecision(4) << std::endl;	// set precision to 4 decimal places
		 msg << "p: " << pkt.p_cmd << " q: " << pkt.q_cmd << " r: " << pkt.r_cmd << std::endl;
		 msg << "qo: " << pkt.qo_cmd  << " qx: " << pkt.qx_cmd  << " qy: " << pkt.qy_cmd  << " qz: " << pkt.qz_cmd << std::endl;
		 msg << "qo: " << pkt.qo_meas  << " qx: " << pkt.qx_meas  << " qy: " << pkt.qy_meas  << " qz: " << pkt.qz_meas << std::endl;
		 msg << "throttle: " << (double)pkt.throttle << " pitch: " << (double)pkt.pitch << " attcmd: " << (double)pkt.AttCmd << std::endl << std::endl;
		 ROS_INFO_THROTTLE(0.5,msg.str().c_str()); // Print at 1/0.5 Hz = 2 Hz*/
	}
	else
	{
		ROS_WARN_STREAM("SHUTTING DOWN");
		ABORT_cnt++;
		if (ABORT_cnt > 0)
		{
			sleep(1.0);
			ROS_INFO_STREAM("Starting to write log file");
			writeDataLog2File();

			ros::shutdown();
		}
		pkt.p_cmd = 0;
		pkt.q_cmd = 0;
		pkt.r_cmd = 0;
		pkt.qo_cmd = 0;
		pkt.qx_cmd = 0;
		pkt.qy_cmd = 0;
		pkt.qz_cmd = 0;
		pkt.qo_meas = 0;
		pkt.qx_meas = 0;
		pkt.qy_meas = 0;
		pkt.qz_meas = 0;
		pkt.throttle = 0;
		pkt.pitch = 0;
		pkt.AttCmd = 0;
	}
	if (not sim)
		SendPacket(PACKETID_CMD, sizeof(tCmdPacket), seq++, (uint8_t *) &pkt);

}

// calculate the average throttle and attitude needed to sustain flight
void Quadrotor::sendEmergency()
{
	// only send emergency data if you have been flying for more than 20 seconds
	// otherwise, the emergency data will be to just kill the motors
	if (not (attStatus and (ros::Time::now().toSec() - startTime.toSec() > 20.0)))
		return;

	ROS_INFO_STREAM("Sending emergency data!");

	// calculate the emergency data
	tf::Vector3 int_err;
	int_err.setValue(I_x.value, I_y.value, I_z.value);
	tf::Vector3 F_int = gains.Ki * int_err;
	tf::Vector3 F;
	F.setX(mass * F_int.getX());
	F.setY(mass * F_int.getY());
	F.setZ(mass * GRAVITY);
	tf::Quaternion att = getAttitude(F);
	F.setZ(mass * (F_int.getZ() + GRAVITY));
	double throttle_avg = F2motorCmd(F.length());

	// send the emergency data
	tEmergencyPacket pkt;
	static uint8_t seq = 0;

	pkt.qo_avg = (int16_t) (att.getW() * QUAT_SCALE);
	pkt.qx_avg = (int16_t) (att.getX() * QUAT_SCALE);
	pkt.qy_avg = (int16_t) (att.getY() * QUAT_SCALE);
	pkt.qz_avg = (int16_t) (att.getZ() * QUAT_SCALE);
	pkt.throttle_avg = (uint8_t) (throttle_avg * 255.0);
	if (pkt.throttle_avg == 0)
		pkt.throttle_avg = 1;

	if (not sim)
		SendPacket(PACKETID_EMERGENCY, sizeof(tEmergencyPacket), seq++, (uint8_t *) &pkt);

}

void Quadrotor::sendGains()
{
	// Convert command data to a command packet - scale the data

	tGainsPacket pkt;
	static uint8_t seq = 0;
	pkt.Kp_roll = (int16_t) (innerGains.Kp.getX() * GAINS_SCALE);
	pkt.Ki_roll = (int16_t) (innerGains.Ki.getX() * GAINS_SCALE);
	pkt.Kd_roll = (int16_t) (innerGains.Kd.getX() * GAINS_SCALE);

	pkt.Kp_pitch = (int16_t) (innerGains.Kp.getY() * GAINS_SCALE);
	pkt.Ki_pitch = (int16_t) (innerGains.Ki.getY() * GAINS_SCALE);
	pkt.Kd_pitch = (int16_t) (innerGains.Kd.getY() * GAINS_SCALE);

	pkt.Kp_yaw = (int16_t) (innerGains.Kp.getZ() * GAINS_SCALE);
	pkt.Ki_yaw = (int16_t) (innerGains.Ki.getZ() * GAINS_SCALE);
	pkt.Kd_yaw = (int16_t) (innerGains.Kd.getZ() * GAINS_SCALE);

	pkt.Servo1_trim = (int16_t) innerGains.PWM_trim[0];
	pkt.Servo2_trim = (int16_t) innerGains.PWM_trim[1];
	pkt.Servo3_trim = (int16_t) innerGains.PWM_trim[2];
	pkt.Servo4_trim = (int16_t) innerGains.PWM_trim[3];

	pkt.m1_trim = (int16_t) (innerGains.M_trim[0] * ROT_SCALE);
	pkt.m2_trim = (int16_t) (innerGains.M_trim[1] * ROT_SCALE);
	pkt.m3_trim = (int16_t) (innerGains.M_trim[2] * ROT_SCALE);
	pkt.m4_trim = (int16_t) (innerGains.M_trim[3] * ROT_SCALE);

	pkt.maxang = (int16_t) (innerGains.maxang * GAINS_SCALE);
	pkt.lowBatt = (uint16_t) (innerGains.lowBatt * 1000); // convert to millivolts
	pkt.motorFF = (int16_t) (innerGains.motorFF * GAINS_SCALE * GAINS_SCALE);
	pkt.stream_data = (uint8_t) flags.streamData;

	/*std::ostringstream msg;
	 msg.setf(std::ios::fixed); 	  				// give all the doubles the same precision
	 msg.setf(std::ios::showpos);				// show +/- signs always
	 msg  << std::setprecision(4) << std::endl;	// set precision to 4 decimal places
	 msg << "kp_roll: " << pkt.Kp_roll << " ki_roll: " << pkt.Ki_roll << " kd_roll: " << pkt.Kd_roll << std::endl;
	 msg << "kp_pitch: " << pkt.Kp_pitch << " ki_pitch: " << pkt.Ki_pitch << " kd_pitch: " << pkt.Kd_pitch << std::endl;
	 msg << "kp_yaw: " << pkt.Kp_yaw << " ki_yaw: " << pkt.Ki_yaw << " kd_yaw: " << pkt.Kd_yaw << std::endl;

	 msg << "maxang: " << pkt.maxang << " lowbatt: " << pkt.lowBatt << " motorFF: " << pkt.motorFF << " stream_data: " << pkt.stream_data << std::endl;

	 ROS_INFO_STREAM(msg.str().c_str()); // Print at 1/0.5 Hz = 2 Hz*/

	if (not sim)
		SendPacket(PACKETID_GAINS, sizeof(tGainsPacket), seq++, (uint8_t *) &pkt);

}

int16_t volt_tmp;
void Quadrotor::parsePacket(uint8_t ID, uint8_t length, uint8_t seq, uint8_t * data)
{
	switch (ID)
	{
	case PACKETID_VOLTAGE:
		// voltage packet
		memcpy(&volt_tmp, data, sizeof(tVoltagePacket));
		healthData.voltage = (double) volt_tmp / 1000.0;
		break;

	case PACKETID_AHRS:
		// AHRS data packetSendPacket
		memcpy(&ahrs, data, sizeof(tAHRSpacket));
		healthData.rate.x = ((double) ahrs.p) / ROT_SCALE;
		healthData.rate.y = ((double) ahrs.q) / ROT_SCALE;
		healthData.rate.z = ((double) ahrs.r) / ROT_SCALE;
		healthData.att.w = ((double) ahrs.qo_est) / QUAT_SCALE;
		healthData.att.x = ((double) ahrs.qx_est) / QUAT_SCALE;
		healthData.att.y = ((double) ahrs.qy_est) / QUAT_SCALE;
		healthData.att.z = ((double) ahrs.qz_est) / QUAT_SCALE;
		healthData.att_meas.w = ((double) ahrs.qo_meas) / QUAT_SCALE;
		healthData.att_meas.x = ((double) ahrs.qx_meas) / QUAT_SCALE;
		healthData.att_meas.y = ((double) ahrs.qy_meas) / QUAT_SCALE;
		healthData.att_meas.z = ((double) ahrs.qz_meas) / QUAT_SCALE;
		healthData.motor_diff[0] = ((double) ahrs.dm1) / ROT_SCALE;
		healthData.motor_diff[1] = ((double) ahrs.dm2) / ROT_SCALE;
		healthData.motor_diff[2] = ((double) ahrs.dm3) / ROT_SCALE;
		healthData.motor_diff[3] = ((double) ahrs.dm4) / ROT_SCALE;

		break;

	case PACKETID_HEALTH:
		// AHRS data packetSendPacket
		memcpy(&healthPacket, data, sizeof(tHealthPacket)); // todo: implement proper scaling of current and temp values
		healthData.current[0] = (double) healthPacket.current1;
		healthData.current[1] = (double) healthPacket.current2;
		healthData.current[2] = (double) healthPacket.current3;
		healthData.current[3] = (double) healthPacket.current4;
		healthData.temperature[0] = (double) healthPacket.temp1;
		healthData.temperature[1] = (double) healthPacket.temp2;
		healthData.temperature[2] = (double) healthPacket.temp3;
		healthData.temperature[3] = (double) healthPacket.temp4;

		break;

	case PACKETID_SENSORS:
		memcpy(&sensorsPacket, data, sizeof(tSensorsPacket));
		sensors.gyro.x = (double) sensorsPacket.gyroX;
		sensors.gyro.y = (double) sensorsPacket.gyroY;
		sensors.gyro.z = (double) sensorsPacket.gyroZ;
		sensors.accel.x = (double) sensorsPacket.accelX;
		sensors.accel.y = (double) sensorsPacket.accelY;
		sensors.accel.z = (double) sensorsPacket.accelZ;
		sensors.mag.x = (double) sensorsPacket.magX;
		sensors.mag.y = (double) sensorsPacket.magY;
		sensors.mag.z = (double) sensorsPacket.magZ;
		sensors.sonar = (double) sensorsPacket.sonarZ;
		sensors.sonar /= 1000; // into meters
		sensors.pressure = (double) sensorsPacket.pressure;

		/************ Sensor Data **********/
		static int count = 0;
		count++;
		sensors.header.stamp = ros::Time::now();
		sensors.header.frame_id = name;
		sensors.true_pose.position.x = state.pos.getX();
		sensors.true_pose.position.y = state.pos.getY();
		sensors.true_pose.position.z = state.pos.getZ();
		tf::quaternionTFToMsg(state.att, sensors.true_pose.orientation);
		tf::vector3TFToMsg(state.vel, sensors.true_vel.linear);
		tf::vector3TFToMsg(state.rate, sensors.true_vel.angular);
		if (count > 1000)
			sensorPub.publish(sensors);
		break;

	case PACKETID_ALTITUDE:
		memcpy(&altitudePacket, data, sizeof(tAltitudePacket));
		sensors.sonar = (double) altitudePacket.sonarZ;
		sensors.sonar /= 1000; // into meters
		sensors.pressure = (double) altitudePacket.pressure;
		sensors.temperature = (double) altitudePacket.temperature;
		sensors.accel.x = ((double) altitudePacket.accelX) / 1000;
		sensors.accel.y = ((double) altitudePacket.accelY) / 1000;
		sensors.accel.z = ((double) altitudePacket.accelZ) / 1000;
		break;

		break;
	default:
		ROS_INFO("Packet ID not recognized!\n");
		break;
	}

}

void Quadrotor::SendPacket(uint8_t ID, uint8_t length, uint8_t seq, uint8_t * data)
{
	// Calculate the checksum
	uint8_t checkSum = length + ID + seq;
	for (int i = 0; i < length; i++)
	{
		checkSum += data[i];
	}
	//checkSum = (uint8_t)(256-checkSum);
	checkSum ^= 0xFF;
	checkSum += 1;

	// Send start sequence, ID, length
	int headersize = 5;
	uint8_t header[headersize];
	header[0] = 0xFF;
	header[1] = 0xFE;
	header[2] = ID;
	header[3] = length;
	header[4] = seq;

	// merge data into one array
	uint8_t *packet = (uint8_t *) malloc((headersize + length + 1)*sizeof(uint8_t));

	memcpy(packet, header, headersize*sizeof(uint8_t)); // copy header into packet
	memcpy(packet + headersize, data, length*sizeof(uint8_t));
	memcpy(packet + headersize + length, &checkSum, sizeof(uint8_t));

	if (flags.use_xbee)
		ser.spSend(packet, headersize + length + 1);
	else
		udp.udpSend(packet, headersize + length + 1);

}

void *listen(void *param)
{
	Quadrotor * quad = (Quadrotor *) param;
	uint8_t packetID;
	uint8_t packetLength;
	uint8_t packetData[256];
	int packetCnt = 0;
	uint8_t checksum;

	uint8_t thisByte = 0;
	uint8_t lastByte = 0;

	uint8_t STX1 = 0xFF;
	uint8_t STX2 = 0xFE;

	uint8_t packetSeq;

	ReceiveState rxstate =  RXSTATE_STXA;

	while(1)
	{
		// COMMUNICATION THROUGH EXTERNAL PORT
		uint8_t bufr[2048];
		int received_bytes = 0;
		if (quad->flags.use_xbee)
		{
			received_bytes = 1;
			bufr[0] = quad->ser.spReceiveSingle();
		} else {
			received_bytes = quad->udp.udpReceive((char*) bufr, sizeof(bufr));
		}

		// Try to get a new message
		for (int i=0; i<received_bytes; i++)
		{
			uint8_t b = bufr[i];

			switch(rxstate){
                case RXSTATE_STXA:
                    if (b == STX1) rxstate = RXSTATE_STXB;
                    break;
                case RXSTATE_STXB:
                    if (b == STX1)
                            rxstate = RXSTATE_STXB;
                    else if (b == STX2)
                            rxstate = RXSTATE_PACKETID;
                    else
                            rxstate = RXSTATE_STXA;
                    break;
                case RXSTATE_PACKETID:
                    packetID = b;
                    rxstate = RXSTATE_LEN;
                    checksum = b;
                    break;
				case RXSTATE_LEN:
                    packetLength = b;
                    rxstate = RXSTATE_SEQ;
                    checksum += b;
                    break;
                case RXSTATE_SEQ:
                	packetSeq = b;
                	rxstate = RXSTATE_DATA;
                	checksum += b;
                	packetCnt = 0;
                	break;
                case RXSTATE_DATA:
                    packetData[packetCnt] = b;
                    packetCnt++;
                    if (packetCnt == packetLength) rxstate = RXSTATE_CHKSUM;
                    checksum += b;
                    break;
                case RXSTATE_CHKSUM:
                    rxstate = RXSTATE_STXA;
                    checksum += b;
					// Parse packet if checksum correct
					if (checksum == 0)
					{
						quad->parsePacket(packetID, packetLength, packetSeq, packetData);
						// std::cout << int(packetSeq) << std::endl;
					}
					else
					{
						ROS_INFO("Bad checksum! \n");
					}
					break;
                default:
                        break;

			}
		}
	}
}

