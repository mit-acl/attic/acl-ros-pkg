/*
 * comm.hpp
 *
 *  Created on: Oct 24, 2012
 *      Author: mark
 */

#ifndef COMM_HPP_
#define COMM_HPP_

enum ReceiveState{
	RXSTATE_STXA,
	RXSTATE_STXB,
	RXSTATE_PACKETID,
	RXSTATE_LEN,
	RXSTATE_SEQ,
	RXSTATE_DATA,
	RXSTATE_CHKSUM
};

//*********************** COMM STRUCTS ***********************//
#define PACKETID_SENSORCAL 0x01
typedef struct sSensorCalPacket
{
  int16_t gyroScale;
  int16_t accelScale;
  int16_t K_Att_Filter;
  int16_t kGyroBias;
}__attribute__((__packed__)) tSensorCalPacket;

#define PACKETID_AHRS 0x02
typedef struct __attribute__ ((packed)) sAHRSpacket
{
  int16_t p;
  int16_t q;
  int16_t r;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t qo_meas;
  int16_t qx_meas;
  int16_t qy_meas;
  int16_t qz_meas;
  int16_t dm1;
  int16_t dm2;
  int16_t dm3;
  int16_t dm4;

} tAHRSpacket;

#define PACKETID_CMD 0x03
typedef struct __attribute__ ((packed)) sCmdPacket
{
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  int16_t qo_meas;
  int16_t qx_meas;
  int16_t qy_meas;
  int16_t qz_meas;
  uint8_t throttle;
  int8_t pitch;
  uint8_t AttCmd;
} tCmdPacket;

#define PACKETID_GAINS 0x04
typedef struct __attribute__ ((packed)) sGainsPacket
{
  int16_t Kp_roll;
  int16_t Kd_roll;
  int16_t Ki_roll;
  int16_t Kp_pitch;
  int16_t Kd_pitch;
  int16_t Ki_pitch;
  int16_t Kp_yaw;
  int16_t Ki_yaw;
  int16_t Kd_yaw;
  int16_t Servo1_trim;
  int16_t Servo2_trim;
  int16_t Servo3_trim;
  int16_t Servo4_trim;
  int16_t m1_trim;
  int16_t m2_trim;
  int16_t m3_trim;
  int16_t m4_trim;
  int16_t maxang;
  int16_t motorFF;
  uint16_t lowBatt;
  uint8_t stream_data;
} tGainsPacket;

#define PACKETID_VOLTAGE 0x05
typedef struct __attribute__ ((packed)) sVoltagePacket
{
  int16_t voltage;
} tVoltagePacket;

#define PACKETID_LOG 0x06
typedef struct __attribute__((packed)) sLogPacket
{
  int16_t p_est;
  int16_t q_est;
  int16_t r_est;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  uint8_t throttle;
  int8_t collective;
} tLogPacket;

#define PACKETID_HEALTH 0x07
typedef struct __attribute__ ((packed)) sHealthPacket
{
  int8_t current1;
  int8_t temp1;
  int8_t current2;
  int8_t temp2;
  int8_t current3;
  int8_t temp3;
  int8_t current4;
  int8_t temp4;
} tHealthPacket;

#define PACKETID_SENSORS 0x08
typedef struct __attribute__((packed)) sSensorsPacket
{
  int16_t gyroX;
  int16_t gyroY;
  int16_t gyroZ;
  int16_t accelX;
  int16_t accelY;
  int16_t accelZ;
  int16_t magX;
  int16_t magY;
  int16_t magZ;
  int16_t sonarZ;
  int32_t pressure;
} tSensorsPacket;

#define PACKETID_ALTITUDE 0x09
typedef struct __attribute__((packed)) sAltitudePacket
{
  int16_t sonarZ;
  int32_t pressure;
  int32_t temperature;
  int16_t accelX;
  int16_t accelY;
  int16_t accelZ;
} tAltitudePacket;

#define PACKETID_ADAPTCMD 0x0A

typedef struct __attribute__((packed)) sAdaptCmdPacket
{
  int16_t cr_x;
  int16_t cr_y;
  int16_t cr_z;
  int16_t pd_x;
  int16_t pd_y;
  int16_t pd_z;
  int16_t ad_x;
  int16_t ad_y;
  int16_t ad_z;
  uint8_t AttCmd;
} tAdaptCmdPacket;

#define PACKETID_EMERGENCY 0x0B
typedef struct __attribute__ ((packed)) sEmergencyPacket
{
  int16_t qo_avg;
  int16_t qx_avg;
  int16_t qy_avg;
  int16_t qz_avg;
  uint8_t throttle_avg;
} tEmergencyPacket;

#endif /* COMM_HPP_ */
