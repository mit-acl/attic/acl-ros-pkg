/*
 * control.cpp
 *
 *  Created on: May 30, 2012
 *      Author: mark
 */

#include "quadrotor.hpp"

tf::Vector3 Quadrotor::getAccel()
{
	// Position Errors
	tf::Vector3 pos_err = saturate(goal.pos - state.pos, gains.maxPosErr,
			-gains.maxPosErr);

	// Velocity Errors
	tf::Vector3 vel_err = saturate(goal.vel - state.vel, gains.maxVelErr,
			-gains.maxVelErr);

	// Integrate Position Errors
	if (fabs(pos_err.getX() < gains.I_xy_int_thresh) && this->attStatus)
	{
		I_x.increment(pos_err.getX(), controlDT);
	}
	if (fabs(pos_err.getY() < gains.I_xy_int_thresh) && this->attStatus)
	{
		I_y.increment(pos_err.getY(), controlDT);
	}
	if (fabs(pos_err.getX() < gains.I_z_int_thresh) && this->attStatus)
	{
		I_z.increment(pos_err.getZ(), controlDT);
	}
	tf::Vector3 int_err;
	int_err.setValue(I_x.value, I_y.value, I_z.value);

	// Desired Feedback Acceleration -- this is added to the feedforward acceleration
	goal.accel_fb = gains.Kp * pos_err + gains.Kd * vel_err
			+ gains.Ki * int_err;

	tf::Vector3 r_nom; // ignore feedback here
	r_nom.setX(mass * goal.accel.getX());
	r_nom.setY(mass * goal.accel.getY());
	r_nom.setZ(mass * (goal.accel.getZ() + GRAVITY));
	gains.rmag = r_nom.length();

	bool useFeedback;
	if (gains.rmag > gains.minRmagint)
	{ // use feedback values
		useFeedback = true;
	}
	else
	{
		useFeedback = false;
		I_x.reset();
		I_y.reset();
		I_z.reset(); // reset position integrators when doing "open loop" attitude
		goal.accel_fb.setX(0.0);
		goal.accel_fb.setY(0.0);
	}

	// Numerically Differentiate feedback acceleration
	double dt = ros::Time::now().toSec() - t_old;
	t_old = ros::Time::now().toSec();
	if (dt > 0.0)
	{
		goal.jerk_fb = (goal.accel_fb - accel_fb_old) / dt;

		// filter the differentiation
		double alpha = dt / (0.1 + dt);
		goal.jerk_fb = jerk_fb_old + alpha * (goal.jerk_fb - jerk_fb_old);

	}

	// DEBUG!!!
	//goal.jerk.setX(-goal.jerk.getX());

	// zero numerical differentiation if real commands are being sent
	if (fabs(goal.jerk.getX()) > 0.1)
		goal.jerk_fb.setX(0.0);
	if (fabs(goal.jerk.getY()) > 0.1)
		goal.jerk_fb.setY(0.0);
	if (fabs(goal.jerk.getZ()) > 0.1)
		goal.jerk_fb.setZ(0.0);

	// update variables
	accel_fb_old = goal.accel_fb;
	jerk_fb_old = goal.jerk_fb;

	tf::Vector3 F;
	F.setZ(mass * (goal.accel.getZ() + GRAVITY));
	if (useFeedback)
	{
		F.setX(mass * (goal.accel.getX() + goal.accel_fb.getX()));
		F.setY(mass * (goal.accel.getY() + goal.accel_fb.getY()));
	}
	else
	{
		F.setX(mass * goal.accel.getX());
		F.setY(mass * goal.accel.getY());
	}

	return F;
}

tf::Quaternion Quadrotor::getAttitude(tf::Vector3 F)
{
	tf::Vector3 Fbar = F;
	vnorm(Fbar);
	// pure quaternion (0 scalar part)
	tf::Quaternion qFbar = tf::Quaternion(Fbar.getX(), Fbar.getY(), Fbar.getZ(),
			0.0);

	tf::Vector3 Fbarb, FbxF;
	Fbarb.setValue(0.0, 0.0, 1.0);
	FbxF = Fbarb.cross(Fbar);
	double FbdF = Fbarb.dot(Fbar);

	tf::Quaternion qmin = tf::Quaternion::getIdentity();
	double eps = 0.05;
	if (gains.rmag > gains.minRmag)
	{
		if (FbdF + 1 > eps)
		{
			double tmp = 1.0 / sqrt(2 * (1 + FbdF));
			qmin.setValue(tmp * FbxF.getX(), tmp * FbxF.getY(),
					tmp * FbxF.getZ(), tmp * (1 + FbdF));
		}
	}
	qnorm(qmin);

	// recompute r3 using true desired acceleration
	F.setZ(mass * (goal.accel.getZ() + goal.accel_fb.getZ() + GRAVITY));
	Fbar = F;
	Fbar.normalize();
	qFbar.setValue(Fbar.getX(), Fbar.getY(), Fbar.getZ(), 0.0); // pure quaternion (0 scalar part)
	qnorm(qFbar);

	// Calculate sum of forces for thrust control
	tf::Quaternion qFbarb = qmin * qFbar * qmin.inverse(); // todo: is the order of q qFbar q* right?
	qnorm(qFbarb);

	goal.f_total = F.length(); // * qFbarb.getZ();

	// only assign goal attitude if you aren't close to a singularity
	tf::Quaternion att_des;
	if (gains.rmag > gains.minRmag)
	{
		// ensure quaternions agree with each other
		qmin = checkQuat(qmin, qmin_old, state.att);
		qmin_old = qmin;

		att_des = qmin;

		// Rotate by yaw
		tf::Quaternion qyaw_des;
		qyaw_des.setRPY(0.0, 0.0, goal.yaw);
		att_des *= qyaw_des;
	} else {
		att_des = qmin_old;
	}

	return att_des;
}

tf::Vector3 Quadrotor::getRate(tf::Vector3 F)
{
	tf::Vector3 rate;

	// Compute desired rates - see GNC uber 2012 for derivation
	tf::Vector3 Fdot, Fbardot, Fbar, omega;
	Fbar = F / F.length();
	Fdot = mass * (goal.jerk + goal.jerk_fb);
	Fbardot = Fdot / F.length() - F * F.dot(Fdot) / pow(F.length(), 3);
	omega = Fbar.cross(Fbardot);

	if (gains.rmag > gains.minRmag)
	{
		rate = omega;
	}
	rate.setZ(0.0); // yaw rate is not valid to compute here

	// rotate by yaw
	tf::Quaternion qrate, qup;
	qrate.setValue(rate.getX(), rate.getY(), rate.getZ(), 0.0);
	qup.setRPY(0.0, 0.0, tf::getYaw(state.att));
	qrate = qup.inverse() * qrate * qup;
	rate.setX(qrate.getX());
	rate.setY(qrate.getY());
	rate.setZ(goal.dyaw); // yaw rate is specified as an input, doesn't come from angular accelerations

	return rate;
}

// convert a desired force in newtons to desired motor command
double Quadrotor::F2motorCmd(double f)
{
	double throttle = gains.a * f * f + gains.b * f + gains.c;
	return acl::saturate(throttle, 0.9, 0.0);
}

