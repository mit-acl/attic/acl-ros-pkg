/*
 * defines.h
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef DEFINES_H_
#define DEFINES_H_

//Defines
#define controlDT 		        0.01 	// 1/100 Control loop rate [seconds]
// Safety values
#define MAX_VICON_WAIT_TIME		0.5 // wait time in seconds for new vicon data
#define MAX_ACCEL_XY			2.0
#define MAX_ACCEL_Z			0.8

#define SCREEN_PRINT_RATE	0.5 // Print at 1/0.5 Hz = 2 Hz

// Waypoint message types
#define TAKEOFF			1
#define CUT_POWER		2
#define RESET_XY_INT	3
#define RESET_XYZ_INT	4
#define ATTITUDE		5

// Flight Mode
#define DISABLED 		0
#define FLYING			1

//## communications defines -- must be the same values on embedded code
#define QUAT_SCALE		10000.0  	// value to scale quaternions by for sending as int
#define ROT_SCALE		100.0		// value to scale rotations (p,q,r) by for sending as int
#define GAINS_SCALE		10.0		// value to scale gains by for sending as int

//## Joystick Key and Axis Mappings
#define A			0
#define B			1
#define X			2
#define Y			3
#define LB			4
#define RB			5
#define BACK		        6
#define START		        7
#define ON			8
#define LEFT_AXIS	        9
#define RIGHT_AXIS	        10
#define LEFT		        11
#define RIGHT		        12
#define UP			13
#define DOWN		        14

#define LEFT_X		        0
#define LEFT_Y		        1
#define LT			2
#define RIGHT_X 	        3
#define RIGHT_Y 	        4
#define RT			5

#endif /* DEFINES_H_ */
