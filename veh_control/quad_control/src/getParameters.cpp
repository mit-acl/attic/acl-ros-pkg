/*
 * getParameters.cpp
 *
 *  Created on: Jun 1, 2012
 *      Author: mark
 */

#include "quadrotor.hpp"

// Get parameters from ROS Param Server
void Quadrotor::getParams()
{

	// Parameters that might get changed on the fly

	// Set flags
	flags.streamData = raven::param_bool("streamData", true);
	flags.screenPrint = raven::param_bool("screenPrint", true);
	flags.demo = raven::param_bool("demo", true);
	flags.attitude_mode = raven::param_bool("joyAttitude", true);


	// OuterLoop raven::parameters
	double kp_xy = raven::param_double("Kp/xy", true);
	double ki_xy = raven::param_double("Ki/xy", true);
	double kd_xy = raven::param_double("Kd/xy", true);
	gains.Kp.setX(kp_xy);
	gains.Kp.setY(kp_xy);
	gains.Ki.setX(ki_xy);
	gains.Ki.setY(ki_xy);
	gains.Kd.setX(kd_xy);
	gains.Kd.setY(kd_xy);

	double kp_z = raven::param_double("Kp/z", true);
	double ki_z = raven::param_double("Ki/z", true);
	double kd_z = raven::param_double("Kd/z", true);
	gains.Kp.setZ(kp_z);
	gains.Ki.setZ(ki_z);
	gains.Kd.setZ(kd_z);

	// Parameters that we are only going to read once
	if (not parameters_read)
	{
		parameters_read = true;

		flags.use_xbee = raven::param_bool("useXbee");

		double maxposerr_xy = raven::param_double("maxPosErr/xy");
		double maxposerr_z  = raven::param_double("maxPosErr/z");
		double maxvelerr_xy = raven::param_double("maxVelErr/xy");
		double maxvelerr_z  = raven::param_double("maxVelErr/z");
		gains.maxPosErr.setX(maxposerr_xy);
		gains.maxPosErr.setY(maxposerr_xy);
		gains.maxPosErr.setZ(maxposerr_z);
		gains.maxVelErr.setX(maxvelerr_xy);
		gains.maxVelErr.setY(maxvelerr_xy);
		gains.maxVelErr.setZ(maxvelerr_z);

		gains.I_xy_int_thresh = raven::param_double("I_thresh/xy");
		gains.I_z_int_thresh = raven::param_double("I_thresh/z");

		gains.minRmag = raven::param_double("minRmag");
		gains.minRmagint = raven::param_double("minRmagint");

		gains.a = raven::param_double("f2cmd/a");
		gains.b = raven::param_double("f2cmd/b");
		gains.c = raven::param_double("f2cmd/c");

		// InnerLoop raven::parameters
		double kpr = raven::param_double("Kp/roll");
		double kir = raven::param_double("Ki/roll");
		double kdr = raven::param_double("Kd/roll");
		innerGains.Kp.setX(kpr);
		innerGains.Ki.setX(kir);
		innerGains.Kd.setX(kdr);

		double kpp = raven::param_double("Kp/pitch");
		double kip = raven::param_double("Ki/pitch");
		double kdp = raven::param_double("Kd/pitch");
		innerGains.Kp.setY(kpp);
		innerGains.Ki.setY(kip);
		innerGains.Kd.setY(kdp);

		double kpy = raven::param_double("Kp/yaw");
		double kiy = raven::param_double("Ki/yaw");
		double kdy = raven::param_double("Kd/yaw");
		innerGains.Kp.setZ(kpy);
		innerGains.Ki.setZ(kiy);
		innerGains.Kd.setZ(kdy);

		double total_trim = raven::param_double("PWM/total_trim");
		innerGains.PWM_trim[0] = raven::param_double("PWM/trim1") + total_trim;
		innerGains.PWM_trim[1] = raven::param_double("PWM/trim2") + total_trim;
		innerGains.PWM_trim[2] = raven::param_double("PWM/trim3") + total_trim;
		innerGains.PWM_trim[3] = raven::param_double("PWM/trim4") + total_trim;

		innerGains.M_trim[0] = raven::param_double("M/trim1");
		innerGains.M_trim[1] = raven::param_double("M/trim2");
		innerGains.M_trim[2] = raven::param_double("M/trim3");
		innerGains.M_trim[3] = raven::param_double("M/trim4");
						
		innerGains.maxang = raven::param_double("maxang");
		innerGains.lowBatt = raven::param_double("lowBatt");

		// on-board calibration data
		calData.gyroScale = raven::param_double("gyroScale");
		calData.accelScale = raven::param_double("accelScale");
		calData.K_Att_Filter = raven::param_double("K_Att_Filter") * controlDT;
		calData.kGyroBias = raven::param_double("kGyroBias") * controlDT;

		// Serial port
		if (flags.use_xbee)
		{
			serialPort = raven::param_string("serialPort");
			baudRate = (int) raven::param_double("baudRate");
		} else {
			// UDP port
			ip_address = raven::param_string("ipAddress");
			rxport = (int) raven::param_double("rxport");
			txport = (int) raven::param_double("txport");
		}

		// Quadrotor parameters
		mass = raven::param_double("mass");

		if (!name.compare("/UQ01") || (!name.compare("/UQ02"))
				|| (!name.compare("/thrust")))
		{
			innerGains.motorFF = raven::param_double("motorFF");
		}

	}
}
