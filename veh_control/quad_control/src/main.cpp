/*
 * main.cpp
 *
 *  Created on: Mar 26, 2011
 *      Author: mark
 */

// Global includes
#include <iostream>
#include <signal.h>

// Local includes
#include "quadrotor.hpp"
#include "defines.h"

// Global vars
bool ABORT = false;
int ABORT_cnt = -20;

// Prototypes
void controlC(int sig);

int main(int argc, char **argv)
{

	// Initialize ROS and node n -- generic name is "Quad", overwritten by name remapping from command line
	ros::init(argc, argv, "cntrl", ros::init_options::NoSigintHandler);
	ros::NodeHandle n;

	// Check for proper renaming of node
	if (!ros::this_node::getNamespace().compare("/"))
	{
		ROS_ERROR(
				"Error :: You should be using a launch file to specify the node namespace!\n");
		return (-1);
	}

	//## Welcome screen
	ROS_INFO("\n\nStarting ROS_RAVEN_code for %s...\n\n\n",
			ros::this_node::getNamespace().c_str());

	// Initialize vehicle
	Quadrotor quad;

	// initialize listener callbacks that pickup the vicon data
	ros::Subscriber sub_pose = n.subscribe("pose", 1, &Quadrotor::poseCallback,
			&quad);
	ros::Subscriber sub_vel = n.subscribe("vel", 1, &Quadrotor::velCallback,
			&quad);
	ros::Subscriber sub_acc = n.subscribe("accel", 1, &Quadrotor::accelCallback,
			&quad);

	// initialize main controller loop
	ros::Timer controllerTimer = n.createTimer(ros::Duration(controlDT),
			&Quadrotor::runController, &quad);

	// initialize listener callback that subscribes to goal data
	ros::Subscriber sub_goal = n.subscribe("goal", 1, &Quadrotor::goalCallback,
			&quad);

	// publish controller and goal commands
	quad.cmds = n.advertise<acl_msgs::QuadCmd>("cmds", 5);
	quad.health = n.advertise<acl_msgs::QuadHealth>("health", 5);
	quad.sensorPub = n.advertise<acl_msgs::QuadRawSensors>("sensors", 5);

	//## Setup the CTRL-C trap
	signal(SIGINT, controlC);

	sleep(0.5);

	// run the code
	// start asynchronous spinner
	ros::AsyncSpinner spinner(4); // Use 4 threads
	spinner.start();
	ros::waitForShutdown();

	return 0;
}

//## Custom Control-C handler
void controlC(int sig)
{
	ABORT = true; // ros::shutdown called in Quadrotor::sendCmd() function
}
