//============================================================================
// Name        : processQuadLogFile.cpp
// Author      : Mark Cutler
// Version     :
// Copyright   : ACL MIT, 6/15/2012
// Description : Process quadrotor binary log file and create a human-readable version
//============================================================================

// This function accepts a log file generated on board the quadrotor
// and produces a human readable text file in quad_control/matlab/quadOnBoardLog.txt

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <string>
#include <stdlib.h>

// ROS includes
#include <ros/ros.h>
#include <ros/package.h>

//## communications defines -- must be the same values on embedded code
#define QUAT_SCALE			10000.0  	// value to scale quaternions by for sending as int
#define ROT_SCALE			100.0		// value to scale rotations (p,q,r) by for sending as int
#define GAINS_SCALE			1000.0		// value to scale gains by for sending as int
#define PACKETID_LOG 0x06
typedef struct __attribute__((packed)) sLogPacket
{
  int16_t p_est;
  int16_t q_est;
  int16_t r_est;
  int16_t qo_est;
  int16_t qx_est;
  int16_t qy_est;
  int16_t qz_est;
  int16_t p_cmd;
  int16_t q_cmd;
  int16_t r_cmd;
  int16_t qo_cmd;
  int16_t qx_cmd;
  int16_t qy_cmd;
  int16_t qz_cmd;
  uint8_t throttle;
  int8_t collective;
  uint8_t attStatus;
} tLogPacket;
std::string LogHeader =
    "p_est,q_est,r_est,qo_est,qx_est,qy_est,qz_est,p_cmd,q_cmd,r_cmd,qo_cmd,qx_cmd,qy_cmd,qz_cmd,throttle,collective,attStatus\r\n";

#define PACKETID_MOTORCMDS 0x0A
typedef struct __attribute__((packed)) sMotorCmdsPacket
{
  uint8_t m1;
  uint8_t m2;
  uint8_t m3;
  uint8_t m4;
  uint8_t throttle;
} tMotorCmdsPacket;
std::string MotorHeader = "m1,m2,m3,m4,throttle\r\n";

struct sGoal
{
  double p_est, q_est, r_est;
  double qo_est, qx_est, qy_est, qz_est;
  double p_cmd, q_cmd, r_cmd;
  double qo_cmd, qx_cmd, qy_cmd, qz_cmd;
  double throttle, collective;
  int attStatus;

};

struct sMotorCmds
{
  double m1, m2, m3, m4, throttle;

};

tLogPacket logPacket;
tMotorCmdsPacket motorPacket;
sGoal goal;
sMotorCmds motor;

void parsePacket(uint8_t packetID, uint8_t * data)
{
  switch (packetID)
  {
    case PACKETID_LOG:

      memcpy(&logPacket, data, sizeof(tLogPacket));

      goal.p_est = (double)logPacket.p_est / ROT_SCALE;
      goal.q_est = (double)logPacket.q_est / ROT_SCALE;
      goal.r_est = (double)logPacket.r_est / ROT_SCALE;
      goal.qo_est = (double)logPacket.qo_est / QUAT_SCALE;
      goal.qx_est = (double)logPacket.qx_est / QUAT_SCALE;
      goal.qy_est = (double)logPacket.qy_est / QUAT_SCALE;
      goal.qz_est = (double)logPacket.qz_est / QUAT_SCALE;

      goal.p_cmd = (double)logPacket.p_cmd / ROT_SCALE;
      goal.q_cmd = (double)logPacket.q_cmd / ROT_SCALE;
      goal.r_cmd = (double)logPacket.r_cmd / ROT_SCALE;
      goal.qo_cmd = (double)logPacket.qo_cmd / QUAT_SCALE;
      goal.qx_cmd = (double)logPacket.qx_cmd / QUAT_SCALE;
      goal.qy_cmd = (double)logPacket.qy_cmd / QUAT_SCALE;
      goal.qz_cmd = (double)logPacket.qz_cmd / QUAT_SCALE;

      goal.throttle = (double)logPacket.throttle;
      goal.collective = (double)logPacket.collective;
      goal.attStatus = logPacket.attStatus;

      break;

    case PACKETID_MOTORCMDS:

      memcpy(&motorPacket, data, sizeof(tMotorCmdsPacket));

      motor.m1 = (double)motorPacket.m1;
      motor.m2 = (double)motorPacket.m2;
      motor.m3 = (double)motorPacket.m3;
      motor.m4 = (double)motorPacket.m4;
      motor.throttle = (double)motorPacket.throttle;

      break;

    default:
      break;
  }
}

int main(int argc, char *argv[])
{
  std::string fileName("defaultfile.txt");
  if (argc == 2)
  {
    fileName = argv[1];
  }
  else
  {
    printf("Need to specify file to process.\n");
    printf("Example:  rosrun quad_control processOnBoardLogFile log.txt\n\n");
    return -1;
  }

  // Open file for human readable format textfile
  std::string path = ros::package::getPath("quad_control");
  path += "/matlab/quadOnBoardLog.txt";

  FILE * pFile;
  long lSize;
  char * buffer;
  size_t result;

  pFile = fopen(fileName.c_str(), "rb");
  printf("Processing %s\n", fileName.c_str());

  if (pFile == NULL)
  {
    fputs("File error -- couldn't open file", stderr);
    exit(1);
  }

  // obtain file size:
  fseek(pFile, 0, SEEK_END);
  lSize = ftell(pFile);
  rewind(pFile);

  // allocate memory to contain the whole file:
  buffer = (char*)malloc(sizeof(char) * lSize);
  if (buffer == NULL)
  {
    fputs("Memory error", stderr);
    exit(2);
  }

  // copy the file into the buffer:
  result = fread(buffer, 1, lSize, pFile);
  if (result != lSize)
  {
    fputs("Reading error", stderr);
    exit(3);
  }

  /* the whole file is now loaded in the memory buffer. */

  // close file
  fclose(pFile);

  // Open file for human readable format textfile
  FILE * writeFile;
  writeFile = fopen(path.c_str(), "w");
  char outStr[512];

  // Process File
  uint8_t packetID;
  uint8_t packetLength;
  uint8_t packetSeq;
  uint8_t packetData[256];
  uint8_t checksum;

  uint8_t thisByte = 0;
  uint8_t lastByte = 0;

  uint8_t STX1 = 0xFF;
  uint8_t STX2 = 0xFE;

  bool DONE = false;
  int cnt = 0;
  int linesConverted = 0;
  bool headerWritten = false;
  while (not DONE)
  {

    // Wait for packet start
    while (!(thisByte == STX2 && lastByte == STX1))
    {
      lastByte = thisByte;
      thisByte = *buffer;
      buffer++;
      if (cnt > 100)
      {
        DONE = true;
        break;
      }
      cnt++;
    }
    cnt = 0; // got good packet start bytes
    thisByte = lastByte = 0;

    // Get packet ID and length
    packetID = *buffer;
    buffer++;
    packetLength = *buffer;
    buffer++;
    packetSeq = *buffer;
    buffer++;
    checksum = packetLength + packetID + packetSeq;

    // Get packet data
    for (int i = 0; i < packetLength; i++)
    {
      packetData[i] = *buffer;
      buffer++;
      checksum += packetData[i];
    }

    // Get checksum
    checksum += *buffer;

    // Parse packet if checksum correct
    if (checksum == 0)
    {
      parsePacket(packetID, packetData);
      switch (packetID)
      {
        case PACKETID_LOG:
          if (not headerWritten)
          {
            sprintf(outStr, LogHeader.c_str());
            fputs(outStr, writeFile);
            headerWritten = true;
          }
          sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d\r\n", goal.p_est, goal.q_est, goal.r_est,
                  goal.qo_est, goal.qx_est, goal.qy_est, goal.qz_est, goal.p_cmd, goal.q_cmd, goal.r_cmd, goal.qo_cmd,
                  goal.qx_cmd, goal.qy_cmd, goal.qz_cmd, goal.throttle, goal.collective, goal.attStatus);
          fputs(outStr, writeFile);
          linesConverted++;
          break;
        case PACKETID_MOTORCMDS:
          if (not headerWritten)
          {
            sprintf(outStr, MotorHeader.c_str());
            fputs(outStr, writeFile);
            headerWritten = true;
          }
          sprintf(outStr, "%f,%f,%f,%f,%f\r\n", motor.m1, motor.m2, motor.m3, motor.m4, motor.throttle);
          fputs(outStr, writeFile);
          linesConverted++;
          break;
        default:
          break;
      }
    }
    else
    {
      printf("Bad checksum! \n");
    }

  }

  fclose(writeFile);
  printf("Log file successfully converted %d lines\n", linesConverted);
  return 0;
}
