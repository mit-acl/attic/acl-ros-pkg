/*
 * quadrotor.cpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#include "quadrotor.hpp"

// Initialize Quadrotor Vehicle
Quadrotor::Quadrotor()
{

	// Get vehicle name
	name = ros::this_node::getNamespace();
	parameters_read = false;
	cntrl_cnt = 0;
	// pull off leading slash
//	name.erase(0,1);

	std::cout<< name << std::endl;
	sim = false;
	// if (name.back() == 's')
	if (*name.rbegin() == 's') //For C++03
	{
		sim = true;
		ROS_INFO_STREAM("This is a simulated vehicle -- simulating hardware handshake");
		healthData.voltage = 12.0;
	}

	// Initialize integrators
	I_x = Integrator();
	I_y = Integrator();
	I_z = Integrator();

	// Initialize structs
	gains = sOuterGains();
	goal = sGoal();
	state = sState();
	calData = sCalibrationData();
	innerGains = sInnerGains();
	flags = sFlags();

	// Initialize variables
	emergencyLand = false;
	yawInitialized = false;
	averages_set = false;
	attStatus = DISABLED;
	throttle = 0.0;
	accel_fb_old.setZero();
	jerk_fb_old.setZero();
	t_old = 0.0;
	qmin_old = qmin_old.getIdentity();

	// get parameters from param server
	getParams();

	if (flags.use_xbee)
	{
	//## Start serial
	if (not sim and not ser.spInitialize(serialPort.c_str(), baudRate, true))
	  {
		ROS_ERROR("Serial port failed to open");
		ros::shutdown();
	  }
	} else
	{
		// first setup the wifly to make sure it is talking to this computer
		raven::set_wifly_host_ip_telnet(ip_address, txport);

		if (not sim and not udp.udpInit(true, (char *) ip_address.c_str(), rxport, txport))
	  {
		ROS_ERROR("UDP port failed to open");
		ros::shutdown();
	  }
	}

	//## Start a listen thread
	pthread_t threads;
	if (not sim and pthread_create(&threads, NULL, listen, (void *) this))
	  {
		ROS_ERROR("Listen thread failed to start");
	  }

	sendInit();

}

void Quadrotor::sendInit(void)
{
	// Send on-board gains to quadrotor
	for (int i = 0; i < 5; i++)
	{
		sendCal();
		sleep(0.1);
	}

	for (int i = 0; i < 5; i++)
	{
		sendGains();
		sleep(0.1);
	}
}

// Main control loop
void Quadrotor::runController(const ros::TimerEvent& e)
{
	// Get parameters from param server -- really only gets anything when parameters are changed
	getParams();

	// outer loop controller
	if (not emergencyLand and not flags.attitude_mode)
	{
		tf::Vector3 F = getAccel();
		goal.att = getAttitude(F);
		goal.rate = getRate(F);
		// convert F to motor commands, set throttle
		throttle = F2motorCmd(goal.f_total);
	}
	else if (flags.attitude_mode)
	{
		tf::Vector3 F = getAccel();
		tf::Quaternion temp;
		temp = getAttitude(F);
		// convert F to motor commands, set throttle
		throttle = F2motorCmd(goal.f_total);
	}


	//send data
	checkStatus();
	sendCmd();

	cntrl_cnt++;
	if (cntrl_cnt % 100 == 0)
		sendEmergency();

	// broadcast calculated data as ros messages for loggin' and debuggin'
	broadcastROSdata();

	// print pertinent data to screen
	if (flags.screenPrint)
		screenPrint();

}

// check the status of the vehicle and set desired values to zero if not good
void Quadrotor::checkStatus()
{
	bool shouldFly = true;
	errorMsg.str(""); // clear string
	warnMsg.str(""); // clear string

	// initialization routine
	double spinup_time = 5.;
	if (sim)
		spinup_time = 0.5;
	if (startTime.now().toSec() < (startTime.toSec() + spinup_time))
	{
		goal.rate.setZero();
		I_x.reset();
		I_y.reset();
		I_z.reset();
		if (not yawInitialized)
		{
			goal.yaw = tf::getYaw(state.att); //set goal yaw to initial yaw
			if (goal.yaw != 0.0)
			{
				yawInitialized = true; // this function could be called before the vicon callbacks.  Real yaw data will not be equal to 0
			}
		}
		goal.att.setRPY(0.0, 0.0, goal.yaw);
		throttle = 0.2;
	}

	// check for communication with the quadrotor
	if (healthData.voltage == 0)
	{
		warnMsg
				<< "Not communicating with Quadrotor -- try checking xbee connection"
				<< std::endl;
		//shouldFly = false;
	}

	// Check that we are actually receiving vicon data
	if (state.vel.getX() == 0.0 && state.vel.getY() == 0.0
			&& state.vel.getZ() == 0.0)
	{
		errorMsg
				<< "Not getting vehicle Vicon data. Is ROSTracker running or is the vehicle occluded?"
				<< std::endl;
		shouldFly = false;
	} else
		shouldFly = true;

	// Check that the vicon data we have is current
	if ((ros::Time::now().toSec() - state.time.toSec())
			> MAX_VICON_WAIT_TIME)
		emergencyLand = true;
	else
		emergencyLand = false;

	if (emergencyLand)
	{
		errorMsg
				<< "Emergency Landing! Haven't received Vicon information recently!"
				<< std::endl;

		//goal.att.setRPY(0.0, 0.0, goal.yaw);
		// Get the acceleration command just from the intergators--this is the 
				// nominal hover command
		tf::Vector3 int_err;
		int_err.setValue(I_x.value, I_y.value, I_z.value);
		tf::Vector3 F_int = gains.Ki * int_err;
		tf::Vector3 F;
		F.setX(mass * F_int.getX());
		F.setY(mass * F_int.getY());
		F.setZ(mass * GRAVITY);
		goal.att = getAttitude(F);
		goal.rate.setZero();
		F.setZ(mass * (F_int.getZ() + GRAVITY));
		double throttle_avg = F2motorCmd(F.length());
		if (not averages_set)
		{
			averages_set = true;
			throttle = throttle_avg;
		}
		throttle = acl::saturate(throttle - 0.0001, 0.9, 0.0); // slowly decrease throttle
		if (throttle < 0.1)
		{
			// shut off the motors
			attStatus = DISABLED;
		}

	}

	// If you shouldn't be flying, zero all the commands
	if (not shouldFly or attStatus == DISABLED)
	{
		throttle = 0.0;
		attStatus = DISABLED;
		goal.att = goal.att.getIdentity();
		goal.rate.setZero();
		I_x.reset();
		I_y.reset();
		I_z.reset();
		goal.f_total = 0.0;
		goal.pos.setZero();
		goal.vel.setZero();
		goal.accel.setZero();
		goal.jerk.setZero();
		goal.accel_fb.setZero();
		goal.jerk_fb.setZero();
		goal.accel_int.setZero();
	}
}

// broadcast all pertinent control data as ros messages for logging and debugging
void Quadrotor::broadcastROSdata()
{

	/******** Command Data *********/
	acl_msgs::QuadCmd cmd;

	// Header
	cmd.header.stamp = ros::Time::now();
	cmd.header.frame_id = name;

	// Commanded values
	cmd.pose.position.x = goal.pos.getX();
	cmd.pose.position.y = goal.pos.getY();
	cmd.pose.position.z = goal.pos.getZ();
	tf::quaternionTFToMsg(goal.att, cmd.pose.orientation);
	tf::vector3TFToMsg(goal.vel, cmd.twist.linear);
	tf::vector3TFToMsg(goal.rate, cmd.twist.angular);
	tf::vector3TFToMsg(goal.accel, cmd.accel);
	tf::vector3TFToMsg(goal.accel_fb, cmd.accel_fb);
	tf::vector3TFToMsg(goal.jerk, cmd.jerk);
	tf::vector3TFToMsg(goal.jerk_fb, cmd.jerk_fb);
	cmd.pos_integrator.x = I_x.value;
	cmd.pos_integrator.y = I_y.value;
	cmd.pos_integrator.z = I_z.value;
	cmd.f_total = goal.f_total;
	cmd.throttle = throttle;
	cmd.att_status = attStatus;

	double r, p, y;
	quaternion2Euler(goal.att, r, p, y);
	cmd.rpy.x = r; // roll
	cmd.rpy.y = p; // pitch
	cmd.rpy.z = y; // yaw

	cmds.publish(cmd);

	/************ Health Data **********/
	healthData.header.stamp = ros::Time::now();
	healthData.header.frame_id = name;

	quaternion2Euler(healthData.att, r, p, y);
	healthData.rpy.x = r; // roll
	healthData.rpy.y = p; // pitch
	healthData.rpy.z = y; // yaw

	if (flags.streamData) // if we are streaming data, use it, otherwise
	{ // rebroadcast vicon attitude in euler angles
		quaternion2Euler(healthData.att_meas, r, p, y);
		healthData.rpy_meas.x = r; // roll
		healthData.rpy_meas.y = p; // pitch
		healthData.rpy_meas.z = y; // yaw
	}
	else
	{
		quaternion2Euler(state.att, r, p, y);
		healthData.rpy_meas.x = r; // roll
		healthData.rpy_meas.y = p; // pitch
		healthData.rpy_meas.z = y; // yaw
	}

	health.publish(healthData);

}

void Quadrotor::screenPrint()
{
	if (not errorMsg.str().empty())
		ROS_ERROR_STREAM_THROTTLE(SCREEN_PRINT_RATE, errorMsg.str());

	if (not warnMsg.str().empty())
		ROS_WARN_STREAM_THROTTLE(SCREEN_PRINT_RATE, warnMsg.str());

	std::ostringstream msg;
	msg.setf(std::ios::fixed); // give all the doubles the same precision
	msg.setf(std::ios::showpos); // show +/- signs always
	msg << std::setprecision(4) << std::endl; // set precision to 4 decimal places
	msg << "Act Pos:  x: " << state.pos.getX() << "  y: " << state.pos.getY()
			<< "  z: " << state.pos.getZ() << std::endl;
	msg << "Des Pos:  x: " << goal.pos.getX() << "  y: " << goal.pos.getY()
			<< "  z: " << goal.pos.getZ() << std::endl << std::endl;

	msg << "Act Vel:  x: " << state.vel.getX() << "  y: " << state.vel.getY()
			<< "  z: " << state.vel.getZ() << std::endl;
	msg << "Des Vel:  x: " << goal.vel.getX() << "  y: " << goal.vel.getY()
			<< "  z: " << goal.vel.getZ() << std::endl << std::endl;

	double r, p, y;
	quaternion2Euler(state.att, r, p, y);
	msg << "Act Att:  r: " << r << "  p: " << p << "  y: " << y << std::endl;
	quaternion2Euler(goal.att, r, p, y);
	msg << "Des Att:  r: " << r << "  p: " << p << "  y: " << y << std::endl
			<< std::endl;

	msg << "Act Rate: p: " << state.rate.getX() << "  q: " << state.rate.getY()
			<< "  r: " << state.rate.getZ() << std::endl;
	msg << "Des Rate: p: " << goal.rate.getX() << "  q: " << goal.rate.getY()
			<< "  r: " << goal.rate.getZ() << std::endl << std::endl;

	msg << "Thrust: " << goal.f_total << "  Throttle: " << throttle
			<< std::endl;
	msg << "AttStatus " << attStatus << "  Voltage:  " << healthData.voltage
			<< std::endl << std::endl;

	msg << "Seconds since last Vicon message: "
			<< ros::Time::now().toSec() - state.time.toSec();
	msg << std::endl << std::endl;

	ROS_INFO_STREAM_THROTTLE(SCREEN_PRINT_RATE, msg.str());
	// Print at 1/0.5 Hz = 2 Hz

}

// Get POSE messages from Vicon
void Quadrotor::poseCallback(const geometry_msgs::PoseStamped& msg)
{

	//ros::Time msg_time = msg.header.stamp;
	//double delay = msg_time.now().toSec() - msg_time.toSec();
	//printf("delay: %f\n",delay);

	// check that the data we received is different from the data last time
	if (not ((msg.pose.position.x == state.pos.getX())
			and (msg.pose.position.y == state.pos.getY())))
	{
		state.time = ros::Time::now(); // get current time -- used in check status to see if vicon data is current
		// time is only evaluated if we are getting new data
	}

	// update state variables
	geometry_msgs::Vector3 position;
	position.x = msg.pose.position.x;
	position.y = msg.pose.position.y;
	position.z = msg.pose.position.z;
	tf::vector3MsgToTF(position, state.pos);
	tf::quaternionMsgToTF(msg.pose.orientation, state.att);

}

// Get TWIST messages from Vicon
void Quadrotor::velCallback(const geometry_msgs::TwistStamped& msg)
{

	// update state variables
	geometry_msgs::Vector3 vel;
	vel.x = msg.twist.linear.x;
	vel.y = msg.twist.linear.y;
	vel.z = msg.twist.linear.z;

	geometry_msgs::Vector3 rate;
	rate.x = msg.twist.angular.x;
	rate.y = msg.twist.angular.y;
	rate.z = msg.twist.angular.z;

	tf::vector3MsgToTF(vel, state.vel);
	tf::vector3MsgToTF(rate, state.rate);

}

// Get ACCEL messages from Vicon
void Quadrotor::accelCallback(const geometry_msgs::Vector3Stamped& msg)
{
	// update state variables
	geometry_msgs::Vector3 accel;
	accel.x = msg.vector.x;
	accel.y = msg.vector.y;
	accel.z = msg.vector.z;

	tf::vector3MsgToTF(accel, state.accel);

}


// room bound filter
// determines whether goal is valid
acl_msgs::QuadGoal Quadrotor::checkRoomBound(
		const acl_msgs::QuadGoal& msg, int mode)
{
	// parameters defining the room
	double x_min = -7.5;
	double x_max = 1.1;
	double y_min = -6;
	double y_max = 1.2;
	double x_corner = -1.5;
	double y_corner = -1.8;
	double x_min_pole = -3.9;
	double x_max_pole = -2.6;
	double y_min_pole = -5;
	double y_max_pole = -3.7;

	double x_cur = state.pos.getX();
	double y_cur = state.pos.getY();

	acl_msgs::QuadGoal goal = msg;

	// whether the message has been changed
	bool msg_changed = false;

	// default, L-shape
	if (mode == 1)
	{
		// ************* absolute bounding box ******************//
		// x_min
		if (goal.pos.x < x_min)
		{
			goal.pos.x = x_min;
			msg_changed = true;
		}
		// x_max
		if (goal.pos.x > x_max)
		{
			goal.pos.x = x_max;
			msg_changed = true;
		}
		// y_min
		if (goal.pos.y < y_min)
		{
			goal.pos.y = y_min;
			msg_changed = true;
		}
		// y_max
		if (goal.pos.y > y_max)
		{
			goal.pos.y = y_max;
			msg_changed = true;
		}

		// ************* unallowed corner ******************//
		if (goal.pos.x < x_corner && goal.pos.y > y_corner)
		{
			msg_changed = true;
			float dist_x = fabs(goal.pos.x - x_corner);
			float dist_y = fabs(goal.pos.y - y_corner);
			if (dist_x >= dist_y)
				goal.pos.y = y_corner;
			else
				goal.pos.x = x_corner;
		}

		// ************* near the pole ******************//
		if (goal.pos.x > x_min_pole && goal.pos.x < x_max_pole
				&& goal.pos.y > y_min_pole && goal.pos.y < y_max_pole)
		{
			msg_changed = true;
			double dist_x_min = fabs(goal.pos.x - x_min_pole);
			double dist_x_max = fabs(goal.pos.x - x_max_pole);
			double dist_y_min = fabs(goal.pos.y - y_min_pole);
			double dist_y_max = fabs(goal.pos.y - y_max_pole);

			// correct in x
			if (std::min(dist_x_min, dist_x_max)
					<= std::min(dist_y_min, dist_y_max))
			{
				if (dist_x_min < dist_x_max)
					goal.pos.x = x_min_pole;
				else
					goal.pos.x = x_max_pole;
			}
			else // correct in y
			{
				if (dist_y_min < dist_y_max)
					goal.pos.y = y_min_pole;
				else
					goal.pos.y = y_max_pole;
			}
		}

		// check whether the line between the current quad position and its goal goes through the infeasible region
		int if_collide = 0;
		if_collide += checkHline(x_cur, y_cur, goal.pos.x, goal.pos.y, x_min,
				x_corner, y_corner);
		if_collide += checkHline(x_cur, y_cur, goal.pos.x, goal.pos.y,
				x_min_pole, x_max_pole, y_min_pole);
		if_collide += checkHline(x_cur, y_cur, goal.pos.x, goal.pos.y,
				x_min_pole, x_max_pole, y_max_pole);
		if_collide += checkVline(x_cur, y_cur, goal.pos.x, goal.pos.y, y_max,
				y_corner, x_corner);
		if_collide += checkVline(x_cur, y_cur, goal.pos.x, goal.pos.y,
				y_min_pole, y_max_pole, x_min_pole);
		if_collide += checkVline(x_cur, y_cur, goal.pos.x, goal.pos.y,
				y_min_pole, y_max_pole, x_max_pole);
		if (if_collide > 0)
		{
			msg_changed = true;
			goal.pos.x = x_cur;
			goal.pos.y = y_cur;
			warnMsg << "trying to cut through infeasible region" << std::endl;
		}

		// check whether the current position is feasible
		// ************* absolute bounding box ******************//
		// x_min
		double tol = 0.2;
		if (x_cur < x_min)
		{
			goal.pos.x = x_min + tol;
			msg_changed = true;
		}
		// x_max
		if (x_cur > x_max)
		{
			goal.pos.x = x_max - tol;
			msg_changed = true;
		}
		// y_min
		if (y_cur < y_min)
		{
			goal.pos.y = y_min + tol;
			msg_changed = true;
		}
		// y_max
		if (y_cur > y_max)
		{
			goal.pos.y = y_max - tol;
			msg_changed = true;
		}

		// ************* unallowed corner ******************//
		if (x_cur < x_corner && y_cur > y_corner)
		{
			msg_changed = true;
			float dist_x = fabs(x_cur - x_corner);
			float dist_y = fabs(y_cur - y_corner);
			if (dist_x >= dist_y)
				goal.pos.y = y_corner - tol;
			else
				goal.pos.x = x_corner + tol;
		}

		// ************* near the pole ******************//
		if (x_cur > x_min_pole && x_cur < x_max_pole && y_cur > y_min_pole
				&& y_cur < y_max_pole)
		{
			msg_changed = true;
			double dist_x_min = fabs(x_cur - x_min_pole);
			double dist_x_max = fabs(x_cur - x_max_pole);
			double dist_y_min = fabs(y_cur - y_min_pole);
			double dist_y_max = fabs(y_cur - y_max_pole);

			/* std::cout << x_cur << '\t' << y_cur << '\t' << x_min_pole << '\t' <<
			 x_max_pole << '\t' << y_min_pole << '\t' << y_max_pole
			 << std::endl;
			 std::cout << dist_x_min << '\t' << dist_x_max << '\t' << dist_y_min << '\t' <<
			 dist_y_max << '\t' << std::min(dist_x_min, dist_x_max) << '\t' <<
			 std::min(dist_y_min, dist_y_max) << std::endl; */
			// correct in x
			if (std::min(dist_x_min, dist_x_max)
					<= std::min(dist_y_min, dist_y_max))
			{
				if (dist_x_min < dist_x_max)
					goal.pos.x = x_min_pole - tol;
				else
					goal.pos.x = x_max_pole + tol;
			}
			else // correct in y
			{
				if (dist_y_min < dist_y_max)
					goal.pos.y = y_min_pole - tol;
				else
					goal.pos.y = y_max_pole + tol;
			}
		}
	}

	// box shape
	else
	{
		x_min = x_corner;
		// x_min
		if (goal.pos.x < x_min)
		{
			goal.pos.x = x_min;
			msg_changed = true;
		}
		// x_max
		if (goal.pos.x > x_max)
		{
			goal.pos.x = x_max;
			msg_changed = true;
		}
		// y_min
		if (goal.pos.y < y_min)
		{
			goal.pos.y = y_min;
			msg_changed = true;
		}
		// y_max
		if (goal.pos.y > y_max)
		{
			goal.pos.y = y_max;
			msg_changed = true;
		}

	}

	// handle vel,accel,jerk
	if (msg_changed == true)
	{
		goal.vel.x = 0;
		goal.vel.y = 0;
		goal.vel.z = 0;
		goal.accel.x = 0;
		goal.accel.y = 0;
		goal.accel.z = 0;
		goal.jerk.x = 0;
		goal.jerk.y = 0;
		goal.jerk.z = 0;
	}

	return goal;

}

// check for collision between a line between two points and a horizontal line
bool Quadrotor::checkHline(double x1, double y1, double x2, double y2,
		double x3, double x4, double y34)
{
	// check if horizontal line
	if (y1 == y2)
		return false;
	// x = ay + b;
	double a = (x1 - x2) / (y1 - y2);
	double b = x1 - a * y1;
	double x_int = a * y34 + b;
	if (x_int < std::max(x3, x4) && x_int > std::min(x3, x4)
			&& x_int < (std::max(x1, x2) - 0.0001)
			&& x_int > (std::min(x1, x2) + 0.0001))
	{
		//std::cout << x1 << '\t' << y1 << '\t'
		//     << x2 << '\t' << y2 << '\t'
		//    << x3 << '\t' << x4 << '\t'
		//     << y34 << std::endl;
		return true;
	}
	else
		return false;
}

// check for collision between a line between two points and a vertical line
bool Quadrotor::checkVline(double x1, double y1, double x2, double y2,
		double y3, double y4, double x34)
{
	// check if horizontal line
	if (x1 == x2)
		return false;
	// y = ax + b;
	double a = (y1 - y2) / (x1 - x2);
	double b = y1 - a * x1;
	double y_int = a * x34 + b;

	if (y_int < std::max(y3, y4) && y_int > std::min(y3, y4)
			&& y_int < (std::max(y1, y2) - 0.0001)
			&& y_int > (std::min(y1, y2) + 0.0001))
	{
		//std::cout << x1 << '\t' << y1 << '\t'
		//     << x2 << '\t' << y2 << '\t'
		//     << y3 << '\t' << y4 << '\t'
		//     << x34 << std::endl;
		//std::cout << a << '\t' << b << '\t'
		//     << y_int << std::endl;
		return true;
	}
	else
		return false;
}

// get goal states
void Quadrotor::goalCallback(const acl_msgs::QuadGoal& msg)
{

	// Note: ignoring room bound check here for now
	acl_msgs::QuadGoal filtered_goal = msg; //checkRoomBound(msg, 1);

	// goal convertion and bound checking in checkRoomBound()
	tf::vector3MsgToTF(filtered_goal.pos, goal.pos);
	tf::vector3MsgToTF(filtered_goal.vel, goal.vel);
	tf::vector3MsgToTF(filtered_goal.accel, goal.accel);
	tf::vector3MsgToTF(filtered_goal.jerk, goal.jerk);
	goal.yaw = filtered_goal.yaw;
	goal.dyaw = filtered_goal.dyaw;

	if (msg.waypointType == TAKEOFF && attStatus == DISABLED) // takeoff
	{
		ROS_INFO_STREAM("Takeoff command received");
		attStatus = FLYING;
		startTime = startTime.now();

		// Send on-board gains to quadrotor
		for (int i = 0; i < 2; i++)
		{
			sendCal();
			sleep(0.1);
			sendGains();
			sleep(0.1);
		}
	}
	else if (msg.waypointType == CUT_POWER) // disable
	{
		attStatus = DISABLED;
	}
	else if (msg.waypointType == RESET_XY_INT) // Reset xy integrators
	{
		I_x.reset();
		I_y.reset();
	}
	else if (msg.waypointType == RESET_XYZ_INT) // Reset xyz integrators
	{
		I_x.reset();
		I_y.reset();
		I_z.reset();
	}
	else if (msg.waypointType == ATTITUDE) // attitude/thrust mode
	{
		attStatus = FLYING;
		double roll_cmd = goal.pos.getX();
		double pitch_cmd = goal.pos.getY();
		double yaw_cmd = goal.yaw;
		goal.att.setRPY(roll_cmd, pitch_cmd, yaw_cmd);
		goal.rate.setX(goal.vel.getX());
		goal.rate.setY(goal.vel.getY());
		goal.rate.setZ(goal.dyaw);
		throttle = goal.accel.getX();
	}
}

// Get the saved bag file at the end of a flight and convert it to a MATLAB-readable file
int Quadrotor::writeDataLog2File()
{

	// open /tmp/ directory where bag files are saved
	DIR *dp;
	struct dirent *dirp;
	if ((dp = opendir("/tmp/")) == NULL)
	{
		ROS_ERROR("Error opening /tmp/");
		return -1;
	}

	// get a list of all the sensor bag files in that directory
	std::vector<sensorFile> filesVec;
	name.erase(0,1);
	int hl = name.size() + 7; // length of name + header
	while ((dirp = readdir(dp)) != NULL)
	{
		std::string fileName = dirp->d_name;
		std::string wantedName = name + "LogFile";
		if (wantedName.compare(fileName.substr(0, hl)) == 0)
		{

			// name fomat: BQ05sensors_2012-09-26-10-22-22.bag
			// header: 0,11 or 12 (starting at position 0, 11 chars long
			// year: 12,4
			// month: 17,2
			// day: 20,2
			// hour: 23,2
			// minute: 26,2
			// second: 29,2
			sensorFile sf;
			sf.name = "/tmp/" + fileName;
			sf.date.tm_year = (int) atoi(fileName.substr(hl+1, 4).c_str()) - 1900;
			sf.date.tm_mon = (int) atoi(fileName.substr(hl+6, 4).c_str()) - 1;
			sf.date.tm_mday = (int) atoi(fileName.substr(hl+9, 2).c_str());
			sf.date.tm_hour = (int) atoi(fileName.substr(hl+12, 2).c_str());
			sf.date.tm_min = (int) atoi(fileName.substr(hl+15, 2).c_str());
			sf.date.tm_sec = (int) atoi(fileName.substr(hl+18, 2).c_str());
			time_t epochtime;
			epochtime = mktime(&sf.date);
			sf.time = (int)epochtime;
			filesVec.push_back(sf);
		}
	}
	closedir(dp);

	// sort that list to get the one with the latest time stamp
	if (filesVec.empty())
	{
		ROS_ERROR("Couldn't find bag file to process");
		return -1;
	}
	std::sort(filesVec.begin(), filesVec.end(), sensorFile());
	sensorFile file2Parse = filesVec.back();
	std::cout << std::endl << "Parsing bag file " << file2Parse.name
			<< std::endl << std::endl;

	rosbag::Bag bag;
	bag.open(file2Parse.name, rosbag::bagmode::Read);

	// Open file for human readable format textfile
	std::string path = ros::package::getPath("quad_control");
	path += "/matlab/quadlog.txt";
	FILE * writeFile;
	writeFile = fopen(path.c_str(), "w");
	char outStr[512];

	std::vector<std::string> topics;
	topics.push_back("cmds");
	topics.push_back("health");
	topics.push_back("pose");
	topics.push_back("vel");

	rosbag::View view(bag, rosbag::TopicQuery(topics));

	acl_msgs::QuadCmd cmd;
	acl_msgs::QuadHealth health;
	geometry_msgs::PoseStamped pose;
	geometry_msgs::TwistStamped twist;

	int cnt = 0;
	BOOST_FOREACH(rosbag::MessageInstance const m, view)
	{
		acl_msgs::QuadCmd::ConstPtr q =
				m.instantiate<acl_msgs::QuadCmd>();
		if (q != NULL)
		{
			cmd = *q;
			// the pose and twist data is published twice as fast as the command data;  However,
			// only save data when command data is published because this is the most relavent.

			sprintf(outStr,
					"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
					(double) cmd.header.stamp.toSec(), cmd.pose.position.x,
					cmd.pose.position.y, cmd.pose.position.z,
					cmd.pose.orientation.w, cmd.pose.orientation.x,
					cmd.pose.orientation.y, cmd.pose.orientation.z,
					cmd.twist.linear.x, cmd.twist.linear.y, cmd.twist.linear.z,
					cmd.twist.angular.x, cmd.twist.angular.y,
					cmd.twist.angular.z, cmd.rpy.x, cmd.rpy.y, cmd.rpy.z,
					cmd.accel.x, cmd.accel.y, cmd.accel.z, cmd.accel_fb.x,
					cmd.accel_fb.y, cmd.accel_fb.z, cmd.jerk.x, cmd.jerk.y,
					cmd.jerk.z, cmd.jerk_fb.x, cmd.jerk_fb.y, cmd.jerk_fb.z,
					cmd.pos_integrator.x, cmd.pos_integrator.y,
					cmd.pos_integrator.z, cmd.throttle,
					cmd.f_total);
			fputs(outStr, writeFile);

			sprintf(outStr,
					"%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
					(double) health.header.stamp.toSec(), health.voltage,
					health.current[0], health.current[1], health.current[2],
					health.current[3], health.temperature[0],
					health.temperature[1], health.temperature[2],
					health.temperature[3], health.att.w, health.att.x,
					health.att.y, health.att.z, health.att_meas.w,
					health.att_meas.x, health.att_meas.y, health.att_meas.z,
					health.rpy.x, health.rpy.y, health.rpy.z, health.rpy_meas.x,
					health.rpy_meas.y, health.rpy_meas.z, health.rate.x,
					health.rate.y, health.rate.z);
			fputs(outStr, writeFile);

			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,",
					(double) pose.header.stamp.toSec(), pose.pose.position.x,
					pose.pose.position.y, pose.pose.position.z,
					pose.pose.orientation.w, pose.pose.orientation.x,
					pose.pose.orientation.y, pose.pose.orientation.z);
			fputs(outStr, writeFile);

			sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f\r\n",
					(double) twist.header.stamp.toSec(), twist.twist.linear.x,
					twist.twist.linear.y, twist.twist.linear.z,
					twist.twist.angular.x, twist.twist.angular.y,
					twist.twist.angular.z);
			fputs(outStr, writeFile);

			cnt++;

		}

		acl_msgs::QuadHealth::ConstPtr h = m.instantiate<
				acl_msgs::QuadHealth>();
		if (h != NULL)
		{
			health = *h;
		}

		geometry_msgs::PoseStamped::ConstPtr p = m.instantiate<
				geometry_msgs::PoseStamped>();
		if (p != NULL)
		{
			pose = *p;
		}

		geometry_msgs::TwistStamped::ConstPtr t = m.instantiate<
				geometry_msgs::TwistStamped>();
		if (t != NULL)
		{
			twist = *t;
		}

	}

	std::cout << "Processed " << cnt << " lines successfully" << std::endl;
	std::cout << "File saved: " << path << std::endl << std::endl << std::endl;

	fclose(writeFile);
	bag.close();

	return 0;
}
