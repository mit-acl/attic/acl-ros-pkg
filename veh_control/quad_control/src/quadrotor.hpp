/*
 * quadrotor.hpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef QUADROTOR_HPP_
#define QUADROTOR_HPP_

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "tf/transform_datatypes.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float64.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float64MultiArray.h"
#include "visualization_msgs/Marker.h"
#include <interactive_markers/interactive_marker_server.h>
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include <ros/package.h>

// custom messages
#include "acl_msgs/QuadCmd.h"
#include "acl_msgs/QuadHealth.h"
#include "acl_msgs/QuadRawSensors.h"
#include "acl_msgs/QuadGoal.h"

// ACL shared library
#include "acl/comms/serialPort.hpp"
#include "acl/comms/udpPort.hpp"
#include "acl/utils.hpp"

// ACL ros library
#include "raven_utils/utils.hpp"

// Local includes
#include "structs.h"
#include "comm.hpp"
#include "defines.h"

// Global includes
#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <boost/foreach.hpp>
#include <signal.h>

//## listen thread
void *listen(void *param);

class Quadrotor
{
public:
	Quadrotor();

	//## ROS message publishing
	ros::Publisher cmds, health, sensorPub;
	ros::Publisher simCmdPub, simGainsPub, simSensorCalPub;

	// message callbacks
	void poseCallback(const geometry_msgs::PoseStamped& msg);
	void velCallback(const geometry_msgs::TwistStamped& msg);
	void accelCallback(const geometry_msgs::Vector3Stamped& msg);
	void keyboardCallback(const geometry_msgs::Point & msg);
	void goalCallback(const acl_msgs::QuadGoal& msg);

	// setup functions
	void sendInit(void);

	// convert rosbag file to csv file
	int writeDataLog2File(void);

	// ROS timed functions
	void runController(const ros::TimerEvent&);

	//## Communication with Quadrotor
	acl::SerialPort ser;
	acl::UDPPort udp;
	void parsePacket(uint8_t ID, uint8_t length, uint8_t seq, uint8_t * data);

	sFlags flags;

	//## Communication with Quadrotor
	void SendPacket(uint8_t ID, uint8_t length, uint8_t seq, uint8_t * data);

private:

	std::string name; // name of the node (quadrotor that is flying)
	bool sim; // true if this is a simulated vehicle
	bool parameters_read; // true after you have read all of the parameters

	//## Control functions
	tf::Vector3 getAccel();
	tf::Quaternion getAttitude(tf::Vector3 F);
	tf::Vector3 getRate(tf::Vector3 F);
	void checkStatus();
	double F2motorCmd(double f);
	acl_msgs::QuadGoal checkRoomBound(const acl_msgs::QuadGoal& msg,
			int mode);
	bool checkHline(double x1, double y1, double x2, double y2, double x3,
			double x4, double y34);
	bool checkVline(double x1, double y1, double x2, double y2, double y3,
			double y4, double x34);

	//## Logging and Debugging Functions
	void broadcastROSdata();
	void screenPrint();

	//## Parameter functions for accessing param server
	void getParams();
	/*double param(std::string n);
	void param(std::string n, bool &out);
	void param(std::string n, std::string &out);*/

	//## Control Structs and msgs
	sGoal goal;
	sState state;
	sOuterGains gains;
	sInnerGains innerGains;
	sCalibrationData calData;
	Integrator I_x, I_y, I_z;
	acl_msgs::QuadHealth healthData;
	acl_msgs::QuadRawSensors sensors;

	//## Communication with Quadrotor
	void sendCmd(void);
	void sendGains(void);
	void sendCal(void);
	void sendEmergency(void);
	std::string serialPort;
	int baudRate;
	std::string ip_address;
	int rxport, txport;

	//## Comm structs
	tSensorCalPacket sensorCal;
	tAHRSpacket ahrs;
	tLogPacket logPacket;
	tHealthPacket healthPacket;
	tSensorsPacket sensorsPacket;
	tAltitudePacket altitudePacket;
	tAdaptCmdPacket adaptcmdPacket;

	// control information
	double throttle;
	bool emergencyLand, yawInitialized, averages_set;
	int attStatus;
	int cntrl_cnt;
	std::ostringstream errorMsg, warnMsg;

	// timer for initializing motors
	ros::Time startTime;

	// Quadrotor Mass
	double mass;

	// for numerical differentiation of acceleration
	double t_old;
	tf::Vector3 accel_fb_old, jerk_fb_old;
	tf::Quaternion qmin_old;

	// Utilities
	void quaternion2Euler(tf::Quaternion q, double &roll, double &pitch,
			double &yaw);
	void quaternion2Euler(geometry_msgs::Quaternion q, double &roll,
			double &pitch, double &yaw);
	tf::Vector3 saturate(tf::Vector3 val, tf::Vector3 max, tf::Vector3 min);
	tf::Quaternion checkQuat(tf::Quaternion qmin, tf::Quaternion old,
			tf::Quaternion state);
	void qnorm(tf::Quaternion &q);
	void vnorm(tf::Vector3 &q);
};

#endif /* QUADROTOR_HPP_ */
