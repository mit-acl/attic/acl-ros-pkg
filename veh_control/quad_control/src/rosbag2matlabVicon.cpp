/*
 * rosbag2matlab.cpp
 *
 *  Created on: Jul 10, 2012
 *      Author: Mark Cutler
 */

// ROS includes
#include "ros/ros.h"
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include <ros/package.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"

// Global includes
#include <iostream>
#include <signal.h>
#include <boost/foreach.hpp>

/*****************************************
 * rosbag2matlab - opens a rosbag file saved by quad_control and
 * saves the data as a matlab readable csv file
 * INPUTS - path to rosbag file to be converted
 * OUTPUTS - none (saves .csv file of same name as the input)
 */
int main(int argc, char **argv)
{
  std::string fileName("defaultfile.txt");
  if (argc == 2)
  {
    fileName = argv[1];
  }
  else
  {
    printf("Need to specify file to process.\n");
    printf("Example:  ./rosbag2matlab file.bag\n\n");
    return -1;
  }

  std::string quadName;
  const size_t last_slash_idx = fileName.rfind('/');
  if (std::string::npos != last_slash_idx)
  {
    quadName = fileName.substr(last_slash_idx + 1, 4);
  }

  rosbag::Bag bag;
  bag.open(fileName, rosbag::bagmode::Read);
  printf("Processing %s for quad %s\n", fileName.c_str(), quadName.c_str());

  // Open file for human readable format textfile
  std::string path = ros::package::getPath("quad_control");
  path += "/matlab/viconLog.txt";
  FILE * writeFile;
  writeFile = fopen(path.c_str(), "w");
  char outStr[512];

  std::vector<std::string> topics;
  topics.push_back(quadName + std::string("/pose"));
  topics.push_back(quadName + std::string("/vel"));
  topics.push_back(quadName + std::string("/accel"));

  rosbag::View view(bag, rosbag::TopicQuery(topics));

  geometry_msgs::PoseStamped pose;
  geometry_msgs::TwistStamped twist;
  geometry_msgs::Vector3Stamped accel;

  int cnt = 0;
  BOOST_FOREACH(rosbag::MessageInstance const m, view){

  geometry_msgs::PoseStamped::ConstPtr p = m.instantiate<geometry_msgs::PoseStamped>();
  if (p != NULL)
  {
    pose = *p;
  }

  geometry_msgs::TwistStamped::ConstPtr t = m.instantiate<geometry_msgs::TwistStamped>();
  if (t != NULL)
  {
    twist = *t;
  }

  geometry_msgs::Vector3Stamped::ConstPtr a = m.instantiate<geometry_msgs::Vector3Stamped>();
  if (a != NULL)
  {
    accel = *a;
    // the pose and twist data is published twice as fast as the command data;  However,
    // only save data when command data is published because this is the most relavent.

    sprintf(outStr,"%f,%f,%f,%f,%f,%f,%f,%f,",
        (double) pose.header.stamp.toSec(),
        pose.pose.position.x, pose.pose.position.y, pose.pose.position.z,
        pose.pose.orientation.w, pose.pose.orientation.x, pose.pose.orientation.y, pose.pose.orientation.z);
    fputs(outStr,writeFile);

    sprintf(outStr,"%f,%f,%f,%f,%f,%f,%f,",
        (double) twist.header.stamp.toSec(),
        twist.twist.linear.x, twist.twist.linear.y, twist.twist.linear.z,
        twist.twist.angular.x, twist.twist.angular.y, twist.twist.angular.z);
    fputs(outStr,writeFile);

    sprintf(outStr,"%f,%f,%f,%f\r\n",
        (double) accel.header.stamp.toSec(),
        accel.vector.x, accel.vector.y, accel.vector.z);
    fputs(outStr,writeFile);

    cnt++;
  }
}

  std::cout << "Processed " << cnt << " lines successfully" << std::endl;
  std::cout << "File saved: " << path << std::endl;

  fclose(writeFile);
  bag.close();

  return 1;
}

