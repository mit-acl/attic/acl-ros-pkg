/*
 * rosbag2txt.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: James Wiken
 */

// ROS includes
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "tf/transform_datatypes.h"
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/PoseArray.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Float64.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float64MultiArray.h"
#include "visualization_msgs/Marker.h"
#include <interactive_markers/interactive_marker_server.h>
#include "rosbag/bag.h"
#include "rosbag/view.h"
#include <ros/package.h>

// custom messages
#include "acl_msgs/QuadCmd.h"
#include "acl_msgs/QuadHealth.h"
#include "acl_msgs/QuadRawSensors.h"
#include "acl_msgs/QuadGoal.h"

// ACL shared library
#include "acl/comms/serialPort.hpp"
#include "acl/utils.hpp"

// Local includes
#include "structs.h"
#include "comm.hpp"
#include "defines.h"

// Global includes
#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <signal.h>

/*****************************************
 * rosbag2matlab - opens a rosbag file saved by quad_control and
 * saves the data as a txt file
 * INPUTS - path to rosbag file to be converted
 * OUTPUTS - none (saves .txt file of same name as the input)
 */
int main(int argc, char **argv)
{
  std::string fileName("defaultfile.txt");
  if (argc == 2)
  {
    fileName = argv[1];
  }
  else
  {
    printf("Need to specify file to process.\n");
    printf("Example:  ./rosbag2matlab file.bag\n\n");
    return -1;
  }

  std::string quadName;
  const size_t last_slash_idx = fileName.rfind('/');
  if (std::string::npos != last_slash_idx)
  {
    quadName = fileName.substr(last_slash_idx + 1, 4);
  }

  rosbag::Bag bag;
  bag.open(fileName, rosbag::bagmode::Read);

  boost::filesystem::path p(fileName);
  // Open file for human readable format textfile
  std::string path = ros::package::getPath("quad_control");
  path += "/matlab/";
  path += boost::filesystem::basename (p);
  path += ".txt";
  FILE * writeFile;
  writeFile = fopen(path.c_str(), "w");
  char outStr[512];

  std::vector<std::string> topics;
  topics.push_back("cmds");
  topics.push_back("health");
  topics.push_back("pose");
  topics.push_back("vel");

  rosbag::View view(bag, rosbag::TopicQuery(topics));

  acl_msgs::QuadCmd cmd;
  acl_msgs::QuadHealth health;
  geometry_msgs::PoseStamped pose;
  geometry_msgs::TwistStamped twist;

  int cnt = 0;
  BOOST_FOREACH(rosbag::MessageInstance const m, view)
  {
    acl_msgs::QuadCmd::ConstPtr q =
        m.instantiate<acl_msgs::QuadCmd>();
    if (q != NULL)
    {
      cmd = *q;
      // the pose and twist data is published twice as fast as the command data;  However,
      // only save data when command data is published because this is the most relavent.

      sprintf(outStr,
          "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
          (double) cmd.header.stamp.toSec(), cmd.pose.position.x,
          cmd.pose.position.y, cmd.pose.position.z,
          cmd.pose.orientation.w, cmd.pose.orientation.x,
          cmd.pose.orientation.y, cmd.pose.orientation.z,
          cmd.twist.linear.x, cmd.twist.linear.y, cmd.twist.linear.z,
          cmd.twist.angular.x, cmd.twist.angular.y,
          cmd.twist.angular.z, cmd.rpy.x, cmd.rpy.y, cmd.rpy.z,
          cmd.accel.x, cmd.accel.y, cmd.accel.z, cmd.accel_fb.x,
          cmd.accel_fb.y, cmd.accel_fb.z, cmd.jerk.x, cmd.jerk.y,
          cmd.jerk.z, cmd.jerk_fb.x, cmd.jerk_fb.y, cmd.jerk_fb.z,
          cmd.pos_integrator.x, cmd.pos_integrator.y,
          cmd.pos_integrator.z, cmd.throttle,
          cmd.f_total);
      fputs(outStr, writeFile);

      sprintf(outStr,
          "%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,",
          (double) health.header.stamp.toSec(), health.voltage,
          health.current[0], health.current[1], health.current[2],
          health.current[3], health.temperature[0],
          health.temperature[1], health.temperature[2],
          health.temperature[3], health.att.w, health.att.x,
          health.att.y, health.att.z, health.att_meas.w,
          health.att_meas.x, health.att_meas.y, health.att_meas.z,
          health.rpy.x, health.rpy.y, health.rpy.z, health.rpy_meas.x,
          health.rpy_meas.y, health.rpy_meas.z, health.rate.x,
          health.rate.y, health.rate.z);
      fputs(outStr, writeFile);

      sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f,%f,",
          (double) pose.header.stamp.toSec(), pose.pose.position.x,
          pose.pose.position.y, pose.pose.position.z,
          pose.pose.orientation.w, pose.pose.orientation.x,
          pose.pose.orientation.y, pose.pose.orientation.z);
      fputs(outStr, writeFile);

      sprintf(outStr, "%f,%f,%f,%f,%f,%f,%f\r\n",
          (double) twist.header.stamp.toSec(), twist.twist.linear.x,
          twist.twist.linear.y, twist.twist.linear.z,
          twist.twist.angular.x, twist.twist.angular.y,
          twist.twist.angular.z);
      fputs(outStr, writeFile);

      cnt++;

    }

    acl_msgs::QuadHealth::ConstPtr h = m.instantiate<
        acl_msgs::QuadHealth>();
    if (h != NULL)
    {
      health = *h;
    }

    geometry_msgs::PoseStamped::ConstPtr p = m.instantiate<
        geometry_msgs::PoseStamped>();
    if (p != NULL)
    {
      pose = *p;
    }

    geometry_msgs::TwistStamped::ConstPtr t = m.instantiate<
        geometry_msgs::TwistStamped>();
    if (t != NULL)
    {
      twist = *t;
    }

  }

  std::cout << "Processed " << cnt << " lines successfully" << std::endl;
  std::cout << "File saved: " << path << std::endl << std::endl << std::endl;

  fclose(writeFile);
  bag.close();
}
