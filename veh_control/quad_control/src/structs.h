/*
 * structs.h
 *
 *  Created on: Aug 9, 2011
 *      Author: mark
 */

#ifndef STRUCTS_H_
#define STRUCTS_H_

#include <stdint.h>
#include <vector>
#include "tf/transform_datatypes.h"
#include "geometry_msgs/Vector3.h"

//********************* CONTROL STRUCTS **********************//
struct sGoal
{
  tf::Quaternion att;						// Attitude
  tf::Vector3 rate;						// Angular rates
  tf::Vector3 pos, vel, accel, jerk;		// Position, velocity, accleration and jerk
  tf::Vector3 accel_fb, jerk_fb;			// Feedback modified acceleration and jerk
  tf::Vector3 accel_int;					// Integrator modified acceleration
  double f_total; 						// Thrust in newtons
  double yaw, dyaw;						// Yaw angle, yaw rate

  // Constructor
  sGoal()
  {
    att = att.getIdentity();
    rate.setZero();
    pos.setZero();
    vel.setZero();
    accel.setZero();
    jerk.setZero();
    accel_fb.setZero();
    accel_int.setZero();
    jerk_fb.setZero();
    f_total = yaw = dyaw = 0.0;
  }
  ;
};

struct sState
{
  tf::Quaternion att;					// Attitude
  tf::Vector3 pos, vel, accel, rate;	// Position, velocity, acceleration, rate
  ros::Time time; // last time we received information

  // Constructor
  sState()
  {
    att = att.getIdentity();
    pos.setZero();
    vel.setZero();
    accel.setZero();
    rate.setZero();
    time.fromNSec(0);
  }
};

struct sOuterGains
{

  tf::Vector3 Kp, Kd, Ki;	// Position PID gains
  tf::Vector3 maxPosErr;  // maximum position error allowed
  tf::Vector3 maxVelErr;  // maximum velocity error allowed
  double I_xy_int_thresh; // threshold on integrating position error
  double I_z_int_thresh; 	// threshold on integrating position error

  double rmag;		// acceleration magnitude
  double minRmag; 	// minimum allowable acceleration magnitude for calculating attitude
  double minRmagint; 	// minimum acceleration magnitude for listening to integrated values

  double a, b, c;   	// quadratic terms for calculating motor term from thrust in Newtons

  // Constructor - initialize all values to zero
  sOuterGains()
  {
    Kp.setZero();
    Ki.setZero();
    Kd.setZero();
    maxPosErr.setZero();
    maxVelErr.setZero();
    I_xy_int_thresh = I_z_int_thresh = 0.0;
    rmag = minRmag = minRmagint = 0.0;
    a = b = c = 0.0;
  }
  ;
};

struct sInnerGains
{
  tf::Vector3 Kp, Kd, Ki; // x -> roll, y -> pitch, z - > yaw
  double PWM_trim[4];
  double M_trim[4];
  double maxang; // max allowable angle error onboard
  double lowBatt;
  double motorFF;

  // Constructor:
  sInnerGains()
  {
    Kp.setZero();
    Kd.setZero();
    Ki.setZero();
    PWM_trim[0] = PWM_trim[1] = PWM_trim[2] = PWM_trim[3] = 0.0;
    M_trim[0] = M_trim[1] = M_trim[2] = M_trim[3] = 0.0;
    maxang = lowBatt = motorFF = 0.0;
  }
  ;
};

struct sCalibrationData
{
  double gyroScale;
  double accelScale;
  double K_Att_Filter;
  double kGyroBias;

  // Constructor:
  sCalibrationData()
  {
    gyroScale = 0.0;
    accelScale = 0.0;
    K_Att_Filter = 0.0;
    kGyroBias = 0.0;
  }
  ;
};

// FLAGS ############
struct sFlags
{
  bool streamData;	// stream imu data from autopilot
  bool screenPrint;	// print information to screen
  bool demo;			// run the xbox demo
  bool attitude_mode; 	// directly send attitude and thrust commands
  bool use_xbee;		// communicate with quad using an xbee instead of wifly

  // Constructor:
  sFlags()
  {
    streamData = false;
    screenPrint = true;
    demo = false;
    attitude_mode = false;
    use_xbee = false;
  }
  ;
};
//************************************************************//

// File structs
struct sensorFile
{
  std::string name;
  tm date;
  double time;

  bool operator ()(const sensorFile &a, const sensorFile &b) const
  {
    return a.time < b.time;
  }
};

// Integrator
struct Integrator
{
  double value;

  // Constructors:
  Integrator()
  {
    value = 0;
  }
  ;

  // Methods
  void increment(double inc, double dt)
  {
    value += inc * dt;
  }
  void reset()
  {
    value = 0;
  }

};

#endif /* STRUCTS_H_ */
