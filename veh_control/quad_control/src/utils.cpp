/*
 * utils.cpp
 *
 *  Created on: Jun 1, 2012
 *      Author: mark
 */

#include "quadrotor.hpp"

//## From Wikipedia - http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
void Quadrotor::quaternion2Euler(tf::Quaternion q, double &roll, double &pitch,
		double &yaw)
{
	double q0, q1, q2, q3;
	q0 = q.getW();
	q1 = q.getX();
	q2 = q.getY();
	q3 = q.getZ();
	roll = atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2))
			* 180/ PI;
	pitch = asin(2 * (q0 * q2 - q3 * q1)) * 180 / PI;
	yaw =
			atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3))
					* 180/ PI;
}

//## From Wikipedia - http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
void Quadrotor::quaternion2Euler(geometry_msgs::Quaternion q, double &roll,
		double &pitch, double &yaw)
{
	double q0, q1, q2, q3;
	q0 = q.w;
	q1 = q.x;
	q2 = q.y;
	q3 = q.z;
	roll = atan2(2 * (q0 * q1 + q2 * q3), 1 - 2 * (q1 * q1 + q2 * q2))
			* 180/ PI;
	pitch = asin(2 * (q0 * q2 - q3 * q1)) * 180 / PI;
	yaw =
			atan2(2 * (q0 * q3 + q1 * q2), 1 - 2 * (q2 * q2 + q3 * q3))
					* 180/ PI;
}

tf::Vector3 Quadrotor::saturate(tf::Vector3 val, tf::Vector3 max,
		tf::Vector3 min)
{
	val.setMin(max); // set to min of val and max
	val.setMax(min); // set to max of val and min
	return val;
}

tf::Quaternion Quadrotor::checkQuat(tf::Quaternion qmin, tf::Quaternion old,
		tf::Quaternion state)
{
	// check for continuity with last qmin
	if (qmin.dot(old) < 0.0)
	{
		qmin *= -1.0;
	}

	// check for continuity with state quaternion
	if (qmin.dot(state) < 0.0)
	{
		qmin *= -1.0;
	}
	qmin.normalize();

	return qmin;
}

void Quadrotor::qnorm(tf::Quaternion &q)
{
	if (q.length() < 0.001)
		ROS_WARN("Can't normalize quaternion -- length == 0.0");
	else
		q.normalize();
}

void Quadrotor::vnorm(tf::Vector3 &v)
{
	if (v.length() < 0.001)
		ROS_WARN("Can't normalize vector -- length == 0.0");
	else
		v.normalize();
}
