#!/usr/bin/env python
import roslib
roslib.load_manifest('upucc_control')
import rospy
import socket
from std_msgs.msg import Float64MultiArray
from aclpy import utils

class UpuccCmd:

    def __init__(self, host, txport, rxport):
        self.driving = False
        self.sockSend = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.sockRecv = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.host = host
        self.txport = txport
        self.rxport = rxport
        print 'Initializing udp with host %s txport %s rxport %s' %(self.host,
        	self.txport, self.rxport)

        self.sockRecv.bind(("",self.rxport))
        #self.sockRecv.setblocking(0)
        print 'Udp listerning binding to port %s' %(self.rxport) 

        self.vL = self.vR = 0.0
        #self.cal_coeffs = [0.4184, 0.6687, 0.0743] # these come from upucc_cal.m
        self.cal_coeffs = [0.0353, 0.8404, 0.0557] # these come from upucc_cal.m (5V on-board reg. version)
        
    def __del__(self):
        self.sockSend.close()
        self.sockRecv.close()
        
    def cmdCB(self, data):
        self.driving = data.data[0]
        if self.driving:
            self.vL = data.data[1]
            self.vR = data.data[2]
        else:
            self.vL = self.vR = 0.0
        
        # convert vL and vR to commands for the uPucc
        cmdL = self.convertCmd(self.vL)
        cmdR = self.convertCmd(self.vR)

        #print "cmdL: %s cmdR: %s" %(cmdL,cmdR)
        
        #cmd = 1.0
        #cmdL = -cmd
        #cmdR = cmd

        # vL and vR are -1 to 1
        cmdL = utils.saturate(cmdL, 1.0, -1.0)
        cmdR = utils.saturate(cmdR, 1.0, -1.0)
        # print "L: " + str(cmdL) + " R: " + str(cmdR)
        self.vL = cmdL
        self.vR = cmdR

    def convertCmd(self, vel):
        if vel == 0:
            return 0
        sign = 1
        if vel < 0:
            vel *= -1
            sign = -1
        cmd = self.cal_coeffs[0]*vel**2 + self.cal_coeffs[1]*vel + self.cal_coeffs[2]
        cmd *= sign
        return cmd
        
    # send commands over bluetooth at a fixed rate
    def send_cmd(self, event):
        motor = bytearray(1)
        motor[0] = 64 + (int)(62.0*self.vL)
        self.sockSend.sendto(str(motor),(self.host,self.txport))
        motor[0] = 192 + (int)(62.0*self.vR)
        self.sockSend.sendto(str(motor),(self.host,self.txport))
        # check for bluetooth data
    
    def read_data(self, event):
        data = self.sockRecv.recv(4096)
        if len(data):
            print "Voltage: " + str(ord(data[0]))


def upucc_cmd():
    host = '192.168.0.53'
    rxport = 20053
    txport = 30053

    c = UpuccCmd(host, txport, rxport)
    rospy.Subscriber("upucc_cmd", Float64MultiArray, c.cmdCB)
    rospy.Timer(rospy.Duration(1/30.0), c.send_cmd) # start sending commands over bluetooth
    rospy.Timer(rospy.Duration(1/5.0), c.read_data) # read voltage from bluetooth
    rospy.spin()

if __name__ == '__main__':

    rospy.init_node('cmd')
    upucc_cmd()
