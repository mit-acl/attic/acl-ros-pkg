#!/usr/bin/env python
import roslib
roslib.load_manifest('upucc_control')
import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from aclpy import utils

A = 0
B = 1
X = 2
Y = 3
CENTER = 8
RB = 5


class UpuccJoy:

    def __init__(self):
        self.driving = False
        self.pub_cmd = rospy.Publisher('upucc_cmd', Float64MultiArray, queue_size=1)

    def joyCB(self, data):
        if data.buttons[A]:
            self.driving = True
        if data.buttons[B]:
            self.driving = False

        # get throttle and turn commands from joystick, then publish them
        throttle = data.axes[1]
        turn = -data.axes[3]

        vL = throttle + turn/2.0
        vR = throttle - turn/2.0

        if not self.driving:
            vL = 0.0
            vR = 0.0

        vL = utils.saturate(vL, 1.0, -1.0)
        vR = utils.saturate(vR, 1.0, -1.0)
    
        cmd = Float64MultiArray()
        cmd.data = [self.driving, vL, vR]
        self.pub_cmd.publish(cmd)


def joyListen():
    c = UpuccJoy()
    rospy.Subscriber("/joy", Joy, c.joyCB)
    rospy.spin()

if __name__ == '__main__':

    rospy.init_node('upucc_joy')
    joyListen()
