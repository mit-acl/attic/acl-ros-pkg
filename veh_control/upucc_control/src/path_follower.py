#!/usr/bin/env python
import roslib
roslib.load_manifest('upucc_control')
import rospy
import numpy as np
import pure_pursuit as pp
from std_msgs.msg import Float64
from std_msgs.msg import Bool
from std_msgs.msg import MultiArrayDimension
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistStamped
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Point
from nav_msgs.msg import Path
import math
from aclpy import utils


class PathFollower:

    def __init__(self):

        self.old_time = rospy.get_time()

        # initialize marker data
        self.marker = Marker(type=Marker.ARROW, action=Marker.ADD)
        self.pathmarker1 = Marker(type=Marker.LINE_STRIP, action=Marker.ADD)
        self.pathmarker2 = Marker(type=Marker.LINE_STRIP, action=Marker.ADD)
        self.initMarker(self.marker)
        self.initMarker(self.pathmarker1)
        self.initMarker(self.pathmarker2)
        self.marker.color.a = 1.0
        self.pathmarker1.id += 1
        self.pathmarker2.id += 2
        self.pathmarker1.scale.x = 0.035
        self.pathmarker1.color.r = 0
        self.pathmarker1.color.g = 1
        self.pathmarker1.color.a = 1
        self.pathmarker2.scale.x = 0.035
        self.pathmarker2.color.a = 0.2
        self.pathmarker2.color.r = .8
        self.pathmarker2.color.g = .8
        self.pathmarker2.color.b = .8
        self.pubMarker = rospy.Publisher('/RAVEN_world', MarkerArray, queue_size=5)

        # goal/pose data
        self.pose = PoseStamped()
        self.psi = 0
        self.twist = TwistStamped()
        self.pubPathComplete = rospy.Publisher('path_percent', Float64, queue_size=5)

        # L1 pure pursuit controller
        self.L1_nom = 0.12
        self.vcmd_min = 0.2
        self.L1_gain = self.L1_nom / self.vcmd_min
        self.pp = pp.PurePursuit(np.array([[0, 0, 0, 0]]),
                                 self.L1_nom, self.L1_gain)

        # Mutex should prevent us from looking up purepursuit data as
        # the purepursuit module is getting updated in a threaded callback
        self.ppMutex = False

        self.commanded_radius = np.Inf
        self.currentSegment = 0
        self.startTime = 0
        self.percent_complete = 0

        # ensure that path data is available to late subscribers by setting
        # latch to true
        self.pub1 = rospy.Publisher("/polyPath1", Path, latch=True, queue_size=5)
        self.pub = rospy.Publisher("/polyPath", Path, latch=True, queue_size=5)

        self.pub_cmd = rospy.Publisher('upucc_cmd', Float64MultiArray, queue_size=1)


        # setup gains and parameters
        self.kp = 0.2  # 1.0
        self.kd = 0.02
        self.kd_steer = 0.0
        self.deltaV_prev = 0
        self.kp_v = 0.6
        self.ki_v = 0.03
        self.verr_int = 0.01
        self.minV = 0.1
        self.v = 0

        self.Lw = 0.083  # wheel base of upuccs in meters

        self.status = True

        # # temporary path following hack
        # s = np.linspace(0, 2*np.pi, 100)
        # s2 = np.linspace(0, np.pi, 100)
        # x = np.sin(s)
        # y = np.linspace(0, -5, 100)
        # z = np.zeros(100)
        # v = np.sin(s2) +0.1
        # path = np.array([x, y, z, v]).transpose()

        # self.pp.updateTree(path)

    # generate a path from a float64multiarray message
    def createPathCB(self, data):

        path = multiarray2DToNumpyArray(data)

        self.createBarePath(path)

    # create a path from a numpy array data set
    def createBarePath(self, data):

        self.pathmarker1.points = createPath(data[:, 0],
                                             data[:, 1],
                                             data[:, 2])
        if rospy.get_namespace() == '/uP01s/':
            self.pathmarker1.color.r = 0.0           
            self.pathmarker1.color.g = 1.0
            self.pathmarker1.color.b = 0.0
        if rospy.get_namespace() == '/uP02s/':
            self.pathmarker1.color.r = 1.0           
            self.pathmarker1.color.g = 0.0
            self.pathmarker1.color.b = 0.0
        if rospy.get_namespace() == '/uP03s/':
            self.pathmarker1.color.r = 0.0           
            self.pathmarker1.color.g = 0.0
            self.pathmarker1.color.b = 1.0
        if rospy.get_namespace() == '/uP04s/':
            self.pathmarker1.color.r = 1.0           
            self.pathmarker1.color.g = 1.0
            self.pathmarker1.color.b = 0.0
        # broadcast path marker for visualization purposes
        ma = MarkerArray()
        ma.markers.append(self.pathmarker1)
        self.pubMarker.publish(ma)

        self.pp.updateTree(data)


#-----------------------------------------------------------------------------
# main timer for generating goal data
#-----------------------------------------------------------------------------
    def findGoal(self, event):

        if self.status and not self.ppMutex:
            # try:
            # get current position and velocity in numpy formats
            p = np.array([self.pose.pose.position.x,
                          self.pose.pose.position.y,
                          self.pose.pose.position.z])
            v = np.array([self.twist.twist.linear.x,
                          self.twist.twist.linear.y,
                          self.twist.twist.linear.z])

            # calculate pure pursuit control vectors
            pL1, v_cmd, r_cmd, psi_cmd, index_L1, index_close = self.pp.calcL1Control(p, v, self.psi)

            # saturate velocity goal command
            if v_cmd != 0:
                v_cmd = utils.saturate(v_cmd, 1.0, -1.0)

            # compute percent of path complete based on closest point projection
            self.percent_complete = 100.0 * index_L1 / len(self.pp.speed)
            pL1 = self.pp.path[index_L1, 0:-2]

            # broadcast current percent complete
            percent_complete = Float64()
            percent_complete.data = self.percent_complete
            self.pubPathComplete.publish(percent_complete)


            # generate marker data for visualization
            self.marker.pose.position.x = pL1[0]
            self.marker.pose.position.y = pL1[1]
            self.marker.pose.orientation.w = math.cos(psi_cmd / 2.0)
            self.marker.pose.orientation.z = math.sin(psi_cmd / 2.0)

            # broadcast goal marker for visualization purposes
            # ma = MarkerArray()
            # ma.markers.append(self.marker)
            # self.pubMarker.publish(ma)

            self.control(psi_cmd, r_cmd, v_cmd)

            # except:
                # rospy.logerr("pure pursuit lookup error")

        if not self.status:
            # broadcast goal commands to control software
            self.goal.header.stamp = rospy.Time.now()
            self.goal.r = 0
            self.goal.v = 0
            self.goal.e = 0
            self.goal.reset_v_int = False
            self.pubGoal.publish(self.goal)

    # calculate commands to send to the upucc
    def control(self, psi, r, v):

        #r, v = self.limitWheelSpeeds(r, v)
        #print 'v: ' + str(v) + ' r: ' + str(r)

        deltaV_psi = self.kp * utils.wrap(self.psi - psi)
        deltaV_ff = self.Lw * r
        deltaV_rate = self.kd * (self.twist.twist.angular.z - r)

        deltaV = deltaV_ff + deltaV_psi + deltaV_rate

        # debug -- good slow driving
        # print str(v)
        maxdv = v*1
        deltaV = utils.saturate(deltaV, maxdv, -maxdv)
        # print str(deltaV) + " max dv: " + str(maxdv)

        # steer damping term (really just a first order filter on the steering signal)
        #deltaV = deltaV + self.kd_steer * (self.deltaV_prev - deltaV)
        #self.deltaV_prev = deltaV

        # longitudinal PI controller
        verr = v - self.v
        new_time = rospy.get_time()
        dt = new_time - self.old_time #1.0/50.0  # params.CNTRL_RATE
        self.old_time = new_time
        if self.status:
            self.verr_int += verr * dt
        v_cmd = 0
        if verr != 0:
            v_cmd = self.kp_v * verr + self.ki_v * self.verr_int

        # get individual wheel speeds.  Note, v = 1/2(vL+vR), dV = vR-vL
        deltaV *= -1.0
        desV = v + v_cmd
        vL = desV - 0.5*deltaV
        vR = desV + 0.5*deltaV
        
        # if v is zero, we want to stop
        if v == 0:
            vL = vR = 0.0

        # convert vR and vL from m/s to motor commands
        # TODO: implement

        vR, vL = self.limitWheelSpeeds(vR, vL)

#        vR = 0.0
#        vL = 0.0

        # send data
        if self.status:
            cmd = Float64MultiArray()
            cmd.data = [self.status, vL, vR]
            self.pub_cmd.publish(cmd)

        
    def limitWheelSpeeds(self, vR, vL):

        # Read the color parameter
        if not rospy.has_param("color"):
            # print 'no color'
            return vR, vL
        grid_color = rospy.get_param("color")
        #print grid_color

        if grid_color == "b": # or grid_color == 'k':
            # No turning
            v = np.mean([vR, vL])
            vR = vL = v
        else:
            pass
        
        return vR, vL

    # listen to the upucc pose data
    def poseCB(self, data):
        self.pose = data
        self.psi = utils.quat2yaw(self.pose.pose.orientation)

    # listen to the upucc vel data
    def velCB(self, data):
        self.twist = data
        self.v = utils.norm(self.twist.twist.linear.x,
                            self.twist.twist.linear.y)

    # drive or stop driving
    def shouldDriveCB(self, msg):
        self.status = msg.data

    # setup goal marker for visualization in rviz
    def initMarker(self, m):
        m.header.frame_id = "/world"
        m.header.stamp = rospy.Time.now()
        m.pose.position.x = 0.0
        m.pose.position.y = 0.0
        m.pose.position.z = 0.0

        m.ns = "RAVEN_upucc"
        ns = rospy.get_namespace()
        m.id = 0
        for letter in str(ns):
            m.id += ord(letter)  # cheap way to get a unique marker id

        m.pose.orientation.x = 0
        m.pose.orientation.y = 0
        m.pose.orientation.z = 0.0
        m.pose.orientation.w = 1
        m.scale.x = 0.5
        m.scale.y = 0.03
        m.scale.z = 0.03
        m.color.r = 1.0
        m.color.g = 0.0
        m.color.b = 0.0
        m.color.a = 1.0
        m.lifetime = rospy.Duration(50)
        return m


# form the discretized position variables into a ros navigation path message
def createPath(x, y, z):
    points = []
    for i in range(len(x)):
        p = Point()
        p.x = x[i]
        p.y = y[i]
        p.z = z[i]
        points.append(p)
    return points


# -----------------------------------------------------------------------------
# published the filled path over the network
# -----------------------------------------------------------------------------
def broadcastPath(pub, path):
    pub.publish(path)


#-----------------------------------------------------------------------------
# 2D Multiarray to numpy array -- convert multiarray data type to a numpy array
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def multiarray2DToNumpyArray(ma):
    I = ma.layout.dim[0].size
    J = ma.layout.dim[1].size
    na = np.empty([I, J])
    for i in range(I):
        for j in range(J):
            index = ma.layout.data_offset + ma.layout.dim[1].stride * i + j
            na[i, j] = ma.data[index]
    return na

#-----------------------------------------------------------------------------
# 2D numpy array to multiarray
# I'm sure there are more efficient ways to do this
#-----------------------------------------------------------------------------
def numpyArray2DToMultiarray(na, ma):
    I = na.shape[0]
    J = na.shape[1]
    d1 = MultiArrayDimension()
    d2 = MultiArrayDimension()
    ma.layout.dim = [d1, d2]
    ma.layout.dim[0].size = I
    ma.layout.dim[0].stride = I * J
    ma.layout.dim[1].size = J
    ma.layout.dim[1].stride = J
    for i in range(I):
        ma.data = np.append(ma.data, na[i, :])



if __name__ == '__main__':
    try:
        ns = rospy.get_namespace()
        rospy.init_node('path_follower')
        if str(ns) == '/':
            rospy.logfatal("Need to specify namespace as vehicle name.")
            rospy.logfatal("This is typically accomplished in a launch file.")
            rospy.logfatal("Command line: ROS_NAMESPACE=uP04 $ rosrun upucc_control path_follower.py")
        else:
            rospy.loginfo("Starting project on path for: " + str(ns))
            c = PathFollower()

            # set up subscribers
            rospy.Subscriber("pose", PoseStamped, c.poseCB)
            rospy.Subscriber("vel", TwistStamped, c.velCB)
            rospy.Subscriber("should_drive", Bool, c.shouldDriveCB)
            rospy.Subscriber("path", Float64MultiArray, c.createPathCB)

            # set up 50 Hz timer
            rospy.Timer(rospy.Duration(1.0 / 50.0), c.findGoal)
            rospy.spin()
    except rospy.ROSInterruptException:
        pass
