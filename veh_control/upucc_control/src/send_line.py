#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray
from upucc_sim.srv import ResetState
from geometry_msgs.msg import PoseStamped
from raven_utils import raven_utils as ru


def sendPath(name, x, y, z, v):
    pub = rospy.Publisher('/' + name + '/path', Float64MultiArray, latch='true', queue_size=5)
#    if name[-1] == 's' or name[-1] == 'S': # this is a simulated vehicle
#        print "Waiting for service"
#        rospy.wait_for_service('/' + name + '/reset')
#        print "Conencted with service"
#        resetState = rospy.ServiceProxy('/' + name + '/reset', ResetState)
#        try:
#            p_start = PoseStamped()
#            p_start.pose.position.x = x[0]
#            p_start.pose.position.y = y[0]
#            p_start.pose.position.z = z[0]
#            p_start.pose.orientation.w = 1.0
#            resetState(p_start)
#        except:
#            print "no go"

    pathnp = np.array([x, y, z, v]).transpose()

    pathma = Float64MultiArray()
#    numpyArray2DToMultiarray(pathnp, pathma)
    ru.numpyArray2MultiArray(pathnp, pathma)

    pub.publish(pathma)


if __name__ == '__main__':
    rospy.init_node('send_line')

    # y = np.linspace(-6.0, 3.0, 100)
    # x = np.linspace(-3.0, 2.0, 100)

    # y = np.linspace(3.0, -6.0, 100)
    # x = np.linspace(2.0, -3.0, 100)

    #y = -np.linspace(.0, 6.0, 100)
    #x = -np.sin(1.5*y)
    
    y1 = -np.linspace(0.0, 1, 50)
    y2 = np.append(y1, -np.linspace(1, 2, 50))
    y3 = np.append(y2,-np.linspace(2,3,50))
    x1 = np.linspace(0., 1., 50)
    x2 = np.append(x1, np.linspace(1., 0., 50))
    x3 = np.append(x2,np.zeros(50))
    y3 = y3[::-1]
    x3 = x3[::-1]
    z = np.zeros(150)
    v = 0.4*np.ones(50) #0.28*0.15*np.ones(100)
    v = np.append(v,0.3*0.4*np.ones(50))
    v = np.append(v,1.5*0.4*np.ones(50))

    v = 0*v + 0.08

    v[-1] = 0 # stop at the end

    sendPath('uP01', x3, -y3-3, z, v)
    #sendPath('uP01', x3, y3, z, v)

    rospy.sleep(0.5)
