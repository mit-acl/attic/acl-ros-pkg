#!/usr/bin/env python
import roslib
roslib.load_manifest('upucc_control')
import rospy
import numpy as np
from std_msgs.msg import MultiArrayDimension
from std_msgs.msg import Float64MultiArray
from upucc_sim.srv import ResetState
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Vector3
from path_planner.srv import GenPath
from path_planner.msg import Trajectory


def sendPath(name, path):
    pub = rospy.Publisher('/' + name + '/path', Float64MultiArray, latch='true')
    print "now here"
    rospy.wait_for_service('/' + name + '/reset')
    print "finally here"
    resetState = rospy.ServiceProxy('/' + name + '/reset', ResetState)
    try:
        resp = resetState(path.poses[0])
    except:
        print "no go"

    x = np.zeros(len(path.poses))
    y = np.zeros(len(path.poses))
    z = np.zeros(len(path.poses))
    v = np.zeros(len(path.poses))
    for i in range(len(path.poses)):
        x[i] = path.poses[i].pose.position.x
        y[i] = path.poses[i].pose.position.y
        z[i] = path.poses[i].pose.position.z
        v[i] = path.poses[i].pose.orientation.w+0.1


    pathnp = np.array([x, y, z, v]).transpose()

    pathma = Float64MultiArray()
    numpyArray2DToMultiarray(pathnp, pathma)

    pub.publish(pathma)


if __name__ == '__main__':
    rospy.init_node('send_path')

    rospy.wait_for_service('/gen_path')
    genPath = rospy.ServiceProxy('/gen_path', GenPath)
    print "got path service~"

    p0 = np.array([[0, 0], [2, 0], [1, 1], [1, -1]])
    p1 = np.array([[2, 0], [0, 0], [1,-1], [1, 1]])
    v0 = np.zeros((4,2))
    v1 = np.zeros((4,2))
    a0 = np.zeros((4,2))
    a1 = np.zeros((4,2))

    p0 = np.array([[0, 0], [2, 0], [1, -1]])
    p1 = np.array([[2, 0], [0, 0], [1,1]])
    v0 = np.zeros((3,2))
    v1 = np.zeros((3,2))
    a0 = np.zeros((3,2))
    a1 = np.zeros((3,2))

    p0 = np.array([[0, 0], [2, 0]])
    p1 = np.array([[2, 0], [0, 0]])
    v0 = np.zeros((2,2))
    v1 = np.zeros((2,2))
    a0 = np.zeros((2,2))
    a1 = np.zeros((2,2))

    # p0m = Float64MultiArray()
    # p1m = Float64MultiArray()
    # v0m = Float64MultiArray()
    # v1m = Float64MultiArray()
    # a0m = Float64MultiArray()
    # a1m = Float64MultiArray()

    # numpyArray2DToMultiarray(p0, p0m)
    # numpyArray2DToMultiarray(p1, p1m)
    # numpyArray2DToMultiarray(v0, v0m)
    # numpyArray2DToMultiarray(v1, v1m)
    # numpyArray2DToMultiarray(a0, a0m)
    # numpyArray2DToMultiarray(a1, a1m)

    # print p0m

    N = p0.shape[0]
    numD = p0.shape[1]
    h = 0.1
    T = 3.0
    R = 0.5

    p0v = numpyArray2DToVector3Array(p0, numD)
    v0v = numpyArray2DToVector3Array(v0, numD)
    a0v = numpyArray2DToVector3Array(a0, numD)
    p1v = numpyArray2DToVector3Array(p1, numD)
    v1v = numpyArray2DToVector3Array(v1, numD)
    a1v = numpyArray2DToVector3Array(a1, numD)


    # resp = genPath(p0m,v0m,a0m,p1m,v1m,a1m)
    # paths = resp.paths

    resp = genPath(N, numD, T, h, R, p0v, v0v, a0v, p1v, v1v, a1v)

    print resp

    # path1 = paths[0]
    # path2 = paths[1]
    # #path2.poses[0].pose.orientation.w = 0.7
    # path2.poses[0].pose.orientation.z = 1

    # print path1
    # sendPath('uP01s',path1)
    # sendPath('uP02s',path2)

    # try:
    #     path3 = paths[2]
    #     path3.poses[0].pose.orientation.z = -1
    #     sendPath('uP03s',path3)
    # except:
    #     pass

    # try:
    #     path4 = paths[3]
    #     sendPath('uP04s',path4)
    # except:
    #     pass

    # rospy.sleep(0.5)
