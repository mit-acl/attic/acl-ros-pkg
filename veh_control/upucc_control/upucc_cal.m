% getting rough estimate of command to wheel speed for upucc
% data collected using a 8.0V battery at the start of the test
% unforutately, real-time battery voltage was not collected

cmd_r = .1:.1:1;
omega = [1.25, 4.0, 6.6, 9.1, 11.9, 14.1, 16.1, 18.0, 19.6, 21.3];
omega = [1.35, 4.1, 6.8, 9.5, 12.7, 15.3, 18.0, 20.5, 23.1, 26.0]; % w/ 5V reg
wheel_base = 0.083; % m

% since we are spinning in place, make this assumption
v_r = omega*wheel_base/2;

figure(1); clf;
plot(v_r, cmd_r)

disp('coeffs for computing omega based on deltaCmd')
p = polyfit(v_r, cmd_r, 2)
hold all
plot(v_r, polyval(p, v_r))
legend('data', 'fit');

figure(2); clf;
delta_r = 2*cmd_r;
plot(omega, delta_r)

% sanity check
% controller wants 5 rad/s
delta_v = wheel_base*5;
v_r = delta_v/2;
cmd_r = polyval(p, v_r)
2*cmd_r

%% good

% thus, cmd_r = polyval(p, v_r)
