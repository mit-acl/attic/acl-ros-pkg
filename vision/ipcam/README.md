#IpCam#
IpCam provides an easy way to get timestamped video steam through wifi with an Android phone.

##Install Customized IP Webcam App onto Android Phone##
Copy the file `com.pas.webcam-184.apk` to your Android phone. Use an apk installer (ex: [apk installer][apk-installer]) to install the customized app. After installation you should see an app named `IP Webcam` on your Android.

__Special Note:__ The customized version of the IP Webcam app is provided generously by [Pavel Khlebovich][pk-homepage], the author of the app [IP Webcam][ipwebcam]. Please do not distribute the apk file.

##Adjust IP Webcam Settings##
Adjust the app for the ideal framerate/quality.

Open the app. Under `Video Preferences`, adjust the video resolution. 320x240 should be good on most phone. Also adjust `FPS Limit` to the maximum possible (depends on the phone).

Under `Sensors`, make sure the `Enable sensors` is unchecked for better framerate.

Under `Audio mode`, disable audio for better framerate.

##Run Streaming Demo##
Make sure your phone and your computer is connected to the same wireless network.

Open the app, click `Start server`. You should see the live stream on your phone screen with the ip on the bottom.

Go to the folder that contains ipcam.py. Run the following command from a terminal:
```
$ python ipcam.py 128.31.33.209:8080
```
(replace the ip and port by the ones shown on your phone)

You should see a live steam on your computer and the terminal should be printing out the Epoch timestamp of when the image frame was taken on the phone.

## Run as a ROS node ##
You can use the `ipcam.launch` launch file to launch a `ipcam_node` that publish `sensor_msgs.Image` message.

Supported arguments are:
* `host`: Host of the ipcam. ex: `192.167.2.101:8080`
* `rate`: Specify desired FPS. ex: `30.0`
* `name`: Specify name of the node. Make sure each ipcam_node has different names when using multiple ipcams.
* `frame_id`: Specify the `frame_id` of the `Image` message. Default is `""`.
* `show_fps`: If set to `true` will show FPS estimatoin for each frame. Default `false`.

Example:
```
$ roslaunch ipcam ipcam.launch host:=10.0.0.5:8080 name:=ipcam_BQ04 show_fps:=true rate:=30.0 frame_id=camera_BQ04
```

## Dependencies ##
The `ipcam.py` code depends on

* The `cv2` module. See [Installing python-opencv][python-opencv]
* The `requests` module. See [Installing Requests][requests]

The `ipcam_node.py` additionally depends on

* The `cv_bridge` ROS package. See [cv_bridge][cv_bridge] 

NOTE: this has already been installed on boeing under the "Home/ipcam" folder

[apk-installer]: https://play.google.com/store/apps/details?id=com.graphilos.apkinst&hl=en
[ipwebcam]: https://play.google.com/store/apps/details?id=com.pas.webcam&hl=en
[pk-homepage]: http://ip-webcam.appspot.com/
[open-cv]: https://help.ubuntu.com/community/OpenCV#Installation
[python-opencv]: http://askubuntu.com/questions/447409/how-to-install-opencv-2-9-for-python
[requests]: http://docs.python-requests.org/en/latest/user/install/#install
[cv_bridge]: http://wiki.ros.org/cv_bridge

# Time syncing
The timestamp provided by the ipcam is the Android time, not the time on the computer. To have more accurate time information, you need to sync the time between the android phone and your machine.

One way to do so is to use Network Time Protocol (NTP).

##Install NTP on your computer
In terminal
```
$ sudo apt-get install ntp
```
This will install NTP and start a NTP server on your machine.
If you just want to sync the Android time with your machine, no additoinal setup is necessary.
You can sync your machine time with other machines by editing `/etc/ntp.conf`.
For more details, see [How to setup NTP][ntp_how_to].

##Root your phone
A lot of Android apps provide NTP time lookup, however to set Android time using a NTP server requires root access.
* [How to root Moto G][root_moto_g]

##Use NTPSync to synce time
Install the [NTPSync][ntp_sync] Android app and set the NTP Server to the IP of the computer you want to sync time with. Then select `Query and set time` to sync time with the computer.

#Activate Pro Version
`rmads(3picxeaz)`

* 353216057099322 
* 990004549265225

`rmads(3pwomqjg)`


[ntp_sync]: https://play.google.com/store/apps/details?id=org.ntpsync
[ntp_how_to]: http://askubuntu.com/questions/488072/setting-up-a-standalone-ntp-server-on-ubuntu
[root_moto_g]: http://www.theandroidcop.com/how-to-root-motorola-moto-g/
