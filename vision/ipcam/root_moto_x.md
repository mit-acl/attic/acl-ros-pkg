# How to root Moto X #

# Install adb and fastboot #

```
$ sudo apt-get install android-tools-fastboot
$ sudo apt-get install android-tools-adb
```

# Get the oem string
google account: `acl.camera@gmail.com`

password: `r*****r*****`

Follow instruction on this [page](https://motorola-global-portal.custhelp.com/app/standalone/bootloader/unlock-your-device-b)

If it says your developer version Moto X is not eligible, following instruction on this [page](https://forums.motorola.com/posts/f2061da875) and send the support an email. You will receive your unlock code.

# Apply the unlock code
Follow instruction on this [page](https://motorola-global-portal.custhelp.com/app/standalone/bootloader/unlock-your-device-c)
This unlocks the bootloader.

## Unlock Code for Moto X that are not working with the unlock page
You can check the IMEI of the phone by dailing `*#06*`

* IMEI 990004549474314: `JTC43WCYFYRGH4HHWFOD `
* IMEI 990004549265225:  LTKYWHLGOWUVJQQBEI6J

# Get root access
Follow instruction on this [page](http://www.reddit.com/r/MotoX/comments/20j27k/the_ultimate_moto_x_root_thread/)
