"""
IpcamGrabber

@date: Dec. 30, 2014
@author: Shayegan Omidshafiei <shayegan@mit.edu> 
@author: Shih-Yuan Liu <syliu@mit.edu>
"""

import cv2
import numpy as np
import sys
import time
import requests

class IpCam(object):
    def __init__(self,host):
        self.host = host
        self.hoststr = 'http://' + self.host + '/shot.jpg?timehdr=1'
        self.cargo = None
        self.rawImg = None
        self.timeStamp = None
        # self.grabFrame()

    def grabFrame(self):
        # Use requests to grab data. Return True if grab is successful, False if not.
        # Grabbed data are saved to self.cargo, self.rawImg, and self.timeStamp.
        
        try:
            self.cargo = requests.get(self.hoststr)
        except requests.exceptions.RequestException as e:
            # print e
            # rospy.logdebug(e)
            return False

        jpgBytes = self.cargo.content
        rawImg = cv2.imdecode(np.fromstring(jpgBytes, dtype=np.uint8),cv2.CV_LOAD_IMAGE_COLOR)
        if type(rawImg) == np.ndarray:
            self.rawImg = cv2.imdecode(np.fromstring(jpgBytes, dtype=np.uint8),cv2.CV_LOAD_IMAGE_COLOR)
        else:
            return False
        
        # Get Android timestamp if available
        if 'x-timestamp-microseconds' in self.cargo.headers.keys():
            self.timeStamp = float(self.cargo.headers['x-timestamp-microseconds'])/1000000.0
        else:
            # This works with non-customized version of ipcam.
            self.timeStamp = rospy.Time.now()
        return True

if __name__ == '__main__':
    if len(sys.argv)>1:
        host = sys.argv[1]
    else:
        print 'Please provide host and port.'
        exit(0)
        
    grabber = IpCam(host)
    print 'Streaming:' + host
    last_time = time.time()
    while True:
        issue_time = time.time()
        if grabber.grabFrame():
            rawImg = grabber.rawImg
            timeStamp = grabber.timeStamp
            receive_time = time.time()

        print "FPS: %s Receive Latency: %s Front Diff: %s Rear Diff: %s" %(1/(receive_time-last_time),
            issue_time - receive_time, timeStamp - issue_time,receive_time - timeStamp)
        last_time = receive_time

        cv2.imshow(host,rawImg)
        if cv2.waitKey(1)==27:
            exit(0)




