#!/usr/bin/env python
import rospy
import time
from sensor_msgs.msg import Image

from ipcam import IpCam
from cv_bridge import CvBridge, CvBridgeError

def setup_param(param_name,param_value):
    if not rospy.has_param(param_name):
        rospy.set_param(param_name,param_value)
    return rospy.get_param(param_name) 

if __name__ == '__main__':
    rospy.init_node('ipcam', anonymous=False)
    pub = rospy.Publisher('~image', Image, queue_size=1)
    
    host = setup_param('~host','192.168.0.116')
    rate = setup_param('~rate',30.0)
    frame_id = setup_param("~frame_id","")
    show_fps = setup_param("~show_fps",False)
    node_name = rospy.get_name()
    r = rospy.Rate(rate)

    grabber = IpCam(host)
    rospy.loginfo("%s streaming %s" %(node_name,host))
    
    bridge = CvBridge()
    last_time = time.time()
    
    while not rospy.is_shutdown():
        issue_time = time.time()
        if grabber.grabFrame():
            # rospy.loginfo("[ipcam] grabbed frame.")
            # Got a frame
            rawImg = grabber.rawImg
            timeStamp = grabber.timeStamp
            receive_time = time.time()
            msg = bridge.cv2_to_imgmsg(rawImg,'bgr8')
            msg.header.stamp = rospy.Time(timeStamp)
            msg.header.frame_id = frame_id
            pub.publish(msg)

            fps = 1.0/(receive_time - last_time)
            if show_fps:
                rospy.loginfo("%s FPS: %.1f Latency: %f" %(host,fps,(receive_time - timeStamp)))
            last_time = receive_time
        else:
            # rospy.loginfo("[ipcam] no frame")
            if show_fps:
                rospy.loginfo("%s no image." %(node_name))
        r.sleep()

    rospy.loginfo("%s terminated." %(node_name))


